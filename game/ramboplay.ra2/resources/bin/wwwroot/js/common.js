if (self.frameElement && self.frameElement.tagName == "IFRAME") {
    var ipcRenderer = parent.ipcRenderer
} else {
    var { ipcRenderer } = require("electron");
}
var indev = getQueryVariable('code')
if (indev == 'test') {
    localStorage.setItem('test', 1)
    localStorage.removeItem('pre');
}
if (indev == 'pre') {
    localStorage.setItem('pre', 1)
    localStorage.removeItem('test');
}
if (indev == 'prod') {
    localStorage.removeItem('test');
    localStorage.removeItem('pre');
}


// 用户信息
var userInfo = ''
// 全部配置信息
var comOption = {
    optionsCountry: [
        { sideId: 10, country: '', cname: '观战' },
        { sideId: -1, country: ' ', cname: '随机' },
        { sideId: 2, country: 'France', cname: '法国' },
        { sideId: 6, country: 'Iraq', cname: '伊拉克' },
        { sideId: 1, country: 'Korea', cname: '韩国' },
        { sideId: 5, country: 'Libya', cname: '利比亚' },
        { sideId: 0, country: 'America', cname: '美国' },
        { sideId: 7, country: 'Cuba', cname: '古巴' },
        { sideId: 3, country: 'Germany', cname: '德国' },
        { sideId: 8, country: 'Russia', cname: '苏俄' },
        { sideId: 4, country: 'GreatBritain', cname: '英国' },
        { sideId: 9, country: 'Yuri', cname: '尤里' },
    ],
    optionsColor: [
        { colorId: -1, color: '', name: '随机' },
        { colorId: 0, color: '222,227,8', name: 'Gold', cname: '黄色' },
        { colorId: 1, color: '255,24,24', name: 'Red', cname: '红色' },
        { colorId: 2, color: '41,117,231', name: 'Teal', cname: '蓝色' },
        { colorId: 3, color: '57,211,41', name: 'Green', cname: '绿色' },
        { colorId: 4, color: '255,162,24', name: 'Orange', cname: '橙色' },
        { colorId: 5, color: '49,215,231', name: 'Blue', cname: '青色' },
        { colorId: 6, color: '148,40,189', name: 'Purple', cname: '紫色' },
        { colorId: 7, color: '255,154,239', name: 'Metalic', cname: '粉色' }
    ],
    optionsTeam: [
        { teamId: -1, name: '单人' },
        { teamId: 0, name: 'A队' },
        { teamId: 1, name: 'B队' },
        { teamId: 2, name: 'C队' },
        { teamId: 3, name: 'D队' },
    ],//队伍配置
    optionsMode: [
        { name: '作战', code: 'Battle', dec: '作战模式下可建造所有单位。但热门官方地图北极圈在小队结盟模式下' },
        { name: '邪恶联盟', code: 'Unholy Alliance', dec: '邪恶联盟模式下，每位玩家都拥有苏/盟两国基地车（YR模式下还有尤里基地车），可以建筑除特色兵种外的相应建筑和兵种' },
        { name: '海战', code: 'Naval War', dec: '海战模式下，您不能够建造陆军和空军单位。' },
        { name: '生死斗', code: 'Meat Grinder', dec: '生死斗模式下，玩家只可以建造基本的陆战单位和国家特色单位。不能建造作战实验室。天启坦克/光棱坦克等。' },
        { name: '抢地盘', code: 'Land Rush', dec: '抢地盘模式下，玩家出生在一起。需要将基地车移动到合适的位置来发育。' },
        { name: '巨富', code: 'Megawealth', dec: '巨富模式下，玩家不能建造矿场和采矿车，需要用工程师占领油井来获取经济。' },
        { name: '抢箱子', code: 'PickBox', dec: '捡箱子模式下，您只能用初始部队捡起地图上随机出现的箱子获得作战单位，不能建造和生产' },
        { name: '闪击战', code: 'Blitz', dec: '闪击战使用特殊的小型地图，苏军或盟军将共享科技，尤里科技为苏军科技。玩家需要建造最多 12 个油井来获取经济。模式作者: RA2CashGames' }
    ],//mode配置
    labelArray: [
        { name: '全部', code: '' },
        { name: '经典对战', code: 'Battle' },
        { name: '娱乐对战', code: 'Recreation' },
        { name: '任务', code: 'Task' },
        { name: '防守', code: 'Defense' },
    ],
    settingArray: [
        { name: '可摧毁桥梁', code: 'BridgeDestroy' },
        { name: '超级武器', code: 'Superweapons' },
        { name: '工具箱', code: 'Crates' },
        { name: '基地重新部署', code: 'MCVRedeploy' },
        { name: '快速游戏', code: 'ShortGame' },
    ],
    optionsGameVersion: [
        { name: '尤里的复仇', code: '0', en: 'yuri' },
        { name: '红警原版', code: '1', en: 'war' },
        { name: '共和国之辉', code: '3', en: 'republic' }
    ],
    userBot: [
        { name: '主播', img: '/images/iconfont/user-anchor.png', code: 1 },
        { name: '明星', img: '/images/iconfont/user-star.png', code: 2 },
        { name: '官方', img: '/images/iconfont/user-official.png', code: 3 },
        { name: '地图作者', img: '/images/iconfont/user-map.png', code: 4 }
    ],
    roomTypeArray: [
        { cname: '自由模式', code: 0, type: 1, minMapPlayer: 1 },
        { cname: '混战模式', code: 1, type: 1, minMapPlayer: 1 },
        { cname: '1V1模式', code: 2, type: 2, minMapPlayer: 2 },
        { cname: '2V2模式', code: 3, type: 2, minMapPlayer: 4 },
        { cname: '3V3模式', code: 4, type: 2, minMapPlayer: 6 },
        { cname: '4V4模式', code: 5, type: 2, minMapPlayer: 8 }
    ],
    roomTypeVerandaArray: [
        { cname: '自由', code: 0, type: 1, minMapPlayer: 1 },
        { cname: '混战', code: 1, type: 1, minMapPlayer: 1 },
        { cname: '1V1', code: 2, type: 2, minMapPlayer: 2 },
        { cname: '2V2', code: 3, type: 2, minMapPlayer: 4 },
        { cname: '3V3', code: 4, type: 2, minMapPlayer: 6 },
        { cname: '4V4', code: 5, type: 2, minMapPlayer: 8 }
    ],
    LBWlanguage: [
        { name: '简体中文', code: 'zh' },
        { name: '繁體中文', code: 'zh_TW' },
        { name: 'English', code: 'en' },
        { name: ' بالعربية ', code: 'ar' }
    ],
    optionsLevels: [],
    optionsAvatar: [],
    optionscomplaint: [],
    guideCode: {
        newHelp: '1'
    },//引导
    roomConfig: null
}
userInfo = JSON.parse(localStorage.getItem('userInfo'))

// document.onkeydown = function (e) {//键盘按键控制
//     e = e || window.event; if ((e.ctrlKey && e.keyCode == 82) || //ctrl+R
//         e.keyCode == 116) {//F5刷新，禁止
//         location.reload()
//     }

//     if (e.code == 'F6') {
//         ipcRenderer.send('Main', {
//             msgType: "ShowModal",
//             jsonInfo: {
//                 window: 'Stylemodal',
//                 url: '/Styleui/Index',
//                 width: 1000,
//                 height: 1200,
//             }
//         });
//     }
// }
// 导航栏
$(document).on('click', '.gx-nav-item', function () {
    $this = $(this)
    $this.closest('.gx-nav').find('.gx-nav-item').removeClass('gx-nav-item-active')
    $this.addClass('gx-nav-item-active')
})
//窗口方法
$(document).on('click', '.window-operation', function () {
    $this = $(this)
    var type = $this.attr('data-type')
    var windowName = $this.attr('data-window') || 'main'
    // 关闭方法
    controlWindow(windowName, type)
})
function controlWindow(windowName, type) {
    if (type == 'close') {
        ipcRenderer.send('Main', {
            msgType: "Close",
            jsonInfo: {
                window: windowName
            }
        });
        if (windowName != 'settingmodal') {
            setTimeout(() => {
                window.close();
            }, 200)
        }
    } else {
        if (type == 'hide') {
            $('body').css('display', 'none')
            window.location.reload()
        }
        ipcRenderer.send('Main', {
            msgType: "WindowSize",
            jsonInfo: {
                window: windowName,
                size: type
            }
        });

    }
}
//btn loading 
$.fn.gxbtn = function (type, text) {
    var $that = $(this)
    if (type == 'loading') {
        $that.addClass('gx-disabled')
        $that.data('button', $that.html())
        if (text) {
            $that.html('<span>' + text + '</span>')
        } else {
            $that.html('<div class="gx-loading"></div>')
            setTimeout(() => {
                $that.find('.gx-loading').addClass('gx-loading-show')
            }, 1)
        }
    } else if (type == 'reset') {
        setTimeout(() => {
            $that.find('.gx-loading').removeClass('gx-loading-show')
        }, 1)
        setTimeout(() => {
            $that.removeClass('gx-disabled')
            if (text) {
                $that.html(text)
            } else {
                $that.html($that.data('button'))
            }
        }, 300)
    }
}
$.fn.gxmenuin = function (type, text) {
    var $that = $(this)
    if (type == 'loading') {
        $that.addClass('gx-disabled')
    } else if (type == 'reset') {
        $that.removeClass('gx-disabled')
    }
}
// $.fn.gxaniamteBtn = function (type, text) {
//     var $that = $(this)
//     var btnaniamteposition = 0
//     var btnaniamtepositionend = -1824
//     var btnaniamtepositionendclick = false
//     $that.css('background-position-y', 0)
//     setInterval(() => {
//         btnaniamteposition = btnaniamteposition - 48
//         if (btnaniamteposition < btnaniamtepositionend) {
//             if (btnaniamtepositionendclick) {
//                 $that.css('background-position-x', -165)
//                 btnaniamteposition = 0
//                 btnaniamtepositionend = -1152
//             }
//             btnaniamteposition = 0
//         }
//         $that.css('background-position-y', btnaniamteposition)
//     }, 60)
//     $that.on('mouseenter', function () {
//         $that.css('background-position-x', -165)
//         btnaniamteposition = 0
//         btnaniamtepositionend = -1000
//     })
//     $that.on('mouseleave', function () {
//         $that.css('background-position-x', 0)
//         btnaniamteposition = 0
//         btnaniamtepositionend = -1824
//     })
//     $that.on('click', function () {
//         $that.css('background-position-x', -330)
//         btnaniamteposition = 0
//         btnaniamtepositionend = -720
//         btnaniamtepositionendclick = true
//     })
// }
//swich
$.fn.gxswich = function (option, callback) {
    var $that = $(this)
    var rangeSetTimeout = null
    $that.find('input[type="range"]').rangeslider({ polyfill: false });
    $that.find('input[type="range"]').on('input', function (e) {
        $that.find('.gx-swich-num').text(e.target.value)
        $that.find('.gx-swich-num-input input').val(e.target.value)
        clearTimeout(rangeSetTimeout)
        rangeSetTimeout = setTimeout(() => {
            callback(e.target.value)
        }, 300)
    });
    $that.find('.gx-swich-num-input input').on('input', function (e) {
        var $this = $(this)
        var getnum = Number($this.val())
        if (getnum > Number($this.attr('max'))) { getnum = ($this.attr('max')) }
        clearTimeout(rangeSetTimeout)
        rangeSetTimeout = setTimeout(() => {
            if (getnum < Number($this.attr('min'))) { getnum = ($this.attr('min')) }
            var srwert = (getnum - 5000) / (Number($this.attr('max')) - Number($this.attr('min'))) * ($that.find('.rangeslider').width() - $that.find('.rangeslider__handle').width())
            $that.find('.gx-swich-num-input input').val(getnum)
            $that.find('.rangeslider__handle').css('left', srwert)
            callback(getnum)
        }, 500)
    })
}
// 弱提示
function showtoast(option) {
    let optionbody = option.body || 'body'
    let optiontime = option.time || 3000
    $(optionbody).toast({
        position: 'fixed',
        content: option.message,
        duration: optiontime,
        isCenter: true,
        background: 'rgba(90, 101, 133, 1)',
        animateIn: 'bounceInUp-hastrans',
        animateOut: 'bounceOutDown-hastrans',
    });
}
// 下拉
$.fn.gxmenu = function (option, callback) {
    var $that = $(this)
    var clickHide = true
    let dataplacement = $that.attr('data-placement')
    if (!option.clickHide) {
        clickHide = false
    }
    if (callback == 'hide') {
        gxmenuHide($(this))
        return false
    }
    var topheight = option.top || 40
    $that.find('.gx-menu-ul').css('top', topheight)
    if (dataplacement == 'top') {
        $that.find('.gx-menu-ul').css({ top: 'auto', bottom: '0px' })
        $that.find('.gx-menu-ul').css('bottom', topheight)
    } else {
        $that.find('.gx-menu-ul').css('top', topheight)
    }
    $that.each(function () {
        var $this = $(this)
        var $this_in = $(this).find('.gx-menu-in')
        $this_in.off('click').on('click', function () {
            if ($this.hasClass('gx-menu-active')) {
                $this.removeClass('gx-menu-active')
                $this.find('.gx-menu-ul').css({ 'height': 0, 'overflowY': 'hidden', 'border': 'none' })
                // $this.find('.gx-menu-li').each(function (index, item) {
                //     $(this).addClass('animated fadeOutRight');
                //     setTimeout(() => {
                //         $(this).removeClass('fadeOutRight');
                //     }, 1000);
                // })
            } else {
                $(".gx-menu-active").each(function () {
                    gxmenuHide($(this))
                })
                var num = $this.find('.gx-menu-li').length * parseInt(option.height) + 2
                let maxheight = 200
                if (option.maxheight) {
                    maxheight = option.maxheight
                    $this.find('.gx-menu-ul').css('max-height', maxheight)
                }
                if (num > maxheight) { num = maxheight }
                if (num > 2) {
                    $this.addClass('gx-menu-active')
                    $this.find('.gx-menu-ul').css('height', num + "px")
                    $this.find('.gx-menu-ul').css('border', '1px solid #3F599B')
                }
                setTimeout(() => {
                    // $this.find('.gx-menu-ul').css('overflowY', "scroll")
                    $this.find('.gx-menu-ul').css('overflowY', "auto")
                }, 300)
                // $this.find('.gx-menu-li').each(function (index, item) {
                //     $(this).addClass('animated fadeInRight');
                //     setTimeout(() => {
                //         $(this).removeClass('fadeInRight');
                //     }, 1000);
                // })
            }
        })
        if (clickHide) {
            $(this).find('.gx-menu-ul').off('click').on('click', function () {
                $this.removeClass('gx-menu-active')
                $this.find('.gx-menu-ul').css({ 'height': 0, 'overflowY': 'hidden', 'border': 'none' })
            })
        }
    })
};
// 下拉
function gxmenuHide(obj) {
    if (obj.hasClass('gx-menu-active')) {
        obj.removeClass('gx-menu-active')
        obj.find('.gx-menu-ul').css({ 'height': 0, 'overflowY': 'hidden', 'border': 'none' })
        obj.find('.gx-menu-li').each(function (index, item) {
            // $(this).addClass('animated fadeOutRight');
            // setTimeout(() => {
            //     $(this).removeClass('fadeOutRight');
            // }, 1000);
        })
    }
}
// 下拉
$(document).on('click', function (e) {
    var target = $(e.target);
    if (target.closest(".gx-menu").length == 0) { //点击id为parentId之外的地方触发
        $(".gx-menu-active").each(function () {
            gxmenuHide($(this))
        })
    }
})
// 取地址
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}
// 特殊输入框
$(document).on('focus', '.login-gx-input .gx-input', function () {
    $(this).closest('.login-gx-input').addClass('login-gx-input-focus')
})
$(document).on('blur', '.login-gx-input .gx-input', function () {
    $(this).closest('.login-gx-input').removeClass('login-gx-input-focus')
})
// 获取URL、
function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) { return pair[1]; }
    }
    return (false);
}
// 打开浏览器
function OpenWebUrl(url) {
    ipcRenderer.send('Main', {
        msgType: "OpenExternal",
        jsonInfo: {
            url
        }
    });
}
window.addEventListener('message', function (e) {
    if (e.origin.indexOf('ramboplay') != -1 || e.origin.indexOf('ra2') != -1) {
        if (e && e.data && typeof e.data == 'string' && e.data.indexOf('{') != -1) {
            let gete = JSON.parse(e.data)
            if (gete.type == 'openwindow') {
                OpenRamUrl(gete.data)
            } else if (gete.type == 'openarticle') {
                if (window.location.href.indexOf('/Hall/index') != -1) {
                    $.star({
                        type: 'GET',
                        url: '/battlecenter/article/v1/query/one',
                        showerror: true,
                        data: {
                            id: gete.data
                        },
                        success: function (res) {
                            let message = res.data
                            $('#informationtextModal .modal-dialog .informationtext-left').html(template('informationtextModal-script', { message: message }))
                            $('#informationtextModal .informationtext-right').html(template('informationtext-right-script'))
                            informationMessage.gethotlivelistfun(informationMessage.livelist)
                            informationMessage.getHotList()

                            $('.informationtext-text').html(message.articleContent)
                            $('#informationtextModal').modal('show')
                        },
                        error: function (req) {

                        }
                    })
                }
            }
        }
    }
}, false);


// 打开浏览器aoken
function OpenRamUrl(url) {
    $.star({
        type: 'POST',
        url: '/community-user/authorize/generate/code',
        showerror: true,
        success: function (coderes) {
            let tourl = url
            if (tourl.indexOf('?') != -1) {
                OpenWebUrl(tourl + '&token=' + coderes.data.authCode)
            } else {
                OpenWebUrl(tourl + '?token=' + coderes.data.authCode)
            }
        },
        error: function (req) {

        }
    });
}
// 地图放大功能
$(document).on('click', '.gx-map', function (e) {
    e.stopPropagation();
    if ($('#gx-map-modal').length > 0) {
        return false
    }
    var $this = $(this)
    if ($this.attr('data-nobig')) {
        return false
    }
    var imgName = $this.attr('title')
    var imgUrl = ""
    if ($this.css('background').match(/url\(\"(\S*)\"\)/)) {
        imgUrl = $this.css('background').match(/url\(\"(\S*)\"\)/)[1];
    }
    if ($this.attr('data-map')) {
        imgUrl = $this.attr('data-map')
    }
    $('body').append('<div class="modal fade" id="gx-map-modal" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="reportModal"><div class="modal-dialog" role="document"><div class="relative" style="display: inline-block;"><img data-dismiss="modal" src="' + imgUrl + '" alt=""><div class="gx-map-modal-name text-left">' + imgName + '</div></div></div>')
    $('#gx-map-modal').modal('show')
    $('#gx-map-modal').on('shown.bs.modal', function () {
        if ($('#gx-map-modal img').height() > $('#gx-map-modal img').width()) {
            $('#gx-map-modal .modal-dialog').css('margin-top', '13vh')
            $('#gx-map-modal .relative').css('width', 'auto')
            $('#gx-map-modal .relative img').css('width', 'auto')
            $('#gx-map-modal .relative img').css('height', '64vh')
        }
    })
    $('#gx-map-modal').on('hidden.bs.modal', function () {
        $('#gx-map-modal').remove();
    })
})
// 弹窗
function gxmodal(option) {
    // if ($('.gx-gxmodal').length > 0) {
    //     return false
    // }
    var btnHTML = ""
    if (option.buttons) {
        for (var i = 0; i < option.buttons.length; i++) {
            option.buttons[i].id = 'gx-gxmodal-btn-' + i
            btnHTML = btnHTML + '<a href="javascript:void(0)" ondragstart="return false" id="' + option.buttons[i].id + '" class="' + option.buttons[i].class + '">' + option.buttons[i].text + '</a>'
        }
    }
    var titleHtml = ""
    if (option.title) {
        titleHtml = '<div class="ft-white-tr f16 text-center">' + option.title + '</div>'
    }
    let idhtml = '<div class="modal gx-gxmodal" id="gx-gxmodal"'
    let idrandom = ""
    if (option.id) {
        idrandom = option.id
        idhtml = '<div class="modal gx-gxmodal" id="' + option.id + '"'
    } else {
        idrandom = "gx-gxmodal" + parseInt(Math.random() * 100)
        idhtml = '<div class="modal gx-gxmodal" id="' + idrandom + '"'
    }
    var getHTML = idhtml + ' " data-keyboard="false" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="ActivateCodeModal"><div class="modal-dialog gx-modal-smell-dialog" role="document"><div class="gx-modal-smell"><div class="gx-modal-smell-header"><div class="header-window"><span class="header-close iconfont ft-ash" data-dismiss="modal">&#xe602;</span></div></div><div class="text-center">' + titleHtml + option.centent + '</div><div class="gx-gxmodal-foot">' + btnHTML + '</div></div></div></div>'
    $('body').append(getHTML)
    let idDOM = ""
    if (option.id) {
        idDOM = $('#' + option.id)
    } else {
        idDOM = $('#' + idrandom)
    }
    if (option.closeCoinHide) {
        idDOM.find('.header-window').addClass('hide')
    }
    idDOM.modal('show')
    if (option.buttons) {
        for (var i = 0; i < option.buttons.length; i++) {
            let btnobj = $('#' + idrandom + ' #' + option.buttons[i].id)
            if (option.buttons[i].callback) {
                btnobj.one('click', option.buttons[i].callback)
            }
            if (option.buttons[i].preventDefault) {

            } else {
                btnobj.one('click', function () { idDOM.modal('hide') })
            }
        }
    }
    idDOM.on('hidden.bs.modal', function () {
        idDOM.remove();
    })
};
// 去除回退
jQuery(document).ready(function () {
    if (window.history && window.history.pushState) {
        $(window).on('popstate', function () {/// 当点击浏览器的 后退和前进按钮 时才会被触发， 
            window.history.pushState('forward', null, '');
            window.history.forward(1);
        });
    }
    window.history.pushState('forward', null, '');  //在IE中必须得有这两行
    window.history.forward(1);
});
// 用户信息展示
$(document).on('mouseleave', '.gx-avatar-hover', function () {
    $(this).removeClass('gx-avatar-hover-hover')
})

$(document).on('mouseenter', '.gx-avatar-hover', function () {
    $(this).addClass('gx-avatar-hover-hover')
    var $this = $(this)
    var isbody = $this.attr('data-body')
    var isparent = $this.attr('data-parent')
    var needfriend = $this.attr('data-friend') || false
    var placement = $this.attr('data-placement') || 'right'
    if ($this.attr('data-html')) {
        return false
    }
    $.star({
        type: 'GET',
        url: '/battlecenter/redwar/user/v1/info/hover',
        showerror: false,
        data: {
            uid: $this.attr('data-id'),
        },
        success: function (res) {
            var friendDOM = ""
            if (needfriend && $this.attr('data-id') != userInfo.userId) {
                // 是否需要增加好友参数 相较普通需要掉接口
                carefriend($this, res, isbody, isparent, needfriend, placement)
                return false
            }
            if (!res.data.matchCount) {
                res.data.matchCount = "--"
            }
            if (!res.data.offlineRate) {
                res.data.offlineRate = "0%"
            }
            if (!res.data.winRate) {
                res.data.winRate = "--"
            }
            var isRating = true
            isRating = res.data.userLevel.rating
            let userlevel = ""
            if (res.data.isShowUserLevel || res.data.uid == userInfo.userId) {
                let levelname = translatesrting(res.data.userLevel.levelName) + res.data.userLevel.secondLevel
                if (res.data.userLevel.level == 8) {
                    levelname = translatesrting(res.data.userLevel.levelName)
                }
                userlevel = '<div class="gx-avatar-content-level-div gx-avatar-content-level-div' + res.data.userLevel.level + '"><div class="gx-avatar-content-level-shak"></div><div class="gx-avatar-content-level-shak"></div><div class="gx-avatar-content-level-shak"></div><div class="gx-avatar-content-level-shak"></div><img src="/images/rank/badge-min/ribbon/' + res.data.userLevel.level + '.png" width="30" alt=""/><div class="gx-avatar-content-level-name">' + levelname + '</div></div>'
            }
            if (!res.data.userLevel.rating) {
                userlevel = ""
            }
            var popoverhtml = '<div class="gx-avatar-content"><div class="media gx-avatar-content-top"><div class="gx-avatar-content-gameAge">' + userlevel + '</div><div class="media-left media-middle relative">' + friendDOM + '<div class="gx-avatar-avatar">'
            if (res.data.avatarFrame != '') {
                popoverhtml += '<div class="gx-frame" style="background-image: url(' + formatapngfun(res.data.avatarFrame, 173) + ')"></div>'
            } else {
                popoverhtml += '<div class="gx-frame"></div>'
            }
            popoverhtml += '<img src="' + res.data.avatar + '?x-oss-process=image/resize,w_92,h_92' + '" alt=""></div></div><div class="media-body media-top"><div class="name-ellipsis" style="max-width:84px;margin-left: 12px;margin-top: 15px">' + res.data.username + '</div>'
            if (res.data.authenticationType) {
                let imgurl = ''
                let triggertext = ''
                comOption.userBot.map((item) => {
                    if (res.data.authenticationType == item.code) {
                        imgurl = item.img
                    }
                })
                if (res.data.authenticationContent) {
                    triggertext = 'data-toggle="tooltip" data-trigger="hover" title="' + res.data.authenticationContent + '" data-placement="bottom"'
                }
                popoverhtml += '<img ' + triggertext + ' class="gx-avatar-type-icon" src="' + imgurl + '" alt="type" width="20">'
            }
            popoverhtml += '</div></div><div class="gx-avatar-content-row row ft-A1"><div class="gx-avatar-content-row-border"></div><div class="gx-avatar-content-row-border"></div><div class="gx-avatar-content-row-border"></div><div class="col-xs-3">' + translatesrting('游戏天数') + '<br/><b class="ft-white">' + res.data.gameAge + '</b></div><div class="col-xs-3">' + translatesrting('总场次') + '<br/><b class="ft-white">' + res.data.total + '</b></div><div class="col-xs-3">' + translatesrting('胜率') + '<br/><b class="ft-white">' + res.data.totalWinRate + '</b></div><div class="col-xs-3">' + translatesrting('掉线率') + '<br/><b class="ft-white">' + res.data.offlineRate + '</b></div></div></div>'
            if (isparent) {
                $this.popover({
                    html: true,
                    container: isbody,
                    content: popoverhtml,
                    placement: placement
                })
                if ($this.hasClass('gx-avatar-hover-hover')) {
                    $this.popover('show')
                }
                $this.parent().on('mouseenter', function () {
                    $this.popover('show')
                })
                $this.parent().on('mouseleave', function () {
                    $this.popover('hide')
                })
            } else {
                $this.popover({
                    trigger: 'hover',
                    html: true,
                    container: isbody,
                    content: popoverhtml,
                    placement: placement
                })
                if ($this.hasClass('gx-avatar-hover-hover')) {
                    $this.popover('show')
                }
            }
            $this.attr('data-html', popoverhtml)
        },
        error: function (req) {
        }
    });
})
// 需要好友
function carefriend($this, res, isbody, isparent, needfriend, placement) {
    $.star({
        type: 'GET',
        url: '/community-user/friend/vi/user/is/friend',
        showerror: false,
        data: {
            userId: $this.attr('data-id'),
        },
        success: function (friendres) {
            var friendDOM = ""
            if (!friendres.data) {
                friendDOM = ''
                if (!res.data.userBlack) {
                    friendDOM = '<a href="javascript:void(0)" class="gx-avatar-content-friend" ondragstart="return false" data-id="' + res.data.uid + '"><span>+' + translatesrting('添加好友') + '</span></a>'
                    friendDOM += '<a href="javascript:void(0)" class="gx-avatar-black-friend" ondragstart="return false" data-blcak="addblack" data-id="' + res.data.uid + '"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="10px" viewBox="0 0 10 10" version="1.1"><g id="1.36版本（创建房间、大厅公告、设置）" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="名片（加黑名单）" transform="translate(-717.000000, -450.000000)" fill="#5F6C93" fill-rule="nonzero"><g id="黑名单-(3)" transform="translate(717.000000, 450.000000)"><path d="M7.62886598,5.25773195 C6.34020618,5.25773195 5.25773195,6.2886598 5.25773195,7.62886598 C5.25773195,8.91752577 6.2886598,10 7.62886598,10 C8.91752577,10 10,8.96907216 10,7.62886598 C9.94845361,6.2886598 8.91752577,5.25773195 7.62886598,5.25773195 Z M7.62886598,6.08247423 C7.88659793,6.08247423 8.1443299,6.18556701 8.40206185,6.2886598 L6.34020618,8.35051547 C6.18556701,8.1443299 6.13402061,7.83505155 6.13402061,7.57731959 C6.08247423,6.75257732 6.75257732,6.08247423 7.62886598,6.08247423 L7.62886598,6.08247423 Z M7.62886598,9.12371134 C7.37113402,9.12371134 7.11340206,9.07216495 6.9072165,8.91752577 L8.96907217,6.8556701 C9.07216495,7.06185567 9.17525774,7.31958763 9.17525774,7.57731959 C9.12371135,8.45360825 8.45360825,9.12371134 7.62886598,9.12371134 L7.62886598,9.12371134 Z" id="形状"/><path d="M5,5.46391752 C6.49484536,5.46391752 7.73195876,4.22680412 7.73195876,2.73195876 C7.73195876,1.2371134 6.49484536,0 5,0 C3.50515464,0 2.26804124,1.2886598 2.26804124,2.78350516 C2.26804124,3.60824743 2.62886598,4.32989691 3.19587628,4.79381443 C1.34020618,5.51546392 0,7.31958763 0,9.43298969 L0,9.94845361 L4.94845361,9.94845361 L4.94845361,8.91752577 L1.08247423,8.91752577 C1.34020618,6.95876289 2.98969073,5.46391752 5,5.46391752 L5,5.46391752 Z M3.29896907,2.78350516 C3.29896907,1.8556701 4.07216494,1.08247423 5,1.08247423 C5.92783506,1.08247423 6.70103093,1.8556701 6.70103093,2.78350516 C6.70103093,3.71134022 5.92783506,4.48453608 5,4.48453608 C4.07216494,4.48453608 3.29896907,3.65979381 3.29896907,2.78350516 Z" id="形状"/></g></g></g></svg><span>' + translatesrting('加入黑名单') + '</span></a>'
                } else {
                    // friendDOM += '<a href="javascript:void(0)" class="gx-avatar-black-friend" ondragstart="return false" data-blcak="removeblack" data-id="' + res.data.uid + '"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="10px" viewBox="0 0 10 10" version="1.1"><g id="1.36版本（创建房间、大厅公告、设置）" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="名片（加黑名单）" transform="translate(-717.000000, -450.000000)" fill="#5F6C93" fill-rule="nonzero"><g id="黑名单-(3)" transform="translate(717.000000, 450.000000)"><path d="M7.62886598,5.25773195 C6.34020618,5.25773195 5.25773195,6.2886598 5.25773195,7.62886598 C5.25773195,8.91752577 6.2886598,10 7.62886598,10 C8.91752577,10 10,8.96907216 10,7.62886598 C9.94845361,6.2886598 8.91752577,5.25773195 7.62886598,5.25773195 Z M7.62886598,6.08247423 C7.88659793,6.08247423 8.1443299,6.18556701 8.40206185,6.2886598 L6.34020618,8.35051547 C6.18556701,8.1443299 6.13402061,7.83505155 6.13402061,7.57731959 C6.08247423,6.75257732 6.75257732,6.08247423 7.62886598,6.08247423 L7.62886598,6.08247423 Z M7.62886598,9.12371134 C7.37113402,9.12371134 7.11340206,9.07216495 6.9072165,8.91752577 L8.96907217,6.8556701 C9.07216495,7.06185567 9.17525774,7.31958763 9.17525774,7.57731959 C9.12371135,8.45360825 8.45360825,9.12371134 7.62886598,9.12371134 L7.62886598,9.12371134 Z" id="形状"/><path d="M5,5.46391752 C6.49484536,5.46391752 7.73195876,4.22680412 7.73195876,2.73195876 C7.73195876,1.2371134 6.49484536,0 5,0 C3.50515464,0 2.26804124,1.2886598 2.26804124,2.78350516 C2.26804124,3.60824743 2.62886598,4.32989691 3.19587628,4.79381443 C1.34020618,5.51546392 0,7.31958763 0,9.43298969 L0,9.94845361 L4.94845361,9.94845361 L4.94845361,8.91752577 L1.08247423,8.91752577 C1.34020618,6.95876289 2.98969073,5.46391752 5,5.46391752 L5,5.46391752 Z M3.29896907,2.78350516 C3.29896907,1.8556701 4.07216494,1.08247423 5,1.08247423 C5.92783506,1.08247423 6.70103093,1.8556701 6.70103093,2.78350516 C6.70103093,3.71134022 5.92783506,4.48453608 5,4.48453608 C4.07216494,4.48453608 3.29896907,3.65979381 3.29896907,2.78350516 Z" id="形状"/></g></g></g></svg><span>' + translatesrting('移出黑名单') + '</span></a>'
                }
            }
            // var genderimg = ""
            // if (res.data.gender == 1) {
            //     genderimg = '<span class="iconfont gx-avatar-iconfont" style="color:#1C93FF">&#xe640;</span>'
            // } else if (res.data.gender == 2) {
            //     genderimg = '<span class="iconfont gx-avatar-iconfont" style="color:rgb(255,68,68)">&#xe63f;</span>'
            // }
            if (!res.data.matchCount) {
                res.data.matchCount = "--"
            }
            if (!res.data.offlineRate) {
                res.data.offlineRate = "0%"
            }
            if (!res.data.winRate) {
                res.data.winRate = "--"
            }
            var isRating = true
            isRating = res.data.userLevel.rating
            let userlevel = ""
            if (res.data.isShowUserLevel || res.data.uid == userInfo.userId) {
                let levelname = translatesrting(res.data.userLevel.levelName) + res.data.userLevel.secondLevel
                if (res.data.userLevel.level == 8) {
                    levelname = translatesrting(res.data.userLevel.levelName)
                }
                userlevel = '<div class="gx-avatar-content-level-div gx-avatar-content-level-div' + res.data.userLevel.level + '"><div class="gx-avatar-content-level-shak"></div><div class="gx-avatar-content-level-shak"></div><div class="gx-avatar-content-level-shak"></div><div class="gx-avatar-content-level-shak"></div><img src="/images/rank/badge-min/ribbon/' + res.data.userLevel.level + '.png" width="30" alt=""/><div class="gx-avatar-content-level-name">' + levelname + '</div></div>'
            }
            if (!res.data.userLevel.rating) {
                userlevel = ""
            }
            var popoverhtml = '<div class="gx-avatar-content"><div class="media gx-avatar-content-top"><div class="gx-avatar-content-gameAge">' + userlevel + '</div><div class="media-left media-middle relative">' + friendDOM + '<div class="gx-avatar-avatar">'


            if (res.data.avatarFrame != '') {
                popoverhtml += '<div class="gx-frame" style="background-image: url(' + formatapngfun(res.data.avatarFrame, 173) + ')"></div>'
            } else {
                popoverhtml += '<div class="gx-frame"></div>'
            }
            popoverhtml += '<img src="' + res.data.avatar + '?x-oss-process=image/resize,w_92,h_92' + '" alt=""></div></div><div class="media-body media-top"><div class="name-ellipsis" style="max-width:84px;margin-left: 12px;margin-top: 15px">' + res.data.username + '</div>'
            if (res.data.authenticationType) {
                let imgurl = ''
                let triggertext = ''
                comOption.userBot.map((item) => {
                    if (res.data.authenticationType == item.code) {
                        imgurl = item.img
                    }
                })
                if (res.data.authenticationContent) {
                    triggertext = 'data-toggle="tooltip" data-trigger="hover" title="' + res.data.authenticationContent + '" data-placement="bottom"'
                }

                popoverhtml += '<img ' + triggertext + ' class="gx-avatar-type-icon" src="' + imgurl + '" alt="type" width="20">'
            }
            popoverhtml += '</div></div><div class="gx-avatar-content-row row ft-A1"><div class="gx-avatar-content-row-border"></div><div class="gx-avatar-content-row-border"></div><div class="gx-avatar-content-row-border"></div><div class="col-xs-3">' + translatesrting('游戏天数') + '<br/><b class="ft-white">' + res.data.gameAge + '</b></div><div class="col-xs-3">' + translatesrting('总场次') + '<br/><b class="ft-white">' + res.data.total + '</b></div><div class="col-xs-3">' + translatesrting('胜率') + '<br/><b class="ft-white">' + res.data.totalWinRate + '</b></div><div class="col-xs-3">' + translatesrting('掉线率') + '<br/><b class="ft-white">' + res.data.offlineRate + '</b></div></div></div>'
            if (isparent) {
                $this.popover({
                    html: true,
                    container: isbody,
                    content: popoverhtml,
                    placement: placement
                })
                if ($this.hasClass('gx-avatar-hover-hover')) {
                    $this.popover('show')
                }
                $this.parent().on('mouseenter', function () {
                    $this.popover('show')
                })
                $this.parent().on('mouseleave', function () {
                    $this.popover('hide')
                })
            } else {
                $this.popover({
                    trigger: 'hover',
                    html: true,
                    container: isbody,
                    content: popoverhtml,
                    placement: placement
                })
                if ($this.hasClass('gx-avatar-hover-hover')) {
                    $this.popover('show')
                }
            }
        }
    })
}
$(document).on('mouseenter', '.gx-avatar-content img[data-toggle="tooltip"]', function () {
    var $this = $(this)
    $this.tooltip().tooltip('show')
})
// 名片添加好友
$(document).on('click', '.gx-avatar-content-friend', function () {
    var $this = $(this)
    let friendId = $this.attr('data-id')
    let source = 0
    $.star({
        type: 'POST',
        url: '/community-user/friend/v1/request',
        showerror: false,
        data: {
            friendId: friendId,
            source: source
        },
        success: function (res) {
            showtoast({
                message: '好友申请已发送'
            })
        },
        error: function (req) {
            if (req.errorCode == 260114) {
                gxmodal({
                    title: translatesrting('提示'),
                    centent: '<div class="mt25">' + translatesrting('好友数量超过上限') + '</div>',
                    buttons: [
                        {
                            text: translatesrting('确定'),
                            class: 'btn-middle btn-middle-blue',
                            callback: function () {

                            }
                        }
                    ]
                })
            } else {
                showtoast({
                    message: req.errorMsg
                })
            }
        }
    });
})
// 黑名单
$(document).on('click', '.gx-avatar-black-friend', function () {
    var $this = $(this)
    let friendId = $this.attr('data-id')
    let blcak = $this.attr('data-blcak')
    if (blcak == 'addblack') {
        $.star({
            type: 'POST',
            url: '/community-user/friend/user/v1/blacklist',
            showerror: true,
            data: {
                blackUserId: friendId,
            },
            success: function (res) {
                showtoast({
                    message: translatesrting('玩家成功添加黑名单')
                })
            },
            error: function (req) {

            }
        });
        return false
    }
    if (blcak == 'removeblack') {
        $.star({
            type: 'Delete',
            url: '/community-user/friend/user/v1/blacklist',
            showerror: true,
            data: {
                blackUserId: friendId,
            },
            success: function (res) {
                showtoast({
                    message: translatesrting('玩家成功移出黑名单')
                })
            },
            error: function (req) {

            }
        });
        return false
    }
})

// base64->file
function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(',');
    var mime = arr[0].match(/:(.*?);/)[1];
    var bstr = atob(arr[1]);
    var n = bstr.length;
    var u8arr = new Uint8Array(n);
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }
    //转换成file对象
    return new File([u8arr], filename, { type: mime });
    //转换成成blob对象
    //return new Blob([u8arr],{type:mime});
}
// 禁止缩放 / tab 退出
$(document).keydown(function (e) {
    if (e.which == 9 && window.location.href.indexOf('/Login/index') == -1) {
        e.preventDefault();
        e.keyCode = false;//加上这一句
    }
    if (e.ctrlKey && e.which == 189) {
        e.preventDefault();
        e.keyCode = false;//加上这一句
    }
    if (e.ctrlKey && e.which == 87) {
        e.preventDefault();
        e.keyCode = false;//加上这一句
    }
});
// 禁止缩放
$(document).keydown(function (e) {
    if (e.which == 122) {
        e.preventDefault();

        e.keyCode = false;//加上这一句
    }
});
var _hmt = _hmt || [];
(function () {
    var hm = document.createElement("script");
    hm.src = "https://hm.baidu.com/hm.js?18c2c7349f94b5804f9a5af0b3e8b259";
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(hm, s);
})();
(function () {
    var hm = document.createElement("script");
    hm.src = "https://o.alicdn.com/sls/sls-rum/sls-rum.js";
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(hm, s);
    setTimeout(() => {
        window.SLS_RUM &&
            SLS_RUM.init({
                host: 'cn-shanghai.log.aliyuncs.com',
                project: 'redwar-slsrum',
                logstore: 'redwar-slsrum-rum-raw',
                instance: 'redwar-slsrum',
                env: 'prod',
                service: 'web',
                enableError: true,
                enableResourcePerf: true,
                enableAjax: true,
                enablePerf: true,
                enableTrace: true,
                sampleRate: 0.1,
                uid: userInfo ? userInfo.userId : '',
                version: localStorage.getItem('Version') || "",
            })
    }, 1000)
})();

function commConfig() {
    return new Promise(function (resolve, reject) {
        $.star({
            type: 'GET',
            url: '/battlecenter/platform/config/v1/list',
            showerror: false,
            data: {},
            success: function (res) {
                resolve(res)
                // if (window.location.href.indexOf('Login') != -1) {
                //     if (res.data.backgroundImage.indexOf('.mp4') != -1) {
                //         $('.login-bg').attr('src', res.data.backgroundImage)
                //         $('.login-bg').removeClass('hide')
                //         $('.login-bg').on('play', function () {
                //             $('body').css({ 'transform': 'scale(1)', 'opacity': '1' })
                //         })
                //         setTimeout(() => {
                //             $('body').css({ 'transform': 'scale(1)', 'opacity': '1' })
                //             $('body').css({ 'backgroundColor': '#28375d' })
                //         }, 1000)
                //     } else {
                //         $('.login-content').css('backgroundImage', "url(" + res.data.backgroundImage + ")")
                //         $('body').css({ 'transform': 'scale(1)', 'opacity': '1' })
                //     }
                // }
            },
            error: function (req) {
                if (window.location.href.indexOf('Login') != -1) {
                    $('.login-content').css('background', "black")
                    $('body').css({ 'transform': 'scale(1)', 'opacity': '1' })
                }
            }
        });
    });

}
$(function () {
    commConfig().then(function (res) {
        if (res.success) {
            comOption.optionsAvatar = res.data.defaultAvatars
            comOption.optionscomplaint = res.data.reports
            comOption.roomConfig = res.data.roomConfig
            comOption.optionsLevels = res.data.levels
            saveMessage('matchTypes', res.data.matchTypes)
        }
    })
})
$(document).on('click', '.gx-search-clear', function () {
    $(this).closest('.gx-search').find('input').val("")
})

function wsUserStatus(type) {
    console.log(type)
    // 1:在线 2:不在线 3:天梯-寻找比赛 4:天梯模式 5:匹配-寻找比赛 6:匹配模式 7:自定义房间 8:自定义模式
    let sendtype = ''
    if (type == 1 || type == 7 || type == 3 || type == 5) {
        sendtype = 1
    }
    if (type == 4 || type == 8 || type == 6) {
        sendtype = 3
    }
    if (type == 7) {
        localStorage.setItem('isdetails', true);
    } else {
        localStorage.removeItem('isdetails')
    }
    saveMessage('wsUserStatus', type)
    if (localStorage.getItem('myStatus')) {
        let getmyStatus = localStorage.getItem('myStatus')
        if (getmyStatus == sendtype) {
            return false
        }
    }
    saveMessage('myStatus', sendtype)
    $.star({
        type: 'GET',
        url: '/community-user/friend/v1/point/user/status',
        showerror: false,
        data: {
            status: sendtype
        },
        success: function (res) {

        },
        error: function (req) {
        }
    });
    if (type == 7 || type == 4) {
        localStorage.setItem('isdetails', true);
    } else {
        localStorage.removeItem('isdetails')
    }
    saveMessage('wsUserStatus', type)
}

// 刷新页面
$(document).on('click', '.hall-header-refresh', function () {
    if (hallMessage && hallMessage.indexTo) {
        window.location.href = "/Hall/index?innav=" + hallMessage.indexTo
    } else {
        window.location.reload()
    }
})
var fengjinTime = null
function fengjinTimefun(type) {
    clearInterval(fengjinTime)
    fengjinTime = setInterval(function () {
        let time = Number($('.fengjinTime').attr('data-time'))
        time = time - 1
        if (time <= 0) {
            clearInterval(fengjinTime)
            $('#fengjinTime').modal('hide')
            return false
        }
        $('.fengjinTime').attr('data-time', time)
        $('.fengjinTime').text(formatMatchTime(time))
    }, 1000)
}
// 跳官网
// $(document).on('click', '.hall-header-logo', function () {
//     $.star({
//         type: 'POST',
//         url: '/community-user/authorize/generate/code',
//         showerror: true,
//         success: function (coderes) {
//             let tourl = 'https://ramboplay.com/'
//             OpenWebUrl(tourl + '?token=' + coderes.data.authCode)
//         },
//         error: function (req) {

//         }
//     });
// })
//---------------------皮肤------------------------------
// var skinMessage = {
//     id: null,
//     changeskin: null,
//     issnow: false,
//     snowfun: null,
//     snow: null
// }
// skinMessage.snowfun = function (type) {
//     if (!skinMessage.issnow) {
//         return false
//     }
//     if (typeof Snowflakes != 'function') {
//         /*! Snowflakes | © 2021 Denis Seleznev | MIT License | https://github.com/hcodes/snowflakes/ */
//         !function (t, e) {
//             "object" == typeof exports && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : (t = "undefined" != typeof globalThis ? globalThis : t || self).Snowflakes = e()
//         }(this, (function () {
//             "use strict";
//             var t = "";
//             function e(e, a) {
//                 Object.keys(a).forEach((function (n) {
//                     var i = n;
//                     t && n.search("animation") > -1 && (i = t + n[0].toUpperCase() + n.substr(1)),
//                         e.style[i] = a[n]
//                 }
//                 ))
//             }
//             function a(t) {
//                 t && t.parentNode && t.parentNode.removeChild(t)
//             }
//             function n(t, e) {
//                 t.classList.add(e)
//             }
//             function i(t, e) {
//                 t.classList.remove(e)
//             }
//             function l(t, e) {
//                 return t + Math.floor(Math.random() * (e - t))
//             }
//             function o(t, e, a, n, i) {
//                 return n + (i - n) * (t - e) / (a - e)
//             }
//             "undefined" != typeof window && (t = Array.prototype.slice.call(window.getComputedStyle(document.documentElement, "")).join(",").search(/,animation/) > -1 ? "" : "webkit");
//             function s(t, e, a) {
//                 return Math.floor(o(t, 0, 20, e, a))
//             }
//             var r = function () {
//                 function t(t) {
//                     var a = t.minSize === t.maxSize;
//                     this.innerSize = a ? 0 : l(0, 15),
//                         this.size = s(this.innerSize, t.minSize, t.maxSize);
//                     var i = document.createElement("div")
//                         , r = document.createElement("div")
//                         , h = this.getAnimationProps(t)
//                         , m = {
//                             animationName: "snowflake_gid_" + t.gid + "_y",
//                             animationDelay: h.animationDelay,
//                             animationDuration: h.animationDuration,
//                             left: 99 * Math.random() + "%",
//                             top: -Math.sqrt(2) * this.size + "px",
//                             width: this.size + "px",
//                             height: this.size + "px"
//                         };
//                     a || (m.opacity = String(o(this.size, t.minSize, t.maxSize, t.minOpacity, t.maxOpacity))),
//                         e(i, m),
//                         e(r, {
//                             animationName: "snowflake_gid_" + t.gid + "_x_" + this.innerSize,
//                             animationDelay: 4 * Math.random() + "s"
//                         }),
//                         n(i, "snowflake"),
//                         n(r, "snowflake__inner"),
//                         t.types && n(r, "snowflake__inner_type_" + l(0, t.types)),
//                         t.wind && n(r, "snowflake__inner_wind"),
//                         t.rotation && n(r, "snowflake__inner_rotation" + (Math.random() > .5 ? "" : "_reverse")),
//                         i.appendChild(r),
//                         this.elem = i
//                 }
//                 return t.prototype.resize = function (t) {
//                     var a = this.getAnimationProps(t);
//                     this.elem && e(this.elem, a)
//                 }
//                     ,
//                     t.prototype.appendTo = function (t) {
//                         this.elem && t.appendChild(this.elem)
//                     }
//                     ,
//                     t.prototype.destroy = function () {
//                         delete this.elem
//                     }
//                     ,
//                     t.prototype.getAnimationProps = function (t) {
//                         var e = t.containerHeight / 50 / t.speed
//                             , a = e / 3;
//                         return {
//                             animationDelay: Math.random() * e + "s",
//                             animationDuration: String(o(this.size, t.minSize, t.maxSize, e, a) + "s")
//                         }
//                     }
//                     ,
//                     t
//             }();
//             return function () {
//                 function t(e) {
//                     this.destroyed = !1,
//                         this.flakes = [],
//                         this.isBody = !1,
//                         this.params = this.setParams(e),
//                         this.isBody = this.params.container === document.body,
//                         t.gid++,
//                         this.gid = t.gid,
//                         this.container = this.appendContainer(),
//                         this.params.stop && this.stop(),
//                         this.appendStyles(),
//                         this.appendFlakes(),
//                         this.containerSize = {
//                             width: this.width(),
//                             height: this.height()
//                         },
//                         this.handleResize = this.handleResize.bind(this),
//                         window.addEventListener("resize", this.handleResize, !1)
//                 }
//                 return t.prototype.start = function () {
//                     i(this.container, "snowflakes_paused")
//                 }
//                     ,
//                     t.prototype.stop = function () {
//                         n(this.container, "snowflakes_paused")
//                     }
//                     ,
//                     t.prototype.show = function () {
//                         i(this.container, "snowflakes_hidden")
//                     }
//                     ,
//                     t.prototype.hide = function () {
//                         n(this.container, "snowflakes_hidden")
//                     }
//                     ,
//                     t.prototype.destroy = function () {
//                         this.destroyed || (this.destroyed = !0,
//                             t.instanceCounter && t.instanceCounter--,
//                             this.removeStyles(),
//                             a(this.container),
//                             this.flakes.forEach((function (t) {
//                                 return t.destroy()
//                             }
//                             )),
//                             this.flakes = [],
//                             window.removeEventListener("resize", this.handleResize, !1))
//                     }
//                     ,
//                     t.prototype.handleResize = function () {
//                         var t = this.width()
//                             , a = this.height();
//                         if (this.containerSize.width !== t || this.containerSize.height !== a) {
//                             this.containerSize.width = t,
//                                 this.containerSize.height = a;
//                             var n = this.getFlakeParams();
//                             e(this.container, {
//                                 display: "none"
//                             }),
//                                 this.flakes.forEach((function (t) {
//                                     return t.resize(n)
//                                 }
//                                 )),
//                                 this.updateAnimationStyle(),
//                                 function (t) {
//                                     e(t, {
//                                         display: "block"
//                                     })
//                                 }(this.container)
//                         }
//                     }
//                     ,
//                     t.prototype.appendContainer = function () {
//                         var t = document.createElement("div");
//                         return n(t, "snowflakes"),
//                             n(t, "snowflakes_gid_" + this.gid),
//                             this.isBody && n(t, "snowflakes_body"),
//                             e(t, {
//                                 zIndex: String(this.params.zIndex)
//                             }),
//                             this.params.container.appendChild(t),
//                             t
//                     }
//                     ,
//                     t.prototype.appendStyles = function () {
//                         t.instanceCounter || (this.mainStyleNode = this.injectStyle('.snowflake{-webkit-animation:snowflake_unknown 10s linear infinite;animation:snowflake_unknown 10s linear infinite;pointer-events:none;position:absolute;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;will-change:transform}.snowflake__inner,.snowflake__inner:before{bottom:0;left:0;position:absolute;right:0;top:0}.snowflake__inner:before{background-size:100% 100%;content:""}.snowflake__inner_wind{-webkit-animation:snowflake_unknown 2s ease-in-out infinite alternate;animation:snowflake_unknown 2s ease-in-out infinite alternate}.snowflake__inner_rotation:before{-webkit-animation:snowflake_rotation 10s linear infinite;animation:snowflake_rotation 10s linear infinite}.snowflake__inner_rotation_reverse:before{-webkit-animation:snowflake_rotation_reverse 10s linear infinite;animation:snowflake_rotation_reverse 10s linear infinite}@-webkit-keyframes snowflake_rotation{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}@keyframes snowflake_rotation{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}@-webkit-keyframes snowflake_rotation_reverse{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}to{-webkit-transform:rotate(-1turn);transform:rotate(-1turn)}}@keyframes snowflake_rotation_reverse{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}to{-webkit-transform:rotate(-1turn);transform:rotate(-1turn)}}.snowflakes{pointer-events:none}.snowflakes_paused .snowflake,.snowflakes_paused .snowflake__inner,.snowflakes_paused .snowflake__inner:before{-webkit-animation-play-state:paused;animation-play-state:paused}.snowflakes_hidden{visibility:hidden}.snowflakes_body{height:1px;left:0;position:fixed;top:0;width:100%}')),
//                             t.instanceCounter++,
//                             this.imagesStyleNode = this.injectStyle(".snowflakes_gid_value .snowflake__inner_type_0:before{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' width='36.283' height='36.283'%3E%3Cpath d='M35.531 17.391h-3.09l.845-1.464a.748.748 0 1 0-1.297-.75l-1.276 2.214H28.61l2.515-4.354a.751.751 0 0 0-.272-1.024.75.75 0 0 0-1.024.274l-2.948 5.104h-2.023a6.751 6.751 0 0 0-2.713-4.684l1.019-1.76 5.896-.002a.75.75 0 0 0 0-1.5l-5.029.002 1.051-1.82 2.557.002a.75.75 0 0 0 0-1.5l-1.689-.002 1.545-2.676a.75.75 0 1 0-1.302-.75l-1.547 2.676-.844-1.463a.749.749 0 1 0-1.297.75l1.278 2.213-1.051 1.818-2.514-4.354a.75.75 0 0 0-1.298.75l2.946 5.104-1.016 1.758a6.692 6.692 0 0 0-2.706-.57 6.74 6.74 0 0 0-2.707.568l-1.013-1.754 2.946-5.105a.75.75 0 0 0-1.298-.75L13.56 8.697l-1.05-1.818 1.278-2.217a.749.749 0 0 0-1.298-.75l-.845 1.465-1.551-2.678a.75.75 0 0 0-1.024-.273.748.748 0 0 0-.274 1.023l1.545 2.678H8.652a.75.75 0 0 0 0 1.5h2.556l1.05 1.818H7.231a.75.75 0 0 0 0 1.5h5.894l1.017 1.762a6.755 6.755 0 0 0-2.712 4.684H9.406l-2.95-5.104a.75.75 0 1 0-1.299.75l2.516 4.354H5.569l-1.277-2.213a.75.75 0 0 0-1.298.75l.845 1.463H.75a.75.75 0 0 0 0 1.5h3.09l-.845 1.465a.747.747 0 0 0 .275 1.022.75.75 0 0 0 .374.103.75.75 0 0 0 .65-.375l1.277-2.215h2.103l-2.516 4.354a.75.75 0 0 0 1.299.75l2.949-5.104h2.024a6.761 6.761 0 0 0 2.712 4.685l-1.017 1.762H7.232a.75.75 0 0 0 0 1.5h5.026l-1.05 1.818H8.651a.75.75 0 0 0 0 1.5h1.69l-1.545 2.676a.75.75 0 0 0 1.299.75l1.546-2.676.846 1.465a.755.755 0 0 0 .65.375.737.737 0 0 0 .375-.103.747.747 0 0 0 .274-1.022l-1.279-2.215 1.05-1.82 2.515 4.354a.75.75 0 0 0 1.299-.75l-2.947-5.104 1.013-1.756a6.72 6.72 0 0 0 5.415 0l1.014 1.756-2.947 5.104a.75.75 0 0 0 1.298.75l2.515-4.354 1.053 1.82-1.277 2.213a.75.75 0 0 0 1.298.75l.844-1.463 1.545 2.678c.141.24.393.375.65.375a.75.75 0 0 0 .649-1.125l-1.548-2.678h1.689a.75.75 0 0 0 0-1.5h-2.557l-1.051-1.82 5.029.002a.75.75 0 0 0 0-1.5l-5.896-.002-1.019-1.76a6.75 6.75 0 0 0 2.711-4.685h2.023l2.947 5.104a.753.753 0 0 0 1.025.273.749.749 0 0 0 .272-1.023l-2.515-4.354h2.104l1.279 2.215a.75.75 0 0 0 .649.375c.127 0 .256-.03.375-.103a.748.748 0 0 0 .273-1.022l-.848-1.465h3.092a.75.75 0 0 0 .003-1.5zm-12.136.75c0 .257-.041.502-.076.75a5.223 5.223 0 0 1-1.943 3.358 5.242 5.242 0 0 1-1.291.766 5.224 5.224 0 0 1-1.949.384 5.157 5.157 0 0 1-3.239-1.15 5.22 5.22 0 0 1-1.943-3.358c-.036-.247-.076-.493-.076-.75s.04-.503.076-.75a5.22 5.22 0 0 1 1.944-3.359c.393-.312.82-.576 1.291-.765a5.219 5.219 0 0 1 1.948-.384c.69 0 1.344.142 1.948.384.471.188.898.454 1.291.765a5.222 5.222 0 0 1 1.943 3.359c.035.247.076.493.076.75z' fill=':color:'/%3E%3C/svg%3E\")}.snowflakes_gid_value .snowflake__inner_type_1:before{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' width='32.813' height='32.813'%3E%3Cpath d='M29.106 24.424a.781.781 0 0 1-.781.781h-3.119v3.119a.782.782 0 0 1-1.562 0v-4.682h4.682c.43.001.78.351.78.782zM4.673 9.352h4.682V4.671a.781.781 0 0 0-1.563 0V7.79H4.673a.781.781 0 0 0 0 1.562zM3.708 24.24c0 .431.35.781.781.781H7.61v3.12a.78.78 0 1 0 1.562 0v-4.683H4.489a.782.782 0 0 0-.781.782zM28.923 8.39a.78.78 0 0 0-.781-.781h-3.121V4.488a.781.781 0 0 0-1.562 0v4.684h4.684a.783.783 0 0 0 .78-.782zm3.889 8.017c0 .431-.35.781-.781.781h-3.426l1.876 1.873a.784.784 0 0 1 0 1.107.791.791 0 0 1-.554.228.773.773 0 0 1-.55-.228l-2.979-2.98h-2.995a6.995 6.995 0 0 1-1.728 3.875h5.609a.781.781 0 0 1 0 1.562h-4.666v4.667a.782.782 0 0 1-1.562 0v-5.61a7 7 0 0 1-3.866 1.719v2.995l2.978 2.98c.306.305.306.8 0 1.104a.78.78 0 0 1-1.104 0l-1.874-1.876v3.427a.781.781 0 0 1-1.562 0v-3.427l-1.875 1.876a.78.78 0 1 1-1.105-1.104l2.979-2.98v-2.995a7.016 7.016 0 0 1-3.865-1.717v5.608a.781.781 0 0 1-1.562 0v-4.667H5.535a.781.781 0 0 1 0-1.562h5.607a7.022 7.022 0 0 1-1.728-3.875H6.417l-2.979 2.979a.784.784 0 0 1-1.104 0 .781.781 0 0 1 0-1.106l1.874-1.873H.782a.78.78 0 1 1-.001-1.563h3.426L2.333 13.75a.783.783 0 0 1 1.105-1.106l2.979 2.979h2.995a6.996 6.996 0 0 1 1.72-3.866H5.533a.781.781 0 0 1 0-1.562h4.666V5.528a.781.781 0 0 1 1.562 0v5.599a6.995 6.995 0 0 1 3.865-1.717V6.415l-2.978-2.979a.782.782 0 0 1 1.105-1.105l1.874 1.875V.781a.78.78 0 1 1 1.562 0v3.426l1.875-1.875a.777.777 0 0 1 1.104 0 .78.78 0 0 1 0 1.105l-2.978 2.98v2.996a7.021 7.021 0 0 1 3.866 1.718V5.532a.78.78 0 1 1 1.562 0v4.666h4.666a.78.78 0 1 1 0 1.562h-5.599a7 7 0 0 1 1.718 3.866h2.995l2.979-2.979a.783.783 0 0 1 1.106 1.106l-1.876 1.874h3.427a.777.777 0 0 1 .778.78zm-11.006-.782a5.457 5.457 0 0 0-4.618-4.617c-.257-.037-.514-.079-.781-.079-.268 0-.524.042-.781.079a5.458 5.458 0 0 0-4.618 4.617c-.038.257-.079.514-.079.781s.041.522.079.781a5.455 5.455 0 0 0 4.618 4.616c.257.036.514.079.781.079s.524-.043.781-.079a5.457 5.457 0 0 0 4.618-4.616c.037-.259.079-.515.079-.781s-.043-.524-.079-.781z' fill=':color:'/%3E%3C/svg%3E\")}.snowflakes_gid_value .snowflake__inner_type_2:before{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' width='35.79' height='35.79'%3E%3Cpath d='M7.161 22.223l.026-.047.865.5-.026.047a.503.503 0 0 1-.434.25c-.019 0-.034-.013-.053-.016l-.355-.205a.493.493 0 0 1-.023-.529zM9.969 8.988l2.785.001 1.393-2.414a.502.502 0 0 0-.869-.499l-1.103 1.913-2.208-.001a.5.5 0 1 0 .002 1zm15.854 17.813h-2.785l-1.393 2.411a.499.499 0 0 0 .436.75c.172 0 .34-.09.434-.25l1.104-1.911h2.207c.274 0 .5-.224.5-.5a.505.505 0 0 0-.503-.5zM23.038 8.99h2.785a.5.5 0 0 0 0-1h-2.207l-1.105-1.913a.5.5 0 0 0-.868.5l1.395 2.413zM12.754 26.801H9.967a.5.5 0 0 0 0 1h2.209l1.105 1.912a.496.496 0 0 0 .682.184.5.5 0 0 0 .184-.684l-1.393-2.412zm-7.218-6.309a.502.502 0 0 0 .685-.184l1.391-2.413-1.394-2.413a.5.5 0 0 0-.867.5l1.104 1.913-1.104 1.913a.5.5 0 0 0 .185.684zM30.254 15.3a.505.505 0 0 0-.685.183l-1.392 2.412 1.395 2.414a.501.501 0 0 0 .867-.5l-1.104-1.914 1.104-1.912a.5.5 0 0 0-.185-.683zm3.138 11.542a.501.501 0 0 1-.683.184l-.98-.565-2.137 1.231a.516.516 0 0 1-.5 0l-2.385-1.377a.502.502 0 0 1-.25-.433v-.854h-4.441l-2.225 3.852.736.428c.154.088.25.254.25.432l.001 2.755a.5.5 0 0 1-.25.433l-2.133 1.229v1.136c0 .274-.225.5-.5.5s-.5-.226-.5-.5v-1.136l-2.136-1.23a.5.5 0 0 1-.25-.433l.001-2.755c0-.178.096-.344.25-.432l.738-.427-2.224-3.849H9.332l.002.851a.505.505 0 0 1-.25.435l-2.387 1.377a.5.5 0 0 1-.5 0L4.06 26.46l-.982.567a.5.5 0 0 1-.5-.867l.982-.567.001-2.465c0-.179.097-.344.25-.434l2.388-1.377a.497.497 0 0 1 .5 0l.736.426 2.221-3.848-2.222-3.849-.737.426a.51.51 0 0 1-.5 0l-2.386-1.377a.5.5 0 0 1-.25-.434l.002-2.464-.983-.567a.501.501 0 0 1-.184-.683.502.502 0 0 1 .684-.183l.983.568 2.134-1.233a.5.5 0 0 1 .5 0l2.385 1.379c.156.089.25.255.25.433v.85h4.443l2.223-3.846-.74-.427a.501.501 0 0 1-.25-.434l.002-2.755c0-.178.096-.343.25-.433l2.135-1.233V.5a.5.5 0 0 1 1 0v1.135l2.134 1.231c.154.089.25.254.25.434l-.002 2.755a.503.503 0 0 1-.25.433l-.733.425 2.224 3.849h4.44l-.002-.851c0-.179.096-.344.25-.434l2.388-1.378a.502.502 0 0 1 .5 0l2.136 1.233.982-.568a.5.5 0 1 1 .5.866l-.983.568v2.464a.503.503 0 0 1-.25.433l-2.388 1.378a.5.5 0 0 1-.5 0l-.735-.426-2.222 3.849 2.223 3.849.734-.425a.506.506 0 0 1 .5 0l2.389 1.375c.154.09.25.255.25.435l-.002 2.462.982.568c.24.137.321.444.182.682zm-2.165-1.828l.001-1.597-1.888-1.087-.734.424-.348.201-.301.173-.5.289v2.179l1.885 1.088 1.386-.802.498-.286.001-.582zm-3.736-11.467l-.531-.307-2.283 1.318-2.443 3.337 2.442 3.337 2.283 1.316.531-.306-2.514-4.348 2.515-4.347zm-7.712 16.478l-.762-.438-.339-.194-.283-.166-.5-.289-.5.289-.279.162-.349.2-.757.437-.001 2.177 1.386.797.501.289.499-.287 1.386-.798-.002-2.179zM16.008 5.767l.736.425.371.214.279.16.5.288.5-.289.281-.163.367-.212.732-.424.002-2.178-1.381-.797-.502-.289-.498.287-1.385.8-.002 2.178zm6.52 14.227l-1.535-2.099 1.535-2.098.732-1-1.232.134-2.585.281-1.048-2.379-.5-1.133-.5 1.134-1.049 2.379-2.585-.281-1.232-.134.732 1 1.536 2.097-1.536 2.098-.732 1 1.232-.134 2.585-.281 1.049 2.379.5 1.134.5-1.134 1.048-2.379 2.585.281 1.232.134-.732-.999zm8.2-10.084l-1.386-.8-1.887 1.089v1.279l.002.32v.577l.5.289.28.163.367.213.732.424 1.888-1.089v-2.178l-.496-.287zM18.927 7.413l-.532.307v2.637l1.667 3.784 4.111-.447 2.283-1.317-.002-.613h-5.02l-2.507-4.351zm-9.594 4.348v.614l2.283 1.318 4.111.447 1.668-3.785V7.719l-.531-.306-2.509 4.347-5.022.001zm-2.15 1.279l.37-.213.279-.162.5-.289V10.2L6.446 9.11l-1.384.8-.499.289v.578l-.002 1.599 1.885 1.088.737-.424zm1.119 9.205l.53.306 2.281-1.316 2.443-3.339-2.442-3.337-2.281-1.317-.531.307 2.511 4.348-2.511 4.348zm-1.115-.069l-.026.047a.493.493 0 0 0 .023.529l-.734-.424-1.887 1.089-.001 1.599v.578l.5.288 1.386.8 1.887-1.088v-1.278l-.002-.321v-.577l-.5-.289-.293-.169c.02.002.035.017.055.017a.5.5 0 0 0 .433-.25l.026-.047-.867-.504zm9.679 6.202l.529-.306v-2.637l-1.668-3.785-4.111.447-2.283 1.316.002.611 5.021.002 2.51 4.352zm9.591-4.349v-.612L24.174 22.1l-4.111-.447-1.667 3.783v2.639l.531.307 2.512-4.352h5.018v-.001z' fill=':color:'/%3E%3C/svg%3E\")}.snowflakes_gid_value .snowflake__inner_type_3:before{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' width='32.815' height='32.815'%3E%3Cpath d='M4.581 23.55h4.681v4.681a.78.78 0 1 1-1.562 0v-3.118H4.581a.781.781 0 0 1 0-1.563zM29.016 8.481a.781.781 0 0 0-.781-.781h-3.119V4.582a.781.781 0 0 0-1.562 0v4.681h4.682c.429 0 .78-.35.78-.782zm-24.252.598l4.683-.001V4.395a.781.781 0 0 0-1.562 0v3.121l-3.121.001a.781.781 0 0 0 0 1.562zm23.655 14.287h-4.685l.002 4.684a.78.78 0 1 0 1.562 0l-.002-3.121h3.122a.781.781 0 0 0 .001-1.563zm4.394-6.96a.78.78 0 0 1-.781.781h-3.426l1.876 1.875a.782.782 0 0 1-1.104 1.105l-2.979-2.979h-1.986L17.19 24.41v1.987l2.977 2.979a.781.781 0 0 1-1.103 1.106l-1.874-1.875v3.426a.78.78 0 1 1-1.562 0v-3.426l-1.875 1.875a.782.782 0 0 1-1.105-1.105l2.978-2.979V24.41l-7.219-7.22H6.418l-2.98 2.98a.777.777 0 0 1-1.103 0 .781.781 0 0 1 0-1.106L4.21 17.19H.783a.78.78 0 1 1 0-1.562h3.426l-1.876-1.875a.782.782 0 1 1 1.106-1.105l2.979 2.979h1.989l7.219-7.218v-1.99L12.648 3.44a.782.782 0 1 1 1.106-1.105l1.874 1.874V.781a.782.782 0 0 1 1.563 0v3.426l1.875-1.875a.783.783 0 0 1 1.106 1.105l-2.979 2.979v1.99l7.216 7.218h1.992l2.979-2.979a.782.782 0 0 1 1.105 1.105l-1.876 1.874h3.427a.781.781 0 0 1 .777.782zm-10.613.782l.778-.78-.781-.782-5.009-5.008-.781-.781-.781.781-5.01 5.008-.781.781.781.781 5.01 5.011.782.781.78-.779 5.012-5.013zm5.863 4.646a.782.782 0 0 0-.781-.781h-6.229v6.228a.78.78 0 1 0 1.562 0v-4.665h4.666a.782.782 0 0 0 .782-.782zm-.001-10.855a.782.782 0 0 0-.781-.781h-4.664V5.532a.782.782 0 0 0-1.562 0v6.228h6.227a.78.78 0 0 0 .78-.781zm-23.318 0c0 .432.35.781.781.781h6.228V5.532a.781.781 0 0 0-1.562 0v4.666H5.525a.781.781 0 0 0-.781.781zm.002 10.855c0 .432.35.781.781.781h4.664v4.665a.78.78 0 1 0 1.562 0v-6.228H5.527a.783.783 0 0 0-.781.782z' fill=':color:'/%3E%3C/svg%3E\")}.snowflakes_gid_value .snowflake__inner_type_4:before{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' width='37.794' height='37.794'%3E%3Cpath d='M30.638 17.313l-.914 1.584.915 1.585a.78.78 0 1 1-1.352.78l-1.366-2.366 1.366-2.365a.782.782 0 0 1 1.067-.286c.372.215.5.692.284 1.068zM11.65 11.08l2.733.002 1.367-2.367a.78.78 0 0 0-1.352-.781l-.915 1.585-1.831-.002h-.001a.78.78 0 0 0-.001 1.563zm14.491 15.633h-2.733l-1.365 2.365a.78.78 0 1 0 1.352.78l.914-1.584h1.831a.781.781 0 0 0 .001-1.561zm-4.1-17.998l1.367 2.367h2.733a.78.78 0 1 0 0-1.562h-1.833l-.915-1.585a.78.78 0 0 0-1.352.78zM15.75 29.08l-1.368-2.366h-2.733a.781.781 0 0 0 0 1.562h1.832l.917 1.585c.146.25.409.391.677.391a.779.779 0 0 0 .675-1.172zm-8.313-7.531a.78.78 0 0 0 1.067-.284L9.87 18.9l-1.367-2.368a.781.781 0 0 0-1.351.781l.916 1.587-.914 1.584a.776.776 0 0 0 .283 1.065zm27.827 6.798a.784.784 0 0 1-1.067.285l-.89-.515-2.096 1.209a.793.793 0 0 1-.391.105.762.762 0 0 1-.391-.105l-2.484-1.435a.78.78 0 0 1-.391-.676l-.002-2.417-2.408-1.392a7.714 7.714 0 0 1-5.467 3.168v2.773l2.093 1.208a.78.78 0 0 1 .391.676l.001 2.868c0 .28-.149.537-.392.676l-2.093 1.205v1.032a.781.781 0 0 1-1.562 0V35.98l-2.095-1.207a.78.78 0 0 1-.391-.676l.001-2.868c0-.28.15-.537.391-.676l2.094-1.206v-2.773a7.718 7.718 0 0 1-5.468-3.168l-2.408 1.392.002 2.415c0 .281-.15.539-.391.676l-2.487 1.437a.785.785 0 0 1-.782 0l-2.095-1.209-.893.518a.782.782 0 0 1-.782-1.354l.893-.517.001-2.414a.78.78 0 0 1 .391-.677l2.487-1.434a.774.774 0 0 1 .781 0l2.093 1.208 2.407-1.39a7.655 7.655 0 0 1 0-6.317l-2.406-1.39-2.096 1.209a.772.772 0 0 1-.782 0l-2.485-1.434a.786.786 0 0 1-.391-.676l.002-2.416-.894-.517a.78.78 0 0 1-.285-1.066.788.788 0 0 1 1.07-.283l.893.514 2.093-1.208a.774.774 0 0 1 .781 0L9.851 9.91c.24.14.391.398.391.675L10.24 13l2.408 1.392a7.712 7.712 0 0 1 5.468-3.167V8.45L16.02 7.242a.78.78 0 0 1-.391-.676l.002-2.87c0-.279.15-.538.391-.675l2.094-1.208V.781a.781.781 0 0 1 1.562 0v1.032l2.093 1.206a.785.785 0 0 1 .391.677l-.002 2.87c0 .28-.149.536-.391.674l-2.091 1.208v2.772a7.708 7.708 0 0 1 5.467 3.167l2.409-1.392-.002-2.416c0-.28.149-.539.391-.676l2.487-1.436c.24-.14.539-.14.781 0l2.095 1.208.894-.514a.78.78 0 1 1 .781 1.352l-.894.516v2.417c0 .279-.15.538-.391.675l-2.487 1.436a.785.785 0 0 1-.782 0l-2.092-1.209-2.408 1.39c.436.967.684 2.032.684 3.158a7.65 7.65 0 0 1-.684 3.158l2.408 1.391 2.091-1.206a.782.782 0 0 1 .78 0l2.488 1.432c.24.141.392.398.392.677l-.002 2.414.893.517a.783.783 0 0 1 .287 1.068zm-6.147-16.251l.001.9.78.453.921.531 1.706-.982v-1.965l-.78-.451-.923-.533-1.707.983.002 1.064zm-20.443-.002l.002-1.063-1.706-.985-.922.535-.778.451-.001.902-.001 1.063 1.703.982.924-.533.779-.451v-.901zm0 13.604l-.001-.899-.781-.451-.919-.533-1.706.982-.001 1.064v.901l.781.451.923.533 1.707-.982-.003-1.066zm15.109-3.076c.315-.413.586-.864.789-1.351a6.121 6.121 0 0 0 0-4.748 6.175 6.175 0 0 0-.789-1.35 6.158 6.158 0 0 0-4.106-2.375 6.48 6.48 0 0 0-.781-.056c-.266 0-.525.022-.781.056a6.149 6.149 0 0 0-4.106 2.375 6.128 6.128 0 0 0-.789 1.35 6.104 6.104 0 0 0-.479 2.374 6.1 6.1 0 0 0 1.268 3.725 6.15 6.15 0 0 0 4.106 2.374c.256.031.516.056.781.056s.525-.022.781-.056a6.142 6.142 0 0 0 4.106-2.374zM17.19 6.113l.924.531.781.452.781-.452.919-.531.002-1.968-.921-.531-.784-.452-.779.451-.922.532-.001 1.968zm3.408 25.57l-.921-.532-.781-.452-.781.452-.922.532-.001 1.966.923.531.782.451.78-.449.922-.533-.001-1.966zm11.925-5.819l.001-1.063-1.707-.981-.919.529-.782.451v.901l.001 1.065 1.702.981.924-.533.778-.449.002-.901z' fill=':color:'/%3E%3C/svg%3E\")}.snowflakes_gid_value .snowflake__inner_type_5:before{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' width='31.25' height='31.25'%3E%3Cpath d='M20.581 1.176l-3.914 3.915V0h1.041v2.576L19.845.439l.736.737zm-1.615 9.069l.351.217 6.623-6.625-.736-.737-6.048 6.051a7.141 7.141 0 0 0-1.449-.6v-.082l5.082-5.082-.737-.737-5.387 5.388v1.33l.402.093a6.213 6.213 0 0 1 1.899.784zm2.041 2.043c.368.585.63 1.224.786 1.893l.094.403h1.028l5.171-5.173-.736-.737-4.699 4.701a7.523 7.523 0 0 0-.549-1.28l6.048-6.05-.737-.735-6.622 6.625.216.353zm7.368 1.254l1.921-1.923-.736-.735-3.699 3.7h5.39v-1.042h-2.876zm1.185 6.826l.736-.736-1.923-1.923h2.877v-1.042h-5.389l3.699 3.701zm-6.915-2.498l4.705 4.707.736-.736-5.171-5.174h-1.03l-.096.4a6.24 6.24 0 0 1-.795 1.883l-.22.353 6.639 6.641.736-.736-6.061-6.062c.227-.414.414-.84.557-1.276zm-3.7 3.125a6.241 6.241 0 0 1-1.88.794l-.399.096v1.33l5.387 5.387.736-.736-5.082-5.082v-.089a7.322 7.322 0 0 0 1.434-.605l6.061 6.062.736-.736-6.641-6.641-.352.22zM16.667 31.25h1.041v-2.576l2.137 2.137.736-.737-3.914-3.914v5.09zm-5.26-.439l2.134-2.137v2.576h1.042v-5.093l-3.913 3.916.737.738zm.897-9.816l-.352-.222-6.642 6.641.736.736 6.062-6.062c.456.254.937.456 1.433.605v.089l-5.08 5.082.736.736 5.387-5.387v-1.33l-.4-.096a6.175 6.175 0 0 1-1.88-.792zm-2.046-2.047a6.315 6.315 0 0 1-.798-1.883l-.096-.4H8.335l-5.172 5.174.737.736 4.706-4.71c.145.441.329.865.556 1.276L3.1 25.202l.736.736 6.643-6.643-.221-.347zM0 16.667v1.042h2.876L.954 19.632l.736.736 3.698-3.701H0zm1.69-5.783l-.736.735 1.921 1.923H0v1.042h5.39l-3.7-3.7zm6.916 2.498L3.9 8.674l-.736.737 5.172 5.173h1.029l.096-.4a6.15 6.15 0 0 1 .798-1.881l.222-.352L3.837 5.31l-.736.736 6.062 6.06a7.268 7.268 0 0 0-.557 1.276zm-.145-9.996l5.08 5.082v.088c-.497.15-.977.352-1.433.606L6.047 3.101l-.736.737 6.643 6.643.352-.222a6.223 6.223 0 0 1 1.88-.797l.4-.095v-1.33L9.2 2.649l-.739.737zm5.081-.81L11.408.439l-.736.737 3.913 3.917V0h-1.042v2.576zm-1.757 14.831a4.2 4.2 0 0 0 2.06 2.058l.739.338v-3.136h-3.138l.339.74zm0-3.562l-.337.738h3.135v-3.136l-.739.338a4.223 4.223 0 0 0-2.059 2.06zm7.679 3.561l.338-.739h-3.135v3.136l.738-.338a4.204 4.204 0 0 0 2.059-2.059zm0-3.561a4.198 4.198 0 0 0-2.059-2.06l-.738-.34v3.138h3.135l-.338-.738z' fill=':color:'/%3E%3C/svg%3E\")}".replace(/:color:/g, encodeURIComponent(this.params.color))),
//                             this.animationStyleNode = this.injectStyle(this.getAnimationStyle())
//                     }
//                     ,
//                     t.prototype.injectStyle = function (t, e) {
//                         return function (t, e) {
//                             return e || (e = document.createElement("style"),
//                                 document.body.appendChild(e)),
//                                 e.textContent = t,
//                                 e
//                         }(t.replace(/_gid_value/g, "_gid_" + this.gid), e)
//                     }
//                     ,
//                     t.prototype.getFlakeParams = function () {
//                         var t = this.height()
//                             , e = this.params;
//                         return {
//                             containerHeight: t,
//                             gid: this.gid,
//                             count: e.count,
//                             speed: e.speed,
//                             rotation: e.rotation,
//                             minOpacity: e.minOpacity,
//                             maxOpacity: e.maxOpacity,
//                             minSize: e.minSize,
//                             maxSize: e.maxSize,
//                             types: e.types,
//                             wind: e.wind
//                         }
//                     }
//                     ,
//                     t.prototype.appendFlakes = function () {
//                         var t = this
//                             , e = this.getFlakeParams();
//                         this.flakes = [];
//                         for (var a = 0; a < this.params.count; a++)
//                             this.flakes.push(new r(e));
//                         this.flakes.sort((function (t, e) {
//                             return t.size - e.size
//                         }
//                         )).forEach((function (e) {
//                             e.appendTo(t.container)
//                         }
//                         ))
//                     }
//                     ,
//                     t.prototype.setParams = function (t) {
//                         var e = t || {}
//                             , a = {}
//                             , n = {
//                                 color: "#5ECDEF",
//                                 container: document.body,
//                                 count: 50,
//                                 speed: 1,
//                                 stop: !1,
//                                 rotation: !0,
//                                 minOpacity: .6,
//                                 maxOpacity: 1,
//                                 minSize: 10,
//                                 maxSize: 25,
//                                 types: 6,
//                                 width: void 0,
//                                 height: void 0,
//                                 wind: !0,
//                                 zIndex: 9999
//                             };
//                         return Object.keys(n).forEach((function (t) {
//                             a[t] = void 0 === e[t] ? n[t] : e[t]
//                         }
//                         )),
//                             a
//                     }
//                     ,
//                     t.prototype.getAnimationStyle = function () {
//                         for (var t = this.height() + this.params.maxSize * Math.sqrt(2) + "px", e = this.gid, a = "@-webkit-keyframes snowflake_gid_" + e + "_y{from{-webkit-transform:translateY(0px)}to{-webkit-transform:translateY(" + t + ");}}\n@keyframes snowflake_gid_" + e + "_y{from{transform:translateY(0px)}to{transform:translateY(" + t + ")}}", n = 0; n <= 20; n++) {
//                             var i = s(n, this.params.minSize, this.params.maxSize) + "px";
//                             a += "@-webkit-keyframes snowflake_gid_" + e + "_x_" + n + "{from{-webkit-transform:translateX(0px)}to{-webkit-transform:translateX(" + i + ");}}\n@keyframes snowflake_gid_" + e + "_x_" + n + "{from{transform:translateX(0px)}to{transform:translateX(" + i + ")}}"
//                         }
//                         return a
//                     }
//                     ,
//                     t.prototype.updateAnimationStyle = function () {
//                         this.injectStyle(this.getAnimationStyle(), this.animationStyleNode)
//                     }
//                     ,
//                     t.prototype.removeStyles = function () {
//                         t.instanceCounter || (a(this.mainStyleNode),
//                             delete this.mainStyleNode),
//                             a(this.imagesStyleNode),
//                             delete this.imagesStyleNode,
//                             a(this.animationStyleNode),
//                             delete this.animationStyleNode
//                     }
//                     ,
//                     t.prototype.width = function () {
//                         return this.params.width || (this.isBody ? (e = document.body,
//                             a = document.documentElement,
//                             window.innerWidth ? t = window.innerWidth : a && a.clientWidth ? t = a.clientWidth : e && (t = e.clientWidth),
//                             t || 0) : this.params.container.offsetWidth);
//                         var t, e, a
//                     }
//                     ,
//                     t.prototype.height = function () {
//                         return this.params.height || (this.isBody ? (e = document.body,
//                             a = document.documentElement,
//                             window.innerHeight ? t = window.innerHeight : a && a.clientHeight ? t = a.clientHeight : e && (t = e.clientHeight),
//                             t || 0) : this.params.container.offsetHeight + this.params.maxSize);
//                         var t, e, a
//                     }
//                     ,
//                     t.instanceCounter = 0,
//                     t.gid = 0,
//                     t
//             }()
//         }
//         ));
//     }
//     if (type == 'clear' && skinMessage.snow) {
//         skinMessage.snow.destroy()
//         skinMessage.snow = null
//     } else {
//         if ($('#snow').length == 0) {
//             let addhsnow = '<div id="snow"></div>'
//             $("main").append(addhsnow);
//         }
//         skinMessage.snow = new Snowflakes({
//             color: "#ffffff",
//             count: 75,
//             minOpacity: 0.2,
//             maxOpacity: 0.6,
//             container: $('#snow')[0],
//             width: 1280,
//             height: 660
//         });
//     }
// }
// skinMessage.changeskin = function (skinname) {
//     if (window.location.href.indexOf('/Login/index') != -1) {
//         return false
//     }
//     // if (skinname == 'normal') {
//     if (true) {
//         $('.skincss').remove()
//         skinMessage.snowfun('clear')
//         $('body').css('opacity', 1)
//         return false
//     }
//     if (
//         window.location.href.indexOf('/Hall/index') == -1 &&
//         window.location.href.indexOf('/Modal/setting') == -1 &&
//         window.location.href.indexOf('/Details/index') == -1 &&
//         window.location.href.indexOf('/Modal/createRoom') == -1 &&
//         window.location.href.indexOf('/Details/match') == -1
//     ) {
//         $('body').css('opacity', 1)
//         return false
//     }
//     let addhtmllink = "<link class='skincss' rel='stylesheet' href='" + "/css/skin/" + skinname + ".css" + "' />"
//     $("head").append(addhtmllink);
//     // 下雪特效
//     if (window.location.href.indexOf('/Hall/index') == -1 || !skinMessage.issnow) {
//         return false
//     }
//     skinMessage.snowfun()
// }
$(function () {
    if (window.location.href.indexOf('/Login/index') == -1) {
        $('body').css('opacity', 1)
    }
    // $('body').css('opacity', 1)
    return false
    if (getSaveMessage('LBWskin')) {
        skinMessage.id = getSaveMessage('LBWskin').id
    } else {
        $('.J_skinModal').closest('.gx-menu-li').find('.red-bot').removeClass('hide')
        $('.hall-header-setting-select .hall-header-fun-item_icon').find('.red-bot').removeClass('hide')
    }
    if (skinMessage.id == 2 || skinMessage.id == null) {
        skinMessage.changeskin('Springfestival')
    } else {
        $('body').css('opacity', 1)
    }
})
// $(document).on('click', '.J_skinModal', function () {
//     $('#skinModal .skinModal-content').html(template('skinModal-script', { id: skinMessage.id }))
//     $('#skinModal').modal('show')
//     $('.hall-header-setting-select').find('.red-bot').addClass('hide')
// })
// $(document).on('click', '.skinModal-content-img', function () {
//     var $this = $(this)
//     var id = $this.attr('data-id')
//     skinMessage.id = id
//     $('#skinModal .row .col-xs-6>div').removeClass('active')
//     $this.closest('div').addClass('active')
//     saveMessage('LBWskin', { id: id })
//     if (id == 1) {
//         skinMessage.changeskin('normal')
//     } else if (id == 2) {
//         skinMessage.changeskin('Springfestival')
//     }
// })

//---------------------皮肤end------------------------------
//---------------------声音------------------------------

function player_sound_click() {
    let player_click = document.getElementById('Audio_click')
    if (player_click) {
        player_click.play()
    }
}
$(document).on('click', '.J_sound_click', function () {
    let player_click = document.getElementById('Audio_click')
    if (player_click) {
        player_click.play()
    }
})

function player_sound_outroom() {
    let player_click = document.getElementById('Audio_outroom')
    if (player_click) {
        player_click.play()
    }
}
function player_sound_inrank() {
    let player_click = document.getElementById('Audio_inrank')
    if (player_click) {
        player_click.play()
    }
}
function player_sound_sendchat() {
    let player_click = document.getElementById('Audio_sendchat')
    if (player_click) {
        player_click.play()
    }
}
function player_sound_ready() {
    let player_click = document.getElementById('Audio_ready')
    if (player_click) {
        player_click.play()
    }
}
function player_sound_openfail() {
    let player_click = document.getElementById('Audio_openfail')
    if (player_click) {
        player_click.play()
    }
}
function player_sound_autoopen(type) {
    let player_click = document.getElementById('Audio_autoopen')
    if (player_click) {
        if (type == 'pause') {
            player_click.pause()
        } else {
            player_click.currentTime = 0
            player_click.play()
        }
    }
}
function volumeFn() {
    let temp_json = getSaveMessage('ClientVolume')
    let player_global_volume = temp_json['globalVolume'].volume
    let player_global_muted = temp_json['globalVolume'].mute

    var player_bgm = document.getElementById('Audio_BGM')
    var player_muted = [
        'Audio_click',
        'Audio_inroom',
        'Audio_outroom',
        'Audio_inrank',
        'Audio_sendchat',
        'Audio_ready',
        'Audio_openfail',
        'Audio_autoopen',
    ]
    if (player_bgm != null) {
        player_bgm.volume = Number(temp_json['clientBgm'].volume * player_global_volume).toFixed(2)
        if (player_global_muted == true) {
            player_bgm.muted = true
        } else {
            player_bgm.muted = temp_json['clientBgm'].mute
        }
    }
    player_muted.map((item) => {
        if ($('#' + item).length > 0) {
            let musicitem = $('#' + item)[0]
            musicitem.volume = Number(temp_json['clientSound'].volume * player_global_volume).toFixed(2)
            if (player_global_muted == true) {
                musicitem.muted = true
            } else {
                musicitem.muted = temp_json['clientSound'].mute
            }
        }
    })
}

function globalMute(boolean) {
    let temp_json = getSaveMessage('ClientVolume')
    temp_json['globalVolume'].mute = boolean
    saveMessage('ClientVolume', temp_json)
}
function showhallGameRoom(type) {
    if (type == 'close') {
        player_sound_outroom()
        $('#detailsSwiper>iframe').attr('src', 'about:blank');
        //关闭iframe导致窗口无法拖动
        $('.hall-header').css('-webkit-app-region', 'no-drag')
        setTimeout(() => {
            $('.hall-header').css('-webkit-app-region', 'drag')
        }, 1000)
        $('.hall-header-refresh').removeClass('hide')
        // localStorage.removeItem('isdetails')
        return false
    }
    if (type == 'openmatch') {
        if ($('#detailsSwiper>iframe').attr('src') != '/Details/match') {
            $('#detailsSwiper>iframe').attr('src', '/Details/match');
        }
        // $('#detailsSwiper').html('<iframe src="" frameborder="0"></iframe>')
    } else {
        // if ($('#detailsSwiper>iframe').attr('src') != '/Details/index') {
        //     $('#detailsSwiper>iframe').attr('src', '/Details/index');
        // }
        if ($('#detailsSwiper>iframe').attr('src') != '/Details/newindex') {
            $('#detailsSwiper>iframe').attr('src', '/Details/newindex');
        }
        // $('#detailsSwiper').html('<iframe src="/Details/index" frameborder="0"></iframe>')
    }
    hallContentSwiper(5)
    //关闭iframe导致窗口无法拖动
    $('.hall-header').css('-webkit-app-region', 'no-drag')
    setTimeout(() => {
        $('.hall-header').css('-webkit-app-region', 'drag')
    }, 1000)
}

$(document).on('click', '.hall-header-returnroom ,.J_veranda_return_room', function () {
    hallContentSwiper(5)
})

//---------------------百度统计埋点------------------------------

function trackingFunc(value, num, time) {
    switch (value) {
        case 'login_success':
            _hmt.push(['_trackEvent', 'login', 'success', getSaveMessage('Version')]);
            break;
        case 'login_fail':
            _hmt.push(['_trackEvent', 'login', 'fail', getSaveMessage('Version')]);
            break;
        case 'update_start':
            _hmt.push(['_trackEvent', 'update', 'start', getSaveMessage('Version')]);
            break;
        case 'update_end':
            _hmt.push(['_trackEvent', 'update', 'end', getSaveMessage('Version')]);
            break;
        case 'check_success_ares':
            _hmt.push(['_trackEvent', 'check', 'successares', getSaveMessage('Version')]);
            break;
        case 'check_success_game':
            _hmt.push(['_trackEvent', 'check', 'successgame', getSaveMessage('Version')]);
            break;
        case 'check_fail_ares':
            _hmt.push(['_trackEvent', 'check', 'failares', getSaveMessage('Version')]);
            break;
        case 'check_fail_game':
            _hmt.push(['_trackEvent', 'check', 'failgame', getSaveMessage('Version')]);
            break;
        case 'check_modal':
            _hmt.push(['_trackEvent', 'check', 'modal', getSaveMessage('Version')]);
            break;
        case 'check_clickgame':
            _hmt.push(['_trackEvent', 'check', 'clickgame', getSaveMessage('Version')]);
            break;
        case 'check_clickares':
            _hmt.push(['_trackEvent', 'check', 'clickares', getSaveMessage('Version')]);
            break;
        case 'join_fast':
            _hmt.push(['_trackEvent', 'join', 'fast', getSaveMessage('Version')]);
            break;
        case 'join_hotmap':
            _hmt.push(['_trackEvent', 'join', 'hotmap', getSaveMessage('Version')]);
            break;
        case 'join_list':
            _hmt.push(['_trackEvent', 'join', 'list', getSaveMessage('Version')]);
            break;
        case 'join_map':
            _hmt.push(['_trackEvent', 'join', 'map', getSaveMessage('Version')]);
            break;
        case 'join_hall':
            _hmt.push(['_trackEvent', 'join', 'hall', getSaveMessage('Version')]);
            break;
        case 'join_friend':
            _hmt.push(['_trackEvent', 'join', 'friend', getSaveMessage('Version')]);
            break;
        case 'join_find':
            _hmt.push(['_trackEvent', 'join', 'find', getSaveMessage('Version')]);
            break;
        case 'create_modal':
            _hmt.push(['_trackEvent', 'create', 'modal', getSaveMessage('Version')]);
            break;
        case 'create_room':
            _hmt.push(['_trackEvent', 'create', 'room', getSaveMessage('Version')]);
            break;
        case 'Match_start':
            _hmt.push(['_trackEvent', 'match', 'start', getSaveMessage('Version')]);
            break;
        case 'Match_success':
            _hmt.push(['_trackEvent', 'match', 'success', getSaveMessage('Version')]);
            break;
        case 'match_ready':
            _hmt.push(['_trackEvent', 'matchready', num, getSaveMessage('Version')]);
            break;
        case 'match_close4':
            _hmt.push(['_trackEvent', 'matchclose4', num, getSaveMessage('Version')]);
            break;
        case 'match_fail':
            _hmt.push(['_trackEvent', 'matchfail', num, getSaveMessage('Version')]);
            break;
        case 'match_close7':
            _hmt.push(['_trackEvent', 'matchclose7', num, getSaveMessage('Version')]);
            break;
        case 'match_show':
            _hmt.push(['_trackEvent', 'match', 'show', getSaveMessage('Version')]);
            break;
        case 'rank_start':
            _hmt.push(['_trackEvent', 'rank', 'start', getSaveMessage('Version')]);
            break;
        case 'rank_success':
            _hmt.push(['_trackEvent', 'rank', 'success', getSaveMessage('Version')]);
            break;
        case 'rank_ready':
            _hmt.push(['_trackEvent', 'rankready', num, getSaveMessage('Version')]);
            break;
        case 'rank_close4':
            _hmt.push(['_trackEvent', 'rankclose4', num, getSaveMessage('Version')]);
            break;
        case 'rank_close7':
            _hmt.push(['_trackEvent', 'rankclose7', num, getSaveMessage('Version')]);
            break;
        case 'rankfail':
            _hmt.push(['_trackEvent', 'rankfail', num, getSaveMessage('Version')]);
            break;
        case 'rank_show':
            _hmt.push(['_trackEvent', 'rank', 'show', getSaveMessage('Version')]);
            break;
        case 'room_RemoveRoom':
            _hmt.push(['_trackEvent', 'room', 'RemoveRoom', getSaveMessage('Version')]);
            break;
        case 'room_autoclose':
            _hmt.push(['_trackEvent', 'room', 'autoclose', getSaveMessage('Version')]);
            break;
        case 'room_hostclose':
            _hmt.push(['_trackEvent', 'room', 'hostclose', getSaveMessage('Version')]);
            break;
        case 'start_custom':
            _hmt.push(['_trackEvent', 'start', 'custom', getSaveMessage('Version')]);
            break;
        case 'join_error':
            _hmt.push(['_trackEvent', 'join', 'error', getSaveMessage('Version')]);
            break;
        case 'live':
            _hmt.push(['_trackEvent', 'live', num, getSaveMessage('Version')]);
            break;
        case 'livetime':
            _hmt.push(['_trackEvent', 'live', num + '&' + time, getSaveMessage('Version')]);
            break;
        case 'newhelp_init':
            _hmt.push(['_trackEvent', 'newhelp', 'init', getSaveMessage('Version')]);
            break;
        case 'newhelp_find':
            _hmt.push(['_trackEvent', 'newhelp', 'find', getSaveMessage('Version')]);
            break;
        case 'newhelp_finderror':
            _hmt.push(['_trackEvent', 'newhelp', 'finderror', getSaveMessage('Version')]);
            break;
        case 'newhelp_InstallGame':
            _hmt.push(['_trackEvent', 'newhelp', 'InstallGame', getSaveMessage('Version')]);
            break;
        case 'newhelp_appoint':
            _hmt.push(['_trackEvent', 'newhelp', 'appoint', getSaveMessage('Version')]);
            break;
        case 'newhelp_appointerror':
            _hmt.push(['_trackEvent', 'newhelp', 'appointerror', getSaveMessage('Version')]);
            break;
        case 'newhelp_InstallGameFast':
            _hmt.push(['_trackEvent', 'newhelp', 'InstallGameFast', getSaveMessage('Version')]);
            break;
        case 'newhelp_InstallGamesuc':
            _hmt.push(['_trackEvent', 'newhelp', 'InstallGamesuc', getSaveMessage('Version')]);
            break;
        case 'newhelp_CheckGame':
            _hmt.push(['_trackEvent', 'newhelp', 'CheckGame', getSaveMessage('Version')]);
            break;
        case 'newhelp_CheckGamesuc':
            _hmt.push(['_trackEvent', 'newhelp', 'CheckGamesuc', getSaveMessage('Version')]);
            break;
        case 'newhelp_CheckGameerror':
            _hmt.push(['_trackEvent', 'newhelp', 'CheckGameerror', getSaveMessage('Version')]);
            break;
        case 'newhelp_CheckGameRepair':
            _hmt.push(['_trackEvent', 'newhelp', 'CheckGameRepair', getSaveMessage('Version')]);
            break;
        case 'newhelp_CheckGameRepairsuc':
            _hmt.push(['_trackEvent', 'newhelp', 'CheckGameRepairsuc', getSaveMessage('Version')]);
            break;
        case 'newhelp_CheckGameRepairInstall':
            _hmt.push(['_trackEvent', 'newhelp', 'CheckGameRepairInstall', getSaveMessage('Version')]);
            break;
        case 'newhelp_StartTest':
            _hmt.push(['_trackEvent', 'newhelp', 'StartTest', getSaveMessage('Version')]);
            break;
        case 'newhelp_StartTestError':
            _hmt.push(['_trackEvent', 'newhelp', 'StartTestError', getSaveMessage('Version')]);
            break;
        case 'newhelp_StartTestSuc':
            _hmt.push(['_trackEvent', 'newhelp', 'StartTestSuc', getSaveMessage('Version')]);
            break;
        case 'newhelp_StartTestagain':
            _hmt.push(['_trackEvent', 'newhelp', 'StartTestagain', getSaveMessage('Version')]);
            break;
        case 'newhelp_findhelp':
            _hmt.push(['_trackEvent', 'newhelp', 'findhelp', getSaveMessage('Version')]);
            break;
        case 'newhelp_Sucend':
            _hmt.push(['_trackEvent', 'newhelp', 'Sucend', getSaveMessage('Version')]);
            break;
        case 'newhelp_succlick':
            _hmt.push(['_trackEvent', 'newhelp', 'succlick', getSaveMessage('Version')]);
            break;
        case 'hall_banner':
            _hmt.push(['_trackEvent', 'hall_banner', num, getSaveMessage('Version')]);
            break;
        case 'url_open':
            _hmt.push(['_trackEvent', 'url_open', 'success', getSaveMessage('Version')]);
            break;
        default:
            break;
    }
}

// 地图推荐加入
$(document).on('click', '.veranda-content-hot .J_veranda_list_fast_btn', function () {
    trackingFunc('join_hotmap')
})
// 列表模式加入
$(document).on('click', '.veranda-roomlist-body-item .J_veranda_join', function () {
    trackingFunc('join_list')
})
// 地图模式加入
$(document).on('click', '.veranda-maplist-scroll .J_veranda_list_fast_btn', function () {
    trackingFunc('join_map')
})
// 大厅喊话邀请加入
$(document).on('click', '.veranda-content-chatContent-scroll .J_veranda_join', function () {
    trackingFunc('join_hall')
})
// 好友邀请邀请加入
$(document).on('click', '.Invite-gxmodal .J_veranda_join', function () {
    trackingFunc('join_friend')
})


//---------------------百度统计埋点------------------------------
$(document).on('change', '.gx-switch input', function () {
    var $this = $(this).closest('.gx-switch')
    if ($this.hasClass('disabled')) {
        return false
    }
    if ($this.hasClass('open')) {
        $this.removeClass('open')
    } else {
        $this.addClass('open')
    }
})
// 窗口
function showWindowModal(windowname, url) {
    if (windowname == 'settingmodal') {
        ipcRenderer.send('Main', {
            msgType: "ShowModal",
            jsonInfo: {
                window: windowname,
                url: url,
                width: 620,
                height: 456,
                alwaysOnTop: true,//窗口置顶
                skipTaskbar: true
            }
        });
    } else if (windowname == 'downloadmodal') {
        ipcRenderer.send('Main', {
            msgType: "ShowModal",
            jsonInfo: {
                window: 'downloadmodal',
                url: url,
                width: 500,
                height: 424,
                alwaysOnTop: true,//窗口置顶
                skipTaskbar: true
            }
        });
    }
}
function DoesExistChinese(string) {
    const patrn = /[\u4E00-\u9FA5]|[\uFE30-\uFFA0]/gi;
    return !patrn.exec(string) ? string : false;
}
function customFieldFun(type, array) {
    let resulttest = ''
    let addtest = '+++++++++'
    if (type == 'encryption') {
        resulttest = JSON.stringify(array) + addtest
    } else if (type == 'decode') {
        if (array) {
            if (!array.split) {
                resulttest = array
            } else {
                resulttest = array.split(addtest)[0]
                if (resulttest) {
                    if (resulttest.indexOf('{') == -1) {
                        // 这个兼容老用户
                        resulttest = {
                            avatarFrame: resulttest,
                            authenticateType: null,
                            authenticateContent: null,
                            isAuthenticate: null
                        }
                    } else {
                        resulttest = JSON.parse(resulttest)
                    }
                }
                if (resulttest.isAuthenticate) {
                    resulttest.authenticateType = null
                    resulttest.authenticateContent = null
                }
            }
        } else {
            resulttest = {
                avatarFrame: null,
                authenticateType: null,
                authenticateContent: null,
                isAuthenticate: null
            }
        }
    }
    return resulttest
}
// 滑块
$.fn.GXRangeSlider = function (cfg) {
    this.sliderCfg = {
        min: cfg && !isNaN(parseFloat(cfg.min)) ? Number(cfg.min) : null,
        max: cfg && !isNaN(parseFloat(cfg.max)) ? Number(cfg.max) : null,
        step: cfg && Number(cfg.step) ? cfg.step : 1,
        callback: cfg && cfg.callback ? cfg.callback : null,
        color: cfg.color == 'nocolor' ? false : true
    };

    var $input = $(this);
    var min = this.sliderCfg.min;
    var max = this.sliderCfg.max;
    var step = this.sliderCfg.step;
    var callback = this.sliderCfg.callback;
    var color = this.sliderCfg.color;

    $input.attr('min', min)
        .attr('max', max)
        .attr('step', step);

    $input.bind("input", function (e) {
        $input.attr('value', this.value);
        if (color) {
            $input.css('background', 'linear-gradient(90deg,  #6B9EFF 0, #6B9EFF ' + this.value / max * 100 + '%, #1C2744 0, #1C2744)');
        }
        // $input.css('background', 'linear-gradient(to right, #6B9EFF, #6B9EFF ' + this.value + '%, white)');

        if ($.isFunction(callback)) {
            callback(this);
        }
    });
    $input.trigger('input')
}

function parseBoolean(string) {
    switch (String(string).toLowerCase()) {
        case "true":
        case "1":
        case "yes":
        case "y":
            return true;
        case "false":
        case "0":
        case "no":
        case "n":
            return false;
        default:
            //you could throw an error, but 'undefined' seems a more logical reply
            return undefined;
    }
}


// --------图片数据处理
setCache()
function setCache() {
    let request = indexedDB.open('ramboplay', '1.0');
    request.onerror = function (e) {
        console.log('indexedDB错误')
    };
    request.onsuccess = function (e) {
        let db = e.target.result;
        let tt = db.transaction(["imgUrl"], "readwrite");
        let st = tt.objectStore("imgUrl");
        let count = st.count();
        count.onsuccess = function () {
            let countnum = count.result
            let maxnum = 500
            if (countnum > maxnum) {
                let requestall = st.getAll()
                requestall.onsuccess = function () {
                    let allarray = requestall.result
                    allarray = allarray.sort(function (a, b) {
                        return b.time - a.time;
                    })
                    for (let i = maxnum; i < allarray.length; i++) {
                        let requestdelete = st.delete(allarray[i].url)
                        requestdelete.onsuccess = function () {
                            console.log('delete')
                        }
                        requestdelete.onerror = function () {
                            console.log('delete error')
                        }
                    }
                };
            }
        };
    };
    request.onupgradeneeded = function (e) {
        console.log('创建indexedDB')
        db = e.target.result;
        let objectStore;
        if (!db.objectStoreNames.contains('imgUrl')) {
            objectStore = db.createObjectStore(
                'imgUrl',
                { keyPath: 'url' }
            );
        }
    };
}
// 更新时间
function insertIndexedDBImgUrlTime(updata) {
    if (updata.length > 0) {
        let request = indexedDB.open('ramboplay', '1.0');
        request.onerror = function (e) {
            console.log('indexedDB错误')
        };
        request.onsuccess = function (e) {
            let db = e.target.result;
            let tt = db.transaction(["imgUrl"], "readwrite");
            let st = tt.objectStore("imgUrl");
            updata.forEach(data => {
                let request2 = st.put(data)
                request2.onsuccess = function (event) {
                };
                request2.onerror = function (event) {
                    console.log("数据更新失败");
                }
            })

        };
    }
}
// 写入数据
function insertIndexedDBImgUrl(item) {
    let img = new Image();
    img.setAttribute("crossOrigin", 'Anonymous')
    img.onload = () => {
        let canvas = document.createElement('canvas');
        canvas.height = img.height;
        canvas.width = img.width;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        let nowtime = new Date
        let da = {
            url: item,
            base64: canvas.toDataURL(),
            time: nowtime.getTime()
        }
        if (da.base64 == 'data:,') {
            da.base64 = ''
        }
        let request = indexedDB.open('ramboplay', '1.0');
        request.onerror = function (e) {
            console.log('indexedDB错误')
        };
        request.onsuccess = function (e) {
            let db = e.target.result;
            let tt = db.transaction(["imgUrl"], "readwrite");
            let st = tt.objectStore("imgUrl");
            let request2 = st.add(da);
            request2.onsuccess = function (event) {
                // console.log('数据写入成功');
            };
            request2.onerror = function (event) {
                console.log('数据写入失败');
            }
        };
    }
    img.src = item;
}
// 读取数据
async function readerIndexedDBImgUrl(keyarray) {
    if (keyarray.size == 0) {
        return keyarray
    }
    let dellarrya = []
    for (let item of keyarray) {
        dellarrya.push(item[0]);
    }
    dellarrya = dellarrya.sort()
    return new Promise((res) => {
        let request = indexedDB.open('ramboplay', '1.0'); //打开数据库
        request.onerror = function (e) {
            //错误的回到方法
            res(keyarray)
        };
        request.onsuccess = function (e) {
            let db = e.target.result;
            let tt = db.transaction(["imgUrl"], "readonly") || 'close';
            if (tt == 'close') {
                res(keyarray)
            }
            let st = tt.objectStore("imgUrl");
            for (let i = 0; i < dellarrya.length; i++) {
                let item = dellarrya[i]
                let request = st.get(item)
                request.onsuccess = (e) => {
                    let getitem = e.target.result
                    if (getitem) {
                        keyarray.set(getitem.url, getitem.base64)
                        let nowtime = new Date
                        let da = {
                            url: getitem.url,
                            base64: getitem.base64,
                            time: nowtime.getTime()
                        }
                        insertIndexedDBImgUrlTime([da])
                    } else {
                        insertIndexedDBImgUrl(item)
                    }
                    if (i == (dellarrya.length - 1)) {
                        res(keyarray)
                    }
                }
                request.onerror = (e) => {
                    insertIndexedDBImgUrl(item)
                    if (i == (dellarrya.length - 1)) {
                        res(keyarray)
                    }
                }
            }
        };
    })
}
// 单图片处理
async function oneImgCache(img) {
    let topMapList = [img]
    let imgarray = new Map()
    let Cachereturn = ''
    if (img.indexOf('data:image') != -1) {
        Cachereturn = img
        return Cachereturn
    }
    for (let i = 0; i < topMapList.length; i++) {
        imgarray.set(topMapList[i], '')
    }
    let dellimgarray = await readerIndexedDBImgUrl(imgarray)
    let itemimg = img
    if (dellimgarray.has(itemimg)) {
        if (dellimgarray.get(itemimg)) {
            Cachereturn = dellimgarray.get(itemimg)
        } else {
            Cachereturn = itemimg
        }
    }
    return Cachereturn
}
// --------图片数据处理