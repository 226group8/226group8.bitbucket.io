/*****************************************************************
                  jQuery Ajax封装通用类  (linjq)       
*****************************************************************/
/**
 * ajax封装
 * url 发送请求的地址
 * data 发送到服务器的数据，数组存储，如：{"date": new Date().getTime(), "state": 1}
 * async 默认值: true。默认设置下，所有请求均为异步请求。如果需要发送同步请求，请将此选项设置为 false。
 *       注意，同步请求将锁住浏览器，用户其它操作必须等待请求完成才可以执行。
 * type 请求方式("POST" 或 "GET")， 默认为 "GET"
 * dataType 预期服务器返回的数据类型，常用的如：xml、html、json、text
 * successfn 成功回调函数
 * errorfn 失败回调函数
 */
var device_id = ""
var access_token = ""

// var ajaxUrl = 'http://dev-app.zbt.com'
// var ajaxUrl = 'https://test-gateway.c5game.com/'
var ajaxUrl = 'https://api.ramboplay.com'

// var ajaxUrl = 'http://yapi.c5game.cn/mock/505'

if ($.cookie('deviceId')) {
    device_id = $.cookie('deviceId')
} else {
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
    device_id = [];
    for (let i = 0; i < 16; i++) device_id[i] = chars[0 | Math.random() * 32];
    device_id = device_id.join('')
    var timestamp = new Date().getTime();
    device_id = 'REDWAR_' + device_id + timestamp
    $.cookie('deviceId', device_id)
}


jQuery.star = function (option) {
    if (getSaveMessage('userInfo')) {
        access_token = getSaveMessage('userInfo').accessToken
    } else {
        access_token = ""
    }
    if (option.access_token) {
        access_token = option.access_token
    }
    var getUrl = ""
    if (option.url.indexOf('http://') != -1) {
        getUrl = option.url
    } else {
        if (getSaveMessage('test')) {
            getUrl = 'https://test-gateway.ramboplay.com' + option.url
        } else if (getSaveMessage('pre')) {
            getUrl = 'https://pre-api.ramboplay.com' + option.url
        } else {
            getUrl = ajaxUrl + option.url
        }
    }
    // let charsrem = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
    // let traceId_id = [];
    // for (let i = 0; i < 5; i++) traceId_id[i] = charsrem[0 | Math.random() * 32];
    // traceId_id = traceId_id.join('')
    // let timestamp = new Date().getTime();
    // traceId_id = timestamp + traceId_id
    let LBWlanguage = localStorage.getItem('LBWlanguage') || 'zh'
    if (LBWlanguage == 'en') {
        LBWlanguage = 'en_US'
    }
    $.ajax({
        type: option.type || 'GET',
        async: option.async || true,
        data: option.type == 'GET' ? option.data : JSON.stringify(option.data),
        timeout: 6000, //超时时间设置，单位毫秒
        url: getUrl,
        dataType: option.dataType || 'JSON',
        headers: {
            'Content-type': 'application/json',
            'device_id': device_id,
            'device': 5,
            'platform': 3,
            'language': LBWlanguage,
            'access_token': access_token,
            'app_version_code': getSaveMessage('msgType21').version || "",
        },
        success: function (d) {
            if (d.success) {
                option.success(d);
            } else if (d.errorCode == 101 && window.location.href.indexOf('/Hall/index') != -1) {
                ipcRenderer.send('Main', {
                    msgType: "ChangeWindowSize",
                    jsonInfo: {
                        window: 'main',
                        width: 1280,
                        height: 720,
                        miniWidth: 1280,
                        miniHeight: 720,
                        isCenter: true
                    }
                });
                location.href = '/Login/index?code=101'
            } else {
                d.errorMsg = translatesrting(d.errorMsg)
                if (d.errorCode == 210001) {
                    d.errorMsg = translatesrting('服务器开小差，请稍后再试下哦~')
                }
                if (option.showerror && d.errorMsg) {
                    showtoast({
                        message: d.errorMsg
                    })
                }
                option.error(d);
            }
        },
        error: function (d) {
            if (option.showerror) {
                showtoast({
                    message: translatesrting('当前网络连接出现问题')
                })
            }
            option.error({
                data: null,
                errorCode: 0014,
                errorData: null,
                errorMsg: translatesrting('当前网络连接出现问题'),
                success: false,
            });
        },
        complete: function (XMLHttpRequest, status) {
            if (status == 'timeout') {//超时,status还有success,error等值的情况
                option.error({
                    data: null,
                    errorCode: 0014,
                    errorData: null,
                    errorMsg: translatesrting("连接超时，请检查网络"),
                    success: false,
                });
            }
        }

    });
};
/**
 * ajax封装
 * url 发送请求的地址
 * data 发送到服务器的数据，数组存储，如：{"date": new Date().getTime(), "state": 1}
 * successfn 成功回调函数
 */
jQuery.starpost = function (url, data, successfn) {
    data = (data == null || data == "" || typeof (data) == "undefined") ? { "date": new Date().getTime() } : data;
    $.ajax({
        type: "post",
        data: data,
        url: url,
        dataType: "json",
        success: function (d) {
            successfn(d);
        }
    });
};
/**
 * ajax封装
 * url 发送请求的地址
 * data 发送到服务器的数据，数组存储，如：{"date": new Date().getTime(), "state": 1}
 * dataType 预期服务器返回的数据类型，常用的如：xml、html、json、text
 * successfn 成功回调函数
 * errorfn 失败回调函数
 */
jQuery.starspost = function (url, data, successfn, errorfn) {
    data = (data == null || data == "" || typeof (data) == "undefined") ? { "date": new Date().getTime() } : data;
    $.ajax({
        type: "post",
        data: data,
        url: url,
        dataType: "json",
        success: function (d) {
            successfn(d);
        },
        error: function (e) {
            errorfn(e);
        }
    });
};

jQuery.controlAjax = function (option) {
    if (getSaveMessage('userInfo')) {
        access_token = getSaveMessage('userInfo').accessToken
    } else {
        access_token = ""
    }
    let LBWlanguage = localStorage.getItem('LBWlanguage') || 'zh'
    if (LBWlanguage == 'en') {
        LBWlanguage = 'en_US'
    }
    $.ajax({
        type: option.type || 'get',
        url: option.url,
        data: option.type == 'get' ? $.param(option.data, true) : JSON.stringify(option.data),
        headers: {
            'Content-type': 'application/json',
            'device_id': device_id,
            'device': 5,
            'platform': 3,
            'language': LBWlanguage,
            'access_token': access_token,
            'app_version_code': getSaveMessage('msgType21').version || ""
        },
        success: function (d) {
            if (d.success) {
                option.success(d);
            } else if (d.statusCode == 401 && window.location.href.indexOf('/Hall/index') != -1) {
                location.href = '/Login/index?code=401'
            } else if (d.errorCode == 101 && window.location.href.indexOf('/Hall/index') != -1) {
                location.href = '/Login/index?code=101'
            } else {
                if (d.errorMsg == "您已加入游戏房间" || d.errorMsg == "您已经创建房间") {
                    d.JerrorCode = 'inroom'
                }
                d.errorMsg = translatesrting(d.errorMsg)
                if (d.errorCode == 210001) {
                    d.errorMsg = translatesrting('服务器开小差，请稍后再试下哦~')
                }
                if (option.showerror && d.errorMsg) {
                    showtoast({
                        message: d.errorMsg
                    })
                }
                option.error(d);
            }
        },
        error: function (d) {
            option.error({
                data: null,
                errorCode: 0014,
                errorData: null,
                errorMsg: translatesrting("当前网络连接出现问题"),
                success: false,
            });
        },
        complete: function (XMLHttpRequest, status) {
            if (status == 'timeout') {//超时,status还有success,error等值的情况
                option.error({
                    data: null,
                    errorCode: 0014,
                    errorData: null,
                    errorMsg: translatesrting("连接超时，请检查网络"),
                    success: false,
                });
            }
        }
    })
};
jQuery.imgAjax = function (option) {
    var getUrl = ""
    if (option.url.indexOf('http://') != -1) {
        getUrl = option.url
    } else {
        if (getSaveMessage('test')) {
            getUrl = 'https://test-gateway.ramboplay.com' + option.url
        } else if (getSaveMessage('pre')) {
            getUrl = 'https://pre-api.ramboplay.com' + option.url
        } else {
            getUrl = ajaxUrl + option.url
        }
    }
    let LBWlanguage = localStorage.getItem('LBWlanguage') || 'zh'
    if (LBWlanguage == 'en') {
        LBWlanguage = 'en_US'
    }
    $.ajax({
        type: option.type || 'get',
        url: getUrl,
        data: option.data,
        contentType: false,
        processData: false,
        headers: {
            'device_id': device_id,
            'device': 5,
            'platform': 3,
            'language': LBWlanguage,
            'access_token': access_token,
            'app_version_code': getSaveMessage('msgType21').version || ""
        },
        success: function (d) {
            if (d.success) {
                option.success(d);
            } else if (d.statusCode == 401 && window.location.href.indexOf('/Hall/index') != -1) {
                location.href = '/Login/index?code=401'
            } else {
                option.error(d);
            }
        },
        error: function (d) {
            option.error(d);
        },
        complete: function (XMLHttpRequest, status) {
            if (status == 'timeout') {//超时,status还有success,error等值的情况
                option.error({
                    data: null,
                    errorCode: 0014,
                    errorData: null,
                    errorMsg: "连接超时，请检查网络",
                    success: false,
                });
            }
        }
    })
};