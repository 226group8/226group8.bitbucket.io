var ChooseMapMessage = {
    intype: null,
    label: null,
    hot: 0,
    newest: 0,
    pageNum: 1,
    pageSize: 20,
    chooseIndex: 0,
    localList: [],
    chooseItem: null,
    pageloading: false,
    key: "",//地图关键词
    sort: 'active',//排序字段，playerNum(最大玩家数排序)|active(活跃度排序)|publishTime(发布时间排序)
    asc: false,//默认true, 正序true, 逆序false
    playerNum: null,//筛选玩家数
    isPrivate: false
}
// 监听搜索地图名
$(document).on('input', '.createRoomlist-keyInput', function () {
    var $this = $(this)
    ChooseMapMessage.key = $this.val()
})
$(document).on('submit', '.createRoomlist-keyForm', function (e) {
    e.preventDefault()
    MapInitAjax()
})


// 详情页修改地图按钮
function chooseMapInit() {
    ChooseMapMessage.intype = comOption.optionsMode[0].code
    ChooseMapMessage.label = comOption.labelArray[0].code
    ChooseMapMessage.pageNum = 1
    $('.createRoomlist-modal-typelist').html(template('createRoomlist-modal-typelist-script', {
        typeArray: comOption.optionsMode,
        intype: ChooseMapMessage.intype
    }))
    $('.createRoomlist-modal-navList').html(template('createRoomlist-modal-navList-script', {
        labelArray: comOption.labelArray,
        label: ChooseMapMessage.label,
        hot: ChooseMapMessage.hot,
        newest: ChooseMapMessage.newest,
    }))
    isneedprivate()
    $('.createRoomlist-modal-tablelist .gx-menu').gxmenu({
        height: "31px",
        top: "30px",
        maxheight: "250px",
        clickHide: true//点击内部是否消失
    }, function (e) {

    })
    $('.createRoomlist-chooseNum-text').text("")
    $('[data-toggle="tooltip"]').tooltip()
    ChooseMapMessage.key = ""
    ChooseMapMessage.sort = 'active'
    ChooseMapMessage.asc = false
    ChooseMapMessage.playerNum = null
    $('.createRoomlist-keyInput').val("")
    MapInitAjax()
    $('#modalChangeMap').modal('show')
    // 恢复
    $('.J_createRoomlist-sort-icon').css('opacity', 1)
    $('.createRoomlist-modal-tablelist-thead-normal .gx-menu').find('.caret').css('opacity', 1)
    $('.createRoomlist-modal-tablelist-thead-normal .gx-menu').css('pointer-events', 'auto')
    $('.needprivate_nav').css('display', 'none')
    $('.createRoomlist-modal-tablelist-scroll').css('height', '413px')
}
function isneedprivate() {
    $.controlAjax({
        type: "get",
        url: '/api/lobby/author/maps/',
        data: {
            page: 1,
            limit: 1,
            status: 1,
            isPrivate: 1
        },
        success: function (res) {
            if (res.data.list.length > 0) {
                $('.createRoomlist-modal-navList').find('.needprivate_nav').removeClass('hide')
                $('.createRoomlist-modal-navList').addClass('createRoomlist-modal-navList8')
            }
        },
        error: function (req) {

        }
    })
}
// 选择最大玩家数
$(document).on('click', '.createRoomlist-chooseNum', function () {
    var $this = $(this)
    ChooseMapMessage.playerNum = $this.attr('data-code')
    $this.closest('.gx-menu').find('.createRoomlist-chooseNum-text').text(ChooseMapMessage.playerNum)
    MapInitAjax()
})
// 选择热度排序
$(document).on('click', '.J_createRoomlist-sort', function () {
    var $this = $(this)
    if ($this.find('.J_createRoomlist-sort-icon').css('opacity') == 0) {
        return false
    }
    ChooseMapMessage.asc = $this.attr('data-code')
    if (ChooseMapMessage.asc == 'true') {
        $this.attr('data-code', 'false')
        $this.find('.triangle-up').removeClass('hide')
        $this.find('.triangle-down').addClass('hide')
    } else {
        $this.attr('data-code', 'true')
        $this.find('.triangle-up').addClass('hide')
        $this.find('.triangle-down').removeClass('hide')
    }
    MapInitAjax()
})
// 选择地图类型
$(document).on('click', '.J_chooseMapType', function () {
    var $this = $(this)
    $this.siblings().removeClass('active')
    $this.addClass('active')
    ChooseMapMessage.intype = $this.attr('data-code')
    MapInitAjax()
})
$(document).on('click', '.createRoom-choosemodal-item', function () {
    var $this = $(this).find('input')
    ChooseMapMessage.intype = $this.val()
})
// 收藏
$(document).on('click', '.J_mapcollection', function () {
    let choose = !ChooseMapMessage.chooseItem.collectStatus
    ChooseMapMessage.chooseItem.collectStatus = choose
    ChooseMapMessage.localList.map((item) => {
        if (item.id == ChooseMapMessage.chooseItem.id) {
            item.collectStatus = choose
        }
    })
    var $this = $(this)
    if (choose) {
        $this.find('.J_mapcollection-close').addClass('hide')
        $this.find('.J_mapcollection-in').removeClass('hide')
        choose = 1
    } else {
        $this.find('.J_mapcollection-close').removeClass('hide')
        $this.find('.J_mapcollection-in').addClass('hide')
        choose = 0
    }
    $.star({
        type: 'POST',
        url: '/battlecenter/redwar/map/v1/collect-map',
        showerror: true,
        data: {
            mapId: ChooseMapMessage.chooseItem.id,
            operate: choose,
        },
        success: function (res) {
            if (choose) {
                showtoast({
                    message: translatesrting('收藏成功')
                })
            } else {
                showtoast({
                    message: translatesrting('取消收藏成功')
                })
            }
        },
        error: function (req) {
        }
    });

})

// 选择菜单类型
$(document).on('click', '.J_chooseMapNav', function () {
    var $this = $(this)
    $this.closest('.createRoomlist-modal-navList').each(function () {
        $(this).find('.createRoomlist-modal-nav').removeClass('active')
    })
    $('.J_chooseMapPrivate').each(function () {
        $(this).removeClass('active')
        if ($(this).attr('data-code') == 'private-close')
            $(this).addClass('active')
    })
    $this.addClass('active')
    let code = $this.attr('data-code')
    ChooseMapMessage.sort = 'active'
    $('.J_createRoomlist-sort-icon').css('opacity', 1)
    if (code == 'hot' || code == 'newest') {
        ChooseMapMessage.hot = "0"
        ChooseMapMessage.newest = "0"
        if (code == 'hot') {
            ChooseMapMessage.asc = false
            ChooseMapMessage.hot = "0"
            ChooseMapMessage.sort = 'weekAddActive'
        } else if (code == 'newest') {
            ChooseMapMessage.newest = "1"
            ChooseMapMessage.sort = 'publishTime'
        }
        $('.J_createRoomlist-sort-icon').css('opacity', 0)
        ChooseMapMessage.label = null
        ChooseMapMessage.isPrivate = false
    } else {
        ChooseMapMessage.hot = "0"
        ChooseMapMessage.newest = "0"
        if (code != 'private') {
            ChooseMapMessage.isPrivate = false
            ChooseMapMessage.label = $this.attr('data-code')
        } else {
            ChooseMapMessage.isPrivate = true
        }
    }
    if (code == 'mycollection') {
        $('.createRoomlist-modal-tablelist-thead-normal .gx-menu').find('.caret').css('opacity', 0)
        $('.createRoomlist-modal-tablelist-thead-normal .gx-menu').css('pointer-events', 'none')
        $('.J_createRoomlist-sort-icon').css('opacity', 0)
        $('.needprivate_nav').css('display', 'inline-block')
        if (!$('.needprivate_nav').hasClass('hide')) {
            $('.createRoomlist-modal-tablelist-scroll').css('height', '386px')
        }
    } else {
        $('.createRoomlist-modal-tablelist-thead-normal .gx-menu').find('.caret').css('opacity', 1)
        $('.createRoomlist-modal-tablelist-thead-normal .gx-menu').css('pointer-events', 'auto')
        $('.needprivate_nav').css('display', 'none')
        $('.createRoomlist-modal-tablelist-scroll').css('height', '413px')
    }
    MapInitAjax()
})
// 选择地图
$(document).on('click', '.J_chooseItem', function () {
    var $this = $(this)
    if ($this.hasClass('active')) {
        return false
    }
    $('#createRoomlist-modal-tablelist-tbody').find('.J_chooseItem.active').removeClass('active')
    $this.addClass('active')
    ChooseMapMessage.chooseIndex = $('.J_chooseItem').index($this)
    ChooseMapMessage.chooseItem = ChooseMapMessage.localList[ChooseMapMessage.chooseIndex]
    ChooseMapMessage.chooseItem.Jmodels = []
    ChooseMapMessage.chooseItem.models.map((models) => {
        comOption.optionsMode.map((itemmodels) => {
            if (models == itemmodels.code) {
                ChooseMapMessage.chooseItem.Jmodels.push(itemmodels)
            }
        })
    })
    ChooseMapMessage.chooseItem.isPrivate = ChooseMapMessage.isPrivate
    let imgsize = '?x-oss-process=image/resize,w_600'
    if (ChooseMapMessage.chooseItem.imageUrl.indexOf(imgsize) == -1 && ChooseMapMessage.chooseItem.imageUrl.indexOf('data:image') == -1) {
        ChooseMapMessage.chooseItem.imageUrl = ChooseMapMessage.chooseItem.imageUrl + imgsize
    }
    oneImgCache(ChooseMapMessage.chooseItem.imageUrl).then((Cachereturn) => {
        ChooseMapMessage.chooseItem.localimageUrl = Cachereturn
        $('.createRoomlist-details').html(template('createRoomlist-details-script', ChooseMapMessage.chooseItem))
        $('[data-toggle="tooltip"]').tooltip()
        $('.createRoom-choosemodal-item').eq(0).click()
        $('.createRoom-choosemodal-item').eq(0).find('input').click()
    })
})
// 下拉
function changeMapPage() {
    $('.createRoomlist-modal-tablelist-scroll').on('scroll', function () {
        var $this = $(this)
        var nScrollHight = $this[0].scrollHeight,
            nScrollTop = $this[0].scrollTop,
            nDivHight = $this.height();
        if (nScrollTop + nDivHight + 300 >= nScrollHight && !ChooseMapMessage.isLoading) {
            ChooseMapMessage.pageNum = ChooseMapMessage.pageNum + 1
            mapPage()
        }
    })
}
// 分页
function mapPage() {
    if (ChooseMapMessage.isLoading) {
        return false;
    }
    ChooseMapMessage.isLoading = true
    let url = '/Modal/HomeGetMapList'
    let data = {
        key: ChooseMapMessage.key,
        label: ChooseMapMessage.label,
        // model: ChooseMapMessage.intype,
        pageNum: ChooseMapMessage.pageNum,
        pageSize: ChooseMapMessage.pageSize,
        sort: ChooseMapMessage.sort,
        asc: ChooseMapMessage.asc,
        playerNum: ChooseMapMessage.playerNum,
        hot: ChooseMapMessage.hot,
        newest: ChooseMapMessage.newest
    }
    if (data.label == "mycollection") {
        data.label = ""
        data.collect = true
    } else {
        data.collect = false
    }
    if (ChooseMapMessage.isPrivate) {
        data.isPrivate = 1
        data.label = ""
    }
    $.controlAjax({
        type: "get",
        url: url,
        data: data,
        showerror: true,
        success: function (res) {
            if (res.success) {
                ChooseMapMessage.isLoading = false
                var getlist = res.data.list
                getlist.map((items) => {
                    items.activityNum = formatMapHot(items.activityNum)
                    items.name = items.name + '&&||' + items.enName
                })
                // if (ChooseMapMessage.isPrivate) {
                //     for (let i = 0; i < getlist.length; i++) {
                //         getlist[i].name = getlist[i].mapName
                //     }
                // }
                ChooseMapMessage.localList = ChooseMapMessage.localList.concat(getlist)
                $('#createRoomlist-modal-tablelist-tbody').append(template('createRoomlist-modal-tablelist-tbody-script', { isadd: true, list: getlist, isprivate: ChooseMapMessage.isPrivate, label: ChooseMapMessage.label }))
                if (getlist.length < ChooseMapMessage.pageSize) {
                    $('.createRoomlist-modal-tablelist-scroll-end').addClass('hide')
                    $('.createRoomlist-modal-tablelist-scroll').off('scroll')
                } else {
                    $('.createRoomlist-modal-tablelist-scroll-end').removeClass('hide')
                }
            } else {
                ChooseMapMessage.isLoading = false
                ChooseMapMessage.pageNum = ChooseMapMessage.pageNum - 1
                if (ChooseMapMessage.pageNum > 1) {
                    ChooseMapMessage.pageNum = 1
                }
            }
        },
        error: function (req) {
            $('.createRoomlist-modal-tablelist-scroll-end').removeClass('hide')
            $('.createRoomlist-modal-tablelist-scroll-end').text(req.errorMsg)
        }
    })
}
// 初始化请求
function MapInitAjax() {
    ChooseMapMessage.pageNum = 1
    let url = '/Modal/HomeGetMapList'
    let data = {
        key: ChooseMapMessage.key,
        label: ChooseMapMessage.label,
        // model: ChooseMapMessage.intype,
        pageNum: ChooseMapMessage.pageNum,
        pageSize: ChooseMapMessage.pageSize,
        sort: ChooseMapMessage.sort,
        asc: ChooseMapMessage.asc,
        playerNum: ChooseMapMessage.playerNum,
        hot: ChooseMapMessage.hot,
        newest: ChooseMapMessage.newest
    }
    if (data.label == "mycollection") {
        data.label = ""
        data.collect = true
    } else {
        data.collect = false
    }
    if (ChooseMapMessage.isPrivate) {
        data.isPrivate = 1
        data.label = ""
        $('.createRoomlist-modal-tablelist-thead-private').removeClass('hide')
        $('.createRoomlist-modal-tablelist-thead-normal').addClass('hide')
    } else {
        $('.createRoomlist-modal-tablelist-thead-private').addClass('hide')
        $('.createRoomlist-modal-tablelist-thead-normal').removeClass('hide')
    }

    $.controlAjax({
        type: "get",
        url: url,
        data: data,
        showerror: true,
        success: function (res) {
            $('.createRoomlist-modal-tablelist-scroll').off('scroll')
            $('.createRoomlist-modal-tablelist-scroll').scrollTop(0)
            var getlist = res.data.list
            getlist.map((items) => {
                items.activityNum = formatMapHot(items.activityNum)
                items.name = items.name + '&&||' + items.enName
            })
            setTimeout(() => {
                if (getlist.length < ChooseMapMessage.pageSize) {
                    $('.createRoomlist-modal-tablelist-scroll-end').addClass('hide')
                    $('.createRoomlist-modal-tablelist-scroll').off('scroll')
                } else {
                    $('.createRoomlist-modal-tablelist-scroll-end').removeClass('hide')
                    changeMapPage()
                }
            }, 300)
            // if (ChooseMapMessage.isPrivate) {
            //     for (let i = 0; i < getlist.length; i++) {
            //         getlist[i].name = getlist[i].mapName
            //     }
            // }
            ChooseMapMessage.localList = getlist
            ChooseMapMessage.chooseIndex = 0
            ChooseMapMessage.chooseItem = ChooseMapMessage.localList[ChooseMapMessage.chooseIndex]
            $('.createRoomlist-modal-tablelist-scroll').attr("class", "createRoomlist-modal-tablelist-scroll " + ChooseMapMessage.label);
            $('#createRoomlist-modal-tablelist-tbody').html(template('createRoomlist-modal-tablelist-tbody-script', { isadd: false, list: getlist, isprivate: ChooseMapMessage.isPrivate, label: ChooseMapMessage.label }))
            $('#createRoomlist-modal-tablelist-tbody tr').eq(ChooseMapMessage.chooseIndex).addClass('active')
            if (ChooseMapMessage.chooseItem) {
                ChooseMapMessage.chooseItem.Jmodels = []
                ChooseMapMessage.chooseItem.models.map((models) => {
                    comOption.optionsMode.map((itemmodels) => {
                        if (models == itemmodels.code) {
                            ChooseMapMessage.chooseItem.Jmodels.push(itemmodels)
                        }
                    })
                })
                ChooseMapMessage.chooseItem.isPrivate = ChooseMapMessage.isPrivate
                let imgsize = '?x-oss-process=image/resize,w_600'
                if (ChooseMapMessage.chooseItem.imageUrl.indexOf(imgsize) == -1 && ChooseMapMessage.chooseItem.imageUrl.indexOf('data:image') == -1) {
                    ChooseMapMessage.chooseItem.imageUrl = ChooseMapMessage.chooseItem.imageUrl + imgsize
                }
                oneImgCache(ChooseMapMessage.chooseItem.imageUrl).then((Cachereturn) => {
                    ChooseMapMessage.chooseItem.localimageUrl = Cachereturn
                    $('.createRoomlist-details').html(template('createRoomlist-details-script', ChooseMapMessage.chooseItem))
                    $('[data-toggle="tooltip"]').tooltip()
                    $('.createRoom-choosemodal-item').eq(0).click()
                    $('.createRoom-choosemodal-item').eq(0).find('input').click()
                })
            } else {
                $('.createRoomlist-details').html(template('createRoomlist-details-script', { isempty: true }))
            }

        },
        error: function (req) {
            $('.createRoomlist-modal-tablelist-scroll-end').removeClass('hide')
            $('.createRoomlist-modal-tablelist-scroll-end').text(req.errorMsg)
        }
    })
}
function mapCreateChoose() {
    // ipcRenderer.send('Main', {
    //     msgType: "ChangeWindowSize",
    //     jsonInfo: {
    //         window: 'createroom',
    //         width: 518,
    //         height: 394,
    //         isCenter: true
    //     }
    // });
    $('#createRoomModal .modal-dialog').removeClass('createRoomwidth')
    $('.createRoom-gameVersion-div').removeClass('disabled-check')
    if (ChooseMapMessage.chooseItem.gameVersion == 2) {
        $('.createRoom-gameVersion-div')[0].click()
    } else {
        for (var i = 0; i < $('.createRoom-gameVersion-div').length; i++) {
            if ($('.createRoom-gameVersion-div').eq(i).attr('data-type') == ChooseMapMessage.chooseItem.gameVersion) {
                $('.createRoom-gameVersion-div').eq(i).removeClass('disabled-check')
                $('.createRoom-gameVersion-div').eq(i).click()
            } else {
                $('.createRoom-gameVersion-div').eq(i).addClass('disabled-check')
            }
        }
    }
    $('.createRoomlist-modal').addClass('hide')
    $('.createRoomfirst-modal').removeClass('createRoom-hide')
    $('#createRoom-inMap').html(template('createRoom-inMap-script', ChooseMapMessage.chooseItem))
    createMessage.localimageUrl = ChooseMapMessage.chooseItem.localimageUrl
    createMessage.activeMap = ChooseMapMessage.chooseItem.name
    createMessage.maxPlayers = ChooseMapMessage.chooseItem.maxPlayerNum
    createMessage.mapSha1 = ChooseMapMessage.chooseItem.mapSha1 ? ChooseMapMessage.chooseItem.mapSha1 : ChooseMapMessage.chooseItem.shA1
    createMessage.model = ChooseMapMessage.intype
    createMessage.isDownload = ChooseMapMessage.chooseItem.isDownload
    createMessage.gold = ChooseMapMessage.chooseItem.gold
    createMessage.mapUrl = ChooseMapMessage.chooseItem.mapUrl
    createMessage.imageUrl = ChooseMapMessage.chooseItem.imageUrl ? ChooseMapMessage.chooseItem.imageUrl : null
    createMessage.supportedVersion = ChooseMapMessage.chooseItem.supportedVersion
    if (ChooseMapMessage.chooseItem.labels && ChooseMapMessage.chooseItem.labels.length > 0) {
        createMessage.labels = ChooseMapMessage.chooseItem.labels.join('·')
    } else {
        createMessage.labels = "-"
    }
    // 版本
    if (ChooseMapMessage.chooseItem.supportedVersion != "") {
        let supportedVersion = ChooseMapMessage.chooseItem.supportedVersion.split(',')
        for (var i = 0; i < $('.createRoom-gameVersion-div').length; i++) {
            if (supportedVersion.indexOf($('.createRoom-gameVersion-div').eq(i).attr('data-type')) == -1) {
                $('.createRoom-gameVersion-div').eq(i).addClass('disabled-check')
            } else {
                $('.createRoom-gameVersion-div').eq(i).removeClass('disabled-check')
            }
        }
    }
    for (var i = 0; i < $('.createRoom-gameVersion-div').length; i++) {
        if (!$('.createRoom-gameVersion-div').eq(i).hasClass('disabled-check') && !$('.createRoom-gameVersion-div').eq(i).hasClass('no-choose')) {
            $('.createRoom-gameVersion-div').eq(i).click()
            break
        }
    }
    $('.createRoom-modal-btn').removeClass('hide')
    $('.createRoom-modal-btn-downloading').addClass('hide')
    $('.createRoom-modal-btn').gxbtn('reset')
    ChooseMapMessage = {
        intype: null,
        label: null,
        pageNum: 1,
        pageSize: 15,
        chooseIndex: 0,
        localList: [],
        chooseItem: null,
        pageloading: false,
        key: "",//地图关键词
        sort: 'active',//排序字段，playerNum(最大玩家数排序)|active(活跃度排序)
        asc: false,//默认true, 正序true, 逆序false
        playerNum: null,//筛选玩家数
        hot: 0,
        newest: 0,
        isPrivate: false
    }
}
function mapDetailChoose() {
    var maxplay = ChooseMapMessage.chooseItem.maxPlayers ? ChooseMapMessage.chooseItem.maxPlayers : ChooseMapMessage.chooseItem.maxPlayerNum
    // return false
    let nogetminMapPlayer = false
    comOption.roomTypeArray.map((item) => {
        if (item.code == newdetailMessage.detailsData.roomType) {
            if (item.minMapPlayer > maxplay) {
                nogetminMapPlayer = true
            }
        }
    })
    if (nogetminMapPlayer) {
        showtoast({
            message: translatesrting('地图人数') + ' ' + maxplay + ' ' + translatesrting('不支持当前房间模式，请切换房间模式')
        })
        return false
    }
    var reallyUserNum = 0
    var mapSha1 = ChooseMapMessage.chooseItem.mapSha1 ? ChooseMapMessage.chooseItem.mapSha1 : ChooseMapMessage.chooseItem.shA1
    var maxplay = ChooseMapMessage.chooseItem.maxPlayers ? ChooseMapMessage.chooseItem.maxPlayers : ChooseMapMessage.chooseItem.maxPlayerNum
    let labels = ''
    if (ChooseMapMessage.chooseItem.labels && ChooseMapMessage.chooseItem.labels.length > 0) {
        labels = ChooseMapMessage.chooseItem.labels.join('·')
    } else {
        labels = "-"
    }
    $('#modalChangeMap').modal('hide')
    ipcRenderer.send('Main', {
        msgType: "ChangeMap",
        jsonInfo: {
            mapName: ChooseMapMessage.chooseItem.name,
            mapGameMode: ChooseMapMessage.intype,
            mapSha1: mapSha1,
            imageUrl: ChooseMapMessage.chooseItem.imageUrl,
            label: labels
        }
    });
    ChooseMapMessage = {
        intype: null,
        label: null,
        pageNum: 1,
        pageSize: 15,
        chooseIndex: 0,
        localList: [],
        chooseItem: null,
        pageloading: false,
        key: "",//地图关键词
        sort: 'active',//排序字段，playerNum(最大玩家数排序)|active(活跃度排序)
        asc: false,//默认true, 正序true, 逆序false
        playerNum: null,//筛选玩家数
        hot: 0,
        newest: 0
    }
}
var reportMessage = {
    mapName: null,
    mapId: null,
    reasonId: 1
}
// 点击举报
$(document).on('click', '.J_showreportModal', function () {
    reportMessage.reasonId = 1
    reportMessage.mapName = ChooseMapMessage.chooseItem.name
    reportMessage.mapId = ChooseMapMessage.chooseItem.id
    $('#reportModal-content').html(template('reportModal-content-script', reportMessage))
    $('#reportModal #J_reportSure').text(translatesrting('举报'))
    $('#reportModal .btn-style-ash').text(translatesrting('取消'))
    $('#reportModal').modal('show')
    $('#reportModal-content .gx-menu').gxmenu({
        height: "20px",
        top: "24px",
        clickHide: true//点击内部是否消失
    }, function (e) {

    })
})
// 举报选项
$(document).on('click', '.J_choosereport', function () {
    var $this = $(this)
    reportMessage.reasonId = $this.attr('data-code')
    $('.reportModal-select .reportModal-select-text').text($this.html())
})
// 举报
$(document).on('click', '#J_reportSure', function () {
    $.controlAjax({
        type: "get",
        url: '/Modal/ReportMapInfo',
        showerror: true,
        data: {
            reasonId: reportMessage.reasonId,
            mapId: reportMessage.mapId
        },
        success: function (res) {
            $('#reportModal').modal('hide')
            spop({
                template: '举报成功',
                autoclose: 3000,
                style: 'success'
            });
        },
        error: function (req) {

        }
    })
})
// 私密
$(document).on('click', '.J_chooseMapPrivate', function () {
    var $this = $(this)
    $this.siblings().removeClass('active')
    $this.addClass('active')
    let code = $this.attr('data-code')
    if (code == 'private') {
        ChooseMapMessage.isPrivate = true
        ChooseMapMessage.label = 'private'
    } else {
        ChooseMapMessage.isPrivate = false
        ChooseMapMessage.label = 'mycollection'
    }
    MapInitAjax()
})