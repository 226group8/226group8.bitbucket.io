// 显示日期
function formatDate(date, fmt) {
    var date = new Date(date * 1000),
        fmt = fmt || 'yyyy-MM-dd hh:mm:ss',
        o = {
            "M+": date.getMonth() + 1,                 //月份 
            "d+": date.getDate(),                    //日 
            "h+": date.getHours(),                   //小时 
            "m+": date.getMinutes(),                 //分 
            "s+": date.getSeconds()                 //秒 
        }
        ;

    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    }

    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }

    return fmt;
}
// 计算时间差
function formatDateBefor(time, systime) {
    var Difference = Number(systime) - Number(time)
    var fmt = ""
    if (Difference < 60) {
        fmt = '刚刚'
    } else if (Difference < 3600) {
        fmt = Math.floor(Difference / 60) + translatesrting('分钟前')
    } else if (Difference < 86400) {
        fmt = Math.floor(Difference / 3600) + translatesrting('小时前')
    } else if (Difference < 2592000) {
        fmt = Math.floor(Difference / 86400) + translatesrting('天前')
    } else if (Difference < 31104000) {
        fmt = Math.floor(Difference / 2592000) + translatesrting('月前')
    } else {
        fmt = Math.floor(Difference / 31104000) + translatesrting('年前')
    }
    return fmt;
}
// 计算时间差
function formatDateBeforDay(time, systime) {
    var Difference = Number(systime) - Number(time)
    var fmt = ""
    if (Difference < 60) {
        fmt = translatesrting('今天')
    } else if (Difference < 3600) {
        fmt = translatesrting('今天')
    } else if (Difference < 86400) {
        fmt = translatesrting('今天')
    } else if (Difference < 259200) {
        fmt = Math.floor(Difference / 86400) + translatesrting('天前')
    } else {
        let oDate = new Date(time * 1000)
        let oYear = oDate.getFullYear()//年
        let oMonth = oDate.getMonth() + 1//月
        let oDay = oDate.getDate()//日
        fmt = oYear + '/' + oMonth + '/' + oDay
    }
    return fmt;
}
// 显示时间
function formatTime(time) {
    var d = Math.floor(time / 86400)
    var h = Math.floor((time % (3600 * 24)) / 3600)
    if (h < 10) {
        h = '0' + h
    }
    var m = Math.floor(((time % (3600 * 24)) % 3600) / 60)
    if (m < 10) {
        m = '0' + m
    }
    var s = Math.floor((((time % (3600 * 24)) % 3600) % 60))
    if (s < 10) {
        s = '0' + s
    }
    if (d > 0) {
        return d + '天' + h + '小时'
    } else {
        if (h == '00') {
            return m + ':' + s
        } else {
            return h + ':' + m + ':' + s
        }
    }
}
// 显示时间
function formatTimeEnd(time) {
    var d = Math.floor(time / 86400)
    var h = Math.floor((time % (3600 * 24)) / 3600)
    if (h < 10) {
        h = '0' + h
    }
    var m = Math.floor(((time % (3600 * 24)) % 3600) / 60)
    if (m < 10) {
        m = '0' + m
    }
    var s = Math.floor((((time % (3600 * 24)) % 3600) % 60))
    if (s < 10) {
        s = '0' + s
    }
    if (d > 0) {
        return '【' + d + '】天【' + h + '】小时'
    } else if (h > 0) {
        return '【' + h + '】小时'
    } else {
        return '【' + m + '】分钟'
    }
}
// 显示时间 匹配正计时
function formatMatchTime(time) {
    var h = Math.floor((time % (3600 * 24)) / 3600)
    if (h < 10) {
        h = '0' + h
    }
    var m = Math.floor(((time % (3600 * 24)) % 3600) / 60)
    if (m < 10) {
        m = '0' + m
    }
    var s = Math.floor((((time % (3600 * 24)) % 3600) % 60))
    if (s < 10) {
        s = '0' + s
    }
    if (h != '00') {
        return h + ':' + m + ':' + s
    } else {
        return m + ':' + s
    }
}
function formatImageSmell(url) {
    return url.split(".png")[0] + '_small' + '.png'
}
// 粉丝数
function formatFuns(num) {
    let string = ''
    if (num > 10000) {
        if ((num / 10000).toFixed(1).split('.')[1] != 0) {
            string = String((num / 10000).toFixed(1) + 'W')
        } else {
            string = String((num / 10000).toFixed(1).split('.')[0] + 'W')
        }
    } else {
        string = num
    }
    return string
}
var translatesrtinglocalStorage = localStorage.getItem('LBWlanguage') || 'zh'
function translatesrting(string, type) {
    if (string == '') {
        return ''
    }
    if (string == null) {
        return null
    }
    if (type == 'GH') {
        if (localStorage.getItem('GHharmonious')) {
            let reg = new RegExp("共和国之辉", "g");
            let regs = new RegExp("共辉", "g");
            if (string) {
                let stringharmonious = string.replace(reg, 'GH')
                stringharmonious = stringharmonious.replace(regs, 'GH')
                return stringharmonious
            } else {
                return ''
            }
        } else {
            return string
        }
    }
    if (translatesrtinglocalStorage == 'zh') {
        if (!translateJson[string] && translateJsonNeed[string] != 1) {
            translateJsonNeed[string] = "1"
            localStorage.setItem('translateJsonNeed', JSON.stringify(translateJsonNeed))
        }
        if (localStorage.getItem('GHharmonious')) {
            let reg = new RegExp("共和国之辉", "g");
            let regs = new RegExp("共辉", "g");
            if (string) {
                let stringharmonious = string.replace(reg, 'GH')
                stringharmonious = stringharmonious.replace(regs, 'GH')
                return stringharmonious
            } else {
                return ''
            }
        }
        return string
    } else if (translateJson[translatesrtinglocalStorage]) {
        if (translateJson[translatesrtinglocalStorage][string]) {
            return translateJson[translatesrtinglocalStorage][string]
        } else {
            if (!translateJson[translatesrtinglocalStorage][string] && translateJsonNeed[string] != 1) {
                translateJsonNeed[string] = "1"
                localStorage.setItem('translateJsonNeed', JSON.stringify(translateJsonNeed))
            }
            return string
        }
    } else {
        translateJsonNeed[string] = "1"
        localStorage.setItem('translateJsonNeed', JSON.stringify(translateJsonNeed))
        return string
    }
}
function translateCss(string) {
    if (translatesrtinglocalStorage == 'en') {
        if (translateJsonCss[string]) {
            return translateJsonCss[string]
        } else {
            return string
        }
    } else {
        return string
    }
}
function formattoFixed(num, wei) {
    let a = Number(num * 100);
    let b = a.toFixed(wei);
    return b
}
function formatapngfun(url, size) {
    let a = url
    if (a) {
        if (url.indexOf('.apng') != -1) {
            a = url
        } else {
            a = url + '?x-oss-process=image/resize,w_' + size
        }
    }
    return a
}
//热度
function formatMapHot(num) {
    let getnum = Number(num)
    let sendnum = 0
    if (getnum > 1000000) {
        sendnum = getnum / 1000000
        sendnum = sendnum.toFixed(1)
        sendnum = sendnum + 'M'
    } else if (getnum > 1000) {
        sendnum = getnum / 1000
        sendnum = sendnum.toFixed(1)
        sendnum = sendnum + 'K'
    } else {
        sendnum = getnum
    }
    return sendnum
}
function formatMapName(name) {
    let getname = name.split('&&||')
    if (translatesrtinglocalStorage == 'en' && getname[1] && getname[1] != '' && getname[1] != ' ' && getname[1] != 'null') {
        return getname[1]
    } else {
        return getname[0]
    }
}
template.helper('formatImageSmell', formatImageSmell);
template.helper('formatDate', formatDate);
template.helper('formatTime', formatTime);
template.helper('translate', translatesrting);
template.helper('translateCss', translateCss);
template.helper('formatDateBeforDay', formatDateBeforDay);
template.helper('formattoFixed', formattoFixed);
template.helper('formatFuns', formatFuns);
template.helper('formatapngfun', formatapngfun);
template.helper('formatMapName', formatMapName);

var translateJsonNeed = {}
if (localStorage.getItem('translateJsonNeed')) {
    translateJsonNeed = JSON.parse(localStorage.getItem('translateJsonNeed'))
}
var translateJson = {}

var translateJsonCss = {
    'setting-left': 'setting-left-en',
    'modal-content': 'modal-content-en',
    'rule.png': 'rule-en.png',
    'rank-list.png': 'rank-list-en.png',
    'chooseCountry.png': 'chooseCountry-en.png',
    'gameIcon.png': 'gameIcon-en.png',
    'win.png': 'win-en.png',
    'fail.png': 'fail-en.png',
    'hall-header-logo': 'hall-header-logo-en'
}
var languageValuelocal = {
    "开启鼠标连点器": "Multiclicker",
    "状态": "States",
    "快速匹配": "Quick Game",
    "Ares测试运行": "Ares Test",
    "准备": "Ready",
    "语音发送成功": "Success",
    "您确定消耗": "Are you sure to consume",
    "分钟后未开始游戏，系统将自动解散房间": "minutes to START, or room will be disbanded.",
    "可更新到最新版本": "Can be updated to the latest version",
    "生产海军": "Ships Built",
    "准备游戏": "Ready",
    "排长": "LT",
    "操作": "Operate",
    "作者": "Author",
    "最新地图": "New",
    "金币不足": "Insufficient Gold",
    "您已被禁用作者功能": "You have been prohibited",
    "韩国": "Korea",
    "初始部队数": "Starting Units",
    "申请理由：往期作品，擅长玩法": "Please input your reason for application. e.g. Good or famous map.",
    "服务条款": "Terms of Service",
    "邪恶联盟": "Unholy Alliance",
    "返回房间": "Return to Your Room",
    "举报": "Report",
    "取消准备": "Cancel",
    "登录": "Sign In",
    "总": "Total",
    "服务器开小差，请稍后再试下哦~": "Network Failed. Please retry later",
    "模式选择": "Select Mode",
    "作者名": "Author",
    "我的奖章": "My Medal",
    "举报类型不能为空": "Report type is empty",
    "月前": " Monthes",
    "首场比赛": "First Match",
    "删除": "Delete",
    "使用次数": "Used",
    "国家": "Country",
    "立即更新": "Update",
    "金币详情": "Gold Details",
    "房间公告": "Notice",
    "地图上传": "Upload",
    "旅长": "BG",
    "等待加入": "Open",
    "上传新地图": "Upload new maps",
    "玩家数": "Players",
    "举报他！": "Report",
    "房间名不得为空": "Room Name Can't Be Empty",
    "申请成为平台作者": "Apply for ME",
    "排行榜": "Trending",
    "地图文件": "File",
    "不超过140个汉字": "140 characters limited",
    "生产步兵": "Infantry Built",
    "离线": "Offline",
    "请设置新密码": "Set New Password",
    "地图下载中": "Downloading",
    "利比亚": "Libya",
    "余额": "Remain",
    "点击": "Click",
    "平台音效": "SFX",
    "平台延迟": "Ping",
    "掉线率": "DC Rate",
    "取消": "Cancel",
    "忘记密码": "Forget Password",
    "简单电脑": "Easy AI",
    "师长": "MG",
    "暂未查询到比赛记录，马上去玩一盘吧": "No Match Record Found.",
    "生涯": "Career",
    "显示模式": "Display",
    "点这里可以返回房间": "Return to Your Room",
    "涉及敏感政治内容": "Sensitive / political contents invloved",
    "观战": "Observer",
    "更新完成自动重启": "Automatic restart after update",
    "胜率最高地图": "Most winning map",
    "地图作者中心": "Map Center",
    "全部": "All",
    "团队联盟": "Team Alliance",
    "下载游戏": "Download",
    "生产建筑": "Built",
    "昵称不能为空": "Nickname can not be blank",
    "多核CPU": "Multi-core",
    "匹配模式将会从以下地图中随机选择一张进行对战": "Casual maps will be random choose from following",
    "请输入QQ": "Input QQ number",
    "运行方式": "Launch Mode",
    "修改密码": "Password",
    "确认倒计时": "Countdown",
    "在线": "Online",
    "推荐": "Hot",
    "游戏启动测试": "Game Launch Test",
    "旧密码": "Old Password",
    "这是盗版地图": "Pirated Map",
    "独占全屏显示": "Full Screen",
    "无边框窗口模式": "Borderless Windowed",
    "自定义": "Custom",
    "兰博玩对战平台": "Rambo Arena",
    "匹配暂不开放": "Closed",
    "游戏聊天": "Chat",
    "天梯": "Ladder",
    "请输入手机号": "Phone Number",
    "游戏内语音": "Voices",
    "初始部队": "Units",
    "立即注册": "Sign Up",
    "设置为私有地图后，仅有您自己可以使用该地图创建自定义房间": "After setting as a private map only you can use the map to create a custom room",
    "选择文件": "Select file",
    "请输入游戏路径": "Enter Ra2.exe Path",
    "地图描述": "Description",
    "您的申请正在审核中": "Your application is verifying",
    "建造数": "Units Built",
    "备注昵称": "Nickname",
    "胜利": "Win",
    "地图版本": "Version",
    "规则": "Rule",
    "法国": "France",
    "工程师免疫警犬": "No Dog Engi Kills",
    "比赛详情": "Details",
    "的房间": "'s Room",
    "修改备注": "Notes",
    "邮箱": "E-Mail",
    "游戏部队": "Starting Units",
    "头像框": "Frame",
    "盟友建造场旁建设": "Build Off Ally",
    "输入首字母或关键字": "Enter the initials or keywords",
    "新手引导": "Guide",
    "工具箱": "Crates",
    "兰博玩平台将尽快审核并通知您处理结果，请耐心等待": "We will verify and notice you result, please wait",
    "自定义胜率": "Win Rate",
    "请输入房间名": "Enter Room Name",
    "段位显示": "Show Rank",
    "胜": "Win",
    "队伍": "Team",
    "立刻申请": "Apply",
    "粉丝数": "Follower",
    "确定": "OK",
    "Ares检测": "Ares Test",
    "很抱歉，您的作者申请未通过": "Sorry your application failed",
    "可摧毁桥梁": "Destroyable Bridges",
    "欢迎回来": "Welcome, Commander",
    "密码": "Password",
    "流局的比赛，不纳入天梯总场次和天梯胜率统计": "Draw matches are not included in ladder matches and win rates",
    "使用地图": "Map",
    "剩余战车": "Units Left",
    "游戏内结盟：允许游戏内玩家间互相结盟": "Ingame Allying",
    "确认": "Confirm",
    "更改地图": "Change Map",
    "游戏文件完整性检测失败": "Gamefile integrity detection failed",
    "模式": "Mode",
    "设置密码": "Password",
    "取消下载": "Cancel",
    "系统设置": "Settings",
    "无法连接服务器，请重启客户端后重试": "Can't connect to server, please restart the client",
    "删除选中": "Delete Selected",
    "使用此地图": "Confirm",
    "热门推荐": "Hot",
    "只有您一名玩家，是否开启单人模式？": "Play Single Mode?",
    "普通匹配": "Casual",
    "伊拉克": "Iraq",
    "资金": "Credits",
    "负": "Lose",
    "美国": "America",
    "地图名称": "Map Name",
    "玩家": "Player",
    "联系客服": "Contact",
    "开启": "Open",
    "很抱歉，您暂时没有地图作者权限": "Sorry, you are not ME.",
    "红警原版": "Ra2",
    "更新": "Update",
    "审核中": "Verifing",
    "对战": "Battle",
    "更新检查": "Check for update",
    "男": "Male",
    "ARES地图": "Ares Map",
    "换一换": "Refresh",
    "举报地图": "Report Map",
    "匹配": "Match",
    "游戏解压中": "Unpacking",
    "颜色": "Color",
    "短信验证码": "SMS Code",
    "修改手机号": "Phone Number",
    "请输入房间号搜索加入": "Search by Room ID",
    "还没有账号？立即注册": "Sign Up Now",
    "搜索结果": "Results",
    "请输入验证码": "CAPTCHA",
    "欢迎注册兰博玩对战平台": "Register",
    "摧毁战车": "Units Killed",
    "8人混战": "8-player",
    "游戏测试！": "Launch Testing!",
    "地图权限": "Permissions",
    "你确定保存修改吗？": "Save Changes?",
    "关闭位置": "Close",
    "您的金币不足,房间已自动解散，请玩一玩匹配或者加入别人的房间一起玩可获得大量的金币哦": "Your coins are insufficient, room has been automatically closed please play matchmaking or join other rooms to get more coins.",
    "简体中文": "Chinese",
    "房间列表": "Room List",
    "网络可能出现点问题,请稍后再试": "Network problem. Please try again later",
    "全部地图": "All maps",
    "游戏版本": "Game Version",
    "个人信息保护政策": "Privacy Policy",
    "经典对战": "Classic",
    "防守、任务类地图及单人模式玩法平台暂时不纳入胜率统计": "Defense mission maps or single-player gameplay are temporarily not included in the win rate statistics",
    "遇到问题?": "Have question?",
    "地图简介": "Map introduction",
    "暂无收藏的地图": "No Favorites",
    "游戏地图": "Game Map",
    "女": "Female",
    "连长": "Cpt",
    "您已加入游戏房间": "You've Joined a Room",
    "匿名玩家": "Anonymous",
    "筛选房间过于频繁": "Operate Too Frequently",
    "立即检测": "Check Now",
    "免费": "0",
    "版本号:": "Version:",
    "钱多多": "Wealth",
    "已复制房间号": "Room ID copied to your Clipboard",
    "首页": "Home",
    "未设置": "Not Set",
    "娱乐对战": "Fun Mode",
    "公开地图": "Public maps",
    "创建游戏": "Create Game",
    "满员": "Full",
    "返回": "Back",
    "请输入QQ号": "QQ Number",
    "游戏下载中": "Downloading",
    "选择举报原因": "Report Reason",
    "我的好友": "Friends",
    "下一步": "Next",
    "游戏即将开始": "Starting",
    "比赛概要": "Summary",
    "本地丢包": "D.C.",
    "游戏模式": "Game Mode",
    "红警2客户端完整性检测": "Ra2 Client Integrity Check",
    "开始匹配": "Start Match",
    "全部头像": "All Avatars",
    "加载中": "Loading...",
    "房间密码": "Password",
    "游戏资金": "Credits",
    "游戏下载完成": "Download Succes",
    "游戏音量设置": "Game Volume",
    "对战模式": "Battle",
    "地图": "Map",
    "通过审核的地图": "Verified",
    "快速游戏：摧毁敌方所有建筑即可获得胜利": "Short Game",
    "输入房间密码加入": "Input Room Password ",
    "高级电脑": "Hard AI",
    "生产战车": "Units Built",
    "上传图片不得大于5M": "No larger than 5M",
    "游戏内音乐": "BGM",
    "私有地图": "private",
    "一小块地": "Battlefield",
    "版本设置": "Game Version",
    "可点击地图右上角【收藏】按钮收藏": "You can add by click Favourites on Map",
    "总场次": "Matches",
    "全屏适配": "Best Adaptation",
    "版本": "Version",
    "营长": "Maj",
    "游戏天数": "Days",
    "摧毁数": "Destroyed",
    "3v3": "3v3",
    "举报成功": "Report Success",
    "天梯匹配开始前，您可以针对不同的地图，设置对应的国家。系统匹配成功后，将按照您预设的国家启动游戏。": "Please select your preset countries on each map",
    "A队": "A",
    "切换语言": "Lang",
    "小时前": " Hours",
    "APM（每分钟操作次数）": "APM",
    "请输入当前密码": "Enter The Current Password",
    "点击地图名查看地图": "Click to view the map",
    "修改消耗": "-",
    "邪恶联盟模式下，每位玩家都拥有苏/盟两国基地车（YR模式下还有尤里基地车），可以建筑除特色兵种外的相应建筑和兵种": "In Unholy Alliance mode each player has a Soviet MCV and a Alliance MCV (YR mode includes a yuri MCV) that can build everything except the special units(snipers terroists etc)",
    "放弃匹配": "Cancel",
    "天梯开放时间": "Openning Time",
    "单人": "-",
    "头像": "Avatar",
    "检查更新": "Check For Updates",
    "总司令": "GEN",
    "摧毁建筑": "Buildings Destroyed",
    "B队": "B",
    "失败": "Fail",
    "筛选房间": "Filter",
    "平台总音量": "Total",
    "海战模式下，您不能够建造陆军和空军单位。": "You cannot build ground or air units in Naval War mode.",
    "尤里": "Yuri",
    "回放": "Replay",
    "私密": "Private",
    "隐私设置": "Privacy",
    "德国": "Germany",
    "2v2": "2v2",
    "点击次数": "Number of clicks",
    "请先填写游戏路径": "Enter Ra2 Path",
    "匹配中": "Matching",
    "娱乐": "Fun",
    "重新申请": "Apply Again",
    "作战": "Battle",
    "赛前测试": "Pre-match Test",
    "最喜爱国家": "Favorite Country",
    "重新确认密码": "Confirm Password",
    "胜率": "Win Rate",
    "英国": "Britain",
    "C队": "C",
    "邀请信息已发送到自定义大厅聊天频道": "Invitation has been sent to Lobby",
    "被禁用的地图": "Banned Maps",
    "天梯匹配": "Ladder",
    "金币": "Gold",
    "观战中": "Observing",
    "删除成功": "Successful",
    "匹配失败": "Matching Failure",
    "请按正常流程关闭游戏": "Please close the game normally",
    "天前": " days",
    "年前": " Years",
    "赛前准备": "Pre-match Setting",
    "添加文件": "Add files",
    "密码房间": "Password",
    "昵称": "Nickname",
    "匹配胜率": "Win Rate",
    "发送": "Send",
    "单位花费": "Unit Cost",
    "D队": "D",
    "复制房间号": "Copy Room ID",
    "上传": "Map Editor",
    "抢箱子": "Crates Only",
    "抢地盘模式下，玩家出生在一起。需要将基地车移动到合适的位置来发育。": "In Land Rush mode players are spawned together. Players need to move MCVs to the appropriate spots to Expand.",
    "好友申请": "Request",
    "寻找比赛": "Find Match",
    "参与红色警戒2极具竞技性的比赛匹配": "Join Our Competitive Casual Game",
    "开始游戏": "Start",
    "1v1": "1v1",
    "初始资金": "Credits",
    "剩余步兵": "Infantry Left",
    "好友列表": "Friend List",
    "记住密码": "Remember Me",
    "基地重新部署：允许展开后的基地重新收为基地车": "MCV Repacks",
    "明细": "Details",
    "您已完成检测，可正常进行游戏": "Test Successful",
    "类型": "Type",
    "生死斗模式下，玩家只可以建造基本的陆战单位和国家特色单位。不能建造作战实验室。天启坦克/光棱坦克等。": "In Meat Grinder mode players can only build basic land units and special units(snipers tesla tanks etc). We cant build a battle lab or Apocalypse tanks/Prism Tanks etc.",
    "房间号": "Room ID",
    "阅读": "Read",
    "地图作者": "Author",
    "单核CPU": "Single-core",
    "作战模式下可建造所有单位。但热门官方地图北极圈在小队结盟模式下": "All units can be built in Battle mode. Except Arctic Circle",
    "超级武器：允许玩家建造/使用超级武器": "Superweapons Allowed",
    "上传自定义头像": "Upload Profile Picture",
    "审核中的地图": "Verifying Map",
    "快速加入": "Quick Join",
    "盟友建造场旁建设：允许在盟友基地旁建造建筑": "Allow Building off ally conyards",
    "随机": "Rd.",
    "游戏测试": "Launch Test",
    "古巴": "Cuba",
    "大厅喊话": "Hall Invite",
    "重新检测": "Test Again",
    "房间聊天": "Room Chat",
    "游戏启动失败，请检查后重试": "Game Launch Failed, Please Try Again",
    "列表模式": "List",
    "擅长使用国家": "Preferred",
    "删除好友": "Delete",
    "快速游戏": "Short Game",
    "时间": "Time",
    "地图选择": "Change",
    "抢地盘": "Land Rush",
    "最大游戏人数": "Max Players",
    "不允许输入除-_以外的特殊字符": " Special characters other than-_ are not allowed",
    "巨富模式下，玩家不能建造矿场和采矿车，需要用工程师占领油井来获取经济。": "In Megawealth mode players cant build miners or refs but need to capture oil derricks with engineers to gain economy.",
    "平台背景音乐": "BGM",
    "天梯模式将会从以下地图中随机选择一张进行对战": "Ladder Map Pool",
    "苏俄": "Russia",
    "排位赛规则": "Ladder Matching Rules",
    "游戏内音效": "SFX",
    "金币修改昵称吗？": " Gold to change Nickname？",
    "退出确认": "Leave Now?",
    "游戏直播": "Streamer",
    "比赛编号": "Match ID",
    "加入": "Join",
    "关闭": "Closed",
    "修复": "Repair",
    "团长": "Col",
    "注：匹配玩法采用统一游戏规则，玩家无法自行修改。": "Players can not modify the match rules",
    "创建房间": "Create Room",
    "更新检测中": "Updating",
    "当前连接已断开，请关闭平台后启动游戏客户端": "Network disconnect, please restart the client",
    "剩余空军": "Planes Left",
    "常用国家": "Preferred",
    "先按下Shift，再按鼠标左键，即开始连点": "Hold Shift and click LMB",
    "是否删除此地图": "Delete Map",
    "匹配玩法地图统一为作战模式，玩家可以建造所有单位。采用统一地图池，玩家匹配成功后系统将在地图池中随机选择一张地图，地图池不定期更新。": "All map from QM is Battle RA2 player could build and use all units.QM use unified map pool.The system will choose a random map when establish* a connection with your opponent map pool will be updated from time to time",
    "大厅邀请": "Invite",
    "邀请一起": "Invite",
    "单人匹配": "Solo",
    "知道了": "Close",
    "忽略全部": "Ignore All",
    "性别": "Gender",
    "巨富": "Megawealth",
    "注册账号": "Register",
    "封面图": "Cover",
    "原版红警": "Ra2",
    "手机号": "Phone Number",
    "被举报次数": "Reported times",
    "天梯排行榜": "Leaderboard",
    "预设国家": "Preset Country",
    "地图标题/缩略图存在违规内容": "Map title/thumbnail has illegal content",
    "个人生涯": "Career",
    "跳过结束界面": "Skip the Scoreboard",
    "断开": "D.C.",
    "好友搜索": "Search",
    "工具箱：战场上随机生成工具箱": "Crates Appear",
    "连点器": "Multiclicker",
    "注册即代表您同意": "I Agree",
    "点击重试": "Retry",
    "剩余建筑": "Buildings Left",
    "您确定要退出当前房间吗？": "Leave the Room Now？",
    "找回密码": "Forgot Password",
    "混战": "Combat",
    "未找到快速加入的房间": "No Quick Match Room",
    "存在隐秘的不公平现象(作弊魔改图)": "Hidden injustices (rigged maps/cheat involved)",
    "任务": "Missions",
    "人员花费": "Crew Cost",
    "保存": "Save",
    "赛前准备！": "Pre-match Preparation",
    "流局": "Draw",
    "本局比赛设置": "Settings",
    "您确定要退出兰博玩对战平台吗？": "Exit Rambo Arena？",
    "游戏路径": "Ra2 Path",
    "游戏设置": "Game Settings",
    "房间编号": "ID",
    "不接受此文件类型": "File type is not accepted",
    "请输入新手机号": "New Mobile Phone Number",
    "生死斗": "Meat Grinder",
    "公开": "Public",
    "最新活动": "Latest Events",
    "修改后重新提交": "Resubmit",
    "新密码": "New Password",
    "离开/解散": "Leave",
    "已就绪": "Ready",
    "版本号": "Version",
    "场次": "Matches",
    "摧毁步兵": "Infantry Killed",
    "已确定": "Confirmed",
    "大厅": "Lobby",
    "请告诉我您要搜索的区号": "Please input the country code",
    "您的网络出了点问题，需关闭客户端重新打开。": "Network issue, please restart the client",
    "窗口模式": "Windowed",
    "正常": "Normal",
    "超级武器": "Superweapons",
    "选择地图": "Change",
    "擅长国家": "Preferred",
    "放弃比赛": "Reject",
    "输入房间密码": "Room Password",
    "房间名称": "Name",
    "支持模式": "Modes",
    "该地图无法正常启动游戏": "Map Error, Launch Game Failed",
    "变动后数量": "Remain",
    "特殊工程师": "Multi Engi",
    "可加入玩家数": "Max Players",
    "刚刚": "Just now",
    "屏幕分辨率": "Resolution",
    "军长": "LTG",
    "请勿频繁切换出生点": "Change spawn too frequently",
    "版本更新中，请耐心等待": "Updating...",
    "分钟前": " Minutes",
    "基本资料": "Information",
    "自动搜索": "Automatic",
    "收藏": "Favorites",
    "搜索玩家": "Search",
    "本地延迟": "Ping",
    "热度": "Trending",
    "测试结果": "Test Result",
    "小队结盟包含作战不存在的地图，多数是2v2，3v3，4v4地图。地图的特点是同盟玩家间隔较近。例如北极圈地图": "Team Alliance mode include maps that do not exist in Battle mode mostly 2V2 3V3 and 4V4 maps. The maps are characterized by close proximity between allied players.(for example arctic ciecle)",
    "超级AI": "Brutal AI",
    "保存成功": "Saved",
    "上传图片不得大于3张": "Less than 3 pictures",
    "摧毁空军": "Planes Killed",
    "设置": "Settings",
    "邀请": "Invite",
    "中级电脑": "Medium AI",
    "提示": "Notice",
    "请再次输入新密码": "Please Enter The New Password Again",
    "剩余倒计时": "Countdown",
    "全部头像框": "All Frames",
    "天梯胜率": "Win Rate",
    "我的收藏": "Favorites",
    "基地重新部署": "MCV Repacks",
    "建筑花费": "Structure Cost",
    "匹配开放时间": "Openning Time",
    "短信发送成功": "Success",
    "您当前已是最新版本": "Up to Date",
    "游戏内结盟": "Ingame Allying",
    "未定级": "N.A.",
    "在此处输入聊天内容": "Type here to chat...",
    "地图附件": "Attachment",
    "辅助功能": "Tools",
    "行为": "Behavior",
    "消耗": "Consume",
    "机器人": "AI",
    "防守": "Survival",
    "海战": "Naval War",
    "添加好友": "Add Friends",
    "平均FPS": "Avg FPS",
    "尤里复仇": "Yuri's Revenge",
    "邮箱格式输入不正确": "E-Mail format is incorrect",
    "开始倒计时": "Start Countdown",
    "金币变动": "Gold Change",
    "今天": "Today",
    "搜索好友": "Search",
    "游戏中": "In Game",
    "赛季全部奖章": "All Medals",
    "载入界面不显示位置": "No Spawn Previews",
    "放大": "Details",
    "地图模式": "Map View",
    "全选": "All",
    "游戏下载失败": "Download Failure",
    "语音验证码": "Voice Code",
    "出生点": "Spawn",
    "天梯玩法地图统一为作战模式，玩家可以建造所有单位。采用统一地图池，玩家匹配成功后系统将在地图池中随机选择一张地图，地图池不定期更新。": "Battle mode is preset in Ladder. Maps will be updated by time",
    "剩余海军": "Ships Left",
    "生产空军": "Planes Built",
    "消息通讯": "Msg.",
    "取消搜索": "Cancel",
    "账户管理": "Manage Profile",
    "Ares重新检测": "Ares Test",
    "修改地图": "Change",
    "存在问题？举报地图": "Report Map",
    "正在加入房间中": "Joining Room...",
    "尤里的复仇": "Yuri's Revenge",
    "立即前往": "Go",
    "捡箱子模式下，您只能用初始部队捡起地图上随机出现的箱子获得作战单位，不能建造和生产": "In Crates Only mode you can only pick up random crates on the map with your initial unit to gain troops you cant build or produce units",
    "房主": "Host",
    "胜负": "Result",
    "立即加入": "Join Now",
    "请输入兰博玩昵称": "Rambo Arena Nickname",
    "主题皮肤": "Theme",
    "平台音量设置": "Arena Volume",
    "查看更多": "More",
    "音量设置": "Volume",
    "您的网络可能出现了点问题，请重启客户端": "Network problem，please restart client",
    "最近游戏玩家": "Recent Played",
    "请输入密码": "Please Enter The Password",
    "摧毁海军": "Ships Killed"
}

translateJson = JSON.parse(localStorage.getItem("languageValue")) || { en: languageValuelocal }