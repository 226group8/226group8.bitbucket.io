/**
 * jQuery plugin for Pretty looking right click context menu.
 *
 * Requires popup.js and popup.css to be included in your page. And jQuery, obviously.
 *
 * Usage:
 *
 *   $('.something').contextPopup({
 *     title: 'Some title',
 *     items: [
 *       {label:'My Item', icon:'/some/icon1.png', action:function() { alert('hi'); }},
 *       {label:'Item #2', icon:'/some/icon2.png', action:function() { alert('yo'); }},
 *       null, // divider
 *       {label:'Blahhhh', icon:'/some/icon3.png', action:function() { alert('bye'); }},
 *     ]
 *   });
 *
 * Icon needs to be 16x16. I recommend the Fugue icon set from: http://p.yusukekamiyamane.com/ 
 *
 * - Joe Walnes, 2011 http://joewalnes.com/
 *   https://github.com/joewalnes/jquery-simple-context-menu
 *
 * MIT License: https://github.com/joewalnes/jquery-simple-context-menu/blob/master/LICENSE.txt
 */
jQuery.fn.contextPopup = function (menuData) {
  // Define default settings
  var settings = {
    contextMenuClass: 'contextMenuPlugin',
    signClass: 'contextMenuPlugin',
    gutterLineClass: 'gutterLine',
    headerClass: 'header',
    seperatorClass: 'divider',
    title: '',
    items: [],
    body: document.body
  };

  // merge them
  $.extend(settings, menuData);

  // Build popup menu HTML
  function createMenu(e) {
    if ($("." + settings.contextMenuClass).length > 0) {
      $("." + settings.contextMenuClass).remove()
    }
    var menu = $('<ul class="' + settings.contextMenuClass + " " + settings.signClass + " " + '"><div class="' + settings.gutterLineClass + '"></div></ul>')
      .appendTo(settings.body);
    if (settings.title) {
      $('<li class="' + settings.headerClass + '"></li>').text(settings.title).appendTo(menu);
    }
    settings.items.forEach(function (item) {
      if (item) {
        var rowCode = '<li><a href="#"><span></span></a></li>';
        // if(item.icon)
        //   rowCode += '<img>';
        // rowCode +=  '<span></span></a></li>';
        var row = $(rowCode).appendTo(menu);
        if (item.icon) {
          var icon = $('<img>');
          icon.attr('src', item.icon);
          icon.insertBefore(row.find('span'));
        }
        row.find('span').text(item.label);
        if (item.action) {
          row.find('a').click(function () { item.action(e); });
        }
      } else {
        $('<li class="' + settings.seperatorClass + '"></li>').appendTo(menu);
      }
    });
    menu.find('.' + settings.headerClass).text(settings.title);
    return menu;
  }

  // On contextmenu event (right click)
  this.bind('contextmenu', function (e) {
    var menu = createMenu(e)
      .show();
    if (settings.body != document.body) {
      var left = e.pageX - settings.body.offset().left + 10, /* nudge to the right, so the pointer is covering the title */
        top = e.pageY - settings.body.offset().top + 5;
      if (top + menu.height() >= settings.body.height()) {
        top -= menu.height();
      }
      if (left + menu.width() >= settings.body.width()) {
        left -= menu.width();
      }
    } else {
      var left = e.pageX
      top = e.pageY + 5;
      if (top + menu.height() >= $('body').height()) {
        top -= menu.height();
      }
      if (left + menu.width() >= $('body').width()) {
        left -= menu.width();
      }
    }

    // Create and show menu
    menu.css({ zIndex: 1000001, left: left, top: top })
      .bind('contextmenu', function () { return false; });

    // Cover rest of page with invisible div that when clicked will cancel the popup.
    var bg = $(document)
      .bind('contextmenu click', function () {
        // If click or right click anywhere else on page: remove clean up.
        bg.remove();
        menu.remove();
      });

    // When clicking on a link in menu: clean up (in addition to handlers on link already)
    menu.find('a').click(function () {
      bg.remove();
      menu.remove();
    });
    // Cancel event, so real browser popup doesn't appear.
    return false;
  });

  return this;
};

