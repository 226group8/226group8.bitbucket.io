
//存储方法
function saveMessage(name, message) {
    if (self.frameElement && self.frameElement.tagName == "IFRAME") {
        parent.saveMessage(name, message)
    } else {
        window.localStorage.setItem(name, JSON.stringify(message))
    }
}
// 提取
function getSaveMessage(name, isclose) {
    if (self.frameElement && self.frameElement.tagName == "IFRAME") {
        if (parent.localStorage.getItem(name)) {
            var message = JSON.parse(parent.localStorage.getItem(name));
        } else {
            var message = ""
        }
        if (isclose) {
            parent.localStorage.removeItem(name)
        }
        return message
    } else {
        if (localStorage.getItem(name)) {
            var message = JSON.parse(localStorage.getItem(name));
        } else {
            var message = ""
        }
        if (isclose) {
            localStorage.removeItem(name)
        }
        return message
    }
}
ipcRenderer.on('WebIpc', (event, message) => {
    if (message.msgType != 3 && message.msgType != 33 && message.msgType != 18 && message.msgType != 10) {
        // if (message.msgType != 33 && message.msgType != 18) {
        console.log({ msgType: message.msgType, jsonInfo: JSON.parse(message.jsonInfo) }, window.location.pathname)
    }
    let jsonmessage = JSON.parse(message.jsonInfo)
    if (message.msgType == 2) {
        //房间详情
        jsonmessage.JslocalTime = Date.parse(new Date())
        saveMessage('msgType2', jsonmessage)
    } else if (message.msgType == 10) {
        //房间用户列表
        saveMessage('msgType10', jsonmessage)
    } else if (message.msgType == 16) {
        //窗口全屏】
        if (jsonmessage == 1) {
            //全屏
            $('.window-operation').each(function () {
                if ($(this).attr('data-type') == 'normal') {
                    $(this).removeClass('hide')
                }
                if ($(this).attr('data-type') == 'max') {
                    $(this).addClass('hide')
                }
            })
        } else {
            $('.window-operation').each(function () {
                if ($(this).attr('data-type') == 'normal') {
                    $(this).addClass('hide')
                }
                if ($(this).attr('data-type') == 'max') {
                    $(this).removeClass('hide')
                }
            })
        }
    } else if (message.msgType == 19) {
        if (jsonmessage.code == 304) {
            let isinCream = false
            let isinCreamMEssage = jsonmessage.msg
            // let isinCreamMEssage = "检测到您未进行过Ares模式赛前准备!立即测试会自动退出当前房间"
            // if (window.location.pathname == '/Modal/createRoom') {
            //     isinCream = true
            //     isinCreamMEssage = '检测到您未进行过Ares模式赛前准备!'
            // }
            let inpage = ''
            if (localStorage.getItem('isdetails')) {
                inpage = 'Details/newindex'
                parent.startModal('close')
            } else {
                inpage = 'Hall/index'
            }
            if (window.location.href.indexOf(inpage) != -1) {
                gxmodal({
                    title: translatesrting('提示'),
                    centent: '<div class="mt15" style="padding: 0px 22px;">' + isinCreamMEssage + '<div>',
                    buttons: [
                        {
                            text: translatesrting('立即检测'),
                            class: 'btn-middle btn-middle-blue J_alerttocheck',
                            callback: function () {
                                if (inpage == 'Details/newindex') {
                                    LeaveGameRoom()
                                }
                                if (JSON.parse(message.jsonInfo).msg.indexOf('Ares') != -1) {
                                    showWindowModal('settingmodal', '/Modal/setting?page=gamesetingTest&attention=ares')
                                } else {
                                    showWindowModal('settingmodal', '/Modal/setting?page=gamesetingTest&attention=game')
                                }
                            }
                        },
                        {
                            text: translatesrting('取消'),
                            class: 'btn-middle btn-style-ash',
                            callback: function () {
                                if (isinCream) {
                                } else {
                                    $('.detail-container-startbtn').gxbtn('reset')
                                    detailsMessage.startTime = 5
                                    clearInterval(detailsMessage.startTimefun)
                                    $('.detail-start-mark').addClass('hide')
                                    $('.detail-container-startbtn').text(translatesrting('开始游戏'))
                                }
                            }
                        }
                    ]
                })
            }
            return false
        }
        if (jsonmessage.msg == "您已加入游戏房间" || jsonmessage.msg == "您已经创建房间") {
            if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
                showhallGameRoom()
            } else {
                if (!$('.hall-header-returnroom').hasClass('hide')) {
                    $('.hall-header-returnroom').tooltip('show')
                    setTimeout(function () {
                        $('.hall-header-returnroom').tooltip('hide')
                    }, 3000)
                }
            }
        }
        if (jsonmessage.msg == "您已经创建房间") {
            if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
                showhallGameRoom()
            }
        }
        //通知
        if (jsonmessage.msg != '') {
            if (self.frameElement && self.frameElement.tagName == "IFRAME") {

            } else {
                spop({
                    template: translatesrting(jsonmessage.msg),
                    autoclose: jsonmessage.isAuto ? 3000 : '',
                    style: jsonmessage.type == 1 ? 'default' : jsonmessage.type == 2 ? 'success' : jsonmessage.type == 3 ? 'warning' : 'error'
                });
            }
        }
        if (jsonmessage.code == 305) {
            showhallGameRoom('close')
            wsUserStatus(1)
            if (hallContentSwiper) (hallContentSwiper(0))
            if (window.location.href.indexOf('Hall/index') != -1) {
                trackingFunc('room_RemoveRoom')
            }
        }
        if (jsonmessage.code == 306) {
            showhallGameRoom('close')
            wsUserStatus(1)
            if (hallContentSwiper) (hallContentSwiper(1))
            if (window.location.href.indexOf('Hall/index') != -1) {
                trackingFunc('room_RemoveRoom')
            }
        }
        if (window.location.href.indexOf('Hall/index') != -1) {
            if (
                jsonmessage.msg == "服务器繁忙请稍候再试" ||
                jsonmessage.msg == "无效的游戏房间号" ||
                jsonmessage.msg == "正在加入游戏请等待" ||
                jsonmessage.msg == "正在加入游戏" ||
                jsonmessage.msg == "游戏正在运行不能加入" ||
                jsonmessage.msg == "选择的游戏锁定中" ||
                jsonmessage.msg == "您不存在于已保存的游戏中" ||
                jsonmessage.msg == "游戏人数已满" ||
                jsonmessage.msg == "游戏检测未通过" ||
                jsonmessage.msg == "正在加入房间" ||
                jsonmessage.msg == "房间已开始游戏"
            ) {
                trackingFunc('join_error')
            }
        }
        if (jsonmessage.code == 302) {
            // 用于记录游戏未设置路径
            if (getSaveMessage('errorCode302')) {
                showWindowModal('settingmodal', '/Modal/setting?noclose=true')
            } else {
                saveMessage('msgType19', jsonmessage)
            }
        }
    } else if (message.msgType == 21) {
        //版本信息

        saveMessage('msgType21', jsonmessage)
    } else if (message.msgType == 31) {
        //刷新
        saveMessage('ClientBGM', 'true')
        window.location.href = '/Hall/index'
        // window.location.reload()
    }
});
