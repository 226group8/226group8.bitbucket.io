var settlementMessage = {
    swiper: null,
    userList: [],//总列表
    showList: [],//展示列表
    canChooseUser: [],//可选用户列表
    testNum: 8,//循环次数
    id: null,
    complaint: {
        id: null,
        user: null,
        reportTotal: null,
        reportedTotal: null,
        reportCode: null,
        description: null
    }
}

// 结束页
function showSettlement(id, polling, gameType) {
    settlementMessage.id = id
    // if (polling) {
    //     localStorage.setItem('changeGold', 1);
    //     // settlementActive()
    //     rankMessage.getResult(id, true)
    //     // return false
    //     return false
    // }
    $.star({
        type: 'GET',
        url: '/battlecenter/redwar/match/v1/summary',
        showerror: false,
        data: {
            matchId: id,
            polling: polling,
            gameType: gameType
        },
        success: function (res) {
            // if (polling) {
            //     localStorage.setItem('changeGold', 1);
            //     // settlementActive()
            //     rankMessage.getResult(id, true)
            //     // return false
            //     if (res.data.gameStats == 2) {
            //         // 流局
            //         return false
            //     }
            // }
            res.data.mapName = res.data.mapName + '&&||' + res.data.enName
            settlementMessage.testNum = 8
            res.data.gameSetting = JSON.parse(res.data.gameSetting)
            let GameVersion = null
            // let optionsCountry = JSON.parse(JSON.stringify(comOption.optionsCountry))
            let optionsCountry = JSON.parse(JSON.stringify(comOption.optionsCountry))
            res.data.gameSetting.map((item, index) => {
                if (item.value == "True" || item.value == "true") {
                    item.value = true
                }
                if (item.value == "False" || item.value == "false") {
                    item.value = false
                }
                if (item.key == 'GameVersion') {
                    GameVersion = item.value
                    res.data.gameSetting.splice(index, 1)
                }
            })
            if (GameVersion == 3) {
                optionsCountry.map((item) => {
                    if (item.sideId == 9) {
                        item.country = 'Shenzhen'
                        item.cname = '神州'
                    }
                })
            }
            let cancomplaint = false
            if (res.data.systemTime - res.data.gameFinishTime < 86400) {
                cancomplaint = true
            }
            let setdetails = res.data
            if (setdetails.label && (setdetails.label.indexOf('Task') != -1 || setdetails.label.indexOf('Defense') != -1)) {
                setdetails.unmatchStats = true
            }
            setdetails.teams.map((teams) => {
                teams.details.map((details) => {
                    comOption.optionsColor.map((color) => {
                        if (color.colorId == details.colorId) {
                            details.Jcolor = color.color
                        }
                    })
                })
            })
            oneImgCache(setdetails.mapImage).then((Cachereturn) => {
                setdetails.mapImage = Cachereturn
                $('#settlement .modal-dialog').html(template('settlement-script', {
                    gameType: gameType,
                    message: setdetails,
                    optionsCountry: optionsCountry,
                    optionsTeam: comOption.optionsTeam,
                    myid: userInfo.userId,
                    cancomplaint: cancomplaint
                }))
                $('.modal-backdrop').each(function () {
                    $(this).remove()
                })
                if (polling) {

                } else {
                    $('#settlement').modal('show')
                }
                setTimeout(function () {
                    settlementMessage.swiper = new Swiper('.settlement-swiper-container', {
                        autoplay: false,//可选选项，自动滑动
                        allowTouchMove: false,
                    })
                    // settlementMessage.swiper.slideTo(1, 0, false)
                }, 300)
                showSettlementUser(id, gameType)
                $('#settlement [data-toggle="tooltip"]').tooltip()
            })
        },
        error: function (req) {
            if (req.errorCode == 250001 && settlementMessage.testNum > 0) {
                setTimeout(() => {
                    settlementMessage.testNum = settlementMessage.testNum - 1
                    // showSettlement(id, true, gameType)
                }, 1000)
            }
        }
    });
}
// 用户信息
function showSettlementUser(id, gameType) {
    $.star({
        type: 'GET',
        url: '/battlecenter/redwar/match/v1/detail',
        showerror: false,
        data: {
            matchId: id,
            gameType: gameType
        },
        success: function (res) {
            settlementMessage.userList = [];
            for (item of res.data) {
                for (items of item.details) {
                    settlementMessage.userList.push(items)
                }
            }

            // 因为机器人ID相同问题 增加唯一识别
            settlementMessage.showList = []
            settlementMessage.userList.map((item, index) => {
                item.JsIndex = index;
                if (index <= 3) {
                    settlementMessage.showList.push(item)
                }
            })
            settlementMessage.userList.map((item, index) => {
                var isinit = false
                settlementMessage.showList.map((showitem, index) => {
                    if (item.JsIndex == showitem.JsIndex) {
                        isinit = true
                    }
                })
                if (!isinit) {
                    settlementMessage.canChooseUser.push(item)
                }
            })
            $('.settlement-details').html(template('settlement-details-script', {
                list: settlementMessage.showList,
                canChoose: settlementMessage.canChooseUser,
                iscanSlect: settlementMessage.canChooseUser.length > 0 ? true : false,
                isrow: true
            }))
            $('.settlement-details-div-scroll').removeClass('settlement-details-div-scroll-hide')
            $('#settlement').modal('show')
            $('#settlement .settlement-details-select.iscanSlect').gxmenu({
                height: "28px",
                top: "32px",
                clickHide: true//点击内部是否消失
            }, function (e) {

            })
        },
        error: function (req) {
        }
    });
}
// 选择显示用户
$(document).on('click', '.J_settlement-details-name', function () {
    var $this = $(this)
    var chooseIndex = $this.attr('data-index')
    var inIndex = $this.closest('.settlement-details-select').attr('data-index')
    settlementMessage.showList.map((item, index) => {
        if (item.JsIndex == inIndex) {
            settlementMessage.showList.splice(index, 1)
        }
    })
    settlementMessage.userList.map((item, index) => {
        if (item.JsIndex == chooseIndex) {
            settlementMessage.showList.push(item)
            $this.closest('.col-xs-3').html(template('settlement-details-script', {
                list: { 0: item },
                isrow: false,
                canChoose: settlementMessage.canChooseUser,
                iscanSlect: settlementMessage.canChooseUser.length > 0 ? true : false,
            }))
        }
    })
    settlementMessage.canChooseUser = []
    settlementMessage.userList.map((item, index) => {
        var isinit = false
        settlementMessage.showList.map((showitem, index) => {
            if (item.JsIndex == showitem.JsIndex) {
                isinit = true
            }
        })
        if (!isinit) {
            settlementMessage.canChooseUser.push(item)
        }
    })
    $('.settlement-details-select .gx-menu-ul').html(template('settlement-details-select-script', {
        canChoose: settlementMessage.canChooseUser,
    }))
    setTimeout(() => {
        $('#settlement .settlement-details-select').gxmenu({
            height: "28px",
            top: "32px",
            clickHide: true//点击内部是否消失
        }, function (e) {

        })
        $('.settlement-details-div-scroll').removeClass('settlement-details-div-scroll-hide')
    }, 100)
})
// 切换菜单
$(document).on('click', '.settlement_nav_item', function () {
    var $this = $(this)
    $this.siblings().removeClass('active')
    $this.addClass('active')
    settlementMessage.swiper.slideTo(Number($(this).attr('data-index')), 500, false)
})
// 清数据
$(document).on('hidden.bs.modal', '#settlement', function () {
    settlementMessage = {
        swiper: null,
        userList: [],//总列表
        showList: [],//展示列表
        canChooseUser: [],//可选用户列表
        testNum: 8,//循环次数
        id: null,
        complaint: {
            id: null,
            user: null,
            reportTotal: null,
            reportedTotal: null,
            reportCode: null,
            description: null,
            btnobj: null
        }
    }
    if (settlementMessage.swiper) {
        settlementMessage.swiper.destroy()
    }
})


$(document).on('click', '.settlement-complaint-show', function () {
    var $this = $(this)
    let gameType = $this.attr('data-gameType')
    settlementMessage.complaint.btnobj = $this
    settlementMessage.userList.map((item) => {
        if (item.userId == $this.attr('data-id')) {
            settlementMessage.complaint.user = item
        }
    })
    $.star({
        type: 'GET',
        url: '/battlecenter/redwar/match/v1/report',
        showerror: false,
        data: {
            gameType: gameType
        },
        success: function (res) {
            settlementMessage.complaint.reportTotal = res.data.reportTotal
            settlementMessage.complaint.reportedTotal = res.data.reportedTotal
            settlementMessage.complaint.optionscomplaint = comOption.optionscomplaint
            $('#settlement-complaint .modal-dialog').html(template('settlement-complaint-script', settlementMessage.complaint))
            $('.settlement-complaint-gx-menu').gxmenu({
                height: "28px",
                top: "32px",
                clickHide: true//点击内部是否消失
            })
            $('#settlement-complaint').modal('show')
        },
        error: function (req) {
        }
    });
})
// 举报图片上传
$(document).on('change', '#settlement-complaint-file', function (obj) {
    var $this = $(this)
    var fileTypes = [".jpg", ".png", ".jpge"];
    var filePath = $this.val();
    if (filePath) {
        var isNext = false;
        var fileEnd = filePath.substring(filePath.indexOf("."));
        for (var i = 0; i < fileTypes.length; i++) {
            if (fileTypes[i] == fileEnd) {
                isNext = true;
                break;
            }
        }
        if (!isNext) {
            spop({
                template: translatesrting('不接受此文件类型'),
                autoclose: 3000,
                style: 'error'
            });
            $this.val("")
            return false;
        }
    } else {
        return false;
    }
    // 大小限制
    if ((obj.currentTarget.files[0].size / 1024).toFixed(0) > 5120) {
        spop({
            template: translatesrting('上传图片不得大于5M'),
            autoclose: 3000,
            style: 'error'
        });
        return false
    }
    // 数量限制
    if ($('.settlement-complaint-updataDivlist').length >= 3) {
        spop({
            template: translatesrting('上传图片不得大于3张'),
            autoclose: 3000,
            style: 'error'
        });
        return false
    }
    let formData = new FormData();
    formData.append("file", obj.currentTarget.files[0]);
    formData.append("path", '/redwar/report');

    $.imgAjax({
        type: 'POST',
        url: '/battlecenter/platform/v1/upload',
        data: formData,
        success: function (res) {
            let appendHtml = '<div class="settlement-complaint-updataDivlist"><div class="settlement-complaint-updataDiv" style="background:url(' + res.data.url + ') no-repeat;background-size: cover;"><img width="40" class="hide" height="40" src="' + res.data.url + '" alt=""><a class="J_settlement-complaint-updataDiv-close" href="javascript:void(0)" ondragstart="return false">x</a></div></div>'
            $this.closest('div').append(appendHtml)
            $this.val("")
        },
        error: function (req) {
        }
    });
})

$(document).on('click', '#settlement-complaint .J_settlement-complaint-updataDiv-close', function () {
    var $this = $(this)
    $this.closest('.settlement-complaint-updataDivlist').remove()
})
$(document).on('click', '#settlement-complaint .J_settlement-reportCode', function () {
    var $this = $(this)
    $this.closest('.settlement-complaint-gx-menu').find('.settlement-complaint-gx-menu-span').text($this.text())
    settlementMessage.complaint.reportCode = $this.attr('data-code')
})
$(document).on('input', '#settlement-complaint .J_settlement-description', function () {
    var $this = $(this)
    settlementMessage.complaint.description = $this.val()
    $('.settlement-complaint-areanum').text(settlementMessage.complaint.description.length + '/100')
})
// 立即举报
$(document).on('click', '#settlement-complaint .J_settlement-complaint-btn', function () {
    if ($(this).hasClass('disabled')) {
        return false
    }
    if (settlementMessage.complaint.reportCode == null) {
        spop({
            template: translatesrting('举报类型不能为空'),
            autoclose: 3000,
            style: 'error'
        });
        return false
    }
    let gameType = $('.settlement-complaint-show').attr('data-gameType')
    let getreportImgs = []
    $('.settlement-complaint-updataDivlist').each(function () {
        var $this = $(this)
        getreportImgs.push($this.find('img').attr('src'))

    })
    if ($('.settlement-complaint-updataDivlist').length > 0) {
        $('.settlement-complaint-updataDivlist')
    }
    $.star({
        type: 'POST',
        url: '/battlecenter/redwar/match/v1/report',
        data: {
            matchId: settlementMessage.id,
            reportCode: settlementMessage.complaint.reportCode,
            reportImgs: getreportImgs,
            description: settlementMessage.complaint.description,
            reportUserId: settlementMessage.complaint.user.userId,
            gameType: gameType
        },
        success: function (res) {
            $('#settlement-complaint').modal('hide')
            settlementMessage.complaint.btnobj.remove()
            gxmodal({
                title: translatesrting('举报成功'),
                centent: '<div class="mt25">' + translatesrting('兰博玩平台将尽快审核并通知您处理结果，请耐心等待') + '</div>',
                buttons: [
                    {
                        text: translatesrting('确定'),
                        class: 'btn-middle btn-middle-blue',
                    }
                ]
            })
        },
        error: function (req) {
            spop({
                template: req.errorMsg,
                autoclose: 3000,
                style: 'error'
            });
        }
    });
})
// 清数据
$(document).on('hidden.bs.modal', '#settlement-complaint', function () {
    settlementMessage.complaint = {
        id: null,
        user: null,
        reportTotal: null,
        reportedTotal: null,
        reportCode: null,
        description: null,
        btnobj: null
    }
})
function settlementActive() {
    if (getSaveMessage('mapCollect')) {
        let activeTime = getSaveMessage('mapCollect').time
        let activeName = getSaveMessage('mapCollect').name
        if (activeName == userInfo.userId && activeTime == new Date().toLocaleDateString()) {
            return false
        }
    }
    $.star({
        type: 'GET',
        url: '/activity/platform/tips',
        showerror: false,
        success: function (tipsres) {
            if (tipsres.data.mapCollect != null && tipsres.data.mapCollect.votes >= 1) {
                saveMessage('mapCollect', { time: new Date().toLocaleDateString(), name: userInfo.userId })
                gxmodal({
                    title: translatesrting('提示'),
                    centent: '<div class="mt30" style="padding: 0px 22px;">' + '您已进行一场游戏，获得地图征集活动选票，快去投票抽大奖吧>>' + '<div>',
                    buttons: [
                        {
                            text: translatesrting('立即前往'),
                            class: 'btn-middle btn-middle-blue',
                            callback: function () {
                                $.star({
                                    type: 'POST',
                                    url: '/community-user/authorize/generate/code',
                                    showerror: false,
                                    success: function (coderes) {
                                        let tourl = tipsres.data.mapCollect.url
                                        if (tourl.indexOf('?') != -1) {
                                            OpenWebUrl(tourl + '&token=' + coderes.data.authCode)
                                        } else {
                                            OpenWebUrl(tourl + '?token=' + coderes.data.authCode)
                                        }
                                    },
                                    error: function (req) {
                                        spop({
                                            template: req.errorMsg,
                                            autoclose: 5000,
                                            style: 'error'
                                        });
                                    }
                                });
                            }
                        },
                        {
                            text: translatesrting('取消'),
                            class: 'btn-middle btn-style-ash',
                            callback: function () {
                            }
                        }
                    ]
                })
            }
        },
        error: function (req) {
            spop({
                template: req.errorMsg,
                autoclose: 5000,
                style: 'error'
            });
        }
    });
}
