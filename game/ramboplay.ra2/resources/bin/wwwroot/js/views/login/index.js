// 翻译
$('#loginDiv').html(template('loginDiv-script', { LBWlanguage: comOption.LBWlanguage }))

var loginMessage = {
    loginSwiper: null,
    loginPhone: null,//登录手机
    loginPassword: null,//登录密码
    isRemember: null,//是否记住密码
    deviceOS: null,//设备型号
    machineCode: null,//设备号
    forget: {
        phone: null,
        code: null
    },
    newpassword: null,
    newpasswordTwo: null,
    register: {
        phone: null,
        password: null,
        code: null
    },
    mobilereg: /^\d{11}$/,
    sendLoading: false,
    sendLoadingNum: 60,
    sendLoadingFun: null,
    isoutlogin: null,
    countryCode: 86,
    isLoginToken: false,
    verifySmart: null,
    updateSetTime: null,
    updateSetTimeclose: null
}
// init
$(function () {
    localStorage.removeItem('wsUserStatus')
    localStorage.removeItem('GameRulesAlert')
    localStorage.removeItem('isinitfirst')
    localStorage.removeItem('msgType19')
    localStorage.removeItem('isdetails')//预防邀请信息位置判断
    localStorage.removeItem('verandaType')
    wsUserStatus(1)
    $.star({
        type: 'GET',
        url: '/battlecenter/platform/v2/language',
        data: {
            languageType: 'en'
        },
        success: function (res) {
            let newarray = {}
            res.data.map((item) => {
                newarray[item.languageType] = JSON.parse(item.languageValue)
            })
            saveMessage('languageValue', newarray)
            translateJson = newarray
        },
        error: function (req) {
        }
    })

    localStorage.removeItem('userInfo')
    localStorage.removeItem('errorCode302')//为了预防新手引导 游戏设置同时判断问题
    loginMessage.isoutlogin = getQueryVariable('code')
    if (loginMessage.isoutlogin == 101 || loginMessage.isoutlogin == 401) {
        loginnormal()
        LoginError('login', translatesrting('无法连接服务器，请重启客户端后重试'))
        // setTimeout(() => { $('#login-in').click() }, 1000)
    }

    if (loginMessage.isoutlogin == 'change') {
        loginnormal()
    } else {
        loginMessage.updateSetTime = setInterval(() => {
            ipcRenderer.send('Main', {
                msgType: "CheckUpdate",
                jsonInfo: {
                    status: 0
                }
            });
        }, 10000)
        loginMessage.updateSetTimeclose = setTimeout(() => {
            gxmodal({
                title: translatesrting('提示'),
                centent: '<div class="mt25">' + translatesrting('更新检测失败，请关闭并重启客户端') + '</div>',
                buttons: [
                    {
                        text: translatesrting('关闭'),
                        class: 'btn-middle btn-middle-blue',
                        callback: function () {
                            $('.header-close.window-operation').click()
                        }
                    }
                ]
            })
        }, 30000)
    }
    if (getQueryVariable('noCheck')) {
        $('.J_updataCheck').addClass('hide')
        $('#login-in').removeClass('hide')
        $('.loginDiv-login-content .gx-menu').gxmenu({
            height: "30px",
            top: "26px",
            clickHide: true//点击内部是否消失
        }, function (e) {

        })
    }
    // ipcRenderer.send('Main', {
    //     msgType: "ChangeWindowSize",
    //     jsonInfo: {
    //         window: 'main',
    //         width: 481,
    //         height: 500,
    //         miniWidth: 481,
    //         miniHeight: 500,
    //         isCenter: true
    //     }
    // });
    loginMessage.loginSwiper = new Swiper('.loginDiv-swiper', {
        slidesPerView: 1,
        spaceBetween: 0,
        autoplay: false,
        // effect: 'fade',
        allowTouchMove: false,
        on: {
            slideChangeTransitionEnd: function () {
                loginSetInterval('hide')
            },
        },
    })
    //获取设备号
    getdeviceOS()

    let showlanguage = '简体中文'
    if (localStorage.getItem('LBWlanguage')) {
        comOption.LBWlanguage.map((item) => {
            if (item.code == localStorage.getItem('LBWlanguage')) {
                showlanguage = item.name
            }
        })
    }
    $('.loginDiv-language-text').text(showlanguage)

    if (getSaveMessage('userphone')) {
        loginMessage.loginPhone = getSaveMessage('userphone')
        $('#login-phone').val(loginMessage.loginPhone)
    }
    // 记住
    if (getSaveMessage('isRemember') && getSaveMessage('accessToken')) {

        $.star({
            type: 'GET',
            url: '/community-user/user/v2/info',
            data: '',
            access_token: getSaveMessage('accessToken'),
            showerror: false,
            success: function (res) {
                loginMessage.isLoginToken = true
                $('#remember').prop('checked', 'checked')
                loginMessage.isRemember = getSaveMessage('isRemember')
                loginMessage.loginPassword = getSaveMessage('userpass')
                $('#login-password').val(loginMessage.loginPassword)
            },
            error: function (req) {

            }
        });
    }
    if (!getSaveMessage('isphonepasshide')) {
        $('#login-phone').attr('type', 'password')
        $('.login-phone-icon-hide').removeClass('hide')
        $('.login-phone-icon-show').addClass('hide')
    } else {
        if (getSaveMessage('isphonepasshide') == "close") {
            // 关闭
            $('#login-phone').attr('type', 'password')
            $('.login-phone-icon-hide').removeClass('hide')
            $('.login-phone-icon-show').addClass('hide')
        } else if (getSaveMessage('isphonepasshide') == "open") {
            // 打开
            $('#login-phone').attr('type', 'text')
            $('.login-phone-icon-show').removeClass('hide')
            $('.login-phone-icon-hide').addClass('hide')
        }
    }
    // var updateMessage = getSaveMessage('msgType21')
    // if (updateMessage.Status == 1) {
    //     updatefun()
    // }
    isbusy()
})
function getMobilCode() {
    var code = 86
    if ($.cookie('countryCode')) {
        code = $.cookie('countryCode')
    }
    loginMessage.countryCode = code
    $('.input-gx-menu').find('.input-beforcode').text('+' + loginMessage.countryCode)
    $.star({
        type: "GET",
        url: '/community-user/verify/mobile/v1/code',
        data: "",
        showerror: true,
        success: function (res) {
            let codeHtml = ""
            codeHtml += '<div class="chooseCountryInput">' + '<input class="gx-input" placeholder="' + translatesrting('请告诉我您要搜索的区号') + '"/>' + '</div>'
            res.data.map((item) => {
                codeHtml += '<div class="gx-menu-li"><a class="chooseCountryCode" href="javascript:void(0)" ondragstart="return false" data-code="' + item.code + '">' + translatesrting(item.name) + ' ' + ' +' + item.code + '</a></div>'
            })
            $('.input-gx-menu').find('.gx-menu-ul').html(codeHtml)
            $('.input-gx-menu').gxmenu({
                height: "20px",
                top: "45px",
                clickHide: false//点击内部是否消失
            }, function (e) {

            })
        },
        error: function (req) {

        }
    })
}
// 选国家号
$(document).on('click', '.chooseCountryCode', function () {
    var $this = $(this);
    let chooseCode = $this.attr('data-code')
    $('.input-gx-menu').find('.input-beforcode').text('+' + chooseCode)
    loginMessage.countryCode = chooseCode
    $('.input-gx-menu').gxmenu({}, 'hide')
})
// 筛选家号
$(document).on('input', '.chooseCountryInput input', function () {
    var $this = $(this);
    var code = $this.val();
    $('.input-gx-menu').find('.chooseCountryCode').each(function () {
        if ($(this).attr('data-code').indexOf(code) != -1) {
            $(this).closest('.gx-menu-li').removeClass('hide')
        } else {
            $(this).closest('.gx-menu-li').addClass('hide')
        }
    })
})
if (getSaveMessage('Version') != null) {
    let test = ''
    if (getSaveMessage('test')) {
        test = '-' + 'test'
    }
    if (getSaveMessage('pre')) {
        test = '-' + 'pre'
    }
    $(".header-version").text(translatesrting('版本号') + ':' + getSaveMessage('Version') + test)
}

if (!getSaveMessage('ClientVolume')) {
    let temp_volume = {
        globalVolume: { volume: 1, mute: false },
        clientBgm: { volume: 0.2, mute: false },
        clientSound: { volume: 0.2, mute: false },
        scoreVolume: { volume: 0.2, mute: false },
        soundVolume: { volume: 0.2, mute: false },
        soiceVolume: { volume: 0.2, mute: false },
    }
    saveMessage('ClientVolume', temp_volume)
} else {
    if (getSaveMessage('ClientVolume')['globalVolume'] == undefined) {
        let temp_volume = {
            globalVolume: { volume: 1, mute: false },
            clientBgm: { volume: 0.2, mute: false },
            clientSound: { volume: 0.2, mute: false },
            scoreVolume: { volume: 0.2, mute: false },
            soundVolume: { volume: 0.2, mute: false },
            soiceVolume: { volume: 0.2, mute: false },
        }
        saveMessage('ClientVolume', temp_volume)
    } else {
        if (getSaveMessage('ClientVolume')['globalVolume'].mute == true) {
            $('.volume-mute-div').addClass('volume-mute')
        }
    }
}

saveMessage('ClientBGM', 'false')

$(document).on('click', '.volume-mute-div', function () {
    if ($(this).hasClass('volume-mute')) {
        $(this).removeClass('volume-mute')
        globalMute(false)
    } else {
        $(this).addClass('volume-mute')
        globalMute(true)
    }
})

// setTimeout(() => {
// $(document).on('submit', '.J_form_login', function (e) {
//     $('#login-in').click()
//     e.preventDefault();
// })
// $('.J_updataCheck').addClass('hide')
// $('#login-in').removeClass('hide')
// $('.loginDiv-login-content .gx-menu').gxmenu({
//     height: "30px",
//     top: "26px",
//     clickHide: true//点击内部是否消失
// }, function (e) {

// })
// }, 100)
// 获得更新信息
// function updatefun() {
//     ipcRenderer.send('Main', {
//         msgType: "ChangeWindowSize",
//         jsonInfo: {
//             window: 'main',
//             width: 600,
//             height: 400,
//             miniWidth: 600,
//             miniHeight: 400,
//             isCenter: true
//         }
//     });
//     window.location.href = "/Start/index"
// }
// 监听登录手机
$(document).on('input', '#login-phone', function () {
    loginMessage.loginPhone = $(this).val()
    loginMessage.loginPassword = ""
    $('#login-password').val("")
    if (loginMessage.isLoginToken && loginMessage.loginPhone == getSaveMessage('userphone')) {
        loginMessage.loginPassword = getSaveMessage('userpass')
        $('#login-password').val(loginMessage.loginPassword)
        firstkey = true
    } else {
        firstkey = false
    }
})
// 监听登录密码
var firstkey = true
$(document).on('input', '#login-password', function () {
    if (loginMessage.isLoginToken && firstkey) {
        firstkey = false
        $(this).val($(this).val().split(getSaveMessage('userpass'))[1])
    }
    loginMessage.loginPassword = $(this).val()
})
// 为了不可选中
$(document).on('click', '#login-password-mack', function () {
    $('#login-password').focus()
})

// 监听是否记住密码
$(document).on('change', '#remember', function () {
    loginMessage.isRemember = $(this).prop('checked')
    saveMessage('isRemember', loginMessage.isRemember)
})
// 登录
$(document).on('click', '#login-in', function () {
    // if (!loginMessage.mobilereg.test(loginMessage.loginPhone)) {
    //     LoginError('login', '请输入正确的手机号')
    //     return false
    // }
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    if (loginMessage.isLoginToken && loginMessage.loginPhone == getSaveMessage('userphone') && loginMessage.loginPassword == getSaveMessage('userpass')) {
        $this.gxbtn('loading')
        tokeLogin()
        return false
    }
    $this.gxbtn('loading')
    loginpwd()
})
function loginpwd() {
    let sendData = {
        mobile: loginMessage.loginPhone,
        password: loginMessage.loginPassword,
        deviceModel: 'PC_REDWAR',
        deviceOS: loginMessage.deviceOS,
        machineCode: loginMessage.machineCode,
        countryCode: loginMessage.countryCode,
        utmSource: localStorage.getItem('utm_source') || ''
    }
    if ($('.J_login_ucShow_div').find($('#nc')).length > 0) {
        sendData.verifySmart = loginMessage.verifySmart
        if (sendData.verifySmart == null) {
            $('#login-in').gxbtn('reset')
            return false
        }
    }
    if (loginMessage.verifySmart) {
        // 有可能消失#nc
        sendData.verifySmart = loginMessage.verifySmart
    }
    $.star({
        type: 'POST',
        url: '/community-user/redwar/v1/login/pwd',
        data: sendData,
        showerror: false,
        success: function (res) {
            loginpassSuccess(res)
        },
        error: function (req) {
            $('#login-in').gxbtn('reset')

            if (req.errorCode == '210104') {
                LoginError('login', translatesrting('请输入正确的手机号'))
            } else if (req.errorCode == '210216') {
                LoginError('login', translatesrting('您被封禁到') + formatDate(req.errorData.endTime))
                if (req.errorData.reportType == 7) {
                    DleteCheatFile()
                }
            } else if (req.errorCode == '210115') {
                loginMessage.verifySmart = null
                LoginError('login', translatesrting('密码错误'))
                $('.J_login_ucShow_div').addClass('login-nc-show')
                $('.J_login_ucShow_div').html('<div id="nc"></div>')
                setTimeout(() => { $('.J_login_ucShow_div').addClass('login-nc-show-option') }, 300)
                AWSCfun('login')
            } else if (req.errorCode == '210221') {
                LoginError('login', translatesrting('账号处于注销反悔期'), '反悔注销', () => {
                    gxmodal({
                        title: translatesrting('反悔注销'),
                        centent: '<div class="mt25">' + translatesrting('当前账号处于30天注销反悔期内，是否反悔账号注销？') + '</div>',
                        buttons: [
                            {
                                text: translatesrting('反悔注销'),
                                class: 'btn-middle btn-middle-blue',
                                callback: function () {
                                    $.star({
                                        type: 'POST',
                                        url: '/community-user/user/v1/cancel/goback',
                                        data: {
                                            mobile: loginMessage.loginPhone,
                                            password: loginMessage.loginPassword,
                                        },
                                        success: function (res) {
                                            showtoast({
                                                message: translatesrting('反悔注销成功')
                                            })
                                        },
                                        error: function (req) {
                                            showtoast({
                                                message: translatesrting(req.errorMsg)
                                            })
                                        }
                                    })
                                }
                            },
                            {
                                text: translatesrting('关闭'),
                                class: 'btn-middle',
                                callback() {

                                }
                            }
                        ]
                    })
                })
            } else if (req.errorCode == '210222') {
                LoginError('login', translatesrting('该账号已被注销，无法登录'))
            } else {
                LoginError('login', req.errorMsg)
            }
        }
    });
}
// 记住密码
function tokeLogin() {
    $.star({
        type: 'POST',
        url: '/community-user/redwar/v1/login/token',
        data: {
            deviceOS: loginMessage.deviceOS,
            machineCode: loginMessage.machineCode,
            deviceModel: 'PC_REDWAR',
            utmSource: localStorage.getItem('utm_source') || ''
        },
        access_token: getSaveMessage('accessToken'),
        showerror: false,
        success: function (res) {
            let resmessage = res
            resmessage.data.accessToken = getSaveMessage('accessToken')
            loginpassSuccess(resmessage)
        },
        error: function (req) {
            $('#login-in').gxbtn('reset')
            if (req.errorCode == '210104') {
                LoginError('login', translatesrting('请输入正确的手机号'))
            } else if (req.errorCode == '210216') {
                LoginError('login', translatesrting('您被封禁到') + formatDate(req.errorData.endTime))
                if (req.errorData.reportType == 7) {
                    DleteCheatFile()
                }
            } else if (req.errorCode == '210221') {
                LoginError('login', translatesrting('账号处于注销反悔期'), '反悔注销', () => {
                    gxmodal({
                        title: translatesrting('反悔注销'),
                        centent: '<div class="mt25">' + translatesrting('当前账号处于30天注销反悔期内，是否反悔账号注销？') + '</div>',
                        buttons: [
                            {
                                text: translatesrting('反悔注销'),
                                class: 'btn-middle btn-middle-blue',
                                callback: function () {
                                    $.star({
                                        type: 'POST',
                                        url: '/community-user/user/v1/cancel/goback',
                                        data: {
                                            mobile: loginMessage.loginPhone,
                                        },
                                        access_token: getSaveMessage('accessToken'),
                                        success: function (res) {
                                            showtoast({
                                                message: translatesrting('反悔注销成功')
                                            })
                                        },
                                        error: function (req) {
                                            showtoast({
                                                message: translatesrting(req.errorMsg)
                                            })
                                        }
                                    })
                                }
                            },
                            {
                                text: translatesrting('关闭'),
                                class: 'btn-middle',
                                callback() {
                                }
                            }
                        ]
                    })
                })
            } else if (req.errorCode == '210222') {
                LoginError('login', translatesrting('该账号已被注销，无法登录'))
            } else {
                LoginError('login', req.errorMsg)
            }
        }
    });
}
function loginpassSuccess(res) {
    // 存储
    saveMessage('userphone', loginMessage.loginPhone)
    $.cookie('countryCode', loginMessage.countryCode)
    if (loginMessage.isRemember) {
        let numpass = ''
        let numpassarray = loginMessage.loginPassword.split('')
        for (let i = 0; i < numpassarray.length; i++) {
            numpass += "*"
        }
        saveMessage('userpass', numpass)
        saveMessage('accessToken', res.data.accessToken)
    } else {
        localStorage.removeItem('isRemember')
        // localStorage.removeItem('userphone')
        localStorage.removeItem('userpass')
        localStorage.removeItem('accessToken')
    }
    saveMessage('userInfo', res.data)
    let LBWlanguage = localStorage.getItem('LBWlanguage') || 'zh'
    if (LBWlanguage == 'en') {
        LBWlanguage = 'en_US'
    }
    var devheaders = {
        'Content-type': 'application/json',
        'device_id': $.cookie('deviceId'),
        'device': 5,
        'platform': 3,
        'language': LBWlanguage,
        'access_token': res.data.accessToken,
        'app_version_code': getSaveMessage('msgType21').version || ""
    }

    let customField = customFieldFun('encryption', {
        avatarFrame: res.data.avatarFrame,
        authenticateType: res.data.authenticateType,
        authenticateContent: res.data.authenticateContent,
        isAuthenticate: res.data.isAuthenticate
    })
    $.controlAjax({
        type: "POST",
        url: '/api/login/success/',
        data: {
            customField: customField,
            isAnchor: res.data.anchor,
            nickName: res.data.username,
            avatar: res.data.avatar,
            userName: loginMessage.mobile,
            userId: res.data.userId,
            token: res.data.accessToken,
            headers: JSON.stringify(devheaders)
        },
        success: function (res) {
            LoginError('hide')//取消报错样式
            $('body').css('opacity', 0)
            $('body').css('background', 'transparent')
            // ipcRenderer.send('Main', {
            //     msgType: "ChangeWindowSize",
            //     jsonInfo: {
            //         window: 'main',
            //         width: 1596,
            //         height: 800,
            //         miniWidth: 1366,
            //         miniHeight: 768,
            //         isCenter: true
            //     }
            // });
            $('#login-in').gxbtn('reset')
            setTimeout(() => {
                trackingFunc('login_success')
                window.location.href = '/Hall/index'
            })
        },
        error: function (req) {
            $('#login-in').gxbtn('reset')
            LoginError('login', translatesrting('与服务器连接出现问题，请稍后再试'))
            trackingFunc('login_fail')
        }
    })
}
function loginSuccessAnimated() {
    $('.pageAnimate-border').addClass('pageAnimate-border-in')
    setTimeout(() => {
        $('#loginDiv').css('left', 1000)
    }, 320)
}
var LoginErrorfun = null
// 错误样式渲染
function LoginError(type, message, operation, callback) {
    $('.alert-success').removeClass('showOpacity').removeClass('fadeIn')
    if (type != 'hide') {
        $('#' + type + '-error .loginDiv-login-error-span').text(message)
        if (!!operation && !!callback) {
            $('#' + type + '-error .loginDiv-login-error-operation-span').text(operation)
            $(document).one('click', '.loginDiv-login-error-operation-span', callback)
        } else {
            $('#' + type + '-error .loginDiv-login-error-operation-span').text('')
        }
        let alertDom = $('#' + type + '-error')
        clearTimeout(LoginErrorfun)
        alertDom.animate({ 'opacity': 0 }, 200)
        alertDom.animate({ 'opacity': 1 }, 200)
        LoginErrorfun = setTimeout(function () {
            alertDom.animate({ 'opacity': 0 }, 200)
            $(document).off('click', '.loginDiv-login-error-operation-span')
        }, 3000)
    } else if (type == 'hide') {
        $(document).off('click', '.loginDiv-login-error-operation-span')
        $('.alert-error').removeClass('showOpacity').removeClass('fadeIn')
    }
}
function LoginSuccess(type, message) {
    $('.alert-error').removeClass('showOpacity').removeClass('fadeIn')
    if (type != 'hide') {
        $('#' + type + '-success .loginDiv-login-success-span').text(message)
        $('#' + type + '-success').addClass('showOpacity').addClass('animated fadeIn')
        setTimeout(() => {
            $('#' + type + '-success').removeClass('fadeIn');
        }, 1000)
    } else if (type == 'hide') {
        $('.alert-success').removeClass('showOpacity').removeClass('fadeIn')
    }
}
$(document).on('click', '.loginDiv-return', function () {
    var index = $(this).attr('data-index')
    loginMessage.loginSwiper.slideTo(index)
})

// 忘记密码
$(document).on('click', '#J_forget_btn', function () {
    $('.J_login_ucShow_div').removeClass('login-nc-show-option')
    $('.J_login_ucShow_div').removeClass('login-nc-show')
    $('.J_login_ucShow_div').html(' ')
    $('#slideTwo').html(template('loginDiv-forget-script'))
    getMobilCode()
    loginMessage.loginSwiper.slideTo(1)
    setTimeout(() => {
        $('#forget-phone').focus()
    }, 300)
    loginMessage.forget = {
        phone: null,
        code: null
    }
})
// 监听忘记密码手机
$(document).on('input', '#forget-phone', function () {
    loginMessage.forget.phone = $(this).val()
})
// 监听忘记密码验证码
$(document).on('input', '#forget-code', function () {
    loginMessage.forget.code = $(this).val()
})
// 监听忘记密码获取验证码
$(document).on('click', '.J_forget_ucShow', function () {
    if (loginMessage.sendLoading) {
        return false
    }
    if (loginMessage.forget.phone == "" || loginMessage.forget.phone == null) {
        LoginError('forget', translatesrting('请输入手机号'))
        return false
    }
    // if (!loginMessage.mobilereg.test(loginMessage.forget.phone)) {
    //     LoginError('forget', '请输入正确的手机号')
    //     return false
    // }
    LoginError('hide')
    $('.J_forget_ucShow_div').addClass('login-nc-show')
    $('.J_forget_ucShow_div').html('<div id="nc"></div>')
    setTimeout(() => { $('.J_forget_ucShow_div').addClass('login-nc-show-option') }, 300)
    AWSCfun('forget', $(this).attr('data-type'))
})
// 忘记密码下一步
$(document).on('click', '#forget-next', function () {
    if (loginMessage.forget.phone == "" || loginMessage.forget.phone == null) {
        LoginError('forget', translatesrting('请输入手机号'))
        return false
    }
    // if (!loginMessage.mobilereg.test(loginMessage.forget.phone)) {
    //     LoginError('forget', '请输入正确的手机号')
    //     return false
    // }
    if (loginMessage.forget.code == "" || loginMessage.forget.code == null) {
        LoginError('forget', translatesrting('请输入验证码'))
        return false
    }
    LoginError('hide')
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    $this.gxbtn('loading')
    $.star({
        type: 'POST',
        url: '/community-user/verify/v1/temp/valid',
        data: {
            mobile: loginMessage.forget.phone,
            type: 4,
            countryCode: loginMessage.countryCode,
            verifyCode: loginMessage.forget.code
        },
        showerror: false,
        success: function (res) {
            $this.gxbtn('reset')
            LoginError('hide')
            $('#slideThree').html(template('loginDiv-setPassWord-script'))
            loginMessage.loginSwiper.slideTo(2)
            setTimeout(() => {
                $('#setPassWord-new').focus()
            }, 300)
        },
        error: function (req) {
            $this.gxbtn('reset')
            LoginError('forget', req.errorMsg)
        }
    });
})
//监听新密码
$(document).on('input', '#setPassWord-new', function () {
    loginMessage.newpassword = $(this).val()
})
//监听新密码two
$(document).on('input', '#setPassWord-newtwo', function () {
    loginMessage.newpasswordTwo = $(this).val()
})
$(document).on('click', '#sureSetNewBtn', function () {
    if (loginMessage.newpassword == "" || loginMessage.newpassword == null) {
        LoginError('setpass', translatesrting('请输入新密码'))
        return false
    }
    if (loginMessage.newpassword != loginMessage.newpasswordTwo) {
        LoginError('setpass', translatesrting('两次输入的密码不一致'))
        return false
    }
    if (loginMessage.newpassword.indexOf('??') != -1) {
        LoginError('setpass', '密码不得添加特殊字符')
        return false
    }
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    $this.gxbtn('loading')
    $.star({
        type: 'POST',
        url: '/community-user/redwar/v1/forget/pwd',
        data: {
            mobile: loginMessage.forget.phone,
            verifyCode: loginMessage.forget.code,
            password: loginMessage.newpassword
        },
        showerror: false,
        success: function (res) {
            $this.gxbtn('reset')
            LoginError('hide')
            LoginSuccess('setpass', '密码修改成功')
            setTimeout(() => {
                loginMessage.loginSwiper.slideTo(0, 0, false)
                loginMessage.loginPhone = loginMessage.forget.phone
                loginMessage.loginPassword = null
                $('#login-phone').val(loginMessage.forget.phone)
                $('#login-password').val("")
                $('#login-password').focus()
            }, 2000)
        },
        error: function (req) {
            $this.gxbtn('reset')
            LoginError('setpass', req.errorMsg)
        }
    });
})

// 注册
$(document).on('click', '#J_register_btn', function () {
    $('.J_login_ucShow_div').removeClass('login-nc-show-option')
    $('.J_login_ucShow_div').removeClass('login-nc-show')
    $('.J_login_ucShow_div').html(' ')
    $('#slideTwo').html(template('loginDiv-register-script'))
    getMobilCode()
    loginMessage.loginSwiper.slideTo(1)
    setTimeout(() => {
        $('#register-phone').focus()
    }, 300)
    loginMessage.register = {
        phone: null,
        password: null,
        code: null,
        username: null
    }
})
//监听注册手机号
$(document).on('input', '#register-phone', function () {
    loginMessage.register.phone = $(this).val()
})
//监听注册密码
$(document).on('input', '#register-password', function () {
    loginMessage.register.password = $(this).val()
})
//监听注册昵称
$(document).on('input', '#register-name', function () {
    loginMessage.register.username = $(this).val()
})
$(document).on('blur', '#register-name', function () {
    $.star({
        type: 'POST',
        url: '/community-user/user/username/v1/verify',
        data: {
            username: loginMessage.register.username
        },
        showerror: false,
        success: function (res) {
            LoginError('hide')
        },
        error: function (req) {
            let errormess = req.errorMsg
            LoginError('register', errormess)
        }
    });

})
//监听注册验证码
$(document).on('input', '#register-code', function () {
    loginMessage.register.code = $(this).val()
})
// 监听注册获取验证码
$(document).on('click', '.J_register_ucShow', function () {
    if (loginMessage.sendLoading) {
        return false
    }
    if (loginMessage.register.phone == "" || loginMessage.register.phone == null) {
        LoginError('register', '请输入手机号')
        return false
    }
    // if (!loginMessage.mobilereg.test(loginMessage.register.phone)) {
    //     LoginError('register', '请输入正确的手机号')
    //     return false
    // }
    LoginError('hide')
    $('.J_register_ucShow_div').addClass('login-nc-show')
    $('.J_register_ucShow_div').html('<div id="nc"></div>')
    setTimeout(() => { $('.J_register_ucShow_div').addClass('login-nc-show-option') }, 300)
    AWSCfun('register', $(this).attr('data-type'))
})
// 监听注册
$(document).on('click', '#register-btn', function () {
    if (loginMessage.register.phone == "" || loginMessage.register.phone == null) {
        LoginError('register', '请输入手机号')
        return false
    }
    if (loginMessage.register.password == "" || loginMessage.register.password == null) {
        LoginError('register', '请输入密码')
        return false
    }
    if (loginMessage.register.password.indexOf('??') != -1) {
        LoginError('register', '密码不得添加特殊字符')
        return false
    }
    if (loginMessage.register.code == "" || loginMessage.register.code == null) {
        LoginError('register', '请输入验证码')
        return false
    }
    // if (!loginMessage.mobilereg.test(loginMessage.register.phone)) {
    //     LoginError('register', '请输入正确的手机号')
    //     return false
    // }
    if (loginMessage.register.username == "" || loginMessage.register.username == null) {
        LoginError('register', '请输入昵称')
        return false
    }
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    $this.gxbtn('loading')
    $.star({
        type: 'POST',
        url: '/community-user/redwar/v1/regist',
        data: {
            mobile: loginMessage.register.phone,
            verifyCode: loginMessage.register.code,
            password: loginMessage.register.password,
            deviceModel: 'PC_REDWAR',
            deviceOS: loginMessage.deviceOS,
            machineCode: loginMessage.machineCode,
            username: loginMessage.register.username,
            countryCode: loginMessage.countryCode,
            utmSource: localStorage.getItem('utm_source') || ''
        },
        showerror: false,
        success: function (res) {
            // 存储
            $this.gxbtn('reset')
            LoginSuccess('register', '注册成功')
            saveMessage('userInfo', res.data)
            $.cookie('countryCode', loginMessage.countryCode)
            let LBWlanguage = localStorage.getItem('LBWlanguage') || 'zh'
            if (LBWlanguage == 'en') {
                LBWlanguage = 'en_US'
            }
            var devheaders = {
                'Content-type': 'application/json',
                'device_id': $.cookie('deviceId'),
                'device': 5,
                'platform': 3,
                'language': LBWlanguage,
                'access_token': res.data.accessToken,
                'app_version_code': getSaveMessage('msgType21').version || ""
            }
            let customField = customFieldFun('encryption', {
                avatarFrame: res.data.avatarFrame,
                authenticateType: res.data.authenticateType,
                authenticateContent: res.data.authenticateContent,
                isAuthenticate: res.data.isAuthenticate
            })
            $.controlAjax({
                type: "POST",
                url: '/api/login/success/',
                data: {
                    customField: customField,
                    isAnchor: res.data.anchor,
                    nickName: res.data.username,
                    avatar: res.data.avatar,
                    userName: loginMessage.mobile,
                    userId: res.data.userId,
                    token: res.data.accessToken,
                    headers: JSON.stringify(devheaders)
                },
                success: function (res) {
                    LoginError('hide')
                    // 清除本地报存信息
                    saveMessage('userphone', loginMessage.register.phone)
                    localStorage.removeItem('isRemember')
                    localStorage.removeItem('userpass')
                    localStorage.removeItem('accessToken')
                    // 清除本地报存信息end
                    $('body').css('opacity', 0)
                    // ipcRenderer.send('Main', {
                    //     msgType: "ChangeWindowSize",
                    //     jsonInfo: {
                    //         window: 'main',
                    //         width: 1596,
                    //         height: 800,
                    //         miniWidth: 1366,
                    //         miniHeight: 768,
                    //         isCenter: true
                    //     }
                    // });
                    $('#register-btn').gxbtn('reset')
                    window.location.href = '/Hall/index'
                    trackingFunc('login_success')
                }
            })
        },
        error: function (req) {
            $this.gxbtn('reset')
            if (req.errorCode == '210220') {
                LoginError('register', translatesrting('该手机号已被注销，无法注册'))
            } else {
                let errormess = req.errorMsg
                LoginError('register', errormess)
            }

        }
    });
})

function AWSCfun(type, issms) {
    AWSC.use("nc", function (state, module) {
        var ncOption = {
            // 应用类型标识。它和使用场景标识（scene字段）一起决定了滑动验证的业务场景与后端对应使用的策略模型。您可以在阿里云验证码控制台的配置管理页签找到对应的appkey字段值，请务必正确填写。
            appkey: "FFFF0N00000000009F6B",
            //使用场景标识。它和应用类型标识（appkey字段）一起决定了滑动验证的业务场景与后端对应使用的策略模型。您可以在阿里云验证码控制台的配置管理页签找到对应的scene值，请务必正确填写。
            scene: type == 'login' ? "nc_login" : "nc_message_h5",
            // 声明滑动验证需要渲染的目标ID。
            renderTo: "nc",
            //前端滑动验证通过时会触发该回调参数。您可以在该回调参数中将会话ID（sessionId）、签名串（sig）、请求唯一标识（token）字段记录下来，随业务请求一同发送至您的服务端调用验签。
            success: function (data) {
                loginSetInterval(type)
                if (type == 'forget') {
                    var voice = false
                    if (issms == 'voice') {
                        voice = true
                    }
                    $.star({
                        type: 'POST',
                        url: '/community-user/verify/v2/smsCode',
                        data: {
                            mobile: loginMessage.forget.phone,
                            type: 4,
                            countryCode: loginMessage.countryCode,
                            voice: voice,
                            verifySmart: {
                                sign: data.sig,
                                sessionId: data.sessionId,
                                token: data.token,
                                scene: 'nc_message_h5'
                            }
                        },
                        showerror: false,
                        success: function (res) {
                            LoginError('hide')
                            if (issms == 'sms') {
                                LoginSuccess('forget', translatesrting('短信发送成功'))
                            } else if (issms == 'voice') {
                                LoginSuccess('forget', translatesrting('语音发送成功'))
                            }
                            $('#forget-code').focus()
                            setTimeout(() => {
                                $('.J_forget_ucShow_div').removeClass('login-nc-show-option')
                                $('.J_forget_ucShow_div').removeClass('login-nc-show')
                                $('.J_forget_ucShow_div').html(' ')
                            }, 2000)
                        },
                        error: function (req) {
                            $('.J_forget_ucShow_div').removeClass('login-nc-show-option')
                            $('.J_forget_ucShow_div').removeClass('login-nc-show')
                            $('.J_forget_ucShow_div').html(' ')
                            LoginError(type, req.errorMsg)
                        }
                    });
                }
                if (type == 'register') {
                    var voice = false
                    if (issms == 'voice') {
                        voice = true
                    }
                    $.star({
                        type: 'POST',
                        url: '/community-user/verify/v2/smsCode',
                        data: {
                            mobile: loginMessage.register.phone,
                            type: 2,
                            countryCode: loginMessage.countryCode,
                            voice: voice,
                            verifySmart: {
                                sign: data.sig,
                                sessionId: data.sessionId,
                                token: data.token,
                                scene: 'nc_message_h5'
                            }
                        },
                        showerror: false,
                        success: function (res) {
                            LoginError('hide')
                            if (issms == 'sms') {
                                LoginSuccess('register', '短信发送成功')
                            } else if (issms == 'voice') {
                                LoginSuccess('register', '语音发送成功')
                            }
                            $('#register-code').focus()
                            setTimeout(() => {
                                $('.J_register_ucShow_div').removeClass('login-nc-show-option')
                                $('.J_register_ucShow_div').removeClass('login-nc-show')
                                $('.J_register_ucShow_div').html(' ')
                            }, 2000)
                        },
                        error: function (req) {
                            $('.J_register_ucShow_div').removeClass('login-nc-show-option')
                            $('.J_register_ucShow_div').removeClass('login-nc-show')
                            $('.J_register_ucShow_div').html(' ')
                            LoginError(type, req.errorMsg)
                        }
                    });
                }
                if (type == 'login') {
                    let verifySmart = {
                        sign: data.sig,
                        sessionId: data.sessionId,
                        token: data.token,
                        scene: type == 'login' ? "nc_login" : "nc_message_h5",
                    }
                    loginMessage.verifySmart = verifySmart
                    setTimeout(() => {
                        $('.J_login_ucShow_div').removeClass('login-nc-show-option')
                        $('.J_login_ucShow_div').removeClass('login-nc-show')
                        $('.J_login_ucShow_div').html(' ')
                    }, 2000)
                }
            },
            // 滑动验证失败时触发该回调参数。
            fail: function (failCode) {
                if (type == "login") {
                    LoginError(type, '验证失败' + errorCode)
                } else {
                    LoginError(type, '验证码发送失败' + failCode)
                }
            },
            // 验证码加载出现异常时触发该回调参数。
            error: function (errorCode) {
                if (type == "login") {
                    LoginError(type, '验证失败' + errorCode)
                } else {
                    LoginError(type, '验证码发送失败' + errorCode)
                }
            }
        } // 滑动验证初始化参数
        // 初始化 调用module.init对滑动验证进行初始化
        window.nc = module.init(ncOption);
    })
}
// 回车键
$(document).on('submit', '.J_form_register', function (e) {
    $('#register-btn').click()
    e.preventDefault();
})
$(document).on('submit', '.J_form_forget', function (e) {
    $('#forget-next').click()
    e.preventDefault();
})
$(document).on('submit', '.J_form_setPassWord', function (e) {
    $('#sureSetNewBtn').click()
    e.preventDefault();
})
// 倒计时
function loginSetInterval(type) {
    loginMessage.sendLoadingNum = 60
    loginMessage.sendLoading = true
    var setIntervalObject = null
    var setIntervalObjectfa = null
    if (type == 'forget') {
        setIntervalObject = $('.J_forget_ucShow')
        setIntervalObjectfa = $('.J_forget_ucShow').closest('.login-gx-input')
    } else if (type == 'register') {
        setIntervalObject = $('.J_register_ucShow')
        setIntervalObjectfa = $('.J_register_ucShow').closest('.login-gx-input')
    }
    if (setIntervalObject) {
        setIntervalObjectfa.find('.input-group-addon').removeClass('hide')
        setIntervalObject.addClass('hide')
        setIntervalObjectfa.find('.J_ucShow_count').text(loginMessage.sendLoadingNum + 's')
        loginMessage.sendLoadingFun = setInterval(() => {
            if (loginMessage.sendLoadingNum <= 0) {
                clearInterval(loginMessage.sendLoadingFun)
                loginMessage.sendLoading = false
                setIntervalObjectfa.find('.input-group-addon').addClass('hide')
                setIntervalObject.removeClass('hide')
                loginMessage.sendLoadingNum = 60
                return false
            }

            loginMessage.sendLoadingNum = loginMessage.sendLoadingNum - 1
            setIntervalObjectfa.find('.J_ucShow_count').text(loginMessage.sendLoadingNum + 's')
        }, 1000)
    } else {
        if (loginMessage.sendLoadingFun) {
            clearInterval(loginMessage.sendLoadingFun)
        }
        loginMessage.sendLoading = false
        // setIntervalObjectfa.find('.input-group-addon').addClass('hide')
        // setIntervalObject.removeClass('hide')
        loginMessage.sendLoadingNum = 60
    }
}

//结束初始动画
function StartWork() {
    const { ipcRenderer } = require("electron");

    ipcRenderer.send('start-work', 'Hello');
}
StartWork()
// 切换环境
// $(document).on('click', '.J_swich_dev', function () {
//     var $this = $(this)
//     if ($this.attr('data-type') == 'dev') {
//         saveMessage('dev', 1)
//     } else if ($this.attr('data-type') == 'test') {
//         saveMessage('dev', 2)
//     } else {
//         localStorage.removeItem('dev');
//     }
//     $.ajax({
//         type: "get",
//         url: '/Login/CheangeEnv/',
//         data: $.param({ env: $this.attr('data-type') }, true),
//         success: function (res) {

//         },
//         error: function (req) {

//         }
//     })
//     setTimeout(() => {
//         $('#login-in').click()
//     }, 300)
// })
$(document).on('click', '.J_login_url', function () {
    var $this = $(this)
    var url = $this.attr('data-url')
    OpenWebUrl(url)
})
// 掩码
$(document).on('click', '.login-phone-icon', function () {
    var $this = $(this)
    if ($this.hasClass('login-phone-icon-show')) {
        // 关闭
        $('#login-phone').attr('type', 'password')
        $('.login-phone-icon-hide').removeClass('hide')
        $('.login-phone-icon-show').addClass('hide')
        saveMessage('isphonepasshide', 'close')
    } else {
        // 打开
        $('#login-phone').attr('type', 'text')
        $('.login-phone-icon-show').removeClass('hide')
        $('.login-phone-icon-hide').addClass('hide')
        saveMessage('isphonepasshide', 'open')
    }
})


$(document).on('click', '.J_language', function () {
    var $this = $(this);
    let language = $this.attr('data-type')
    localStorage.setItem('LBWlanguage', language);

    window.location.href = "/Login/index?code=change"
    localStorage.removeItem('translateJsonNeed')
})

// 修复
$(document).on('click', '.loginDiv-login .loginDiv-Repair-content a', function () {
    $.controlAjax({
        type: 'GET',
        url: '/Login/Repair/',
        success: function (res) {
        },
        error: function (req) {
        }
    })
})




// const { ipcRenderer } = require("electron");

var startMessage = {
    aniamtefun: null,
    progressnum: 0,
    isfirst: true
}
localStorage.removeItem('OutInvite')
ipcRenderer.send('start-work', 'Hello');


var getipcRenderer21 = ''
var getipcRenderer42 = ''

ipcRenderer.on('WebIpc', (event, message) => {
    let jsonmessage = JSON.parse(message.jsonInfo)
    if (message.msgType == 21) {
        clearInterval(loginMessage.updateSetTime)
        clearTimeout(loginMessage.updateSetTimeclose)
        let localVersion = getSaveMessage('Version')
        let oldVersion = jsonmessage.oldVersion
        let NewVersion = jsonmessage.version
        if (!localVersion && oldVersion == NewVersion) {
            console.log('第一次')
            localStorage.setItem('Version', JSON.stringify(jsonmessage.version))
            envelopeFun()//更新日志
        } else {
            if (oldVersion != NewVersion) {
                // 更新
                console.log('更新中')
            }
            if (oldVersion == NewVersion) {
                // 更新后 & 正常
                if (localVersion != oldVersion) {
                    // 更新后
                    console.log('更新后')
                    localStorage.setItem('Version', JSON.stringify(jsonmessage.version))
                    envelopeFun()//更新日志
                } else {
                    // 正常  
                    console.log('正常')
                    localStorage.setItem('Version', JSON.stringify(jsonmessage.version))
                }
            }
        }
        let test = ''
        if (getSaveMessage('test')) {
            test = '-' + 'test'
        }
        if (getSaveMessage('pre')) {
            test = '-' + 'pre'
        }
        $(".header-version").text(translatesrting('版本号') + ':' + oldVersion + test)
        //版本信息
        if (jsonmessage.status == 1) {
            //更新
            $('.login-pre').removeClass('hide')
            $('.J_updataCheck').text(translatesrting('下载中'))
            // $('.upload').addClass('hide')
            // $('.updown').removeClass('hide')
            ipcRenderer.send('Main', {
                msgType: "CheckUpdate",
                jsonInfo: {
                    status: 1
                }
            });
            trackingFunc('update_start')
        } else {
            //正常
            loginnormal()
            getipcRenderer21 = 'canin'
            if (getipcRenderer42 == 'get') {
                if (loginMessage.isLoginToken) {
                    $('#login-in').click()
                }
            }
        }
    } else if (message.msgType == 20 && jsonmessage.sign == 'updateClient' && Number(startMessage.progressnum) < Number(jsonmessage.nowPercentage)) {
        startMessage.progressnum = Number(jsonmessage.nowPercentage)
        $('.login-pre-num').text(jsonmessage.nowPercentage + '%')
        $('.login-pre-div').css('width', jsonmessage.nowPercentage + "%")
        if (startMessage.progressnum >= 90 && startMessage.isfirst) {
            startMessage.isfirst = false
            trackingFunc('update_end')
        }
    }
    if (message.msgType == 42) {
        let joinDetails = JSON.parse(JSON.parse(message.jsonInfo))
        saveMessage('OutInvite', joinDetails)
        getipcRenderer42 = 'get'
        if (getipcRenderer21 == 'canin') {
            if (loginMessage.isLoginToken) {
                $('#login-in').click()
            }
        }
        trackingFunc('url_open')
    }
});
// ipcRenderer.on('WebIpc', (event, message) => {
//     let jsonmessage = JSON.parse(message.jsonInfo)
//     if (message.msgType == 21) {

//         //版本信息
//         if (jsonmessage.status == 1) {
//             updatefun()
//         } else {


//         }
//     }
// });
// 客服参数
var qimoClientId = { userId: '', nickName: '' }
$(document).on('click', '#J_showqimoClient', function () {
    qimoChatClick()
})

var noticeMessage = {
    list: [],
    noticeSwiper: null,
    closeLogin: false
}
// 公告
getNotice()
function getNotice() {
    $.star({
        type: 'GET',
        url: '/battlecenter/notice/v1/maintain',
        showerror: false,
        data: {},
        success: function (res) {
            if (res.data.isWhiteList) {
                saveMessage('isNoticeWhiteList', 1)
            } else {
                saveMessage('isNoticeWhiteList', 2)
            }
            if (res.data.maintainVO.content) {
                let type = ''
                if (res.data.maintainVO.type == 0) {
                    type = 'serious'
                } else if (res.data.maintainVO.type == 1) {
                    type = 'tips'
                } else {
                    type = 'warning'
                }
                if (type == 'tips') {
                    let myDate = new Date;
                    let myDatedate = myDate.getDate()
                    $('.login-notice').html(template('notice-item', { type: type }))
                    $('.login-notice-content').html(res.data.maintainVO.content)
                    if (getSaveMessage('allnotice') && getSaveMessage('allnotice').id == res.data.maintainVO.id && getSaveMessage('allnotice').day == myDatedate) {
                        $('.login-notice-close').addClass('hide')
                        $('.login-notice-content').addClass('hide')
                        // return false
                    } else {
                        let myDate = new Date;
                        let myDatedate = myDate.getDate()
                        saveMessage('allnotice', { id: res.data.maintainVO.id, day: myDatedate })
                    }
                } else {
                    $('.login-notice').html(template('notice-item', { type: type }))
                    $('.login-notice-content').html(res.data.maintainVO.content)
                    if (type == 'serious' && !res.data.isWhiteList) {
                        noticeMessage.closeLogin = true
                        $('.login-mark').removeClass('hide')
                    } else {
                        noticeMessage.closeLogin = false
                        $('.login-mark').addClass('hide')
                    }
                }
            } else {
                $('.login-notice').html('')
            }
        },
        error: function (req) {

        }
    });
}
setInterval(function () {
    getNotice()
}, 30000)
$(document).on('click', '.login-notice-close', function () {
    var $this = $(this)
    $this.addClass('hide')
    $this.closest('.login-notice').find('.login-notice-content').addClass('hide')
})
$(document).on('mouseenter', '.login-notice-icon', function () {
    var $this = $(this)
    if ($this.closest('.login-notice').find('.login-notice-close').hasClass('hide')) {
        $this.closest('.login-notice').find('.login-notice-content').removeClass('hide')
        $this.closest('.login-notice').find('.login-notice-content').css({ opacity: 0 })
        $this.closest('.login-notice').find('.login-notice-content').animate({ opacity: 1 }, 100, 'linear', function () {
            $this.closest('.login-notice').find('.login-notice-content').css({ opacity: 1 })
        })
    }
})
$(document).on('mouseleave', '.login-notice-icon', function () {
    var $this = $(this)
    if ($this.closest('.login-notice').find('.login-notice-close').hasClass('hide')) {
        $this.closest('.login-notice').find('.login-notice-content').animate({ opacity: 0 }, 100, 'linear', function () {
            $this.closest('.login-notice').find('.login-notice-content').addClass('hide')
        })
    }
})
// setInterval(() => {
//     getNotice()
// }, 300000)
// function getNotice() {
//     $.star({
//         type: 'GET',
//         url: '/battlecenter/notice/v1/list',
//         showerror: false,
//         data: {
//             size: 5,
//             type: 1
//         },
//         success: function (res) {
//             noticeMessage.list = res.data
//             if (noticeMessage.list.length > 0) {
//                 $('.login-notice').removeClass('hide')
//                 $('.login-notice .swiper-wrapper').html(template('notice-item', { list: noticeMessage.list }))
//                 if (noticeMessage.noticeSwiper) {
//                     noticeMessage.noticeSwiper.destroy()
//                 }
//                 if (noticeMessage.list.length > 1) {
//                     if (noticeMessage.noticeSwiper) {
//                         noticeMessage.noticeSwiper.destroy()
//                     }
//                     noticeMessage.noticeSwiper = new Swiper('.login-notice', {
//                         autoplay: {
//                             delay: 5000,
//                             stopOnLastSlide: false,
//                             disableOnInteraction: true,
//                         },
//                         direction: 'vertical',
//                         allowTouchMove: true,
//                         loop: true,
//                     })
//                 }
//             }
//         },
//         error: function (req) {

//         }
//     });
// }
var firstHallMessage = {
    getlist: null,
    getbanner: null,
    getrangklist: null,
    getlivelist: null
}
$.star({
    type: 'GET',
    url: '/battlecenter/article/v1/list',
    showerror: false,
    data: {
        classifyId: 0,
        pageNum: 1,
        pageSize: 3
    },
    success: function (res) {
        firstHallMessage.getlist = res
    },
    error: function (req) {
    }
});
$.star({
    type: 'GET',
    url: '/battlecenter/article/v1/banner',
    showerror: true,
    data: {
    },
    success: function (res) {
        firstHallMessage.getbanner = res
    },
    error: function (req) {

    }
});
$.star({
    type: 'GET',
    url: '/battlecenter/qualifying/v1/information/rank',
    showerror: true,
    data: {
        size: 5
    },
    success: function (res) {
        firstHallMessage.getrangklist = res
    },
    error: function (req) {

    }
})
$.star({
    type: 'GET',
    url: '/battlecenter/live/v1/anchor',
    showerror: true,
    data: {
    },
    success: function (res) {
        firstHallMessage.getlivelist = res
    },
    error: function (req) {

    }
})
// 保险 网络情况慢时
// 菜单功能去除
setInterval(() => {
    // 菜单功能
    if (localStorage.getItem('JmenuOpenIndex') && localStorage.getItem('JmenuOpenIndex') == 1) {
        // showtoast({
        //     message: translatesrting('请登录后操作')
        // })
        localStorage.removeItem('JmenuOpenIndex')
    }
    // 菜单功能
    if (localStorage.getItem('JmenuChangeUser') && localStorage.getItem('JmenuChangeUser') == 1) {
        showtoast({
            message: translatesrting('请登录后操作')
        })
        localStorage.removeItem('JmenuChangeUser')
    }
    if (localStorage.getItem('JmenuSettingOpen') && localStorage.getItem('JmenuSettingOpen') == 1) {
        showtoast({
            message: translatesrting('请登录后操作')
        })
        localStorage.removeItem('JmenuSettingOpen')
    }
    if (localStorage.getItem('Jmenuclose') && localStorage.getItem('Jmenuclose') == 1) {
        $('.login-header .header-close').click()
        localStorage.removeItem('Jmenuclose')
    }
}, 500);
// 报错
function DleteCheatFile() {
    $.controlAjax({
        type: "GET",
        url: '/Login/DleteCheatFile/',
        success: function (res) {
        },
        error: function (req) {
        }
    })
}
function updataFun() {
    $.star({
        type: 'GET',
        url: '/battlecenter/redwar/version/v1/update/log',
        data: {
            version: getSaveMessage('Version')
        },
        success: function (res) {
            if (res.data.content) {
                $('#updataMessageModal').find('.modal-dialog').html(template('updataMessageModal-script', { message: res.data }))
                $('.updataMessageModal-content-text').html(res.data.content)
                $('#updataMessageModal').modal('show')
            }
        },
        error: function (req) {
        }
    })
}
function envelopeFun() {
    // $('#envelopeModal').find('.modal-dialog').html(template('envelopeModal-script'))
    // $('#envelopeModal').modal('show')
    // setTimeout(function () {
    //     $('.envelopeModal-btn').removeClass('hide')
    //     $('.envelopeModal-btn').animate({ opacity: 1 })
    // }, 5000)
    // $('#envelopeModal').on('hide.bs.modal', function () {
    updataFun()
    // })
}
function loginnormal() {
    $(document).on('submit', '.J_form_login', function (e) {
        $('#login-in').click()
        e.preventDefault();
    })
    $('.J_updataCheck').addClass('hide')
    $('#login-in').removeClass('hide')
    $('.loginChangeOne').removeClass('hide')
    $('.loginChangeTwo').addClass('hide')
    $('.loginDiv-login-content .gx-menu').gxmenu({
        height: "30px",
        top: "26px",
        clickHide: true//点击内部是否消失
    }, function (e) {

    })
}
function isbusy() {
    $.star({
        type: 'GET',
        url: '/battlecenter/redwar/room/v1/busyness',
        data: {},
        success: function (res) {
            if (res.data) {
                localStorage.setItem('isbusy', true)
            } else {
                localStorage.removeItem('isbusy')
            }
        },
        error: function (req) {
        }
    })
}

$('.login-bg').attr('src', '/images/login/login-bg.mp4')
$('.login-bg').removeClass('hide')
$('.login-bg').on('play', function () {
    $('body').css({ 'transform': 'scale(1)', 'opacity': '1' })
})
setTimeout(() => {
    $('body').css({ 'transform': 'scale(1)', 'opacity': '1' })
    $('body').css({ 'backgroundColor': '#28375d' })
}, 2000)
var getdeviceOSTime = 4
function getdeviceOS() {
    $.ajax({
        type: "get",
        url: '/api/login/os/info/',
        data: "",
        success: function (res) {
            getdeviceOSTime = getdeviceOSTime - 1
            let resdata = {
                cpu: res.cpu,
                memory: res.memory,
                os: res.os
            }
            loginMessage.deviceOS = JSON.stringify(resdata)
            if (res.hardwareCode) {
                loginMessage.machineCode = res.hardwareCode
            } else {
                if (getdeviceOSTime > 0) {
                    setTimeout(() => {
                        getdeviceOS()
                    }, 300)
                }
            }
            localStorage.setItem('utm_source', res.channel)
        },
        error: function (req) {

        }
    })
}

if (localStorage.getItem('translateJsonNeed')) {
    let sendtranslateJsonNeed = JSON.parse(localStorage.getItem('translateJsonNeed'))
    let originalName = []
    for (var p in sendtranslateJsonNeed) {
        originalName.push(p)
    }
    $.star({
        type: 'POST',
        url: '/battlecenter/platform/v1/language/record',
        data: {
            languageType: localStorage.getItem('LBWlanguage') || 'zh',
            originalName
        },
        success: function (res) {
            localStorage.removeItem('translateJsonNeed')
        },
        error: function (req) {
        }
    })
}