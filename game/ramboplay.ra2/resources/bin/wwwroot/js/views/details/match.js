// 翻译
$('.detail-container').html(template('detail-container-script'))
$('.details-all-close-text').text(translatesrting('离开/解散'))
var detailsMessage = {
    roomNo: null,//房间号
    HostId: 0,//房主ID 用于聊天判断
    maxPlayers: 0,//房间最大人数
    gameOption: [],//游戏设置
    isHost: false,//是否房主
    optionsCountry: [],//国家配置
    localUserlist: [],//本地维护的用户信息
    counTimefun: null,
    counTimenum: 0,
}
$('.details-all-close-sure').addClass('hide')
$('.window-operation.header-close').addClass('hide')
ipcRenderer.on('WebIpc', (event, message) => {
    var res = JSON.parse(message.jsonInfo)
    if (message.msgType == 10) {
        // 用户列表
        if (canmsgType10) {
            ipcUser(res, 'update')
        }
    }
    if (message.msgType == 18) {
        // ping值
        var res = JSON.parse(message.jsonInfo)
        if (res.userId != 0) {
            $('.detail-user-list-item').each(function () {
                var id = $(this).attr('data-id')
                if (id == res.userId) {
                    $(this).find('.details-ping-iconfont').removeClass('hide')
                    $(this).find('.detail-ping-hover-tunnelPing-span').text(res.tunnelPing + 'MS')
                    $(this).find('.detail-ping-hover-tunnelPkLose-span').text(res.tunnelPkLose + '%')
                    if (res.tunnelPing >= 300) {
                        $(this).find('.details-ping').css('color', '#FF4444')
                        $(this).find('.details-ping-svg').css('fill', '#FF4444')
                    } else if (res.tunnelPing >= 200) {
                        $(this).find('.details-ping').css('color', '#FFA333')
                        $(this).find('.details-ping-svg').css('fill', '#FFA333')
                    } else {
                        $(this).find('.details-ping').css('color', '#A2FF33')
                        $(this).find('.details-ping-svg').css('fill', '#A2FF33')
                    }
                    if (res.tunnelPkLose > 10) {
                        $(this).find('.details-ping').css('color', '#FF4444')
                        $(this).find('.details-ping-svg').css('fill', '#FF4444')
                        $(this).find('.details-ping-iconfont .detail-ping-hover-tunnelPing-span').text(translatesrting('丢包'))
                    }
                }
            })
        }
    }
    if (message.msgType == 15) {
        // 修改房间配置通知
        var res = JSON.parse(message.jsonInfo)
        var gameOption = detailsMessage.gameOption
        res.map((item) => {
            gameOption.map((options) => {
                if (options.key == item.key) {
                    options.value = item.value
                }
            })
        })
        detailsMessage.gameOption = gameOption
        JSgameOption(detailsMessage.gameOption)
    }
    if (message.msgType == 24) {
        // 修改房间地图配置通知
        var res = JSON.parse(message.jsonInfo)
        if (res.imageUrl || res.mapPreviewBase64) {
            oneImgCache(res.imageUrl).then((Cachereturn) => {
                res.imageUrl = Cachereturn
                $('.detail-map-content').html(template('detail-map-script', res))
                detailsImgSize()
            })
        } else {
            detailsImgSize()
        }
    }
    if (message.msgType == 22) {
        // 聊天信息
        var res = JSON.parse(message.jsonInfo)
        if (res.SenderId != userInfo.userId) {
            if (detailsMessage.gameType == 4) {
                res.SenderName = translatesrting('旗鼓相当的对手')
            }
            addChatMessage(false, res.SenderName, res.Message)
        }
    } else if (message.msgType == 30) {
        let getuser = []
        getuser.push(res)
        ipcUser(getuser, 'error')
    }
})
var canmsgType10 = false
startMatch(false)
function startMatch(type) {
    // 创建时房间数据  完全听接口
    var details = getSaveMessage('msgType2', false)
    if (type) {
        detailsMessage.gameType = details.isLadder ? 4 : 3 //4天梯
        // detailsMessage.gameType = 3
        if (detailsMessage.gameType == 4) {
            // 埋点
            wsUserStatus(4)
            trackingFunc('rank_success')
            // 天梯
            $('.detail-container').addClass('detail-rank')
            // $('.rank-btn').removeClass('hide')
        } else {
            // 埋点
            wsUserStatus(6)
            trackingFunc('Match_success')
            $('.detail-container').removeClass('detail-rank')
            // $('.rank-btn').addClass('hide')
        }
        canmsgType10 = true
        detailsMessage.roomNo = details.roomNo//房间号
        detailsMessage.mapName = details.mapName//房间号
        comOption.optionsMode.map((item) => {
            if (item.code == details.mode) {
                details.Jmode = item.name
            }
        })

        // oneImgCache(details.imageUrl).then((Cachereturn) => {
        // details.imageUrl = Cachereturn
        $.star({
            type: 'GET',
            url: '/battlecenter/redwar/map/v1/get-map',
            showerror: false,
            data: {
                mapSha1: details.mapSha1,
            },
            success: function (res) {
                let dellImageUrl = details.imageUrl
                details.imageUrl = ''
                details.mapName = details.mapName + '&&||' + res.data.enName
                $('.detail-map-content').html(template('detail-map-script', details))
                dellDetailImage(dellImageUrl)
                if (res.data && res.data.description) {
                    $('.detail-map-desc').removeClass('hide')
                    $('.detail-map-desc').text(res.data.description)
                }
            },
            error: function (req) {
            }
        });
        if (details.gameVersion == '1') {
            // 原版
            detailsMessage.optionsCountry = comOption.optionsCountry
            detailsMessage.optionsCountry.splice(detailsMessage.optionsCountry.length - 1, 1)
        } else {
            detailsMessage.optionsCountry = comOption.optionsCountry
        }
        detailsMessage.optionsCountry.map((item, index) => {
            if (item.sideId == 10) {
                detailsMessage.optionsCountry.splice(index, 1)
            }
        })
        // 创建时用户数据
        var userlist = details.players
        // var userlist = getSaveMessage('msgType10')

        // userlist.map((item) => {
        //     if (item.isHost && item.userId == userInfo.userId) {
        //         detailsMessage.isHost = true// 判断房主 自动准备
        //         detailsMessage.isReady = true
        //     }
        //     if (item.isHost) {
        //         detailsMessage.HostId = item.userId//房主ID
        //     }
        // })
        detailsMessage.isReady = false
        detailsMessage.HostId = null
        JSgameOption(details.gameOption)//游戏设置
        scriptuser(userlist)//用户设置
        detailsImgSize()
        startCountTime(details.countdownTime)
        // })
    } else {
        $.controlAjax({
            type: "get",
            url: '/api/details/inroom/info',
            showerror: true,
            data: {
                matchIng: true
            },
            success: function (res) {
                if (res.data.gameOption == [] || !res.data.gameOption) {
                    setTimeout(() => {
                        startMatch(false)
                    }, 1000)
                } else {
                    saveMessage('msgType2', res.data)
                    startMatch(true)
                }
            },
            error: function (req) {

            }
        })
    }
}

function startCountTime(num) {
    detailsMessage.counTimenum = num
    detailsMessage.counTimefun = setInterval(() => {
        $('.match-countTime').text(detailsMessage.counTimenum)
        detailsMessage.counTimenum = detailsMessage.counTimenum - 1
        if (detailsMessage.counTimenum <= 5) {
            $('.detail-user-infor-content').addClass('nochoose')
        }
        if (detailsMessage.counTimenum < 0) {
            $('.detail-container-col-right .rank-btn').addClass('hide')
            $('.detail-container-col-right .match-title').removeClass('hide')
            $('.match-title svg').addClass('hide')
            $('.match-title-s').addClass('hide')
            $('.detail-container-col-right .match-title .match-countTime').text(translatesrting('游戏即将开始'))
            $('.header-window .details-all-close').removeClass('hide')
            clearInterval(detailsMessage.counTimefun)
        }
        if (detailsMessage.counTimenum == 3) {
            parent.startModal('fast')
        }
    }, 1000)
}
// 渲染方程式
function scriptuser(userList) {
    for (var i = 0; i < Number(detailsMessage.maxPlayers); i++) {
        detailsMessage.localUserlist.push({ userId: 'empty' })
    }
    userList.map((item, index) => {
        detailsMessage.optionsCountry.map((optionC) => {
            if (item.sideId == optionC.sideId) {
                item.JsCountry = optionC
            }
        })
        comOption.optionsColor.map((optionC) => {
            if (item.colorId == optionC.colorId) {
                item.JsColor = optionC
            }
        })
        comOption.optionsTeam.map((optionC) => {
            if (item.teamId == optionC.teamId) {
                item.JsTeam = optionC
            }
        })
        detailsMessage.localUserlist[index] = item
    })
    // $('.detail-user-list').html(template('detail-user-list-script', {
    //     userList: detailsMessage.localUserlist,
    //     optionsCountry: detailsMessage.optionsCountry,
    //     optionsColor: comOption.optionsColor,
    //     myuserId: userInfo.userId,
    //     gameType: detailsMessage.gameType,
    //     isHost: detailsMessage.isHost,
    //     notr: false
    // }))
    detailsMessage.localUserlist.map((item) => {
        item.customField = customFieldFun('decode', item.customField);
    })
    $('.detail-user-infor-left-bg-in .detail-user-list-match').html(template('detail-user-list-match-script', {
        userList: [detailsMessage.localUserlist[0]],
        optionsCountry: detailsMessage.optionsCountry,
        optionsColor: comOption.optionsColor,
        myuserId: userInfo.userId,
        gameType: detailsMessage.gameType,
        isHost: detailsMessage.isHost,
        notr: false
    }))

    $('.detail-user-infor-right-bg-in .detail-user-list-match').html(template('detail-user-list-match-script', {
        userList: [detailsMessage.localUserlist[1]],
        optionsCountry: detailsMessage.optionsCountry,
        optionsColor: comOption.optionsColor,
        myuserId: userInfo.userId,
        gameType: detailsMessage.gameType,
        isHost: detailsMessage.isHost,
        notr: false
    }))
    selectfun()
}
// 选项JS
function selectfun() {
    $('.canChange .country-select').gxmenu({
        height: "20px",
        top: "40px",
        clickHide: true
    }, function (e) {

    })
    //用于解决颜色必须不重复
    let isgetColor = []
    $('.color-select-div').each(function () {
        isgetColor.push($(this).attr('data-code'))
    })
    $('.J_chooseColor').each(function () {
        let inChooseColor = $(this).attr('data-code')
        if (isgetColor.indexOf(inChooseColor) != "-1") {
            $(this).closest('.gx-menu-li').addClass('hide')
        } else {
            $(this).closest('.gx-menu-li').removeClass('hide')
        }
    })


    $('.canChange .color-select').gxmenu({
        height: "30px",
        top: "40px",
        clickHide: true
    }, function (e) {

    })
    $('.canChange .detail-robot-menu').gxmenu({
        height: "30px",
        top: "20px",
        clickHide: true
    }, function (e) {

    })
    $('.canChange .detail-robot-menu').each(function (index, e) {
        $(this).css('zIndex', (10 - index))
    })
    var isallready = true
    detailsMessage.localUserlist.map((item) => {
        if (!item.ready) {
            isallready = false
        }
    })
}
// 游戏设置
function JSgameOption(getoptions) {
    detailsMessage.gameOption = getoptions
    detailsMessage.gameOption.map((options) => {
        if (options.value == 'true' || options.value == 'True') {
            options.value = true
        }
        if (options.value == 'false' || options.value == 'False') {
            options.value = false
        }
        if (options.key == 'MaxPlayers') {
            detailsMessage.maxPlayers = options.value
        }
    })
    $('.detail-select-option').html(template('detail-select-option-script', {
        options: detailsMessage.gameOption,
        isHost: detailsMessage.isHost
    }))//右侧游戏设置
    $('.detail-swich-option').html(template('detail-swich-option-script', { options: detailsMessage.gameOption }))//右侧游戏设置
}
// 房间配置
$(document).on('change', '.detail-select-option-item input', function () {
    var $this = $(this)
    if ($this.is(':checked')) {
        $this.prop('checked', false)
    } else {
        $this.prop('checked', true)
    }
    return false;
})
// 发送聊天
$(document).on('submit', '.J_chatForm', function (e) {
    chatUp()
    e.preventDefault();
})
function chatUp() {
    var chatmesg = $('.J_chatInput').val();
    if (chatmesg == "") {
        return false;
    }
    ipcRenderer.send('Main', {
        msgType: "ChatMessage",
        jsonInfo: {
            msg: chatmesg
        }
    });
    $('.J_chatInput').val("")
    $('.J_chatInput').focus()
    addChatMessage(detailsMessage.isHost, userInfo.username, chatmesg, true)
}
// 添加聊天
function addChatMessage(isHost, name, message, isSelf) {
    player_sound_sendchat()
    var reg = new RegExp("<", "g")
    message = message.replace(reg, "<span>" + '<' + "</span>");
    if (false) {
        $('.detail-container-right-chat-list').append('<div class="at-row"><div class="at-col--auto ft-orange">' + name + '(房主)： </div><div class="at-col at-col--wrap">' + message + '</div></div>')
        $('.detail-container-right-chat-list').scrollTop($('.detail-container-right-chat-list').prop('scrollHeight'))
    } else if (isSelf) {
        $('.detail-container-right-chat-list').append('<div class="at-row"><div class="at-col--auto ft-indigo">' + name + '： </div><div class="at-col at-col--wrap ft-indigo">' + message + '</div></div>')
        $('.detail-container-right-chat-list').scrollTop($('.detail-container-right-chat-list').prop('scrollHeight'))
    } else {
        $('.detail-container-right-chat-list').append('<div class="at-row"><div class="at-col--auto">' + name + '： </div><div class="at-col at-col--wrap">' + message + '</div></div>')
        $('.detail-container-right-chat-list').scrollTop($('.detail-container-right-chat-list').prop('scrollHeight'))
    }
    return false
}
// 添加机器人
$(document).on('click', '.J_addRobot', function () {
    var userNum = 0
    detailsMessage.localUserlist.map((item) => {
        if (item.userId != "empty") {
            userNum = userNum + 1
        }
    })
    if (Number(detailsMessage.maxPlayers) <= userNum) {
        return false
    }
    var indexnum = $(this).attr('data-index')
    ipcRenderer.send('Main', {
        msgType: "SavePlayer",
        jsonInfo: {
            isAI: true,
            aILevel: 2,
            Mark: indexnum
        }
    });
})
// 退出
$(document).on('click', '.details-all-close', function () {
    $.controlAjax({
        type: "get",
        url: '/Details/LeaveGameRoom/',
        data: {},
        showerror: true,
        success: function (res) {
            localStorage.removeItem('msgType10')
            parent.hallMessage.closeRoom('match')
        },
        error: function (req) {

        }
    })
})
// 选择机器人等级
$(document).on('click', '.chooseAiLevel', function () {
    var $this = $(this)
    var id = $this.closest('tr').attr('data-id')
    var level = $this.attr('data-level')
    ipcRenderer.send('Main', {
        msgType: "SavePlayer",
        jsonInfo: {
            isAI: true,
            userId: id,
            aILevel: level
        }
    });
})
// 选择颜色
$(document).on('click', '.J_chooseColor', function () {
    var $this = $(this)
    var color = $this.attr('data-color')
    var id = $this.closest('tr').attr('data-id')
    var code = $this.attr('data-code')

    let JsColor = null
    comOption.optionsColor.map((optionC) => {
        if (code == optionC.colorId) {
            JsColor = optionC
        }
    })
    $.controlAjax({
        type: "POST",
        url: '/Details/SvaePlayerInfo',
        data: {
            userId: id,
            colorId: code
        },
        showerror: false,
        success: function (res) {
            detailsMessage.localUserlist.map((item) => {
                if (item.userId == userInfo.userId) {
                    item.colorId = code
                }
            })
            $this.closest('.gx-menu').html(template('detail-user-list-change-script', {
                type: 'colorId',
                item: { 'colorId': code, 'JsColor': JsColor },
                optionsColor: comOption.optionsColor
            }))

            $('.canChange .color-select').gxmenu({
                height: "30px",
                top: "40px",
                clickHide: true
            }, function (e) {

            })
        },
        error: function (req) {

        }
    })

    // ipcRenderer.send('Main', {
    //     msgType: "SavePlayer",
    //     jsonInfo: {
    //         userId: id,
    //         colorId: code
    //     }
    // });
})
// 选择国家
$(document).on('click', '.J_chooseCountry', function () {
    var $this = $(this)
    var id = $(this).closest('tr').attr('data-id')
    var sideId = $(this).attr('data-code')

    let JsCountry = null
    detailsMessage.optionsCountry.map((optionC) => {
        if (sideId == optionC.sideId) {
            JsCountry = optionC
        }
    })

    $.controlAjax({
        type: "POST",
        url: '/Details/SvaePlayerInfo',
        data: {
            userId: id,
            sideId: sideId
        },
        showerror: false,
        success: function (res) {
            $this.closest('.gx-menu').find('.gx-menu-in').html(template('detail-user-list-change-script', {
                type: 'sideId',
                item: { 'sideId': sideId, 'JsCountry': JsCountry }
            }))

            detailsMessage.localUserlist.map((item) => {
                if (item.userId == userInfo.userId) {
                    item.sideId = sideId
                }
            })
        },
        error: function (req) {

        }
    })





    // ipcRenderer.send('Main', {
    //     msgType: "SavePlayer",
    //     jsonInfo: {
    //         userId: id,
    //         sideId: sideId
    //     }
    // });
})
function ipcUser(userarray, type) {
    var addUser = []//新加入用户
    var oldUser = []//已有用户
    var delUser = []//离开的用户
    userarray.map((newItem) => {
        detailsMessage.optionsCountry.map((optionC) => {
            if (newItem.sideId == optionC.sideId) {
                newItem.JsCountry = optionC
            }
        })
        comOption.optionsColor.map((optionC) => {
            if (newItem.colorId == optionC.colorId) {
                newItem.JsColor = optionC
            }
        })
        comOption.optionsTeam.map((optionC) => {
            if (newItem.teamId == optionC.teamId) {
                newItem.JsTeam = optionC
            }
        })
        var isadd = true
        detailsMessage.localUserlist.map((localItem) => {
            if (newItem.userId == localItem.userId) {
                oldUser.push(newItem)
                isadd = false
            }
        })
        if (isadd) {
            addUser.push(newItem)
        }
    })
    detailsMessage.localUserlist.map((localItem) => {
        var isout = true
        userarray.map((newItem) => {
            if (newItem.userId == localItem.userId) {
                isout = false
            }
        })
        if (isout) {
            delUser.push(localItem)
        }
    })
    if (delUser.length > 0 && type != 'error') {
        // 删除消失人
        delUser.map((item) => {
            $('.detail-user-list-item').each(function () {
                if ($(this).attr('data-id') == item.userId) {
                    $(this).removeClass('canChange')
                    $(this).addClass('detail-user-list-item-empty')
                    $(this).attr('data-id', null)
                    $(this).html(template('detail-user-list-script', {
                        userList: [{ userId: 'empty' }],
                        optionsCountry: detailsMessage.optionsCountry,
                        optionsColor: comOption.optionsColor,
                        myuserId: userInfo.userId,
                        gameType: detailsMessage.gameType,
                        isHost: detailsMessage.isHost,
                        notr: true
                    }))
                }
            })
        })
    }
    if (addUser.length > 0 && type != 'error') {
        // 添加新人
        addUser.map((item) => {
            detailsMessage.optionsCountry.map((optionC) => {
                if (item.sideId == optionC.sideId) {
                    item.JsCountry = optionC
                }
            })
            comOption.optionsColor.map((optionC) => {
                if (item.colorId == optionC.colorId) {
                    item.JsColor = optionC
                }
            })
            comOption.optionsTeam.map((optionC) => {
                if (item.teamId == optionC.teamId) {
                    item.JsTeam = optionC
                }
            })
            $('.detail-user-list-item-empty').eq(0).html(template('detail-user-list-script', {
                userList: [item],
                optionsCountry: detailsMessage.optionsCountry,
                optionsColor: comOption.optionsColor,
                myuserId: userInfo.userId,
                gameType: detailsMessage.gameType,
                isHost: detailsMessage.isHost,
                notr: true
            }))
            if (item.userId == userInfo.userId) {
                $('.detail-user-list-item-empty').eq(0).addClass('canChange')
            } else if (detailsMessage.isHost && item.isAI) {
                $('.detail-user-list-item-empty').eq(0).addClass('canChange')
            }
            $('.detail-user-list-item-empty').eq(0).attr('data-id', item.userId)
            $('.detail-user-list-item-empty').eq(0).removeClass('detail-user-list-item-empty')
        })
    }
    if (oldUser.length > 0) {
        // 现有用户状态修改
        oldUser.map((oldUseritem) => {
            detailsMessage.localUserlist.map((localItem) => {
                if (localItem.userId == oldUseritem.userId) {
                    if (oldUseritem.userId == userInfo.userId && type != 'error') {
                        return false
                    }
                    // 进行同步比较
                    $('.detail-user-list-item').each(function () {
                        if ($(this).attr('data-id') == oldUseritem.userId) {
                            if (localItem.ready != oldUseritem.ready) {
                                // 准备状态
                                if (oldUseritem.ready) {
                                    $(this).find('.detail-container-ready-img').removeClass('hide')
                                } else {
                                    $(this).find('.detail-container-ready-img').addClass('hide')
                                }
                            }
                            if (localItem.sideId != oldUseritem.sideId) {
                                // 国家状态
                                $(this).find('.country-select .gx-menu-in').html(template('detail-user-list-change-script', {
                                    type: 'sideId',
                                    item: oldUseritem
                                }))
                            }
                            if (localItem.colorId != oldUseritem.colorId) {
                                // 颜色状态
                                $(this).find('.color-select').html(template('detail-user-list-change-script', {
                                    type: 'colorId',
                                    item: oldUseritem,
                                    optionsColor: comOption.optionsColor
                                }))
                            }
                            if (oldUseritem && (localItem.aiLevel != oldUseritem.aiLevel)) {
                                // 队伍状态
                                $(this).find('.detail-robot-menu .gx-menu-in').html(template('detail-user-list-change-script', {
                                    type: 'aILevel',
                                    item: oldUseritem
                                }))
                            }
                        }
                    })

                }
            })
        })
    }
    if (type != 'error') {
        detailsMessage.localUserlist = userarray
    } else if (type == 'error') {
        detailsMessage.localUserlist.map((item, index) => {
            if (item.userId == userarray[0].userId) {
                detailsMessage.localUserlist[index] = userarray[0]
            }
        })
    }
    selectfun()
}

var mapRandomMove = null

function detailsImgSize() {
    clearInterval(mapRandomMove)

    var _image = $('.detail-map-content .detail-map-img-img')
    _image.on('load', function () {
        var realImgWidth = _image.width();
        var realImgHeight = _image.height();
        $('.detail-map-content .detail-map-img-relative').css({ top: '0', left: '0', transition: '8s' })
        if (realImgHeight / realImgWidth >= 280 / 856) {
            $('.detail-map-content .detail-map-img-img').css({ 'width': '100%', 'height': 'auto' })
            $('.detail-map-content .detail-map-img-relative').css({ 'width': 'calc(100% + 100px)', 'height': 'auto' })
        } else {
            $('.detail-map-content .detail-map-img-img').css({ 'width': 'auto', 'height': '100%' })
            $('.detail-map-content .detail-map-img-relative').css({ 'width': 'auto', 'height': 'calc(100% + 100px)' })
        }

        MapMoveFn()
    })
}

var MapMoveFlag = true;

function MapMoveFn() {
    let End_X = $('.detail-map-content .detail-map-img').width() - $('.detail-map-content .detail-map-img-relative').width()
    let End_Y = $('.detail-map-content .detail-map-img').height() - $('.detail-map-content .detail-map-img-relative').height()

    let temp_num = 1
    mapRandomMove = setInterval(() => {
        if (temp_num > 4) {
            temp_num = 1
        }

        switch (temp_num) {
            case 1:
                $('.detail-map-content .detail-map-img-relative').css({ left: End_X, top: End_Y, transition: '8s' })
                break;
            case 2:
                $('.detail-map-content .detail-map-img-relative').css({ left: '0px', top: End_Y, transition: '8s' })
                break;
            case 3:
                $('.detail-map-content .detail-map-img-relative').css({ left: End_X, top: '0px', transition: '8s' })
                break;
            case 4:
                $('.detail-map-content .detail-map-img-relative').css({ left: '0px', top: '0px', transition: '8s' })
                break;
        }

        temp_num++
    }, 8100);
}

$(document).on('mouseenter', '.detail-map-content .detail-map-img-relative', function () {
    $('.detail-map-content .detail-map-img-relative').css({ transition: '0s' })
    clearInterval(mapRandomMove)
})

$(document).on('mouseleave', '.detail-map-content .detail-map-img-relative', function () {
    clearInterval(mapRandomMove)
    if (MapMoveFlag == true) {
        MapMoveFn()
    }
})

// 鼠标拖拽
$(document).on('mousedown', '.detail-map-content .detail-map-img-relative', function (e) {
    var positionDiv = $('.detail-container .detail-map-content .detail-map-img-relative').offset();
    var distenceX = e.pageX - positionDiv.left;
    var distenceY = e.pageY - positionDiv.top;
    MapMoveFlag = false

    $(document).mousemove(function (e) {
        var x = e.pageX - distenceX - 25
        var y = e.pageY - distenceY - 415;
        $('.detail-container .detail-map-content .detail-map-img-relative').css({ 'cursor': 'grabbing' })

        if (y < 0 && y > $('.detail-container .detail-map-content .detail-map-img').height() - $('.detail-container .detail-map-content .detail-map-img-img').height()) {
            $('.detail-map-content .detail-map-img-relative').css({ 'top': y })
        }
        if (x <= 0 && x >= $('.detail-container .detail-map-content .detail-map-img').width() - $('.detail-container .detail-map-content .detail-map-img-img').width()) {
            $('.detail-container .detail-map-content .detail-map-img-relative').css({ 'left': x })
        }

    })

    $(document).mouseup(function (e) {
        $('.detail-container .detail-map-content .detail-map-img-relative').css({ 'cursor': 'grab' })
        MapMoveFlag = true
        $(document).off('mousemove');
    })
})
// 地图放大
$(document).on('mouseenter', '.detail-map-scale-btn', function () {
    MapMoveFlag = false
    $(document).on('mouseleave', '.detail-map-scale-btn', function () {
        MapMoveFlag = true
    })
})

$(document).on('click', '.detail-map-scale-btn', function () {
    ipcRenderer.send('Main', {
        msgType: "WindowSize",
        jsonInfo: {
            window: 'createroom',
            size: 'max'
        }
    });
    MapMoveFlag = false
    clearInterval(mapRandomMove)
    if ($('#gx-map-modal-birth').length > 0) {
        return false
    }
    $('.details-content').css('opacity', 0)
    var $this = $(this)
    $('body').append('<div class="modal" id="gx-map-modal-birth" data-backdrop="true" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="reportModal"><div class="modal-dialog" data-dismiss="modal" role="document"><div class="detail-map-img-relative relative">' + $('.detail-map-img-relative').html() + '</div></div></div>')
    $('#gx-map-modal-birth .detail-map-img-img').css('width', '100%')
    $('#gx-map-modal-birth .detail-map-img-img').css('height', 'auto')
    $('#gx-map-modal-birth .detail-map-scale-btn').css('display', 'none')
    let maxbili = $('#gx-map-modal-birth').width() / $('#gx-map-modal-birth').height()
    let mapbili = $('.detail-map-content .detail-map-img-img').width() / $('.detail-map-content .detail-map-img-img').height()

    if (maxbili > mapbili) {
        $('#gx-map-modal-birth .modal-dialog').css('margin-top', '0vh')
        $('#gx-map-modal-birth .detail-map-img-relative').css('width', 'auto')
        $('#gx-map-modal-birth .detail-map-img-img').css('width', 'auto')
        $('#gx-map-modal-birth .detail-map-img-img').css('height', '100vh')
    }
    $('#gx-map-modal-birth').one('shown.bs.modal', function () {
        MapMoveFlag = false
    })
    $('#gx-map-modal-birth').modal('show')
    $('#gx-map-modal-birth .detail-map-img-img').attr('data-dismiss', 'modal')
    $('#gx-map-modal-birth').one('hidden.bs.modal', function () {
        $('#gx-map-modal-birth').remove();
        MapMoveFlag = true
    })
    $('#gx-map-modal-birth .detail-map-img-relative').css({ 'transform': 'scale(1)', 'opacity': 1 })
    $('.modal-backdrop.in').css('opacity', 1)
})
$(document).on('hide.bs.modal', '#gx-map-modal-birth', function () {
    MapMoveFn()
    $('.details-content').animate({ 'opacity': 1 })
    $('.modal-backdrop.in').css('opacity', .5)
    ipcRenderer.send('Main', {
        msgType: "WindowSize",
        jsonInfo: {
            window: 'createroom',
            size: 'normal'
        }
    });
    ipcRenderer.send('Main', {
        msgType: "ChangeWindowSize",
        jsonInfo: {
            window: 'createroom',
            width: 1280,
            height: 720,
            isCenter: true
        }
    });
})
// 点击准备
$(document).on('click', '.detail-container-readybtn', function () {
    player_sound_ready()
    $('.detail-user-infor-content').addClass('pointer-events-none')
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    $this.gxbtn('loading')
    $.controlAjax({
        type: "get",
        url: '/api/details/game/ready',
        showerror: true,
        data: {
            ready: true,
            status: true
        },
        success: function (res) {
            detailsMessage.isReady = true
            changeReadyBtn()
        },
        error: function (req) {
            detailsMessage.isReady = detailsMessage.isReady
            changeReadyBtn()
            $('.detail-user-infor-content').removeClass('pointer-events-none')
        }
    })
    return false
})
// 取消准备
$(document).on('click', '.detail-container-readybtn-in', function () {
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    $this.gxbtn('loading')
    $.controlAjax({
        type: "get",
        url: '/api/details/game/ready',
        showerror: true,
        data: {
            ready: true,
            status: false
        },
        success: function (res) {
            detailsMessage.isReady = false
            changeReadyBtn()
        },
        error: function (req) {
            detailsMessage.isReady = detailsMessage.isReady
            changeReadyBtn()
        }
    })
    return false
})
// 准备按钮
function changeReadyBtn() {
    if (detailsMessage.isReady) {
        // 已准备
        $('.details-tbody .detail-start-mark').removeClass('hide')
        $('.details-tbody .detail-start-mark').removeClass('hide')
        $('.detail-container-readybtn-in').removeClass('hide')
        $('.detail-container-readybtn').addClass('hide')
        $('.detail-user-list-item').each(function () {
            if ($(this).attr('data-id') == userInfo.userId) {
                $(this).find('.detail-container-ready-img').removeClass('hide')
                $(this).removeClass('canChange')
            }
        })
    } else {
        // 为准备
        $('.details-tbody .detail-start-mark').addClass('hide')
        $('.detail-container-readybtn-in').addClass('hide')
        $('.detail-container-readybtn').removeClass('hide')
        $('.detail-user-list-item').each(function () {
            if ($(this).attr('data-id') == userInfo.userId) {
                $(this).find('.detail-container-ready-img').addClass('hide')
                $(this).addClass('canChange')
            }
        })
    }
    $('.detail-container-readybtn-in').gxbtn('reset')
    $('.detail-container-readybtn').gxbtn('reset')
}

// 网络信息展示
$(document).on('mouseleave', '.details-ping', function () {
    $(this).find('.detail-ping-hover').css('display', 'none')
})

$(document).on('mouseenter', '.details-ping', function () {
    $(this).find('.detail-ping-hover').css('display', 'block')
})
function dellDetailImage(imageUrl) {
    oneImgCache(imageUrl).then((Cachereturn) => {
        $('.detail-map-img-img').attr('src', Cachereturn)
        $('.detail-map-img').attr('data-map', Cachereturn)
    })
}