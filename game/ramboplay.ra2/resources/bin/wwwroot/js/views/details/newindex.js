$('.newdetail').html(template('newdetail-script'))//翻译
let LBWlanguage = localStorage.getItem('LBWlanguage') || 'zh'
if (LBWlanguage == 'en') {
    $('body').addClass('newdetail-en')
}
var newdetailMessage = {
    basicsArray: [
        { key: "ShortGame" },
        { key: "BridgeDestroy" },
        { key: "MCVRedeploy" },
        { key: "IngameAllying" },
        { key: "BuildOffAlly" },
        { key: "Crates" },
        { key: "UnitCount" },
        { key: "Credits" },
    ],
    gameArray: [
        { key: "InvincibleOilWell" },
        { key: "MultiEngineer" },
        { key: "FreeRadar" },
        { key: "RevealShroud" },
        { key: "BanGarrison" },
        { key: "BanSpy" },
        { key: "ExtremeAI" },
        { key: "AllyRepair" }
    ],
    specialArray: [
        {
            gameVersion: 0, option: [
                { key: "BanFrance" },
                { key: "BanYuri" },
                { key: "BanKorea" },
                { key: "BanNavy" },
                { key: "NoPenetration" },
                { key: "SingleCNST" },
                { key: "NoDogEngiEat" },
                { key: "NoRandmoSameSide" },
                { key: "NoSpawnPreviews" }
            ]
        },
        {
            gameVersion: 1, option: [
                { key: "BanFrance" },
                { key: "BanKorea" },
                { key: "BanNavy" },
                { key: "NoPenetration" },
                { key: "SingleCNST" },
                { key: "NoDogEngiEat" },
                { key: "NoRandmoSameSide" },
                { key: "NoSpawnPreviews" }
            ]
        },
        {
            gameVersion: 3, option: [
                { key: "BanFrance" },
                { key: "BanKorea" },
                { key: "BanNavy" },
                { key: "NoPenetration" },
                { key: "SingleCNST" },
                { key: "BanSz" },
                { key: "NoDogEngiEat" },
                { key: "NoRandmoSameSide" },
                { key: "NoSpawnPreviews" }
            ]
        }
    ],
    detailsData: null,//房间详情
    mapData: null,//地图信息
    isHost: false,//是否房主
    autoopencheck: false,//自动开始游戏
    autoopenready: false,//自动准备
    optionsCountry: [], //国家配置
    localUserlist: [], //本地维护的用户信息
    isgetinit: false,//初始化后才监听
    hostId: '',//用于识别聊天房主
    hallinviteTime: 10,//大厅邀请冷却
    hallinviteTimefun: 10,//大厅邀请冷却
    setTingtype: 'basics',//'basics' 'game' 'alliance' 'special'
    isReady: false,
    roomType: 0,//0 自由 1 混乱
    addrobotIndex: 0,//添加机器人的位置
    changeIndex: 0,//加入观察者的位置
    changeweiIndex: 0,//加入队伍的位置
    roomTypeMessage: {
        roomType2: {
            name: '1v1',
            A: [0],
            B: [1],
            watch: [2, 3, 4, 5, 6, 7]
        },
        roomType3: {
            name: '2v2',
            A: [0, 1],
            B: [2, 3],
            watch: [4, 5, 6, 7]
        },
        roomType4: {
            name: '3v3',
            A: [0, 1, 2],
            B: [3, 4, 5],
            watch: [6, 7]
        },
        roomType5: {
            name: '4V4',
            A: [0, 1, 2, 3],
            B: [4, 5, 6, 7],
            watch: []
        }
    },
    changeweiloading: null,//换位冷却
    changeweiloadingnum: 0,//换位冷却
    changeweiloadingList: [],//换位排队
    downloadMapEnd: false,
    isget15: false,
    autosetTime: false
}

newdetailInit()
// 初始化
function newdetailInit() {
    setTimeout(() => {
        //自动开始保险机制
        newdetailMessage.autosetTime = true
    }, 5000)
    $.controlAjax({
        type: "get",
        url: '/api/details/inroom/info',
        showerror: true,
        data: {
            matchIng: false
        },
        success: function (res) {
            newdetailMessage.isgetinit = true
            newdetailMessage.detailsData = res.data
            newdetailMessage.optionsBirth = res.data.startingLocations
            newdetailMessage.roomType = res.data.roomType
            if (getSaveMessage('batterStorage')) {
                let oldbatterStorage = getSaveMessage('batterStorage')
                if (oldbatterStorage.roomNo == newdetailMessage.detailsData.roomNo) {
                    batterMessage.localstorage = oldbatterStorage
                }
            }

            // 游戏设置读本地
            // if (getSaveMessage('gameOption') && !newdetailMessage.isHost) {
            //     let gameOption = getSaveMessage('gameOption')
            //     if (newdetailMessage.detailsData.roomNo == gameOption.roomNo) {
            //         newdetailMessage.detailsData.gameOption = gameOption.gameOption
            //     }
            // }
            newdetailMessage.detailsData.gameOption.map((item) => {
                if (item.key == 'MaxPlayers') {
                    newdetailMessage.detailsData.maxPlayers = item.value
                }
            })
            $('.newdetail-userList').attr('class', 'newdetail-userList roomType' + newdetailMessage.roomType)
            $('.newdetail-row-left').attr('class', 'newdetail-row-left roomType-left' + newdetailMessage.roomType)
            addroomTypeText()
            // 神州国家配置判断
            newdetailMessage.optionsCountry = JSON.parse(JSON.stringify(comOption.optionsCountry))
            if (newdetailMessage.detailsData.gameVersion == 0) {
                //尤里
                newdetailMessage.optionsCountry.map((item) => {
                    if (item.sideId == 9) {
                        item.country = 'Yuri'
                        item.cname = '尤里'
                    }
                })
            } else if (newdetailMessage.detailsData.gameVersion == 3) {
                //共辉
                newdetailMessage.optionsCountry.map((item) => {
                    if (item.sideId == 9) {
                        item.country = 'Shenzhen'
                        item.cname = '神州'
                    }
                })
            }
            // 房主判断
            for (useritem of newdetailMessage.detailsData.players) {
                if (useritem.userId == userInfo.userId) {
                    newdetailMessage.isHost = useritem.isHost
                    newdetailMessage.isReady = useritem.ready
                }
                if (useritem.isHost) {
                    newdetailMessage.hostId = useritem.userId
                }
            }
            if (!newdetailMessage.isHost) {
                setTimeout(function () {
                    if (!newdetailMessage.isget15) {
                        getRooomOptionAgain()
                        // 保险机制 开启房间过慢
                    }
                }, 2000)
            } else {
                if (localStorage.getItem('RoomNoticeModal')) {
                    let text = localStorage.getItem('RoomNoticeModal')
                    ipcRenderer.send('Main', {
                        msgType: "ChatMessage",
                        jsonInfo: {
                            msg: text,
                            noticeType: 5
                        }
                    });
                }
                if (newdetailMessage.detailsData.isHavePas && getSaveMessage('roomAutoOpen')) {
                    newdetailMessage.autoopencheck = getSaveMessage('roomAutoOpen').check
                }
                if (!newdetailMessage.detailsData.isHavePas) {
                    newdetailMessage.autoopencheck = true
                }
            }
            // 渲染空用户列表
            for (var i = 0; i < Number(newdetailMessage.detailsData.maxPlayers); i++) {
                newdetailMessage.localUserlist.push({
                    userId: 'empty'
                })
                $('.newdetail-userList-item-empty.empty').eq(0).replaceWith(
                    template('newdetail-userList-script', {
                        type: 'empty',
                        isHost: newdetailMessage.isHost,
                    })
                )
            }
            for (var i = 0; i < 8 - Number(newdetailMessage.detailsData.maxPlayers); i++) {
                newdetailMessage.localUserlist.push({
                    userId: 'close'
                })
                $('.newdetail-userList-item-empty.empty').eq(0).replaceWith(
                    template('newdetail-userList-script', {
                        type: 'close',
                        isHost: newdetailMessage.isHost,
                    })
                )
            }
            // 自动准备设置
            if (!newdetailMessage.isHost) {
                let autoReady = getSaveMessage('autoReady')
                if (autoReady && autoReady.autoReady && autoReady.roomNo == newdetailMessage.detailsData.roomNo) {
                    newdetailMessage.autoopenready = true
                }
                timeCheckautoopenready()
            }
            // 用户数据写入
            userDellfun(newdetailMessage.detailsData.players)
            // 渲染头部
            let roomType = ''
            let roomTypezu = []
            let roomTypeArray = JSON.parse(JSON.stringify(comOption.roomTypeArray))
            roomTypeArray.map((item) => {
                if (item.code == newdetailMessage.roomType) {
                    roomType = item
                }
            })
            roomTypeArray.map((item) => {
                if (roomType.type == item.type) {
                    roomTypezu.push(item)
                }
            })

            $('.newdetail-header').html(template('newdetail-header-script', {
                roomNo: newdetailMessage.detailsData.roomNo,
                isHost: newdetailMessage.isHost,
                roomType: roomType,
                roomTypeArray: roomTypezu,
                autoopencheck: newdetailMessage.autoopencheck,
                autoopenready: newdetailMessage.autoopenready,
            }))
            $('.gx-menu-newdetail-roomtype.canChange').gxmenu({
                height: '30px',
                top: "42px",
                clickHide: true
            }, function (e) {

            })
            //复制功能
            let clipboard = new Clipboard('#roomNo-copy-btn');
            roomNotice()//房间公告
            updataMap(newdetailMessage.detailsData.mapSha1, 'init')
            // 观战
            // if (!newdetailMessage.isHost) {
            //     newdetailMessage.detailsData.gameOption = []
            // }
            // updataGameseting('init')//渲染游戏设置
            updataGameseting()//渲染游戏设置
            // updataGameseting()
            if (newdetailMessage.roomType != 0 && newdetailMessage.roomType != 1) {
                roomStartBtn(false)//渲染游戏按钮
            } else {
                roomStartBtn(true)//渲染游戏按钮
            }
            roomInviteBtn()//渲染游戏按钮
            if (newdetailMessage.detailsData.roomStatus == 3) {
                // 进入房间埋点
                wsUserStatusdetails(8)
            } else {
                // 进入房间埋点
                wsUserStatusdetails(7)
                let Audio_inroom = document.getElementById('Audio_inroom')
                Audio_inroom.play()
                AutoClose()
            }
        },
        error: function (req) {

        }
    })
    // 用于设置是否接收用户换位请求
    $.star({
        type: 'GET',
        url: '/community-user/user/config/v1/list',
        showerror: true,
        data: {
            source: 1
        },
        success: function (res) {
            let changewei = 'N'
            res.data.map((item) => {
                if (item.name == 'battle_switch_user_transposition') {
                    changewei = item.value
                }
            })
            if (changewei == 'Y') {
                localStorage.setItem('battle_switch_user_transposition', 1)
            } else if (changewei == 'N') {
                localStorage.removeItem('battle_switch_user_transposition');
            }
        },
        error: function (req) {
        }
    });
}
// 保险机制获取房间设置
function getRooomOptionAgain() {
    $.controlAjax({
        type: "get",
        url: '/api/details/inroom/info',
        showerror: true,
        data: {
            matchIng: false
        },
        success: function (res) {
            let gameOption = res.data.gameOption
            gameOption.map((item) => {
                if (item.key == 'MaxPlayers') {
                    if (item.value != newdetailMessage.detailsData.maxPlayers) {
                        userGetChangeMax(item.value)
                    }
                }
                if (item.key == 'GameVersion') {
                    if (newdetailMessage.detailsData.gameVersion != item.value) {
                        newdetailMessage.detailsData.gameVersion = item.value
                        updataGameVersion()
                    }
                }
            })
            for (var i = 0; i < newdetailMessage.detailsData.gameOption.length; i++) {
                gameOption.map((item) => {
                    if (newdetailMessage.detailsData.gameOption[i].key == item.key) {
                        newdetailMessage.detailsData.gameOption[i].value = item.value
                        newdetailMessage.detailsData.gameOption[i].must = item.must
                    }
                })
            }
            // saveMessage('gameOption', { roomNo: newdetailMessage.detailsData.roomNo, gameOption: newdetailMessage.detailsData.gameOption })
            updataGameseting()
        }
    })
}
//--------------------操作-------------------
// 房间号可见
$(document).on('click', '.newdetail-header-roomNum-numgalss', function () {
    var $this = $(this)
    $this.closest('.newdetail-header-roomNum').toggleClass('active')
})
// 复制房间号提示
$(document).on('click', '#roomNo-copy-btn', function () {
    showtoast({
        message: translatesrting('已复制房间号')
    })
})
// 自动开始按钮
$(document).on('change', '.J-newdetail-autoOpen', function () {
    var $this = $(this)
    if ($this.is(':checked')) {
        autoOpenTime('open')
    } else {
        autoOpenTime('close')
    }
})
// 自动准备按钮
$(document).on('change', '.J-newdetail-autoReady', function () {
    var $this = $(this)
    if ($this.is(':checked')) {
        newdetailMessage.autoopenready = true
        saveMessage('autoReady', {
            roomNo: newdetailMessage.detailsData.roomNo,
            autoReady: true
        })
    } else {
        newdetailMessage.autoopenready = false
        localStorage.removeItem('autoReady')
    }
})
// -----------------房间公告设置---------------
$(document).on('click', '.J_showSetRoomNotice', function () {
    let text = $('.newdetail-chatDiv-header-notice').attr('data-text') || ""
    $('#RoomNoticeModal').html(template('RoomNoticeModal-script', { text: text }))
    $('#RoomNoticeModal').modal('show')
})

$(document).on('input', '.RoomNoticeModal-textarea', function () {
    var $this = $(this)
    let num = $this.val().length
    $this.closest('div').find('.RoomNoticeModal-textarea-num').text(num + '/30')
})
$(document).on('click', '#J_RoomNoticeModal-set', function () {
    var $this = $(this)
    $this.gxbtn('loading')
    let text = $this.closest('.RoomNoticeModal-content').find('.RoomNoticeModal-textarea').val() || '空'
    ipcRenderer.send('Main', {
        msgType: "ChatMessage",
        jsonInfo: {
            msg: text,
            noticeType: 5
        }
    });
    $this.gxbtn('reset')
    $('#RoomNoticeModal').modal('hide')
    if (text != '' && text != '空') {
        localStorage.setItem('RoomNoticeModal', text)
    } else {
        localStorage.removeItem('RoomNoticeModal')
    }
})
// -----------------房间公告设置end---------------
// 退出房间
$(document).on('click', '.J-close-sure', function () {
    let justout = true

    // if (newdetailMessage.isHost) {
    //     justout = false
    // }
    if (newdetailMessage.isHost) {
        newdetailMessage.localUserlist.map((item) => {
            if (item.userId != 'empty' && item.userId != 'close' && !item.isAI && !item.isHost) {
                justout = false
            }
        })
    }

    if (!justout) {
        gxmodal({
            title: translatesrting('退出确认'),
            centent: '<div class="mt25">' + translatesrting('您确定要退出当前房间吗？') + '</div>',
            buttons: [{
                text: translatesrting('确定'),
                class: 'btn-middle btn-middle-blue',
                callback: function () {
                    LeaveGameRoom()
                }
            },
            {
                text: translatesrting('取消'),
                class: 'btn-middle btn-style-ash',
            }
            ]
        })
    } else {
        LeaveGameRoom()
    }
})

// 选择颜色
$(document).on('click', '.J_chooseColor', function () {
    if (!newdetailMessage.isHost) {
        newDetailsgetnoready()
    }
    var $this = $(this)
    var $thismenu = $this.closest('.gx-menu')
    $thismenu.gxmenuin('loading')
    var id = $this.closest('.newdetail-userList-item').attr('data-id')
    var code = $this.attr('data-code')
    let changelist = {}
    for (localItem of newdetailMessage.localUserlist) {
        if (localItem.userId == id) {
            changelist = JSON.parse(JSON.stringify(localItem))
        }
    }
    changelist.colorId = code
    userDellfun([changelist], 'error')
    $.controlAjax({
        type: "POST",
        url: '/Details/SvaePlayerInfo',
        data: {
            userId: id,
            colorId: code
        },
        showerror: true,
        success: function (res) {
            $thismenu.gxmenuin('reset')
        },
        error: function (req) {
            $thismenu.gxmenuin('reset')
        }
    })

})
// 选择国家
$(document).on('click', '.J_chooseCountry', function () {
    var $this = $(this)
    var $thismenu = $this.closest('.gx-menu')
    $thismenu.gxmenuin('loading')
    let id = $this.closest('.newdetail-userList-item').attr('data-id')
    let code = $this.attr('data-code')
    let changelist = {}
    for (localItem of newdetailMessage.localUserlist) {
        if (localItem.userId == id) {
            changelist = JSON.parse(JSON.stringify(localItem))
        }
    }
    changelist.sideId = code
    userDellfun([changelist], 'error')
    $.controlAjax({
        type: "POST",
        url: '/Details/SvaePlayerInfo',
        data: {
            userId: id,
            sideId: code
        },
        showerror: true,
        success: function (res) {
            $thismenu.gxmenuin('reset')
        },
        error: function (req) {
            $thismenu.gxmenuin('reset')
        }
    })
    if (!newdetailMessage.isHost && code == 10) {
        $('.J_getready').click()
    } else {
        if (!newdetailMessage.isHost) {
            newDetailsgetnoready()
        }
    }
})
// 选择队伍
$(document).on('click', '.J_chooseTeam', function () {
    if (!newdetailMessage.isHost) {
        newDetailsgetnoready()
    }
    var $this = $(this)
    var $thismenu = $this.closest('.gx-menu')
    $thismenu.gxmenuin('loading')
    let id = $this.closest('.newdetail-userList-item').attr('data-id')
    let code = $this.attr('data-code')
    let changelist = {}
    for (localItem of newdetailMessage.localUserlist) {
        if (localItem.userId == id) {
            changelist = JSON.parse(JSON.stringify(localItem))
        }
    }
    changelist.teamId = code
    userDellfun([changelist], 'error')
    $.controlAjax({
        type: "POST",
        url: '/Details/SvaePlayerInfo',
        data: {
            userId: id,
            teamId: code
        },
        showerror: true,
        success: function (res) {
            $thismenu.gxmenuin('reset')
        },
        error: function (req) {
            $thismenu.gxmenuin('reset')
        }
    })
})
// 选择出生点
$(document).on('click', '.J_chooseBirth', function () {
    if (!newdetailMessage.isHost) {
        newDetailsgetnoready()
    }
    var $this = $(this)
    var $thismenu = $this.closest('.gx-menu')
    $thismenu.gxmenuin('loading')
    let id = $this.closest('.newdetail-userList-item').attr('data-id')
    let code = $this.attr('data-code')
    let changelist = {}
    for (localItem of newdetailMessage.localUserlist) {
        if (localItem.userId == id) {
            changelist = JSON.parse(JSON.stringify(localItem))
        }
    }
    changelist.startingLocation = code
    userDellfun([changelist], 'error')
    $.controlAjax({
        type: "POST",
        url: '/Details/SvaePlayerInfo',
        data: {
            userId: id,
            startingLocation: code
        },
        showerror: true,
        success: function (res) {
            $thismenu.gxmenuin('reset')
        },
        error: function (req) {
            $thismenu.gxmenuin('reset')
        }
    })
})
// 选择电脑
$(document).on('click', '.J_chooseAILevel', function () {
    var $this = $(this)
    let id = $this.closest('.newdetail-userList-item').attr('data-id')
    let code = $this.attr('data-code')
    let changelist = {}
    for (localItem of newdetailMessage.localUserlist) {
        if (localItem.userId == id) {
            changelist = JSON.parse(JSON.stringify(localItem))
        }
    }
    changelist.aiLevel = code
    userDellfun([changelist], 'error')
    ipcRenderer.send('Main', {
        msgType: "SavePlayer",
        jsonInfo: {
            isAI: true,
            userId: id,
            aILevel: code
        }
    });
})
// 踢出房间
$(document).on('click', '.J_letUserOut', function () {
    var $this = $(this)
    var id = $this.attr('data-id')
    ipcRenderer.send('Main', {
        msgType: "SavePlayer",
        jsonInfo: {
            userId: id,
            hostOperation: 'KickPlayer'
        }
    });
})
// 关闭空位
$(document).on('click', '.J_closeEmpty', function () {
    var $this = $(this)
    let Dom = $this.closest('.newdetail-userList-item')
    if (Dom.attr('data-id') && Dom.attr('data-id') != '') {
        var id = Dom.attr('data-id')
        ipcRenderer.send('Main', {
            msgType: "SavePlayer",
            jsonInfo: {
                userId: id,
                hostOperation: 'KickPlayer'
            }
        });
    }
    Dom.replaceWith(
        template('newdetail-userList-script', {
            type: 'close',
            isHost: newdetailMessage.isHost,
        })
    )
    let usernum = Number(newdetailMessage.detailsData.maxPlayers) - 1
    newdetailMessage.detailsData.maxPlayers = usernum
    ipcRenderer.send('Main', {
        msgType: "SetGameOption",
        jsonInfo: {
            Key: 'MaxPlayers',
            Value: usernum
        }
    });
    addroomTypeText()
})
// 开启空位
$(document).on('click', '.J_closeToEmpty', function () {
    var $this = $(this)
    let Dom = $this.closest('.newdetail-userList-item')
    Dom.replaceWith(
        template('newdetail-userList-script', {
            type: 'empty',
            isHost: newdetailMessage.isHost,
        })
    )
    let usernum = Number(newdetailMessage.detailsData.maxPlayers) + 1
    newdetailMessage.detailsData.maxPlayers = usernum
    ipcRenderer.send('Main', {
        msgType: "SetGameOption",
        jsonInfo: {
            Key: 'MaxPlayers',
            Value: usernum
        }
    });
    addroomTypeText()
})
// 添加电脑
$(document).on('click', '.J_addRobot', function () {
    newdetailMessage.addrobotIndex = $(this).closest('.newdetail-userList-item').index()
    let teamId = 3
    if (newdetailMessage.roomType != 0 && newdetailMessage.roomType != 1) {
        if ($.inArray(newdetailMessage.addrobotIndex, newdetailMessage.roomTypeMessage['roomType' + newdetailMessage.roomType].A) != -1) {
            teamId = 0
        }
        if ($.inArray(newdetailMessage.addrobotIndex, newdetailMessage.roomTypeMessage["roomType" + newdetailMessage.roomType].B) != -1) {
            teamId = 1
        }
    }
    if (newdetailMessage.roomType == 1) {
        teamId = -1
    }
    ipcRenderer.send('Main', {
        msgType: "SavePlayer",
        jsonInfo: {
            isAI: true,
            aILevel: 0,
            teamId: teamId //默认D对
        }
    });
})
// 更换位置
$(document).on('click', '.J_changewei', function () {
    if (newdetailMessage.changeweiloadingnum > 0) {
        // 冷却
        return false
    }
    changeweiloading()
    newdetailMessage.changeweiIndex = $(this).closest('.newdetail-userList-item').index()
    var $this = $(this)
    if ($this.closest('.newdetail-userList-item').hasClass('newdetail-userList-item-empty')) {
        // 空位
        changeindex = $this.closest('.newdetail-userList-item').index()
        let teamId = 1
        if ($.inArray(changeindex, newdetailMessage.roomTypeMessage['roomType' + newdetailMessage.roomType].A) != -1) {
            teamId = 0
        }
        if ($.inArray(changeindex, newdetailMessage.roomTypeMessage["roomType" + newdetailMessage.roomType].B) != -1) {
            teamId = 1
        }
        let id = userInfo.userId
        let code = teamId
        let changelist = {}
        for (localItem of newdetailMessage.localUserlist) {
            if (localItem.userId == id) {
                changelist = JSON.parse(JSON.stringify(localItem))
            }
        }
        changelist.teamId = code
        changelist.sideId = -1
        userDellfun([changelist], 'error')
        $.controlAjax({
            type: "POST",
            url: '/Details/SvaePlayerInfo',
            data: {
                userId: id,
                teamId: code,
                sideId: -1
            },
            showerror: false,
            success: function (res) {

            },
            error: function (req) {

            }
        })
    } else {
        //换位置
        let id = $this.closest('.newdetail-userList-item').attr('data-id')
        let isAi = false
        if (id * 1 < 10) {
            isAi = true
        }
        $.controlAjax({
            type: "get",
            url: '/api/details/exchange/player',
            data: {
                userId: userInfo.userId,
                toUserId: $this.closest('.newdetail-userList-item').attr('data-id'),
                ai: isAi
            },
            showerror: true,
            success: function (res) {

            },
            error: function (req) {

            }
        })
    }
})
// 加入观察这
$(document).on('click', '.J_addwatch', function () {
    if (newdetailMessage.changeweiloadingnum > 0) {
        // 冷却
        return false
    }
    changeweiloading()
    newdetailMessage.changeIndex = $(this).closest('.newdetail-userList-item').index()
    $.controlAjax({
        type: "POST",
        url: '/Details/SvaePlayerInfo',
        data: {
            userId: userInfo.userId,
            sideId: 10
        },
        showerror: false,
        success: function (res) {

        },
        error: function (req) {

        }
    })
})

// 发送聊天信息
$(document).on('click', '.J_chatUp', function () {
    var chatmesg = $.trim($('.J_chatUpinput').val());//去空
    if (chatmesg == "") {
        return false;
    }
    if (chatmesg.toUpperCase() == '/ROLL') {
        let rollnum = parseInt(Math.random() * 100)
        ipcRenderer.send('Main', {
            msgType: "ChatMessage",
            jsonInfo: {
                msg: rollnum,
                noticeType: 8
            }
        });
    } else {
        ipcRenderer.send('Main', {
            msgType: "ChatMessage",
            jsonInfo: {
                msg: chatmesg
            }
        });
    }
    $('.J_chatUpinput').val("")
    $('.J_chatUpinput').focus()
})
// 聊天回车
$(document).on('keyup', '.J_chatUpinput', function (event) {
    if (event.keyCode == 13) {
        $(".J_chatUp").click()
    }
})
// 好友邀请
$(document).on('click', '.J_invitationfriend', function (event) {
    $('#modalInvite').html(template('modalInvite-script'))
    $('#modalInvite').modal('show')
    var temp_inviteJson_friend = {
        type: '1',
        list: []
    }
    var temp_inviteJson_recently = {
        type: '2',
        list: []
    }
    $.star({
        type: 'GET',
        url: '/community-user/friend/v1/list',
        success: function (res) {
            if (res.data.userFriendList == null) {
                temp_inviteJson_friend.list = []
            } else {
                temp_inviteJson_friend.list = res.data.userFriendList
            }

            $('.Invite-MyFriends').html(template('career-invite-script', { message: temp_inviteJson_friend }))

            if (res.data.recentGamersList == null) {
                temp_inviteJson_recently.list = []
            } else {
                temp_inviteJson_recently.list = res.data.recentGamersList
            }

            $('.Invite-Recently').html(template('career-invite-script', { message: temp_inviteJson_recently }))
        },
        error: function (req) {
        }
    });
})
// 好友邀请 添加好友
$(document).on('click', '.Invite-content-AddFriends', function () {
    var $this = $(this)
    var Friend_id = $this.attr('data-id')

    $.star({
        type: 'POST',
        url: '/community-user/friend/v1/request',
        showerror: false,
        data: {
            friendId: Friend_id,
            source: '1'
        },
        success: function (res) {
            showtoast({
                message: translatesrting('已发送好友请求')
            })
        },
        error: function (req) {
            if (req.errorCode == 260114) {
                gxmodal({
                    title: translatesrting('提示'),
                    centent: '<div class="mt25">' + translatesrting('好友数量超过上限') + '</div>',
                    buttons: [
                        {
                            text: translatesrting('确定'),
                            class: 'btn-middle btn-middle-blue',
                            callback: function () {

                            }
                        }
                    ]
                })
            } else {
                showtoast({
                    message: req.errorMsg
                })
            }
        }
    });
    $this.css('display', 'none')
    $this.next().css('display', 'block')
})

// --------------------好友邀请 ------------
$(document).on('click', '.Invite-content-check-content', function () {
    var $this = $(this)
    // *-----房间战斗中阻止-----*
    let isingame = false
    if ($('.newdetail-userList-item-roombot.battle').length > 0) {
        isingame = true
    }
    if (isingame) {
        showtoast({
            message: translatesrting('房间内有人仍在游戏中，无法邀请其他人加入')
        })
        return false
    }
    // *-----房间战斗中阻止-----*
    $this.css({ background: '#888888', cursor: 'default' })
    $this.children(":first").text(translatesrting('已邀请'))

    let temp_send_arr = []
    temp_send_arr.push($this.attr('data-id'))

    $.star({
        type: 'POST',
        url: '/community-user/friend/vi/user/add/room',
        data: {
            roomNo: newdetailMessage.detailsData.roomNo,
            userIds: temp_send_arr,
            invitePeopleName: userInfo.username,
            roomName: newdetailMessage.detailsData.roomName,
            pwd: newdetailMessage.detailsData.password
        },
        success: function (res) {
            showtoast({
                message: translatesrting('已成功发送邀请')
            })
        },
        error: function (req) {
        }
    });
})

// 好友邀请 确认邀请
$(document).on('click', '.Invite-button', function () {
    $('#modalInvite').modal('hide')
})

// 切换好友页和最近好友页
$(document).on('click', '.Invite-body-MyFriends-btn', function () {
    $('.Invite-body-MyFriends-btn').addClass('Invite-content-header-focus')
    $('.Invite-MyFriends').addClass('Invite-content-focus')
    $('.Invite-body-Recently-btn').removeClass('Invite-content-header-focus')
    $('.Invite-Recently').removeClass('Invite-content-focus')
})
$(document).on('click', '.Invite-body-Recently-btn', function () {
    $('.Invite-body-MyFriends-btn').removeClass('Invite-content-header-focus')
    $('.Invite-MyFriends').removeClass('Invite-content-focus')
    $('.Invite-body-Recently-btn').addClass('Invite-content-header-focus')
    $('.Invite-Recently').addClass('Invite-content-focus')
})
// --------------------好友邀请end ------------
// 大厅邀请
$(document).on('click', '.J_newdetail-hall-Invite', function () {
    var $this = $(this)
    if ($this.hasClass('loading')) {
        return false
    }
    // *-----房间战斗中阻止-----*
    let isingame = false
    if ($('.newdetail-userList-item-roombot.battle').length > 0) {
        isingame = true
    }
    if (isingame) {
        showtoast({
            message: translatesrting('房间内有人仍在游戏中，无法邀请其他人加入')
        })
        return false
    }
    // *-----房间战斗中阻止-----*
    let needcoin = $this.attr('data-coin')
    let needcoinAlert = true
    if (needcoin > 0) {
        needcoinAlert = true
    } else {
        needcoinAlert = false
    }
    if (needcoinAlert) {
        if (localStorage.getItem('hallChatInvitday')) {
            needcoinAlert = false
        }
    }
    if (needcoinAlert) {
        gxmodal({
            title: '使用金币确认',
            id: 'hallChatInvitaneedalert',
            centent: '<div class="mt30" style="padding: 0px 22px;">' + translatesrting('您确定使用') + needcoin + translatesrting('金币发送大厅邀请吗？') + '<div><div class="gx-check hallChatInvitaneedalert-div"><input type="checkbox" id="hallChatInvitaneedalertInput"><label for="hallChatInvitaneedalertInput" class="f12"><span>' + translatesrting('今后不再提示') + '</span></label></div>',
            buttons: [
                {
                    text: translatesrting('确定'),
                    class: 'btn-middle btn-middle-blue',
                    callback: function () {
                        hallInviteAjax()
                    }
                },
                {
                    text: translatesrting('取消'),
                    class: 'btn-middle btn-style-ash',
                    callback: function () {
                    }
                }
            ]
        })
    } else {
        hallInviteAjax()
    }
})
$(document).on('change', '#hallChatInvitaneedalertInput', function () {
    let isc = $(this).is(':checked')
    if (isc) {
        let newday = new Date().getDay()
        localStorage.setItem('hallChatInvitday', newday)
    } else {
        localStorage.removeItem('hallChatInvitday', newday)
    }
})
// 邀请
function hallInviteAjax() {
    hallInvitationloading()
    $.star({
        type: 'POST',
        url: '/battlecenter/redwar/room/v1/send-chat',
        showerror: false,
        data: {
            msg: "",
            roomNo: newdetailMessage.detailsData.roomNo, //邀请必填
            inviteStatus: true, //是否是邀请
        },
        success: function (res) {
            parent.getUserGold()
            showtoast({
                message: translatesrting('邀请信息已发送到自定义大厅聊天频道')
            })
        },
        error: function (req) {
            showtoast({
                message: translatesrting(req.errorMsg)
            })
        }
    });
}

function hallInvitationloading() {
    var $this = $('.J_newdetail-hall-Invite')
    if ($this.hasClass('loading')) {
        return false
    }
    $this.addClass('loading')
    $this.find('.newdetail-hall-Invite-loading-num').text(10)
    newdetailMessage.hallinviteTimefun = setInterval(function () {
        if (newdetailMessage.hallinviteTime == 1) {
            clearInterval(newdetailMessage.hallinviteTimefun)
            newdetailMessage.hallinviteTime = 10
            $this.removeClass('loading')
            roomInviteBtn()
            return false
        }
        newdetailMessage.hallinviteTime = newdetailMessage.hallinviteTime - 1
        $this.find('.newdetail-hall-Invite-loading-num').text(newdetailMessage.hallinviteTime)
    }, 1000)
}
// 放大
$(document).on('click', '.J_bigMap', function () {
    $('#bigmap').modal('show')
    $('#bigmap .bigmap-content').html($('.newdetail-map-imgdiv').html())
    var img = new Image();
    img.src = $('.newdetail-map-imgdiv .newdetail-map-img').attr('src');
    if (img.complete) {
        if ((img.width / img.height) > (553.69 / 310)) {
            $('#bigmap .bigmap-content .newdetail-map-img').addClass('width')
        } else {
            $('#bigmap .bigmap-content .newdetail-map-img').addClass('height')
        }
    } else {
        img.onload = function () {
            if ((img.width / img.height) > (553.69 / 310)) {
                $('#bigmap .bigmap-content .newdetail-map-img').addClass('width')
            } else {
                $('#bigmap .bigmap-content .newdetail-map-img').addClass('height')
            }
        }
    }


})
// 选择地图
$(document).on('click', '.J_changeMap', function () {
    $('#modalChangeMap').html(template('modalChangeMap-script'))
    chooseMapInit()
})
// 使用地图
$(document).on('click', '.J_sureChoose', function () {
    mapDetailChoose()
})
// 显示观战设置
$(document).on('click', '.J_battleSetup', function () {
    if (getSaveMessage('batterStorage')) {
        let oldbatterStorage = getSaveMessage('batterStorage')
        if (oldbatterStorage.roomNo == newdetailMessage.detailsData.roomNo) {
            batterMessage.localstorage = oldbatterStorage
        }
    }
    batterMessage.init()
})
// 收藏
$(document).on('click', '.J_getCollectStatusMap', function () {
    var $this = $(this)
    let choose = 1
    if ($this.hasClass('incollectStatus')) {
        choose = 0
    } else {
        choose = 1
    }
    $.star({
        type: 'POST',
        url: '/battlecenter/redwar/map/v1/collect-map',
        showerror: true,
        data: {
            mapId: newdetailMessage.mapData.idStr,
            operate: choose,
        },
        success: function (res) {
            if (choose) {
                showtoast({
                    message: translatesrting('收藏成功')
                })
            } else {
                showtoast({
                    message: translatesrting('取消收藏成功')
                })
            }
            $this.toggleClass('incollectStatus')
        },
        error: function (req) {
        }
    });
})
// 选择游戏版本
$(document).on('click', '.J_chooseGameVersion', function () {
    var $this = $(this)
    var code = $this.attr('data-code')
    ipcRenderer.send('Main', {
        msgType: "SetGameOption",
        jsonInfo: {
            Key: 'GameVersion',
            Value: code
        }
    });
    if (newdetailMessage.detailsData.gameVersion == 0) {
        // 特殊设置
        newdetailMessage.detailsData.gameOption.map((item) => {
            if ('AllyRepair' == item.key) {
                item.value = 'True'
            }
        })
        ipcRenderer.send('Main', {
            msgType: "SetGameOption",
            jsonInfo: {
                Key: 'AllyRepair',
                Value: 'True'
            }
        });
        updataGameseting()
    }
    newdetailMessage.detailsData.gameVersion = code
    updataGameVersion()
})
// 选择游戏模式
$(document).on('click', '.J_chooseGameMode', function () {
    var $this = $(this)
    let mapGameMode = $this.attr('data-code')
    let label = null
    if (newdetailMessage.mapData.labels) {
        label = newdetailMessage.mapData.labels[0]
    }
    ipcRenderer.send('Main', {
        msgType: "ChangeMap",
        jsonInfo: {
            mapName: newdetailMessage.mapData.name,
            mapGameMode: mapGameMode,
            mapSha1: newdetailMessage.mapData.mapSha1,
            imageUrl: newdetailMessage.mapData.imageUrl,
            label: label
        }
    });
})
// 切换设置nav
$(document).on('click', '.newdetail-setting-nav-item', function () {
    var $this = $(this)
    let code = $this.attr('data-code')
    $this.siblings().removeClass('active')
    $this.addClass('active')
    newdetailMessage.setTingtype = code
    updataGameseting()
})
// 房主游戏设置
$(document).on('change', '.newdetail-setting-content input[type="checkbox"]', function () {
    var $this = $(this)
    let key = $this.attr('data-name')
    let value = $this.is(':checked') ? 'True' : 'False'
    newdetailMessage.detailsData.gameOption.map((item) => {
        if (key == item.key) {
            item.value = value
        }
    })
    ipcRenderer.send('Main', {
        msgType: "SetGameOption",
        jsonInfo: {
            Key: key,
            Value: value
        }
    });
})
// 房主游戏设置-滑块
$(document).on('change', '.newdetail-setting-content .setting-range', function () {
    var $this = $(this)
    var key = $this.attr('data-code')
    var value = $this.val()
    newdetailMessage.detailsData.gameOption.map((item) => {
        if (key == item.key) {
            item.value = value
        }
    })
    ipcRenderer.send('Main', {
        msgType: "SetGameOption",
        jsonInfo: {
            Key: key,
            Value: value
        }
    });
})
// 房主游戏设置-武器
$(document).on('click', '.J_BanBigSuperWeapon', function () {
    var $this = $(this)
    var code = $this.attr('data-code')

    let BanAllSuperWeapon = 'False'
    let BanBigSuperWeapons = 'False'

    if (code == 1) {
        BanAllSuperWeapon = 'False'
        BanBigSuperWeapons = 'False'
    } else if (code == 2) {
        BanAllSuperWeapon = 'False'
        BanBigSuperWeapons = 'True'
    } else if (code == 3) {
        BanAllSuperWeapon = 'True'
        BanBigSuperWeapons = 'True'
    }
    ipcRenderer.send('Main', {
        msgType: "SetGameOption",
        jsonInfo: {
            Key: 'BanAllSuperWeapon',
            Value: BanAllSuperWeapon
        }
    });
    ipcRenderer.send('Main', {
        msgType: "SetGameOption",
        jsonInfo: {
            Key: 'BanBigSuperWeapons',
            Value: BanBigSuperWeapons
        }
    });
    newdetailMessage.detailsData.gameOption.map((item) => {
        if ('BanBigSuperWeapons' == item.key) {
            item.value = BanBigSuperWeapons
        }
        if ('BanAllSuperWeapon' == item.key) {
            item.value = BanAllSuperWeapon
        }
    })
    updataGameseting()
})
// 点击开始游戏
$(document).on('click', '.J_startGame', function () {

    // checkWatch()
    // return false
    player_sound_click()
    var $this = $(this)
    // loading阻止
    if ($this.hasClass('disabled')) {
        return false
    }
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    // checkWatch()
    var isonlystart = true;
    newdetailMessage.localUserlist.map((item) => {
        if (item.name && !item.isHost && !item.isAI) {
            isonlystart = false
        }
    })
    if (isonlystart) {
        // 单人提示
        gxmodal({
            title: translatesrting('提示'),
            centent: '<div class="mt25">' + translatesrting('只有您一名玩家，是否开启单人模式？') + '</div>',
            buttons: [{
                text: translatesrting('确定'),
                class: 'btn-middle btn-middle-blue',
                callback: function () {
                    startGame()
                }
            },
            {
                text: translatesrting('取消'),
                class: 'btn-middle btn-style-ash',
            }
            ]
        })
        return false
    }
    startGame()
})
// 访客准备
$(document).on('click', '.J_getready', function () {
    if (newdetailMessage.isHost) {
        return false
    }
    if (!newdetailMessage.downloadMapEnd) {
        showtoast({
            message: translatesrting('地图下载中,无法准备')
        })
        return false
    }
    player_sound_ready()
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    $this.gxbtn('loading')
    $.controlAjax({
        type: "get",
        url: '/api/details/game/ready',
        showerror: true,
        data: {
            ready: true,
            status: true
        },
        success: function (res) {
            newdetailMessage.isReady = true
            roomStartBtn()

        },
        error: function (req) {
            newdetailMessage.isReady = false
            roomStartBtn()
        }
    })
    return false
})
// 取消准备
$(document).on('click', '.J_getnoready', function () {
    newDetailsgetnoready('click')
})
function newDetailsgetnoready(type) {
    if ($('.J_getnoready').length <= 0) {
        return false
    }
    if (newdetailMessage.isHost) {
        return false
    }
    let isinlook = false
    for (localItem of newdetailMessage.localUserlist) {
        if (localItem.userId == userInfo.userId && localItem.sideId == 10) {
            isinlook = true
        }
    }
    if (isinlook) {
        if (newdetailMessage.roomType != 0 && newdetailMessage.roomType != 1) {

        } else {
            // showtoast({
            //     message: translatesrting('观战中')
            // })
        }
        return false
    }
    player_sound_ready()
    var $this = $('.J_getnoready')
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    if (type != 'click' && newdetailMessage.autoopenready) {
        return false
    }
    if (type == 'click') {
        newdetailMessage.autoopenready = false
        if ($('.J-newdetail-autoReady').is(':checked')) {
            $('.J-newdetail-autoReady').click()
        }
    }
    $this.gxbtn('loading')
    $.controlAjax({
        type: "get",
        url: '/api/details/game/ready',
        showerror: true,
        data: {
            ready: true,
            status: false
        },
        success: function (res) {
            newdetailMessage.isReady = false
            roomStartBtn()
        },
        error: function (req) {
            newdetailMessage.isReady = true
            roomStartBtn()
        }
    })
    return false
}
// 切换房间type
$(document).on('click', '.J_chooseRoomType', function () {
    // *-----房间战斗中阻止-----*
    let isingame = false
    if ($('.newdetail-userList-item-roombot.battle').length > 0) {
        isingame = true
    }
    if (isingame) {
        showtoast({
            message: translatesrting('房间内有人仍在游戏中，无法切换房间模式')
        })
        return false
    }
    // *-----房间战斗中阻止-----*
    var $this = $(this)
    var code = $this.attr('data-code')
    var text = $this.text()
    if (code == newdetailMessage.roomType) {
        return false
    }
    let maxplay = newdetailMessage.mapData.maxPlayerNum
    let nogetminMapPlayer = false
    comOption.roomTypeArray.map((item) => {
        if (item.code == code) {
            if (item.minMapPlayer > maxplay) {
                nogetminMapPlayer = true
            }
        }
    })
    if (nogetminMapPlayer) {
        showtoast({
            message: translatesrting('地图人数') + " " + maxplay + " " + translatesrting('不支持此房间模式，请先切换地图')
        })
        return false
    }
    $.controlAjax({
        type: "POST",
        url: '/api/details/change/roomType',
        showerror: true,
        data: {
            roomType: code
        },
        success: function (res) {
            newdetailMessage.roomType = code
            $('.newdetail-header-roomtype').text(text)
            chooseRoomTypeCD()
        },
        error: function (req) {

        }
    })
})
//同意换位置
$(document).on('click', '.J_agreeChangewei', function () {
    var id = $(this).attr('data-id')
    $('#changeAlertModal').modal('hide')
    $.controlAjax({
        type: "get",
        url: '/api/details/exchange/receive',
        showerror: true,
        data: {
            userId: id
        },
        success: function (res) {

        },
        error: function (req) {

        }
    })
})
// 地图描述
$(document).on('mouseenter', '.newdetail-map-title', function () {
    if (newdetailMessage.mapData.description != '') {
        $('.newdetail-map').addClass('borthhide')
    }
})
$(document).on('mouseleave', '.newdetail-map-title', function () {
    $('.newdetail-map').removeClass('borthhide')
})
// 用户操作列表
$(document).on('mouseenter', '.newdetail-userList-item', function () {
    if (!newdetailMessage.isgetinit) {
        return false
    }
    let roomType = newdetailMessage.roomType
    var $this = $(this)
    let operationArray = {
        open: false,
        change: false,
        addRobot: false,
        close: false,
        addwatch: false
    }
    if (roomType == 0 || roomType == 1) {
        if (newdetailMessage.isHost && !$this.attr('data-id') && !$this.hasClass('newdetail-userList-item-close')) {
            operationArray.close = true
            operationArray.addRobot = true
        }
        if (newdetailMessage.isHost && $this.hasClass('newdetail-userList-item-close')) {
            operationArray.open = true
        }
    } else if (roomType == 2 || roomType == 3 || roomType == 4 || roomType == 5) {
        let A = newdetailMessage.roomTypeMessage['roomType' + roomType].A,
            B = newdetailMessage.roomTypeMessage['roomType' + roomType].B,
            watch = newdetailMessage.roomTypeMessage['roomType' + roomType].watch
        let myIndex = $('.newdetail-userList-item-my').index()
        let objIndex = $this.index()
        if (newdetailMessage.isHost) {
            if (($.inArray(myIndex, A) != -1 && $.inArray(objIndex, A) != -1) || ($.inArray(myIndex, B) != -1 && $.inArray(objIndex, B) != -1)) {
                // 同队
                if ($this.hasClass('newdetail-userList-item-empty')) {
                    // 空位
                    operationArray.addRobot = true;
                } else {
                    // 队友
                }
            }
            if (($.inArray(myIndex, A) != -1 && $.inArray(objIndex, B) != -1) || ($.inArray(myIndex, B) != -1 && $.inArray(objIndex, A) != -1)) {
                // 不同队
                if ($this.hasClass('newdetail-userList-item-empty')) {
                    // 空位
                    operationArray.addRobot = true;
                    operationArray.change = true;
                } else if ($this.find('.gx-menu-newdetail-ai').length > 0) {
                    // 对手Ai
                } else {
                    // 对手
                    operationArray.change = true;
                }
            }
            if ($.inArray(myIndex, B) != -1 || $.inArray(myIndex, A) != -1) {
                if ($.inArray(objIndex, watch) != -1) {
                    // 看观察者
                    if ($this.hasClass('newdetail-userList-item-close')) {
                        // 关闭位
                        operationArray.open = true
                    } else if ($this.hasClass('newdetail-userList-item-empty')) {
                        // 空位
                        operationArray.close = true;
                        operationArray.addwatch = true;
                    } else {
                        // 人
                        operationArray.change = true;
                        operationArray.close = true;
                    }

                }
            }
            if ($.inArray(myIndex, watch) != -1) {
                //  当本人观察者
                if ($.inArray(objIndex, A) != -1 || $.inArray(objIndex, B) != -1) {
                    // 看队伍
                    if ($this.hasClass('newdetail-userList-item-empty')) {
                        // 空位
                        operationArray.addRobot = true;
                        operationArray.change = true;
                    } else if ($this.find('.gx-menu-newdetail-ai').length > 0) {
                        // 对手Ai
                    } else {
                        // 人
                        operationArray.change = true;
                    }
                }
                if ($.inArray(objIndex, watch) != -1) {
                    if ($this.hasClass('newdetail-userList-item-close')) {
                        // 关闭位
                        operationArray.open = true
                    } else if (myIndex != objIndex) {
                        // 同观察者
                        operationArray.close = true;
                    }
                }
            }
        } else {
            if (($.inArray(myIndex, A) != -1 && $.inArray(objIndex, A) != -1) || ($.inArray(myIndex, B) != -1 && $.inArray(objIndex, B) != -1)) {
                // 同队
                if ($this.hasClass('newdetail-userList-item-empty')) {
                    // 空位
                } else {
                    // 队友
                }
            }
            if (($.inArray(myIndex, A) != -1 && $.inArray(objIndex, B) != -1) || ($.inArray(myIndex, B) != -1 && $.inArray(objIndex, A) != -1)) {
                // 不同队
                if ($this.hasClass('newdetail-userList-item-empty')) {
                    // 空位
                    operationArray.change = true;
                } else if ($this.find('.gx-menu-newdetail-ai').length > 0) {
                    // 对手Ai
                    operationArray.change = true;
                } else {
                    // 对手
                    operationArray.change = true;
                }
            }
            if ($.inArray(myIndex, B) != -1 || $.inArray(myIndex, A) != -1) {
                if ($.inArray(objIndex, watch) != -1) {
                    // 看观察者
                    if ($this.hasClass('newdetail-userList-item-close')) {
                        // 关闭位
                    } else if ($this.hasClass('newdetail-userList-item-empty')) {
                        // 空位
                        operationArray.addwatch = true;
                    } else {
                        // 人
                        operationArray.change = true;
                    }

                }
            }
            if ($.inArray(myIndex, watch) != -1) {
                //  当本人观察者
                if ($.inArray(objIndex, watch) != -1) {
                    // 同观察者
                }
                if ($.inArray(objIndex, A) != -1 || $.inArray(objIndex, B) != -1) {
                    // 看队伍
                    if ($this.hasClass('newdetail-userList-item-empty')) {
                        // 空位
                        operationArray.change = true;
                    } else if ($this.find('.gx-menu-newdetail-ai').length > 0) {
                        // 对手Ai
                        operationArray.change = true;
                    } else {
                        // 人
                        operationArray.change = true;
                    }
                }
            }
        }
    }
    if (operationArray.open || operationArray.change || operationArray.addRobot || operationArray.close || operationArray.addwatch) {
        $this.find('.newdetail-userList-item-ping').addClass('hide')
        $this.find('.newdetail-userList-item-menu').addClass('hide')
        // $this.one('mouseleave', function () {
        //     $this.find('.newdetail-userList-item-ping').removeClass('hide')
        //     $this.find('.newdetail-userList-item-menu').removeClass('hide')
        // })
        $this.find('.newdetail-userList-item-empty-operation').css('display', 'inline-block')
        $this.find('.newdetail-userList-item-empty-operation').html(template('newdetail-userList-item-empty-operation-script', {
            operationArray: operationArray,
            changeweiloadingnum: newdetailMessage.changeweiloadingnum
        }))
    } else {
        $this.find('.newdetail-userList-item-empty-operation').css('display', 'none')
    }
})
$(document).on('mouseleave', '.newdetail-userList-item', function () {
    var $this = $(this)

    $('.newdetail-userList-item').find('.newdetail-userList-item-ping').removeClass('hide')
    $('.newdetail-userList-item').find('.newdetail-userList-item-menu').removeClass('hide')
})








//--------------------方法-------------------
// 是否在房间判断
function wsUserStatusdetails(type) {
    wsUserStatus(type)
    if (type == 7) {
        localStorage.setItem('isdetails', true);
    } else {
        localStorage.removeItem('isdetails')
    }
}
// 离开房间
function LeaveGameRoom(type, data) {
    $.controlAjax({
        type: "get",
        url: '/Details/LeaveGameRoom/',
        data: {},
        showerror: true,
        success: function (res) {
            localStorage.removeItem('msgType10')
            localStorage.removeItem('msgType2')
            if (type == 'inviteJoin') {
                saveMessage('inviteJoin', data)
            }
            wsUserStatusdetails(1)
            $('body').css('display', 'none')
            parent.hallMessage.closeRoom('room')
        },
        error: function (req) {

        }
    })
}
// 处理用户列表
function userDellfun(userarray, type) {
    // 房主优先 
    if (!newdetailMessage.isgetinit) {
        return false
    }
    let hostitem = []
    let isinite = false
    userarray.map((item, index) => {
        if (item.isHost) {
            hostitem = item
            userarray.splice(index, 1);
            isinite = true
        }
    })
    if (isinite) {
        userarray.unshift(hostitem)
    }
    // 房主优先
    let addUser = [] //新加入用户
    let oldUser = [] //已有用户
    let delUser = [] //离开的用户
    userarray.map((newItem) => {
        newItem.customField = customFieldFun('decode', newItem.customField)
        newItem.startingLocation = Number(newItem.startingLocation)
        newdetailMessage.optionsCountry.map((optionC) => {
            if (newItem.sideId == optionC.sideId) {
                newItem.JsCountry = optionC
            }
        })
        comOption.optionsColor.map((optionC) => {
            if (newItem.colorId == optionC.colorId) {
                newItem.JsColor = optionC
            }
        })
        comOption.optionsTeam.map((optionC) => {
            if (newItem.teamId == optionC.teamId) {
                newItem.JsTeam = optionC
            }
        })
        var isadd = true
        for (localItem of newdetailMessage.localUserlist) {
            if (newItem.userId == localItem.userId) {
                oldUser.push(newItem)
                isadd = false
            }
        }
        if (isadd) {
            addUser.push(newItem)
        }
    })
    for (localItem of newdetailMessage.localUserlist) {
        let isout = true
        userarray.map((newItem) => {
            if (newItem.userId == localItem.userId || localItem.userId == 'close' || localItem.userId == 'empty') {
                isout = false
            }
        })
        if (isout) {
            delUser.push(localItem)
        }
    }
    // 添加新人
    if (addUser.length > 0 && type != 'error') {
        // 新人由房主主动推公告
        if (newdetailMessage.isHost) {
            setTimeout(function () {
                autoRoomNotice()
            }, 1000)
        }
        addUser.map((item) => {
            let noaddold = true//防止重复添加
            if ($('.newdetail-userList-item' + item.userId).length > 0) {
                noaddold = false
            }
            if (noaddold) {
                let needout = true
                // 处理数据进入本地
                for (let i = 0; i < newdetailMessage.localUserlist.length; i++) {
                    if (newdetailMessage.localUserlist[i].userId == 'empty' || newdetailMessage.localUserlist[i].userId == 'close') {
                        newdetailMessage.localUserlist[i] = item
                        needout = false
                        break
                    }
                }
                // 处理数据进入本地
                let canChange = false
                let myitem = false
                if (item.userId == userInfo.userId) {
                    canChange = true
                    myitem = true
                }
                if (item.isAI && newdetailMessage.isHost) {
                    canChange = true
                }
                let addNewitem = $('.newdetail-userList-item-empty').eq(0)
                for (var i = 0; i < $('.newdetail-userList-item-empty').length; i++) {
                    if (!$('.newdetail-userList-item-empty').eq(i).hasClass('newdetail-userList-item-close')) {
                        addNewitem = $('.newdetail-userList-item-empty').eq(i)
                        break
                    }
                }
                if (newdetailMessage.isHost && newdetailMessage.addrobotIndex != 0) {
                    if ($('.newdetail-userList-item').eq(newdetailMessage.addrobotIndex).hasClass('newdetail-userList-item-empty') && !$('.newdetail-userList-item').eq(newdetailMessage.addrobotIndex).hasClass('newdetail-userList-item-close')) {
                        addNewitem = $('.newdetail-userList-item').eq(newdetailMessage.addrobotIndex)
                    }
                }
                addNewitem.replaceWith(
                    template('newdetail-userList-script', {
                        type: 'user',
                        message: item,
                        isHost: newdetailMessage.isHost,
                        canChange: canChange,
                        myitem: myitem,
                        optionsCountry: newdetailMessage.optionsCountry,
                        optionsColor: comOption.optionsColor,
                        optionsTeam: comOption.optionsTeam,
                        optionsBirth: newdetailMessage.optionsBirth,
                        gameVersion: newdetailMessage.detailsData ? newdetailMessage.detailsData.gameVersion : 0
                    })
                )
                if (myitem && item.sideId == 10 && !newdetailMessage.isHost && newdetailMessage.downloadMapEnd) {
                    $('.J_getready').click()
                }
                if (myitem && item.sideId == 10) {
                    $('.newdetail-userList-item' + item.userId).addClass('inlook')
                } else if (myitem) {
                    $('.newdetail-userList-item' + item.userId).removeClass('inlook')
                }
                $('.newdetail-userList-item' + item.userId + ' .newdetail-userList-item-userbot').tooltip()
                $('.newdetail-userList-item' + item.userId + '.canChange .newdetail-userList-item-menu').each(function () {
                    let height = '20px'
                    // if ($(this).hasClass('newdetail-userList-item-menu-team') || $(this).hasClass('newdetail-userList-item-menu-birth')) {
                    //     height = '20px'
                    // }
                    $(this).gxmenu({
                        height: height,
                        top: "27px",
                        clickHide: true
                    }, function (e) {

                    })
                })
                $('.newdetail-userList-item' + item.userId + '.canChange .gx-menu-newdetail-ai').gxmenu({
                    height: "33px",
                    top: "27px",
                    clickHide: true
                }, function (e) {

                })
                if (needout && newdetailMessage.isHost) {
                    $('.newdetail-userList-item' + item.userId + ' .J_letUserOut').click()
                }
            }
        })
        roomStartBtn()
    }
    if (delUser.length > 0 && type != 'error') {
        // 删除消失人
        delUser.map((item) => {
            // 处理数据进入本地
            for (let i = 0; i < newdetailMessage.localUserlist.length; i++) {
                if (newdetailMessage.localUserlist[i].userId == item.userId) {
                    newdetailMessage.localUserlist[i] = { userId: 'empty' }
                    break
                }
            }
            // 处理数据进入本地
            if (item.userId != userInfo.userId) {
                $('.newdetail-userList-item' + item.userId).replaceWith(
                    template('newdetail-userList-script', {
                        type: 'empty',
                        isHost: newdetailMessage.isHost,
                    })
                )
            }
        })
        roomStartBtn()
    }
    if (oldUser.length > 0) {
        let needchangebot = false
        oldUser.map((oldUseritem) => {
            for (localItem of newdetailMessage.localUserlist) {
                if (oldUseritem.userId == localItem.userId) {
                    if (oldUseritem.userId == userInfo.userId && oldUseritem.sideId == 10 && !newdetailMessage.isHost && newdetailMessage.downloadMapEnd) {
                        $('.J_getready').click()
                    }
                    if (oldUseritem.userId == userInfo.userId) {
                        let showWatch = false
                        if (oldUseritem.userId == userInfo.userId
                            && oldUseritem.sideId == 10
                        ) {
                            showWatch = true
                        }
                        if (showWatch) {
                            $('.J_battleSetup ').removeClass('hide')
                            if (newdetailMessage.roomType == 2 || newdetailMessage.roomType == 3 || newdetailMessage.roomType == 4) {
                                AutoWatchOption('init')
                            }
                        } else {
                            $('.J_battleSetup ').addClass('hide')
                        }
                    }
                    // 进行同步比较
                    if (oldUseritem.userId == userInfo.userId && type != 'error') {
                        // 自己数据听本地 除非error
                        break
                    }
                    if (newdetailMessage.isHost && oldUseritem.isAI && type != 'error') {
                        if (newdetailMessage.roomType != 0 && newdetailMessage.roomType != 1) {
                            // 房主机器人听本地
                            for (let i = 0; i < newdetailMessage.localUserlist.length; i++) {
                                if (newdetailMessage.localUserlist[i].userId == oldUseritem.userId) {
                                    newdetailMessage.localUserlist[i] = oldUseritem
                                    break
                                }
                            }

                        }
                        break
                    }
                    let changemenu = false
                    let changemenuAI = false
                    let Dom = $('.newdetail-userList-item' + localItem.userId)
                    if (localItem.ready != oldUseritem.ready) {
                        // 准备状态
                        changemenu = true
                        Dom.find('.newdetail-userList-item-roombot').replaceWith(
                            template('newdetail-useritem-change-script', {
                                type: 'status',
                                message: oldUseritem
                            })
                        )
                        if (oldUseritem.userId == userInfo.userId) {
                            newdetailMessage.isReady = oldUseritem.ready
                            roomStartBtn()
                        }
                    }
                    if (localItem.isInGame != oldUseritem.isInGame) {
                        // 战斗状态
                        changemenu = true
                        Dom.find('.newdetail-userList-item-roombot').replaceWith(
                            template('newdetail-useritem-change-script', {
                                type: 'status',
                                message: oldUseritem
                            })
                        )
                    }
                    if (localItem.sideId != oldUseritem.sideId) {
                        // 国家状态
                        changemenu = true
                        Dom.find('.newdetail-userList-item-menu-country').html(
                            template('newdetail-useritem-change-script', {
                                type: 'country',
                                message: oldUseritem,
                                optionsCountry: newdetailMessage.optionsCountry,
                                gameVersion: newdetailMessage.detailsData.gameVersion,
                            })
                        )

                        let showWatch = false
                        if (oldUseritem.sideId == 10) {
                            Dom.addClass('inlook')
                        } else {
                            Dom.removeClass('inlook')
                        }
                        if (oldUseritem.userId == userInfo.userId && oldUseritem.sideId == 10 && newdetailMessage.roomType != 4) {
                            showWatch = true
                        }
                    }
                    if (localItem.colorId != oldUseritem.colorId) {
                        // 颜色状态
                        changemenu = true
                        needchangebot = true
                        Dom.find('.newdetail-userList-item-menu-color').html(
                            template('newdetail-useritem-change-script', {
                                type: 'color',
                                message: oldUseritem,
                                optionsColor: comOption.optionsColor
                            })
                        )

                    }
                    if (localItem.teamId != oldUseritem.teamId) {
                        // 队伍状态
                        changemenu = true
                        Dom.find('.newdetail-userList-item-menu-team').html(
                            template('newdetail-useritem-change-script', {
                                type: 'team',
                                message: oldUseritem,
                                optionsTeam: comOption.optionsTeam
                            })
                        )
                    }
                    if (localItem.startingLocation != oldUseritem.startingLocation) {
                        // 出生点状态
                        changemenu = true
                        needchangebot = true
                        Dom.find('.newdetail-userList-item-menu-birth').html(
                            template('newdetail-useritem-change-script', {
                                type: 'birth',
                                message: oldUseritem,
                                optionsBirth: newdetailMessage.optionsBirth,
                            })
                        )
                    }

                    if (localItem.aiLevel != oldUseritem.aiLevel) {
                        // ai等级
                        changemenuAI = true
                        Dom.find('.gx-menu-newdetail-ai').html(
                            template('newdetail-useritem-change-script', {
                                type: 'aiLevel',
                                message: oldUseritem,
                            })
                        )
                    }

                    // 处理数据进入本地
                    for (let i = 0; i < newdetailMessage.localUserlist.length; i++) {
                        if (newdetailMessage.localUserlist[i].userId == oldUseritem.userId) {
                            newdetailMessage.localUserlist[i] = oldUseritem
                            break
                        }
                    }
                    // 处理数据进入本地
                    if (changemenu) {
                        $('.newdetail-userList-item' + oldUseritem.userId + '.canChange .newdetail-userList-item-menu').each(function () {
                            let height = '20px'
                            // if ($(this).hasClass('newdetail-userList-item-menu-team') || $(this).hasClass('newdetail-userList-item-menu-birth')) {
                            //     height = '33px'
                            // }
                            $(this).gxmenu({
                                height: height,
                                top: "27px",
                                clickHide: true
                            }, function (e) {

                            })
                        })
                    }
                    if (changemenuAI) {
                        $('.newdetail-userList-item' + oldUseritem.userId + '.canChange .gx-menu-newdetail-ai').gxmenu({
                            height: "33px",
                            top: "27px",
                            clickHide: true
                        }, function (e) {

                        })
                    }
                }
            }
        })
        if (needchangebot) {
            birthbot()
        }
    }
    if (newdetailMessage.isHost) {
        checkAllready()
    }
    repeatChoose()//去重
    userListSort()//排序
    chooseBlackUser()//黑名单
    if (newdetailMessage.isHost && newdetailMessage.autosetTime) {
        autoOpen()
    }
}
// 用户接收位置更改
function userGetChangeMax(max) {
    let getchange = newdetailMessage.detailsData.maxPlayers - max
    newdetailMessage.detailsData.maxPlayers = max
    if (getchange > 0) {
        // 关闭位置
        for (var i = 0; i < getchange; i++) {
            $('.newdetail-userList-item-empty.loadinguser').eq($('.newdetail-userList-item-empty.loadinguser').length - 1).replaceWith(
                template('newdetail-userList-script', {
                    type: 'close',
                    isHost: newdetailMessage.isHost,
                })
            )
        }
    } else if (getchange < 0) {
        // 开启位置
        for (var i = 0; i < (getchange * -1); i++) {
            $('.newdetail-userList-item.newdetail-userList-item-close').eq(0).replaceWith(
                template('newdetail-userList-script', {
                    type: 'empty',
                    isHost: newdetailMessage.isHost,
                })
            )
        }
    }
    addroomTypeText()
}
// 房间公告
function roomNotice() {
    let roomnotic = getSaveMessage('roomnotice')
    let isempty = true
    if (roomnotic) {
        if (roomnotic.message == '空' || roomnotic.message == '' || roomnotic.roomNo != newdetailMessage.detailsData.roomNo) {
            $('.newdetail-chatDiv-header').html(template('newdetail-chatDiv-header-script', {
                isshow: false,
                message: '',
                isHost: newdetailMessage.isHost
            }))
        } else {
            $('.newdetail-chatDiv-header').html(template('newdetail-chatDiv-header-script', {
                isshow: true,
                message: roomnotic.message,
                isHost: newdetailMessage.isHost
            }))
            isempty = false
        }
    } else {
        $('.newdetail-chatDiv-header').html(template('newdetail-chatDiv-header-script', {
            isshow: false,
            message: '',
            isHost: newdetailMessage.isHost
        }))
    }
    if (isempty) {
        $('.newdetail-chatDiv').addClass('emptyMessage')
    } else {
        $('.newdetail-chatDiv').removeClass('emptyMessage')
    }
    $('.newdetail-chatDiv-scroll').scrollTop($('.newdetail-chatDiv-scroll').prop('scrollHeight'))
}
// 发送房间公告
function autoRoomNotice() {
    let text = $('.newdetail-chatDiv-header-notice').attr('data-text') || ""
    if (text != "") {
        ipcRenderer.send('Main', {
            msgType: "ChatMessage",
            jsonInfo: {
                msg: text,
                noticeType: 5
            }
        });
    }
}
// 聊天内容
function addChatMessage(name, message, userId) {
    player_sound_sendchat()
    let isHost = false
    let reg = new RegExp("<", "g")
    message = message.replace(reg, "<span>" + '<' + "</span>");
    if (newdetailMessage.hostId == userId) {
        isHost = true;
    }
    if (message.indexOf('分钟后未开始游戏，系统将自动解散房间') != -1) {
        name = translatesrting('系统提示')
        message = translatesrting(message)
        $('.newdetail-chatDiv-scroll').append(template('newdetail-chatDiv-item-script', { name: name, message: message, isHost: isHost, sys: true }))
    } else {
        $('.newdetail-chatDiv-scroll').append(template('newdetail-chatDiv-item-script', { name: name, message: message, isHost: isHost }))
    }
    $('.newdetail-chatDiv-scroll').scrollTop($('.newdetail-chatDiv-scroll').prop('scrollHeight'))
}
// 地图模块
function updataMap(mapSha1, isint) {
    $.star({
        type: 'GET',
        url: '/battlecenter/redwar/map/v1/get-map',
        showerror: false,
        data: {
            mapSha1: mapSha1,
        },
        success: function (res) {
            // res.data.imageUrl = 'https://img.ramboplay.com/redwar/map/prod/images/b741253201eea81a9615c72fb73a53ae2058ed5b1652676472.png'
            let imgurl = ''
            if (res.data) {
                imgurl = res.data.imageUrl
            } else {
                imgurl = newdetailMessage.detailsData.imageUrl
            }
            res.data.name = res.data.name + '&&||' + res.data.enName
            newdetailMessage.mapData = res.data
            updataMapUrl(imgurl)
            // var img = new Image()
            // img.src = Cachereturn
            // img.onload = function () {
            let showWatch = false
            newdetailMessage.localUserlist.map((item) => {
                if (item.userId == userInfo.userId && item.sideId == 10) {
                    showWatch = true
                }
            })
            if (res.data) {
                $('.newdetail-map').html(template('newdetail-map-script', {
                    imageUrl: '',
                    name: res.data.name,
                    collectStatus: res.data.collectStatus,
                    isHost: newdetailMessage.isHost,
                    showWatch: showWatch,
                    des: res.data.description
                }))
                newdetailMessage.downloadMapEnd = true
            } else {
                $('.newdetail-map').html(template('newdetail-map-script', {
                    imageUrl: '',
                    name: newdetailMessage.detailsData.mapName,
                    collectStatus: false,
                    isHost: newdetailMessage.isHost,
                    showWatch: false,
                    des: ''
                }))
                if (!newdetailMessage.detailsData.mapTest) {
                    downloadMap(30)
                }
            }
            updataGameVersion()
            // }
        },
        error: function (req) { }
    });
}
function updataMapUrl(imgurl) {
    oneImgCache(imgurl).then((Cachereturn) => {
        $('.newdetail-map-imgdiv .newdetail-map-img').attr('src', Cachereturn)
        $('.newdetail-map-imgdiv .newdetail-map-img').one('load', () => {
            birthbot()
        })
    })
}
// 出生点
function birthbot() {
    if (newdetailMessage.detailsData.startingLocations == "" || newdetailMessage.detailsData.startingLocations == null || newdetailMessage.detailsData.startingLocations.length == 0) {
        if (!newdetailMessage.detailsData.mapTest) {
            downloadMap(30)
        }
        return false
    }
    let birtharray = []
    $('.newdetail-map-imgdiv-borth').remove()
    for (var i = 0; i < newdetailMessage.detailsData.startingLocations.length; i++) {
        let startingLocations = newdetailMessage.detailsData.startingLocations[i]
        birtharray.push({
            name: '',
            userId: '',
            color: '',
            startingLocation: i + 1,
            x: startingLocations.x * 100,
            y: startingLocations.y * 100
        })
    }
    for (localItem of newdetailMessage.localUserlist) {
        if (localItem.userId != 'empty' && localItem.userId != 'close' && localItem.startingLocation != -1) {
            for (birtharrayitem of birtharray) {
                if (Number(localItem.startingLocation) + 1 == birtharrayitem.startingLocation) {
                    birtharrayitem.name = localItem.name
                    birtharrayitem.userId = localItem.userId
                    birtharrayitem.color = localItem.JsColor.color
                }
            }
        }
    }
    $('.newdetail-map-imgdiv').append(template('newdetail-map-birth', {
        birtharray: birtharray,
        myUserId: userInfo.userId
    }))
    if ($('#bigmap').hasClass('in')) {
        $('.bigmap-content').append(template('newdetail-map-birth', {
            birtharray: birtharray,
            myUserId: userInfo.userId
        }))
    }
}
// 更新版本模式
function updataGameVersion() {
    if (!newdetailMessage.mapData) {
        return false
    }
    let mapVersion = newdetailMessage.mapData.supportedVersion.split(',')
    for (var i = 0; i < mapVersion.length; i++) {
        for (comitem of comOption.optionsGameVersion) {
            if (comitem.code == mapVersion[i]) {
                mapVersion[i] = comitem
            }
        }
    }
    let getmodesarray = []
    let modesarray = newdetailMessage.mapData.models
    for (var i = 0; i < modesarray.length; i++) {
        for (comitem of comOption.optionsMode) {
            if (comitem.code == modesarray[i]) {
                getmodesarray.push(comitem)
            }
        }
    }
    let mode = newdetailMessage.detailsData.mode

    $('.newdetail-map-title').html(template('newdetail-mapchoose-script', {
        isHost: newdetailMessage.isHost,
        gameVersionarray: mapVersion,
        gameVersion: newdetailMessage.detailsData.gameVersion,
        modesarray: getmodesarray,
        mode: mode,
        name: newdetailMessage.mapData.name,
        des: newdetailMessage.mapData.description
    }))

    if ($.inArray(newdetailMessage.detailsData.gameVersion.toString(), newdetailMessage.mapData.supportedVersion.split(',')) == -1) {
        $('.J_chooseGameVersion').eq(0).click()
    }
    $('.newdetail-mapchoose-menu.canchange').gxmenu({
        height: "30px",
        top: "33px",
        clickHide: true
    }, function (e) {

    })
    $('.J_chooseCountry').each(function () {
        if ($(this).attr('data-code') == 9) {
            $(this).addClass('hide')
        }
    })
    if (newdetailMessage.detailsData.gameVersion == 0) {
        //尤里
        newdetailMessage.optionsCountry.map((item) => {
            if (item.sideId == 9) {
                item.country = 'Yuri'
                item.cname = '尤里'
            }
        })
        $('.J_chooseCountry').each(function () {
            if ($(this).attr('data-code') == 9) {
                $(this).removeClass('hide')
                $(this).text(translatesrting('尤里'))
            }
        })
        $('.newdetail-userList-item.canChange').each(function () {
            let $this = $(this)
            if ($this.find('.newdetail-userList-item-menu-country .gx-menu-in-text').attr('data-name') == '神州') {
                $this.find('.J_chooseCountry').each(function () {
                    let $thischoose = $(this)
                    if ($thischoose.attr('data-code') == -1) {
                        $thischoose.click()
                    }
                })
            }
        })
    } else if (newdetailMessage.detailsData.gameVersion == 3) {
        //共辉
        newdetailMessage.optionsCountry.map((item) => {
            if (item.sideId == 9) {
                item.country = 'Shenzhen'
                item.cname = '神州'
            }
        })
        $('.J_chooseCountry').each(function () {
            if ($(this).attr('data-code') == 9) {
                $(this).removeClass('hide')
                $(this).text(translatesrting('神州'))
            }
        })
        $('.newdetail-userList-item.canChange').each(function () {
            let $this = $(this)
            if ($this.find('.newdetail-userList-item-menu-country .gx-menu-in-text').attr('data-name') == '尤里') {
                $this.find('.J_chooseCountry').each(function () {
                    let $thischoose = $(this)
                    if ($thischoose.attr('data-code') == -1) {
                        $thischoose.click()
                    }
                })
            }
        })
    } else {
        $('.newdetail-userList-item.canChange').each(function () {
            let $this = $(this)
            if ($this.find('.newdetail-userList-item-menu-country .gx-menu-in-text').attr('data-code') == 9) {
                $this.find('.J_chooseCountry').each(function () {
                    let $thischoose = $(this)
                    if ($thischoose.attr('data-code') == -1) {
                        $thischoose.click()
                    }
                })
            }
        })
    }
    if (newdetailMessage.isHost) {
        updataGameseting()
    }
}
// 渲染更新游戏设置
function updataGameseting(type) {
    if (newdetailMessage.isHost) {
        //'basics' 'game' 'alliance' 'special'
        if (newdetailMessage.setTingtype == 'basics') {
            let UnitCount = 0
            let UnitCountmust = false
            let Credits = 0
            newdetailMessage.detailsData.gameOption.map((item) => {
                if (item.key == 'UnitCount') {
                    UnitCount = item.value
                    UnitCountmust = item.must
                }
                if (item.key == 'Credits') {
                    Credits = item.value
                }
                for (var i = 0; i < newdetailMessage.basicsArray.length; i++) {
                    if (item.value == 'False' || item.value == 'false') {
                        item.value = false
                    }
                    if (item.value == 'True' || item.value == 'true') {
                        item.value = true
                    }
                    if (newdetailMessage.basicsArray[i].key == item.key) {
                        newdetailMessage.basicsArray[i] = item
                    }
                }
            })
            $('.newdetail-map-bottom').addClass('isHost')
            $('.newdetail-map-bottom').html(template('newdetail-map-bottom-script', { isHost: newdetailMessage.isHost }))
            $('.newdetail-setting-content').html(template('newdetail-setting-script', {
                type: newdetailMessage.setTingtype,
                basicsArray: newdetailMessage.basicsArray,
                UnitCount: UnitCount,
                Credits: Credits,
                UnitCountmust: UnitCountmust
            }))
            $('.newdetail-setting-content .setting-range').each(function () {
                let settingRange = $(this)
                settingRange.GXRangeSlider({
                    min: settingRange.attr('min'), max: settingRange.attr('max'), step: settingRange.attr('step'), callback: function (e) {
                        settingRange.closest('.media').find('.setting-range-text span').text(e.value)
                    }, color: 'nocolor'
                });
            })

        } else if (newdetailMessage.setTingtype == 'game') {
            let BanAllSuperWeapon = false
            let BanBigSuperWeapons = false
            let showBanBigSuperWeapons = translatesrting('全部可用')
            let BanAllSuperWeaponmust = false
            let BanBigSuperWeaponsmust = false
            newdetailMessage.detailsData.gameOption.map((item) => {
                for (var i = 0; i < newdetailMessage.gameArray.length; i++) {
                    if (item.value == 'False' || item.value == 'false') {
                        item.value = false
                    }
                    if (item.value == 'True' || item.value == 'true') {
                        item.value = true
                    }
                    if (newdetailMessage.gameArray[i].key == item.key) {
                        newdetailMessage.gameArray[i] = item
                    }
                }
                if (item.key == "BanAllSuperWeapon") {
                    BanAllSuperWeapon = item.value
                    if (item.must) {
                        BanAllSuperWeaponmust = true
                    }
                }
                if (item.key == "BanBigSuperWeapons") {
                    BanBigSuperWeapons = item.value
                    if (item.must) {
                        BanBigSuperWeaponsmust = item.must
                    }
                }
            })
            if (BanAllSuperWeapon) {
                showBanBigSuperWeapons = translatesrting('禁用')
            }
            if (!BanAllSuperWeapon && BanBigSuperWeapons) {
                showBanBigSuperWeapons = translatesrting('仅支援型')
            }
            if (!BanAllSuperWeapon && !BanBigSuperWeapons) {
                showBanBigSuperWeapons = translatesrting('全部可用')
            }
            let showBanBigSuperWeaponsarray = [
                { cnValue: translatesrting('全部可用'), code: 1 },
                { cnValue: translatesrting('仅支援型'), code: 2 },
                { cnValue: translatesrting('禁用'), code: 3 }
            ]

            if (BanAllSuperWeaponmust && BanAllSuperWeapon && BanBigSuperWeaponsmust && BanBigSuperWeapons) {
                showBanBigSuperWeaponsarray = [
                    { cnValue: translatesrting('禁用'), code: 3 }
                ]
            }
            if (!BanAllSuperWeaponmust && BanBigSuperWeaponsmust && BanBigSuperWeapons) {
                showBanBigSuperWeaponsarray = [
                    { cnValue: translatesrting('仅支援型'), code: 2 },
                ]
            }
            if (!BanAllSuperWeaponmust && !BanBigSuperWeaponsmust) {
                showBanBigSuperWeaponsarray = [
                    { cnValue: translatesrting('全部可用'), code: 1 },
                    { cnValue: translatesrting('仅支援型'), code: 2 },
                    { cnValue: translatesrting('禁用'), code: 3 }
                ]
            }
            if (BanAllSuperWeaponmust && BanAllSuperWeapon && !BanBigSuperWeaponsmust) {
                showBanBigSuperWeaponsarray = [
                    { cnValue: translatesrting('禁用'), code: 3 }
                ]
            }
            if (BanAllSuperWeaponmust && !BanAllSuperWeapon && BanBigSuperWeaponsmust && !BanBigSuperWeapons) {
                showBanBigSuperWeaponsarray = [
                    { cnValue: translatesrting('全部可用'), code: 1 },
                ]
            }
            if (BanAllSuperWeaponmust && !BanAllSuperWeapon && !BanBigSuperWeaponsmust) {
                showBanBigSuperWeaponsarray = [
                    { cnValue: translatesrting('全部可用'), code: 1 },
                    { cnValue: translatesrting('仅支援型'), code: 2 },]
            }
            if (!BanAllSuperWeaponmust && BanBigSuperWeaponsmust && !BanBigSuperWeapons) {
                showBanBigSuperWeaponsarray = [
                    { cnValue: translatesrting('全部可用'), code: 1 },
                    { cnValue: translatesrting('禁用'), code: 3 }
                ]
            }
            if (BanAllSuperWeaponmust && BanAllSuperWeapon && BanBigSuperWeaponsmust && !BanBigSuperWeapons) {
                showBanBigSuperWeaponsarray = [
                    { cnValue: translatesrting('禁用'), code: 3 }
                ]
            }
            if (BanAllSuperWeaponmust && !BanAllSuperWeapon && BanBigSuperWeaponsmust && BanBigSuperWeapons) {
                showBanBigSuperWeaponsarray = [
                    { cnValue: translatesrting('仅支援型'), code: 2 },
                ]
            }
            let introduce = '<div>' + translatesrting('无敌油井：科技油井无法被摧毁，无法被引爆，即使被摧毁后也可重生。') + '</div>'
            introduce += '<div>' + translatesrting('特殊工程师：开启后需多个工程师才可以占领红血敌方建筑') + '</div>'
            introduce += '<div>' + translatesrting('永久雷达：开局赠送不会失效的小地图雷达。') + '</div>'
            introduce += '<div>' + translatesrting('永久间谍卫星：开局赠送不会失效的间谍卫星。') + '</div>'
            introduce += '<div>' + translatesrting('禁止驻军：所有地图内的中立可驻军建筑变为不可驻军（碉堡除外），且会按照中立单位 AI 进行索敌攻击。') + '</div>'
            introduce += '<div>' + translatesrting('禁止间谍：所有阵营不可以造间谍单位。') + '</div>'
            introduce += '<div>' + translatesrting('使用盟友维修厂：游戏中是否可以使用盟友的维修厂进行修理。原版与共辉默认开启，尤里默认关闭。') + '</div>'
            let canshowBanBigSuperWeapons = false
            if (BanBigSuperWeaponsmust && BanAllSuperWeaponmust) {
                canshowBanBigSuperWeapons = true
            }
            $('.newdetail-setting-content').html(template('newdetail-setting-script', {
                type: 'game',
                gameArray: newdetailMessage.gameArray,
                showBanBigSuperWeapons: showBanBigSuperWeapons,
                showBanBigSuperWeaponsarray: showBanBigSuperWeaponsarray,
                canshowBanBigSuperWeapons: canshowBanBigSuperWeapons,
                introduce: introduce
            }))
            $('.newdetail-setting-content [data-toggle="tooltip"]').tooltip()
            $('.newdetail-setting-game .canchange').gxmenu({
                height: "30px",
                top: "27px",
                clickHide: true
            }, function (e) {

            })
        } else if (newdetailMessage.setTingtype == 'special') {
            let specialArray = []
            newdetailMessage.specialArray.map((item) => {
                if (item.gameVersion == newdetailMessage.detailsData.gameVersion) {
                    specialArray = item.option
                }
            })
            newdetailMessage.detailsData.gameOption.map((item) => {
                for (var i = 0; i < specialArray.length; i++) {
                    if (item.value == 'False' || item.value == 'false') {
                        item.value = false
                    }
                    if (item.value == 'True' || item.value == 'true') {
                        item.value = true
                    }
                    if (specialArray[i].key == item.key) {
                        specialArray[i] = item
                    }
                }
            })
            let introduce = '<div>' + translatesrting('禁止法国：法国开局后会变为美国科技。') + '</div>'
            introduce += '<div>' + translatesrting('禁止尤里：尤里开局后会变为苏俄科技。') + '</div>'
            introduce += '<div>' + translatesrting('禁止韩国：韩国开局后会变为美国科技。') + '</div>'
            introduce += '<div>' + translatesrting('禁止海军：所有阵营不可以造船厂。') + '</div>'
            introduce += '<div>' + translatesrting('禁止渗透高科：所有阵营的 T3 科技建筑将变为不可渗透。') + '</div>'
            introduce += '<div>' + translatesrting('单基地限制：所有玩家只可以出一个基地车/基地，拥有单位则不可建造多余的基地车，但是可以偷取其它玩家的基地。') + '</div>'
            introduce += '<div>' + translatesrting('禁止神州：神州开局后会变为苏俄科技。') + '</div>'
            introduce += '<div>' + translatesrting('随机国家各不相同：同一局中游戏通过「随机国家」不会出现两个相同的国家。') + '</div>'
            $('.newdetail-setting-content').html(template('newdetail-setting-script', { type: 'special', specialArray: specialArray, introduce: introduce }))
            $('.newdetail-setting-content [data-toggle="tooltip"]').tooltip()
        }
    } else {
        let showgameList = [
            { key: "ShortGame" },
            { key: "BridgeDestroy" },
            { key: "MCVRedeploy" },
            { key: "IngameAllying" },
            { key: "Credits" },
            { key: "UnitCount" },
            { key: "BuildOffAlly" }
        ]
        let showgameListtwo = [
            { key: "InvincibleOilWell" },
            { key: "FreeRadar" },
            { key: "RevealShroud" },
            { key: "BanGarrison" },
            { key: "BanSpy" },
            { key: "BanFrance" },
            { key: "BanYuri" },
            { key: "BanKorea" },
            { key: "BanSz" },
            { key: "NoPenetration" },
            { key: "SingleCNST" },
            { key: "BanNavy" },
            { key: "AllyRepair" },
            { key: "NoRandmoSameSide" },
            { key: "ExtremeAI" },
            { key: "NoDogEngiEat" },
            { key: "NoSpawnPreviews" },
            { key: "MultiEngineer" },
            { key: "Crates" }

        ]

        let spuerarray = { key: 'JSSpuer', value: false, valueType: 0, cnValue: translatesrting('超级武器') }
        let BanAllSuperWeapon = false
        let BanBigSuperWeapons = false
        let BanAllSuperWeaponMust = false
        let BanBigSuperWeaponsMust = false
        newdetailMessage.detailsData.gameOption.map((item) => {
            for (var i = 0; i < showgameList.length; i++) {
                if (showgameList[i].key == item.key) {
                    showgameList[i] = item
                    if (item.value == 'False' || item.value == 'false') {
                        showgameList[i].value = false
                    }
                    if (item.value == 'True' || item.value == 'true') {
                        showgameList[i].value = true
                    }
                }
            }
            for (var i = 0; i < showgameListtwo.length; i++) {
                if (showgameListtwo[i].key == item.key) {

                    showgameListtwo[i] = item
                    if (item.value == 'False' || item.value == 'false') {
                        showgameListtwo[i].value = false
                    }
                    if (item.value == 'True') {
                        showgameListtwo[i].value = true
                    }
                    if (showgameListtwo[i].key == "AllyRepair") {
                        if (newdetailMessage.detailsData.gameVersion != 0) {
                            showgameListtwo[i].cnValue = "不使用盟友维修厂"
                            showgameListtwo[i].value = !item.value
                        } else {
                            showgameListtwo[i].cnValue = "使用盟友维修厂"
                            showgameListtwo[i].value = item.value
                        }
                    }
                }
            }
            if (item.key == "BanAllSuperWeapon") {
                if (item.value == 'False' || item.value == 'false') {
                    BanAllSuperWeapon = false
                }
                if (item.value == 'True') {
                    BanAllSuperWeapon = true
                }
            }
            if (item.key == "BanBigSuperWeapons") {
                if (item.value == 'False' || item.value == 'false') {
                    BanBigSuperWeapons = false
                }
                if (item.value == 'True') {
                    BanBigSuperWeapons = true
                }
            }
        })
        if (BanAllSuperWeapon) {
            spuerarray.value = false
            spuerarray.cnValue = translatesrting('禁用超级武器')
        }
        if (!BanAllSuperWeapon && BanBigSuperWeapons) {
            spuerarray.value = true
            spuerarray.cnValue = translatesrting('超级武器仅支援型')
        }
        if (!BanAllSuperWeapon && !BanBigSuperWeapons) {
            spuerarray.value = true
            spuerarray.cnValue = translatesrting('超级武器')
        }
        if (spuerarray.cnValue != translatesrting('超级武器')) {
            spuerarray.value = true
            showgameListtwo.push(spuerarray)
        }
        let introduce = '<div>' + translatesrting('无敌油井：科技油井无法被摧毁，无法被引爆，即使被摧毁后也可重生。') + '</div>'
        introduce += '<div>' + translatesrting('特殊工程师：开启后需多个工程师才可以占领红血敌方建筑') + '</div>'
        introduce += '<div>' + translatesrting('永久雷达：开局赠送不会失效的小地图雷达。') + '</div>'
        introduce += '<div>' + translatesrting('永久间谍卫星：开局赠送不会失效的间谍卫星。') + '</div>'
        introduce += '<div>' + translatesrting('禁止驻军：所有地图内的中立可驻军建筑变为不可驻军（碉堡除外），且会按照中立单位 AI 进行索敌攻击。') + '</div>'
        introduce += '<div>' + translatesrting('禁止间谍：所有阵营不可以造间谍单位。') + '</div>'
        introduce += '<div>' + translatesrting('超级武器：仅支援型将禁用攻击性超级武器。') + '</div>'
        introduce += '<div>' + translatesrting('工具箱：开启后地图内会随机出现升级工具箱。') + '</div>'
        introduce += '<div>' + translatesrting('禁止法国：法国开局后会变为美国科技。') + '</div>'
        introduce += '<div>' + translatesrting('禁止尤里：尤里开局后会变为苏俄科技。') + '</div>'
        introduce += '<div>' + translatesrting('禁止韩国：韩国开局后会变为美国科技。') + '</div>'
        introduce += '<div>' + translatesrting('禁止神州：神州开局后会变为苏俄科技。') + '</div>'
        introduce += '<div>' + translatesrting('禁止渗透高科：所有阵营的 T3 科技建筑将变为不可渗透。') + '</div>'
        introduce += '<div>' + translatesrting('单基地限制：所有玩家只可以出一个基地车/基地，拥有单位则不可建造多余的基地车，但是可以偷取其它玩家的基地。') + '</div>'
        introduce += '<div>' + translatesrting('禁止海军：所有阵营不可以造船厂。') + '</div>'
        introduce += '<div>' + translatesrting('随机国家各不相同：同一局中游戏通过「随机国家」不会出现两个相同的国家。') + '</div>'
        introduce += '<div>' + translatesrting('使用盟友维修厂：游戏中是否可以使用盟友的维修厂进行修理。原版与共辉默认开启，尤里默认关闭。') + '</div>'
        if ($('.newdetail-showjoinUserSetItem-header').length == 0) {
            $('.newdetail-map-bottom').html(template('newdetail-map-bottom-script', { isHost: newdetailMessage.isHost, showgameList, showgameListtwo, introduce, type: type }))
            $('.newdetail-showjoinUserSetItem-header [data-toggle="tooltip"]').tooltip()
        }
        $('.newdetail-showjoinUserSetItem-content-left').html(template('newdetail-map-bottom-list-script', { type: 'left', showgameList, init: type }))
        if (type != 'init') {
            if (newdetailMessage.detailsData.gameVersion == 1) {
                // 原版
                showgameListtwo.map((item) => {
                    if (item.key == 'BanSz') {
                        item.value = false
                    }
                    if (item.key == 'BanYuri') {
                        item.value = false
                    }
                })
            }
            if (newdetailMessage.detailsData.gameVersion == 0) {
                // 尤里
                showgameListtwo.map((item) => {
                    if (item.key == 'BanSz') {
                        item.value = false
                    }
                })
            }
            if (newdetailMessage.detailsData.gameVersion == 3) {
                // 神州
                showgameListtwo.map((item) => {
                    if (item.key == 'BanYuri') {
                        item.value = false
                    }
                })
            }
            $('.newdetail-showjoinUserSetItem-content-right').html(template('newdetail-map-bottom-list-script', { type: 'right', showgameListtwo }))
        }
    }
}
// 房间按钮
function roomStartBtn(allready) {
    $('.newdetail-start').html(template('newdetail-start-script', { isHost: newdetailMessage.isHost, ready: newdetailMessage.isReady, allready: allready }))
    if (!newdetailMessage.isHost) {
        let message = {
            ready: newdetailMessage.isReady
        }
        $('.newdetail-userList-item-my').find('.newdetail-userList-item-roombot').replaceWith(
            template('newdetail-useritem-change-script', {
                type: 'status',
                message: message
            })
        )
    }
}
// 房间邀请按钮
function roomInviteBtn() {
    if (newdetailMessage.detailsData.isHavePas) {
        $('.newdetail-Invite').html(template('newdetail-Invite-script', {
            nopass: !newdetailMessage.detailsData.isHavePas
        }))
    } else {
        $.star({
            type: 'GET',
            url: '/battlecenter/platform/config/gold/v1/list',
            showerror: false,
            data: {
            },
            success: function (res) {
                $('.newdetail-Invite').html(template('newdetail-Invite-script', {
                    nopass: !newdetailMessage.detailsData.isHavePas,
                    coin: res.data.userSendHallInviteGold
                }))
            },
            error: function (req) { }
        });
    }
}
function checkAllready() {
    let allready = true;
    let checkreadyarray = []
    newdetailMessage.localUserlist.map((item) => {
        if (!item.isHost && !item.isAI && item.userId != 'empty' && item.userId != 'close') {
            checkreadyarray.push(item)
        }
    })
    if (checkreadyarray.length > 0) {
        for (var i = 0; i < checkreadyarray.length; i++) {
            if (!checkreadyarray[i].ready || checkreadyarray[i].isInGame) {
                allready = false
            }
        }
    } else {
        allready = true
    }
    if (newdetailMessage.roomType != 0 && newdetailMessage.roomType != 1 && newdetailMessage.roomType != 5 && allready) {
        let roomType = newdetailMessage.roomType
        let A = newdetailMessage.roomTypeMessage['roomType' + roomType].A,
            B = newdetailMessage.roomTypeMessage['roomType' + roomType].B
        A.map((item) => {
            if ($('.newdetail-userList-item').eq(item).hasClass('newdetail-userList-item-empty')) {
                allready = false
            }
        })
        B.map((item) => {
            if ($('.newdetail-userList-item').eq(item).hasClass('newdetail-userList-item-empty')) {
                allready = false
            }
        })
    }
    if (newdetailMessage.roomType == 5 && allready) {
        let roomType = newdetailMessage.roomType
        let A = newdetailMessage.roomTypeMessage['roomType' + roomType].A,
            B = newdetailMessage.roomTypeMessage['roomType' + roomType].B,
            allA = true,
            allB = true,
            Anum = 0,
            Bum = 0
        A.map((item) => {
            if ($('.newdetail-userList-item').eq(item).hasClass('newdetail-userList-item-empty')) {
                Anum = Anum + 1
                allA = false
            }
        })
        B.map((item) => {
            if ($('.newdetail-userList-item').eq(item).hasClass('newdetail-userList-item-empty')) {
                Bum = Bum + 1
                allB = false
            }
        })
        if ((allA || allB) && Anum < 4 && Bum < 4) {
            allready = true
        } else {
            allready = false
        }
    }
    roomStartBtn(allready)
}
// 开始游戏
function startGame() {
    $('.J_startGame').gxbtn('loading')
    parent.startModal()
    wsUserStatusdetails(8)
    $.controlAjax({
        type: "get",
        url: '/api/details/game/ready',
        data: {
            ready: false,
            status: true
        },
        success: function (res) { },
        error: function (req) {
            $('.J_startGame').gxbtn('reset')
        }
    })
}
// 自动关房间
var reloadfun = null
var reloadfunlast = null
var reloadfunlasttwo = null
function AutoClose(type) {
    clearTimeout(reloadfun)
    clearTimeout(reloadfunlast)
    clearTimeout(reloadfunlasttwo)
    if (type == 'close') {
        return false
    }
    if (!newdetailMessage.detailsData.isHavePas && newdetailMessage.isHost) {
        $.star({
            type: 'GET',
            url: '/battlecenter/platform/config/v1/list',
            showerror: false,
            data: {},
            success: function (res) {
                reloadfun = setTimeout(function () {
                    wsUserStatusdetails(1)
                    setTimeout(() => {
                        LeaveGameRoom()
                        trackingFunc('room_autoclose')
                        wsUserStatusdetails(1)
                    }, 1000)
                }, 300 * 1000)
                reloadfunlast = setTimeout(function () {
                    var chatmesg = '2' + translatesrting('分钟后未开始游戏，系统将自动解散房间');
                    ipcRenderer.send('Main', {
                        msgType: "ChatMessage",
                        jsonInfo: {
                            msg: chatmesg
                        }
                    });
                }, 300 * 1000 - 120000)

                reloadfunlasttwo = setTimeout(function () {
                    var chatmesg = '1' + translatesrting('分钟后未开始游戏，系统将自动解散房间');
                    ipcRenderer.send('Main', {
                        msgType: "ChatMessage",
                        jsonInfo: {
                            msg: chatmesg
                        }
                    });
                }, 300 * 1000 - 60000)
            },
            error: function (req) { }
        });
    }
}
// 自动开始游戏
var newautoOpenTime = null
function autoOpenTime(type) {
    // return false
    if (type == 'open') {
        newdetailMessage.autoopencheck = true
        localStorage.removeItem('roomAutoOpen')
        autoOpen()
    } else if (type == 'close') {
        newdetailMessage.autoopencheck = false
        saveMessage('roomAutoOpen', { check: false })
    }
}
var newautoOpen = null
var newautoOpenloading = false
var newautoOpenNum = 10
function autoOpen() {
    // return false
    let checkreadyarray = []
    let canautoOpen = true
    newdetailMessage.localUserlist.map((item) => {
        if (!item.isHost && !item.isAI && item.userId != 'empty' && item.userId != 'close') {
            checkreadyarray.push(item)
        }
    })
    if (checkreadyarray.length > 0) {
        for (var i = 0; i < checkreadyarray.length; i++) {
            if (!checkreadyarray[i].ready || checkreadyarray[i].isInGame) {
                canautoOpen = false
            }
        }
    } else {
        canautoOpen = false
    }
    if (newdetailMessage.roomType != 0 && newdetailMessage.roomType != 1 && canautoOpen) {
        let roomType = newdetailMessage.roomType
        let A = newdetailMessage.roomTypeMessage['roomType' + roomType].A,
            B = newdetailMessage.roomTypeMessage['roomType' + roomType].B
        A.map((item) => {
            if ($('.newdetail-userList-item').eq(item).hasClass('newdetail-userList-item-empty')) {
                canautoOpen = false
            }
        })
        B.map((item) => {
            if ($('.newdetail-userList-item').eq(item).hasClass('newdetail-userList-item-empty')) {
                canautoOpen = false
            }
        })
    } else {
        $('.newdetail-userList-item').each(function () {
            let obj = $(this).eq(0)
            if (obj.hasClass('newdetail-userList-item-empty') && !obj.hasClass('newdetail-userList-item-close')) {
                canautoOpen = false
            }
        })
    }
    if (!newdetailMessage.autoopencheck) {
        canautoOpen = false
    }
    if (canautoOpen && !newautoOpenloading) {
        newautoOpenloading = true
        ipcRenderer.send('Main', {
            msgType: "ChatMessage",
            jsonInfo: {
                noticeType: 10,
                msg: translatesrting('所有人已准备完毕，游戏将在10秒后自动开始。')
            }
        });
        newautoOpen = setInterval(() => {
            newautoOpenNum = newautoOpenNum - 1
            if (newautoOpenNum <= 0) {
                newautoOpenNum = 10
                clearInterval(newautoOpen)
                $('.J_startGame').click()
                return false
            }
        }, 1000)
        setTimeout(function () {
            newautoOpenloading = false
        }, 20000)
    }

    if (!canautoOpen) {
        clearInterval(newautoOpen)
        newautoOpen = null
        newautoOpenNum = 10
    }
}
function downloadMap(num) {
    if (num >= 100) {
        newdetailMessage.downloadMapEnd = true
    } else {
        newdetailMessage.downloadMapEnd = false
    }
    let obj = $('.newdetail-map .gx-map-mask')
    obj.css('display', "inline-block")
    var getnum = num
    var shownum = parseInt(obj.find('.gx-map-mask-circle').attr('data-br'))
    if (getnum != shownum) {
        obj.find('.gx-map-mask-progress-br').css('width', getnum + '%')
        obj.find('.gx-map-mask-progress-br').attr('data-br', getnum)
        if (getnum >= 100) {
            setTimeout(function () {
                obj.css('display', "none")
                // $this.removeClass('Match-map-item-inload')
            }, 300)
        }

    }
}
//邀请
var InviteMessage = {
    list: []
}
setInterval(function () {
    if (getSaveMessage('detailsInvite')) {
        friendInvite(getSaveMessage('detailsInvite'))
        localStorage.removeItem('detailsInvite')
    }
}, 1000)
function friendInvite(data) {
    if ($('.Invite-gxmodal').length >= 3) {
        $('.Invite-gxmodal').eq(0).remove()
    }

    for (var i = 0; i < InviteMessage.list.length; i++) {
        if (InviteMessage.list[i].invitePeopleName == data.invitePeopleName && InviteMessage.list[i].roomNo == data.roomNo) {
            return false
        }
    }
    if (data.roomNo == newdetailMessage.detailsData.roomNo) {
        return false
    }
    InviteMessage.list.push(data)
    let modalId = "Invite-gxmodal" + InviteMessage.list.length
    let btnHTML = '<a href="javascript:void(0)" ondragstart="return false" id="" class="' + 'btn-middle btn-middle-blue J_veranda_join J_sound_click' + '" data-pwd="' + data.pwd + '" data-dismiss="modal" data-id="' + data.roomNo + '" data-url="/Details/newindex?invitename=' + encodeURI(data.invitePeopleName) + '">' + translatesrting('接受') + '</a>'
    btnHTML = btnHTML + '<a href="javascript:void(0)" ondragstart="return false" id="" class="' + 'btn-middle btn-style-ash' + '"data-dismiss="modal">' + translatesrting('拒绝') + '</a>'
    let titleHtml = '<div class="ft-white-tr f16 text-center">' + translatesrting('组队邀请') + '</div>'
    if (data.remark) {
        var centent = '<div class="mt25">' + data.remark + ' ' + translatesrting('邀请您加入') + ' ' + data.roomName + '</div>'
    } else {
        var centent = '<div class="mt25">' + data.invitePeopleName + ' ' + translatesrting('邀请您加入') + ' ' + data.roomName + '</div>'
    }
    let InviteModal = '<div class="modal gx-gxmodal Invite-gxmodal" data-index="' + (InviteMessage.list.length - 1) + '" id="' + modalId + '" data-keyboard="false" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="ActivateCodeModal"><div class="modal-dialog gx-modal-smell-dialog" role="document"><div class="gx-modal-smell"><div class="gx-modal-smell-header"><div class="header-window"><span class="header-close iconfont ft-ash" data-dismiss="modal">&#xe602;</span></div></div><div class="text-center">' + titleHtml + centent + '</div><div class="gx-gxmodal-foot">' + btnHTML + '</div></div></div></div>'
    $('.friendInvite-content').append(InviteModal)
    $('#' + modalId + '').modal('show')
    $('#' + modalId + '').on('hidden.bs.modal', function () {
        $('#' + modalId + '').remove();
    })
}
$(document).on('hide.bs.modal', '.Invite-gxmodal', function () {
    var $this = $(this)
    InviteMessage.list.splice($this.attr('data-index'), 1)
})
$(document).on('click', '.J_veranda_join.J_sound_click', function () {
    var $this = $(this)
    $this.gxbtn('loading')
    var url = $(this).attr('data-url') || '/Details/newindex'
    var id = $this.attr('data-id')
    var pwd = $this.attr('data-pwd') || ""
    $this.gxbtn('reset')
    let data = {
        url: url,
        id: id,
        pwd: pwd,
    }
    LeaveGameRoom('inviteJoin', data)
})
// 去除重复颜色出生的
function repeatChoose() {
    let hideColor = []
    let hidestartingLocation = []
    newdetailMessage.localUserlist.map((item) => {
        if (item.userId != 'close' && item.userId != 'empty') {
            if (item.startingLocation != -1) {
                hidestartingLocation.push(item.startingLocation.toString())
            }
            if (item.colorId != -1) {
                hideColor.push(item.colorId.toString())
            }
        }
    })
    if (hideColor.length > 0) {
        $('.J_chooseColor').each(function () {
            if ($.inArray($(this).attr('data-code'), hideColor) != -1) {
                $(this).closest('.gx-menu-li').addClass('hide')
            } else {
                $(this).closest('.gx-menu-li').removeClass('hide')
            }
        })
    }
    if (hidestartingLocation.length > 0) {
        $('.J_chooseBirth').each(function () {
            if ($.inArray($(this).attr('data-code'), hidestartingLocation) != -1) {
                $(this).closest('.gx-menu-li').addClass('hide')
            } else {
                $(this).closest('.gx-menu-li').removeClass('hide')
            }
        })
    }
}
var endItemtime = null
// 排序
function userListSort(type) {
    let changetime = false
    if (newdetailMessage.roomType != 0 && newdetailMessage.roomType != 1) {
        let AItem = [], BItem = [], watchItem = [], endItem = []
        let roomType = newdetailMessage.roomType

        let A = newdetailMessage.roomTypeMessage['roomType' + roomType].A,
            B = newdetailMessage.roomTypeMessage['roomType' + roomType].B,
            watch = newdetailMessage.roomTypeMessage['roomType' + roomType].watch
        newdetailMessage.localUserlist.map((item) => {
            if (item.teamId == 0) {
                AItem.push(item)
            } else if (item.teamId == 1) {
                BItem.push(item)
            } else if (item.sideId == 10) {
                watchItem.push(item)
            }
        })
        if (AItem.length > 0) {
            AItem.map((item) => {
                if ($.inArray($('.newdetail-userList-item' + item.userId).index(), A) == -1) {
                    let canChange = false
                    let myitem = false
                    if (item.userId == userInfo.userId) {
                        canChange = true
                        myitem = true
                    }
                    if (item.isAI && newdetailMessage.isHost) {
                        canChange = true
                    }
                    let objone = template('newdetail-userList-script', {
                        type: 'user',
                        message: item,
                        isHost: newdetailMessage.isHost,
                        canChange: canChange,
                        myitem: myitem,
                        optionsCountry: newdetailMessage.optionsCountry,
                        optionsColor: comOption.optionsColor,
                        optionsTeam: comOption.optionsTeam,
                        optionsBirth: newdetailMessage.optionsBirth,
                        gameVersion: newdetailMessage.detailsData.gameVersion,
                    })
                    let fir = 0, end = 1
                    if (newdetailMessage.roomType == 2) {
                        fir = 0
                        end = 0
                    }
                    if (newdetailMessage.roomType == 3) {
                        fir = 0
                        end = 1
                    }
                    if (newdetailMessage.roomType == 4) {
                        fir = 0
                        end = 2
                    }
                    if (newdetailMessage.roomType == 5) {
                        fir = 0
                        end = 3
                    }
                    if (
                        newdetailMessage.changeweiIndex != 0
                        && $('.newdetail-userList-item').eq(newdetailMessage.changeweiIndex).hasClass('newdetail-userList-item-empty')
                        && $.inArray(newdetailMessage.changeweiIndex, A) != -1
                    ) {
                        let objtwo = $('.newdetail-userList-item').eq(newdetailMessage.changeweiIndex).clone()
                        $('.newdetail-userList-item' + item.userId).replaceWith(objtwo)
                        $('.newdetail-userList-item').eq(newdetailMessage.changeweiIndex).replaceWith(objone)
                        newdetailMessage.changeweiIndex = 0
                    } else {
                        let isinite = false
                        for (var i = fir; i <= end; i++) {
                            if ($('.newdetail-userList-item').eq(i).hasClass('newdetail-userList-item-empty')) {
                                isinite = true
                                let objtwo = $('.newdetail-userList-item').eq(i).clone()
                                $('.newdetail-userList-item' + item.userId).replaceWith(objtwo)
                                $('.newdetail-userList-item').eq(i).replaceWith(objone)
                                break
                            }
                        }
                        if (!isinite) {
                            $('.newdetail-userList-item' + item.userId).replaceWith(
                                template('newdetail-userList-script', {
                                    type: 'empty',
                                    isHost: newdetailMessage.isHost,
                                }))
                            endItem.push(1)
                        }
                    }
                    changetime = true
                }
            })
        }
        if (BItem.length > 0) {
            BItem.map((item) => {
                if ($.inArray($('.newdetail-userList-item' + item.userId).index(), B) == -1) {
                    let canChange = false
                    let myitem = false
                    if (item.userId == userInfo.userId) {
                        canChange = true
                        myitem = true
                    }
                    if (item.isAI && newdetailMessage.isHost) {
                        canChange = true
                    }
                    let objone = template('newdetail-userList-script', {
                        type: 'user',
                        message: item,
                        isHost: newdetailMessage.isHost,
                        canChange: canChange,
                        myitem: myitem,
                        optionsCountry: newdetailMessage.optionsCountry,
                        optionsColor: comOption.optionsColor,
                        optionsTeam: comOption.optionsTeam,
                        optionsBirth: newdetailMessage.optionsBirth,
                        gameVersion: newdetailMessage.detailsData.gameVersion,
                    })
                    let fir = 0, end = 1
                    if (newdetailMessage.roomType == 2) {
                        fir = 1
                        end = 1
                    }
                    if (newdetailMessage.roomType == 3) {
                        fir = 2
                        end = 3
                    }
                    if (newdetailMessage.roomType == 4) {
                        fir = 3
                        end = 5
                    }
                    if (newdetailMessage.roomType == 5) {
                        fir = 4
                        end = 7
                    }
                    if (
                        newdetailMessage.changeweiIndex != 0
                        && $('.newdetail-userList-item').eq(newdetailMessage.changeweiIndex).hasClass('newdetail-userList-item-empty')
                        && $.inArray(newdetailMessage.changeweiIndex, B) != -1
                    ) {
                        let objtwo = $('.newdetail-userList-item').eq(newdetailMessage.changeweiIndex).clone()
                        $('.newdetail-userList-item' + item.userId).replaceWith(objtwo)
                        $('.newdetail-userList-item').eq(newdetailMessage.changeweiIndex).replaceWith(objone)
                        newdetailMessage.changeweiIndex = 0
                    } else {
                        let isinite = false
                        for (var i = fir; i <= end; i++) {
                            if ($('.newdetail-userList-item').eq(i).hasClass('newdetail-userList-item-empty')) {
                                isinite = true
                                let objtwo = $('.newdetail-userList-item').eq(i).clone()
                                $('.newdetail-userList-item' + item.userId).replaceWith(objtwo)
                                $('.newdetail-userList-item').eq(i).replaceWith(objone)
                                break
                            }
                        }
                        if (!isinite) {
                            $('.newdetail-userList-item' + item.userId).replaceWith(
                                template('newdetail-userList-script', {
                                    type: 'empty',
                                    isHost: newdetailMessage.isHost,
                                }))
                            endItem.push(1)
                        }
                    }
                    changetime = true
                }
            })
        }
        if (watchItem.length > 0) {
            watchItem.map((item) => {
                if ($.inArray($('.newdetail-userList-item' + item.userId).index(), watch) == -1) {
                    let canChange = false
                    let myitem = false
                    if (item.userId == userInfo.userId) {
                        canChange = true
                        myitem = true
                    }
                    if (item.isAI && newdetailMessage.isHost) {
                        canChange = true
                    }
                    let objone = template('newdetail-userList-script', {
                        type: 'user',
                        message: item,
                        isHost: newdetailMessage.isHost,
                        canChange: canChange,
                        myitem: myitem,
                        optionsCountry: newdetailMessage.optionsCountry,
                        optionsColor: comOption.optionsColor,
                        optionsTeam: comOption.optionsTeam,
                        optionsBirth: newdetailMessage.optionsBirth,
                        gameVersion: newdetailMessage.detailsData.gameVersion,
                    })
                    if (
                        newdetailMessage.changeIndex != 0
                        && $('.newdetail-userList-item').eq(newdetailMessage.changeIndex).hasClass('newdetail-userList-item-empty')
                        && $.inArray(newdetailMessage.changeIndex, watch) != -1
                    ) {
                        let objtwo = $('.newdetail-userList-item').eq(newdetailMessage.changeIndex).clone()
                        $('.newdetail-userList-item' + item.userId).replaceWith(objtwo)
                        $('.newdetail-userList-item').eq(newdetailMessage.changeIndex).replaceWith(objone)
                        newdetailMessage.changeIndex = 0
                    } else {
                        let fir = 0, end = 1
                        if (newdetailMessage.roomType == 2) {
                            fir = 2
                            end = 7
                        }
                        if (newdetailMessage.roomType == 3) {
                            fir = 4
                            end = 7
                        }
                        if (newdetailMessage.roomType == 4) {
                            fir = 6
                            end = 7
                        }
                        let isinite = false
                        for (var i = fir; i <= end; i++) {
                            if ($('.newdetail-userList-item').eq(i).hasClass('newdetail-userList-item-empty') && !$('.newdetail-userList-item').eq(i).hasClass('newdetail-userList-item-close')) {
                                isinite = true
                                let objtwo = $('.newdetail-userList-item').eq(i).clone()
                                $('.newdetail-userList-item' + item.userId).replaceWith(objtwo)
                                $('.newdetail-userList-item').eq(i).replaceWith(objone)
                                break
                            }
                        }
                        if (!isinite) {
                            $('.newdetail-userList-item' + item.userId).replaceWith(
                                template('newdetail-userList-script', {
                                    type: 'empty',
                                    isHost: newdetailMessage.isHost,
                                }))
                            endItem.push(1)
                        }
                    }
                    changetime = true
                }
            })
        }
        if (endItem.length > 0 && type != 'again') {
            clearTimeout(endItemtime)
            endItemtime = setTimeout(() => {
                userListSort('again')
            }, 300)
        }
        $('.J_chooseCountry').each(function () {
            if ($(this).attr('data-code') == 10) {
                $(this).closest('.gx-menu-li').remove()
            }
        })
        addroomTypeText()
        AutoWatchOption('auto')
    }
    if (changetime) {
        $('.newdetail-userList-item.canChange .gx-menu').gxmenu({
            height: "33px",
            top: "27px",
            clickHide: true
        }, function (e) {

        })
    }

}
//切换房间状态cd
var chooseRoomTypeCDTime = 60
var chooseRoomTypeCDTimefun = null
function chooseRoomTypeCD() {
    $('.gx-menu-newdetail-roomtype').addClass('eventsnone')
    $('.gx-menu-newdetail-roomtype-cd span').text(chooseRoomTypeCDTime)
    chooseRoomTypeCDTimefun = setInterval(() => {
        chooseRoomTypeCDTime = chooseRoomTypeCDTime - 1
        $('.gx-menu-newdetail-roomtype-cd span').text(chooseRoomTypeCDTime)
        if (chooseRoomTypeCDTime == 0) {
            clearTimeout(chooseRoomTypeCDTimefun)
            chooseRoomTypeCDTime = 60
            $('.gx-menu-newdetail-roomtype').removeClass('eventsnone')
        }
    }, 1000)
}
// 换位弹窗倒计时
var changeAlertModalnum = 10
var changeAlertModaltimefun = null
function changeAlertModalTime() {
    changeAlertModalnum = 10
    changeAlertModaltimefun = setInterval(() => {
        changeAlertModalnum = changeAlertModalnum - 1
        if (changeAlertModalnum < 0) {
            clearInterval(changeAlertModaltimefun)
            $('#changeAlertModal').modal('hide')
            return false
        }
        $('.changeAlertModal-content-time').text(changeAlertModalnum + 'S')
    }, 1000)
}
// 换位弹窗倒计时关闭
$(document).on('click', '.J_agreeChangewei', function () {
    clearInterval(changeAlertModaltimefun)
    newdetailMessage.changeweiloadingList = []
})
$(document).on('click', '.J_agreeChangeweihide', function () {
    clearInterval(changeAlertModaltimefun)
    newdetailMessage.changeweiloadingList.splice(0, 1)
    if (newdetailMessage.changeweiloadingList.length > 0) {
        dellchangeweiList()
    }
})
// 交互冷却
function changeweiloading() {
    newdetailMessage.changeweiloadingnum = 15
    clearInterval(newdetailMessage.changeweiloading)
    newdetailMessage.changeweiloading = setInterval(() => {
        newdetailMessage.changeweiloadingnum = newdetailMessage.changeweiloadingnum - 1
        $('.changeweiloading-num').text(newdetailMessage.changeweiloadingnum + 'S')
        if (newdetailMessage.changeweiloadingnum == 0) {
            $('.newdetail-userList-item-empty-operation-item').removeClass('operation-item-changeweiloading')
            clearInterval(newdetailMessage.changeweiloading)
        }
    }, 1000)
}

ipcRenderer.on('WebIpc', (event, message) => {
    var res = JSON.parse(message.jsonInfo)
    // if (!newdetailMessage.isgetinit) {
    //     return false
    // }
    if (message.msgType == 10) {
        // 用户列表
        let ismyinit = false
        for (var i = 0; i < res.length; i++) {
            if (res[i].userId == userInfo.userId) {
                ismyinit = true
                newdetailMessage.isReady = res[i].ready
            }
        }
        // 确定有自己
        if (ismyinit) {
            userDellfun(res)
        }
    } else if (message.msgType == 18) {
        let Dom = $('.newdetail-userList-item' + res.userId)
        Dom.find('.newdetail-userList-item-ping-tunnelPing').text(res.tunnelPing + 'ms')
        Dom.find('.newdetail-userList-item-ping-tunnelPkLose').text(res.tunnelPkLose + '%')
        var color = ''
        if (res.tunnelPing >= 200) {
            color = '#FF4444'
        } else if (res.tunnelPing >= 100) {
            color = '#FFA333'
        } else {
            color = '#A2FF33'
        }
        if (res.tunnelPkLose > 10) {
            color = '#FF4444'
        }
        Dom.find('.newdetail-userList-item-ping svg').css('fill', color)
        Dom.find('.newdetail-userList-item-ping-div').css('color', color)

    } else if (message.msgType == 15) {
        newdetailMessage.isget15 = true
        // 设置回调
        // if (!newdetailMessage.isHost) {
        res.map((item) => {
            if (item.key == 'MaxPlayers') {
                if (item.value != newdetailMessage.detailsData.maxPlayers) {
                    userGetChangeMax(item.value)
                }
            }
            if (item.key == 'GameVersion') {
                if (newdetailMessage.detailsData.gameVersion != item.value) {
                    newdetailMessage.detailsData.gameVersion = item.value
                    updataGameVersion()
                }
            }
        })
        for (var i = 0; i < newdetailMessage.detailsData.gameOption.length; i++) {
            res.map((item) => {
                if (newdetailMessage.detailsData.gameOption[i].key == item.key) {
                    newdetailMessage.detailsData.gameOption[i].value = item.value
                    newdetailMessage.detailsData.gameOption[i].must = item.must
                }
            })
        }
        // saveMessage('gameOption', { roomNo: newdetailMessage.detailsData.roomNo, gameOption: newdetailMessage.detailsData.gameOption })
        updataGameseting()
        // }
    } else if (message.msgType == 20) {
        if (newdetailMessage.mapData && newdetailMessage.mapData.mapSha1 == res.sign) {
            downloadMap(parseInt(res.nowPercentage))
        }
    } else if (message.msgType == 22) {
        if (res.TypeCode == 10 && res.SenderId != newdetailMessage.hostId) {
            return false
        }
        if (res.TypeCode == 5) {
            // 公告
            let isempty = false
            if (res.Message == '空') {
                $('.newdetail-chatDiv-header').html(template('newdetail-chatDiv-header-script', {
                    isshow: false,
                    message: '',
                    isHost: newdetailMessage.isHost
                }))
                isempty = true
            } else {
                $('.newdetail-chatDiv-header').html(template('newdetail-chatDiv-header-script', {
                    isshow: true,
                    message: res.Message,
                    isHost: newdetailMessage.isHost
                }))
            }
            if (isempty) {
                $('.newdetail-chatDiv').addClass('emptyMessage')
            } else {
                $('.newdetail-chatDiv').removeClass('emptyMessage')
            }
            $('.newdetail-chatDiv-scroll').scrollTop($('.newdetail-chatDiv-scroll').prop('scrollHeight'))
            saveMessage('roomnotice', { message: res.Message, roomNo: newdetailMessage.detailsData.roomNo })
        }
        if (res.TypeCode == 8) {
            addChatMessage(res.SenderName + translatesrting('丢出一面骰子(0 - 100)'), res.Message, res.SenderId)
        }
        if (res.TypeCode == 1 || res.TypeCode == 10) {
            // 聊天
            addChatMessage(res.SenderName, res.Message, res.SenderId)
            return false
        }
        if (res.TypeCode == 3 && res.SenderId == userInfo.userId && newdetailMessage.isHost) {
            // 开始游戏失败
            wsUserStatusdetails(7)
            player_sound_openfail()
            gxmodal({
                title: translatesrting('提示'),
                centent: '<div class="mt25" style="padding-bottom: 20px;">' + res.Message + '</div>',
                buttons: [{
                    text: translatesrting('确定'),
                    class: 'btn-middle btn-middle-blue',
                }]
            })
            if ($('.J-newdetail-autoOpen').is(':checked')) {
                $('.J-newdetail-autoOpen').click()
            }
        }
        if (res.TypeCode == 3 && !newdetailMessage.isHost) {
            addChatMessage(res.SenderName, res.Message, res.SenderId)
        }
        if (res.TypeCode == 7) {
            checkWatch()
        }
        if (res.TypeCode == 7 && !newdetailMessage.isHost) {
            // 访客开启游戏
            parent.startModal()
            wsUserStatusdetails(8)
            AutoClose('close')
            return false
        }
        if (res.TypeCode == 6) {
            addChatMessage(res.SenderName, res.Message, true)
        }
    } else if (message.msgType == 24) {
        newdetailMessage.detailsData.startingLocations = res.startingLocations
        newdetailMessage.optionsBirth = res.startingLocations

        newdetailMessage.detailsData.imageUrl = res.imageUrl
        //出生点选项重置
        $('.newdetail-userList-item.canChange').each(function () {
            let userId = $(this).attr('data-id')
            newdetailMessage.localUserlist.map((item) => {
                if (item.userId == userId) {
                    $(this).find('.newdetail-userList-item-menu-birth').html(
                        template('newdetail-useritem-change-script', {
                            type: 'birth',
                            message: item,
                            optionsBirth: newdetailMessage.optionsBirth,
                        })
                    )
                    $(this).find('.newdetail-userList-item-menu-birth').gxmenu({
                        height: '33px',
                        top: "27px",
                        clickHide: true
                    }, function (e) {

                    })
                }
            })
        })
        if (newdetailMessage.mapData) {
            if (
                newdetailMessage.detailsData.startingLocations.length != 0 &&
                newdetailMessage.mapData.mapSha1 == res.mapSha1 &&
                newdetailMessage.detailsData.mode == res.mapType
            ) {
            } else {
                updataMap(res.mapSha1)
            }
        }
        newdetailMessage.detailsData.mode = res.mapType
        if (res.mapSha1 != newdetailMessage.mapData.mapSha1) {
            $('.newdetail-userList-item.canChange').each(function () {
                let userId = $(this).attr('data-id')
                newdetailMessage.localUserlist.map((item) => {
                    if (item.userId == userId && item.startingLocation > (newdetailMessage.optionsBirth.length - 1)) {
                        $(this).find('.J_chooseBirth[data-code="-1"]').click()
                    }
                })
            })
        }
    } else if (message.msgType == 26) {
        // 游戏结束
        saveMessage('ClientBGM', 'false')
        wsUserStatusdetails(7)
        AutoClose()
        if (!newdetailMessage.isHost) {
            newdetailMessage.isReady = false
            roomStartBtn()
        }
    } else if (message.msgType == 30) {
        // 数据修改矫正
        let getuser = []
        getuser.push(res)
        userDellfun(getuser, 'error')
    } else if (message.msgType == 40) {
        newdetailMessage.roomType = res
        $('.newdetail-userList').attr('class', 'newdetail-userList roomType' + newdetailMessage.roomType)
        $('.newdetail-row-left').attr('class', 'newdetail-row-left roomType-left' + newdetailMessage.roomType)
        comOption.roomTypeArray.map((item) => {
            if (newdetailMessage.roomType == item.code) {
                $('.newdetail-header-roomtype').text(translatesrting(item.cname))
            }
        })
        AutoWatchOption('init')
    } else if (message.msgType == 41) {
        // 收交换邀请
        if (localStorage.getItem('battle_switch_user_transposition') && localStorage.getItem('battle_switch_user_transposition') == 1) {
            return false
        }
        let usermessageID = res.sourcePlayer.userId
        let usermessage = null
        if (localStorage.getItem('Ra_BlackFriends')) {
            let init = false
            let blackUserList = JSON.parse(localStorage.getItem('Ra_BlackFriends'))
            blackUserList.map((item) => {
                if (item.userId == usermessageID) {
                    init = true
                }
            })
            if (init) {
                return false
            }
        }
        newdetailMessage.localUserlist.map((item) => {
            if (item.userId == usermessageID) {
                usermessage = item
            }
        })
        usermessage.colorId = res.sourcePlayer.colorId
        usermessage.sideId = res.sourcePlayer.sideId
        usermessage.startingLocation = res.sourcePlayer.startingLocation
        usermessage.teamId = res.sourcePlayer.teamId
        usermessage.customField = customFieldFun('decode', usermessage.customField)
        usermessage.startingLocation = Number(usermessage.startingLocation)
        newdetailMessage.optionsCountry.map((optionC) => {
            if (usermessage.sideId == optionC.sideId) {
                usermessage.JsCountry = optionC
            }
        })
        comOption.optionsColor.map((optionC) => {
            if (usermessage.colorId == optionC.colorId) {
                usermessage.JsColor = optionC
            }
        })
        comOption.optionsTeam.map((optionC) => {
            if (usermessage.teamId == optionC.teamId) {
                usermessage.JsTeam = optionC
            }
        })
        newdetailMessage.changeweiloadingList.push(usermessage)
        if (newdetailMessage.changeweiloadingList.length == 1) {
            dellchangeweiList()
        }
    }
})
// 处理交换队列
function dellchangeweiList() {
    let usermessage = newdetailMessage.changeweiloadingList[0]
    $('#changeAlertModal .gx-modal-smell').html(template('changeAlertModal-content-script', { message: usermessage }))
    $('#changeAlertModal').modal('show')
    changeAlertModalTime()
}


// A对 B对 观察者 文案
function addroomTypeText() {
    if (newdetailMessage.roomType != 1 && newdetailMessage.roomType != 0) {
        $('.newdetail-userList-item').attr('data-content-after', '')
        let roomType = newdetailMessage.roomType
        let A = newdetailMessage.roomTypeMessage['roomType' + roomType].A,
            B = newdetailMessage.roomTypeMessage['roomType' + roomType].B,
            watch = newdetailMessage.roomTypeMessage['roomType' + roomType].watch
        if (A.length > 0) {
            A.map((item) => {
                $('.newdetail-userList-item').eq(item).attr('data-content-after', translatesrting('队伍') + ' A')
            })
        }
        if (B.length > 0) {
            B.map((item) => {
                $('.newdetail-userList-item').eq(item).attr('data-content-after', translatesrting('队伍') + ' B')
            })
        }
        if (watch.length > 0) {
            watch.map((item) => {
                $('.newdetail-userList-item').eq(item).attr('data-content-after', translatesrting('观察者'))
            })
        }
    }
}
// 自动准备
function timeCheckautoopenready() {
    setInterval(function () {
        if (newdetailMessage.autoopenready && newdetailMessage.downloadMapEnd) {
            if ($('.J_getready').length > 0) {
                $('.J_getready').click()
            }
        }
    }, 1000)
}
// 观察者插件自动配置
function AutoWatchOption(type) {
    if ($('#BatterModal').hasClass('in')) {
        return false
    }
    if (newdetailMessage.roomType != 0 && newdetailMessage.roomType != 1) {
        if (getSaveMessage('batterStorage')) {
            let oldbatterStorage = getSaveMessage('batterStorage')
            if (oldbatterStorage.roomNo == newdetailMessage.detailsData.roomNo) {
                batterMessage.localstorage = oldbatterStorage
            }
        }
        if (type == 'init') {
            if (newdetailMessage.roomType != 0 && newdetailMessage.roomType != 1) {
                if (!userinJ_BatterModalopen) {
                    //是否进行过操作
                    batterMessage.localstorage.isopen = true
                }
                batterMessage.localstorage.teamList[0].teamId = 0
                batterMessage.localstorage.teamList[0].name = 'A队'
                batterMessage.localstorage.teamList[1].teamId = 1
                batterMessage.localstorage.teamList[1].name = 'B队'
                if (newdetailMessage.roomType == 2) {
                    batterMessage.localstorage.modal = 1
                } else if (newdetailMessage.roomType == 3) {
                    batterMessage.localstorage.modal = 2
                } else if (newdetailMessage.roomType == 4) {
                    batterMessage.localstorage.modal = 3
                }
                $('#BatterModal').addClass('BatterModalMust')
            } else {
                batterMessage.localstorage.isopen = false

            }
        } else if (type == 'auto') {
            if (newdetailMessage.roomType == 2) {
                batterMessage.localstorage.modal = 1
            } else if (newdetailMessage.roomType == 3) {
                batterMessage.localstorage.modal = 2
            } else if (newdetailMessage.roomType == 4) {
                batterMessage.localstorage.modal = 3
            }
            if (newdetailMessage.roomType == 2) {
                let ina = false
                let inb = false
                for (localItem of newdetailMessage.localUserlist) {
                    if (localItem.teamId == 0) {
                        batterMessage.localstorage.userList[0].userId = localItem.userId
                        batterMessage.localstorage.userList[0].usermessage = localItem
                        ina = true
                    }
                    if (localItem.teamId == 1) {
                        batterMessage.localstorage.userList[1].userId = localItem.userId
                        batterMessage.localstorage.userList[1].usermessage = localItem
                        inb = true
                    }
                }
                if (!ina) {
                    batterMessage.localstorage.userList[0].userId = ''
                    batterMessage.localstorage.userList[0].usermessage = []
                }
                if (!inb) {
                    batterMessage.localstorage.userList[1].userId = ''
                    batterMessage.localstorage.userList[1].usermessage = []
                }
            }
        }
        saveMessage('batterStorage', batterMessage.localstorage)
    }
}


// 分享
var shareMessage = {
    copyTime: null
}
function shareMessageInit() {
    let password = ''
    if (newdetailMessage.detailsData.isHavePas) {
        password = newdetailMessage.detailsData.password
    }
    $.controlAjax({
        type: "POST",
        url: '/api/details/shareroom ',
        data: {
            gameRoomNo: newdetailMessage.detailsData.roomNo,
            password
        },
        showerror: true,
        success: function (res) {
            let showmessage = translatesrting('将链接发给你的好友、QQ或微信群，邀请他们直接加入房间') + ' ' + res.data.shortLink
            let copymessage = res.data.msg
            if (newdetailMessage.detailsData.roomName.indexOf('的房间') == -1) {
                copymessage = copymessage + ',' + newdetailMessage.detailsData.roomName + '——' + newdetailMessage.mapData.name
            } else {
                copymessage = copymessage + ',' + newdetailMessage.mapData.name
            }
            copymessage = copymessage + ':' + res.data.shortLink
            $('#detailsShareModal .gx-modal-smell').html(template('detailsShareModal-content-script', {
                showmessage,
                copymessage
            }))
            $('#detailsShareModal').modal('show')
        },
        error: function (req) {

        }
    })

}
$(document).on('click', '#newdetail-header-share', function () {
    clearTimeout(shareMessage.copyTime)
    shareMessageInit()
})
$(document).on('shown.bs.modal', '#detailsShareModal', function () {
    let clipboard = new Clipboard('#J_detailsShareModal_copy');
})
$(document).on('click', '.detailsShareModal-content-textarea', function () {
    var $this = $(this)
    $this.select()
})
$(document).on('click', '#J_detailsShareModal_copy', function () {
    $(this).addClass('hide')
    $('#J_detailsShareModal_copy-loading').removeClass('hide')
    shareMessage.copyTime = setTimeout(() => {
        $(this).removeClass('hide')
        $('#J_detailsShareModal_copy-loading').addClass('hide')
    }, 5000)

})

// 分享
// 黑名单
function chooseBlackUser() {
    if (localStorage.getItem('Ra_BlackFriends')) {
        let blackUserList = JSON.parse(localStorage.getItem('Ra_BlackFriends'))
        blackUserList.map((item) => {
            let blackItem = $('.newdetail-userList-item' + item.userId)
            if (blackItem.find('.newdetail-userList-item-blackbot').hasClass('hide')) {
                if (blackItem.find('.newdetail-userList-item-userbot').length > 0) {
                    blackItem.find('.newdetail-userList-item-blackbot-border').removeClass('hide')
                }
                blackItem.find('.newdetail-userList-item-blackbot').removeClass('hide').tooltip()
            }
        })

    }

}