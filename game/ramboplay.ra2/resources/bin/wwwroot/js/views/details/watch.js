var batterMessage = {
    init: null,//初始化
    userfun: null,
    showModal: [
        { name: 'BO「展示BO几」', type: 1 },
        { name: '数字比分', type: 2 },
        { name: '不显示', type: 3 },
    ],
    localstorage: {
        roomNo: null,
        isopen: false,
        showModaltype: 3,
        userList: [
            { userId: null, fraction: '', usermessage: '' },
            { userId: null, fraction: '', usermessage: '' },
        ],
        teamList: [
            { teamId: null, name: '', fraction: '', userList: [] },
            { teamId: null, name: '', fraction: '', userList: [] },
        ],
        Bonumber: '',
        modal: 1,//1对手 2队伍
        version: null,
        Substitute: []
    },
}



// 观战
batterMessage.init = function () {
    if (batterMessage.localstorage.modal == 1) {
        batterMessage.localstorage.userList.map((inuser, index) => {
            let init = false
            newdetailMessage.localUserlist.map((item) => {
                if (item.userId == inuser.userId) {
                    init = true
                }
            })
            if (!init) {
                batterMessage.localstorage.userList[index] = { userId: null, fraction: '', usermessage: '' }
            }
        })
    }
    $('#BatterModal').find('.modal-dialog').html(template('BatterModal-script', {
        isopen: batterMessage.localstorage.isopen,
        showModaltype: batterMessage.localstorage.showModaltype,
        Bonumber: batterMessage.localstorage.Bonumber,
        userList: batterMessage.localstorage.userList,
        teamList: batterMessage.localstorage.teamList,
        modal: batterMessage.localstorage.modal,
        showModal: batterMessage.showModal,
    }))
    $('#BatterModal [data-toggle="tooltip"]').tooltip()
    $('#BatterModal .gx-menu').gxmenu({
        height: "30px",
        top: '20px',
        clickHide: true,
    }, function (e) {

    })
    if (batterMessage.localstorage.modal != 4) {
        $('#BatterModal').removeClass('BatterModal4')
    } else {
        $('#BatterModal').addClass('BatterModal4')
    }
    $('#BatterModal').modal('show')
}
batterMessage.userfun = function (obj) {
    if (batterMessage.localstorage.modal == 1) {
        let selectUser = []
        newdetailMessage.localUserlist.map((item) => {
            if (item.userId != 'empty' && !item.isAI && item.sideId != 10 && item.userId != userInfo.userId) {
                selectUser.push(item)
            }
        })
        let selectUserold = []
        for (var i = 0; i < selectUser.length; i++) {
            var init = false
            for (var z = 0; z < batterMessage.localstorage.userList.length; z++) {
                if (batterMessage.localstorage.userList[z].userId == selectUser[i].userId) {
                    init = true
                }
            }
            if (!init) {
                selectUserold.push(selectUser[i])
            }
        }

        $('.BatterModal-gxmenu-li').html(template('BatterModal-gxmenu-li-script', { userlist: selectUserold }))
        $('#BatterModal .gx-menu').gxmenu({
            height: "30px",
            top: '20px',
            clickHide: true,
        }, function (e) {

        })
        obj.click()
    } else {
        let selectUser = []
        newdetailMessage.localUserlist.map((item) => {
            if (item.userId != 'empty' && !item.isAI && item.sideId != 10 && item.userId != userInfo.userId) {
                selectUser.push(item)
            }
        })
        let teamarray = []
        for (var i = 0; i < selectUser.length; i++) {
            for (var z = 0; z < comOption.optionsTeam.length; z++) {
                if (selectUser[i].teamId == comOption.optionsTeam[z].teamId) {
                    let init = false
                    for (var y = 0; y < teamarray.length; y++) {
                        if (teamarray[y].name == comOption.optionsTeam[z].name) {
                            init = true
                        }
                    }
                    if (!init) {
                        teamarray.push({ name: comOption.optionsTeam[z].name, teamId: comOption.optionsTeam[z].teamId })
                    }

                }
            }
        }
        let teamarrayold = []
        for (var i = 0; i < teamarray.length; i++) {
            var init = false
            for (var z = 0; z < batterMessage.localstorage.teamList.length; z++) {
                if (batterMessage.localstorage.teamList[z].teamId == teamarray[i].teamId) {
                    init = true
                }
            }
            if (!init) {
                teamarrayold.push(teamarray[i])
            }
        }
        for (var i = 0; i < teamarrayold.length; i++) {
            if (teamarrayold[i].teamId == -1) {
                teamarrayold.splice(i, 1)
            }
        }
        $('.BatterModal-gxmenu-li').html(template('BatterModal-gxmenu-li-script', { teamarray: teamarrayold, type: 'team' }))
        $('#BatterModal .gx-menu').gxmenu({
            height: "30px",
            top: '20px',
            clickHide: true,
        }, function (e) {

        })
        obj.click()
    }
}

// 观战启动开关
var userinJ_BatterModalopen = false
$(document).on('change', ".J_BatterModalopen", function () {
    var $this = $(this)
    userinJ_BatterModalopen = true
    if ($this.is(':checked')) {
        batterMessage.localstorage.isopen = true
    } else {
        batterMessage.localstorage.isopen = false
    }
})
// 选展示模式
$(document).on('click', ".J_BatterModal_modal", function () {
    var $this = $(this)
    let modaltype = $this.attr('data-type')
    batterMessage.localstorage.modal = modaltype
    if (modaltype != 4) {
        $this.closest('.BatterModal-gxmenu').find('.BatterModal-gxmenu-text').html(
            '<span>' + modaltype + '</span><span> VS </span><span>' + modaltype + '</span>'
        )
        $('#BatterModal').removeClass('BatterModal4')
    } else {
        $('.J_BatterModal_showmodal[data-type=3]').click()
        $this.closest('.BatterModal-gxmenu').find('.BatterModal-gxmenu-text').html(
            '<span>' + translatesrting('混战') + '</span>'
        )
        $('#BatterModal').addClass('BatterModal4')
    }
    if (batterMessage.localstorage.modal == 1) {
        $('.player_1 .BatterModal-gxmenu').find('.BatterModal-gxmenu-text').text(translatesrting("选择队员"))
        $('.player_2 .BatterModal-gxmenu').find('.BatterModal-gxmenu-text').text(translatesrting("选择队员"))
    } else {
        $('.player_1 .BatterModal-gxmenu').find('.BatterModal-gxmenu-text').text(translatesrting("选择队伍"))
        $('.player_2 .BatterModal-gxmenu').find('.BatterModal-gxmenu-text').text(translatesrting("选择队伍"))
    }
    $('.J_BatterModal-playnum').val('')
    batterMessage.localstorage.userList = [
        { userId: null, fraction: '', usermessage: '' },
        { userId: null, fraction: '', usermessage: '' },
    ]
    batterMessage.localstorage.teamList = [
        { teamId: null, name: '', fraction: '', userList: [] },
        { teamId: null, name: '', fraction: '', userList: [] },
    ]
})
// 选择比分展示模式
$(document).on('click', ".J_BatterModal_showmodal", function () {
    var $this = $(this)
    let showtype = $this.attr('data-type')
    batterMessage.localstorage.showModaltype = showtype
    $this.closest('.BatterModal-gxmenu').find('.BatterModal-gxmenu-text').text($this.text())
    if (showtype == 1) {
        $this.closest('.BatterModal-p').find('.BatterModal-numinput').removeClass('hide')
        $this.closest('.BatterModal-p').find('.BatterModal-numinput').focus()
        $('.J_BatterModal-playnum').each(function () {
            if (Number($(this).val()) > 13) {
                $(this).val(13).trigger('input');
            }
        })
    } else {
        $this.closest('.BatterModal-p').find('.BatterModal-numinput').addClass('hide')
    }
})
// 填入bo数
$(document).on('input', ".J_BatterModal_Bonumber", function () {
    var $this = $(this)
    let num = $this.val()
    num = num.replace(/[^\d]/g, '')
    if (Number(num) > 25) {
        num = 25
    }
    $this.val(num)
    batterMessage.localstorage.Bonumber = num

})
// 保存
$(document).on('click', '#BatterModal .J_submit', function () {
    if (batterMessage.localstorage.isopen) {
        if (batterMessage.localstorage.showModaltype == 1 && batterMessage.localstorage.Bonumber == '') {
            showtoast({
                message: translatesrting('请填写BO场次')
            })
            return false
        }
        if (batterMessage.localstorage.modal == 1 && (batterMessage.localstorage.userList[0].userId == null || batterMessage.localstorage.userList[1].userId == null)) {
            showtoast({
                message: translatesrting('请填写选手')
            })
            return false
        }
        if ((batterMessage.localstorage.modal == 2 || batterMessage.localstorage.modal == 3) && (batterMessage.localstorage.teamList[0].teamId == null || batterMessage.localstorage.teamList[1].teamId == null)) {
            showtoast({
                message: translatesrting('请填写队伍')
            })
            return false
        }
        if (batterMessage.localstorage.showModaltype == 1) {
            if (batterMessage.localstorage.Bonumber % 2 == 0) {
                showtoast({
                    message: translatesrting('BO请填写奇数')
                })
                return false
            }
        }
    }
    batterMessage.localstorage.roomNo = newdetailMessage.detailsData.roomNo
    batterMessage.localstorage.mapName = newdetailMessage.mapData ? formatMapName(newdetailMessage.mapData.name) : ''
    batterMessage.localstorage.version = newdetailMessage.detailsData.gameVersion
    saveMessage('batterStorage', batterMessage.localstorage)
    $('#BatterModal').modal('hide')
    let takeopen = false
    if (batterMessage.localstorage.isopen) {
        takeopen = true
    }
    $.controlAjax({
        type: "POST",
        url: '/api/site/spectator/plugin',
        data: {
            open: takeopen
        },
        success: function (res) {

        },
        error: function (req) {

        }
    })
    if (takeopen) {
        setoverwindow()
    }
})

// 取消
$(document).on('click', '#BatterModal .J_hideModel', function () {
    $('#BatterModal').modal('hide')
})

// 获取用户
$(document).on('click', '.BatterModal-gxmenu .BatterModal-gxmenu-click', function () {
    var obj = $(this).closest('.BatterModal-gxmenu').find('.gx-menu-in')
    batterMessage.userfun(obj)
})
// 选择角色
$(document).on('click', '.J_BatterModal-chooseuser', function () {
    var $this = $(this);
    var text = $this.text();
    $this.closest('.BatterModal-gxmenu').find('.BatterModal-gxmenu-text').text(text)
    let id = $this.attr('data-id')
    let type = $this.closest('.J_gxmenu-li').attr('data-type')
    newdetailMessage.localUserlist.map((item) => {
        if (id == item.userId) {
            batterMessage.localstorage.userList[type].userId = item.userId
            batterMessage.localstorage.userList[type].usermessage = item
        }
    })
})
// 选择队伍
$(document).on('click', '.J_BatterModal-chooseteam', function () {
    var $this = $(this);
    var text = $this.text();
    $this.closest('.BatterModal-gxmenu').find('.BatterModal-gxmenu-text').text(text)
    let id = $this.attr('data-id')
    let type = $this.closest('.J_gxmenu-li').attr('data-type')
    batterMessage.localstorage.teamList[type].teamId = id
    batterMessage.localstorage.teamList[type].name = text
    newdetailMessage.localUserlist.map((item) => {
        if (id == item.teamId) {
            batterMessage.localstorage.teamList[type].userList.push(item)
        }
    })
})

// 填写比分
$(document).on('input', ".J_BatterModal-playnum", function () {
    var $this = $(this)
    let num = $this.val()
    num = num.replace(/[^\d]/g, '')
    if (batterMessage.localstorage.showModaltype == 1 && num > 13) {
        num = 13
    }
    $this.val(num)
    let type = $this.attr('data-type')
    if (batterMessage.localstorage.modal == 2 || batterMessage.localstorage.modal == 3) {
        batterMessage.localstorage.teamList[type].fraction = num
    } else {
        batterMessage.localstorage.userList[type].fraction = num
    }
})
function checkWatch(type) {
    if (getSaveMessage('batterStorage')) {
        let oldbatterStorage = getSaveMessage('batterStorage')
        if (oldbatterStorage.roomNo == newdetailMessage.detailsData.roomNo) {
            batterMessage.localstorage = oldbatterStorage
        }
    }
    batterMessage.localstorage.Substitute = []
    if (batterMessage.localstorage.isopen) {
        var canopen = true
        if (batterMessage.localstorage.modal == 2) {
            // 队伍
            for (var i = 0; i < batterMessage.localstorage.teamList.length; i++) {
                batterMessage.localstorage.teamList[i].userList = []
                newdetailMessage.localUserlist.map((item) => {
                    if (item.teamId == batterMessage.localstorage.teamList[i].teamId) {
                        batterMessage.localstorage.teamList[i].userList.push(item)
                    }
                })
            }
            for (var i = 0; i < batterMessage.localstorage.teamList.length; i++) {
                if (batterMessage.localstorage.teamList[i].userList.length <= 1 || batterMessage.localstorage.teamList[i].userList.length > 2) {
                    canopen = false
                }
            }
        } else if (batterMessage.localstorage.modal == 3) {
            // 队伍
            for (var i = 0; i < batterMessage.localstorage.teamList.length; i++) {
                batterMessage.localstorage.teamList[i].userList = []
                newdetailMessage.localUserlist.map((item) => {
                    if (item.teamId == batterMessage.localstorage.teamList[i].teamId) {
                        batterMessage.localstorage.teamList[i].userList.push(item)
                    }
                })
            }
            for (var i = 0; i < batterMessage.localstorage.teamList.length; i++) {
                if (batterMessage.localstorage.teamList[i].userList.length != 3) {
                    canopen = false
                }
            }
        } else if (batterMessage.localstorage.modal == 4) {
            for (var i = 0; i < batterMessage.localstorage.teamList.length; i++) {
                batterMessage.localstorage.teamList[i].userList = []
            }
            let userlocationlist = []
            for (var z = 0; z < newdetailMessage.localUserlist.length; z++) {
                if (
                    newdetailMessage.localUserlist[z].userId != 'close'
                    && newdetailMessage.localUserlist[z].userId != 'empty'
                    && newdetailMessage.localUserlist[z].sideId != 10
                ) {
                    userlocationlist.push(newdetailMessage.localUserlist[z])
                }
            }
            for (var z = 0; z < userlocationlist.length; z++) {
                if (z % 2 == 0) {
                    if (batterMessage.localstorage.teamList[0].userList.length >= 3) {
                        batterMessage.localstorage.Substitute.push(userlocationlist[z])
                    } else {
                        batterMessage.localstorage.teamList[0].userList.push(userlocationlist[z])
                    }
                } else {
                    if (batterMessage.localstorage.teamList[1].userList.length >= 3) {
                        batterMessage.localstorage.Substitute.push(userlocationlist[z])
                    } else {
                        batterMessage.localstorage.teamList[1].userList.push(userlocationlist[z])
                    }
                }
            }
            if (userlocationlist.length < 2) {
                canopen = false
            }
            if (userlocationlist.length == 2) {
                batterMessage.localstorage.userList = [
                    { userId: userlocationlist[0].userId, fraction: '', usermessage: userlocationlist[0] },
                    { userId: userlocationlist[1].userId, fraction: '', usermessage: userlocationlist[1] }
                ]
            }
        } else if (batterMessage.localstorage.modal == 1) {
            // 人
            for (var i = 0; i < batterMessage.localstorage.userList.length; i++) {
                let init = false
                newdetailMessage.localUserlist.map((item) => {
                    if (item.userId == batterMessage.localstorage.userList[i].userId && item.sideId != 10) {
                        init = true
                    }
                })
                if (!init) {
                    canopen = false
                }
            }
        }
        let init = false
        newdetailMessage.localUserlist.map((item) => {
            if (item.isAI) {
                init = true
            }
        })
        if (init) {
            canopen = false
        }
        newdetailMessage.localUserlist.map((item) => {
            if (item.userId == userInfo.userId && item.sideId != 10) {
                canopen = false
            }
        })
        $.controlAjax({
            type: "POST",
            url: '/api/site/spectator/plugin',
            data: {
                open: canopen
            },
            success: function (res) {

            },
            error: function (req) {

            }
        })
        if (canopen) {
            setoverwindow()
        }
    } else {
        $.controlAjax({
            type: "POST",
            url: '/api/site/spectator/plugin',
            data: {
                open: false
            },
            success: function (res) {

            },
            error: function (req) {

            }
        })
    }
    batterMessage.localstorage.roomNo = newdetailMessage.detailsData.roomNo
    batterMessage.localstorage.mapName = newdetailMessage.mapData ? formatMapName(newdetailMessage.mapData.name) : ''
    batterMessage.localstorage.version = newdetailMessage.detailsData.gameVersion
    saveMessage('batterStorage', batterMessage.localstorage)
}
function setoverwindow() {
    $.controlAjax({
        type: "get",
        url: '/api/site/settings/info/',
        success: function (ress) {
            let windowMode = ''
            if (ress.data.windowMode == 'FullScreen') {
                windowMode = 'WindowFullScreen'
            }
            if (ress.data.windowMode == 'Window') {
                windowMode = 'BorderlessWindow'
            }
            if (ress.data.windowMode == 'Window' || ress.data.windowMode == 'FullScreen') {
                let sendarray = {
                    gamePath: ress.data.GamePath,
                    gameScreen: ress.data.gameScreen,
                    windowMode: windowMode,
                    renderMode: ress.data.renderMode,
                    singleCpu: ress.data.singleCpu,
                    scrollRate: ress.data.scrollRate,
                    skipScoreScreen: ress.data.skipScoreScreen,
                    tooltips: ress.data.tooltips,
                    showHiddenObjects: ress.data.showHiddenObjects,
                    targetLines: ress.data.targetLines,
                }
                $.controlAjax({
                    type: "POST",
                    url: '/api/site/game/personalize',
                    showerror: false,
                    data: sendarray,
                    success: function (res) {

                    },
                    error: function (req) {

                    }
                })
            }
        },
        error: function (req) {

        }
    })
}