// 翻译
$('.detail-container').html(template('detail-container-script'))
$('#modalChangeMap').html(template('modalChangeMap-script'))
$('.details-all-close-text').text(translatesrting('离开/解散'))
$('#modalInvite').html(template('modalInvite-script'))
var detailsMessage = {
    roomNo: null, //房间号
    HostId: 0, //房主ID 用于聊天判断
    maxPlayers: 0, //房间最大人数
    mapMaxPlayers: 8, //地图最大人数
    gameOption: [], //游戏设置
    isHost: false, //是否房主
    isReady: false, //是否准备
    optionsCountry: [], //国家配置
    optionsBirth: [], //出生点
    localUserlist: [], //本地维护的用户信息
    startTime: 5, //开始倒计时
    startTimefun: null, //开始倒计时
    gameVersion: 0,//游戏版本
    gameVersionMust: false,
    probabilityChart: null,
    isHavePas: null,
    roomGold: 0,
    mapMove: false,
    pwd: null,
    initeErrorNum: 3,
    hostStartGameCountfun: null,
    hostStartGameCountnum: 5,
    autoopen: true,//10S保护机制
    autoopencheck: getSaveMessage('roomAutoOpen') ? getSaveMessage('roomAutoOpen').check : true,//10S保护机制
    autoopentime: 10,//10S保护机制
    autoopenfun: null,//10S保护机制
    gameautoOpen: true
}

var batterMessage = {
    init: null,//初始化
    userfun: null,
    showModal: [
        { name: 'BO「展示BO几」', type: 1 },
        { name: '数字比分', type: 2 },
        { name: '不显示', type: 3 },
    ],
    localstorage: {
        roomNo: null,
        isopen: false,
        showModaltype: 3,
        userList: [
            { userId: null, fraction: '', usermessage: '' },
            { userId: null, fraction: '', usermessage: '' },
        ],
        teamList: [
            { teamId: null, name: '', fraction: '', userList: [] },
            { teamId: null, name: '', fraction: '', userList: [] },
        ],
        Bonumber: '',
        modal: 1,//1对手 2队伍
        version: null
    },
}
ipcRenderer.on('WebIpc', (event, message) => {
    var res = JSON.parse(message.jsonInfo)
    if (message.msgType == 10) {
        // 用户列表
        let ismyinit = false
        for (var i = 0; i < res.length; i++) {
            if (res[i].userId == userInfo.userId) {
                ismyinit = true
            }
        }
        if (ismyinit) {
            ipcUser(res, 'update')
        }
    } else if (message.msgType == 18) {
        // ping值
        var res = JSON.parse(message.jsonInfo)
        if (res.userId != 0) {
            $('.detail-user-list-item').each(function () {
                var id = $(this).attr('data-id')
                if (id == res.userId) {
                    $(this).find('.details-ping-iconfont').removeClass('hide')
                    $(this).find('.detail-ping-hover-tunnelPing-span').text(res.tunnelPing + 'ms')
                    $(this).find('.detail-ping-hover-tunnelPkLose-span').text(res.tunnelPkLose + '%')
                    if (res.tunnelPing >= 200) {
                        $(this).find('.details-ping').css('color', '#FF4444')
                        $(this).find('.details-ping-svg').css('fill', '#FF4444')
                    } else if (res.tunnelPing >= 100) {
                        $(this).find('.details-ping').css('color', '#FFA333')
                        $(this).find('.details-ping-svg').css('fill', '#FFA333')
                    } else {
                        $(this).find('.details-ping').css('color', '#A2FF33')
                        $(this).find('.details-ping-svg').css('fill', '#A2FF33')
                    }
                    if (res.tunnelPkLose > 10) {
                        $(this).find('.details-ping').css('color', '#FF4444')
                        $(this).find('.details-ping-svg').css('fill', '#FF4444')
                    }
                }
            })
            detailsMessage.localUserlist.map((item) => {
                if (item.userId == res.userId) {
                    item.ping = res.tunnelPing
                    item.packageLose = res.tunnelPkLose
                }
            })
        }
    } else if (message.msgType == 15) {
        // 修改房间配置通知
        var res = JSON.parse(message.jsonInfo)
        var gameOption = detailsMessage.gameOption
        res.map((item) => {
            if (item.key == 'SupportedVersion') {
                detailsMessage.gameVersionMust = item.value.split(',')
            }
            if (item.key == "GameVersion") {
                if (item.value != detailsMessage.gameVersion) {
                    detailsMessage.gameVersion = item.value
                    if (item.value == 1) {
                        $('.detail-user-list .detail-user-list-item.canChange').each(function () {
                            let mychoose = $(this).find('.country-select').find('.gx-menu-in').children().children().text()
                            if (mychoose.indexOf(translatesrting('尤里')) == 0 || mychoose.indexOf(translatesrting('神州')) == 0) {
                                $(this).find('.J_chooseCountry')[0].click()
                            }
                        })
                    } else if (item.value == 3) {
                        $('.detail-user-list .detail-user-list-item.canChange').each(function () {
                            let mychoose = $(this).find('.country-select').find('.gx-menu-in').children().children().text()
                            if (mychoose.indexOf(translatesrting('尤里')) == 0) {
                                $(this).find('.J_chooseCountry')[0].click()
                            }
                        })
                    } else if (item.value == 0) {
                        $('.detail-user-list .detail-user-list-item.canChange').each(function () {
                            let mychoose = $(this).find('.country-select').find('.gx-menu-in').children().children().text()
                            if (mychoose.indexOf(translatesrting('神州')) == 0) {
                                $(this).find('.J_chooseCountry')[0].click()
                            }
                        })
                    }
                    $('.detail-option-version-content').find('.detail-option-version').each(function () {
                        if ($(this).attr('data-type') == item.value) {
                            $(this).removeClass('prohibit')
                            $(this).find('i').css('display', 'block')
                        } else {
                            $(this).addClass('prohibit')
                            $(this).find('i').css('display', 'none')
                        }
                    })
                    $('.detail-map-img-name').find('.detail-map-img-name-img').each(function () {
                        if ($(this).attr('data-type') == item.value) {
                            $(this).css('display', 'inline-block')
                        } else {
                            $(this).css('display', 'none')
                        }
                    })
                    if (detailsMessage.gameVersion == 3) {
                        detailsMessage.optionsCountry.map((item) => {
                            if (item.sideId == 9) {
                                item.country = 'Shenzhen'
                                item.cname = '神州'
                            }
                        })
                    } else {
                        detailsMessage.optionsCountry.map((item) => {
                            if (item.sideId == 9) {
                                item.country = 'Yuri'
                                item.cname = '尤里'
                            }
                        })
                    }
                }
            }
            gameOption.map((options) => {
                if (options.key == item.key) {
                    options.value = item.value
                    options.must = item.must
                }
            })
        })
        detailsMessage.gameOption = gameOption
        saveMessage('gameOption', { roomNo: detailsMessage.roomNo, gameOption: gameOption })
        JSgameOption(detailsMessage.gameOption, detailsMessage.isHost)
        isYuriCountry()
    } else if (message.msgType == 20) {
        // 下载地图
        var res = JSON.parse(message.jsonInfo)
        if (detailsMessage.mapmessage && detailsMessage.mapmessage.mapSha1 == res.sign) {
            downloadMap(parseInt(res.nowPercentage))
        }
    } else if (message.msgType == 22) {
        // 聊天信息
        var res = JSON.parse(message.jsonInfo)
        if (res.TypeCode == 6) {
            addChatMessage(true, res.SenderName, res.Message)
            return false
        }
        if (res.TypeCode == 7 && !detailsMessage.isHost) {
            parent.startModal()
            return false
        }
        if (res.SenderId != userInfo.userId && res.TypeCode != 5) {
            var temp_msg = res.Message.split('&beInvited*#17&')
            if (temp_msg.length == 2) {
                BeInvitedRoom(temp_msg[0], temp_msg[1])
            } else {
                if (detailsMessage.HostId == res.SenderId) {
                    addChatMessage(true, res.SenderName, res.Message)
                } else {
                    addChatMessage(false, res.SenderName, res.Message)
                }
            }
        }
        if (res.SenderId == userInfo.userId && detailsMessage.isHost && res.TypeCode == 3) {
            detailsMessage.gameautoOpen = true
            wsUserStatusdetails(7)
            player_sound_openfail()
            // 开始游戏失败
            gxmodal({
                title: translatesrting('提示'),
                centent: '<div class="mt25" style="padding-bottom: 20px;">' + res.Message + '</div>',
                buttons: [{
                    text: translatesrting('确定'),
                    class: 'btn-middle btn-middle-blue',
                }]
            })

            $('.details-all-close-sure').removeClass('hide')
            $('.detail-container-startbtn span').gxbtn('reset')
            detailsMessage.startTime = 5
            clearInterval(detailsMessage.startTimefun)
            $('.detail-start-mark').addClass('hide')
            $('.detail-option').removeClass('pointer-events-none')
            $('.detail-container-startbtn span').text(translatesrting('开始游戏'))
        }
        if (res.TypeCode == 3) {
            if (detailsMessage.isHost) {
                $('.detail-option').removeClass('pointer-events-none')
            }
            $('.detail-start-mark').addClass('hide')
            $('.detail-container-startbtn').closest('.relative').find('.detail-start-mark').addClass('hide')
            $('.detail-container-readybtn').removeClass('disabled')
            $('.detail-container-readybtn-in').removeClass('disabled')
        }
        if (res.TypeCode == 5) {
            if (res.Message == '空') {
                $('.detail-container-right-chat-bottom .detail-container-right-notice').addClass('hide')
                $('.detail-container-right-chat-bottom .J_noticeText').text("")
                $('.detail-container-right-chat-bottom .J_noticeText').attr('data-text', "")
            } else {
                $('.detail-container-right-chat-bottom .detail-container-right-notice').removeClass('hide')
                $('.detail-container-right-chat-bottom .J_noticeText').text(res.Message)
                $('.detail-container-right-chat-bottom .J_noticeText').attr('data-text', res.Message)
            }
            noticeHeight()
            saveMessage('roomnotice', { message: res.Message, roomNo: detailsMessage.roomNo })
        }
    } else if (message.msgType == 24) {
        // 修改房间地图配置通知
        var res = JSON.parse(message.jsonInfo)
        if (res.imageUrl || res.mapPreviewBase64) {
            comOption.optionsMode.map((item) => {
                if (item.code == res.mode) {
                    res.Jmode = item.name
                }
            })
            res.gameVersion = detailsMessage.gameVersion
            comOption.optionsMode.map((item) => {
                if (item.code == res.mapType) {
                    res.Jmode = item.name
                }
            })
            if (detailsMessage.optionsBirth.length != 0 && $('.detail-map-img-img').attr('data-mapSha1') == res.mapSha1 && $('.detail-map-img-name-jmode').attr('data-jmode') == res.Jmode) {
                return false;
            } else {
                detailsMessage.optionsBirth = res.startingLocations
                detailsMessage.mapMove = false
            }
            res.imageUrl = res.imageUrl.split('?x-oss-process=image/resize')[0]
            oneImgCache(res.imageUrl).then((Cachereturn) => {
                res.imageUrl = Cachereturn
                $('.detail-map-content').html(template('detail-map-script', res))
                $.star({
                    type: 'GET',
                    url: '/battlecenter/redwar/map/v1/get-map',
                    showerror: false,
                    data: {
                        mapSha1: res.mapSha1,
                    },
                    success: function (res) {
                        detailsMessage.mapmessage = res.data
                        res.data.Jmodels = []
                        res.data.models.map((models) => {
                            comOption.optionsMode.map((itemmodels) => {
                                if (models == itemmodels.code) {
                                    res.data.Jmodels.push(itemmodels)
                                }
                            })
                        })
                        let addlihtml = ''
                        res.data.Jmodels.map((item) => {
                            addlihtml += '<div class="gx-menu-li"> <div class="J_detailChoosemapType" data-code="' + item.code + '">' + item.name + '</div></div>'
                        })
                        $('.detail-map-img-name .gx-menu-ul').html(addlihtml)
                        if (detailsMessage.isHost) {
                            $('.detail-map-content').find('.gx-menu').gxmenu({
                                height: "30px",
                                top: "40px",
                                clickHide: true
                            }, function (e) {

                            })
                        } else {
                            $('.detail-map-img-name .gx-menu .caret').addClass('hide')
                        }

                        if (res.data && res.data.description) {
                            $('.detail-map-desc').removeClass('hide')
                            $('.detail-map-desc').text(res.data.description)
                        }
                        if (res.data.gold > 0 && detailsMessage.isHost) {
                            $('#detail-gold').removeClass('hide')
                            $('#detail-gold').find('.detail-gold-num').text(res.data.gold)
                        } else {
                            $('#detail-gold').addClass('hide')
                        }
                        detailsMessage.roomGold = res.data.gold
                        if (detailsMessage.isHost == false) {
                            $.controlAjax({
                                type: "get",
                                url: '/api/details/game/ready',
                                showerror: true,
                                data: {
                                    status: false
                                },
                                success: function (res) {
                                    detailsMessage.isReady = false
                                }
                            })
                            $('.detail-container-readybtn-in').addClass('hide')
                            $('.detail-container-readybtn').removeClass('hide')
                            $('.detail-user-list-item').each(function () {
                                if ($(this).attr('data-id') == userInfo.userId) {
                                    $(this).find('.detail-container-ready-img').addClass('hide')
                                    $(this).addClass('canChange')
                                    $(this).addClass('iconfontReady')
                                }
                            })
                        }
                    },
                    error: function (req) { }
                });
                if (detailsMessage.optionsBirth == null || detailsMessage.optionsBirth.length <= 0) {
                    downloadMap(30)
                }
                detailsImgSize()
                // detailsMessage.mapMaxPlayers = res.mapMaxPlayers
                scriptUserReal()
                if (detailsMessage.isHost) {
                    $('.J_changeMap').removeClass('hide') //修改地图按钮
                }
            })
        }
    } else if (message.msgType == 26) {
        // 游戏结束
        // 游戏结束埋点
        detailsMessage.gameautoOpen = true
        saveMessage('ClientBGM', 'false')
        wsUserStatusdetails(7)
        if (res && res.id) {
            // showSettlement(res.id, true)
        } else {
            $('.detail-start-mark').addClass('hide')
            if (!detailsMessage.isHost) {
                $('.detail-container-col-right .gx-img-btn-big').removeClass('disabled')
            } else {
                closeAuto()
            }
        }
        if (detailsMessage.isHost) {
            closeAuto()
            $('.detail-option').removeClass('pointer-events-none')
            // 查询房主金币
            queryHostGold()
        } else {
            if ($('.detail-container-readybtn').hasClass('hide') && $('.detail-container-readybtn-inlook').hasClass('hide')) {
                $('.detail-user-list-item').each(function () {
                    if ($(this).attr('data-id') == userInfo.userId) {
                        $(this).removeClass('inlook')
                        $(this).find('.detail-container-ready-img').addClass('hide')
                        $(this).addClass('canChange')
                        $(this).removeClass('iconfontReady')
                    }
                })
                $('.detail-container-readybtn-in').addClass('hide')
                $('.detail-container-readybtn').removeClass('hide')
                $('.detail-container-readybtn-inlook').addClass('hide')
                detailsMessage.isReady = false
            }
        }

        // ipcRenderer.send('Main', {
        //     msgType: "ChangeWindowSize",
        //     jsonInfo: {
        //         window: 'createroom',
        //         width: 1586,
        //         height: 800,
        //         isCenter: true
        //     }
        // });
    } else if (message.msgType == 28) {
        if (!detailsMessage.isHost) {
            checkWatch(1)
        }
        // 游戏设置锁定
        var res = JSON.parse(message.jsonInfo)
        if (res.status) {
            // 游戏开始埋点
            wsUserStatusdetails(8)
            $('.detail-start-mark').removeClass('hide')
            $('.detail-container-col-right .gx-img-btn-big').addClass('disabled')

            $('.detail-user-list-item').each(function () {
                if ($(this).attr('data-id') == userInfo.userId) {
                    $(this).removeClass('iconfontReady')
                }
            })
        } else {
            $('.detail-start-mark').addClass('hide')
            $('.detail-container-col-right .gx-img-btn-big').removeClass('disabled')
        }
    } else if (message.msgType == 2) {

    } else if (message.msgType == 4) {
        // 监听准备状态

    } else if (message.msgType == 30) {
        let getuser = []
        getuser.push(res)
        ipcUser(getuser, 'error')
    }
})
detailInit()
function detailInit() {
    $('.details-content').css('opacity', 1) //为了防止跳转页面闪一下的问题
    // 创建时房间数据
    $.controlAjax({
        type: "get",
        url: '/api/details/inroom/info',
        showerror: true,
        data: {
            matchIng: false
        },
        success: function (res) {
            // 进入房间埋点
            wsUserStatusdetails(7)
            var details = res.data
            if (details) {
                if (details.roomStatus == 3) {
                    detailsMessage.gameautoOpen = false
                } else {
                    let Audio_inroom = document.getElementById('Audio_inroom')
                    Audio_inroom.play()
                }
                let ismyinit = false
                for (var i = 0; i < details.players.length; i++) {
                    if (details.players[i].userId == userInfo.userId) {
                        ismyinit = true
                    }
                }
                if (!ismyinit) {
                    if (detailsMessage.initeErrorNum > 0) {
                        setTimeout(function () {
                            detailsMessage.initeErrorNum = detailsMessage.initeErrorNum - 1
                            detailInit()
                        }, 1000)
                        return false
                    }
                }
                detailsMessage.optionsBirth = details.startingLocations
                if (details.gameVersion == '2') {
                    detailsMessage.gameVersion = 0
                } else {
                    detailsMessage.gameVersion = details.gameVersion
                }
                detailsMessage.optionsCountry = comOption.optionsCountry
                if (detailsMessage.gameVersion == 3) {
                    detailsMessage.optionsCountry.map((item) => {
                        if (item.sideId == 9) {
                            item.country = 'Shenzhen'
                            item.cname = '神州'
                        }
                    })
                }
                $('.detail-option-version-content').find('.detail-option-version').each(function () {
                    if ($(this).attr('data-type') == details.gameVersion) {
                        $(this).find('i').css('display', 'block')
                        $(this).removeClass('prohibit')
                    } else {
                        $(this).addClass('prohibit')
                        $(this).find('i').css('display', 'none')
                    }
                })
                detailsInit(details)
            } else {
                detailInit()
            }
        },
        error: function (req) {

        }
    })
}
// 渲染详情页
function detailsInit(details) {
    detailsMessage.roomNo = details.roomNo //房间号
    detailsMessage.roomName = details.roomName //房间号
    detailsMessage.mapName = details.mapName //地图名字、方便修改地图时显示第一个
    detailsMessage.isHavePas = details.isHavePas //地图名字、方便修改地图时显示第一个

    if (detailsMessage.isHavePas == true) {
        detailsMessage.pwd = details.password
    }
    var temp_invitename = window.location.href.split('?invitename=')
    if (temp_invitename.length == 2) {
        var temp_msg = String(decodeURI(temp_invitename[1]) + '&beInvited*#17&' + userInfo.username)
        ipcRenderer.send('Main', {
            msgType: "ChatMessage",
            jsonInfo: {
                msg: temp_msg
            }
        });
        BeInvitedRoom(decodeURI(temp_invitename[1]), userInfo.username)
    }
    // detailsMessage.mapMaxPlayers = details.mapMaxPlayers//地图最大人数
    $('#roomNo').text(detailsMessage.roomNo) //房间号
    $('#roomNo-copy-btn').attr('data-clipboard-text', detailsMessage.roomNo)
    var clipboard = new Clipboard('#roomNo-copy-btn');

    $('#roomName').text(details.roomName) //房间号
    comOption.optionsMode.map((item) => {
        if (item.code == details.mapType) {
            details.Jmode = item.name
        }
    })
    details.imageUrl = details.imageUrl.split('?x-oss-process=image/resize')[0]
    // oneImgCache(details.imageUrl).then((Cachereturn) => {
    let dellImageUrl = details.imageUrl
    details.imageUrl = ''
    // details.imageUrl = Cachereturn
    $('.detail-map-content').html(template('detail-map-script', details))
    dellDetailImage(dellImageUrl)
    if (details.mapSha1) {
        $.star({
            type: 'GET',
            url: '/battlecenter/redwar/map/v1/get-map',
            showerror: false,
            data: {
                mapSha1: details.mapSha1,
            },
            success: function (res) {
                detailsMessage.mapmessage = res.data
                res.data.Jmodels = []
                res.data.models.map((models) => {
                    comOption.optionsMode.map((itemmodels) => {
                        if (models == itemmodels.code) {
                            res.data.Jmodels.push(itemmodels)
                        }
                    })
                })
                let addlihtml = ''
                res.data.Jmodels.map((item) => {
                    addlihtml += '<div class="gx-menu-li"> <div class="J_detailChoosemapType" data-code="' + item.code + '">' + item.name + '</div></div>'
                })
                $('.detail-map-img-name .gx-menu-ul').html(addlihtml)
                if (detailsMessage.isHost) {
                    $('.detail-map-content').find('.gx-menu').gxmenu({
                        height: "30px",
                        top: "40px",
                        clickHide: true
                    }, function (e) {

                    })
                } else {
                    $('.detail-map-img-name .gx-menu .caret').addClass('hide')
                }
                // if (res.data.gameVersion == '2') {
                //     $('.detail-option-version-content').removeClass('pointer-events-none')
                // } else {
                //     $('.detail-option-version-content').addClass('pointer-events-none')
                // }
                if (res.data && res.data.description) {
                    $('.detail-map-desc').removeClass('hide')
                    $('.detail-map-desc').text(res.data.description)
                }
                if (res.data && res.data.gold > 0 && detailsMessage.isHost) {
                    $('#detail-gold').removeClass('hide')
                    $('#detail-gold').find('.detail-gold-num').text(res.data.gold)
                    detailsMessage.roomGold = res.data.gold
                } else {
                    $('#detail-gold').addClass('hide')
                }
            },
            error: function (req) { }
        });
    }
    if (detailsMessage.optionsBirth == null || detailsMessage.optionsBirth.length <= 0) {
        downloadMap(30)
    }
    detailsImgSize()
    // 渲染用户数据
    var userlist = getSaveMessage('msgType10', false)
    if (!userlist) {
        userlist = details.players
    }
    if (details.players) {
        details.players.map((item) => {
            if (item.isHost && item.userId == userInfo.userId) {
                detailsMessage.isHost = true // 判断房主 自动准备
                detailsMessage.isReady = true
                if (details.roomStatus != 3) {
                    closeAuto()//房主 自动关闭
                }
            }
            if (item.isHost) {
                detailsMessage.HostId = item.userId //房主ID,用于聊天
            }
        })
    } else {
        userlist.map((item) => {
            if (item.isHost && item.userId == userInfo.userId) {
                detailsMessage.isHost = true // 判断房主 自动准备
                detailsMessage.isReady = true
                if (details.roomStatus != 3) {
                    closeAuto()//房主 自动关闭
                }
            }
            if (item.isHost) {
                detailsMessage.HostId = item.userId //房主ID,用于聊天
            }
        })
    }
    if (!details.isHavePas) {
        showCoinChat()
    }
    // 渲染游戏设置
    if (getSaveMessage('gameOption') && !detailsMessage.isHost) {
        let gameOption = getSaveMessage('gameOption')
        if (details.roomNo == gameOption.roomNo) {
            JSgameOption(gameOption.gameOption) //游戏设置
        } else {
            JSgameOption(details.gameOption) //游戏设置
        }
    } else {
        JSgameOption(details.gameOption) //游戏设置
    }
    // saveMessage('gameOption', { roomNo: details.roomNo, gameOption: details.gameOption })
    scriptuser(userlist) //用户设置
    if (detailsMessage.isHost) {
        $('.J_changeMap').removeClass('hide') //修改地图按钮
        $('.J_showSetRoomNotice').removeClass('hide') //修改地图按钮
        $('.detail-container-startbtn').removeClass('hide') //开始游戏按钮
        $('.detail-container-readybtn').addClass('hide')
        $('.detail-option').removeClass('pointer-events-none')
    } else {
        $('.detail-container-readybtn').removeClass('hide') //准备游戏按钮
    }
    let roomnotic = getSaveMessage('roomnotice')
    if (roomnotic) {
        if (roomnotic.roomNo == detailsMessage.roomNo) {
            if (roomnotic.message == '空' || roomnotic.message == '') {
                $('.detail-container-right-chat-bottom .detail-container-right-notice').addClass('hide')
                $('.detail-container-right-chat-bottom .J_noticeText').text("")
                $('.detail-container-right-chat-bottom .J_noticeText').attr('data-text', "")
            } else {
                $('.detail-container-right-chat-bottom .detail-container-right-notice').removeClass('hide')
                $('.detail-container-right-chat-bottom .J_noticeText').text(roomnotic.message)
                $('.detail-container-right-chat-bottom .J_noticeText').attr('data-text', roomnotic.message)
            }
            noticeHeight()
        } else {
            // localStorage.removeItem('roomnotice')
        }
    }
    if (getSaveMessage('batterStorage')) {
        let oldbatterStorage = getSaveMessage('batterStorage')
        if (oldbatterStorage.roomNo == detailsMessage.roomNo) {
            batterMessage.localstorage = oldbatterStorage
        }
    }
    // })
}

// 渲染游戏设置
function JSgameOption(getoptions, ishost) {
    detailsMessage.gameOption = getoptions
    var changeMax = 0
    detailsMessage.gameOption.map((options) => {
        // 解决数据不一致问题
        if (options.value == 'true' || options.value == 'True') {
            options.value = true
        }
        if (options.value == 'false' || options.value == 'False') {
            options.value = false
        }
        if (options.key == 'MaxPlayers') {
            changeMax = options.value //更新房间大人数
        }

        if (options.key == 'GameVersion') {
            detailsMessage.gameVersion = options.value
        }
        if (options.key == 'SupportedVersion') {
            detailsMessage.gameVersionMust = options.value.split(',')
        }
    })

    if (!ishost) {
        $('.detail-select-option').html(template('detail-select-option-script', {
            options: detailsMessage.gameOption,
            isautopen: detailsMessage.autoopencheck,
            isHost: detailsMessage.isHost
        })) //右侧游戏设置
        $('.detail-swich-option').html(template('detail-swich-option-script', {
            options: detailsMessage.gameOption,
            isHost: detailsMessage.isHost
        })) //右侧游戏设置
    } else {
        $('.detail-select-option').html(template('detail-select-option-script', {
            options: detailsMessage.gameOption,
            isautopen: detailsMessage.autoopencheck,
            isHost: detailsMessage.isHost
        })) //右侧游戏设置
        $('.detail-swich-option').html(template('detail-swich-option-script', {
            options: detailsMessage.gameOption,
            isHost: detailsMessage.isHost
        })) //右侧游戏设置
    }
    if (detailsMessage.gameVersionMust) {
        $('.detail-option-version').each(function () {
            if (detailsMessage.gameVersionMust.indexOf($(this).attr('data-type')) == -1) {
                $(this).find('.detail-option-version-i').addClass('pointer-events-none-version')
            } else {
                $(this).find('.detail-option-version-i').removeClass('pointer-events-none-version')
            }
        })
        if (detailsMessage.gameVersionMust.indexOf(detailsMessage.gameVersion.toString()) == -1) {
            for (var i = 0; i < $('.detail-option-version').length; i++) {
                if (detailsMessage.gameVersionMust.indexOf($('.detail-option-version').eq(i).attr('data-type')) != -1) {
                    $('.detail-option-version').eq(i).click()
                    break
                }
            }
        }
    } else {
        $('.detail-option-version-content').removeClass('pointer-events-none')
    }
    if (detailsMessage.maxPlayers != changeMax) {
        detailsMessage.maxPlayers = changeMax
        scriptUserReal()
    }
    $('.Credits-swich').each(function () {
        var $this = $(this)
        $this.gxswich({}, function (e) {
            player_sound_click()
            ipcRenderer.send('Main', {
                msgType: "SetGameOption",
                jsonInfo: {
                    Key: $this.attr('data-key'),
                    Value: e
                }
            });
        })
    })
}

function scriptuser(userList) {
    let hostitem = []
    let isinite = false
    userList.map((item, index) => {
        if (item.isHost) {
            hostitem = item
            userList.splice(index, 1);
            isinite = true
        }
    })
    if (isinite) {
        userList.unshift(hostitem)
    }
    detailsMessage.localUserlist = []
    for (var i = 0; i < Number(detailsMessage.mapMaxPlayers); i++) {
        detailsMessage.localUserlist.push({
            userId: 'empty',
            prohibit: true
        })
    }
    for (var i = 0; i < Number(detailsMessage.maxPlayers); i++) {
        detailsMessage.localUserlist[i] = {
            userId: 'empty',
            prohibit: false
        }
    }
    userList.map((item, index) => {
        detailsMessage.optionsCountry.map((optionC) => {
            if (item.sideId == optionC.sideId) {
                item.JsCountry = optionC
            }
        })
        comOption.optionsColor.map((optionC) => {
            if (item.colorId == optionC.colorId) {
                item.JsColor = optionC
            }
        })
        comOption.optionsTeam.map((optionC) => {
            if (item.teamId == optionC.teamId) {
                item.JsTeam = optionC
            }
        })
        item.customField = customFieldFun('decode', item.customField)
        detailsMessage.localUserlist[index] = item
    })
    $('.detail-user-list').html(template('detail-user-list-script', {
        userList: detailsMessage.localUserlist,
        optionsCountry: detailsMessage.optionsCountry,
        optionsColor: comOption.optionsColor,
        optionsTeam: comOption.optionsTeam,
        optionsBirth: detailsMessage.optionsBirth,
        myuserId: userInfo.userId,
        isHost: detailsMessage.isHost,
        notr: false
    }))
    $('.details-userlabel[data-toggle="tooltip"]').tooltip()
    userList.map((item) => {
        if (item.ping != '-1') {
            $('.detail-user-list-item').each(function () {
                var id = $(this).attr('data-id')
                if (id == item.userId) {
                    $(this).find('.details-ping-iconfont').removeClass('hide')
                    if (item.ping >= 200) {
                        $(this).find('.details-ping').css('color', '#FF4444')
                        $(this).find('.details-ping-svg').css('fill', '#FF4444')
                    } else if (item.ping >= 100) {
                        $(this).find('.details-ping').css('color', '#FFA333')
                        $(this).find('.details-ping-svg').css('fill', '#FFA333')
                    } else {
                        $(this).find('.details-ping').css('color', '#A2FF33')
                        $(this).find('.details-ping-svg').css('fill', '#A2FF33')
                    }
                    if (item.packageLose >= 30) {
                        $(this).find('.details-ping').css('color', '#FF4444')
                        $(this).find('.details-ping-svg').css('fill', '#FF4444')
                    }
                }
            })
        }
        if (item.userId == userInfo.userId) {
            if (item.sideId == 10) {
                $('.detail-user-list-item').each(function () {
                    if ($(this).attr('data-id') == userInfo.userId) {
                        $(this).addClass('inlook')
                    }
                })
                if (!detailsMessage.isHost) {
                    $.controlAjax({
                        type: "get",
                        url: '/api/details/game/ready',
                        showerror: true,
                        data: {
                            status: true
                        },
                        success: function (res) {
                            if (!detailsMessage.isHost) {
                                detailsMessage.isReady = true
                            }
                        }
                    })
                    $('.detail-container-readybtn-in').addClass('hide')
                    $('.detail-container-readybtn').addClass('hide')
                    $('.detail-container-readybtn').closest('div').addClass('hide')
                    $('.detail-container-readybtn-inlook').removeClass('hide')
                }
            }
        }

        // if (item.userId == userInfo.userId) {
        //     if (item.sideId == 10) {
        //         $('.detail-user-list-item').each(function () {
        //             if ($(this).attr('data-id') == userInfo.userId) {
        //                 $(this).addClass('inlook')
        //                 $(this).find('.detail-container-ready-img').removeClass('hide')
        //                 $(this).removeClass('canChange')
        //                 $(this).addClass('iconfontReady')
        //             }
        //         })
        //         if( !detailsMessage.isHost){
        //             $('.detail-container-readybtn-in').addClass('hide')
        //             $('.detail-container-readybtn').addClass('hide')
        //             $('.detail-container-readybtn').closest('div').addClass('hide')
        //             $('.detail-container-readybtn-inlook').removeClass('hide')
        //         }
        //     }
        // }
    })
    selectfun()
    isallready()
    isYuriCountry()
}

function selectfun() {
    $('.canChange .country-select').gxmenu({
        height: "20px",
        top: "40px",
        clickHide: true
    }, function (e) {

    })
    $('.canChange .color-select').gxmenu({
        height: "30px",
        top: "40px",
        clickHide: true
    }, function (e) {

    })
    $('.canChange .team-select').gxmenu({
        height: "30px",
        top: "40px",
        clickHide: true
    }, function (e) {

    })
    $('.canChange .birth-select').gxmenu({
        height: "30px",
        top: "40px",
        clickHide: true
    }, function (e) {

    })
    $('.canChange .detail-robot-menu').gxmenu({
        height: "30px",
        top: "20px",
        clickHide: true
    }, function (e) {

    })
    $('.detail-user-list-item-empty .detail-robot-menu').gxmenu({
        height: "30px",
        top: "20px",
        clickHide: true
    }, function (e) {

    })
    //用于解决颜色必须不重复
    let isgetColor = []
    $('.color-select-div').each(function () {
        isgetColor.push($(this).attr('data-code'))
    })
    $('.J_chooseColor').each(function () {
        let inChooseColor = $(this).attr('data-code')
        if (isgetColor.indexOf(inChooseColor) != "-1") {
            $(this).closest('.gx-menu-li').addClass('hide')
        } else {
            $(this).closest('.gx-menu-li').removeClass('hide')
        }
    })
    //用于解决出生点必须不重复
    let isgetBirth = []
    $('.Birth-select-div').each(function () {
        isgetBirth.push($(this).attr('data-code'))
    })
    $('.J_chooseBirth').each(function () {
        let inChooseBirth = $(this).attr('data-code')
        if (isgetBirth.indexOf(inChooseBirth) != "-1") {
            $(this).closest('.gx-menu-li').addClass('hide')
        } else {
            $(this).closest('.gx-menu-li').removeClass('hide')
        }
    })
    //用于解决层级
    $('.detail-robot-menu').each(function (index, e) {
        $(this).css('zIndex', (10 - index))
    })
}

// 房主是否可以点开始游戏
function isallready() {
    // 房主是否可以点开始游戏
    var isallready = true
    detailsMessage.localUserlist.map((item) => {
        if (!item.ready && item.userId != 'empty' && !item.isHost) {
            isallready = false
        }
    })
    detailsMessage.localUserlist.map((item) => {
        if (item.isInGame && item.userId != 'empty' && !item.isHost && !item.isAI) {
            isallready = false
        }
    })
    if (isallready && detailsMessage.isHost) {
        $('.detail-container-startbtn').removeClass('disabled')
    } else {
        autoOpen('clean')
        $('.detail-container-startbtn').addClass('disabled')
    }
    // 10s自动开游戏机制
    var autoopen = true
    var isonlyHost = true
    var hasC = 0
    var ainum = 0
    var obnum = 0
    var emptynum = 0
    detailsMessage.localUserlist.map((item) => {
        if (!item.isAI && !item.isHost && item.userId != 'empty' && !item.ready) {//准备判断
            autoopen = false
        }
        if (!item.isAI && !item.isHost && item.userId != 'empty') {//只有房主判断
            isonlyHost = false
        }
        if (!item.isAI && !item.isHost && item.userId != 'empty' && item.isInGame) {//战斗中判断
            autoopen = false
        }
        if (item.prohibit == false) {//空位判断
            emptynum = emptynum + 1
            autoopen = false
        }
        if (item.userId != 'empty') {
            hasC = hasC + 1
        }

        if (item.userId != 'empty' && item.isAI) {
            ainum = ainum + 1
        }
        if (item.userId != 'empty' && item.sideId == 10) {
            obnum = obnum + 1
        }
    })
    if (emptynum > 0 && detailsMessage.autoopentime != 10) {
        autoOpen('clean')
    }
    if ((hasC - ainum - obnum) < 2) {
        autoopen = false
    }
    if (detailsMessage.autoopencheck && !isonlyHost && autoopen && detailsMessage.autoopen && detailsMessage.isHost && detailsMessage.gameautoOpen) {
        detailsMessage.autoopen = false
        setTimeout(function () {
            // 10s一次机制
            detailsMessage.autoopen = true
        }, 20000)
        autoOpen()
    }
}
function autoOpen(type) {
    if (type == 'clean') {
        $('.detail-container-startbtn').find('span').text(translatesrting('开始游戏'))
        $('.detail-container-startbtn').removeClass('autoOpenBtn')
        detailsMessage.autoopentime = 10
        clearInterval(detailsMessage.autoopenfun)
        player_sound_autoopen('pause')
    } else {
        player_sound_autoopen()
        ipcRenderer.send('Main', {
            msgType: "WindowSize",
            jsonInfo: {
                window: 'main',
                size: 'flashFrame'
            }
        });
        ipcRenderer.send('Main', {
            msgType: "ChatMessage",
            jsonInfo: {
                msg: translatesrting('所有人已准备完毕，游戏将在10秒后自动开始。')
            }
        });
        addChatMessage(detailsMessage.isHost, userInfo.username, translatesrting('所有人已准备完毕，游戏将在10秒后自动开始。'), false)
        $('.detail-container-startbtn').addClass('autoOpenBtn')
        detailsMessage.autoopenfun = setInterval(function () {
            if (detailsMessage.autoopentime > 0) {
                if ($('.detail-container-startbtn').find('span').text() != translatesrting('取消自动开始') && $('.detail-container-startbtn').find('span').text() != translatesrting('启动中')) {
                    $('.detail-container-startbtn').find('span').text(translatesrting('自动开始') + detailsMessage.autoopentime + 's')
                }
                detailsMessage.autoopentime = detailsMessage.autoopentime - 1
            } else {
                $('.detail-container-startbtn').find('span').text(translatesrting('开始游戏'))
                $('.detail-container-startbtn').removeClass('autoOpenBtn')
                $('.detail-container-startbtn').click()
                detailsMessage.gameautoOpen = false
                detailsMessage.autoopentime = 10
                clearInterval(detailsMessage.autoopenfun)
            }
        }, 1000)
    }
}
// 房间配置
$(document).on('change', '.detail-select-option-item input', function () {
    var $this = $(this)
    //房主有权
    if (!detailsMessage.isHost) {
        if ($this.is(':checked')) {
            $this.prop('checked', false)
        } else {
            $this.prop('checked', true)
        }
        return false;
    }
    var keys = $this.attr('data-name')
    if (keys == 'AutoOpen') {
        if ($this.is(':checked')) {
            detailsMessage.autoopencheck = true
            localStorage.removeItem('roomAutoOpen')
        } else {
            detailsMessage.autoopencheck = false
            saveMessage('roomAutoOpen', { check: false })
            autoOpen('clean')
            return false
        }
    }
    ipcRenderer.send('Main', {
        msgType: "SetGameOption",
        jsonInfo: {
            Key: keys,
            Value: $this.is(':checked')
        }
    });
    detailsMessage.gameOption.map((options) => {
        if (options.key == keys) {
            options.value = $this.is(':checked')
        }
    })
    saveMessage('gameOption', { roomNo: detailsMessage.roomNo, gameOption: detailsMessage.gameOption })
})
$(document).on('mouseenter', '.autoOpenBtn', function () {
    var $this = $(this).find('span')
    $this.text(translatesrting('取消自动开始'))
})
$(document).on('mouseleave', '.autoOpenBtn', function () {
    var $this = $(this).find('span')
    $this.text('')
})
// 点击开始游戏
$(document).on('click', '.detail-container-startbtn', function () {
    player_sound_click()
    var $this = $(this).find('span')
    if ($(this).hasClass('autoOpenBtn')) {
        autoOpen('clean')
        return false
    }
    var $thisbtn = $(this)
    if ($thisbtn.hasClass('disabled')) {
        return false
    }
    if ($this.hasClass('gx-disabled')) {
        return false
    }

    // 一个无准备不可点击 一个loading
    if (Number(detailsMessage.roomGold) > 0) {
        $.star({
            type: 'GET',
            url: '/community-user/account/v1/query',
            showerror: false,
            data: {},
            success: function (res) {
                if (Number(res.data.gold) > 0) {
                    startGamebefor()
                } else {
                    gxmodal({
                        title: translatesrting('金币不足'),
                        closeCoinHide: true,
                        centent: '<div class="mt30" style="padding:0px 16px">' + translatesrting('您的金币不足,房间已自动解散，请玩一玩匹配或者加入别人的房间一起玩可获得大量的金币哦') + '</div>',
                        buttons: [{
                            text: translatesrting('知道了'),
                            class: 'btn-middle btn-middle-blue',
                            callback: function () {
                                LeaveGameRoom()
                            }
                        }]
                    })
                }
            },
            error: function (req) { }
        });
    } else {
        startGamebefor()
    }
})

function startGamebefor() {
    var isonlystart = true;
    detailsMessage.localUserlist.map((item) => {
        if (item.name && !item.isHost && !item.isAI) {
            isonlystart = false
        }
    })
    if (isonlystart) {
        gxmodal({
            title: translatesrting('提示'),
            centent: '<div class="mt25">' + translatesrting('只有您一名玩家，是否开启单人模式？') + '</div>',
            buttons: [{
                text: translatesrting('确定'),
                class: 'btn-middle btn-middle-blue',
                callback: function () {
                    startGame()
                }
            },
            {
                text: translatesrting('取消'),
                class: 'btn-middle btn-style-ash',
            }
            ]
        })
        return false
    }
    startGame()
}

function startGame() {
    clearTimeout(reloadfun)
    clearTimeout(reloadfunlast)
    clearTimeout(reloadfunlasttwo)
    $('.detail-option').addClass('pointer-events-none')
    var $this = $('.detail-container-startbtn span')
    $this.gxbtn('loading')
    parent.startModal()
    checkWatch(2)//检测观战
    // 是否开启观战
    detailsMessage.startTimefun = setInterval(() => {
        $('.detail-start-mark').removeClass('hide')
        // $this.text(translatesrting('开始倒计时') + ' ' + detailsMessage.startTime + 'S')
        $this.text(translatesrting('启动中'))
        detailsMessage.startTime = detailsMessage.startTime - 1
        $('.details-all-close-sure').addClass('hide')
        // storageBatterMessage(1, batterMessage)



        if (detailsMessage.startTime < 0) {
            $('.details-all-close-sure').removeClass('hide')
            wsUserStatusdetails(8)
            $this.gxbtn('reset')
            detailsMessage.startTime = 5
            clearInterval(detailsMessage.startTimefun)
            setTimeout(() => {
                $('.detail-start-mark').addClass('hide')
                $this.text(translatesrting('开始游戏'))
                $('.detail-option').removeClass('pointer-events-none')
            }, 6000)
        }
    }, 1000)
    $.controlAjax({
        type: "get",
        url: '/api/details/game/ready',
        data: {
            status: true
        },
        success: function (res) { },
        error: function (req) {
            $this.gxbtn('reset')
            detailsMessage.startTime = 5
            clearInterval(detailsMessage.startTimefun)
            $('.detail-start-mark').addClass('hide')
            $this.text(translatesrting('开始游戏'))
            $('.detail-option').removeClass('pointer-events-none')
        }
    })
}
var readyCompensate = null
// 点击准备
$(document).on('click', '.detail-container-readybtn', function () {
    player_sound_ready()
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    $this.gxbtn('loading')
    $.controlAjax({
        type: "get",
        url: '/api/details/game/ready',
        showerror: true,
        data: {
            status: true
        },
        success: function (res) {
            if (!detailsMessage.isHost) {
                detailsMessage.isReady = !detailsMessage.isReady
                changeReadyBtn()
            }
        },
        error: function (req) {
            detailsMessage.isReady = detailsMessage.isReady
            changeReadyBtn()
        }
    })
    return false
})
// 取消准备
$(document).on('click', '.detail-container-readybtn-in', function () {
    player_sound_click()

    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    $this.gxbtn('loading')
    $.controlAjax({
        type: "get",
        url: '/api/details/game/ready',
        showerror: true,
        data: {
            status: false
        },
        success: function (res) {
            if (!detailsMessage.isHost) {
                detailsMessage.isReady = !detailsMessage.isReady
                changeReadyBtn()
            }
        },
        error: function (req) {
            detailsMessage.isReady = detailsMessage.isReady
            changeReadyBtn()
        }
    })
    return false
})
//  发送聊天
$(document).on('submit', '.J_chatForm', function (e) {
    chatUp()
    e.preventDefault();
})

function chatUp() {
    var chatmesg = $.trim($('.J_chatInput').val());
    if (chatmesg == "") {
        return false;
    }
    ipcRenderer.send('Main', {
        msgType: "ChatMessage",
        jsonInfo: {
            msg: chatmesg
        }
    });
    $('.J_chatInput').val("")
    $('.J_chatInput').focus()
    addChatMessage(detailsMessage.isHost, userInfo.username, chatmesg, true)
}
// 添加聊天
function addChatMessage(isHost, name, message, isSelf) {
    player_sound_sendchat()
    var reg = new RegExp("<", "g")
    message = message.replace(reg, "<span>" + '<' + "</span>");
    if (isHost) {
        $('.detail-container-right-chat-list').append('<div class="at-row"><div class="at-col--auto ft-orange">' + name + '(' + translatesrting('房主') + ')： </div><div class="at-col at-col--wrap">' + message + '</div></div>')
        $('.detail-container-right-chat-list').scrollTop($('.detail-container-right-chat-list').prop('scrollHeight'))
    } else if (isSelf) {
        $('.detail-container-right-chat-list').append('<div class="at-row"><div class="at-col--auto ft-indigo">' + name + '： </div><div class="at-col at-col--wrap">' + message + '</div></div>')
        $('.detail-container-right-chat-list').scrollTop($('.detail-container-right-chat-list').prop('scrollHeight'))
    } else {
        $('.detail-container-right-chat-list').append('<div class="at-row"><div class="at-col--auto">' + name + '： </div><div class="at-col at-col--wrap">' + message + '</div></div>')
        $('.detail-container-right-chat-list').scrollTop($('.detail-container-right-chat-list').prop('scrollHeight'))
    }
    return false
}
// 添加机器人
var indexnum = null
$(document).on('click', '.J_addRobot', function () {
    var userNum = 0
    detailsMessage.localUserlist.map((item) => {
        if (item.userId != "empty") {
            userNum = userNum + 1
        }
    })
    var aILevel = $(this).attr('data-level')
    indexnum = $('.detail-user-list-item-empty').index($(this).closest('.detail-user-list-item-empty'))
    ipcRenderer.send('Main', {
        msgType: "SavePlayer",
        jsonInfo: {
            isAI: true,
            aILevel: aILevel,
            teamId: 3 //默认D对
        }
    });
})
// 重点 用户列表信息接收
function ipcUser(userarray, type) {
    var addUser = [] //新加入用户
    var oldUser = [] //已有用户
    var delUser = [] //离开的用户
    userarray.map((newItem) => {
        newItem.customField = customFieldFun('decode', newItem.customField)
        newItem.startingLocation = Number(newItem.startingLocation)
        detailsMessage.optionsCountry.map((optionC) => {
            if (newItem.sideId == optionC.sideId) {
                newItem.JsCountry = optionC
            }
        })
        comOption.optionsColor.map((optionC) => {
            if (newItem.colorId == optionC.colorId) {
                newItem.JsColor = optionC
            }
        })
        comOption.optionsTeam.map((optionC) => {
            if (newItem.teamId == optionC.teamId) {
                newItem.JsTeam = optionC
            }
        })
        var isadd = true
        detailsMessage.localUserlist.map((localItem) => {
            if (newItem.userId == localItem.userId) {
                oldUser.push(newItem)
                isadd = false
            }
        })
        if (isadd) {
            addUser.push(newItem)
        }
    })
    detailsMessage.localUserlist.map((localItem) => {
        var isout = true
        userarray.map((newItem) => {
            if (newItem.userId == localItem.userId) {
                isout = false
            }
        })
        if (isout) {
            delUser.push(localItem)
        }
    })
    if (delUser.length > 0 && type != 'error') {
        // 删除消失人
        delUser.map((item) => {
            if (item.userId != userInfo.userId) {
                $('.detail-user-list-item').each(function () {
                    if ($(this).attr('data-id') == item.userId) {
                        $(this).removeClass('canChange')
                        $(this).removeClass('iconfontReady')
                        $(this).addClass('detail-user-list-item-empty')
                        $(this).attr('data-id', null)
                        $(this).html(template('detail-user-list-script', {
                            userList: [{
                                userId: 'empty'
                            }],
                            optionsCountry: detailsMessage.optionsCountry,
                            optionsColor: comOption.optionsColor,
                            optionsTeam: comOption.optionsTeam,
                            optionsBirth: detailsMessage.optionsBirth,
                            myuserId: userInfo.userId,
                            isHost: detailsMessage.isHost,
                            notr: true
                        }))
                    }
                })
            }
        })
    }
    if (addUser.length > 0 && type != 'error') {
        if (detailsMessage.isHost) {
            setTimeout(function () {
                autoRoomNotice()
            }, 1000)
        }
        // 添加新人
        addUser.map((item) => {
            var noaddold = true
            // 是否已拥有
            $('.detail-user-list-item').each(function () {
                if ($(this).attr('data-id') == item.userId) {
                    noaddold = false
                }
            })
            if (noaddold) {
                detailsMessage.optionsCountry.map((optionC) => {
                    if (item.sideId == optionC.sideId) {
                        item.JsCountry = optionC
                    }
                })
                comOption.optionsColor.map((optionC) => {
                    if (item.colorId == optionC.colorId) {
                        item.JsColor = optionC
                    }
                })
                comOption.optionsTeam.map((optionC) => {
                    if (item.teamId == optionC.teamId) {
                        item.JsTeam = optionC
                    }
                })
                var emptyindex = 0
                if (item.isAI && indexnum && !emptyToAi) {
                    emptyindex = indexnum
                }
                emptyToAi = false
                if ($('.detail-user-list-item-empty').eq(emptyindex).length <= 0) {
                    emptyindex = 0
                }
                item.customField = customFieldFun('decode', item.customField)
                $('.detail-user-list-item-empty').eq(emptyindex).html(template('detail-user-list-script', {
                    userList: [item],
                    optionsCountry: detailsMessage.optionsCountry,
                    optionsColor: comOption.optionsColor,
                    optionsTeam: comOption.optionsTeam,
                    optionsBirth: detailsMessage.optionsBirth,
                    myuserId: userInfo.userId,
                    isHost: detailsMessage.isHost,
                    notr: true
                }))
                if (item.userId == userInfo.userId) {
                    $('.detail-user-list-item-empty').eq(emptyindex).addClass('canChange')
                    $('.detail-user-list-item-empty').eq(emptyindex).addClass('mytrstyle')
                } else if (detailsMessage.isHost && item.isAI) {
                    $('.detail-user-list-item-empty').eq(emptyindex).addClass('canChange')
                }
                $('.detail-user-list-item-empty').eq(emptyindex).attr('data-id', item.userId)
                $('.detail-user-list-item-empty').eq(emptyindex).removeClass('detail-user-list-item-empty')
            }
        })
        isYuriCountry()
        $('.details-userlabel[data-toggle="tooltip"]').tooltip()
    }
    if (oldUser.length > 0) {
        // 现有用户状态修改
        oldUser.map((oldUseritem) => {
            detailsMessage.localUserlist.map((localItem) => {
                if (localItem.userId == oldUseritem.userId) {
                    // 进行同步比较
                    $('.detail-user-list-item').each(function () {
                        if ($(this).attr('data-id') == oldUseritem.userId) {
                            if (oldUseritem.userId != userInfo.userId) {
                                if (localItem.ready != oldUseritem.ready) {
                                    // 准备状态
                                    if (oldUseritem.ready) {
                                        $(this).find('.detail-container-ready-img').removeClass('hide')
                                    } else {
                                        $(this).find('.detail-container-ready-img').addClass('hide')
                                    }
                                }
                            } else {
                                if (oldUseritem.sideId == 10) {
                                    $('.J_battleSetup').removeClass('hide')
                                } else {
                                    $('.J_battleSetup').addClass('hide')
                                }
                            }
                            if (localItem.isInGame != oldUseritem.isInGame) {
                                // 是否游戏状态
                                if (oldUseritem.isInGame) {
                                    $(this).find('.detail-container-ingame-img').removeClass('hide')
                                } else {
                                    $(this).find('.detail-container-ingame-img').addClass('hide')
                                }
                            }
                            if (oldUseritem.userId == userInfo.userId && type != 'error') {
                                // 判断自己准备状态
                                // if (!detailsMessage.isHost) {
                                //     detailsMessage.isReady = oldUseritem.ready
                                //     if (oldUseritem.ready) {
                                //         $(this).removeClass('canChange')
                                //         $(this).find('.detail-container-ready-img').removeClass('hide')
                                //         if (oldUseritem.sideId == 10) {
                                //             $('.detail-container-readybtn-in').addClass('hide')
                                //             $('.detail-container-readybtn').addClass('hide')
                                //             $('.detail-container-readybtn-inlook').removeClass('hide')
                                //         } else {
                                //             $('.detail-container-readybtn-in').removeClass('hide')
                                //             $('.detail-container-readybtn').addClass('hide')
                                //         }
                                //     } else {
                                //         $(this).addClass('canChange')
                                //         $('.detail-container-readybtn-in').addClass('hide')
                                //         $('.detail-container-readybtn').removeClass('hide')
                                //         $('.detail-container-readybtn-inlook').addClass('hide')
                                //         $(this).find('.detail-container-ready-img').addClass('hide')
                                //     }
                                // }
                                if (oldUseritem.sideId == 10) {
                                    $('.detail-user-list-item').each(function () {
                                        if ($(this).attr('data-id') == userInfo.userId) {
                                            $(this).addClass('inlook')
                                        }
                                    })
                                } else {
                                    $('.detail-user-list-item').each(function () {
                                        if ($(this).attr('data-id') == userInfo.userId) {
                                            $(this).removeClass('inlook')
                                        }
                                    })
                                }
                            } else {
                                if (localItem.sideId != oldUseritem.sideId) {
                                    // 国家状态
                                    $(this).find('.country-select .gx-menu-in').html(template('detail-user-list-change-script', {
                                        type: 'sideId',
                                        item: oldUseritem
                                    }))
                                }
                                if (localItem.colorId != oldUseritem.colorId) {
                                    // 颜色状态
                                    $(this).find('.color-select').html(template('detail-user-list-change-script', {
                                        type: 'colorId',
                                        item: oldUseritem,
                                        optionsColor: comOption.optionsColor
                                    }))
                                }

                                if (localItem.teamId != oldUseritem.teamId) {
                                    // 队伍状态
                                    $(this).find('.team-select .gx-menu-in').html(template('detail-user-list-change-script', {
                                        type: 'teamId',
                                        item: oldUseritem
                                    }))
                                }
                                if (localItem.startingLocation != oldUseritem.startingLocation) {
                                    // 出生点状态
                                    $(this).find('.birth-select').html(template('detail-user-list-change-script', {
                                        type: 'startingLocation',
                                        item: oldUseritem,
                                        optionsBirth: detailsMessage.optionsBirth,
                                    }))
                                }

                                if (oldUseritem && (localItem.aiLevel != oldUseritem.aiLevel)) {
                                    // 队伍状态
                                    $(this).find('.detail-robot-menu .gx-menu-in').html(template('detail-user-list-change-script', {
                                        type: 'aILevel',
                                        item: oldUseritem
                                    }))
                                }
                            }
                        }
                    })

                }
            })
        })
    }
    if (type != 'error') {
        // 更新本地用户数据
        detailsMessage.localUserlist = []
        for (var i = 0; i < Number(detailsMessage.mapMaxPlayers); i++) {
            detailsMessage.localUserlist.push({
                userId: 'empty',
                prohibit: true
            })
        }
        for (var i = 0; i < Number(detailsMessage.maxPlayers); i++) {
            detailsMessage.localUserlist[i] = {
                userId: 'empty',
                prohibit: false
            }
        }
        userarray.map((item, index) => {
            detailsMessage.localUserlist[index] = item
        })
    } else if (type == 'error') {
        detailsMessage.localUserlist.map((item, index) => {
            if (item.userId == userarray[0].userId) {
                detailsMessage.localUserlist[index] = userarray[0]
            }
        })
    }
    setTimeout(() => {
        selectfun()
        isallready()
    })
}

function CancelReady() {
    if (detailsMessage.isHost == false && detailsMessage.isReady == true) {
        $('.detail-container-readybtn-in').click()
    }
}

// 选择队伍
$(document).on('click', '.J_chooseTeam', function () {
    var $this = $(this)
    var id = $this.closest('tr').attr('data-id')
    var code = $this.attr('data-code')
    let JsTeam = null
    CancelReady()
    comOption.optionsTeam.map((optionC) => {
        if (code == optionC.teamId) {
            JsTeam = optionC
        }
    })
    $.controlAjax({
        type: "POST",
        url: '/Details/SvaePlayerInfo',
        data: {
            userId: id,
            teamId: code
        },
        showerror: false,
        success: function (res) {
            $this.closest('.gx-menu').find('.gx-menu-in').html(template('detail-user-list-change-script', {
                type: 'teamId',
                item: {
                    'teamId': code,
                    'JsTeam': JsTeam
                }
            }))
            detailsMessage.localUserlist.map((item) => {
                if (item.userId == userInfo.userId) {
                    item.teamId = code
                }
            })
        },
        error: function (req) {

        }
    })



    // setTimeout(() => {
    // ipcRenderer.send('Main', {
    //     msgType: "SavePlayer",
    //     jsonInfo: {
    //         userId: id,
    //         teamId: code
    //     }
    // });
    // }, 300)
})
// 选择颜色
$(document).on('click', '.J_chooseColor', function () {
    var $this = $(this)
    var color = $this.attr('data-color')
    var id = $this.closest('tr').attr('data-id')
    var code = $this.attr('data-code')
    let JsColor = null
    CancelReady()
    comOption.optionsColor.map((optionC) => {
        if (code == optionC.colorId) {
            JsColor = optionC
        }
    })
    $.controlAjax({
        type: "POST",
        url: '/Details/SvaePlayerInfo',
        data: {
            userId: id,
            colorId: code
        },
        showerror: false,
        success: function (res) {
            $this.closest('.gx-menu').html(template('detail-user-list-change-script', {
                type: 'colorId',
                item: {
                    'colorId': code,
                    'JsColor': JsColor
                },
                optionsColor: comOption.optionsColor
            }))
            $('.canChange .color-select').gxmenu({
                height: "30px",
                top: "40px",
                clickHide: true
            }, function (e) {

            })
            detailsMessage.localUserlist.map((item) => {
                if (item.userId == userInfo.userId) {
                    item.colorId = code
                }
            })
        },
        error: function (req) {

        }
    })



    // setTimeout(() => {
    // ipcRenderer.send('Main', {
    //     msgType: "SavePlayer",
    //     jsonInfo: {
    //         userId: id,
    //         colorId: code
    //     }
    // });
    // }, 300)
})
// 选择国家
$(document).on('click', '.J_chooseCountry', function () {
    var $this = $(this)
    var id = $(this).closest('tr').attr('data-id')
    var sideId = $(this).attr('data-code')
    let JsCountry = null
    if (sideId == 10) {
        $('.J_battleSetup').removeClass('hide')
    } else {
        $('.J_battleSetup').addClass('hide')
    }
    if (detailsMessage.isHost == false) {
        if (sideId == 10) {
            $('.detail-user-list-item').each(function () {
                if ($(this).attr('data-id') == userInfo.userId) {
                    $(this).addClass('inlook')
                    $(this).find('.detail-container-ready-img').removeClass('hide')
                    $(this).removeClass('canChange')
                    $(this).addClass('iconfontReady')
                }
            })
            $('.detail-container-readybtn-in').addClass('hide')
            $('.detail-container-readybtn').addClass('hide')
            $('.detail-container-readybtn').closest('div').addClass('hide')
            $('.detail-container-readybtn-inlook').removeClass('hide')
            $.controlAjax({
                type: "get",
                url: '/api/details/game/ready',
                showerror: true,
                data: {
                    status: true
                },
                success: function (res) {
                    if (!detailsMessage.isHost) {
                        detailsMessage.isReady = true
                    }
                },
                error: function (req) {
                    detailsMessage.isReady = false

                    $('.detail-user-list-item').each(function () {
                        if ($(this).attr('data-id') == userInfo.userId) {
                            $(this).removeClass('inlook')
                            $(this).find('.detail-container-ready-img').addClass('hide')
                            $(this).addClass('canChange')
                            $(this).removeClass('iconfontReady')
                        }
                    })
                    $('.detail-container-readybtn-in').removeClass('hide')
                    $('.detail-container-readybtn').removeClass('hide')
                    $('.detail-container-readybtn').closest('div').removeClass('hide')
                    $('.detail-container-readybtn-inlook').addClass('hide')
                }
            })
        } else {
            // if (detailsMessage.isReady == true) {
            $('.detail-user-list-item').each(function () {
                if ($(this).attr('data-id') == userInfo.userId) {
                    $(this).removeClass('inlook')
                    $(this).find('.detail-container-ready-img').addClass('hide')
                    $(this).addClass('canChange')
                    $(this).removeClass('iconfontReady')
                }
            })
            $('.detail-container-readybtn-in').addClass('hide')
            $('.detail-container-readybtn').removeClass('hide')
            $('.detail-container-readybtn').closest('div').removeClass('hide')
            $('.detail-container-readybtn-inlook').addClass('hide')
            $.controlAjax({
                type: "get",
                url: '/api/details/game/ready',
                showerror: true,
                data: {
                    status: false
                },
                success: function (res) {
                    if (!detailsMessage.isHost) {
                        detailsMessage.isReady = false
                    }
                }
            })
            // }
        }
        selectfun()
    }

    detailsMessage.optionsCountry.map((optionC) => {
        if (sideId == optionC.sideId) {
            JsCountry = optionC
        }
    })
    // $this.closest('.gx-menu').find('.gx-menu-in').html(template('detail-user-list-change-script', {
    //     type: 'sideId',
    //     item: {
    //         'sideId': sideId,
    //         'JsCountry': JsCountry
    //     }
    // }))
    detailsMessage.localUserlist.map((item) => {
        if (item.userId == userInfo.userId) {
            item.sideId = sideId
        }
    })
    // setTimeout(() => {
    // ipcRenderer.send('Main', {
    //     msgType: "SavePlayer",
    //     jsonInfo: {
    //         userId: id,
    //         sideId: sideId
    //     }
    // });
    // }, 300)

    $.controlAjax({
        type: "POST",
        url: '/Details/SvaePlayerInfo',
        data: {
            userId: id,
            sideId: sideId
        },
        showerror: false,
        success: function (res) {
            $this.closest('.gx-menu').find('.gx-menu-in').html(template('detail-user-list-change-script', {
                type: 'sideId',
                item: {
                    'sideId': sideId,
                    'JsCountry': JsCountry
                }
            }))
            if (sideId == 10) {
                let changetr = $this.closest('tr')
                // 颜色状态
                changetr.find('.color-select').html(template('detail-user-list-change-script', {
                    type: 'colorId',
                    item: {
                        'colorId': '-1',
                    },
                    optionsColor: comOption.optionsColor
                }))
                // 队伍状态
                changetr.find('.team-select .gx-menu-in').html(template('detail-user-list-change-script', {
                    type: 'teamId',
                    item: {
                        'teamId': '-1',
                    },
                }))
                // 出生点状态
                changetr.find('.birth-select').html(template('detail-user-list-change-script', {
                    type: 'startingLocation',
                    item: {
                        'startingLocation': '-1',
                    },
                    optionsBirth: detailsMessage.optionsBirth,
                }))
            }
        },
        error: function (req) {

        }
    })
})
// 选择出生点
var BirthLoading = false
$(document).on('click', '.J_chooseBirth', function () {
    if (BirthLoading) {
        spop({
            template: translatesrting('请勿频繁切换出生点'),
            autoclose: 2000,
            style: 'warning'
        });
        return false
    }
    BirthLoading = true
    setTimeout(function () {
        BirthLoading = false
    }, 1000)
    var $this = $(this)
    var id = $(this).closest('tr').attr('data-id')
    var codeId = $(this).attr('data-code')
    CancelReady()
    // $this.closest('.gx-menu').find('.Birth-select-div').text(Number(codeId) + 1)
    // $this.closest('.gx-menu').find('.Birth-select-div').attr('data-code', codeId)
    $.controlAjax({
        type: "POST",
        url: '/Details/SvaePlayerInfo',
        data: {
            userId: id,
            startingLocation: codeId
        },
        showerror: false,
        success: function (res) {
            $this.closest('.gx-menu').html(template('detail-user-list-change-script', {
                type: 'startingLocation',
                item: {
                    'startingLocation': Number(codeId),
                },
                optionsBirth: detailsMessage.optionsBirth,
            }))
            detailsMessage.localUserlist.map((item) => {
                if (item.userId == userInfo.userId) {
                    item.startingLocation = Number(codeId)
                }
            })
            $('.canChange .birth-select').gxmenu({
                height: "30px",
                top: "40px",
                clickHide: true
            }, function (e) {

            })
        },
        error: function (req) {

        }
    })
    // setTimeout(() => {
    // ipcRenderer.send('Main', {
    //     msgType: "SavePlayer",
    //     jsonInfo: {
    //         userId: id,
    //         startingLocation: codeId
    //     }
    // });
    // }, 300)
})

// 踢人
$(document).on('click', '.J_signOut', function () {
    var $this = $(this)
    var id = $this.closest('tr').attr('data-id')
    ipcRenderer.send('Main', {
        msgType: "SavePlayer",
        jsonInfo: {
            userId: id,
            hostOperation: 'KickPlayer'
        }
    });
})
// 关闭空位
$(document).on('click', '.J_delMax', function () {
    var $this = $(this)
    if ($this.attr('data-del') == 'robot') {
        var id = $this.closest('tr').attr('data-id')
        ipcRenderer.send('Main', {
            msgType: "SavePlayer",
            jsonInfo: {
                userId: id,
                hostOperation: 'KickPlayer'
            }
        });
    }
    setTimeout(() => {
        detailsMessage.maxPlayers = Number(detailsMessage.maxPlayers) - 1
        scriptUserReal()
        ipcRenderer.send('Main', {
            msgType: "SetGameOption",
            jsonInfo: {
                Key: 'MaxPlayers',
                Value: detailsMessage.maxPlayers
            }
        });
    }, 300)
})
var emptyToAi = false //空转机器 跳动问题
// 关闭->空位
$(document).on('click', '.J_AddMax', function () {
    detailsMessage.maxPlayers = Number(detailsMessage.maxPlayers) + 1
    scriptUserReal()
    ipcRenderer.send('Main', {
        msgType: "SetGameOption",
        jsonInfo: {
            Key: 'MaxPlayers',
            Value: detailsMessage.maxPlayers
        }
    });
    var aILevel = $(this).attr('data-level')
    if (aILevel) {
        setTimeout(() => {
            emptyToAi = true
            indexnum = $('.detail-user-list-item-empty').index($(this).closest('.detail-user-list-item-empty'))
            ipcRenderer.send('Main', {
                msgType: "SavePlayer",
                jsonInfo: {
                    isAI: true,
                    aILevel: aILevel,
                    teamId: 3 //默认D对
                }
            });
        })
    }
})
// 选择机器人等级
$(document).on('click', '.chooseAiLevel', function () {
    var $this = $(this)
    var id = $this.closest('tr').attr('data-id')
    var level = $this.attr('data-level')
    setTimeout(() => {
        ipcRenderer.send('Main', {
            msgType: "SavePlayer",
            jsonInfo: {
                isAI: true,
                userId: id,
                aILevel: level
            }
        });
    }, 300)
})
// 触发本地user去渲染
function scriptUserReal() {
    var reallyUser = []
    detailsMessage.localUserlist.map((item) => {
        if (item.userId != "empty") {
            reallyUser.push(item)
        }
    })
    scriptuser(reallyUser)
}
// 退出房间
$(document).on('click', '.details-all-close-sure', function () {
    if (detailsMessage.isHost) {
        gxmodal({
            title: translatesrting('退出确认'),
            centent: '<div class="mt25">' + translatesrting('您确定要退出当前房间吗？') + '</div>',
            buttons: [{
                text: translatesrting('确定'),
                class: 'btn-middle btn-middle-blue',
                callback: function () {
                    LeaveGameRoom()
                }
            },
            {
                text: translatesrting('取消'),
                class: 'btn-middle btn-style-ash',
            }
            ]
        })
    } else {
        LeaveGameRoom()
    }

})
$(document).on('click', '.details-all-close', function () {
    wsUserStatusdetails(1)
    $('body').css('display', 'none')
    parent.hallMessage.closeRoom('room')
    // ipcRenderer.send('Main', {
    //     msgType: "WindowSize",
    //     jsonInfo: {
    //         window: 'createroom',
    //         size: 'hide'
    //     }
    // });
})

$(document).on('click', '.J_changeMap', function () {
    chooseMapInit()
})
// 使用地图
$(document).on('click', '.J_sureChoose', function () {
    mapDetailChoose()
})

// 显示观战设置
$(document).on('click', '.J_battleSetup', function () {
    // let data = storageBatterMessage(2);
    // if (data) {
    //     batterMessage = data;
    // }
    if (getSaveMessage('batterStorage')) {
        let oldbatterStorage = getSaveMessage('batterStorage')
        if (oldbatterStorage.roomNo == detailsMessage.roomNo) {
            batterMessage.localstorage = oldbatterStorage
        }
    }
    batterMessage.init()
})

// 好友邀请
$(document).on('click', '.J_inviteFriends', function () {
    $('#modalInvite').modal('show')

    var temp_inviteJson_friend = {
        type: '1',
        list: []
    }
    var temp_inviteJson_recently = {
        type: '2',
        list: []
    }
    $.star({
        type: 'GET',
        url: '/community-user/friend/v1/list',
        success: function (res) {
            if (res.data.userFriendList == null) {
                temp_inviteJson_friend.list = []
            } else {
                temp_inviteJson_friend.list = res.data.userFriendList
            }

            $('.Invite-MyFriends').html(template('career-invite-script', { message: temp_inviteJson_friend }))

            if (res.data.recentGamersList == null) {
                temp_inviteJson_recently.list = []
            } else {
                temp_inviteJson_recently.list = res.data.recentGamersList
            }

            $('.Invite-Recently').html(template('career-invite-script', { message: temp_inviteJson_recently }))
        },
        error: function (req) {
        }
    });
})

$(document).on('click', '.Invite-header span', function () {
    $('#modalInvite').modal('hide')
})

// 好友邀请 添加好友
$(document).on('click', '.Invite-content-AddFriends', function () {
    var $this = $(this)
    var Friend_id = $this.attr('data-id')

    $.star({
        type: 'POST',
        url: '/community-user/friend/v1/request',
        showerror: false,
        data: {
            friendId: Friend_id,
            source: '1'
        },
        success: function (res) {
            showtoast({
                message: translatesrting('已发送好友请求')
            })
        },
        error: function (req) {
            if (req.errorCode == 260114) {
                gxmodal({
                    title: translatesrting('提示'),
                    centent: '<div class="mt25">' + translatesrting('好友数量超过上限') + '</div>',
                    buttons: [
                        {
                            text: translatesrting('确定'),
                            class: 'btn-middle btn-middle-blue',
                            callback: function () {

                            }
                        }
                    ]
                })
            } else {
                showtoast({
                    message: req.errorMsg
                })
            }
        }
    });

    $this.css('display', 'none')
    $this.next().css('display', 'block')
})

// 好友邀请 
$(document).on('click', '.Invite-content-check-content', function () {
    var $this = $(this)
    // *-----房间战斗中阻止-----*
    let isingame = false
    $('.detail-container-ingame-img').each(function () {
        if (!$(this).hasClass('hide')) {
            isingame = true
        }
    })
    if (isingame) {
        showtoast({
            message: translatesrting('房间内有人仍在游戏中，无法邀请其他人加入')
        })
        return false
    }
    // *-----房间战斗中阻止-----*
    $this.css({ background: '#888888', cursor: 'default' })
    $this.children(":first").text(translatesrting('已邀请'))

    let temp_send_arr = []
    temp_send_arr.push($this.attr('data-id'))

    $.star({
        type: 'POST',
        url: '/community-user/friend/vi/user/add/room',
        data: {
            roomNo: detailsMessage.roomNo,
            userIds: temp_send_arr,
            invitePeopleName: userInfo.username,
            roomName: detailsMessage.roomName,
            pwd: detailsMessage.pwd
        },
        success: function (res) {
            showtoast({
                message: translatesrting('已成功发送邀请')
            })
        },
        error: function (req) {
        }
    });
})

// 好友邀请 确认邀请
$(document).on('click', '.Invite-button', function () {
    $('#modalInvite').modal('hide')
})

// 切换好友页和最近好友页
$(document).on('click', '.Invite-body-MyFriends-btn', function () {
    $('.Invite-body-MyFriends-btn').addClass('Invite-content-header-focus')
    $('.Invite-MyFriends').addClass('Invite-content-focus')
    $('.Invite-body-Recently-btn').removeClass('Invite-content-header-focus')
    $('.Invite-Recently').removeClass('Invite-content-focus')
})
$(document).on('click', '.Invite-body-Recently-btn', function () {
    $('.Invite-body-MyFriends-btn').removeClass('Invite-content-header-focus')
    $('.Invite-MyFriends').removeClass('Invite-content-focus')
    $('.Invite-body-Recently-btn').addClass('Invite-content-header-focus')
    $('.Invite-Recently').addClass('Invite-content-focus')
})

// 切换游戏设置和游戏版本设置
$(document).on('click', '.detail-option-btn-game', function () {
    $('.detail-option-btn-game').removeClass('detail-option-btn-focus')
    $('.detail-option-btn-version').addClass('detail-option-btn-focus')
    $('.detail-option-content').css('display', 'block')
    $('.detail-option-version-content').css('display', 'none')
})
$(document).on('click', '.detail-option-btn-version', function () {
    $('.detail-option-btn-game').addClass('detail-option-btn-focus')
    $('.detail-option-btn-version').removeClass('detail-option-btn-focus')
    $('.detail-option-content').css('display', 'none')
    $('.detail-option-version-content').css('display', 'flex')
})


function isYuriCountry() {
    $('.J_chooseCountry').each(function () {
        if ($(this).attr('data-code') == 9) {
            if (detailsMessage.gameVersion == 0 || detailsMessage.gameVersion == 3) {
                $(this).css('display', 'block')
            } else {
                $(this).css('display', 'none')
            }
        }
        if ($(this).attr('data-code') == 9) {
            if (detailsMessage.gameVersion == 3) {
                $(this).text(translatesrting('神州'))
            } else {
                $(this).text(translatesrting('尤里'))
            }
        }
    })
}

// 切换红警原版和尤里复仇
$(document).on('click', '.detail-option-version', function () {
    var $this = $(this)
    let gettype = $this.attr('data-type')
    if (!detailsMessage.isHost || detailsMessage.gameVersion == gettype || $(this).find('.detail-option-version-i').hasClass('pointer-events-none-version')) {
        return false;
    } else {
        detailsMessage.gameVersion = gettype
        $('.detail-option-version-content').find('.detail-option-version').each(function () {
            if ($(this).attr('data-type') == detailsMessage.gameVersion) {
                $(this).find('i').css('display', 'block')
                $(this).removeClass('prohibit')
            } else {
                $(this).addClass('prohibit')
                $(this).find('i').css('display', 'none')
            }
        })
        $('.detail-map-img-name').find('.detail-map-img-name-img').each(function () {
            if ($(this).attr('data-type') == detailsMessage.gameVersion) {
                $(this).css('display', 'inline-block')
            } else {
                $(this).css('display', 'none')
            }
        })
        if (detailsMessage.gameVersion == 1) {
            $('.detail-user-list .detail-user-list-item.canChange').each(function () {
                let mychoose = $(this).find('.country-select').find('.gx-menu-in').children().children().text()
                if (mychoose.indexOf(translatesrting('尤里')) == 0 || mychoose.indexOf(translatesrting('神州')) == 0) {
                    $(this).find('.J_chooseCountry')[0].click()
                }
            })
        } else if (detailsMessage.gameVersion == 3) {
            $('.detail-user-list .detail-user-list-item.canChange').each(function () {
                let mychoose = $(this).find('.country-select').find('.gx-menu-in').children().children().text()
                if (mychoose.indexOf(translatesrting('尤里')) == 0) {
                    $(this).find('.J_chooseCountry')[0].click()
                }
            })
        } else if (detailsMessage.gameVersion == 0) {
            $('.detail-user-list .detail-user-list-item.canChange').each(function () {
                let mychoose = $(this).find('.country-select').find('.gx-menu-in').children().children().text()
                if (mychoose.indexOf(translatesrting('神州')) == 0) {
                    $(this).find('.J_chooseCountry')[0].click()
                }
            })
        }
        if (detailsMessage.gameVersion == 3) {
            detailsMessage.optionsCountry.map((item) => {
                if (item.sideId == 9) {
                    item.country = 'Shenzhen'
                    item.cname = '神州'
                }
            })
        } else {
            detailsMessage.optionsCountry.map((item) => {
                if (item.sideId == 9) {
                    item.country = 'Yuri'
                    item.cname = '尤里'
                }
            })
        }
        ipcRenderer.send('Main', {
            msgType: "SetGameOption",
            jsonInfo: {
                Key: 'GameVersion',
                Value: detailsMessage.gameVersion
            }
        });
        isYuriCountry()
    }
})

// 被邀请成功进入房间
function BeInvitedRoom(inviteName, beInvitedName) {
    $('.detail-container-right-chat-list').append('<div class="at-row"><div class="at-col" style="word-spacing:5px">' + translatesrting('玩家') + ' <a style="color: rgb(107, 158, 255);">' + inviteName + '</a> ' + translatesrting('成功邀请') + ' <a style="color: rgb(107, 158, 255);">' + beInvitedName + '</a> ' + translatesrting('进入房间') + '</div></div>')
    $('.detail-container-right-chat-list').scrollTop($('.detail-container-right-chat-list').prop('scrollHeight'))
}

var mapRandomMove = null

function detailsImgSize() {
    clearInterval(mapRandomMove)

    var _image = $('.detail-map-content .detail-map-img-img')
    _image.on('load', function () {
        var realImgWidth = _image.width();
        var realImgHeight = _image.height();
        $('.detail-map-content .detail-map-img-relative').css({ top: '0', left: '0', transition: '8s' })
        if (realImgHeight / realImgWidth >= 280 / 856) {
            $('.detail-map-content .detail-map-img-img').css({
                'width': '100%',
                'height': 'auto'
            })
            $('.detail-map-content .detail-map-img-relative').css({
                'width': 'calc(100% + 100px)',
                'height': 'auto'
            })
        } else {
            $('.detail-map-content .detail-map-img-img').css({
                'width': 'auto',
                'height': '100%'
            })
            $('.detail-map-content .detail-map-img-relative').css({
                'width': 'auto',
                'height': 'calc(100% + 100px)'
            })
        }
        MapMoveFn()
    })
}

var MapMoveFlag = true;

function MapMoveFn() {
    let End_X = $('.detail-map-content .detail-map-img').width() - $('.detail-map-content .detail-map-img-relative').width()
    let End_Y = $('.detail-map-content .detail-map-img').height() - $('.detail-map-content .detail-map-img-relative').height()

    let temp_num = 1
    mapRandomMove = setInterval(() => {
        if (temp_num > 4) {
            temp_num = 1
        }

        switch (temp_num) {
            case 1:
                $('.detail-map-content .detail-map-img-relative').css({ left: End_X, top: End_Y, transition: '8s' })
                break;
            case 2:
                $('.detail-map-content .detail-map-img-relative').css({ left: '0px', top: End_Y, transition: '8s' })
                break;
            case 3:
                $('.detail-map-content .detail-map-img-relative').css({ left: End_X, top: '0px', transition: '8s' })
                break;
            case 4:
                $('.detail-map-content .detail-map-img-relative').css({ left: '0px', top: '0px', transition: '8s' })
                break;
        }

        temp_num++
    }, 8100);
}

$(document).on('mouseenter', '.detail-map-content .detail-map-img-relative', function () {
    $('.detail-map-content .detail-map-img-relative').css({ transition: '0s' })
    clearInterval(mapRandomMove)
})

$(document).on('mouseleave', '.detail-map-content .detail-map-img-relative', function () {
    clearInterval(mapRandomMove)
    if (MapMoveFlag == true) {
        MapMoveFn()
    }
})

// 鼠标拖拽
$(document).on('mousedown', '.detail-map-content .detail-map-img-relative', function (e) {
    var positionDiv = $('.detail-map-content .detail-map-img-relative').offset();
    var distenceX = e.pageX - positionDiv.left;
    var distenceY = e.pageY - positionDiv.top;
    MapMoveFlag = false

    $(document).mousemove(function (e) {
        var x = e.pageX - distenceX - 25
        var y = e.pageY - distenceY - 415;
        $('.detail-map-content .detail-map-img-relative').css({
            'cursor': 'grabbing'
        })

        if (y < 0 && y > $('.detail-map-content .detail-map-img').height() - $('.detail-map-content .detail-map-img-img').height()) {
            $('.detail-map-content .detail-map-img-relative').css({
                'top': y
            })
        }
        if (x <= 0 && x >= $('.detail-map-content .detail-map-img').width() - $('.detail-map-content .detail-map-img-img').width()) {
            $('.detail-map-content .detail-map-img-relative').css({
                'left': x
            })
        }
    })

    $(document).mouseup(function (e) {
        $('.detail-map-content .detail-map-img-relative').css({
            'cursor': 'grab'
        })
        MapMoveFlag = true
        $(document).off('mousemove');
    })
})

// 地图放大
$(document).on('mouseenter', '.detail-map-scale-btn', function () {
    MapMoveFlag = false
    $(document).on('mouseleave', '.detail-map-scale-btn', function () {
        MapMoveFlag = true
    })
})
$(document).on('click', '.detail-map-scale-btn', function () {
    ipcRenderer.send('Main', {
        msgType: "WindowSize",
        jsonInfo: {
            window: 'createroom',
            size: 'max'
        }
    });
    MapMoveFlag = false
    clearInterval(mapRandomMove)
    if ($('#gx-map-modal-birth').length > 0) {
        return false
    }
    $('.details-content').css('opacity', 0)
    var $this = $(this)
    $('body').append('<div class="modal" id="gx-map-modal-birth" data-backdrop="true" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="reportModal"><div class="modal-dialog" data-dismiss="modal" role="document"><div class="detail-map-img-relative relative">' + $('.detail-map-img-relative').html() + '</div></div></div>')
    $('#gx-map-modal-birth .detail-map-img-img').css('width', '100%')
    $('#gx-map-modal-birth .detail-map-img-img').css('height', 'auto')
    $('#gx-map-modal-birth .detail-map-scale-btn').css('display', 'none')
    let maxbili = $('#gx-map-modal-birth').width() / $('#gx-map-modal-birth').height()
    let mapbili = $('.detail-map-content .detail-map-img-img').width() / $('.detail-map-content .detail-map-img-img').height()

    if (maxbili > mapbili) {
        $('#gx-map-modal-birth .modal-dialog').css('margin-top', '0vh')
        $('#gx-map-modal-birth .detail-map-img-relative').css('width', 'auto')
        $('#gx-map-modal-birth .detail-map-img-img').css('width', 'auto')
        $('#gx-map-modal-birth .detail-map-img-img').css('height', '100vh')
    }
    $('#gx-map-modal-birth').one('shown.bs.modal', function () {
        MapMoveFlag = false
    })
    $('#gx-map-modal-birth').modal('show')
    $('#gx-map-modal-birth .detail-map-img-img').attr('data-dismiss', 'modal')
    $('#gx-map-modal-birth').one('hidden.bs.modal', function () {
        $('#gx-map-modal-birth').remove();
        MapMoveFlag = true
    })
    $('#gx-map-modal-birth .detail-map-img-relative').css({ 'transform': 'scale(1)', 'opacity': 1 })
    $('.modal-backdrop.in').css('opacity', 1)
})
$(document).on('hide.bs.modal', '#gx-map-modal-birth', function () {
    MapMoveFn()
    $('.details-content').animate({ 'opacity': 1 })
    $('.modal-backdrop.in').css('opacity', .5)
    ipcRenderer.send('Main', {
        msgType: "WindowSize",
        jsonInfo: {
            window: 'createroom',
            size: 'normal'
        }
    });
    ipcRenderer.send('Main', {
        msgType: "ChangeWindowSize",
        jsonInfo: {
            window: 'createroom',
            width: 1280,
            height: 720,
            isCenter: true
        }
    });
})
// 准备按钮
function changeReadyBtn() {
    if (detailsMessage.isReady) {
        // 已准备
        $('.detail-container-readybtn-in').removeClass('hide')
        $('.detail-container-readybtn').addClass('hide')
        $('.detail-user-list-item').each(function () {
            if ($(this).attr('data-id') == userInfo.userId) {
                $(this).find('.detail-container-ready-img').removeClass('hide')
                $(this).removeClass('canChange')
                $(this).addClass('iconfontReady')
            }
        })
    } else {
        // 为准备
        $('.detail-container-readybtn-in').addClass('hide')
        $('.detail-container-readybtn').removeClass('hide')
        $('.detail-user-list-item').each(function () {
            if ($(this).attr('data-id') == userInfo.userId) {
                $(this).find('.detail-container-ready-img').addClass('hide')
                $(this).addClass('canChange')
                $(this).addClass('iconfontReady')
            }
        })
    }
    $('.detail-container-readybtn-in').gxbtn('reset')
    $('.detail-container-readybtn').gxbtn('reset')
    selectfun()
}
// 进入房间下载地图
function downloadMap(num) {
    let obj = $('.detail-map-img .gx-map-mask')
    obj.css('display', "inline-block")
    var getnum = num
    var shownum = parseInt(obj.find('.gx-map-mask-circle').attr('data-br'))
    if (getnum != shownum) {
        obj.find('.gx-map-mask-progress-br').css('width', getnum + '%')
        obj.find('.gx-map-mask-progress-br').attr('data-br', getnum)
        if (getnum >= 100) {
            setTimeout(function () {
                obj.css('display', "none")
                // $this.removeClass('Match-map-item-inload')
            }, 300)
        }

    }
    // obj.find('.gx-map-mask-circle').text(num + '%')
    // let probabilityEchartsOption = {
    //     title: {
    //         show: true,
    //         text: '',
    //         x: 'center',
    //         y: 'center',
    //         textStyle: {
    //             fontSize: '15',
    //             color: 'white',
    //             fontWeight: 'normal'
    //         }
    //     },
    //     grid: {
    //         top: '0%',
    //         bottom: '0%',
    //     },
    //     tooltip: {
    //         show: false
    //     },
    //     legend: {
    //         show: false
    //     },
    //     series: {
    //         name: '',
    //         type: 'pie',
    //         radius: ['80%', '100%'],
    //         avoidLabelOverlap: true,
    //         hoverAnimation: false,
    //         itemStyle: {
    //             borderRadius: 10
    //         },
    //         label: {
    //             normal: {
    //                 show: false,
    //                 position: 'center'
    //             },
    //             emphasis: {
    //                 show: false
    //             }
    //         },
    //         labelLine: {
    //             normal: {
    //                 show: false
    //             }
    //         },
    //         color: [
    //             new echarts.graphic.LinearGradient(1, 0, 0, 0, [{
    //                 offset: 0,
    //                 color: 'rgba(94, 250, 237, 1)'
    //             }, {
    //                 offset: 1,
    //                 color: 'rgba(63, 121, 238, 1)'
    //             }]),
    //             'rgba(63, 121, 238, 0)'
    //         ],
    //         data: [{
    //             value: num,
    //             name: ''
    //         },
    //         {
    //             value: 100 - num,
    //             name: ''
    //         }
    //         ]
    //     }
    // }
    // if (detailsMessage.probabilityChart) {
    //     detailsMessage.probabilityChart.setOption(probabilityEchartsOption)
    // } else {
    //     detailsMessage.probabilityChart = echarts.init(obj.find('.gx-map-mask-progress')[0]);
    //     detailsMessage.probabilityChart.setOption(probabilityEchartsOption)
    // }
    // if (parseInt(num) >= 100) {
    //     setTimeout(() => {
    //         // detailsMessage.probabilityChart.dispose()
    //         // detailsMessage.probabilityChart = null
    //         obj.css('display', "none")
    //     }, 300)
    // }
}
setTimeout(() => {
    if (detailsMessage.probabilityChart) {
        let obj = $('.detail-map-img .gx-map-mask')
        detailsMessage.probabilityChart = null
        obj.css('display', "none")
    }
}, 30000)
// $('.detail-chathall .gx-menu').gxmenu({
//     height: "33px",
//     clickHide: false
// }, function (e) {

// })
// 大厅邀请
$(document).on('click', '.J_hallInvitation', function () {
    var $this = $(this)
    if ($this.attr('data-loading') == 1) {
        return false
    }
    // *-----房间战斗中阻止-----*
    let isingame = false
    $('.detail-container-ingame-img').each(function () {
        if (!$(this).hasClass('hide')) {
            isingame = true
        }
    })
    if (isingame) {
        showtoast({
            message: translatesrting('房间内有人仍在游戏中，无法邀请其他人加入')
        })
        return false
    }
    // *-----房间战斗中阻止-----*
    // 金币
    let needcoin = $this.attr('data-coin')
    let needcoinAlert = true
    if (needcoin > 0) {
        needcoinAlert = true
    } else {
        needcoinAlert = false
    }
    if (needcoinAlert) {
        if (localStorage.getItem('hallChatInvitday')) {
            let newday = new Date().getDay()
            if (newday != localStorage.getItem('hallChatInvitday')) {
                needcoinAlert = true
            } else {
                needcoinAlert = false
            }
        }
    }
    if (needcoinAlert) {
        gxmodal({
            title: translatesrting('使用金币确认'),
            id: 'hallChatInvitaneedalert',
            centent: '<div class="mt30" style="padding: 0px 22px;">' + translatesrting('您确定使用') + needcoin + translatesrting('金币发送大厅邀请吗？') + '<div><div class="gx-check hallChatInvitaneedalert-div"><input type="checkbox" id="hallChatInvitaneedalertInput"><label for="hallChatInvitaneedalertInput" class="f12"><span>' + translatesrting('今日不再提示') + '</span></label></div>',
            buttons: [
                {
                    text: translatesrting('确定'),
                    class: 'btn-middle btn-middle-blue',
                    callback: function () {
                        hallInvitationloading()
                        $.star({
                            type: 'POST',
                            url: '/battlecenter/redwar/room/v1/send-chat',
                            showerror: false,
                            data: {
                                msg: "",
                                roomNo: detailsMessage.roomNo, //邀请必填
                                inviteStatus: true, //是否是邀请
                            },
                            success: function (res) {
                                parent.getUserGold()
                                showtoast({
                                    message: translatesrting('邀请信息已发送到自定义大厅聊天频道')
                                })
                            },
                            error: function (req) {
                                showtoast({
                                    message: translatesrting(req.errorMsg)
                                })
                            }
                        });
                    }
                },
                {
                    text: translatesrting('取消'),
                    class: 'btn-middle btn-style-ash',
                    callback: function () {
                    }
                }
            ]
        })
    } else {
        hallInvitationloading()
        $.star({
            type: 'POST',
            url: '/battlecenter/redwar/room/v1/send-chat',
            showerror: false,
            data: {
                msg: "",
                roomNo: detailsMessage.roomNo, //邀请必填
                inviteStatus: true, //是否是邀请
            },
            success: function (res) {
                parent.getUserGold()
                showtoast({
                    message: translatesrting('邀请信息已发送到自定义大厅聊天频道')
                })
            },
            error: function (req) {
                showtoast({
                    message: translatesrting(req.errorMsg)
                })
            }
        });
    }
})
$(document).on('change', '#hallChatInvitaneedalertInput', function () {
    let isc = $(this).is(':checked')
    if (isc) {
        let newday = new Date().getDay()
        localStorage.setItem('hallChatInvitday', newday)
    } else {
        localStorage.removeItem('hallChatInvitday', newday)
    }
})
var reloadfun = null
var reloadfunlast = null
var reloadfunlasttwo = null
// 自动关房间
function closeAuto() {
    $.star({
        type: 'GET',
        url: '/battlecenter/platform/config/v1/list',
        showerror: false,
        data: {},
        success: function (res) {
            clearTimeout(reloadfun)
            clearTimeout(reloadfunlast)
            clearTimeout(reloadfunlasttwo)
            comOption.roomConfig = res.data.roomConfig
            if (detailsMessage.isHost && comOption.roomConfig.isOpen == 1) {
                if (detailsMessage.isHavePas && comOption.roomConfig.roomWithPassword == 0) {
                    return false
                }
                reloadfun = setTimeout(function () {
                    wsUserStatusdetails(1)
                    setTimeout(() => {
                        LeaveGameRoom()
                        trackingFunc('room_autoclose')
                        wsUserStatusdetails(1)
                    }, 1000)
                }, comOption.roomConfig.activeInterval * 1000)
                reloadfunlast = setTimeout(function () {
                    var chatmesg = '2' + translatesrting('分钟后未开始游戏，系统将自动解散房间');
                    ipcRenderer.send('Main', {
                        msgType: "ChatMessage",
                        jsonInfo: {
                            msg: chatmesg
                        }
                    });
                    addChatMessage(detailsMessage.isHost, userInfo.username, chatmesg, true)
                }, comOption.roomConfig.activeInterval * 1000 - 120000)

                reloadfunlasttwo = setTimeout(function () {
                    var chatmesg = '1' + translatesrting('分钟后未开始游戏，系统将自动解散房间');
                    ipcRenderer.send('Main', {
                        msgType: "ChatMessage",
                        jsonInfo: {
                            msg: chatmesg
                        }
                    });
                    addChatMessage(detailsMessage.isHost, userInfo.username, chatmesg, true)
                }, comOption.roomConfig.activeInterval * 1000 - 60000)
            }
        },
        error: function (req) { }
    });

}
// 查询房主金币
function queryHostGold() {
    if (Number(detailsMessage.roomGold) > 0) {
        $.star({
            type: 'GET',
            url: '/community-user/account/v1/query',
            showerror: false,
            data: {},
            success: function (res) {
                if (Number(res.data.gold) <= 0) {
                    gxmodal({
                        title: translatesrting('金币不足'),
                        closeCoinHide: true,
                        centent: '<div class="mt30" style="padding:0px 16px">' + translatesrting('您的金币不足,房间已自动解散，请玩一玩匹配或者加入别人的房间一起玩可获得大量的金币哦') + '</div>',
                        buttons: [{
                            text: translatesrting('知道了'),
                            class: 'btn-middle btn-middle-blue',
                            callback: function () {
                                LeaveGameRoom()
                            }
                        }]
                    })
                }
            },
            error: function (req) { }
        });
    }
}

// 复制房间号提示
$(document).on('click', '#roomNo-copy-btn', function () {
    showtoast({
        message: translatesrting('已复制房间号')
    })
})
// 是否在房间判断
function wsUserStatusdetails(type) {
    wsUserStatus(type)
    if (type == 7) {
        localStorage.setItem('isdetails', true);
    } else {
        localStorage.removeItem('isdetails')
    }
}
setInterval(function () {
    if (getSaveMessage('detailsInvite')) {
        friendInvite(getSaveMessage('detailsInvite'))
        localStorage.removeItem('detailsInvite')
    }
}, 1000)

var InviteMessage = {
    list: []
}
function friendInvite(data) {
    if ($('.Invite-gxmodal').length >= 3) {
        $('.Invite-gxmodal').eq(0).remove()
    }

    for (var i = 0; i < InviteMessage.list.length; i++) {
        if (InviteMessage.list[i].invitePeopleName == data.invitePeopleName && InviteMessage.list[i].roomNo == data.roomNo) {
            return false
        }
    }
    if (data.roomNo == detailsMessage.roomNo) {
        return false
    }
    InviteMessage.list.push(data)
    let modalId = "Invite-gxmodal" + InviteMessage.list.length
    let btnHTML = '<a href="javascript:void(0)" ondragstart="return false" id="" class="' + 'btn-middle btn-middle-blue J_veranda_join J_sound_click' + '" data-pwd="' + data.pwd + '" data-dismiss="modal" data-id="' + data.roomNo + '" data-url="/Details/newindex?invitename=' + encodeURI(data.invitePeopleName) + '">' + translatesrting('接受') + '</a>'
    btnHTML = btnHTML + '<a href="javascript:void(0)" ondragstart="return false" id="" class="' + 'btn-middle btn-style-ash' + '"data-dismiss="modal">' + translatesrting('拒绝') + '</a>'
    let titleHtml = '<div class="ft-white-tr f16 text-center">' + translatesrting('组队邀请') + '</div>'
    if (data.remark) {
        var centent = '<div class="mt25">' + data.remark + ' ' + translatesrting('邀请您加入') + ' ' + data.roomName + '</div>'
    } else {
        var centent = '<div class="mt25">' + data.invitePeopleName + ' ' + translatesrting('邀请您加入') + ' ' + data.roomName + '</div>'
    }
    let InviteModal = '<div class="modal gx-gxmodal Invite-gxmodal" data-index="' + (InviteMessage.list.length - 1) + '" id="' + modalId + '" data-keyboard="false" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="ActivateCodeModal"><div class="modal-dialog gx-modal-smell-dialog" role="document"><div class="gx-modal-smell"><div class="gx-modal-smell-header"><div class="header-window"><span class="header-close iconfont ft-ash" data-dismiss="modal">&#xe602;</span></div></div><div class="text-center">' + titleHtml + centent + '</div><div class="gx-gxmodal-foot">' + btnHTML + '</div></div></div></div>'
    $('.friendInvite-content').append(InviteModal)
    $('#' + modalId + '').modal('show')
    $('#' + modalId + '').on('hidden.bs.modal', function () {
        $('#' + modalId + '').remove();
    })
}
$(document).on('hide.bs.modal', '.Invite-gxmodal', function () {
    var $this = $(this)
    InviteMessage.list.splice($this.attr('data-index'), 1)
})
$(document).on('click', '.J_veranda_join.J_sound_click', function () {
    var $this = $(this)
    $this.gxbtn('loading')
    var url = $(this).attr('data-url') || '/Details/newindex'
    var id = $this.attr('data-id')
    var pwd = $this.attr('data-pwd') || ""
    $this.gxbtn('reset')
    let data = {
        url: url,
        id: id,
        pwd: pwd,
    }
    LeaveGameRoom('inviteJoin', data)
})
// 公告
$(document).on('click', '.detail-container-right-notice-btn', function () {
    var $this = $(this)
    let isopen = $this.hasClass('open')
    if (isopen) {
        $('.detail-container-right-notice-btn').removeClass('hide')
        $('.detail-container-right-notice-btn.open').addClass('hide')
        $this.closest('.detail-container-right-notice').css('height', 'auto')
    } else {
        $('.detail-container-right-notice-btn').addClass('hide')
        $('.detail-container-right-notice-btn.open').removeClass('hide')
        $this.closest('.detail-container-right-notice').css('height', '0px')
    }
    noticeHeight()
})
noticeHeight()
function noticeHeight() {
    let height = $('.details-tbody').height()
    height = height - parseInt($('.detail-container-right-chat-bottom').css('paddingTop')) * 2
    if (!$('.detail-container-right-notice').hasClass('hide')) {
        height = height - $('.detail-container-right-notice').height()
        height = height - 34
    } else {
        height = height - 10
    }
    // height = height - $('.detail-container-right-chat-bottom .pt10.text-nowarp').height() + 10
    height = height - 26
    $('.detail-container-right-chat-list').css('height', height)

}
$(document).on('click', '.J_showSetRoomNotice', function () {
    let text = $('.detail-container-right-chat-bottom .J_noticeText').attr('data-text') || ""
    $('#RoomNoticeModal').html(template('RoomNoticeModal-script', { text: text }))
    $('#RoomNoticeModal').modal('show')
})
$(document).on('input', '.RoomNoticeModal-textarea', function () {
    var $this = $(this)
    let num = $this.val().length
    $this.closest('div').find('.RoomNoticeModal-textarea-num').text(num + '/100')
})
$(document).on('click', '#J_RoomNoticeModal-set', function () {
    var $this = $(this)
    $this.gxbtn('loading')
    let text = $this.closest('.RoomNoticeModal-content').find('.RoomNoticeModal-textarea').val() || '空'
    ipcRenderer.send('Main', {
        msgType: "ChatMessage",
        jsonInfo: {
            msg: text,
            noticeType: 5
        }
    });
    $this.gxbtn('reset')
    $('#RoomNoticeModal').modal('hide')
})
function autoRoomNotice() {
    let text = $('.detail-container-right-chat-bottom .J_noticeText').attr('data-text') || ""
    if (text != "") {
        ipcRenderer.send('Main', {
            msgType: "ChatMessage",
            jsonInfo: {
                msg: text,
                noticeType: 5
            }
        });
    }
}

// 网络信息展示
$(document).on('mouseleave', '.details-ping', function () {
    $(this).find('.detail-ping-hover').css('display', 'none')
})

$(document).on('mouseenter', '.details-ping', function () {
    $(this).find('.detail-ping-hover').css('display', 'block')
})

// 离开房间
$(document).on('click', '.detail-container-leave', function () {
    if (detailsMessage.isHost) {
        gxmodal({
            title: translatesrting('退出确认'),
            centent: '<div class="mt25">' + translatesrting('您确定要退出当前房间吗？') + '</div>',
            buttons: [{
                text: translatesrting('确定'),
                class: 'btn-middle btn-middle-blue',
                callback: function () {
                    LeaveGameRoom()
                    trackingFunc('room_hostclose')
                }
            },
            {
                text: translatesrting('取消'),
                class: 'btn-middle btn-style-ash',
            }
            ]
        })
    } else {
        LeaveGameRoom()
    }
})

// 进入房间声音
volumeFn()
$(document).on('click', '.J_detailChoosemapType', function () {
    var $this = $(this)
    let mapGameMode = $this.attr('data-code')
    let label = null
    if (detailsMessage.mapmessage.labels) {
        label = detailsMessage.mapmessage.labels[0]
    }
    ipcRenderer.send('Main', {
        msgType: "ChangeMap",
        jsonInfo: {
            mapName: detailsMessage.mapmessage.name,
            mapGameMode: mapGameMode,
            mapSha1: detailsMessage.mapmessage.mapSha1,
            imageUrl: detailsMessage.mapmessage.imageUrl,
            label: label
        }
    });
})
function LeaveGameRoom(type, data) {
    $.controlAjax({
        type: "get",
        url: '/Details/LeaveGameRoom/',
        data: {},
        showerror: true,
        success: function (res) {
            localStorage.removeItem('msgType10')
            localStorage.removeItem('msgType2')
            if (type == 'inviteJoin') {
                saveMessage('inviteJoin', data)
            }
            $('.details-all-close').click()
        },
        error: function (req) {

        }
    })
}

$(document).on('click', '.room-header-No', function () {
    $this = $(this)
    $this.toggleClass('show')
})
// 进入音效


// 观战
batterMessage.init = function () {
    if (batterMessage.localstorage.modal == 1) {
        batterMessage.localstorage.userList.map((inuser, index) => {
            let init = false
            detailsMessage.localUserlist.map((item) => {
                if (item.userId == inuser.userId) {
                    init = true
                }
            })
            if (!init) {
                batterMessage.localstorage.userList[index] = { userId: null, fraction: '', usermessage: '' }
            }
        })
    }
    $('#BatterModal').find('.modal-dialog').html(template('BatterModal-script', {
        isopen: batterMessage.localstorage.isopen,
        showModaltype: batterMessage.localstorage.showModaltype,
        Bonumber: batterMessage.localstorage.Bonumber,
        userList: batterMessage.localstorage.userList,
        teamList: batterMessage.localstorage.teamList,
        modal: batterMessage.localstorage.modal,
        showModal: batterMessage.showModal,
    }))
    $('#BatterModal [data-toggle="tooltip"]').tooltip()
    $('#BatterModal .gx-menu').gxmenu({
        height: "30px",
        top: '20px',
        clickHide: true,
    }, function (e) {

    })
    $('#BatterModal').modal('show')
}
batterMessage.userfun = function (obj) {
    if (batterMessage.localstorage.modal == 1) {
        let selectUser = []
        detailsMessage.localUserlist.map((item) => {
            if (item.userId != 'empty' && !item.isAI && item.sideId != 10 && item.userId != userInfo.userId) {
                selectUser.push(item)
            }
        })
        let selectUserold = []
        for (var i = 0; i < selectUser.length; i++) {
            var init = false
            for (var z = 0; z < batterMessage.localstorage.userList.length; z++) {
                if (batterMessage.localstorage.userList[z].userId == selectUser[i].userId) {
                    init = true
                }
            }
            if (!init) {
                selectUserold.push(selectUser[i])
            }
        }

        $('.BatterModal-gxmenu-li').html(template('BatterModal-gxmenu-li-script', { userlist: selectUserold }))
        $('#BatterModal .gx-menu').gxmenu({
            height: "30px",
            top: '20px',
            clickHide: true,
        }, function (e) {

        })
        obj.click()
    } else {
        let selectUser = []
        detailsMessage.localUserlist.map((item) => {
            if (item.userId != 'empty' && !item.isAI && item.sideId != 10 && item.userId != userInfo.userId) {
                selectUser.push(item)
            }
        })
        let teamarray = []
        for (var i = 0; i < selectUser.length; i++) {
            for (var z = 0; z < comOption.optionsTeam.length; z++) {
                if (selectUser[i].teamId == comOption.optionsTeam[z].teamId) {
                    let init = false
                    for (var y = 0; y < teamarray.length; y++) {
                        if (teamarray[y].name == comOption.optionsTeam[z].name) {
                            init = true
                        }
                    }
                    if (!init) {
                        teamarray.push({ name: comOption.optionsTeam[z].name, teamId: comOption.optionsTeam[z].teamId })
                    }

                }
            }
        }
        let teamarrayold = []
        for (var i = 0; i < teamarray.length; i++) {
            var init = false
            for (var z = 0; z < batterMessage.localstorage.teamList.length; z++) {
                if (batterMessage.localstorage.teamList[z].teamId == teamarray[i].teamId) {
                    init = true
                }
            }
            if (!init) {
                teamarrayold.push(teamarray[i])
            }
        }
        for (var i = 0; i < teamarrayold.length; i++) {
            if (teamarrayold[i].teamId == -1) {
                teamarrayold.splice(i, 1)
            }
        }
        $('.BatterModal-gxmenu-li').html(template('BatterModal-gxmenu-li-script', { teamarray: teamarrayold, type: 'team' }))
        $('#BatterModal .gx-menu').gxmenu({
            height: "30px",
            top: '20px',
            clickHide: true,
        }, function (e) {

        })
        obj.click()
    }
}

// 观战启动开关
$(document).on('change', ".J_BatterModalopen", function () {
    var $this = $(this)
    if ($this.is(':checked')) {
        batterMessage.localstorage.isopen = true
    } else {
        batterMessage.localstorage.isopen = false
    }
})
// 选展示模式
$(document).on('click', ".J_BatterModal_modal", function () {
    var $this = $(this)
    let modaltype = $this.attr('data-type')
    batterMessage.localstorage.modal = modaltype
    $this.closest('.BatterModal-gxmenu').find('.BatterModal-gxmenu-text').html(
        '<span>' + modaltype + '</span><span> VS </span><span>' + modaltype + '</span>'
    )
    if (batterMessage.localstorage.modal == 1) {
        $('.player_1 .BatterModal-gxmenu').find('.BatterModal-gxmenu-text').text(translatesrting("选择队员"))
        $('.player_2 .BatterModal-gxmenu').find('.BatterModal-gxmenu-text').text(translatesrting("选择队员"))
    } else {
        $('.player_1 .BatterModal-gxmenu').find('.BatterModal-gxmenu-text').text(translatesrting("选择队伍"))
        $('.player_2 .BatterModal-gxmenu').find('.BatterModal-gxmenu-text').text(translatesrting("选择队伍"))
    }
    $('.J_BatterModal-playnum').val('')
    batterMessage.localstorage.userList = [
        { userId: null, fraction: '', usermessage: '' },
        { userId: null, fraction: '', usermessage: '' },
    ]
    batterMessage.localstorage.teamList = [
        { teamId: null, name: '', fraction: '', userList: [] },
        { teamId: null, name: '', fraction: '', userList: [] },
    ]
})
// 选择比分展示模式
$(document).on('click', ".J_BatterModal_showmodal", function () {
    var $this = $(this)
    let showtype = $this.attr('data-type')
    batterMessage.localstorage.showModaltype = showtype
    $this.closest('.BatterModal-gxmenu').find('.BatterModal-gxmenu-text').text($this.text())
    if (showtype == 1) {
        $this.closest('.BatterModal-p').find('.BatterModal-numinput').removeClass('hide')
        $this.closest('.BatterModal-p').find('.BatterModal-numinput').focus()
        $('.J_BatterModal-playnum').each(function () {
            if (Number($(this).val()) > 13) {
                $(this).val(13).trigger('input');
            }
        })
    } else {
        $this.closest('.BatterModal-p').find('.BatterModal-numinput').addClass('hide')
    }
})
// 填入bo数
$(document).on('input', ".J_BatterModal_Bonumber", function () {
    var $this = $(this)
    let num = $this.val()
    num = num.replace(/[^\d]/g, '')
    if (Number(num) > 25) {
        num = 25
    }
    $this.val(num)
    batterMessage.localstorage.Bonumber = num

})
// 保存
$(document).on('click', '#BatterModal .J_submit', function () {
    if (batterMessage.localstorage.isopen) {
        if (batterMessage.localstorage.showModaltype == 1 && batterMessage.localstorage.Bonumber == '') {
            showtoast({
                message: translatesrting('请填写BO场次')
            })
            return false
        }
        if (batterMessage.localstorage.modal == 1 && (batterMessage.localstorage.userList[0].userId == null || batterMessage.localstorage.userList[1].userId == null)) {
            showtoast({
                message: translatesrting('请填写选手')
            })
            return false
        }
        if (batterMessage.localstorage.modal == 2 && (batterMessage.localstorage.teamList[0].teamId == null || batterMessage.localstorage.teamList[1].teamId == null)) {
            showtoast({
                message: translatesrting('请填写队伍')
            })
            return false
        }
        if (batterMessage.localstorage.showModaltype == 1) {
            if (batterMessage.localstorage.Bonumber % 2 == 0) {
                showtoast({
                    message: translatesrting('BO请填写奇数')
                })
                return false
            }
        }
    }
    batterMessage.localstorage.roomNo = detailsMessage.roomNo
    batterMessage.localstorage.mapName = detailsMessage.mapmessage ? detailsMessage.mapmessage.name : ''
    batterMessage.localstorage.version = detailsMessage.gameVersion
    saveMessage('batterStorage', batterMessage.localstorage)
    $('#BatterModal').modal('hide')
    let takeopen = false
    if (batterMessage.localstorage.isopen) {
        takeopen = true
    }
    $.controlAjax({
        type: "POST",
        url: '/api/site/spectator/plugin',
        data: {
            open: takeopen
        },
        success: function (res) {

        },
        error: function (req) {

        }
    })
    if (takeopen) {
        setoverwindow()
    }
})

// 取消
$(document).on('click', '#BatterModal .J_hideModel', function () {
    $('#BatterModal').modal('hide')
})

// 获取用户
$(document).on('click', '.BatterModal-gxmenu .BatterModal-gxmenu-click', function () {
    var obj = $(this).closest('.BatterModal-gxmenu').find('.gx-menu-in')
    batterMessage.userfun(obj)
})
// 选择角色
$(document).on('click', '.J_BatterModal-chooseuser', function () {
    var $this = $(this);
    var text = $this.text();
    $this.closest('.BatterModal-gxmenu').find('.BatterModal-gxmenu-text').text(text)
    let id = $this.attr('data-id')
    let type = $this.closest('.J_gxmenu-li').attr('data-type')
    detailsMessage.localUserlist.map((item) => {
        if (id == item.userId) {
            batterMessage.localstorage.userList[type].userId = item.userId
            batterMessage.localstorage.userList[type].usermessage = item
        }
    })
})
// 选择队伍
$(document).on('click', '.J_BatterModal-chooseteam', function () {
    var $this = $(this);
    var text = $this.text();
    $this.closest('.BatterModal-gxmenu').find('.BatterModal-gxmenu-text').text(text)
    let id = $this.attr('data-id')
    let type = $this.closest('.J_gxmenu-li').attr('data-type')
    batterMessage.localstorage.teamList[type].teamId = id
    batterMessage.localstorage.teamList[type].name = text
    detailsMessage.localUserlist.map((item) => {
        if (id == item.teamId) {
            batterMessage.localstorage.teamList[type].userList.push(item)
        }
    })
})

// 填写比分
$(document).on('input', ".J_BatterModal-playnum", function () {
    var $this = $(this)
    let num = $this.val()
    num = num.replace(/[^\d]/g, '')
    if (batterMessage.localstorage.showModaltype == 1 && num > 13) {
        num = 13
    }
    $this.val(num)
    let type = $this.attr('data-type')
    if (batterMessage.localstorage.modal == 2) {
        batterMessage.localstorage.teamList[type].fraction = num
    } else {
        batterMessage.localstorage.userList[type].fraction = num
    }
})
function checkWatch(type) {
    if (getSaveMessage('batterStorage')) {
        let oldbatterStorage = getSaveMessage('batterStorage')
        if (oldbatterStorage.roomNo == detailsMessage.roomNo) {
            batterMessage.localstorage = oldbatterStorage
        }
    }
    if (batterMessage.localstorage.isopen) {
        var canopen = true
        if (batterMessage.localstorage.modal == 2) {
            // 队伍
            for (var i = 0; i < batterMessage.localstorage.teamList.length; i++) {
                batterMessage.localstorage.teamList[i].userList = []
                detailsMessage.localUserlist.map((item) => {
                    if (item.teamId == batterMessage.localstorage.teamList[i].teamId) {
                        batterMessage.localstorage.teamList[i].userList.push(item)
                    }
                })
            }
            for (var i = 0; i < batterMessage.localstorage.teamList.length; i++) {
                if (batterMessage.localstorage.teamList[i].userList.length <= 1 || batterMessage.localstorage.teamList[i].userList.length > 2) {
                    canopen = false
                }
            }
        } else {
            // 人
            for (var i = 0; i < batterMessage.localstorage.userList.length; i++) {
                let init = false
                detailsMessage.localUserlist.map((item) => {
                    if (item.userId == batterMessage.localstorage.userList[i].userId && item.sideId != 10) {
                        init = true
                    }
                })
                if (!init) {
                    canopen = false
                }
            }
        }
        let init = false
        detailsMessage.localUserlist.map((item) => {
            if (item.isAI) {
                init = true
            }
        })
        if (init) {
            canopen = false
        }
        $.controlAjax({
            type: "POST",
            url: '/api/site/spectator/plugin',
            data: {
                open: canopen
            },
            success: function (res) {

            },
            error: function (req) {

            }
        })
        if (canopen) {
            setoverwindow()
        }
    } else {
        $.controlAjax({
            type: "POST",
            url: '/api/site/spectator/plugin',
            data: {
                open: false
            },
            success: function (res) {

            },
            error: function (req) {

            }
        })
    }
    batterMessage.localstorage.roomNo = detailsMessage.roomNo
    batterMessage.localstorage.mapName = detailsMessage.mapmessage ? detailsMessage.mapmessage.name : ''
    batterMessage.localstorage.version = detailsMessage.gameVersion
    saveMessage('batterStorage', batterMessage.localstorage)
}

function setoverwindow() {
    $.controlAjax({
        type: "get",
        url: '/api/site/settings/info/',
        success: function (ress) {
            if (ress.data.windowMode == 'BorderlessWindow') {
                return false
            }
            let sendarray = {
                gamePath: ress.data.GamePath,
                gameScreen: ress.data.gameScreen,
                windowMode: 'BorderlessWindow',
                renderMode: ress.data.renderMode,
                singleCpu: ress.data.singleCpu,
                scrollRate: ress.data.scrollRate,
                skipScoreScreen: ress.data.skipScoreScreen,
                tooltips: ress.data.tooltips,
                showHiddenObjects: ress.data.showHiddenObjects,
                targetLines: ress.data.targetLines,
            }
            $.controlAjax({
                type: "POST",
                url: '/api/site/game/personalize',
                showerror: false,
                data: sendarray,
                success: function (res) {

                },
                error: function (req) {

                }
            })
        },
        error: function (req) {

        }
    })
}
function showCoinChat(type) {
    $.star({
        type: 'GET',
        url: '/battlecenter/platform/config/gold/v1/list',
        showerror: false,
        data: {
        },
        success: function (res) {
            $('.detail-chathall').html(template('detail-chathall-script', { coin: res.data.userSendHallInviteGold }))
            $('.detail-chathall').removeClass('hide')
        },
        error: function (req) { }
    });
}
function hallInvitationloading(callback) {
    var $this = $('.J_hallInvitation')
    $this.attr('data-loading', 1)
    $this.find('.detail-chathall-a').addClass('hide')
    var chatlistloading = $this.find('.detail-chathall-loading')
    chatlistloading.removeClass('hide')
    var chatlistloadingtime = 10
    var chatlistloadingfun = setInterval(function () {
        chatlistloadingtime = chatlistloadingtime - 1
        chatlistloading.text($this.find('.detail-chathall-a').attr('data-text') + '(' + chatlistloadingtime + ')')
        if (chatlistloadingtime == 0) {
            showCoinChat()
            $this.attr('data-loading', 2)
            clearInterval(chatlistloadingfun)
            chatlistloading.addClass('hide')
            $this.find('.detail-chathall-a').removeClass('hide')
            chatlistloading.text($this.find('.detail-chathall-a').attr('data-text') + '(' + 10 + ')')
        }
    }, 1000)
}
function dellDetailImage(imageUrl) {
    oneImgCache(imageUrl).then((Cachereturn) => {
        $('.detail-map-img-img').attr('src', Cachereturn)
        $('.detail-map-img').attr('data-map', Cachereturn)
    })
}