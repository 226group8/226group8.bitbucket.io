// 翻译
// $('.battle-container').html(template('battle-container-script'))


var comOption = {
    optionsCountry: [{
        sideId: 10,
        country: '',
        cname: '观战'
    },
    {
        sideId: -1,
        country: ' ',
        cname: '随机'
    },
    {
        sideId: 2,
        country: 'France',
        cname: '法国',
        url: 'France.png'
    },
    {
        sideId: 6,
        country: 'Iraq',
        cname: '伊拉克',
        url: 'Iraq.png'
    },
    {
        sideId: 1,
        country: 'Korea',
        cname: '韩国',
        url: 'korea.png'
    },
    {
        sideId: 5,
        country: 'Libya',
        cname: '利比亚',
        url: 'Libya.png'
    },
    {
        sideId: 0,
        country: 'America',
        cname: '美国',
        url: 'USA.png'
    },
    {
        sideId: 7,
        country: 'Cuba',
        cname: '古巴',
        url: 'cuba.png'
    },
    {
        sideId: 3,
        country: 'Germany',
        cname: '德国',
        url: 'Germany.png'
    },
    {
        sideId: 8,
        country: 'Russia',
        cname: '苏俄',
        url: 'russia.png'
    },
    {
        sideId: 4,
        country: 'GreatBritain',
        cname: '英国',
        url: 'GreatBritain.png'
    },
    {
        sideId: 9,
        country: 'Yuri',
        cname: '尤里',
        url: 'Yuri.png'
    },
    ],
    optionsColor: [{
        colorId: -1,
        color: '',
        name: '随机'
    },
    {
        colorId: 0,
        color: '222,227,8',
        name: 'Gold'
    },
    {
        colorId: 1,
        color: '255,24,24',
        name: 'Red'
    },
    {
        colorId: 2,
        color: '41,117,231',
        name: 'Teal'
    },
    {
        colorId: 3,
        color: '57,211,41',
        name: 'Green'
    },
    {
        colorId: 4,
        color: '255,162,24',
        name: 'Orange'
    },
    {
        colorId: 5,
        color: '49,215,231',
        name: 'Blue'
    },
    {
        colorId: 6,
        color: '148,40,189',
        name: 'Purple'
    },
    {
        colorId: 7,
        color: '255,154,239',
        name: 'Metalic'
    }
    ],
    optionsTeam: [{
        teamId: -1,
        name: '单人'
    },
    {
        teamId: 0,
        name: 'A队'
    },
    {
        teamId: 1,
        name: 'B队'
    },
    {
        teamId: 2,
        name: 'C队'
    },
    {
        teamId: 3,
        name: 'D队'
    },
    ], //队伍配置
    optionsMode: [{
        name: '作战',
        code: 'Battle',
        dec: '作战模式下可建造所有单位。但热门官方地图北极圈在小队结盟模式下'
    },
    {
        name: '邪恶联盟',
        code: 'Unholy Alliance',
        dec: '邪恶联盟模式下，每位玩家都拥有苏/盟两国基地车（YR模式下还有尤里基地车），可以建筑除特色兵种外的相应建筑和兵种'
    },
    {
        name: '海战',
        code: 'Naval War',
        dec: '海战模式下，您不能够建造陆军和空军单位。'
    },
    {
        name: '生死斗',
        code: 'Meat Grinder',
        dec: '生死斗模式下，玩家只可以建造基本的陆战单位和国家特色单位。不能建造作战实验室。天启坦克/光棱坦克等。'
    },
    {
        name: '抢地盘',
        code: 'Land Rush',
        dec: '抢地盘模式下，玩家出生在一起。需要将基地车移动到合适的位置来发育。'
    },
    {
        name: '巨富',
        code: 'Megawealth',
        dec: '巨富模式下，玩家不能建造矿场和采矿车，需要用工程师占领油井来获取经济。'
    },
    {
        name: '抢箱子',
        code: 'PickBox',
        dec: '捡箱子模式下，您只能用初始部队捡起地图上随机出现的箱子获得作战单位，不能建造和生产'
    },
    {
        name: '闪击战',
        code: 'Blitz',
        dec: '闪击战使用特殊的小型地图，苏军或盟军将共享科技，尤里科技为苏军科技。玩家需要建造最多 12 个油井来获取经济。模式作者: RA2CashGames'
    }
    ], //mode配置
    labelArray: [{
        name: '全部',
        code: ''
    },
    {
        name: '经典对战',
        code: 'Battle'
    },
    {
        name: '娱乐对战',
        code: 'Recreation'
    },
    {
        name: '任务',
        code: 'Task'
    },
    {
        name: '防守',
        code: 'Defense'
    },
    ],
    settingArray: [{
        name: '可摧毁桥梁',
        code: 'BridgeDestroy'
    },
    {
        name: '超级武器',
        code: 'Superweapons'
    },
    {
        name: '工具箱',
        code: 'Crates'
    },
    {
        name: '基地重新部署',
        code: 'MCVRedeploy'
    },
    {
        name: '快速游戏',
        code: 'ShortGame'
    },
    ],
    optionsLevels: [],
    optionsAvatar: [],
    optionscomplaint: [],
    guideCode: {
        newHelp: '1'
    }, //引导
    roomConfig: null,
    isinitefullscript: false
}

// 默认排序
const deflutSlot = {
    // 盟军
    allied: ["GTGCAN", "GAREFN", "BFRT", "FV", "GGI", "MGTK", "ROBO", "SREF", "SPY", "CLEG",
        "SHAD", "TANY", "ORCA", "BEAG", "SNIPE", "GHOST", "TNKD"
    ],
    // 苏军
    soviet: ["DRON", "HTK", "SCHP", "DESO", "IVAN", "YURI", "SHK", "FLAKT", "V3", "APOC", "ZEP", "SCHP",
        "SQD"
    ],
    // 尤里
    Yuri: [],
    // 神州
    shenzhou: ["CHAR"],
}

// 同类型内的优先级
const sameType = [
    ['CMIN', 'HARV', 'YAREFN', 'SMIN', 'CHAR'], //矿车
    ['MTNK', 'HTNK', 'LTNK'], //灰熊, 犀牛, 狂风
    ['ADOG', 'DOG'], //狗
    ['E1', 'E2', 'INIT', 'PLA'], //大兵
    ['GAWEAP', 'NAWEAP', 'YAWEAP', 'CAWEAP'], //重工
    ['ENGINEER', 'SENGINEER', 'YENGINEER'], //工程师
    ['JUMPJET'],//飞行兵
    ['LCRF', 'SAPC', 'YHVR', 'CAPC'], //运输艇
    ['DEST', 'SUB', 'BSUB', 'DEST2', 'SUB2'], //驱逐舰, 潜艇
    ['AEGIS', 'HYD'], //防空船
    ['CARRIER', 'DRED', 'CARRIER2'], //航母, 无畏
    ['DLPH'] //海豚
]

// 白名单
const whitelist = {
    // 盟军
    allied: [
        'E1', 'SNIPE', 'JUMPJET', 'SPY', 'GHOST',
        'TANY', 'CLEG', 'ADOG', 'GGI', 'MTNK',
        'FV', 'MGTK', 'SREF', 'BFRT', 'LCRF',
        'DEST', 'DLPH', 'AEGIS', 'CARRIER', 'CMIN',
        'ROBO', 'TNKD', 'SHAD', 'ORCA', 'BEAG',
        'GAWEAP', 'GTGCAN', 'ORCA2'
    ],
    // 苏军
    soviet: [
        'E2', 'FLAKT', 'SHK', 'SENGINEER', 'DOG',
        'IVAN', 'BORIS', 'TERROR', 'DESO', 'DRON',
        'HTK', 'HTNK', 'V3', 'APOC', 'SAPC',
        'HYD', 'SUB', 'SQD', 'DRED', 'HARV',
        'TTNK', 'DTRUCK', 'SCHP', 'ZEP', 'NAWEAP', 'YURI', 'HIND'
    ],
    // 尤里
    Yuri: [
        'INIT', 'YENGINEER', 'BRUTE', 'VIRUS', 'YURI',
        'YURIPR', 'YTNK', 'TELE', 'MIND',
        'DISK', 'CAOS', 'YHVR', 'BSUB', 'SMIN',
        'YAREFN', 'YAWEAP', 'LTNK',
    ],
    // 神州
    shenzhou: [
        'PLA', 'GHOST2', 'HOVI', 'LTNK', 'BGGY', 'HOWI', 'CMCV', 'DEST2', 'SUB2', 'CAPC', 'CDLPH', 'MBOAT', 'CARRIER2', 'CHAR', 'J10', 'CAWEAP'
    ],
}


new Vue({
    el: '#app',
    data: {
        tabs: [{
            title: '资源采集对比',
            id: 0,
            key: "Mine",
            echarts_data: {
                player_1_data: [],
                player_1_line: [],
                player_2_data: [],
                player_2_line: [],
            }
        }, {
            title: '消费对比',
            id: 1,
            key: "Consume",
            echarts_data: {
                player_1_data: [],
                player_1_line: [],
                player_2_data: [],
                player_2_line: [],
            }
        }, {
            title: '当前经济对比',
            id: 2,
            key: "Cash",
            echarts_data: {
                player_1_data: [],
                player_1_line: [],
                player_2_data: [],
                player_2_line: [],
            }
        },
            // {
            //     title: '生产时间轴',
            //     id: 4,
            //     key: "Mine",
            //     echarts_data: {
            //         player_1_data: [],
            //         player_1_line: [],
            //         player_2_data: [],
            //         player_2_line: [],
            //     }
            // },
            // {
            //     title: 'APM',
            //     id: 5,
            //     key: "APM",
            //     echarts_data: {
            //         player_1_data: [],
            //         player_1_line: [],
            //         player_2_data: [],
            //         player_2_line: [],
            //     }
            // }
        ],
        tab_active: 0,
        echarts_data: [],
        player_1: {},//用户游戏数据
        player_2: {},//用户游戏数据
        player_1Message: {},//用户携带信息
        player_2Message: {},//用户携带信息
        player_1Number: 0,//用户携带分数
        player_2Number: 0,//用户携带分数
        player_1array: [],//组队信息
        player_2array: [],//组队信息
        base_data: {},
        player_indexs: [],
        time_axis: [],
        beishu: 1,
        isIgnoreMouseEvents: 1,//是否穿透 1穿透 0不
        isIgnoreMouseEventsHide: 1,//是否消失 0不消失 1消失
        isconfusion: false,//是否混战
        windowsize: {//窗口参数
            winwidth: null,
            winheight: null,
            potX: null,
            potY: null,
        },
        batterStorage: null,//传递够来参数
        modaltype: 1,//1 1V1 2 2V2
        myChart: null,
        beilv: 1,
        oldUserList: [],
        translatesrting: [
            '生产队列',
            '/images/Battle/logo2.png'
        ],
        chatMessageTop: [
        ],
        chatMessageDown: [
        ],
        dumuType: localStorage.getItem('Ra_dumuType') || 1,
        colorNum: [
            { colorId: -1, color: '##899ACB', name: 'OB', cname: 'OB' },
            { colorId: 0, color: '#FFC438', name: 'Gold', cname: '黄色' },
            { colorId: 1, color: '#FF4444', name: 'Red', cname: '红色' },
            { colorId: 2, color: '#6B9EFF', name: 'Teal', cname: '蓝色' },
            { colorId: 3, color: '#A2FF33', name: 'Green', cname: '绿色' },
            { colorId: 4, color: '#FF8247', name: 'Orange', cname: '橙色' },
            { colorId: 5, color: '#6BEBFF', name: 'Blue', cname: '青色' },
            { colorId: 6, color: '#D23CFD', name: 'Purple', cname: '紫色' },
            { colorId: 7, color: '#FF4E9B', name: 'Metalic', cname: '粉色' }
        ],
        hideChat: null,
        testway: 1
    },
    computed: {
        game_time() {
            return function (ts) {
                let s = ts || this.base_data.time
                var h;
                h = Math.floor(s / 60);
                s = s % 60;
                h += '';
                s += '';
                h = (h.length == 1) ? '0' + h : h;
                s = (s.length == 1) ? '0' + s : s;
                return h + ':' + s;
            }
        },
        leftDistance() {
            return function (time) {
                return time / this.base_data.time * 97
            }
        },
        axistimeLeftDistance() {
            return function (time) {
                return (time / this.base_data.time) * 97;
            }
        }
    },
    watch: {
        "base_data.time": {
            handler: function () {
                if (this.time_axis.length >= 16) {
                    if (this.beishu == 1) {
                        this.beishu++
                    } else {
                        this.beishu = this.beishu * 2
                    }
                    this.time_axis = this.time_axis.filter((item) => {
                        return item % this.beishu == 0;
                    });
                }
            },
            deep: true
        },
    },
    created() {
        if (localStorage.getItem('LBWlanguage') && localStorage.getItem('LBWlanguage') != 'zh') {
            this.translatesrting = [
                'Production',
                '/images/hall/hallLogoen.png'
            ]
        }
        this.batterStorage = JSON.parse(localStorage.getItem('batterStorage'));
        if (this.batterStorage.version == 3) {
            comOption.optionsCountry[11].url = 'shenzhou.png'
        }
        if (this.batterStorage.showModaltype == 3) {
            if (this.batterStorage.modal == 1) {
                this.batterStorage.userList.map((item) => {
                    item.fraction = ''
                })
            } else {
                this.batterStorage.teamList.map((item) => {
                    item.fraction = ''
                })
            }
        }
        this.getnickName()
        this.init();
    },
    mounted() {
        // this.switchEchartsData()
        setTimeout(() => {
            ipcRenderer.send('Main', {
                msgType: "WindowSize",
                jsonInfo: {
                    window: 'BattleModal',
                    size: 'alwaysOnTop'
                }
            });
        }, 2000)
    },
    methods: {
        init() {
            var that = this
            setInterval(() => {
                ipcRenderer.send('Heartbeat', 1);
            }, 5000)
            var option = {
                container: "#danmu", //弹幕墙的id
                barrageLen: 15 //弹幕的行数
            }
            barrageWall.init(option);
            ipcRenderer.on('WebIpc', (event, message) => {
                message.jsonInfo = JSON.parse(message.jsonInfo)
                if (message.msgType == 22 && message.jsonInfo.TypeCode == 99) {
                    $.star({
                        type: 'POST',
                        url: '/battlecenter/redalert/game/chat/v1/filter',
                        showerror: false,
                        data: {
                            word: `${message.jsonInfo.Message}`,
                            replaceWord: `*`,
                            userId: message.jsonInfo.SenderId
                        },
                        success: function (res) {
                            if (res.data) {
                                if (message.jsonInfo.Color == '-1') {
                                    message.jsonInfo.SenderName = '(OB)' + message.jsonInfo.SenderName
                                }
                                that.getChat(message.jsonInfo.SenderName, res.data, message.jsonInfo.Color)

                            }
                        },
                        error: function (req) {

                        }
                    });
                }
                if (message.msgType == 43) {
                    let getwei = {
                        Top: message.jsonInfo.Top,
                        Bottom: message.jsonInfo.Bottom,
                        Left: message.jsonInfo.Left,
                        Right: message.jsonInfo.Right
                    }
                    this.getwei(getwei)
                }
                // console.log(event, message)
            });
            $.controlAjax({
                type: "get",
                url: '/api/site/settings/info/',
                showerror: true,
                success: function (res) {
                    that.getsize(res)
                },
                error: function (req) {

                }
            })
            ipcRenderer.send('Main', {
                msgType: "WindowSize",
                jsonInfo: {
                    window: 'BattleModal',
                    size: 'setIgnoreMouseEvents'
                }
            });
            this.getData();
            setInterval(() => {
                this.getData();
            }, 500)
        },
        // 切换图表数据
        onTab(item, index) {
            // this.tab_active = item.id;
            // this.switchEchartsData();
        },
        ontop() {
            ipcRenderer.send('Main', {
                msgType: "WindowSize",
                jsonInfo: {
                    window: 'BattleModal',
                    size: 'show'
                }
            });
        },
        getChat(name, message, colornum) {
            let color = '#000'
            this.colorNum.map((item) => {
                if (colornum == item.colorId) {
                    color = item.color
                }
            })
            if (this.dumuType == 1) {
                clearTimeout(this.hideChat)
                this.hideChat = setTimeout(() => {
                    $('.chat-top-div .chat-item.action').animate({ opacity: '0', top: '10px' }, () => {
                        this.chatMessageTop = []
                    })
                }, 15000)
                this.chatMessageTop.push({ name, message, color })
                if (this.chatMessageTop.length > 5) {
                    let outitem = $('.chat-top-div .chat-item.action').eq(0)
                    outitem.removeClass('action')
                    outitem.animate({ opacity: '0', top: '10px' }, () => {
                        outitem.remove()
                    })
                }
            } else if (this.dumuType == 2) {
                barrageWall.upWall('', name, message, color)
            } else if (this.dumuType == 3) {
                clearTimeout(this.hideChat)
                this.hideChat = setTimeout(() => {
                    $('.chat-top-div .chat-item.action').animate({ opacity: '0', top: '-10px' }, () => {
                        this.chatMessageDown = []
                    })
                }, 15000)
                this.chatMessageDown.push({ name, message, color })
                if (this.chatMessageDown.length > 5) {
                    let outitem = $('.chat-down-div .chat-item.action').eq(0)
                    outitem.removeClass('action')
                    outitem.animate({ lineHeight: '0px', opacity: '0', top: '-10px' }, () => {
                        outitem.remove()
                    })
                }
            }
        },
        setRem() {
            //  PC端
            // 基准大小
            baseSize = 19;
            let basePc = baseSize / 1920; // 表示1920的设计图,使用100PX的默认值
            let vW = window.innerWidth; // 当前窗口的宽度
            let vH = window.innerHeight; // 当前窗口的高度
            // 非正常屏幕下的尺寸换算
            // let dueH = vW * 1080 / 1920
            // if (vH < dueH) { // 当前屏幕高度小于应有的屏幕高度，就需要根据当前屏幕高度重新计算屏幕宽度
            //     vW = vH * 1920 / 1080
            // }
            // let rem = vW * basePc; // 以默认比例值乘以当前窗口宽度,得到该宽度下的相应font-size值
            let botem = vW / 1920
            $('.more-nav').css({
                'transform': 'scale(' + botem + ')'
            })
            $('.nav').css({
                'transform': 'scale(' + botem + ')'
            })
            $('.battle-container .footer').css({
                'transform': 'scale(' + botem + ')'
            })

            $('.body-left-list').css({
                'transform': 'scale(' + botem + ')'
            })
        },
        getData() {
            var that = this;
            let temp = 0;
            $.ajax({
                type: "GET",
                url: 'http://127.0.0.1:57783/data.json',
                headers: {
                },
                success: function (resd) {
                    that.dumuType = 3
                    if (resd.datalist && resd.datalist.length > 0) {
                        // 1 穿透 0 不
                        if (that.isIgnoreMouseEvents != resd.datalist[0].shortCutKey) {
                            that.isIgnoreMouseEvents = resd.datalist[0].shortCutKey
                            if (resd.datalist[0].shortCutKey == 0) {
                                ipcRenderer.send('Main', {
                                    msgType: "WindowSize",
                                    jsonInfo: {
                                        window: 'BattleModal',
                                        size: 'setIgnoreMouseEvents'
                                    }
                                });
                            } else {
                                ipcRenderer.send('Main', {
                                    msgType: "WindowSize",
                                    jsonInfo: {
                                        window: 'BattleModal',
                                        size: 'nosetIgnoreMouseEvents'
                                    }
                                });
                            }
                        }

                        if (that.isIgnoreMouseEventsHide != resd.datalist[0].shortCutKeyF10) {
                            that.isIgnoreMouseEventsHide = resd.datalist[0].shortCutKeyF10
                        }

                        // 位置
                        // var winwidth = ((resd.datalist[0].wnd_right - resd.datalist[0].wnd_left) / (that.beilv * 1)).toFixed(0)
                        // var winheight = ((resd.datalist[0].wnd_bottom - resd.datalist[0].wnd_top) / (that.beilv * 1)).toFixed(0)
                        // var potX = ((resd.datalist[0].wnd_left) / (that.beilv * 1) + 10 / (that.beilv * 1)).toFixed(0)
                        // var potY = ((resd.datalist[0].wnd_top) / (that.beilv * 1)).toFixed(0)
                        // if (potX < 0) {
                        //     potX = 0
                        // }
                        // if (potY < 0) {
                        //     potY = 0
                        // }
                        // if (
                        //     winwidth != that.windowsize.winwidth ||
                        //     winheight != that.windowsize.winheight ||
                        //     potX != that.windowsize.potX ||
                        //     potY != that.windowsize.potY
                        // ) {
                        //     that.windowsize = {
                        //         winwidth: winwidth,
                        //         winheight: winheight,
                        //         potX: potX,
                        //         potY: potY,
                        //     }
                        //     ipcRenderer.send('Main', {
                        //         msgType: "ChangeWindowSize",
                        //         jsonInfo: {
                        //             window: 'BattleModal',
                        //             width: winwidth,
                        //             height: winheight,
                        //             point: `${potX},${potY}`,
                        //             skipTaskbar: true
                        //         }
                        //     });
                        //     if (!that.isinitefullscript) {
                        //         that.isinitefullscript = true
                        //         $.ajax({
                        //             type: "GET",
                        //             url: '/api/site/settings/info/',
                        //             success: function (res) {
                        //                 let relwidth = 1
                        //                 if (res.data.windowMode == 'WindowFullScreen') {
                        //                     let width = winwidth
                        //                     var Reg = /^\w.+?(?=x)/gi
                        //                     var fenwidth = res.data.gameScreen.match(Reg)[0] * 1;
                        //                     relwidth = fenwidth / width
                        //                     $('.map-right').css({
                        //                         'transform': 'scale(' + 1 / Number(relwidth) + ')'
                        //                     })
                        //                 }
                        //                 that.getsize(relwidth);
                        //             },
                        //             error: function (e) { }
                        //         });
                        //     }
                        // }
                        resd.datalist.map((items) => {
                            const data = items;
                            data.data.map((items) => {
                                items.Name = $.base64.decode(items.Name)
                                let batterStorage = that.batterStorage

                                if (batterStorage.modal == 1) {
                                    batterStorage.userList.map((item) => {
                                        if (item.usermessage.name == items.Name) {
                                            if (item.usermessage.pluginUserName) {
                                                items.pluginUserName = item.usermessage.pluginUserName
                                            }
                                        }
                                    })
                                } else if (batterStorage.modal == 2) {
                                    batterStorage.teamList.map((item) => {
                                        item.userList.map((user) => {
                                            if (user.name == items.Name) {
                                                if (user.pluginUserName) {
                                                    items.pluginUserName = user.pluginUserName
                                                }
                                            }
                                        })
                                    })
                                }
                            })
                            // 旧数据顶替
                            if (that.batterStorage.Substitute.length == 0) {
                                if (that.oldUserList.length == 0) {
                                    that.oldUserList = data.data
                                }
                                for (var i = 0; i < that.oldUserList.length; i++) {
                                    let init = false
                                    let indetaindex = 0
                                    for (var z = 0; z < data.data.length; z++) {
                                        if (that.oldUserList[i].Name == data.data[z].Name) {
                                            init = true
                                            indetaindex = z
                                        }
                                    }
                                    if (init) {
                                        that.oldUserList[i] = data.data[indetaindex]
                                    } else {
                                        data.data.push(that.oldUserList[i])
                                    }
                                }
                            }
                            that.base_data = data;
                            if (that.time_axis.length == 0) that.time_axis.push(data.time)
                            if (that.time_axis[that.time_axis.length - 1] != data.time) {
                                if (data.time % that.beishu == 0) {
                                    that.time_axis.push(data.time)
                                }
                            }
                            // 处理国旗、颜色等数据
                            let player_list = data.data.map(item => {
                                const Color = comOption.optionsColor.filter(elem => elem.colorId == item.Color)[0];
                                let temp_color = Color?.color;
                                let temp_name = Color?.name;
                                let temp_svg = comOption.optionsCountry.filter(elem => elem.sideId == item.Side)[0]?.url;

                                item['player_color'] = temp_color ? `rgba(${temp_color})` : ""
                                item['temp_svg'] = temp_svg || "";
                                item['temp_name'] = temp_name || "";
                                item['Plus'] = item.Plus || [];
                                item['Place'] = item.Place || [];
                                for (var i = 0; i < item.Plus.length; i++) {
                                    item.Plus[i].cdwidth = Number(item.Plus[i].cd) / 54 * 100 + '%'
                                }
                                return item;
                            })
                            if (that.batterStorage.modal != 4) {
                                that.modaltype = that.batterStorage.modal
                            } else {
                                that.isconfusion = true
                                let maxlength = 0
                                if (that.batterStorage.teamList[0].userList.length > that.batterStorage.teamList[1].userList.length) {
                                    maxlength = that.batterStorage.teamList[0].userList.length
                                } else {
                                    maxlength = that.batterStorage.teamList[1].userList.length
                                }
                                if (maxlength == 1) {
                                    that.modaltype = 1
                                }
                                if (maxlength == 2) {
                                    that.modaltype = 2
                                }
                                if (maxlength == 3) {
                                    that.modaltype = 3
                                }
                                if (maxlength == 4) {
                                    that.modaltype = 3
                                }
                            }
                            that.setRem()
                            if (that.modaltype == 1) {
                                for (var i = 0; i < player_list.length; i++) {
                                    if (player_list[i].Name == that.batterStorage.userList[0].usermessage.name) {
                                        that.player_1 = player_list[i];
                                        player_list[i].Name = that.batterStorage.userList[0].usermessage.name
                                        that.player_1Message = that.batterStorage.userList[0].usermessage
                                        that.player_1Number = that.batterStorage.userList[0].fraction
                                    }
                                    if (player_list[i].Name == that.batterStorage.userList[1].usermessage.name) {
                                        that.player_2 = player_list[i];
                                        player_list[i].Name = that.batterStorage.userList[1].usermessage.name
                                        that.player_2Message = that.batterStorage.userList[1].usermessage
                                        that.player_2Number = that.batterStorage.userList[1].fraction
                                    }
                                }
                                that.player_1array = [that.player_1, that.player_2]
                                for (var i = 0; i < that.player_1array.length; i++) {
                                    that.player_1array[i].Normal = that.ProductionSlot(that.player_1array[0], that.player_1array[1], [i])
                                }
                            } else {
                                let play1array = []
                                let play2array = []
                                for (var i = 0; i < player_list.length; i++) {
                                    for (var z = 0; z < that.batterStorage.teamList[0].userList.length; z++) {
                                        if (player_list[i].Name == that.batterStorage.teamList[0].userList[z].name) {
                                            play1array.push(player_list[i])
                                        }
                                    }
                                }
                                for (var i = 0; i < player_list.length; i++) {
                                    for (var z = 0; z < that.batterStorage.teamList[1].userList.length; z++) {
                                        if (player_list[i].Name == that.batterStorage.teamList[1].userList[z].name) {
                                            play2array.push(player_list[i])
                                        }
                                    }
                                }
                                if (that.batterStorage.Substitute.length > 0 && (play1array.length < 3 || play2array.length < 3)) {
                                    let needaddlist = []
                                    let locallist = []
                                    if (play1array.length < 3) {
                                        needaddlist = play1array
                                        locallist = that.batterStorage.teamList[0]
                                    } else if (play2array.length < 3) {
                                        needaddlist = play2array
                                        locallist = that.batterStorage.teamList[1]
                                    }
                                    for (var z = 0; z < locallist.userList.length; z++) {
                                        let inite = false
                                        for (var i = 0; i < needaddlist.length; i++) {
                                            if (needaddlist[i].Name == locallist.userList[z].name) {
                                                inite = true
                                            }
                                        }
                                        if (that.batterStorage.Substitute.length > 0 && !inite) {
                                            if (play1array.length < 3) {
                                                that.batterStorage.teamList[0].userList[z] = that.batterStorage.Substitute[0]
                                            } else if (play2array.length < 3) {
                                                that.batterStorage.teamList[1].userList[z] = that.batterStorage.Substitute[0]
                                            }
                                            that.batterStorage.Substitute = []
                                        }
                                    }
                                }

                                let useronearray = JSON.parse(JSON.stringify(play1array[0]))
                                let usertwoarray = JSON.parse(JSON.stringify(play2array[0]))
                                that.player_1 = useronearray;
                                that.player_2 = usertwoarray;
                                that.player_1Number = that.batterStorage.teamList[0].fraction
                                that.player_2Number = that.batterStorage.teamList[1].fraction
                                that.player_1array = play1array
                                that.player_2array = play2array
                                for (var i = 0; i < that.player_1array.length; i++) {
                                    that.player_1array[i].Normal = that.ProductionSlot(that.player_1array[i], that.player_2array[i], 0)
                                }
                                for (var i = 0; i < that.player_2array.length; i++) {
                                    that.player_2array[i].Normal = that.ProductionSlot(that.player_1array[i], that.player_2array[i], 1)
                                }
                                if (that.modaltype == 3) {
                                    for (var i = 0; i < that.player_1array.length; i++) {
                                        that.player_1array[i].Normal = that.player_1array[i].Normal.slice(0, 5)
                                    }
                                    for (var i = 0; i < that.player_2array.length; i++) {
                                        that.player_2array[i].Normal = that.player_2array[i].Normal.slice(0, 5)
                                    }
                                } else if (that.modaltype == 2) {
                                    that.player_1array[0].Normal = that.player_1array[0].Normal.slice(0, 7)
                                    that.player_2array[0].Normal = that.player_2array[0].Normal.slice(0, 7)
                                }
                                let maxshowlength = 7
                                if (that.modaltype == 3) {
                                    maxshowlength = 5
                                } else if (that.modaltype == 2) {
                                    maxshowlength = 7
                                }
                                for (var i = 0; i < that.player_1array.length; i++) {
                                    let maxnum = maxshowlength - that.player_1array[i].Normal.length
                                    if (maxnum < 0) {
                                        maxnum = 0
                                    }
                                    if (that.modaltype == 2) {
                                        if (i > 0) {
                                            maxnum = 15
                                        }
                                    }

                                    that.player_1array[i].maxNormal = []
                                    for (var z = 0; z < maxnum; z++) {
                                        that.player_1array[i].maxNormal.push(1)
                                    }
                                }
                                for (var i = 0; i < that.player_2array.length; i++) {
                                    let maxnum = maxshowlength - that.player_2array[i].Normal.length
                                    if (maxnum < 0) {
                                        maxnum = 0
                                    }
                                    if (that.modaltype == 2) {
                                        if (i > 0) {
                                            maxnum = 15
                                        }
                                    }
                                    that.player_2array[i].maxNormal = []
                                    for (var z = 0; z < maxnum; z++) {
                                        that.player_2array[i].maxNormal.push(1)
                                    }
                                }
                                for (var i = 1; i < play1array.length; i++) {
                                    that.player_1.APM = Number(that.player_1.APM) + Number(play1array[i].APM)
                                    that.player_1.Cash = Number(that.player_1.Cash) + Number(play1array[i].Cash)
                                    that.player_1.Consume = Number(that.player_1.Consume) + Number(play1array[i].Consume)
                                    that.player_1.Mine = Number(that.player_1.Mine) + Number(play1array[i].Mine)
                                    that.player_1.Power = Number(that.player_1.Power) + Number(play1array[i].Power)
                                    that.player_1.PowerLoad = Number(that.player_1.PowerLoad) + Number(play1array[i].PowerLoad)
                                }
                                for (var i = 1; i < play2array.length; i++) {
                                    that.player_2.APM = Number(that.player_2.APM) + Number(play2array[i].APM)
                                    that.player_2.Cash = Number(that.player_2.Cash) + Number(play2array[i].Cash)
                                    that.player_2.Consume = Number(that.player_2.Consume) + Number(play2array[i].Consume)
                                    that.player_2.Mine = Number(that.player_2.Mine) + Number(play2array[i].Mine)
                                    that.player_2.Power = Number(that.player_2.Power) + Number(play2array[i].Power)
                                    that.player_2.PowerLoad = Number(that.player_2.PowerLoad) + Number(play2array[i].PowerLoad)
                                }
                                that.player_1.player_color = 'rgba(41,117,231)'
                                that.player_2.player_color = 'rgba(255,24,24)'
                            }

                            // that.setEchartsData();

                        })
                    }
                },
                error: function (e) { }
            });
        },
        getnickName() {
            let batterStorage = this.batterStorage
            let userIdarray = []
            if (batterStorage.modal == 1) {
                batterStorage.userList.map((item) => {
                    userIdarray.push(item.userId)
                })
            } else if (batterStorage.modal == 2) {
                batterStorage.teamList.map((item) => {
                    item.userList.map((user) => {
                        userIdarray.push(user.userId)
                    })
                })
            }
            $.star({
                type: 'GET',
                url: '/community-user/redwar/user/v1/authentication/alias',
                showerror: false,
                data: {
                    uids: `${userIdarray}`
                },
                success: function (res) {
                    res.data.map((resitem) => {
                        if (batterStorage.modal == 1) {
                            batterStorage.userList.map((item) => {
                                if (item.userId == resitem.id) {
                                    if (resitem.pluginUserName) {
                                        item.usermessage.pluginUserName = resitem.pluginUserName
                                    }
                                }
                            })
                        } else if (batterStorage.modal == 2) {
                            batterStorage.teamList.map((item) => {
                                item.userList.map((user) => {
                                    if (user.userId == resitem.id) {
                                        if (resitem.pluginUserName) {
                                            user.pluginUserName = resitem.pluginUserName
                                        }
                                    }
                                })
                            })
                        }
                    })
                    this.batterStorage = batterStorage
                },
                error: function (req) {

                }
            });
        },
        // monitorStatus(){
        //     $.ajax({
        //         type: "GET",
        //         url: 'http://127.0.0.1:57783/shortCutKey11.json',
        //         success: function (resd) {

        //         },
        //         error: function (e) { }
        //     });
        // },
        getwei(wei) {
            var that = this
            var winwidth = ((wei.Right - wei.Left)).toFixed(0)
            var winheight = ((wei.Bottom - wei.Top)).toFixed(0)
            var potX = ((wei.Left) + 10 / (that.beilv * 1)).toFixed(0)
            var potY = ((wei.Top)).toFixed(0)

            $('.chat').css('paddingRight', 160 / (that.beilv * 1))
            if (potX < 0) {
                potX = 0
            }
            if (potY < 0) {
                potY = 0
            }
            that.windowsize = {
                winwidth: winwidth,
                winheight: winheight,
                potX: potX,
                potY: potY,
            }
            ipcRenderer.send('Main', {
                msgType: "ChangeWindowSize",
                jsonInfo: {
                    window: 'BattleModal',
                    width: winwidth,
                    height: winheight,
                    point: `${potX},${potY}`,
                    skipTaskbar: true
                }
            });
            $.ajax({
                type: "GET",
                url: '/api/site/settings/info/',
                success: function (res) {
                    let relwidth = 1
                    if (res.data.windowMode == 'WindowFullScreen') {
                        let width = winwidth
                        var Reg = /^\w.+?(?=x)/gi
                        var fenwidth = res.data.gameScreen.match(Reg)[0] * 1;
                        relwidth = fenwidth / width
                        $('.map-right').css({
                            'transform': 'scale(' + 1 / Number(relwidth) + ')'
                        })
                    }
                    that.getsize(relwidth);
                },
                error: function (e) { }
            });

        },
        getsize(relwidth) {
            var that = this
            $.ajax({
                type: "GET",
                url: '/api/site/window/display',
                success: function (res) {
                    that.beilv = res.data.scaleFactor
                    $('.map-right').css({
                        'transform': 'scale(' + 1 / Number(res.data.scaleFactor) + ')'
                    })
                    if (relwidth == 1) {
                        $('.map-right').css({
                            'transform': 'scale(' + 1 / Number(res.data.scaleFactor) + ')'
                        })
                    } else {
                        $('.map-right').css({
                            'transform': 'scale(' + (1 / Number(res.data.scaleFactor)) * (1 / relwidth) + ')'
                        })
                    }
                },
                error: function (e) { }
            });
        },
        // 右侧队列排序
        ProductionSlot(play1, play2, type) {
            let player_1_country = this.judgePlayerCountry(play1.Side);
            let player_2_country = this.judgePlayerCountry(play2.Side);
            let Normal1 = this.hasJson('unit_name', play1.Normal, whitelist[player_1_country]);
            let Normal2 = this.hasJson('unit_name', play2.Normal, whitelist[player_2_country]);
            // 默认排序
            if (player_1_country != 'Yuri') {
                Normal1 = defaultSort(Normal1, player_1_country)
            } else {
                Normal1 = YuriSort(Normal1, this.batterStorage.version)
            }

            if (player_2_country != 'Yuri') {
                Normal2 = defaultSort(Normal2, player_2_country)
            } else {
                Normal2 = YuriSort(Normal2, this.batterStorage.version)
            }
            // 同类
            let oneinitarray = []
            for (var i = 0; i < Normal1.length; i++) {
                for (var z = 0; z < sameType.length; z++) {
                    for (var y = 0; y < sameType[z].length; y++) {
                        if (sameType[z][y] == Normal1[i].unit_name) {
                            oneinitarray.push(z)
                        }
                    }
                }
            }
            let twoinitarray = []
            for (var i = 0; i < Normal2.length; i++) {
                for (var z = 0; z < sameType.length; z++) {
                    for (var y = 0; y < sameType[z].length; y++) {
                        if (sameType[z][y] == Normal2[i].unit_name) {
                            twoinitarray.push(z)
                        }
                    }
                }
            }
            let allinite = [];
            for (var i = 0; i < oneinitarray.length; i++) {
                for (var z = 0; z < twoinitarray.length; z++) {
                    if (oneinitarray[i] == twoinitarray[z]) {
                        allinite.push(oneinitarray[i])
                    }
                }
            }
            allinite = allinite.sort((a, b) => a - b)
            for (var i = 0; i < allinite.length; i++) {
                for (var z = 0; z < sameType[allinite[i]].length; z++) {
                    for (var y = 0; y < Normal1.length; y++) {
                        if (Normal1[y].unit_name == sameType[allinite[i]][z]) {
                            Normal1[y].sort = i
                        }
                    }
                    for (var y = 0; y < Normal2.length; y++) {
                        if (Normal2[y].unit_name == sameType[allinite[i]][z]) {
                            Normal2[y].sort = i
                        }
                    }
                }
            }

            Normal1 = Normal1.sort((a, b) => a.sort - b.sort)
            Normal2 = Normal2.sort((a, b) => a.sort - b.sort)
            if (type == 0) {
                return Normal1
            } else {
                return Normal2
            }

            // // 默认排序
            function defaultSort(array, country) {
                for (let i = 0; i < deflutSlot[country].length; i++) {
                    const item = deflutSlot[country][i];

                    for (let j = 0; j < array.length; j++) {
                        let element = array[j];
                        if (item == element.unit_name) {
                            element.sort = i + 99
                        } else {
                            element.sort = 999
                        }
                    }
                }
                return array.sort((a, b) => a.sort - b.sort)
            }

            // // 尤里排序(按照数量排序)
            function YuriSort(array, version) {
                if (version != 3) {
                    for (let i = 0; i < array.length; i++) {
                        array[i].sort = 999
                    }
                    return array.sort((a, b) => b.count - a.count)
                } else {
                    for (let i = 0; i < array.length; i++) {
                        if (array[i].unit_name == 'CHAR') {
                            array[i].sort = 99
                        } else {
                            array[i].sort = 999
                        }
                    }
                    return array.sort((a, b) => a.sort - b.sort)
                }
            }
        },
        // 判断队伍属于哪个组织
        judgePlayerCountry(side) {
            // 0-4 盟军
            // 5-8 苏军
            // 9 尤里
            if (side >= 0 && side <= 4) {
                return 'allied'
            } else if (side >= 5 && side <= 8) {
                return 'soviet'
            } else if (side == 9) {
                return 'Yuri'
            }
        },
        hasJson(key, arr1, arr2, dele = () => { }) {
            let newArray = [];
            for (let i = 0; i < arr1.length; i++) {
                const item = arr1[i];

                for (let j = 0; j < arr2.length; j++) {
                    const element = arr2[j];
                    if (typeof element == 'string' && item[key] == element) {
                        newArray.push(item)
                    } else if (item[key] == element[key]) {
                        dele({ i, j })
                        newArray.push({
                            k1: item,
                            k2: element
                        })
                    }
                }
            }

            return newArray
        },
        // 初始化echarts
        initEcharts() {
            const chartDom = document.getElementById('main');
            const myChart = echarts.init(chartDom);
            option && myChart.setOption(option);

            setTimeout(() => {
                myChart.resize()
            }, 400);
            window.addEventListener('resize', function () {
                myChart.resize()
            });
        },
        // 设置echarts数据
        setEchartsData() {
            const that = this;
            option.xAxis.data.push(that.game_time());

            if (option.xAxis.data.length == 1) {
                for (let i = 0; i < 10; i++) {
                    option.xAxis.data.push(this.base_data.time + i)
                }
            }

            option.series[0].name = that.player_1.Name;
            option.series[1].name = that.player_2.Name;
            option.series[0].areaStyle.normal.color = that.player_1.player_color;
            option.series[1].areaStyle.normal.color = that.player_2.player_color;

            that.tabs.forEach(element => {
                const key = element.key || '';

                // 队伍数值操作判断
                if (that.player_1[key] <= 0 && that.player_2[key] > 0) {
                    element.echarts_data.player_1_data.push(100);
                    element.echarts_data.player_2_data.push(100);
                    element.echarts_data.player_1_line.push(100);
                    element.echarts_data.player_2_line.push(50);

                } else if (that.player_2[key] <= 0 && that.player_1[key] > 0) {
                    element.echarts_data.player_1_data.push(100);
                    element.echarts_data.player_2_data.push(0);
                    element.echarts_data.player_1_line.push(50);
                    element.echarts_data.player_2_line.push(0);

                } else if (that.player_1[key] > 0 && that.player_2[key] > 0) {
                    // 队伍1占比
                    let player_1_proportion = 100;
                    // 队伍2占比
                    let player_2_proportion = that.player_2[key] / (that.player_1[key] + that
                        .player_2[key]) * 100;
                    // 队伍1中间值
                    let player_1_median = (100 - player_2_proportion) / 2 + player_2_proportion;
                    // 队伍2中间值
                    let player_2_median = player_2_proportion / 2;

                    element.echarts_data.player_1_data.push(player_1_proportion);
                    element.echarts_data.player_2_data.push(player_2_proportion);
                    element.echarts_data.player_1_line.push(player_1_median);
                    element.echarts_data.player_2_line.push(player_2_median);
                } else {
                    element.echarts_data.player_1_data.push(100);
                    element.echarts_data.player_2_data.push(50);
                    element.echarts_data.player_1_line.push(75);
                    element.echarts_data.player_2_line.push(25);
                }
            });

            // this.switchEchartsData();
        },
        // 切换echarts数据
        switchEchartsData() {
            const index = this.tab_active;

            option.series[0].data = this.tabs[index].echarts_data.player_1_data;
            option.series[1].data = this.tabs[index].echarts_data.player_2_data;
            option.series[2].data = this.tabs[index].echarts_data.player_1_line;
            option.series[3].data = this.tabs[index].echarts_data.player_2_line;

            this.initEcharts();
        },
        changeNumber(type, num, add) {
            if (this.batterStorage.showModaltype == 3) {
                return false
            }
            let getnum = Number(num);
            if (add == 1) {
                getnum = getnum - 1
                if (getnum < 0) {
                    getnum = 0
                }
            } else {
                getnum = getnum + 1
                if (this.batterStorage.showModaltype == 1 && getnum > 13) {
                    getnum = 13
                }
            }
            if (type == 1) {
                if (this.batterStorage.modal == 1) {
                    this.batterStorage.userList[0].fraction = getnum;
                } else {
                    this.batterStorage.teamList[0].fraction = getnum;
                }
                this.player_1Number = getnum
            } else {
                if (this.batterStorage.modal == 1) {
                    this.batterStorage.userList[1].fraction = getnum;
                } else {
                    this.batterStorage.teamList[1].fraction = getnum;
                }
                this.player_2Number = getnum;
            }
            saveMessage('batterStorage', this.batterStorage)
        }
    },
})


$(document).ready(function () {
    init()
});

// 初始化页面
function init() {
    $(".J_flip").attr('type', 'false').css("transform", "rotate(180deg)")
    // $(".footer-echarts").css("height", "0px")
}

// 切换左侧队伍列表显示状态
$(document).on('click', '.J_panel', function () {
    var $this = $(this)
    let type = $this.attr('type');
    $this.closest('.body-left-list').find(".lists").slideToggle(300)
    if (type == 'true') {
        $this.attr('type', 'false').css("transform", "rotate(180deg)")
    } else {
        $this.attr('type', 'true').css("transform", "rotate(0deg)")
    }
})

// 切换底部图表显示状态
$(".J_flip").click(function () {
    // let type = $(this).attr('type')
    // if (type == 'true') {
    //     $(this).attr('type', 'false').css("transform", "rotate(180deg)")
    //     $(".footer-echarts").css("height", "0")
    // } else {
    //     $(this).attr('type', 'true').css("transform", "rotate(0deg)")
    //     $(".footer-echarts").css("height", "21.375rem")
    // }
});

