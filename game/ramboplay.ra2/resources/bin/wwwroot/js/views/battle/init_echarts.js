

const option = {
    grid: {
        show: false,
        left: '20rem',
        right: '4%',
        bottom: '10rem',
        top:"28rem",
        containLabel: true
    },
    tooltip: {
        trigger: 'axis',
        formatter: function (a) {
            var res = '';
            var nameList = a;
            for (var i = 0; i < a.length; i++) {
                if (nameList[i].data != undefined) {
                    res += nameList[i].seriesName + ':' + nameList[i].data + '%' + '<br>'
                }
            }
            res = res.split('<br>');
            return '队伍1' + a[2].value;
        }
    },
    legend: {
        show: false
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        axisLine: {
            show: false,
        },
        axisLabel: {
            textStyle: {
                color: '#878787',
                fontSize: '16',
                fontWeight: "900"
            }
        },
        splitLine: {
            show: true,
            lineStyle: {
                color: "#282A32"
            }
        },
        data: []
    },
    yAxis: {
        type: 'value',
        axisLine: {
            show: false,
        },
        axisLine: {

        },
        splitLine: {
            show: true,
            lineStyle: {
                color: "#272323"
            }
        },
        axisLabel: {
            textStyle: {
                color: '#878787',
                fontSize: '16',
                fontWeight: "900"
            }
        },
    },
    series: [
        {
            name: '队伍1',
            type: 'line',
            showSymbol: false,
            itemStyle: {
                normal: {
                    lineStyle: {
                        width: 0,
                    }
                },
                emphasis: {   // 设置鼠标放在图上的圆点
                    color: 'transparent',  // 原点背景色（大小默认是  总大小-边框大小）
                    borderColor: 'blue', // 原点背景色
                    borderWidth: 0, // 原点边框大小()
                },
            },
            areaStyle: {
                normal: {
                    //颜色渐变函数 前四个参数分别表示四个位置依次为左、下、右、上
                    color: "red"

                }
            },

            data: []
        },
        {
            name: '队伍2',
            type: 'line',
            showSymbol: false,
            itemStyle: {
                normal: {
                    lineStyle: {
                        width: 2,
                        type: 'solid',
                        color: "RGBA(0, 0, 0, 1)",
                    }
                },
                emphasis: {   // 设置鼠标放在图上的圆点
                    color: 'transparent',  // 原点背景色（大小默认是  总大小-边框大小）
                    borderColor: 'blue', // 原点背景色
                    borderWidth: 0, // 原点边框大小()
                },
            },
            areaStyle: {
                normal: {
                    //颜色渐变函数 前四个参数分别表示四个位置依次为左、下、右、上
                    color: "pink"

                }
            },
            data: []
        },
        {
            name: '',
            type: 'line',
            showSymbol: false,
            itemStyle: {
                normal: {
                    lineStyle: {
                        width: 1,
                        type: 'solid',
                        color: "transparent",
                    }
                },
                emphasis: {   // 设置鼠标放在图上的圆点
                    color: '#FFC925',  // 原点背景色（大小默认是  总大小-边框大小）
                    borderColor: '#ffffff', // 原点背景色
                    borderWidth: 2, // 原点边框大小()
                },
            },
            data: []
        },
        {
            name: '',
            type: 'line',
            showSymbol: false,
            itemStyle: {
                normal: {
                    lineStyle: {
                        width: 1,
                        type: 'solid',
                        color: "transparent",
                    }
                },
                emphasis: {   // 设置鼠标放在图上的圆点
                    color: '#FFC925',  // 原点背景色（大小默认是  总大小-边框大小）
                    borderColor: '#ffffff', // 原点背景色
                    borderWidth: 2, // 原点边框大小()
                },
            },
            data: []
        }

    ]
}


