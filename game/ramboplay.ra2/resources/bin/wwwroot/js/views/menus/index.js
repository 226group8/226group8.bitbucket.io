var { ipcRenderer } = require("electron");
$(document).on('click', '.J_menuOpen', function () {
    ipcRenderer.send('Main', {
        msgType: "ShowModal",
        jsonInfo: {
            window: 'main',
        }
    });
    localStorage.setItem('JmenuOpenIndex', 1)
})
$(document).on('click', '.J_settingOpen', function () {
    ipcRenderer.send('Main', {
        msgType: "ShowModal",
        jsonInfo: {
            window: 'main',
        }
    });
    localStorage.setItem('JmenuSettingOpen', 1)
})
$(document).on('click', '.J_changeUser', function () {
    ipcRenderer.send('Main', {
        msgType: "ShowModal",
        jsonInfo: {
            window: 'main',
        }
    });
    localStorage.setItem('JmenuChangeUser', 1)
})
$(document).on('click', '.J_closeMain', function () {
    ipcRenderer.send('Main', {
        msgType: "ShowModal",
        jsonInfo: {
            window: 'main',
        }
    });
    localStorage.setItem('Jmenuclose', 1)
})
