var userAdminMessage = {
    swiper: null,
    options: {
        thumbBox: '.cutImage-content-befor',
        spinner: '.cutImage-content-befor',
        imgSrc: ''
    },
    cropper: null,
    userMessage: {},
    newMessage: {
        username: null,
        gender: null,
        userAvatar: null
    },
    changePass: {
        oldPass: "",
        newPass: "",
        surePass: ""
    },
    changePhone: {
        phone: "",
        code: "",
        countryCode: 86
    },
    countFun: null,
    countloading: false,
    countnum: 60,
    changeNameGold: 0,
    frameList: [],
    avatarChangeCoin: 0
}
function userAdmin() {
    userAdminMessage = {
        swiper: null,
        options: {
            thumbBox: '.cutImage-content-befor',
            spinner: '.cutImage-content-befor',
            imgSrc: ''
        },
        cropper: null,
        userMessage: {},
        newMessage: {
            username: "",
            gender: "",
            userAvatar: "",
            email: ''
        },
        changePass: {
            oldPass: "",
            newPass: "",
            surePass: ""
        },
        changePhone: {
            phone: "",
            code: "",
            countryCode: 86
        },
        countFun: null,
        countloading: false,
        countnum: 60,
        changeNameGold: 0,
        avatarChangeCoin: 0
    }
    $('#UserAdminModal').find('.modal-dialog').html(template('UserAdminModal-script'))
    $('#UserAdminModal').modal('show')
    userAdmininit()
}
$(document).on('click', '.J_UserAdmin', function () {
    userAdmin()
})
$(document).on('hidden.bs.modal', '#UserAdminModal', function () {
    getUserGold()
    userAdminMessage.swiper.destroy();
    UserAdminModalSetInterval('hide')
    $.star({
        type: 'GET',
        url: '/community-user/user/v2/info',
        showerror: true,
        data: {},
        success: function (res) {
            userInfo.username = res.data.username
            userInfo.avatar = res.data.userAvatar
            userInfo.gender = res.data.gender
            userInfo.avatarFrame = res.data.avatarFrame
            saveMessage('userInfo', userInfo)
            let savenum = $('.userGoldNum').text()
            $('.userGoldNum').text()
            $('#Hall-user').html(template('Hall-user-script', userInfo))
            $('.userGoldNum').text(savenum)
            $('.userGoldNum').attr('data-num', savenum)
            $('.career-content-user-avatar').attr('src', userInfo.avatar)
            let customField = customFieldFun('encryption', {
                avatarFrame: userInfo.avatarFrame,
                authenticateType: userInfo.authenticateType,
                authenticateContent: userInfo.authenticateContent,
                isAuthenticate: userInfo.isAuthenticate
            })
            ipcRenderer.send('Main', {
                msgType: "ModfiyUserInfo",
                jsonInfo: {
                    NickName: userInfo.username,
                    Avatar: userInfo.avatar,
                    customField: customField
                }
            });
        },
        error: function (req) {
        }
    });
})
$(document).on('click', '.UserAdminModal-nav-item', function () {
    var $this = $(this)
    $this.siblings().removeClass('UserAdminModal-nav-active')
    $this.addClass('UserAdminModal-nav-active')
    userAdminMessage.swiper.slideTo($this.attr('data-index'))
    if ($this.attr('data-index') == 2) {
        UserAdminModalGetMobilCode()
    }
})


// 初始化
function userAdmininit() {
    $('#UserAdminModal').find('.modal-dialog').html(template('UserAdminModal-script'))
    $('#UserAdminModal').modal('show')
    $.star({
        type: 'GET',
        url: '/community-user/user/v2/info',
        showerror: true,
        data: {},
        success: function (res) {
            userAdminMessage.userMessage = res.data
            userAdminMessage.newMessage = {
                username: null,
                gender: null,
                birthday: null,
                userAvatar: null,
                email: null,
            }
            // 用户菜单
            $('#UserAdminModal').find('.modal-dialog').html(template('UserAdminModal-script'))
            $('#UserAdminModal-swiper-one').html(template('UserAdminModal-swiper-one-script', res.data))
            $.star({
                type: 'GET',
                url: '/battlecenter/platform/config/gold/v1/list',
                showerror: false,
                data: {
                },
                success: function (res) {
                    let userEditNameGold = res.data.userEditNameGold * 1
                    $('.UserAdminModal-changeNamegolddiv').removeClass('hide')
                    $('.UserAdminModal-changeNamegold').text(userEditNameGold)
                    userAdminMessage.changeNameGold = userEditNameGold
                },
                error: function (req) { }
            });

            $('#UserAdminModal').modal('show')
            if (userAdminMessage.swiper) {
                userAdminMessage.swiper.destroy()
            }
            userAdminMessage.swiper = new Swiper('.UserAdminModal-swiper', {
                autoplay: false,
                // effect: 'fade',
                allowTouchMove: false,
            })
            getuserAdminAvatar(res.data.userAvatar)
        },
        error: function (req) {
        }
    });
}
//默认头像
function getuserAdminAvatar(oldAvatar) {
    $('.UserAdminModalavatar').removeClass('hide')
    $('.UserAdminModalframe').addClass('hide')
    let getlist = JSON.parse(JSON.stringify(comOption.optionsAvatar))
    getlist.map((item, index) => {
        if (item == oldAvatar) {
            getlist.splice(index, 1)
        }
    })
    getlist.unshift(oldAvatar)
    $('#UserAdminModal-swiper-one-avatarlist').html(template('UserAdminModal-swiper-one-avatarlist-script', { list: getlist }))
}
//头像框list
function getuserAdminAvatarframe() {
    $('.UserAdminModalavatar').addClass('hide')
    $('.UserAdminModalframe').removeClass('hide')
    $.star({
        type: 'GET',
        url: '/community-user/user/derive/avatar/v1/frame',
        showerror: true,
        data: {
            all: true,
        },
        success: function (res) {
            userAdminMessage.frameList = res.data
            $('#UserAdminModal-swiper-one-avatarlistframe').html(template('UserAdminModal-swiper-one-avatarlistframe-script', { list: res.data }))
        },
        error: function (req) {
        }
    });
}
//切换
$(document).on('click', '.UserAdminModal-avatar-nav-item', function () {
    var $this = $(this)
    let type = $this.attr('data-type')
    $this.siblings().removeClass('active')
    $this.addClass('active')
    if (type == 'avatar') {
        getuserAdminAvatar(userAdminMessage.newMessage.userAvatar || userInfo.avatar)
    } else {
        getuserAdminAvatarframe()
    }
})
//选择头像框
$(document).on('click', '.avatarlistframe-item', function () {
    // $('#UserAdminModal .UserAdminModal-avatar::after').
})
// 选择默认头像
$(document).on('click', '.UserAdminModal-bottom-avatarList-item', function () {
    var $this = $(this)
    $this.closest('.row').find('.UserAdminModal-bottom-avatarList-item').removeClass('UserAdminModal-bottom-avatarList-item-active')
    $this.addClass('UserAdminModal-bottom-avatarList-item-active')
    userAdminMessage.newMessage.userAvatar = $this.find('img').attr('src').split('?x-oss-process=')[0]
    $('.UserAdminModal-avatar').find('img').attr('src', userAdminMessage.newMessage.userAvatar)
    $('.UserAdminModal-avatar').find('.UserAdminModal-input-error').addClass('hide')
})
// 监听昵称改动
$(document).on('input', '.J_UserAdminModal-name', function () {
    var $this = $(this)
    userAdminMessage.newMessage.username = $this.val()
})
$(document).on('blur', '.J_UserAdminModal-name', function () {
    var $this = $(this)
    $this.closest('.relative').find('.UserAdminModal-input-error').addClass('hide')
    var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？%]");
    var result = userAdminMessage.newMessage.username.match(pattern);
    if (!result) {
        $this.closest('.relative').find('.UserAdminModal-input-error').addClass('hide')
    } else {
        $this.closest('.relative').find('.UserAdminModal-input-error').removeClass('hide')
        $this.closest('.relative').find('.UserAdminModal-input-error').text(translatesrting('不允许输入除“-”“_”以外的特殊字符'))
    }
    if (userAdminMessage.newMessage.username == "") {
        $this.closest('.relative').find('.UserAdminModal-input-error').removeClass('hide')
        $this.closest('.relative').find('.UserAdminModal-input-error').text(translatesrting('昵称不能为空'))
    } else {
        $this.closest('.relative').find('.UserAdminModal-input-error').addClass('hide')
    }
})
// 监听性别改动
$(document).on('click', '.J_UserAdminModal-gender', function () {
    var $this = $(this)
    $this.siblings().removeClass('active')
    $this.addClass('active')
    userAdminMessage.newMessage.gender = $this.attr('data-gender')
})
// 监听头像改动
$(document).on('click', '.avatarlistframe-item', function () {
    var $this = $(this)
    $this.closest('.row').find('.avatarlistframe-item').removeClass('active')
    $this.addClass('active')
    userAdminMessage.newMessage.frameCode = $this.attr('data-code')
    let code = $this.attr('data-code')
    let chooseframe = ""
    if (code != 'default') {
        for (var i = 0; i < userAdminMessage.frameList.length; i++) {
            if (userAdminMessage.frameList[i].code == code) {
                chooseframe = userAdminMessage.frameList[i]
                userAdminMessage.newMessage.avatarFrame = userAdminMessage.frameList[i].frame
            }
        }
        $('.UserAdminModal-avatar').find('.gx-frame').css('background-image', 'url(' + formatapngfun(chooseframe.frame, 200) + '' + ')')
    } else {
        userAdminMessage.newMessage.frameCode = "0"
        userAdminMessage.newMessage.avatarFrame = "0"
        $('.UserAdminModal-avatar').find('.gx-frame').css('background-image', '')
    }
})


// 监听邮箱改动
$(document).on('input', '.J_UserAdminModal-email', function () {
    var $this = $(this)
    userAdminMessage.newMessage.email = $this.val()
})
$(document).on('blur', '.J_UserAdminModal-email', function () {
    var $this = $(this)
    var pattern = /^([\.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/;
    if (pattern.test(userAdminMessage.newMessage.email)) {
        $this.closest('.relative').find('.UserAdminModal-input-error').addClass('hide')
    } else {
        $this.closest('.relative').find('.UserAdminModal-input-error').removeClass('hide')
        $this.closest('.relative').find('.UserAdminModal-input-error').text(translatesrting('邮箱格式输入不正确'))
    }
    if (userAdminMessage.newMessage.email == "" || !userAdminMessage.newMessage.email) {
        $this.closest('.relative').find('.UserAdminModal-input-error').addClass('hide')
    }
})
// 基本信息提交
$(document).on('click', '.J_UserAdminSave', function () {
    var $this = $(this)
    let iserror = false
    let iserrormessage = ""
    $('.UserAdminModal-swiper-content .UserAdminModal-input-error').each(function () {
        if (!$(this).hasClass('hide')) {
            iserror = true
            iserrormessage = $(this).text()
        }
    })
    if (iserror) {
        spop({
            template: iserrormessage,
            autoclose: 3000,
            style: 'error'
        });
        return false
    }
    let changeMessage = {}
    let alertMessage = translatesrting("你确定保存修改吗？")
    let needCoin = 0
    let ischangeUserName = false
    if (userAdminMessage.userMessage.username != userAdminMessage.newMessage.username && userAdminMessage.newMessage.username) {
        changeMessage.username = userAdminMessage.newMessage.username
        ischangeUserName = true
        needCoin = needCoin + (userAdminMessage.changeNameGold * 1)
    }
    if (userAdminMessage.userMessage.gender != userAdminMessage.newMessage.gender && userAdminMessage.newMessage.gender) {
        changeMessage.gender = userAdminMessage.newMessage.gender
    }
    if (userAdminMessage.userMessage.userAvatar != userAdminMessage.newMessage.userAvatar && userAdminMessage.newMessage.userAvatar) {
        changeMessage.userAvatar = userAdminMessage.newMessage.userAvatar
        if ($.inArray(changeMessage.userAvatar, comOption.optionsAvatar) == -1) {
            needCoin = needCoin + (userAdminMessage.avatarChangeCoin * 1)
        }
    }
    if (userAdminMessage.userMessage.email != userAdminMessage.newMessage.email && userAdminMessage.newMessage.email) {
        changeMessage.email = userAdminMessage.newMessage.email
    }
    if (userAdminMessage.userMessage.avatarFrame != userAdminMessage.newMessage.avatarFrame && userAdminMessage.newMessage.avatarFrame) {
        changeMessage.frameCode = userAdminMessage.newMessage.frameCode
    }
    if (needCoin > 0) {
        alertMessage = translatesrting("您确定消耗") + needCoin + translatesrting("金币修改吗？")
    }
    if (
        changeMessage.email == null &&
        changeMessage.gender == null &&
        changeMessage.userAvatar == null &&
        changeMessage.username == null &&
        changeMessage.frameCode == null
    ) {
        $('#UserAdminModal').modal('hide')
    } else {
        if ($this.hasClass('gx-disabled')) {
            return false
        }
        $this.gxbtn('loading')
        setTimeout(() => {
            $this.gxbtn('reset')
        }, 300)
        gxmodal({
            title: translatesrting('提示'),
            centent: '<div class="mt25">' + alertMessage + '</div>',
            buttons: [
                {
                    text: translatesrting('确定'),
                    class: 'btn-middle btn-middle-blue',
                    callback: function () {
                        if (ischangeUserName && localStorage.getItem('isdetails')) {
                            // 改名并且在房间
                            $.controlAjax({
                                type: "get",
                                url: '/Details/LeaveGameRoom/',
                                data: {},
                                showerror: true,
                                success: function (res) {
                                    hallMessage.closeRoom('room')
                                },
                                error: function (req) {

                                }
                            })
                        }
                        $.star({
                            type: 'POST',
                            url: '/community-user/user/v1/info/edit',
                            showerror: false,
                            data: changeMessage,
                            success: function (res) {
                                $('#UserAdminModal').modal('hide')
                                spop({
                                    template: translatesrting('保存成功'),
                                    autoclose: 3000,
                                    style: 'success'
                                });
                                getUserGold()
                            },
                            error: function (req) {
                                let errormess = req.errorMsg
                                if (req.errorCode == 220001 || req.errorCode == 210113 || req.errorCode == 210215 || req.errorCode == 250001) {
                                    showtoast({
                                        message: translatesrting(errormess)
                                    })
                                } else if (req.errorCode == 210114) {
                                    $('.UserAdminModal-avatar').find('.UserAdminModal-input-error').removeClass('hide').text(errormess)
                                } else {
                                    $('.J_UserAdminModal-name').closest('.relative').find('.UserAdminModal-input-error').addClass('hide')
                                    $('.J_UserAdminModal-name').closest('.relative').find('.UserAdminModal-input-error').addClass('hide')
                                    $('.UserAdminModal-avatar').find('.UserAdminModal-input-error').addClass('hide')
                                    spop({
                                        template: errormess,
                                        autoclose: 3000,
                                        style: 'error'
                                    });
                                }
                            }
                        });
                    }
                },
                {
                    text: translatesrting('取消'),
                    class: 'btn-middle btn-style-ash',
                }
            ]
        })
    }
})
$(document).on('click', '.UserAdminModal-bottom-content-updateImg', function () {
    var $this = $(this)
    $this.gxbtn('loading')
    $.star({
        type: 'GET',
        url: '/battlecenter/platform/config/gold/v1/list',
        showerror: true,
        data: {
        },
        success: function (res) {
            $this.gxbtn('reset')
            if (res.data.userEditAvatarGola * 1 > 0) {
                userAdminMessage.avatarChangeCoin = res.data.userEditAvatarGola * 1
                gxmodal({
                    title: translatesrting('提示'),
                    centent: '<div class="mt25">' + translatesrting('您确定消耗') + res.data.userEditAvatarGola + translatesrting('金币修改头像吗') + '</div>',
                    buttons: [{
                        text: translatesrting('确定'),
                        class: 'btn-middle btn-middle-blue',
                        callback: function () {
                            $('#UserAdminModal-upload-file').click()
                        }
                    },
                    {
                        text: translatesrting('取消'),
                        class: 'btn-middle btn-style-ash',
                    }
                    ]
                })
            } else {
                $('#UserAdminModal-upload-file').click()
            }
        },
        error: function (req) {
            $this.gxbtn('reset')
        }
    });
})

// 图片
$(document).on('change', '#UserAdminModal-upload-file', function (obj) {
    // 格式限制
    var fileTypes = [".jpg", ".png"];
    var filePath = $(this).val();
    if (filePath) {
        var isNext = false;
        var fileEnd = filePath.substring(filePath.indexOf("."));
        for (var i = 0; i < fileTypes.length; i++) {
            if (fileTypes[i] == fileEnd) {
                isNext = true;
                break;
            }
        }
        if (!isNext) {
            spop({
                template: translatesrting('不接受此文件类型'),
                autoclose: 3000,
                style: 'error'
            });
            $(this).val("")
            return false;
        }
    } else {
        return false;
    }
    // 大小限制
    if ((obj.currentTarget.files[0].size / 1024).toFixed(0) > 2048) {
        spop({
            template: translatesrting('上传图片不得大于2M'),
            autoclose: 3000,
            style: 'error'
        });
        return false
    }
    var reader = new FileReader();
    reader.onload = function (e) {
        userAdminMessage.options.imgSrc = e.target.result;
        userAdminMessage.cropper = $('.cutImage-content').cropbox(userAdminMessage.options);
    }
    reader.readAsDataURL(obj.currentTarget.files[0]);
    $(this).val("")
    $('#cutImage').modal('show')
})
function UserAdminModalgetImg() {
    var img = userAdminMessage.cropper.getDataURL();
    let formData = new FormData();
    formData.append("file", dataURLtoFile(img, 'avatar'));
    $.imgAjax({
        url: '/community-user/user/avatar/v1/upload',
        data: formData,
        type: 'POST',
        //ajax2.0可以不用设置请求头，但是jq帮我们自动设置了，这样的话需要我们自己取消掉
        contentType: false,
        //取消帮我们格式化数据，是什么就是什么
        processData: false,
        success: function (res) {
            var avatar = res.data.avatar
            $('#cutImage').modal('hide')
            $('.UserAdminModal-avatar').find('img').attr('src', avatar)
            $('.UserAdminModal-avatar').find('.UserAdminModal-input-error').addClass('hide')
            userAdminMessage.newMessage.userAvatar = avatar
        }
    })
}
$(document).on('click', '.J_cutImage', function () {
    UserAdminModalgetImg()
})
$(document).on('click', '.cutImage-zoomin', function () {
    userAdminMessage.cropper.zoomIn();
})
$(document).on('click', '.cutImage-zoomout', function () {
    userAdminMessage.cropper.zoomOut();
})
// 监听旧密码
$(document).on('input', '.J_UserAdminModal-oldPass', function () {
    var $this = $(this)
    userAdminMessage.changePass.oldPass = $this.val()
})
// 监听旧密码
$(document).on('input', '.J_UserAdminModal-newPass', function () {
    var $this = $(this)
    userAdminMessage.changePass.newPass = $this.val()
})
// 监听确认密码
$(document).on('input', '.J_UserAdminModal-surePass', function () {
    var $this = $(this)
    userAdminMessage.changePass.surePass = $this.val()
})
// 保存修改密码
$(document).on('click', '.J_UserAdminChangePass', function () {
    if (userAdminMessage.changePass.oldPass == "" || userAdminMessage.changePass.oldPass == null) {
        UserAdminChangeError('error', '请填写旧密码')
        return false;
    }
    if (userAdminMessage.changePass.newPass == "" || userAdminMessage.changePass.newPass == null) {
        UserAdminChangeError('error', '请填写新密码')
        return false;
    }
    if (userAdminMessage.changePass.surePass == "" || userAdminMessage.changePass.surePass == null) {
        UserAdminChangeError('error', '请填写确认密码')
        return false;
    }
    if (userAdminMessage.changePass.surePass != userAdminMessage.changePass.newPass) {
        UserAdminChangeError('error', '两次填写的密码不一致')
        return false;
    }
    $.star({
        type: 'POST',
        url: '/community-user/user/v1/info/changePwd',
        showerror: false,
        data: {
            password: userAdminMessage.changePass.newPass,
            oldPwd: userAdminMessage.changePass.oldPass
        },
        success: function (res) {
            $('#UserAdminModal').modal('hide')
            UserAdminChangeError('hide')
            spop({
                template: '保存成功',
                autoclose: 3000,
                style: 'success'
            });
        },
        error: function (req) {
            UserAdminChangeError('error', req.errorMsg)
        }
    });

})
// 修改密码错误提示
function UserAdminChangeError(type, message) {
    $('.UserAdminModal-swiper-ps-error').removeClass('showOpacity').removeClass('fadeIn')
    if (type != 'hide') {
        $('.UserAdminModal-swiper-ps-error').text(message)
        $('.UserAdminModal-swiper-ps-error').addClass('showOpacity').addClass('animated fadeIn')
        setTimeout(() => {
            $('.UserAdminModal-swiper-ps-error').removeClass('fadeIn');
        }, 1000)
    } else if (type == 'hide') {
        $('.UserAdminModal-swiper-ps-error').removeClass('showOpacity').removeClass('fadeIn')
    }
}
// 监听新手机
$(document).on('input', '.J_UserAdminModal-newPhone', function () {
    var $this = $(this)
    userAdminMessage.changePhone.phone = $this.val()
})
// 监听验证码
$(document).on('input', '.J_UserAdminModal-newCode', function () {
    var $this = $(this)
    userAdminMessage.changePhone.code = $this.val()
})
// 保存
$(document).on('click', '.J_UserAdminPhone', function () {
    if (userAdminMessage.changePhone.phone == "") {
        UserAdminPhoneError('error', translatesrting('请填写新手机号'))
        return false;
    }
    if (userAdminMessage.changePhone.code == "") {
        UserAdminPhoneError('error', translatesrting('请填写验证码'))
        return false;
    }
    $.star({
        type: 'POST',
        url: '/community-user/user/v1/info/changeMobile',
        showerror: false,
        data: {
            mobile: userAdminMessage.changePhone.phone,
            verifyCode: userAdminMessage.changePhone.code,
            countryCode: userAdminMessage.changePhone.countryCode,
        },
        success: function (res) {
            $('#UserAdminModal').modal('hide')
            UserAdminPhoneError('hide')
            spop({
                template: translatesrting('保存成功'),
                autoclose: 3000,
                style: 'success'
            });
        },
        error: function (req) {
            UserAdminPhoneError('error', req.errorMsg)
        }
    });
})
// 获取验证码
$(document).on('click', '.J_UserAdminModal-getCode', function () {
    var $this = $(this);
    var voice = false;
    if ($this.attr('data-type') == 'voice') {
        voice = true
    }
    if (userAdminMessage.changePhone.phone == "") {
        UserAdminPhoneError('error', translatesrting('请填写新手机号'))
        return false;
    }
    if (userAdminMessage.countloading) {
        return false
    }
    $('.UserAdminModal_ucShow_div').addClass('UserAdminModal_ucShow_div-show')
    $('.UserAdminModal_ucShow_div').html('<div id="nc"></div>')
    AWSC.use("nc", function (state, module) {
        var ncOption = {
            // 应用类型标识。它和使用场景标识（scene字段）一起决定了滑动验证的业务场景与后端对应使用的策略模型。您可以在阿里云验证码控制台的配置管理页签找到对应的appkey字段值，请务必正确填写。
            appkey: "FFFF0N00000000009F6B",
            //使用场景标识。它和应用类型标识（appkey字段）一起决定了滑动验证的业务场景与后端对应使用的策略模型。您可以在阿里云验证码控制台的配置管理页签找到对应的scene值，请务必正确填写。
            scene: "nc_message_h5",
            // 声明滑动验证需要渲染的目标ID。
            renderTo: "nc",
            //前端滑动验证通过时会触发该回调参数。您可以在该回调参数中将会话ID（sessionId）、签名串（sig）、请求唯一标识（token）字段记录下来，随业务请求一同发送至您的服务端调用验签。
            success: function (data) {
                $.star({
                    type: 'POST',
                    url: '/community-user/verify/v2/smsCode',
                    data: {
                        mobile: userAdminMessage.changePhone.phone,
                        type: 3,
                        countryCode: userAdminMessage.changePhone.countryCode,
                        voice: voice,
                        verifySmart: {
                            sign: data.sig,
                            sessionId: data.sessionId,
                            token: data.token,
                            scene: 'nc_message_h5'
                        }
                    },
                    showerror: false,
                    success: function (res) {
                        UserAdminModalSetInterval()
                        $('.J_UserAdminModal-newCode').focus()
                        setTimeout(() => {
                            $('.UserAdminModal_ucShow_div').removeClass('UserAdminModal_ucShow_div-show')
                        }, 2000)
                        UserAdminPhoneError('hide')
                    },
                    error: function (req) {
                        $('.UserAdminModal_ucShow_div').removeClass('UserAdminModal_ucShow_div-show')
                        UserAdminPhoneError('error', req.errorMsg)
                    }
                });
            },
            // 滑动验证失败时触发该回调参数。
            fail: function (failCode) {
                UserAdminPhoneError('error', translatesrting('验证码发送失败') + failCode)
            },
            // 验证码加载出现异常时触发该回调参数。
            error: function (errorCode) {
                UserAdminPhoneError('error', translatesrting('验证码发送失败') + errorCode)
            }
        } // 滑动验证初始化参数
        // 初始化 调用module.init对滑动验证进行初始化
        window.nc = module.init(ncOption);
    })
})

function UserAdminPhoneError(type, message) {
    $('.UserAdminModal-swiper-ph-error').removeClass('showOpacity').removeClass('fadeIn')
    if (type != 'hide') {
        $('.UserAdminModal-swiper-ph-error').text(message)
        $('.UserAdminModal-swiper-ph-error').addClass('showOpacity').addClass('animated fadeIn')
        setTimeout(() => {
            $('.UserAdminModal-swiper-ph-error').removeClass('fadeIn');
        }, 1000)
    } else if (type == 'hide') {
        $('.UserAdminModal-swiper-ph-error').removeClass('showOpacity').removeClass('fadeIn')
    }
}
// 倒计时
function UserAdminModalSetInterval(type) {
    if (type == 'hide') {
        clearInterval(userAdminMessage.countFun)
        userAdminMessage.countloading = false
        $('.J_UserAdminModal-getCode').removeClass('hide')
        $('.J_UserAdminModal-countTime').addClass('hide')
        userAdminMessage.countnum = 60
        return false
    }
    userAdminMessage.countloading = true
    userAdminMessage.countnum = 60
    userAdminMessage.countFun = setInterval(() => {
        if (userAdminMessage.countnum <= 0) {
            clearInterval(userAdminMessage.countFun)
            userAdminMessage.countloading = false
            $('.J_UserAdminModal-getCode').removeClass('hide')
            $('.J_UserAdminModal-countTime').addClass('hide')
            userAdminMessage.countnum = 60
            return false
        }
        $('.J_UserAdminModal-getCode').addClass('hide')
        $('.J_UserAdminModal-countTime').removeClass('hide')
        userAdminMessage.countnum = userAdminMessage.countnum - 1
        $('.J_UserAdminModal-countTime').text(userAdminMessage.countnum + 's')
    }, 1000)
}


function UserAdminModalGetMobilCode() {
    var code = 86
    if ($.cookie('countryCode')) {
        code = $.cookie('countryCode')
    }
    userAdminMessage.changePhone.countryCode = code
    $('.UserAdminModal-country.input-gx-menu').find('.input-beforcode').text('+' + userAdminMessage.changePhone.countryCode)
    $.star({
        type: "GET",
        url: '/community-user/verify/mobile/v1/code',
        data: "",
        showerror: true,
        success: function (res) {
            let codeHtml = ""
            codeHtml += '<div class="chooseCountryInput">' + '<input class="gx-input" placeholder="' + translatesrting('请告诉我您要搜索的区号') + '"/>' + '</div>'
            res.data.map((item) => {
                codeHtml += '<div class="gx-menu-li"><a class="chooseCountryCode" href="javascript:void(0)" ondragstart="return false" data-code="' + item.code + '">' + translatesrting(item.name) + ' ' + ' +' + item.code + '</a></div>'
            })
            $('.UserAdminModal-country.input-gx-menu').find('.gx-menu-ul').html(codeHtml)
            $('.UserAdminModal-country').gxmenu({
                height: "20px",
                top: "38px",
                clickHide: false//点击内部是否消失
            }, function (e) {

            })
        },
        error: function (req) {

        }
    })
}
// 选国家号
$(document).on('click', '.UserAdminModal-country .chooseCountryCode', function () {
    var $this = $(this);
    let chooseCode = $this.attr('data-code')
    $this.closest('.UserAdminModal-country').find('.input-beforcode').text('+' + chooseCode)
    userAdminMessage.changePhone.countryCode = chooseCode;
    $this.closest('.UserAdminModal-country').gxmenu({}, 'hide')
})
// 筛选家号
$(document).on('input', '.UserAdminModal-country .chooseCountryInput input', function () {
    var $this = $(this);
    var code = $this.val();
    $('.UserAdminModal-country').find('.chooseCountryCode').each(function () {
        if ($(this).attr('data-code').indexOf(code) != -1) {
            $(this).closest('.gx-menu-li').removeClass('hide')
        } else {
            $(this).closest('.gx-menu-li').addClass('hide')
        }
    })
})