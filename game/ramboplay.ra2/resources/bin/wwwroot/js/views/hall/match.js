var matchMessage = {
    type: 1,//匹配模式1: 1v1 2:2v2 3:3v3 4:4v4\
    countTime: 30,
    countTimefun: null,
    isReady: false,
    inTime: 0,
    inTimeFun: null,
    aginnum: 5,
    isfirst: true,
    updateMapManagerNum: 0,
    showtype: 'match',
    matchId: ''
}
var firstoutroom = false
// $(function () {
//     matchstar()
// })

function matchstar() {
    return false
    let matchTypes = getSaveMessage('matchTypes')
    if (matchTypes) {
        $('.Match-right-btn-matchtypes').html(template('Match-right-btn-matchtypes-script', { list: matchTypes }))
        if (matchMessage.isfirst) {
            matchMessage.isfirst = false
            matchBtnType('match', 'beforeMatch', 'first')
        }
        matchGame(matchTypes[0].gameType, 'first')
    } else {
        setTimeout(() => {
            matchstar()
        }, 1000)
    }
}
$(document).on('click', '.J_Match_chooseGame', function () {
    var $this = $(this)
    if ($this.hasClass('disabled')) {
        return false
    }
    if ($('#J_Match_start').length == 0) {
        return false
    } else if ($('#J_Match_start').hasClass('gx-disabled')) {
        return false
    }
    $('.J_Match_chooseGame').each(function () {
        $(this).removeClass('active')
    })
    matchGame($this.attr('data-type'))
})
function matchGame(gametype, isfirst) {
    $('.J_Match_chooseGame').each(function () {
        if ($(this).attr('data-type') == gametype) {
            $(this).addClass('active')
        }
    })
    matchMessage.type = gametype
    $.controlAjax({
        type: "get",
        url: '/api/lobby/game/rules/',
        showerror: true,
        data: { 'type': gametype, 'ladder': false },
        success: async function (res) {
            $('.Match-rule-div-list').html("")
            // --------图片数据处理
            let topMapList = res.data.maps
            let imgarray = new Map()
            for (var i = 0; i < topMapList.length; i++) {
                imgarray.set(topMapList[i].imageUrl + '?x-oss-process=image/resize,w_242', '')
            }
            let dellimgarray = await readerIndexedDBImgUrl(imgarray)
            for (list of topMapList) {
                let itemimg = list.imageUrl + '?x-oss-process=image/resize,w_242'
                if (dellimgarray.has(itemimg)) {
                    if (dellimgarray.get(itemimg)) {
                        list.localimageUrl = dellimgarray.get(itemimg)
                    } else {
                        list.localimageUrl = itemimg
                    }
                }
            }
            res.data.maps = topMapList
            // --------图片数据处理
            $('.Match-map-list').html(template('Match-map-list-script', { maplist: res.data.maps }))
            clearTimeout(hallMessage.overTimeDownMap)
            hallMessage.overTimeDownMap = setTimeout(function () {
                $('.Match-map-item-inload').each(function () {
                    $(this).find('.Match-map-item-retry').removeClass('hide')
                })
            }, 10000)
            let maplistarray = []
            // 游戏设置
            res.data.configs.map((item) => {
                if (item.key == 'Credits') {
                    $('.Match-rule-div-item-Credits').text(item.value)
                } else if (item.key == 'UnitCount') {
                    $('.Match-rule-div-item-UnitCount').text(item.value)
                } else if (item.key == 'Version') {
                    $('.Match-rule-div-item-Version').text(translatesrting(item.value))
                } else if (item.value == 'True' || item.value == 'true') {
                    maplistarray.push(item)
                }
            })
            // 开关
            if (!res.data.isOpen) {
                if (gametype == 8) {
                    $('.J_MatchNo').text(translatesrting('8人混战') + translatesrting('匹配暂不开放'))
                } else {
                    $('.J_MatchNo').text(gametype + 'V' + gametype + " " + translatesrting('匹配暂不开放'))
                }
                $('#J_Match_start').addClass('disabled')
            } else {
                $('.J_MatchNo').text('')
                $('#J_Match_start').removeClass('disabled')
            }
            $('#J_Match_start').attr('data-isOpen', res.data.isOpen)
            if (res.data.openTime) {
                $('.J_MatchNoTime').text(translatesrting('匹配开放时间') + '：' + res.data.openTime)
            } else {
                $('.J_MatchNoTime').text('')
            }
            $('[data-toggle="tooltip"]').tooltip()
            downmatchImg()
        },
        error: function (req) {
            if (isfirst == 'first' && matchMessage.aginnum > 0) {
                setTimeout(function () {
                    matchMessage.aginnum = matchMessage.aginnum - 1
                    matchGame(1, 'first')
                }, 300)
            }
        }
    })
}
// 开始匹配
$(document).on('click', '#J_Match_start', function () {
    player_sound_inrank()
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    if ($this.hasClass('disabledtwo')) {
        return false
    }
    if ($this.hasClass('disabled')) {
        if (matchMessage.type == 8) {
            var errormessage = translatesrting('8人混战') + translatesrting('匹配暂不开放')
        } else {
            var errormessage = matchMessage.type + 'v' + matchMessage.type + " " + translatesrting('匹配暂不开放')
        }
        spop({
            template: errormessage,
            autoclose: 1000,
            style: 'warning'
        });
        return false
    }
    trackingFunc('Match_start')
    $this.gxbtn('loading')
    $.controlAjax({
        type: "get",
        url: '/api/lobby/start/matching/',
        data: {
            'operatetype': 'Searching',
            'gametype': matchMessage.type,
            'ladder': false
        },
        success: function (res) {
            $this.gxbtn('reset')
            matchMessage.isReady = false
            matchBtnType('match', 'inMatch')
        },
        error: function (req) {
            $this.gxbtn('reset')
            if (req.errorCode == 250007) {
                let getTime = JSON.parse(req.errorData)
                // spop({
                //     template: translatesrting('您被封禁到') + formatDate(getTime.endTime),
                //     autoclose: 3000,
                //     style: 'warning'
                // });
                let time = getTime.endTime - getTime.sysTime
                let fengjin = '<div class="mt10" style="padding: 0px 22px;">' + getTime.reason + '<div>'
                fengjin += '<div class="fengjinTime" data-time="' + time + '">' + formatMatchTime(time) + '</div>'
                gxmodal({
                    title: translatesrting('进入匹配失败'),
                    centent: fengjin,
                    id: 'fengjinTime',
                    buttons: [
                        {
                            text: translatesrting('确定'),
                            class: 'btn-middle btn-middle-blue',
                            callback: function () {
                            }
                        }
                    ]
                })
                fengjinTimefun()
            } else {
                var errormessage = null
                if (req.errorMsg) {
                    errormessage = req.errorMsg
                } else {
                    errormessage = translatesrting('网络可能出现点问题,请稍后再试')
                }
                spop({
                    template: errormessage,
                    autoclose: 1000,
                    style: 'warning'
                });
                if (req.JerrorCode && req.JerrorCode == "inroom") {
                    if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
                        showhallGameRoom()
                    } else {
                        if (!$('.hall-header-returnroom').hasClass('hide')) {
                            $('.hall-header-returnroom').tooltip('show')
                            setTimeout(function () {
                                $('.hall-header-returnroom').tooltip('hide')
                            }, 3000)
                        }
                    }
                }
            }
        }
    })
})
// 取消匹配
$(document).on('click', '#J_Match_close', function () {
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    $this.gxbtn('loading')
    $.controlAjax({
        type: "get",
        url: '/api/lobby/start/matching/',
        showerror: true,
        data: {
            'operatetype': 'Cancel',
            gametype: matchMessage.type,
            'ladder': false
        },
        success: function (res) {
            $this.gxbtn('reset')
            matchBtnType('match', 'beforeMatch')
        },
        error: function (req) {
            $this.gxbtn('reset')
        }
    })
})
// 确定匹配
$(document).on('click', '#MatchSureBtn', function () {
    player_sound_click()

    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    $this.gxbtn('loading')
    $.controlAjax({
        type: "get",
        url: '/api/lobby/start/matching/',
        showerror: true,
        data: {
            window: 'createroom',
            url: '/Details/match',
            width: 1280,
            height: 720,
            operatetype: 'Confirm',
            gametype: matchMessage.type
        },
        success: function (res) {
            $this.gxbtn('reset')
            matchMessage.isReady = true
            matchBtnType('modal', 'sure')
            // 渲染
            $('.matchModal-user-list .match-user-avatar').each(function () {
                if ($(this).attr('data-id') == userInfo.userId) {
                    $(this).addClass('match-user-avatar-success')
                    var adduser = Number($('#match-modal-num').attr('data-num')) + 1
                    var maxuser = $('#match-modal-num').attr('data-max')
                    if (adduser > maxuser) {
                        adduser = maxusermaxuser
                    }
                    $('#match-modal-num').text(adduser + '/' + $('#match-modal-num').attr('data-max'))
                }
            })
        },
        error: function (req) {
            $this.gxbtn('reset')
        }
    })
})
// 放弃准备
$(document).on('click', '#MatchSureCloseBtn', function () {
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    matchCloseQueue($this)
})
function matchCloseQueue(obj) {
    obj.gxbtn('loading')
    $.controlAjax({
        type: "get",
        url: '/api/lobby/start/matching/',
        showerror: true,
        data: {
            'operatetype': 'Reject',
            gametype: matchMessage.type
        },
        success: function (res) {
            obj.gxbtn('reset')
            if (matchMessage.showtype == 'rank') {
                rankBtnType('rank', 'beforeRank')
            } else if (matchMessage.showtype == 'match') {
                matchBtnType('match', 'beforeMatch')
            }
            $('#matchUserModal').modal('hide')
        },
        error: function (req) {
            obj.gxbtn('reset')
        }
    })
}
// 倒计时
function matchCountTime() {
    $('#matchUserModalCountTime').text(matchMessage.countTime)
    clearInterval(matchMessage.countTimefun)
    matchMessage.countTimefun = setInterval(function () {
        matchMessage.countTime = matchMessage.countTime - 1
        let ladder = $('#matchUserModal').attr('data-ladder')
        if (matchMessage.countTime < 0) {
            $('#MatchSureCloseBtnclosemodal').attr('data-dismiss', 'modal')
            $('#MatchSureCloseBtnclosemodal').text(translatesrting('取消'))
            $('#MatchSureCloseBtnclosemodal').on('click', function () {
                matchCloseQueue($('#MatchSureCloseBtnclosemodal'))
            })
            clearInterval(matchMessage.countTimefun)
            if (!matchMessage.isReady) {
                $.controlAjax({
                    type: "get",
                    url: '/api/lobby/start/matching/',
                    data: {
                        'operatetype': 'Reject',
                        gametype: matchMessage.type,
                        ladder: ladder
                    },
                    success: function (res) {
                        $('#matchUserModal').modal('hide')
                        matchBtnType(matchMessage.showtype, 'beforeMatch')
                    },
                    error: function (req) {
                        $('#matchUserModal').modal('hide')
                        if (req.errorCode == 250007) {
                            let getTime = JSON.parse(req.errorData)
                            spop({
                                template: '您被封禁到' + formatDate(getTime.endTime),
                                autoclose: 3000,
                                style: 'warning'
                            });
                        } else {
                            spop({
                                template: req.errorMsg,
                                autoclose: 1000,
                                style: 'warning'
                            });
                        }
                    }
                })
            }
            return false;
        }
        $('#matchUserModalCountTime').text(matchMessage.countTime)
    }, 1000)
}
// 匹配弹窗出现
$(document).on('show.bs.modal', '#matchUserModal', function () {
    matchMessage.isReady = false
    matchCountTime()
    matchBtnType('modal', 'init')
})
// 匹配弹窗消失
$(document).on('hidden.bs.modal', '#matchUserModal', function () {
    clearInterval(matchMessage.countTimefun)
    matchMessage.isReady = false
})
// 更新
var UpdataModal = {
    canShow: false,
    noclose: false,
    aniamtefun: null,
    progressnum: 0,
    overTime: null
}
// 更新按钮
$(document).on('click', '.J_Upgrade', function () {
    ipcRenderer.send('Main', {
        msgType: "CheckUpdate",
        jsonInfo: {
            status: 0
        }
    });
    UpdataModal.canShow = true
})
// 点击更新
$(document).on('click', '.J_updataBtn', function () {
    $(this).button('loading')
    ipcRenderer.send('Main', {
        msgType: "CheckUpdate",
        jsonInfo: {
            status: 1
        }
    });
    $('.updata-status').html(template('updata-in-script', { nowPercentage: "1" }))
    clearInterval(UpdataModal.aniamtefun)
    // autoUpdata()
})
// 更新前端动画
function autoUpdata() {
    UpdataModal.aniamtefun = setInterval(() => {
        let num = (90 - UpdataModal.progressnum) / 100
        UpdataModal.progressnum = (UpdataModal.progressnum + num).toFixed(2)
        UpdataModal.progressnum = Math.floor(UpdataModal.progressnum * 100) / 100
        $('.updata-status').find('.gx-progress-bar').css('width', UpdataModal.progressnum + "%")
        if (UpdataModal.progressnum > 89) {
            clearInterval(UpdataModal.aniamtefun)
        }
    }, 240)
    // 8分钟补偿
    clearTimeout(UpdataModal.overTime)
    UpdataModal.overTime = setTimeout(() => {
        $('.updata-in-text').html(translatesrting('网络可能出现了点问题，请重启客户端'))
    }, 480000)
}
$(document).on('hidden.bs.modal', '#UpdataModal', function () {
    clearInterval(UpdataModal.aniamtefun)
})
var getpingnegative = 0//-1连续3次
//监听
ipcRenderer.on('WebIpc', (event, message) => {
    if (message.msgType == 18) {
        var pingmessage = JSON.parse(message.jsonInfo)
        if (pingmessage.serverStatus) {
            let serverStatus = pingmessage.serverStatus
            let wsStatus = pingmessage.wsStatus == 0 ? translatesrting('连接中') : pingmessage.wsStatus == 1 ? translatesrting('正常') : pingmessage.wsStatus == 10 ? translatesrting('断开') : ''
            let tunnelPing = pingmessage.tunnelPing
            let tunnelPkLose = pingmessage.tunnelPkLose
            if (Number(serverStatus) == -1) {
                getpingnegative = getpingnegative + 1
            } else {
                getpingnegative = 0
            }
            if (Number(serverStatus) >= 2000 || getpingnegative >= 3) {
                $('.hall-header-ping-right.serverStatus').text(translatesrting('异常'))
            } else {
                $('.hall-header-ping-right.serverStatus').text(translatesrting('正常'))
            }
            $('.hall-header-ping-right.wsStatus').text(wsStatus)
            $('.hall-header-ping-right.tunnelPing').text(tunnelPing + 'ms')
            $('.hall-header-ping-right.tunnelPkLose').text(tunnelPkLose + '%')
            let status = 1
            if (Number(serverStatus) >= 2000 || getpingnegative >= 3) {
                status = 3
                $('.hall-header-ping-right.serverStatus').closest('.hall-header-ping-hover-list').attr('class', 'hall-header-ping-hover-list congestion')
                // } else if (Number(serverStatus) >= 500) {
                //     if (status < 2) { status = 2 }
                //     $('.hall-header-ping-right.serverStatus').closest('.hall-header-ping-hover-list').attr('class', 'hall-header-ping-hover-list busy')
            } else {
                if (status < 1) { status = 1 }
                $('.hall-header-ping-right.serverStatus').closest('.hall-header-ping-hover-list').attr('class', 'hall-header-ping-hover-list')
            }
            if (pingmessage.wsStatus == 10) {
                status = 3
                $('.hall-header-ping-right.wsStatus').closest('.hall-header-ping-hover-list').attr('class', 'hall-header-ping-hover-list congestion')
            } else if (pingmessage.wsStatus == 0) {
                if (status < 2) { status = 2 }
                $('.hall-header-ping-right.wsStatus').closest('.hall-header-ping-hover-list').attr('class', 'hall-header-ping-hover-list busy')
            } else {
                if (status < 1) { status = 1 }
                $('.hall-header-ping-right.wsStatus').closest('.hall-header-ping-hover-list').attr('class', 'hall-header-ping-hover-list')
            }
            if (tunnelPing >= 200) {
                status = 3
                $('.hall-header-ping-right.tunnelPing').closest('.hall-header-ping-hover-list').attr('class', 'hall-header-ping-hover-list congestion')
            } else if (tunnelPing >= 100) {
                if (status < 2) { status = 2 }
                $('.hall-header-ping-right.tunnelPing').closest('.hall-header-ping-hover-list').attr('class', 'hall-header-ping-hover-list busy')
            } else {
                if (status < 1) { status = 1 }
                $('.hall-header-ping-right.tunnelPing').closest('.hall-header-ping-hover-list').attr('class', 'hall-header-ping-hover-list')
            }
            if (tunnelPkLose >= 30) {
                status = 3
                $('.hall-header-ping-right.tunnelPkLose').closest('.hall-header-ping-hover-list').attr('class', 'hall-header-ping-hover-list congestion')
            } else if (tunnelPkLose >= 10) {
                if (status < 2) { status = 2 }
                $('.hall-header-ping-right.tunnelPkLose').closest('.hall-header-ping-hover-list').attr('class', 'hall-header-ping-hover-list busy')
            } else {
                if (status < 1) { status = 1 }
                $('.hall-header-ping-right.tunnelPkLose').closest('.hall-header-ping-hover-list').attr('class', 'hall-header-ping-hover-list')
            }
            if (status == 3) {
                $('.hall-header-ping-span>.iconfont').css('color', '#FF4444')
            } else if (status == 2) {
                $('.hall-header-ping-span>.iconfont').css('color', '#FFA333')
            } else {
                $('.hall-header-ping-span>.iconfont').css('color', '#A2FF33')
            }
        }
    } else if (message.msgType == 23) {
        var Matchipc = JSON.parse(message.jsonInfo)
        var ladder = null
        if (Matchipc.roomInfo) {
            ladder = Matchipc.roomInfo.ladder
        }
        // var ladder = Matchipc.roomInfo.ladder
        if (ladder) {
            matchMessage.showtype = 'rank'
        } else {
            matchMessage.showtype = 'match'
        }
        $('#matchUserModal').attr('data-ladder', ladder)
        if (Matchipc.type == 0) {
            // 匹配中
            if (ladder) {
                rankBtnType('rank', 'inRank')
            } else {
                matchBtnType('match', 'inMatch')
            }
            $('#matchUserModal').modal('hide')
            matchMessage.matchId = ''
        } else if (Matchipc.type == 1) {
            // 匹配成功进入准备
            matchMessage.matchId = Matchipc.matchId
            localStorage.removeItem('msgType2')
            $('#matchUserModal .matchModal-user-list').html(template('matchModal-user-list-script', { list: Matchipc.players, ladder: ladder, myid: userInfo.userId }))
            var isreadynum = 0
            Matchipc.players.map((item) => {
                if (item.confirmStatus) {
                    isreadynum = isreadynum + 1
                }
            })
            $('#match-modal-num').attr('data-num', isreadynum)
            $('#match-modal-num').attr('data-max', Matchipc.players.length)
            $('#match-modal-num').text(isreadynum + '/' + Matchipc.players.length)

            if (!$('#matchUserModal').hasClass('in') && !localStorage.getItem('isdetails')) {
                matchMessage.countTime = Matchipc.roomInfo.confirmTime
                ipcRenderer.send('Main', {
                    msgType: "WindowSize",
                    jsonInfo: {
                        window: 'main',
                        size: 'onlyShow'
                    }
                });
                $('#matchUserModal').modal('show')
                playMusic($('#matchIn')[0])
            }

            if (ladder) {
                rankBtnType('rank', 'inRoom')
                trackingFunc('rank_ready', rankMessage.inTime)
            } else {
                matchBtnType('match', 'inRoom')
                trackingFunc('match_ready', matchMessage.inTime)
            }
        } else if (Matchipc.type == 2) {
            // 匹配准备成功！
        } else if (Matchipc.type == 3) {
            // 匹配超时 当前用户
            if (ladder) {
                rankBtnType('rank', 'beforeRank')
            } else {
                matchBtnType('match', 'beforeMatch')
            }
            spop({
                template: '匹配超时',
                autoclose: 3000,
                style: 'warning'
            });
            $('#matchUserModal').modal('hide')
        } else if (Matchipc.type == 4) {
            // 匹配取消 
            $('#matchUserModal').modal('hide')

            if (ladder) {
                rankBtnType('rank', 'beforeRank')
                trackingFunc('rank_close4', matchMessage.inTime)
            } else {
                matchBtnType('match', 'beforeMatch')
                trackingFunc('match_close4', matchMessage.inTime)
            }
            matchBtnType('match', 'beforeMatch')
        } else if (Matchipc.type == 5) {
            // 匹配失败！
            if (ladder) {
                rankBtnType('rank', 'beforeRank')
            } else {
                matchBtnType('match', 'beforeMatch')
                trackingFunc('match_fail', matchMessage.inTime)
            }
            spop({
                template: '匹配失败',
                autoclose: 3000,
                style: 'error'
            });
            $('#matchUserModal').modal('hide')
        } else if (Matchipc.type == 6) {
            // 匹配并全部准备
            $('#matchUserModal').modal('hide')

            if (ladder) {
                rankBtnType('rank', 'inRoom')
            } else {
                matchBtnType('match', 'inRoom')
            }
        } else if (Matchipc.type == 7) {
            // ！匹配取消 自己取消
            spop({
                template: '匹配失败',
                autoclose: 3000,
                style: 'error'
            });
            $('#matchUserModal').modal('hide')
            if (matchMessage.matchId == Matchipc.matchId) {
                if (ladder) {
                    rankBtnType('rank', 'beforeRank')
                    trackingFunc('rank_close7', matchMessage.inTime)
                } else {
                    matchBtnType('match', 'beforeMatch')
                    trackingFunc('match_close7', matchMessage.inTime)
                }
            }
        } else if (Matchipc.type == 8) {
            $('#matchUserModal').modal('hide')
            matchBtnType('match', 'beforeMatch')
            rankBtnType('rank', 'beforeRank')
            showhallGameRoom('close')
            hallContentSwiper(0)
            // if (ladder) {
            //     rankBtnType('rank', 'beforeRank')
            // } else {
            //     matchBtnType('match', 'beforeMatch')
            // }
        }
    } else if (message.msgType == 25) {
        // 游戏测试
        if (newHelpMessage.commonHelping) {
            return false
        }
        $('#GamebeforModal').modal('show')
        let resback = JSON.parse(message.jsonInfo)
        if (resback.IsAres) {
            $('.GamebeforModal-title').text(translatesrting('Ares检测'))
        } else {
            $('.GamebeforModal-title').text(translatesrting('游戏测试！'))
        }

        $('#GamebeforModal .modal-dialog #GamebeforModal-content').html(template('GamebeforModal-script', {
            type: 'get',
            Completeness: resback.Completeness,
            GameTestStatus: resback.GameTestStatus,
        }))
        if (JSON.parse(message.jsonInfo).Completeness && JSON.parse(message.jsonInfo).GameTestStatus) {
            if (resback.IsAres) {
                trackingFunc('check_success_ares')
            } else {
                trackingFunc('check_success_game')
            }
            saveMessage('gameBefor', {
                complete: true,
                Completeness: true,
                GameTestStatus: true
            })
        } else {
            saveMessage('gameBefor', { complete: false, Completeness: JSON.parse(message.jsonInfo).Completeness, GameTestStatus: JSON.parse(message.jsonInfo).GameTestStatus })
            if (resback.IsAres) {
                trackingFunc('check_fail_ares')
            } else {
                trackingFunc('check_fail_game')
            }
        }
    } else if (message.msgType == 20) {
        var updateMessage = JSON.parse(message.jsonInfo)

        if (updateMessage.sign == 'updateMapManager') {
            let updatepar = Number(updateMessage.nowPercentage)
            let updateparlocal = matchMessage.updateMapManagerNum
            if (updatepar == 100) {
                $('#DownMapModal').modal('hide')
                matchMessage.updateMapManagerNum = 0
                return false
            }
            if (!$('#DownMapModal').hasClass('in')) {
                $('#DownMapModal').modal('show')
                $('#DownMapModal .modal-dialog').html(template('DownMapModal-script'))
            }
            if (updateMessage.speed) {
                $('#DownMapModal .DownMapModal-content .DownMapModal-speed').text(updateMessage.speed)
            }
            if (updateMessage.total) {
                $('#DownMapModal .DownMapModal-content .DownMapModal-total').text(updateMessage.total + 'MB')
                $('#DownMapModal .DownMapModal-content .DownMapModal-get').text(Number(updateMessage.total) * updatepar / 100)
            }
            if (updatepar > updateparlocal) {
                matchMessage.updateMapManagerNum = updatepar
                $('#DownMapModal .DownMapModal-content .DownGameModal-bor').css('width', updatepar + "%")
            }
        }
        if (updateMessage.sign == 'updateClient' && Number(updateMessage.nowPercentage) > Number(UpdataModal.progressnum)) {
            UpdataModal.progressnum = Number(updateMessage.nowPercentage)
            if ($('#UpdataModal .updata-status .gx-progress-bar').length > 0) {
                $('#UpdataModal .updata-status .gx-progress-bar').css('width', updateMessage.nowPercentage + "%")
            } else {
                $('#UpdataModal .updata-status').html(template('updata-in-script', { nowPercentage: updateMessage.nowPercentage }))
            }
            // if (jsonmessage.speed) {
            //     $('.container-speed .container-speed-div').removeClass('hide')
            //     $('.container-speed').find('.speed').text(jsonmessage.speed)
            // }
            // if (jsonmessage.timeLeft) {
            //     $('.container-speed').find('.timeLeft').text(jsonmessage.timeLeft)
            // }
        }
        if ($('.Match-map-item-inload').length > 0) {
            var mapSha1 = updateMessage.sign
            var isupdata = false
            $('.Match-map-item-inload').each(function () {
                if ($(this).attr('data-mapSha') == mapSha1) {
                    $(this).attr('data-load', updateMessage.nowPercentage)
                    isupdata = true
                }
            })
            if (isupdata) {
                downmatchImg()
                downrankImg()
            }
        }

    } else if (message.msgType == 21 && UpdataModal.canShow) {
        // 更新弹框
        // ipcRenderer.send('Main', {
        //     msgType: "ShowModal",
        //     jsonInfo: {
        //         window: 'UpdateGame',
        //         url: '/Modal/UpdateCheck',
        //         width: 400,
        //         height: 200,
        //     }
        // });
        var updateMessage = JSON.parse(message.jsonInfo)
        if (updateMessage.version != "") {
            $('#UpdataModal .updata-status').html(template('updata-status-script', { message: updateMessage, UpdataModal: UpdataModal }))
            if (UpdataModal.noclose) {
                $('#UpdataModal .header-window').addClass('hide')
            } else {
                $('#UpdataModal .header-window').removeClass('hide')
            }
            $('#UpdataModal').modal('show')
        } else {
            spop({
                template: '服务器繁忙，暂时获取不到版本号',
                autoclose: 1000,
                style: 'warning'
            });
        }
        UpdataModal.canShow = false
        UpdataModal.noclose = false
    } else if (message.msgType == 19 && JSON.parse(message.jsonInfo).code == 301) {
        // 检测弹框
        if (!newHelpMessage.commonHelping) {
            localStorage.removeItem('noGameUrl')
            // $('.J_showCheckGameBefor').click()
            localStorage.removeItem('gameBefor')
            // $('.GamebeforModal-title').text(translatesrting('游戏测试！'))
            // $('#GamebeforModal .modal-dialog #GamebeforModal-content').html(template('GamebeforModal-script', { type: 'init', isfirst: true }))
            // $('#GamebeforModal').modal('show')
            ipcRenderer.send('Main', {
                msgType: "Close",
                jsonInfo: {
                    window: "settingmodal"
                }
            });
            let tourl = ''
            if (JSON.parse(message.jsonInfo).msg.indexOf('Ares') != -1) {
                tourl = 'ares'
            } else {
                tourl = 'game'
            }
            setTimeout(function () {
                showWindowModal('settingmodal', '/Modal/setting?page=gamesetingTest&attention=' + tourl)
            }, 200)
            trackingFunc('check_modal')
            if (hallMessage.indexTo == 2) {
                // 帮助自动下载匹配地图
                $('.J_Match_chooseGame.active').click()
            }
        }
    } else if (message.msgType == 26) {
        var Matchipc = JSON.parse(message.jsonInfo)
        // if (Matchipc.id) {
        //     showSettlement(Matchipc.id, true, Matchipc.playGameType)
        // }
        ipcRenderer.send('Main', {
            msgType: "WindowSize",
            jsonInfo: {
                window: 'main',
                size: 'onlyShow'
            }
        });
        saveMessage('ClientBGM', 'false')
        wsUserStatus(1)
        // 3匹配 4天梯
        if (Matchipc.playGameType == 1) {
            // hallContentSwiper(0)
        } else if (Matchipc.playGameType == 3) {
            hallContentSwiper(2)
            // showhallGameRoom('close')
        } else if (Matchipc.playGameType == 4) {
            hallContentSwiper(1)
            // showhallGameRoom('close')
        }
        if (Matchipc.playGameType == 1) {
            // firstoutroom = true
            $.controlAjax({
                type: "get",
                url: '/api/lobby/room/user/status/',
                data: {
                },
                success: function (res) {
                    if (res.data.inRoom) {

                    } else {
                        showhallGameRoom('close')
                        wsUserStatus(1)
                        hallContentSwiper(0)
                    }
                },
                error: function (req) {
                    // firstoutroom = false
                    // hallContentSwiper(0)
                    // showhallGameRoom('close')
                }
            })
        }
    } else if (message.msgType == 19 && JSON.parse(message.jsonInfo).code == 303) {
        ipcRenderer.send('Main', {
            msgType: "CheckUpdate",
            jsonInfo: {
                status: 0
            }
        });
        UpdataModal.canShow = true
        UpdataModal.noclose = true
    } else if (message.msgType == 24) {
        //关闭iframe导致窗口无法拖动
        $('.hall-header').css('-webkit-app-region', 'no-drag')
        setTimeout(() => {
            $('.hall-header').css('-webkit-app-region', 'drag')
        }, 1000)
    } else if (message.msgType == 32) { // 32: 扫描游戏文件完成
        const jsonmessage = JSON.parse(message.jsonInfo)
        const NewHelp = $('.NewHelp-text-content');
        if (jsonmessage && jsonmessage.length > 0) {
            // 扫描完成-发现游戏
            NewHelp.find('.content').hide();
            NewHelp.find('.next-data2').show();
            NewHelp.find('.nav').text(translatesrting('我们在您的电脑中找到下列支持兰博玩对战平台联机的游戏'))
            NewHelp.find('.game-path').text(jsonmessage[0])
        } else {
            // 扫描完成-未发现游戏
            NewHelp.find('.content').hide();
            NewHelp.find('.next-data3').show();
            NewHelp.find('.nav').text(translatesrting('您的电脑中没有任何支持兰博玩对战平台联机的游戏')).css('color', '#FF4444')
        }
    }
});

/**
 * 新手引导第一步流程代码
 * 开始-->
 * */

// 取消搜索游戏-跳转引导下载页
$(document).on('click', '.J-Cancel-search', function () {
    ipcRenderer.send('Main', {
        msgType: "FindGamePath",
        jsonInfo: {
            status: true
        }
    });
    const NewHelp = $('.NewHelp-text-content');
    NewHelp.find('.content').hide();
    NewHelp.find('.next-data3').show();
    NewHelp.find('.nav').text('您的电脑中没有任何支持兰博玩对战平台联机的游戏').css('color', '#FF4444')
})

// 下载游戏文件
$(document).on('click', 'downloadGames', function () {
    const NewHelp = $('.NewHelp-text-content');
    ipcRenderer.send('Main', {
        msgType: "DownLoadFile",
        jsonInfo: {
            isCancel: false,
            Sign: 'game',
            type: '0'
        }
    });
    NewHelp.find('.content').hide();
    NewHelp.find('.next-data4').show();
    NewHelp.find('.nav').text('您的电脑中没有任何支持兰博玩对战平台联机的游戏').css('color', '#FF4444')
})

function matchBtnType(type, status, isfirst) {
    if (type == 'match') {
        $('.match-startBtn-div').html(template('match-startBtn-div-script', { 'status': status, isfirst: isfirst }))
        if (status == 'inMatch') {
            $('.hall-header-refresh').addClass('hide')//匹配中禁止刷新
            wsUserStatus(5)
            matchTime()
        } else {
            $('.hall-header-refresh').removeClass('hide')//匹配中禁止刷新
            if (isfirst != "first") {
                wsUserStatus(1)
            }
            matchTime('clear')
        }
        if (status == 'beforeMatch') {
            $('.J_MatchNoTime').removeClass('hide')
        } else {
            $('.J_MatchNoTime').addClass('hide')
        }

    } else if (type == 'modal') {
        matchTime('clear')
        player_sound_click()
        $('.match-modal-btn-div').html(template('match-modal-btn-div-script', { 'status': status }))
    }
}


// 匹配计时
function matchTime(type) {
    if (type == 'clear') {
        clearInterval(matchMessage.inTimeFun)
        matchMessage.inTime = 0
    } else {
        clearInterval(matchMessage.inTimeFun)
        $('.Match-start-btn-in-time').text(formatMatchTime(matchMessage.inTime))
        matchMessage.inTimeFun = setInterval(function () {
            matchMessage.inTime = matchMessage.inTime + 1
            $('.Match-start-btn-in-time').text(formatMatchTime(matchMessage.inTime))
            if (matchMessage.inTime > 600) {
                // 10分钟补偿
                $('#J_Match_close').click()
            }
        }, 1000)
    }
}
