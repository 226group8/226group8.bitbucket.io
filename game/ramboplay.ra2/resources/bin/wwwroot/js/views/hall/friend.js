
$('.hall-content-friend').html(template('hall-content-friend-script'))

var friendMessage = {
    first: true,//第一次初始化
    init: null,//初始化
    openfun: null,//滑动延迟
    isClickAdd: false,
    myfriendList: [],//我的好友
    ApplyFriends: [],//好友申请
    RecentFriends: [],//最近好友
    BlackFriends: [],//黑名单
    BlackFriendslimit: 30,//黑名单
    addfriendloading: false//添加好友loading
}


$(document).on('mouseenter', '.hall-content-friend-fix', function () {
    var $this = $(this)
    friendMessage.openfun = setTimeout(() => {
        friendMessage.openfun = null
        $this.addClass('active')
        let details = $('.hall-content-friend-fix-details')
        let narrow = $('.hall-content-friend-fix-narrow')
        details.css('display', 'block')
        details.animate({ opacity: '1' }, 300, 'linear', function () {
            details.css('opacity', '1')
        })
        narrow.css('display', 'none')
        narrow.animate({ opacity: '0' }, 0, 'linear', function () {
            narrow.css('opacity', '0')
        })
    }, 200)
})
$(document).on('mouseleave', '.hall-content-friend-fix', function () {
    if (friendMessage.openfun) {
        clearInterval(friendMessage.openfun)
        friendMessage.openfun = null
        return false
    }
    if (friendMessage.isClickAdd) {
        return false
    }
    // 清除右键
    $('.friendRCM').remove()
    $('.hall-content-friend .popover.fade.left.in').remove()
    var $this = $(this)
    $this.removeClass('active')
    let details = $('.hall-content-friend-fix-details')
    let narrow = $('.hall-content-friend-fix-narrow')
    details.css('display', 'none')
    details.animate({ opacity: '0' }, 0, 'linear', function () {
        details.css('opacity', '0')
    })
    narrow.css('display', 'block')
    narrow.animate({ opacity: '1' }, 300, 'linear', function () {
        narrow.css('opacity', '1')
    })
})
friendMessage.init = function (type, message) {
    if (friendMessage.first) {
        friendMessage.first = false
        $.star({
            type: 'GET',
            url: '/community-user/friend/v1/list',
            showerror: true,
            data: {
            },
            success: function (res) {
                if (res.data.userFriendRequestList) {
                    friendMessage.ApplyFriends = res.data.userFriendRequestList
                }
                if (res.data.recentGamersList) {
                    friendMessage.RecentFriends = res.data.recentGamersList
                }
                if (res.data.userFriendList) {
                    friendMessage.myfriendList = res.data.userFriendList
                }
                if (res.data.userBlackVOList) {
                    friendMessage.BlackFriends = res.data.userBlackVOList
                    friendMessage.BlackFriendslimit = res.data.blackUserLimit
                    localStorage.setItem('Ra_BlackFriends', JSON.stringify(friendMessage.BlackFriends))
                }
                friendMessage.template()
            },
            error: function (req) {
            }
        });
    } else {
        let data = message.data
        if (type == 1) {
            // 1: 用户状态
            let myfriendList = friendMessage.myfriendList
            for (var i = 0; i < myfriendList.length; i++) {
                if (myfriendList[i].userId == data.userId) {
                    myfriendList[i].status = data.status
                    myfriendList[i].description = data.description
                }
            }
            // 1: 最近状态
            let RecentFriends = friendMessage.RecentFriends
            for (var i = 0; i < RecentFriends.length; i++) {
                if (RecentFriends[i].userId == data.userId) {
                    RecentFriends[i].status = data.status
                    RecentFriends[i].description = data.description
                }
            }
            friendMessage.myfriendList = myfriendList
            friendMessage.RecentFriends = RecentFriends
        }
        if (type == 2) {
            // 2：申请好友
            let ApplyFriends = friendMessage.ApplyFriends
            let isinit = false
            for (var i = 0; i < ApplyFriends.length; i++) {
                if (ApplyFriends[i].userId == data.userId) {
                    isinit = true
                }
            }
            if (!isinit) {
                ApplyFriends.push(data)
            }
            friendMessage.ApplyFriends = ApplyFriends
        }
        if (type == 3) {
            // 3：添加好友
            let recipient = data.recipient
            let applicant = data.applicant
            let addarray = ""
            if (recipient.userId == userInfo.userId) {
                addarray = applicant
            } else if (applicant.userId == applicant.userId) {
                addarray = recipient
            }
            let myfriendList = friendMessage.myfriendList
            let RecentFriends = friendMessage.RecentFriends

            for (var i = 0; i < RecentFriends.length; i++) {
                if (RecentFriends[i].userId == addarray.userId) {
                    RecentFriends.splice(i, 1)
                }
            }
            friendMessage.RecentFriends = RecentFriends
            myfriendList.push(addarray)
            friendMessage.myfriendList = myfriendList
        }
        if (type == 4) {
            // 4: 删除好友
            let myfriendList = friendMessage.myfriendList
            for (var i = 0; i < myfriendList.length; i++) {
                if (myfriendList[i].userId == data.userId) {
                    myfriendList.splice(i, 1)
                }
            }
            friendMessage.myfriendList = myfriendList
        }
        if (type == 6) {
            // 6: 最近好友
            let RecentFriends = friendMessage.RecentFriends
            let myfriendList = friendMessage.myfriendList
            let trueRecentFriends = []
            // 去除好友
            for (var i = 0; i < data.recentGameList.length; i++) {
                let isinit = false
                for (var z = 0; z < myfriendList.length; z++) {
                    if (data.recentGameList[i].userId == myfriendList[z].userId) {
                        isinit = true
                    }
                }
                if (data.recentGameList[i].userId == userInfo.userId) {
                    isinit = true
                }
                if (!isinit) {
                    trueRecentFriends.push(data.recentGameList[i])
                }
            }
            // 增加最近好友
            for (var i = 0; i < trueRecentFriends.length; i++) {
                let init = false
                for (var z = 0; z < RecentFriends.length; z++) {
                    if (trueRecentFriends[i].userId == RecentFriends[z].userId) {
                        init = true
                    }
                }
                if (!init) {
                    RecentFriends.push(trueRecentFriends[i])
                }
            }
            friendMessage.RecentFriends = RecentFriends
        }
        if (type == 10) {
            // 黑名单删除
            let myfriendList = friendMessage.BlackFriends
            for (var i = 0; i < myfriendList.length; i++) {
                if (myfriendList[i].userId == data.userId) {
                    myfriendList.splice(i, 1)
                }
            }
            friendMessage.BlackFriends = myfriendList
            localStorage.setItem('Ra_BlackFriends', JSON.stringify(friendMessage.BlackFriends))
        }
        if (type == 9) {
            // 黑名单添加
            let myfriendList = friendMessage.BlackFriends
            myfriendList.push(data)
            friendMessage.BlackFriends = myfriendList
            localStorage.setItem('Ra_BlackFriends', JSON.stringify(friendMessage.BlackFriends))
        }
        friendMessage.template()
        // friendMessage.ApplyFriends
        // friendMessage.RecentFriends
        // friendMessage.myfriendList

    }
}
friendMessage.template = function () {
    // 好友列表
    let friendlist = friendMessage.myfriendList
    let infriend = []//在线
    let busyfriend = []//忙碌
    let overfriend = []//离线
    let reg = /^\s+|\s+$/g;
    friendlist.sort(function (a, b) {
        let aname = a.remark ? a.remark : a.username
        let bname = b.remark ? b.remark : b.username
        var af = aname.replace(reg, "").charAt(0);
        var bf = bname.replace(reg, "").charAt(0);
        if (af.localeCompare(bf) > 0) {
            return 1;
        } else if (af.localeCompare(bf) < 0) {
            return -1;
        } else {
            return 0;
        }
    });

    for (let i = 0; i < friendlist.length; i++) {
        if (friendlist[i].status == 2) {
            overfriend.push(friendlist[i])
        } else if (friendlist[i].status == 1) {
            infriend.push(friendlist[i])
        } else {
            busyfriend.push(friendlist[i])
        }
    }
    let showfriendlist = infriend.concat(busyfriend).concat(overfriend)
    let numfriend = infriend.length + busyfriend.length + overfriend.length
    let numinfriend = infriend.length + busyfriend.length
    $('.hall-content-friend-numtext').text('(' + numinfriend + '/' + numfriend + ')')
    $('.hall-content-friend-numtext.blackfriend').text('(' + friendMessage.BlackFriends.length + '/' + friendMessage.BlackFriendslimit + ')')
    // 外层好友显示
    let outlist = []
    for (var i = 0; i < showfriendlist.length; i++) {
        if (i <= 10) {
            outlist.push(showfriendlist[i])
        }
    }
    let reshtml = template("hall-content-friend-fix-narrow-div-script", { list: outlist })
    $('.hall-content-friend-fix-narrow-div').html(reshtml)
    // 展开好友显示
    friendMessage.myfriendList = friendlist
    let resDetailHtml = template("hall-content-friend-item-script", { list: showfriendlist, type: 'myfriend' })
    $('.hall-content-friend-ul-myfriend').html(resDetailHtml)
    if (friendMessage.myfriendList && friendMessage.myfriendList.length > 0) {
        setTimeout(function () {
            frindeRCM()
        }, 200)
    }
    if (friendMessage.BlackFriends && friendMessage.BlackFriends.length > 0) {
        setTimeout(function () {
            frindeBlackRCM()
        }, 200)
    }
    // 邀请列表
    let ApplyFriends = friendMessage.ApplyFriends
    for (var i = 0; i < ApplyFriends.length; i++) {
        for (var z = 0; z < friendlist.length; z++) {
            if (ApplyFriends[i] && ApplyFriends[i].userId == friendlist[z].userId) {
                ApplyFriends.splice(i, 1)
            }
        }
    }
    friendMessage.ApplyFriends = ApplyFriends
    let resApplyHtml = template("hall-content-friend-item-script", { list: friendMessage.ApplyFriends, type: 'apply' })
    $('.hall-content-friend-ul-apply').html(resApplyHtml)
    if (friendMessage.ApplyFriends && friendMessage.ApplyFriends.length > 0) {
        $('.hall-content-friend-ul-ignore').removeClass('hide')
        $('.hall-content-friend-apply-bot').removeClass('hide')
        let shownum = friendMessage.ApplyFriends.length
        if (shownum >= 100) {
            shownum = '99+'
        }
        $('.hall-content-friend-apply-bot').html(shownum)
        $('.hall-content-friend-apply-numtext').html('(' + shownum + ')')
    } else {
        $('.hall-content-friend-ul-ignore').addClass('hide')
        $('.hall-content-friend-apply-bot').addClass('hide')
        $('.hall-content-friend-apply-numtext').html('')
    }
    // 最近战友
    let showRecentFriends = friendMessage.RecentFriends.slice(0, 10)
    // 最近好友排序
    let Recentinfriend = []//在线
    let Recentbusyfriend = []//忙碌
    let Recentoverfriend = []//离线
    showRecentFriends.sort(function (a, b) {
        let aname = a.username
        let bname = b.username
        var af = aname.replace(reg, "").charAt(0);
        var bf = bname.replace(reg, "").charAt(0);
        if (af.localeCompare(bf) > 0) {
            return 1;
        } else if (af.localeCompare(bf) < 0) {
            return -1;
        } else {
            return 0;
        }
    });
    for (let i = 0; i < showRecentFriends.length; i++) {
        if (showRecentFriends[i].status == 2) {
            Recentoverfriend.push(showRecentFriends[i])
        } else if (showRecentFriends[i].status == 1) {
            Recentinfriend.push(showRecentFriends[i])
        } else {
            Recentbusyfriend.push(showRecentFriends[i])
        }
    }
    showRecentFriends = Recentinfriend.concat(Recentbusyfriend).concat(Recentoverfriend)
    let resLatelyHtml = template("hall-content-friend-item-script", { list: showRecentFriends, type: 'lately' })
    $('.hall-content-friend-ul-lately').html(resLatelyHtml)

    let blackFriendHtml = template("hall-content-friend-item-script", { list: friendMessage.BlackFriends, type: 'black' })
    $('.hall-content-friend-ul-blackfriend').html(blackFriendHtml)
}
friendMessage.init()
function frindeRCM() {
    $('.hall-content-friend-ul-myfriend').find('.hall-content-friend-ul-li').each(function () {
        var $this = $(this)
        $this.contextPopup({
            signClass: 'friendRCM',
            body: $('.hall-content-friend-fix'),
            items: [
                {
                    label: translatesrting('备注昵称'), action: function () {
                        let name = $this.closest('.hall-content-friend-ul-li').find('.name-ellipsis').attr('data-name')
                        let realyname = $this.closest('.hall-content-friend-ul-li').find('.name-ellipsis').attr('data-realyname')
                        let id = $this.closest('.hall-content-friend-ul-li').attr('data-id')
                        $('.friendRemarksModal-content').html(template('friendRemarksModal-content-script', { name: name, id: id, realyname: realyname }))
                        $('#friendRemarksModal').modal('show');
                    }
                },
                {
                    label: translatesrting('删除好友'), action: function () {
                        let name = $this.closest('.hall-content-friend-ul-li').find('.name-ellipsis').attr('data-name')
                        gxmodal({
                            title: translatesrting('删除好友'),
                            centent: '<div class="mt10">' + translatesrting('您确定要删除') + name + translatesrting('吗？') + '</div><div class="f12 ft-white-tr" style="position: absolute;bottom: 61px;width: 100%;left: 0px;">' + translatesrting('此操作不可恢复，并且在') + '' + name + translatesrting('的好友中删除自己。') + '</div>',
                            buttons: [
                                {
                                    text: translatesrting('确定'),
                                    class: 'btn-middle btn-middle-blue',
                                    callback: function () {
                                        $.star({
                                            type: 'POST',
                                            url: '/community-user/friend/v1/delete',
                                            showerror: true,
                                            data: {
                                                id: $this.attr('data-id')
                                            },
                                            success: function (res) {
                                                let myfriendList = friendMessage.myfriendList
                                                for (var i = 0; i < myfriendList.length; i++) {
                                                    if (myfriendList[i].id == $this.attr('data-id')) {
                                                        myfriendList.splice(i, 1)
                                                    }
                                                }
                                                friendMessage.myfriendList = myfriendList
                                                $this.remove()
                                                friendMessage.template()
                                            },
                                            error: function (req) {
                                            }
                                        });
                                    }
                                },
                                {
                                    text: translatesrting('取消'),
                                    class: 'btn-middle btn-style-ash',
                                    callback: function () {
                                    }
                                }
                            ]
                        })
                    }
                },
            ]
        });
    })
    return false
}
function frindeBlackRCM() {
    $('.hall-content-friend-ul-blackfriend').find('.hall-content-friend-ul-li').each(function () {
        var $this = $(this)
        $this.contextPopup({
            signClass: 'friendRCM',
            body: $('.hall-content-friend-fix'),
            items: [
                {
                    label: translatesrting('备注黑名单'), action: function () {
                        let name = $this.closest('.hall-content-friend-ul-li').find('.name-ellipsis').attr('data-name')
                        let realyname = $this.closest('.hall-content-friend-ul-li').find('.name-ellipsis').attr('data-realyname')
                        let id = $this.closest('.hall-content-friend-ul-li').attr('data-id')
                        $('.friendRemarksModal-content').html(template('friendRemarksModal-content-script', { name: name, id: id, realyname: realyname, type: 'black' }))
                        $('#friendRemarksModal').modal('show');
                    }
                },
                {
                    label: translatesrting('删除黑名单'), action: function () {
                        let name = $this.closest('.hall-content-friend-ul-li').find('.name-ellipsis').attr('data-name')
                        gxmodal({
                            title: translatesrting('删除黑名单'),
                            centent: '<div class="mt10">' + translatesrting('您确定要删除') + name + translatesrting('吗？') + '</div>',
                            buttons: [
                                {
                                    text: translatesrting('确定'),
                                    class: 'btn-middle btn-middle-blue',
                                    callback: function () {
                                        $.star({
                                            type: 'Delete',
                                            url: '/community-user/friend/user/v1/blacklist',
                                            showerror: true,
                                            data: {
                                                blackUserId: $this.attr('data-id')
                                            },
                                            success: function (res) {
                                                let myfriendList = friendMessage.BlackFriends
                                                for (var i = 0; i < myfriendList.length; i++) {
                                                    if (myfriendList[i].id == $this.attr('data-id')) {
                                                        myfriendList.splice(i, 1)
                                                    }
                                                }
                                                friendMessage.BlackFriends = myfriendList
                                                localStorage.setItem('Ra_BlackFriends', JSON.stringify(friendMessage.BlackFriends))
                                                $this.remove()
                                                friendMessage.template()
                                                showtoast({
                                                    message: translatesrting('黑名单删除成功')
                                                })
                                            },
                                            error: function (req) {
                                            }
                                        });
                                    }
                                },
                                {
                                    text: translatesrting('取消'),
                                    class: 'btn-middle btn-style-ash',
                                    callback: function () {
                                    }
                                }
                            ]
                        })
                    }
                },
            ]
        });
    })
    return false
}

// 修改备注
$(document).on('click', '.J_changeFriendRemarks', function () {
    var $this = $(this);
    let id = $this.attr('data-id')
    let type = $this.attr('data-type')
    let remark = $this.closest('.friendRemarksModal-content').find('input').val()
    let realyname = $this.attr('data-realyname')
    if (remark == "") {
        remark = realyname
    } else {
        let pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？%]");
        let result = remark.match(pattern);
        if (result) {
            showtoast({
                message: translatesrting('不允许输入除“-”“_”以外的特殊字符')
            })
            return false
        }
    }
    if (type == 'black') {
        $.star({
            type: 'PUT',
            url: '/community-user/friend/user/v1/blacklist',
            showerror: true,
            data: {
                blackUserId: id,
                remark: remark
            },
            success: function (res) {
                $('#friendRemarksModal').modal('hide')
                showtoast({
                    message: translatesrting('黑名单备注修改成功')
                })
                $('.hall-content-friend-ul-li-' + id).find('.name-ellipsis.ft-white').text(remark).attr('data-name', remark)
                cons
                let myfriendList = friendMessage.BlackFriends
                myfriendList.map((item) => {
                    if (item.id == id) {
                        item.remark = remark
                    }
                })
                friendMessage.BlackFriends = myfriendList
                localStorage.setItem('Ra_BlackFriends', JSON.stringify(friendMessage.BlackFriends))
            },
            error: function (req) {
            }
        });
        return false
    }
    $.star({
        type: 'POST',
        url: '/community-user/friend/v1/update/remark',
        showerror: true,
        data: {
            id: id,
            remark: remark
        },
        success: function (res) {
            $('#friendRemarksModal').modal('hide')
            showtoast({
                message: translatesrting('好友备注修改成功')
            })
            $('.hall-content-friend-ul-li-' + id).find('.name-ellipsis.ft-white').text(remark).attr('data-name', remark)
            let myfriendList = friendMessage.myfriendList
            myfriendList.map((item) => {
                if (item.id == id) {
                    item.remark = remark
                }
            })
            friendMessage.myfriendList = myfriendList
        },
        error: function (req) {
        }
    });
})
// 搜索张开
$(document).on('click', '.hall-content-friend-search-icon', function () {
    $(this).closest('.hall-content-friend-search').toggleClass('active')
    if (!$(this).closest('.hall-content-friend-search').hasClass('active')) {
        $(this).closest('.hall-content-friend-search').find('.hall-content-friend-search-input').val("")
        $('.hall-content-friend-ul').removeClass('hide')
        $('.hall-content-friend-ul-soushuo').addClass('hide')
        $('.hall-content-friend-ul-soushuo .hall-content-friend-ul-div').html('')
    }
})
// 清空搜索
$(document).on('click', '.hall-content-friend-search-clear', function () {
    $(this).closest('.hall-content-friend-search').removeClass('active')
    $(this).closest('.hall-content-friend-search').find('.hall-content-friend-search-input').val("")
    $('.hall-content-friend-ul').removeClass('hide')
    $('.hall-content-friend-ul-soushuo').addClass('hide')
    $('.hall-content-friend-ul-soushuo .hall-content-friend-ul-div').html('')
})
// 搜索自己好友
$(document).on('input', '.hall-content-friend-search-input', function () {
    var $this = $(this)
    let findindex = $this.val()
    let findarray = []
    if ($this.val() == "") {
        $('.hall-content-friend-ul').removeClass('hide')
        $('.hall-content-friend-ul-soushuo').addClass('hide')
        $('.hall-content-friend-ul-soushuo .hall-content-friend-ul-div').html('')
    } else {
        friendMessage.myfriendList.map((item) => {
            let nametext = $('.hall-content-friend-ul-li-' + item.id).find('.name-ellipsis.ft-white').text()
            if (nametext.indexOf(findindex) != -1) {
                findarray.push(item)
            }
        })
        if (findarray.length > 0) {
            $('.hall-content-friend-ul').addClass('hide')
            $('.hall-content-friend-ul-soushuo').removeClass('hide')
            let resDetailHtml = template("hall-content-friend-item-script", { list: findarray, type: 'myfriend' })
            $('.hall-content-friend-ul-soushuo .hall-content-friend-ul-div').html(resDetailHtml)
        } else {
            $('.hall-content-friend-ul').addClass('hide')
            $('.hall-content-friend-ul-soushuo').removeClass('hide')
            let emptyHtml = '<div class="text-center mt20">' + translatesrting('未搜索到好友') + '</div>'
            $('.hall-content-friend-ul-soushuo .hall-content-friend-ul-div').html(emptyHtml)
        }
    }

})

// 打开
$(document).on('click', '.hall-content-friend-ul-title', function () {
    var $this = $(this)
    if ($this.closest('.hall-content-friend-ul').hasClass('open')) {
        $this.closest('.hall-content-friend-ul').removeClass('open')
        // $this.closest('.hall-content-friend-ul').find('.hall-content-friend-ul-li').each(function (index, item) {
        $this.closest('.hall-content-friend-ul').find('.hall-content-friend-ul-div').each(function (index, item) {
            var useritem = $(this)
            useritem.addClass('hide')
            // useritem.animate({ opacity: "0" }, 100 + index * 200, 'linear', function () {
            //     useritem.css('opacity', 0)
            // });
        })
        // setTimeout(function () {
        //     $this.closest('.hall-content-friend-ul').find('.hall-content-friend-ul-li').addClass('hide')
        // }, $this.closest('.hall-content-friend-ul').find('.hall-content-friend-ul-li').length * 200 + 100);
    } else {
        if ($this.attr('data-type') == 'apply') {
            $('.hall-content-friend-apply-bot').addClass('hide')
        }
        $this.closest('.hall-content-friend-ul').addClass('open')
        // $this.closest('.hall-content-friend-ul').find('.hall-content-friend-ul-li').each(function (index, item) {
        $this.closest('.hall-content-friend-ul').find('.hall-content-friend-ul-div').each(function (index, item) {
            var useritem = $(this)
            useritem.removeClass('hide')
            // useritem.css('opacity', 1)
        })
    }
})
// 忽略全部
$(document).on('click', '.hall-content-friend-ul-ignore', function () {
    var $this = $(this)
    $.star({
        type: 'POST',
        url: '/community-user/friend/v1/request/agree',
        showerror: true,
        data: {
            oppositeId: '',
            state: 2
        },
        success: function (res) {
            $this.addClass('hide')
            $('.hall-content-friend-ul-apply').html("")
            $this.addClass('hide')
            $('.hall-content-friend-apply-bot').addClass('hide')
            $('.hall-content-friend-apply-numtext').html('')
        },
        error: function (req) {
        }
    });
})
// 添加好友
$(document).on('click', '.J_addfriendModal-friendAdd', function () {
    var $this = $(this)
    if (friendMessage.addfriendloading) {
        return false
    }
    let ishide = $(this).attr('data-hide')
    friendMessage.addfriendloading = true
    let friendId = $this.attr('data-id')
    let source = $this.attr('data-source')
    $.star({
        type: 'POST',
        url: '/community-user/friend/v1/request',
        showerror: false,
        data: {
            friendId: friendId,
            source: source
        },
        success: function (res) {
            friendMessage.addfriendloading = false
            $this.closest('div').find('.addfriendModal-friendAdd-over').removeClass('hide')
            showtoast({
                message: translatesrting('好友申请已发送')
            })
            if (ishide) {
                $this.remove()
            }
        },
        error: function (req) {
            friendMessage.addfriendloading = false
            if (req.errorCode == 260114) {
                gxmodal({
                    title: translatesrting('提示'),
                    centent: '<div class="mt25">' + translatesrting('好友数量超过上限') + '</div>',
                    buttons: [
                        {
                            text: translatesrting('确定'),
                            class: 'btn-middle btn-middle-blue',
                            callback: function () {

                            }
                        }
                    ]
                })
            } else {
                showtoast({
                    message: req.errorMsg
                })
            }
        }
    });
})
// 同意/拒绝
$(document).on('click', '.J_friendApply-accept', function () {
    var $this = $(this)
    let oppositeId = $this.attr('data-id')
    let state = $this.attr('data-type')
    $.star({
        type: 'POST',
        url: '/community-user/friend/v1/request/agree',
        showerror: false,
        data: {
            oppositeId: oppositeId,
            state: state
        },
        success: function (res) {
            $this.closest('.hall-content-friend-ul-li').remove()
            if (state == 1) {
                // 拒绝
                let shownum = $('.hall-content-friend-ul-apply').find('.hall-content-friend-ul-li').length
                if (shownum > 0) {
                    $('.hall-content-friend-apply-bot').html(shownum)
                    $('.hall-content-friend-apply-numtext').html('(' + shownum + ')')
                } else {
                    $('.hall-content-friend-apply-bot').addClass('hide')
                    $('.hall-content-friend-apply-numtext').html('')
                }
            }
            let ApplyFriends = friendMessage.ApplyFriends
            for (var i = 0; i < ApplyFriends.length; i++) {
                if (ApplyFriends[i] && ApplyFriends[i].id == oppositeId) {
                    ApplyFriends.splice(i, 1)
                }
            }
            friendMessage.ApplyFriends = ApplyFriends
        },
        error: function (req) {
            if (req.errorCode == 260114) {
                gxmodal({
                    title: translatesrting('提示'),
                    centent: '<div class="mt25">' + translatesrting('好友数量超过上限') + '</div>',
                    buttons: [
                        {
                            text: translatesrting('确定'),
                            class: 'btn-middle btn-middle-blue',
                            callback: function () {

                            }
                        }
                    ]
                })
            } else {
                showtoast({
                    message: req.errorMsg
                })
            }
        }
    });
})
// 打开搜索
$(document).on('click', '.j_friendshowfind', function () {
    friendMessage.isClickAdd = true
    setTimeout(function () { friendMessage.isClickAdd = false }, 100)
    $('#addfriendModal').modal('show')
    if (friendMessage.RecentFriends && friendMessage.RecentFriends.length > 0) {
        let resDetailHtml = template("hall-content-friend-item-script", { list: friendMessage.RecentFriends, type: 'find', status: 'close' })
        $('.addfriendModal-content-scroll-list.lately-list').removeClass('hide')
        $('.addfriendModal-content-scroll-title.lately-list').removeClass('hide')
        $('.addfriendModal-content-scroll-list.lately-list .row').html(resDetailHtml)
    } else {
        $('.addfriendModal-content-scroll-list.lately-list').addClass('hide')
        $('.addfriendModal-content-scroll-title.lately-list').addClass('hide')
        $('.addfriendModal-content-scroll-list.lately-list .row').html('')
    }
})
$(document).on('show.bs.modal', '#addfriendModal,#friendRemarksModal', function () {
    $('.hall-content-friend-fix').css('z-index', 10000)
})
$(document).on('hidden.bs.modal', '#addfriendModal,#friendRemarksModal', function () {
    $('.hall-content-friend-fix').css('z-index', 'nuset')
})
// 清除搜索
$(document).on('click', '#addfriendModal .gx-search-clear', function () {
    $('.addfriendModal-content-scroll-list.result-list').addClass('hide')
    $('.addfriendModal-content-scroll-list-title.result-list').addClass('hide')
})
// 搜索好友
$(document).on('keypress', '#addfriendModal .gx-search input', function (e) {
    if (e.which == 13) {
        friendsearchinput()
    }
})
function friendsearchinput(btn) {
    let username = $('#addfriendModal .gx-search input').val()
    if (username == "") {
        showtoast({
            message: translatesrting('搜索信息不得为空')
        })
        return false
    }
    if ($('.J_findId').hasClass('gx-disabled')) {
        return false
    }
    $('.J_findId').gxbtn('loading')
    $.star({
        type: 'GET',
        url: '/community-user/friend/v1/search',
        showerror: true,
        data: {
            userName: username
        },
        success: function (res) {
            $('.J_findId').gxbtn('reset')
            if (res.data.id != null) {
                let resDetailHtml = template("hall-content-friend-item-script", { list: [res.data], type: 'find' })
                $('.addfriendModal-content-scroll-list.result-list').removeClass('hide')
                $('.addfriendModal-content-scroll-list-title.result-list').removeClass('hide')
                $('.addfriendModal-content-scroll-list.result-list .row').html(resDetailHtml)
            } else {
                let resDetailHtml = '<div class="ft-5A text-center mt10">' + translatesrting('未查询到玩家') + '</div>'
                $('.addfriendModal-content-scroll-list.result-list').removeClass('hide')
                $('.addfriendModal-content-scroll-list-title.result-list').removeClass('hide')
                $('.addfriendModal-content-scroll-list.result-list .row').html(resDetailHtml)
            }
        },
        error: function (req) {
            $('.J_findId').gxbtn('reset')
        }
    });
}
$(document).on('click', '#addfriendModal .J_findId', function () {
    friendsearchinput($(this))
})
$(document).on('hidden.bs.modal', '#addfriendModal', function () {
    $('.addfriendModal-content-scroll-title').addClass('hide')
    $('.addfriendModal-content-scroll-list').addClass('hide')
    $('#addfriendModal .gx-search input').val("")
})

var wsMessage = {
    timestamp: Date.parse(new Date()),
    serverUrl: null,
    stompClient: null,
    socket: null
}
if (getSaveMessage('test')) {
    wsMessage.serverUrl = "https://test-im-websocket.ramboplay.com/ws?x-auth-token=efd1ef523af5522be66e5e215e079c10&x-auth-timestamp=" + wsMessage.timestamp + "&x-platform=redwar"
} else {
    wsMessage.serverUrl = "https://prod-im-ws.ramboplay.com/ws?x-auth-token=efd1ef523af5522be66e5e215e079c10&x-auth-timestamp=" + wsMessage.timestamp + "&x-platform=redwar"
}


$(function () {
    if (window.WebSocket) {
        websocketConfig();
    }
})

function websocketConfig() {
    /*
                * 1. 连接url为endpointChat的endpoint,对应后台WebSoccketConfig的配置
                * 2. SockJS 所处理的URL 是 "http://" 或 "https://" 模式，而不是 "ws://" or  "wss://"
                */
    wsMessage.socket = new SockJS(wsMessage.serverUrl);

    // 通过sock对象监听每个事件节点，非必须,这个必须放在stompClient的方法前面
    sockHandle(wsMessage.socket);

    // 获取 STOMP 子协议的客户端对象
    wsMessage.stompClient = Stomp.over(wsMessage.socket);

    // console.log('URL: ' + wsMessage.serverUrl);

    /*
     * 1. 获取到stomp 子协议后，可以设置心跳连接时间，认证连接，主动断开连接
     * 2，连接心跳有的版本的stomp.js 是默认开启的，这里我们不管版本，手工设置
     * 3. 心跳是双向的，客户端开启心跳，必须要服务端支持心跳才行
     * 4. heartbeat.outgoing 表示客户端给服务端发送心跳的间隔时间
     * 5. 客户端接收服务端心跳的间隔时间，如果为0 表示客户端不接收服务端心跳
     */
    wsMessage.stompClient.heartbeat.outgoing = 10000;
    wsMessage.stompClient.heartbeat.incoming = 0;
    /*
                 * 1. stompClient.connect(headers, connectCallback, errorCallback);
                 * 2. headers表示客户端的认证信息,多个参数 json格式存,这里简单用的httpsessionID，可以根据业务场景变更
                 *    这里存的信息，在服务端StompHeaderAccessor 对象调用方法可以取到
                 * 3. connectCallback 表示连接成功时（服务器响应 CONNECTED 帧）的回调方法；
                 *    errorCallback 表示连接失败时（服务器响应 ERROR 帧）的回调方法，非必须；
                 */
    //var headers = {token: "123"};

    let headers = {
        'x-platform': 'redwar',
        'x-player': userInfo.userId,
        'details': ""
    };

    wsMessage.stompClient.connect(headers, function (frame) {
        // console.log('Connected: ' + frame);
        /*
         * 1. 订阅服务，订阅地址为服务器Controller 中的地址
         * 2. 如果订阅为公告，地址为Controller 中@SendTo 注解地址
         * 3. 如果订阅为私信，地址为setUserDestinationPrefix 前缀+@SendToUser注解地址
         *    或者setUserDestinationPrefix 前缀 + controller的convertAndSendToUser地址一致
         * 4. 这里演示为公告信息，所有订阅了的用户都能接受
         */
        // stompClient.subscribe("/topicTest/hello", function (message) {
        // 	var msg = JSON.parse(message.body).msg;
        // 	console.log("接收到公告信息：" + msg);
        // 	alert("接收到公告信息：" + msg);
        // });

        /*
         * 1. 因为推送为私信，必须带上或者setUserDestinationPrefix前缀 /user
         * 2. 演示自己发送给自己，做websocket向服务器请求资源而已，然后服务器你就把资源给我就行了，
         *    别的用户就不用你广播推送了，简单点，就是我请求，你就推送给我
         */
        // stompClient.subscribe('/user/userTest/own', function (message) {
        // 	var msg = JSON.parse(message.body).msg;
        // 	console.log("接收到私信信息SendToUser：" + msg);
        // 	alert("接收到私信信息SendToUser：" + msg);
        // });

        /*
         * 1. 订阅点对点消息
         * 2. 很多博文这里的路径会写成"/user/{accountId}/userTest/callBack”这种，是因为
         *    @SendToUser发送的代理地址是 /userTest/callBack， 地址将会被转化为 /user/{username}/userTest/callBack
         *    username，为用户的登录名，也是就是Principal或者本文中的WebSocketUserAuthentication对象getName获取的参数
         *    如果在拦截器中配置了认证路径，可以不带参数，不过推荐用带参数的写法
         *
         */

        headers.id = 'sub-isOpen' + userInfo.userId
        wsMessage.stompClient.subscribe('/group/redwar/rank/isOpen', function (message) { wsDellMeassage(message) }, headers);
        headers.id = 'sub-notice' + userInfo.userId
        wsMessage.stompClient.subscribe('/group/redwar/notice', function (message) { wsDellMeassage(message) }, headers);
        headers.id = 'sub-friend' + userInfo.userId
        wsMessage.stompClient.subscribe('/user/redwar' + userInfo.userId + '/queue/message', function (message) {
            wsDellMeassage(message)
        }, headers);

    }, function (error) {
        // console.log('STOMP: ' + error);
        //setTimeout(websocketConfig, 10000);
        // console.log('STOMP: Reconnecting in 10 seconds');
    });
}
function sockHandle(socket) {
    //连接成功
    socket.addEventListener('open', (event) => {
        console.log("------WS连接成功-" + getSaveMessage('wsUserStatus') + "-----");
        // if (getSaveMessage('wsUserStatus') == 8) {
        //     wsUserStatus(8)
        // } else if (getSaveMessage('wsUserStatus') == 6) {
        //     wsUserStatus(6)
        // } else if (getSaveMessage('wsUserStatus') == 4) {
        //     wsUserStatus(4)
        // } else {
        //     wsUserStatus(1)
        // }

        switch (getSaveMessage('wsUserStatus')) {
            case 1:
                wsUserStatus(1)
                break;
            case 3:
                wsUserStatus(3)
                break;
            case 4:
                wsUserStatus(4)
                break;
            case 5:
                wsUserStatus(5)
                break;
            case 6:
                wsUserStatus(6)
                break;
            case 7:
                wsUserStatus(7)
                break;
            case 8:
                wsUserStatus(8)
                break;
        }
    });

    // 监听接受到服务器的消息
    socket.addEventListener('message', function (message) {

    });

    // 关闭连接的回调函数
    socket.addEventListener('close', (event) => {
        // console.log('The connection has been closed successfully.');
        console.log('--------关闭连接: connection closed.------11111111');
        websocketConfig()
    });

    // 连接发生错误
    socket.addEventListener('error', function (event) {
        console.log('WebSocket error: 222222', event);
        websocketConfig()
    });
}
// 关闭websocket
function disconnect() {
    if (wsMessage.socket != null) {
        wsMessage.socket.close();
        wsMessage.socket = null;
    }
}

var InviteMessage = {
    list: []
}
function friendInvite(dataMessage) {
    let data = dataMessage
    for (var i = 0; i < friendMessage.myfriendList.length; i++) {
        if (friendMessage.myfriendList[i].userId == data.userId) {
            if (friendMessage.myfriendList[i].remark && friendMessage.myfriendList[i].remark != "") {
                data.remark = friendMessage.myfriendList[i].remark
            }
        }
    }
    if (localStorage.getItem('isdetails')) {
        saveMessage('detailsInvite', data)
        return false
    }
    if ($('.Invite-gxmodal').length >= 3) {
        $('.Invite-gxmodal').eq(0).remove()
    }

    for (var i = 0; i < InviteMessage.list.length; i++) {
        if (InviteMessage.list[i].roomNo == data.roomNo) {
            return false
        }
    }
    InviteMessage.list.push(data)
    let modalId = "Invite-gxmodal" + data.roomNo
    let btnHTML = '<a href="javascript:void(0)" ondragstart="return false" id="" class="' + 'btn-middle btn-middle-blue J_veranda_join J_sound_click' + '" data-pwd="' + data.pwd + '" data-dismiss="modal" data-id="' + data.roomNo + '" data-url="/Details/newindex?invitename=' + encodeURI(data.invitePeopleName) + '">' + translatesrting('接受') + '</a>'
    btnHTML = btnHTML + '<a href="javascript:void(0)" ondragstart="return false" id="" class="' + 'btn-middle btn-style-ash' + '"data-dismiss="modal">' + translatesrting('拒绝') + '</a>'
    let titleHtml = '<div class="ft-white-tr f16 text-center">' + translatesrting('组队邀请') + '</div>'
    if (data.remark) {
        var centent = '<div class="mt25">' + data.remark + ' ' + translatesrting('邀请您加入') + ' ' + data.roomName + '</div>'
    } else {
        var centent = '<div class="mt25">' + data.invitePeopleName + ' ' + translatesrting('邀请您加入') + ' ' + data.roomName + '</div>'
    }
    let InviteModal = '<div class="modal gx-gxmodal Invite-gxmodal" data-room="' + data.roomNo + '" id="' + modalId + '" data-keyboard="false" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="ActivateCodeModal"><div class="modal-dialog gx-modal-smell-dialog" role="document"><div class="gx-modal-smell"><div class="gx-modal-smell-header"><div class="header-window"><span class="header-close iconfont ft-ash" data-dismiss="modal">&#xe602;</span></div></div><div class="text-center">' + titleHtml + centent + '</div><div class="gx-gxmodal-foot">' + btnHTML + '</div></div></div></div>'
    $('.friendInvite-content').append(InviteModal)
    $('#' + modalId + '').modal('show')
    $('#' + modalId + '').on('hidden.bs.modal', function () {
        $('#' + modalId + '').remove();
    })
    var InviteModalitem = $('#' + modalId + '')
    // 15s关闭
    setTimeout(function () {
        InviteModalitem.modal('hide')
    }, 15000)
}
$(document).on('hide.bs.modal', '.Invite-gxmodal', function () {
    var $this = $(this)
    $this.attr('data-room')
    InviteMessage.list.map((item, index) => {
        if (item.roomNo == $this.attr('data-room'))
            InviteMessage.list.splice(index, 1)
    })
})

setInterval(function () {
    if (getSaveMessage('inviteJoin')) {
        friendInviteJoin(getSaveMessage('inviteJoin'))
        localStorage.removeItem('inviteJoin')
    }
}, 1000)
function friendInviteJoin(data) {
    $.controlAjax({
        type: "get",
        url: '/api/lobby/room/join/',
        showerror: true,
        data: {
            window: 'createroom',
            url: data.url,
            width: 1280,
            height: 720,
            gameRoomNo: data.id,
            password: data.pwd
        },
        success: function (res) {
        },
        error: function (req) {
            if (req.JerrorCode && req.JerrorCode == "inroom") {
                if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
                    showhallGameRoom()
                } else {
                    if (!$('.hall-header-returnroom').hasClass('hide')) {
                        $('.hall-header-returnroom').tooltip('show')
                        setTimeout(function () {
                            $('.hall-header-returnroom').tooltip('hide')
                        }, 3000)
                    }
                }
            }
        }
    })
}
function wsDellMeassage(message) {
    // var msg = JSON.parse(message.body);
    // console.log(msg)
    // return false
    // $('.hall-header-left-swiper-item').each(function () {
    //     var $this = $(this)
    //     if ($this.attr('data-index') == 2) {
    //         $this.tooltip('show')
    //     }
    // })
    var msg = JSON.parse(message.body);
    if (msg.destination && msg.destination == "/group/redwar/notice") {
        let res = {
            data: {
                maintainVO: msg.body.data,
                reason: msg.body.data.reason
            },
        }
        noticeMessage.gettips(res)
    } else if (msg.destination && msg.destination == "/group/redwar/rank/isOpen") {
        $('.hall-header-left-swiper-item').each(function () {
            var $this = $(this)
            if ($this.attr('data-index') == 1) {
                if (msg.body.isOpen) {
                    $this.tooltip('show')
                } else {
                    $this.tooltip('hide')
                }
                $this.one('click', function () {
                    $this.tooltip('hide')
                })
            }
        })
        if (msg.body.isOpen) {
            $('.Match-bg-padding').find('.match-rank-nav .match-rank-nav-item:nth-child(2)').addClass('active-border')
            $('.Match-bg-padding').find('.match-rank-nav .match-rank-nav-item:nth-child(2)').one('click', function () {
                $('.Match-bg-padding').find('.match-rank-nav .match-rank-nav-item:nth-child(2)').removeClass('active-border')
            })
        } else {
            $('.Match-bg-padding').find('.match-rank-nav .match-rank-nav-item:nth-child(2)').removeClass('active-border')
        }
    } else {
        if (msg.messageType == 1 || msg.messageType == 2 || msg.messageType == 3 || msg.messageType == 4 || msg.messageType == 6) {
            // 用户状态修改
            friendMessage.init(msg.messageType, msg)
        }
        if (msg.messageType == 5) {
            // 房间邀请
            friendInvite(msg.data)
        }
        if (msg.messageType == 7) {
            // 升阶级
            rankMessage.getResult(msg.data.userLevel, msg.data.oldUserLevel)
        }
        if (msg.messageType == 10) {
            // 黑名单删除
            friendMessage.init(msg.messageType, msg)
        }
        if (msg.messageType == 9) {
            // 黑名单添加
            friendMessage.init(msg.messageType, msg)
        }
    }
}