var careerMessage = {
    apmChart: null,
    probabilityChart: null,
    apmEchartsOption: null,
    gameType: getSaveMessage('careerGameType') || 4,//游戏类型
    gameType2: getSaveMessage('careerGameType2') || 4,
    nextToken: null,//分页参数
    pageSize: 10,//分页参数
    pageLoading: false,//分页等待
    optionsCountry: [],
    resMessage: null
}

$(function () {
    // getUserCareer()
    // getCareerApm()
    // getCareerList(1)
})
// 获取用户信息
function getUserCareer(type) {
    // if (careerMessage.resMessage) {
    //     getUserCareerfun(careerMessage.resMessage)
    // } else {
    $.star({
        type: 'GET',
        url: '/battlecenter/redwar/user/v2/career',
        showerror: true,
        data: {
            gameType: careerMessage.gameType
        },
        success: function (res) {
            getUserCareerfun(res.data)

        },
        error: function (req) {

        }
    });
    // }
}
function getUserCareerfun(data) {
    careerMessage.resMessage = data
    let gameMessage = ""
    let winRate = ""

    if (careerMessage.gameType == 3) {
        gameMessage = data.match
    } else if (careerMessage.gameType == 1) {
        gameMessage = data.custom
    } else if (careerMessage.gameType == 4) {
        gameMessage = data.ladder
    }

    winRate = parseInt(gameMessage.winRate) || 0
    $('.career-content-user').html(template('career-content-user-script', { data: data, user: userInfo, message: gameMessage, winRate: winRate, type: careerMessage.gameType }))
    $('.career-content-user [data-toggle="tooltip"]').tooltip()
    $('.career-J-menu').gxmenu({
        height: "28px",
        top: "28px",
        clickHide: true//点击内部是否消失
    }, function (e) {

    })
    careerMessage.probabilityEchartsOption = {
        title: {
            show: true,
            text: '',
            x: 'center',
            y: 'center',
            textStyle: {
                fontSize: '15',
                color: 'white',
                fontWeight: 'normal'
            }
        },
        grid: {
            top: '0%',
            bottom: '0%',
        },
        tooltip: {
            show: false
        },
        legend: {
            show: false
        },
        series:
        {
            name: '',
            type: 'pie',
            radius: ['72%', '85%'],
            avoidLabelOverlap: true,
            hoverAnimation: false,
            itemStyle: {
                borderRadius: 10
            },
            label: {
                normal: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    show: false
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            color: [
                new echarts.graphic.LinearGradient(1, 0, 0, 0, [{
                    offset: 0,
                    color: 'rgba(94, 250, 237, 1)'
                }, {
                    offset: 1,
                    color: 'rgba(63, 121, 238, 1)'
                }]),
                'rgba(63, 121, 238, 0)'
            ],
            data: [
                { value: winRate, name: '' },
                { value: 100 - winRate, name: '' }
            ]
        }
    }
    if (careerMessage.probabilityChart) {
        careerMessage.probabilityChart.dispose();
        careerMessage.probabilityChart = echarts.init(document.getElementById('career-content-probability-echarts'));
        careerMessage.probabilityChart.setOption(careerMessage.probabilityEchartsOption)
    } else {
        careerMessage.probabilityChart = echarts.init(document.getElementById('career-content-probability-echarts'));
        careerMessage.probabilityChart.setOption(careerMessage.probabilityEchartsOption)
    }
}
// 获取Apm信息
// function getCareerApm(type) {
//     $.star({
//         type: 'GET',
//         url: '/battlecenter/redwar/user/v1/chart',
//         showerror: false,
//         data: {
//             gameType: careerMessage.gameType
//         },
//         success: function (res) {
//             var timearray = [];
//             var apmarray = [];

//             res.data.map((item) => {
//                 if (item.finishTime == "-1") {
//                     timearray.push('--.--.--')
//                 } else {
//                     timearray.push(formatDate(item.finishTime, 'yyyy-MM-dd  hh:mm:ss'))
//                 }
//                 apmarray.push(item.apm)
//             })
//             careerMessage.apmEchartsOption = {
//                 grid: {
//                     top: '3%',
//                     bottom: '3%',
//                     left: '3%',
//                     right: '3%',
//                 },
//                 tooltip: {
//                     trigger: 'axis',
//                     formatter: '<div class="ft-white-tr">{b}</div><div class="f26 text-center mt5">{c}</div>',
//                     backgroundColor: 'rgba(33, 38, 50, .8)',
//                     textStyle: {
//                         color: '#FFF'
//                     },
//                     borderColor: 'rgba(33, 38, 50, .1)',
//                     axisPointer: {
//                         type: 'none',
//                         lineStyle: {
//                             color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
//                                 offset: 0,
//                                 color: 'rgba(94, 250, 237, 1)'
//                             }, {
//                                 offset: 1,
//                                 color: 'rgba(63, 121, 238, 1)'
//                             }])
//                         }
//                     },
//                     // showDelay: '100'
//                 },
//                 xAxis: {
//                     show: false,
//                     type: 'category',
//                     boundaryGap: false,
//                     data: timearray
//                 },
//                 yAxis: {
//                     show: false,
//                     type: 'value'
//                 },
//                 series: [{
//                     data: apmarray,
//                     type: 'line',
//                     smooth: .4,
//                     showSymbol: false,
//                     symbol: 'circle',
//                     symbolSize: 10,
//                     lineStyle: {
//                         width: 4,
//                         color: new echarts.graphic.LinearGradient(0, 0, 1, 0, [{
//                             offset: 0,
//                             color: 'rgba(94, 250, 237, 1)'
//                         }, {
//                             offset: 1,
//                             color: 'rgba(63, 121, 238, 1)'
//                         }])
//                     },
//                     areaStyle: {
//                         color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
//                             offset: 0,
//                             color: 'rgba(63,121,238,0.2)'
//                         }, {
//                             offset: 1,
//                             color: 'rgba(94,250,237,0)'
//                         }])
//                     },
//                     itemStyle: {
//                         borderWidth: 2,
//                         color: '#313748',
//                         borderColor: '#4694ee',
//                     }
//                 }]
//             };
//             if (careerMessage.apmChart) {
//                 careerMessage.apmChart.dispose();
//                 careerMessage.apmChart = echarts.init(document.getElementById('career-content-apm-echarts'));
//                 careerMessage.apmChart.setOption(careerMessage.apmEchartsOption)
//             } else {
//                 careerMessage.apmChart = echarts.init(document.getElementById('career-content-apm-echarts'));
//                 careerMessage.apmChart.setOption(careerMessage.apmEchartsOption)
//             }
//         },
//         error: function (req) {

//         }
//     });
// }
// 获取列表信息
// 分页
function getCareerList(page) {
    if (page == 1) {
        careerMessage.nextToken = null;
        $('#career-content-bottom-scroll').scrollTop('0')
    }
    if (careerMessage.pageLoading) {
        return false
    }
    careerMessage.pageLoading = true
    $.star({
        type: 'GET',
        url: '/battlecenter/redwar/match/v1/list',
        showerror: false,
        data: {
            gameType: careerMessage.gameType2,
            nextToken: careerMessage.nextToken,
            pageSize: careerMessage.pageSize,
        },
        success: function (res) {
            careerMessage.pageLoading = false
            careerMessage.nextToken = res.data.nextToken
            let getList = ""
            for (item of res.data.list) {
                item.gameFinishTime = formatDateBefor(item.gameFinishTime, item.systemTime)
                item.mapName = item.mapName + '&&||' + item.enName
            }
            getList = res.data.list
            for (var i = 0; i < getList.length; i++) {
                if (getList[i].label && (getList[i].label.indexOf('Task') != -1 || getList[i].label.indexOf('Defense') != -1)) {
                    getList[i].matchStats = 'unset'
                }
            }
            if (page == 1) {
                $('#career-content-bottom-tbody').html(template('career-content-bottom-tbody-script', { message: getList, 'page': 1, optionsCountry: comOption.optionsCountry }))
            } else {
                $('#career-content-bottom-tbody').append(template('career-content-bottom-tbody-script', { message: getList, 'page': page, optionsCountry: comOption.optionsCountry }))
            }
            if (res.data.nextToken) {
                getCareerchangeMapPage()
            } else {
                $('#career-content-bottom-scroll').off('scroll')
            }
        },
        error: function (req) {
            careerMessage.pageLoading = false
        }
    });
}
function getCareerchangeMapPage() {
    $('#career-content-bottom-scroll').on('scroll', function () {
        var $this = $(this)
        var nScrollHight = $this[0].scrollHeight,
            nScrollTop = $this[0].scrollTop,
            nDivHight = $this.height();
        if (nScrollTop + nDivHight + 10 >= nScrollHight && !careerMessage.pageLoading) {
            $('#career-content-bottom-scroll').off('scroll')
            getCareerList()
        }
    })
}

// 选择游戏模式
$(document).on('click', '.career-content-top-nav-item', function () {
    var $this = $(this)
    if ($this.hasClass('active')) {
        return false
    }
    careerMessage.gameType = $this.attr('data-type')
    saveMessage('careerGameType', careerMessage.gameType)
    getUserCareer('updata')
    // getCareerApm('updata')
    // getCareerList(1)
    $this.siblings().removeClass('active')
    $this.addClass('active')
})
// 详情
$(document).on('click', '.J_career_showDetails', function () {
    showSettlement($(this).attr('data-id'), false, careerMessage.gameType2)
})

// 所有奖章
$(document).on('mouseenter', '.career-content-user-level', function () {
    $('.career-content-level-all').css('display', 'block')
})

$(document).on('mouseleave', '.career-content-user-level', function () {
    $('.career-content-level-all').css('display', 'none')
})

$(document).on('click', '.career-content-center div', function () {
    var $this = $(this)
    if ($this.hasClass('career-content-div-active')) {
        return false
    }
    careerMessage.gameType2 = $this.attr('data-type')
    saveMessage('careerGameType2', careerMessage.gameType2)
    $('.career-content-center div').removeClass('career-content-div-active')
    $this.addClass('career-content-div-active')
    getCareerList(1)
})

// 观战
$(document).on('click', '.career-content-playback-Download', function () {
    var $this = $(this)
    ipcRenderer.send('Main', {
        msgType: "DownLoadFile",
        jsonInfo: {
            Sign: 'replay',
            DownLoadPath: $this.attr('data-url'),
            Date: $this.attr('data-date')
        }
    });
    $this.css('display', 'none')
    $this.next().css('display', 'flex')

    ipcRenderer.on('WebIpc', (event, message) => {
        if (message.msgType == 20) {
            var res = JSON.parse(message.jsonInfo)

            if (res.nowPercentage == '100.0') {
                $this.css('display', 'flex')
                $this.next().css('display', 'none')
            } else if (res.nowPercentage == 0) {
                $this.css('display', 'flex')
                $this.next().css('display', 'none')
            }
        }
    })
})