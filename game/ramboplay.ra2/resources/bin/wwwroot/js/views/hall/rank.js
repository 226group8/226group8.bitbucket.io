// 翻译
$('.hall-content-swiper').html(template('hall-content-swiper-script'))
$('.hall-translate-index-modal').html(template('hall-translate-index-modal-script'))

var rankMessage = {
    id: '',//赛季ID
    type: 1,//rank模式
    init: null,//初始化
    getTypefun: null,//获取当前模式数据
    getMyLevel: null,//获取当前模式数据
    MyLevel: null,
    chooseMapCountry: null,//获取当前模式数据
    rankMaps: [],//赛季地图
    rankMapsReserve: [],//预定地图
    rule: null,//规则
    isReady: false,
    inTime: 0,
    inTimeFun: null,
    getAjax: 0//请求次数
}

rankMessage.init = function () {
    rankBtnType('rank', 'beforeRank', 'init')
    // rankBtnType('rank', 'inRoom')
    // rankMessage.getTypefun()
}
// 当前阶级
rankMessage.getMyLevel = function () {
    $.star({
        type: 'GET',
        url: '/battlecenter/qualifying/user/v1/level',
        showerror: true,
        success: function (res) {
            rankMessage.getMyLevelhtml(res.data)
        },
        error: function (req) {

        }
    });
}
rankMessage.getMyLevelhtml = function (message) {
    rankMessage.MyLevel = message
    $('.rank-session').attr('class', 'rank-session rank-session-' + 8)
    let levelarray = []
    for (var i = 0; i < Number(message.fullStars); i++) {
        if (i < message.stars) {
            levelarray.push("1")
        } else {
            levelarray.push("0")
        }
    }
    $('.rank-level-div').html(template('rank-level-div-script', { data: message, star: levelarray }))
    userInfo.level = message

    $('.hall-rank .rank-right').css('opacity', '1')
    rankMessage.getMapfun(message)
}
rankMessage.getTypefun = function () {
    rankMessage.getMyLevel()
    // $('.hall-rank .rank-bg-padding').css('opacity', 0)
    $.star({
        type: "GET",
        url: '/battlecenter/redwar/matching/v2/game-rule',
        showerror: true,
        data: {
            type: 1,
            ladder: true
        },
        success: function (res) {
            $('.rank-rule-div-list').html("")


            if (hallMessage) {
                clearTimeout(hallMessage.overTimeDownMap)
            }
            hallMessage.overTimeDownMap = setTimeout(function () {
                $('.Match-map-item-inload').each(function () {
                    $(this).find('.Match-map-item-retry').removeClass('hide')
                })
            }, 10000)
            rankMessage.rankMaps = res.data.maps
            rankMessage.id = res.data.seasonId
            $('.rank-session').find('.rank-session-img').attr('src', res.data.seasonLogo)
            rankMessage.rule = res.data.seasonRule
            // --------

            let maplistarray = []
            res.data.configs.map((item) => {
                // 游戏设置
                if (item.key == 'Credits') {
                    $('.rank-rule-div-item-Credits').text(item.value)
                } else if (item.key == 'UnitCount') {
                    $('.rank-rule-div-item-UnitCount').text(item.value)
                } else if (item.key == 'Version') {
                    $('.rank-rule-div-item-Version').text(translatesrting(item.value))
                } else if (item.value == 'True' || item.value == 'true') {
                    maplistarray.push(item)
                }
            })
            // 游戏设置
            // 开关
            if (!res.data.isOpen) {
                $('#J_rank_start').addClass('disabled')
                $('.J_rankNo').text(translatesrting('天梯暂不开放'))
            } else {
                $('#J_rank_start').removeClass('disabled')
                $('.J_rankNo').text('')
            }
            $('#J_rank_start').attr('data-isOpen', res.data.isOpen)
            // 时间
            if (res.data.openTime) {
                $('.J_rankNoTime').text(translatesrting('天梯开放时间') + '：' + res.data.openTime)
            } else {
                $('.J_rankNoTime').text('')
            }
            $('[data-toggle="tooltip"]').tooltip()
            downrankImg()
            // $('.hall-rank .rank-bg-padding').css('opacity', 1)
        },
        error: function (req) {

        }
    })
}
rankMessage.init()
// 获取天梯地图
rankMessage.getMapfun = function (level, stage) {
    let navtype = 3
    if (stage) {
        navtype = stage
    } else {
        let navlevelarray = [
            [-1, 0, 1], [2, 3], [4, 5], [6, 7, 8]
        ]
        if (!level.rating) {
            navtype = 2
        } else {
            for (var i = 0; i < navlevelarray.length; i++) {
                if ($.inArray(level.level * 1, navlevelarray[i]) != -1) {
                    navtype = i
                }
            }
        }
    }
    // $('#J_rank_start').css('opacity', 0)
    $('.rank-map-list-nav-item').removeClass('active')
    $('.rank-map-list-nav-item[data-code="' + navtype + '"]').addClass('active')
    $.controlAjax({
        type: "get",
        url: '/api/lobby/matching/maps',
        data: {
            type: 1,
            pageNum: 1,
            pageSize: 36,
            stage: navtype,//0-3
        },
        success: function (res) {
            res.data.list.map((item) => {
                item.mapName = item.mapName + '&&||' + item.enName
            })
            rankMessage.getMapfunurl(res.data.list)
            $('.rank-map-list').html(template('Match-map-list-script', { maplist: res.data.list }))
        },
        error: function (req) {

        }
    })
}
$(document).on('click', '.rank-map-list-nav-item', function () {
    var $this = $(this)
    if ($this.hasClass('active')) {
        return false
    }
    rankMessage.getMapfun(null, $this.attr('data-code'))
})

rankMessage.getMapfunurl = async function (maps) {
    // --------图片数据处理
    let topMapList = maps
    let imgarray = new Map()
    for (var i = 0; i < topMapList.length; i++) {
        imgarray.set(topMapList[i].imageUrl + '?x-oss-process=image/resize,w_242', '')
    }
    let dellimgarray = await readerIndexedDBImgUrl(imgarray)
    for (list of topMapList) {
        let itemimg = list.imageUrl + '?x-oss-process=image/resize,w_242'
        if (dellimgarray.has(itemimg)) {
            if (dellimgarray.get(itemimg)) {
                list.localimageUrl = dellimgarray.get(itemimg)
            } else {
                list.localimageUrl = itemimg
            }
        }
    }
    for (var i = 0; i < topMapList.length; i++) {
        $('.rank-map-list .gx-map[data-map="' + topMapList[i].imageUrl + '"]').css('backgroundImage', 'url(' + topMapList[i].localimageUrl + ')')
    }
    // --------图片数据处理
}

rankMessage.chooseMapCountry = function (navtype) {
    $('.rankMapModal-content-right-nav_item').removeClass('active')
    $('.rankMapModal-content-right-nav_item[data-code="' + navtype + '"]').addClass('active')
    $.star({
        type: 'GET',
        url: '/battlecenter/qualifying/v2/config',
        showerror: true,
        data: {
            stage: navtype
        },
        success: function (res) {
            let choosemap = JSON.parse(JSON.stringify(comOption.optionsCountry))
            choosemap.map((item, index) => {
                if (item.country == "" || item.country == "Yuri") {
                    choosemap.splice(index, 1)
                }
            })
            res.data.map((item) => {
                item.mapName = item.mapName + '&&||' + item.enName

                choosemap.map((country) => {
                    if (country.sideId == item.countryId) {
                        item.JcountryCname = country.cname
                    }
                })

            })
            rankMessage.rankMapsReserve = res.data
            $('.rankMapModal-content-right-scroll').html(template('rankMapModal-content-item-script', { list: res.data }))
            $('.rankMapModal-content-menu-show .gx-menu-ul').html(template('rankMapModal-content-menu-script', { list: choosemap }))
            $('.rankMapModal-content-left-allChoose .gx-menu-ul').html(template('rankMapModal-content-menu-script', { list: choosemap, type: 'chooseall' }))

            $('.rankMapModal-content-left-allChoose .gx-menu').gxmenu({
                height: "17px",
                top: "23px",
                clickHide: true
            }, function () {

            })
            $('.rankMapModal-content-menu-show').gxmenu({
                height: "17px",
                top: "40px",
                clickHide: true
            }, function () {

            })
            $('.rankMapModal-content-right-scroll').find('.rankMapModal-content-item').eq(0).click()
            $('#rankMapModal').modal('show')
        }
    })
}
$(document).on('click', '.J_rankMapModal-chooseAllCountry', function () {
    var $this = $(this)
    let countryId = $this.attr('data-countryid')
    $('.rankMapModal-content-left-allChoose_menuin').text($this.text())
    $('.rankMapModal-content-left-allChoose_menuin').attr('data-countryId', countryId)
})
$(document).on('click', '.JrankMapModalallChoose', function () {
    let countryId = $('.rankMapModal-content-left-allChoose_menuin').attr('data-countryId')
    if (!countryId) {
        return false
    }
    let countryIdArray = []
    rankMessage.rankMapsReserve.map((item) => {
        countryIdArray.push({
            "mapId": item.mapId,
            "countryId": countryId
        })
    })
    $.star({
        type: 'POST',
        url: '/battlecenter/qualifying/v2/config',
        showerror: true,
        data: countryIdArray,
        success: function (res) {
            $('.rankMapModal-content-right-nav_item.active').click()
        }
    })
})
$(document).on('click', '.rankMapModal-content-right-nav_item', function (e) {
    var $this = $(this)
    rankMessage.chooseMapCountry($this.attr('data-code'))
})
var rankMapModalmapId = '0'
$(document).on('click', '.rankMapModal-content-item-right .gx-menu-in', function () {
    let topone = $('.rankMapModal-content-right-scroll').offset().top
    let toptwo = $(this).offset().top
    let topthree = toptwo - topone + 60
    $('.rankMapModal-content-menu-show').css('top', topthree)

    rankMapModalmapId = $(this).closest('.rankMapModal-content-item').attr('data-id')
    $('.rankMapModal-content-menu-show .gx-menu-in').click()
})

// 选择国家
$(document).on('click', '.J_rankMapModal-chooseCountry', function () {
    var $this = $(this)
    let countryId = $this.attr('data-countryid')
    let mapId = rankMapModalmapId
    // $this.closest('.rankMapModal-content-item').find('.J_rankMapModal-countryName').text($this.text())
    $.star({
        type: 'POST',
        url: '/battlecenter/qualifying/v1/config',
        showerror: true,
        data: {
            mapId: mapId,
            countryId: countryId
        },
        success: function (res) {
            $('.rankMapModal-content-item[data-id=' + rankMapModalmapId + '] .J_rankMapModal-countryName').text($this.text())
        },
        error: function (req) {

        }
    });
})
$(document).on('click', '.rankMapModal-content-item', function () {
    var $this = $(this)
    $this.siblings().removeClass('active')
    $this.addClass('active')
    let indexNum = $this.attr('data-index')
    $('.rankMapModal-content-map').css('background-image', 'url(' + rankMessage.rankMapsReserve[indexNum].imageUrl + ')')
    $('.rankMapModal-content-map-name .f12').text(formatMapName(rankMessage.rankMapsReserve[indexNum].mapName))
})
$(document).on('click', '.J_rank-chooseModal', function () {
    rankMessage.chooseMapCountry($('.rank-map-list-nav-item.active').attr('data-code'))
})
$(document).on('click', '.rank-session-icon', function () {
    $('#rankRuleModal').find('.rankRuleModal-content').html(rankMessage.rule)
    $('#rankRuleModal').modal('show')
})
$(document).on('click', '.J_rank-tolist', function () {
    $.star({
        type: 'GET',
        url: '/battlecenter/qualifying/v1/rank/link',
        showerror: false,
        success: function (res) {
            $.star({
                type: 'POST',
                url: '/community-user/authorize/generate/code',
                showerror: true,
                success: function (coderes) {
                    let tourl = res.data
                    if (tourl.indexOf('?') != -1) {
                        OpenWebUrl(tourl + '&token=' + coderes.data.authCode)
                    } else {
                        OpenWebUrl(tourl + '?token=' + coderes.data.authCode)
                    }
                },
                error: function (req) {

                }
            });
        },
        error: function (req) {
            gxmodal({
                title: translatesrting('提示'),
                centent: '<div class="mt25">' + req.errorMsg + '</div>',
                buttons: [
                    {
                        text: translatesrting('确定'),
                        class: 'btn-middle btn-middle-blue',
                    }
                ]
            })
        }
    });
})


// 开始天梯匹配
$(document).on('click', '#J_rank_start', function () {
    player_sound_inrank()
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    if ($this.hasClass('disabledtwo')) {
        return false
    }
    if ($this.hasClass('disabled')) {
        let errormessage = '天梯暂不开放'
        spop({
            template: errormessage,
            autoclose: 1000,
            style: 'warning'
        });
        return false
    }
    trackingFunc('rank_start')
    $this.gxbtn('loading')
    $.controlAjax({
        type: "get",
        url: '/api/lobby/start/matching/',
        data: {
            'operatetype': 'Searching',
            'gametype': rankMessage.type,
            'ladder': true
        },
        success: function (res) {
            $this.gxbtn('reset')
            rankMessage.isReady = false
            rankBtnType('rank', 'inRank')
        },
        error: function (req) {
            $this.gxbtn('reset')
            if (req.errorCode == 250007) {
                let getTime = JSON.parse(req.errorData)
                // spop({
                //     template: '您被封禁到' + formatDate(getTime.endTime),
                //     autoclose: 3000,
                //     style: 'warning'
                // });
                let time = getTime.endTime - getTime.sysTime
                let fengjin = '<div class="mt10" style="padding: 0px 22px;">' + getTime.reason + '<div>'
                fengjin += '<div class="fengjinTime" data-time="' + time + '">' + formatMatchTime(time) + '</div>'
                gxmodal({
                    title: translatesrting('进入匹配失败'),
                    centent: fengjin,
                    id: 'fengjinTime',
                    buttons: [
                        {
                            text: translatesrting('确定'),
                            class: 'btn-middle btn-middle-blue',
                            callback: function () {
                            }
                        }
                    ]
                })
                fengjinTimefun()
            } else if (req.errorCode == 1077) {
                let getTime = JSON.parse(req.errorData).banTime * 1
                let getMsg = JSON.parse(req.errorData).msg
                spop({
                    template: getMsg + " " + formatMatchTime(getTime),
                    autoclose: 3000,
                    style: 'warning'
                });
            } else {
                var errormessage = null
                if (req.errorMsg) {
                    errormessage = req.errorMsg
                } else {
                    errormessage = translatesrting('网络可能出现点问题,请稍后再试')
                }
                spop({
                    template: errormessage,
                    autoclose: 1000,
                    style: 'warning'
                });
                if (req.JerrorCode && req.JerrorCode == "inroom") {
                    if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
                        showhallGameRoom()
                    } else {
                        if (!$('.hall-header-returnroom').hasClass('hide')) {
                            $('.hall-header-returnroom').tooltip('show')
                            setTimeout(function () {
                                $('.hall-header-returnroom').tooltip('hide')
                            }, 3000)
                        }
                    }
                }
            }
        }
    })
})
// 按钮
function rankBtnType(type, status, isfirst) {
    if (type == 'rank') {
        $('.J_rankNoTime').addClass('hide')
        if (status == 'beforeRank') {
            $('.J_rankNoTime').removeClass('hide')
        }
        if (status == 'inRank') {
            $('.hall-header-refresh').addClass('hide')//匹配中禁止刷新
            wsUserStatus(3)
            rankTime()
        } else {
            $('.hall-header-refresh').removeClass('hide')//匹配中禁止刷新
            if (isfirst != 'init') {
                wsUserStatus(1)
            }
            rankTime('clear')
        }
        $('.rank-startBtn-div').html(template('rank-startBtn-div-script', { 'status': status }))
    } else if (type == 'modal') {

    }
}
// 取消匹配
$(document).on('click', '#J_rank_close', function () {
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    $this.gxbtn('loading')
    $.controlAjax({
        type: "get",
        url: '/api/lobby/start/matching/',
        showerror: true,
        data: {
            'operatetype': 'Cancel',
            gametype: rankMessage.type,
            'ladder': true
        },
        success: function (res) {
            $this.gxbtn('reset')
            rankBtnType('rank', 'beforeRank')
        },
        error: function (req) {
            $this.gxbtn('reset')
        }
    })
})

// 阶级变化
rankMessage.getResult = function (newLevel, oldLevel) {
    rankMessage.getMyLevelhtml(newLevel)
    let uplevel = false// 升段
    let uprating = false// 定级
    let stardifference = newLevel.totalStars - oldLevel.totalStars//总星
    let iswin = true//胜负

    if (oldLevel.level != newLevel.level) {
        // 升段
        if (newLevel.rating) {
            uplevel = true
        }
    }
    if (stardifference > 0) {
        iswin = true
    } else {
        iswin = false
    }
    if (newLevel.rating && !oldLevel.rating) {
        uprating = true
    }
    if (oldLevel.level == newLevel.level && oldLevel.secondLevel == newLevel.secondLevel && oldLevel.stars == newLevel.stars && oldLevel.rating == newLevel.rating) {
        // 无改变
        return false
    }
    if (newLevel.rating == false) {
        // 未定级
        return false
    }
    // if (newLevel.level == -1) {
    //     // 新兵
    //     return false
    // }
    // if ((uplevel || uprating) && stardifference >= 0) {
    if ((uplevel && stardifference >= 0) || uprating) {
        //动画
        $('#rankResultModal .modal-dialog').html(template('rankResultModal-script', { iswin: iswin, data: newLevel }))
        if (newLevel.rating && !oldLevel.rating) {
            $('.rankResultModal-video').attr('src', 'https://img.ramboplay.com/redwar/rank/video/ratingto' + newLevel.level + '.mp4')
        } else {
            $('.rankResultModal-video').attr('src', 'https://img.ramboplay.com/redwar/rank/video/' + (Number(newLevel.level) - 1) + 'to' + newLevel.level + '.mp4')
        }
        $('.rankResultModal-video').get(0).play()
        setTimeout(() => {
            $('#rankResultModal .rankResultModal-aftershow').css('opacity', 1)
        }, 6000)
        $('#rankResultModal').modal('show')
        $('.rankResultModal-video')[0].volume = 0.7
    }


    // var oldlevel = userInfo.level
    // if (isfirst) {
    //     rankMessage.getAjax = 0
    // }
    // if (rankMessage.getAjax > 5) {
    //     return false
    // }
    // $.star({
    //     type: 'GET',
    //     url: '/battlecenter/qualifying/user/v1/level',
    //     showerror: true,
    //     success: function (res) {
    //         let newLevel = res.data
    //         let newlevelarray = []

    //         // newLevel.level = 2
    //         // newLevel.stars = 1
    //         // newLevel.fullStars = 3
    //         // newLevel.rating = true
    //         // message.level = 1
    //         // message.stars = 1
    //         // message.fullStars = 3
    //         // message.rating = true

    //         for (var i = 0; i < Number(newLevel.fullStars); i++) {
    //             if (i < newLevel.stars) {
    //                 newlevelarray.push("1")
    //             } else {
    //                 newlevelarray.push("0")
    //             }
    //         }
    //         let oldlevelarray = []
    //         for (var i = 0; i < Number(oldlevel.fullStars); i++) {
    //             if (i < oldlevel.stars) {
    //                 oldlevelarray.push("1")
    //             } else {
    //                 oldlevelarray.push("0")
    //             }
    //         }
    //         rankMessage.getAjax = rankMessage.getAjax + 1
    //         rankMessage.getMyLevelhtml(newLevel)
    //         if (newLevel.rating == false) {
    //             return false
    //         }
    //         if (oldlevel.level == newLevel.level && oldlevel.secondLevel == newLevel.secondLevel && oldlevel.stars == newLevel.stars && oldlevel.rating == newLevel.rating) {
    //             setTimeout(function () {
    //                 rankMessage.getResult()
    //             }, 3000)
    //             return false
    //         }
    //         let stardifference = newLevel.totalStars - oldlevel.totalStars
    //         let iswin = true
    //         if (stardifference > 0) {
    //             iswin = true
    //         } else {
    //             iswin = false
    //         }
    //         let uplevel = false
    //         let upclass = false
    //         let upstar = false
    //         let hidestar = false
    //         if (oldlevel.level != newLevel.level) {
    //             // 升段
    //             if (newLevel.rating) {
    //                 uplevel = true
    //             }
    //         }
    //         if (oldlevel.rating != newLevel.rating) {
    //             // 升段
    //             uplevel = true
    //             hidestar = true
    //             stardifference = 1//为了能弹出来
    //         }
    //         if (oldlevel.secondLevel != newLevel.secondLevel && !uplevel) {
    //             // 升阶级
    //             upclass = true
    //         }
    //         if (oldlevel.stars != newLevel.stars && !uplevel && !upclass) {
    //             // 升星
    //             upstar = true
    //         }
    //         if (uplevel && stardifference > 0) {
    //             //动画
    //             $('#rankResultModal .modal-dialog').html(template('rankResultModal-script', { iswin: iswin, data: newLevel }))
    //             if (hidestar && newLevel.level != 1) {
    //                 $('.rankResultModal-video').attr('src', '../images/video/ratingto' + newLevel.level + '.mp4')
    //             } else {
    //                 $('.rankResultModal-video').attr('src', '../images/video/' + (Number(newLevel.level) - 1) + 'to' + newLevel.level + '.mp4')
    //             }
    //             $('.rankResultModal-video').get(0).play()
    //             setTimeout(() => {
    //                 $('#rankResultModal .rankResultModal-aftershow').css('opacity', 1)
    //             }, 6000)
    //             $('#rankResultModal').modal('show')
    //             // $('#settlement').modal('show')
    //             // return false
    //             // $('.rankResultModal-ba-content-animate').html(template('rankResultModal-ba-content-animate-script', { data: oldlevel, star: oldlevelarray }))
    //             // $('#rankResultModal').modal('show')
    //             // $('.rankResultModal-ba-content-animate').css('opacity', 1)
    //             // setTimeout(() => {
    //             //     $('.rankResultModal-ba-content-animate').css('opacity', 0)
    //             //     setTimeout(() => {
    //             //         $('.rankResultModal-ba-content-animate').html(template('rankResultModal-ba-content-animate-script', { data: newLevel, star: newlevelarray }))
    //             //         $('.rankResultModal-ba-content-animate').css('opacity', 1)
    //             //     }, 300)
    //             // }, 1000)
    //         } else if (upclass) {
    //             return false
    //             $('#rankResultModal .modal-dialog').html(template('rankResultModal-script', { iswin: iswin, data: newLevel }))
    //             $('.rankResultModal-ba-content-animate').html(template('rankResultModal-ba-content-animate-script', { data: oldlevel, star: oldlevelarray }))
    //             $('#rankResultModal').modal('show')
    //             $('.rankResultModal-ba-content-animate').css('opacity', 1)

    //             setTimeout(() => {
    //                 $('.rankResultModal-ba-content-secondLevel').css('opacity', 0)
    //                 $('.rankResultModal-ba-star-animate').css('opacity', 0)
    //                 setTimeout(() => {
    //                     $('.rankResultModal-ba-star-animate').html(template('rankResultModal-ba-star-animate-script', { data: newLevel, star: newlevelarray }))
    //                     $('.rankResultModal-ba-content-secondLevel').text(newLevel.levelName + newLevel.secondLevel)
    //                     $('.rankResultModal-ba-content-secondLevel').css('opacity', 1)
    //                     $('.rankResultModal-ba-star-animate').css('opacity', 1)
    //                 }, 300)
    //             }, 1000)
    //         } else {
    //             return false
    //             $('#rankResultModal .modal-dialog').html(template('rankResultModal-script', { iswin: iswin, data: newLevel }))
    //             $('.rankResultModal-ba-content-animate').html(template('rankResultModal-ba-content-animate-script', { data: oldlevel, star: oldlevelarray }))
    //             $('#rankResultModal').modal('show')
    //             $('.rankResultModal-ba-content-animate').css('opacity', 1)
    //             for (var i = 0; i < stardifference; i++) {
    //                 setTimeout(() => {
    //                     let changeStar = $('.rankResultModal-level-star').find('img[alt="empty"]').eq(0)
    //                     changeStar.css('opacity', 0)
    //                     changeStar.attr('src', '../../../images/rank/star.png')
    //                     changeStar.attr('alt', 'star')
    //                     changeStar.css({ 'transform': 'scale(0)', 'opacity': 0, })
    //                     changeStar.css({ 'transform': 'scale(1.5)', 'opacity': 1, })
    //                     setTimeout(() => {
    //                         changeStar.css({ 'transform': 'scale(1)', 'opacity': 1, })
    //                     }, 300)
    //                 }, 1000 + i * 500)
    //             }
    //             if (stardifference < 0) {
    //                 let getun = stardifference * -1
    //                 for (var i = 0; i < getun; i++) {
    //                     setTimeout(() => {
    //                         let changeStar = $('.rankResultModal-level-star').find('img[alt="star"]').eq($('.rankResultModal-level-star').find('img[alt="star"]').length - 1)
    //                         changeStar.css('opacity', 0)
    //                         changeStar.attr('src', '../../../images/rank/star-empty.png')
    //                         changeStar.attr('alt', 'empty')
    //                         changeStar.css({ 'transform': 'scale(0)', 'opacity': 0, })
    //                         changeStar.css({ 'transform': 'scale(1.5)', 'opacity': 1, })
    //                         setTimeout(() => {
    //                             changeStar.css({ 'transform': 'scale(1)', 'opacity': 1, })
    //                         }, 300)
    //                     }, 1000 + i * 500)
    //                 }
    //             }
    //             if (newLevel.level == 8) {
    //                 $('.rankResultModal-level-star .rank-level-star8 .f16').css('opacity', 0)
    //                 $('.rankResultModal-level-star .rank-level-star8 .f16').text(newLevel.stars)
    //                 $('.rankResultModal-level-star .rank-level-star8 .f16').css('opacity', 1)
    //             }
    //         }
    //         if (stardifference > 0) {
    //             $('.rankResultModal-stardifference').text('+' + stardifference)
    //         } else {
    //             $('.rankResultModal-stardifference').text(stardifference)
    //         }
    //         // if (hidestar) {
    //         //     $('.rankResultModal-stardifference').closest('div').addClass('hide')
    //         // } else {
    //         //     $('.rankResultModal-stardifference').closest('div').removeClass('hide')
    //         // }
    //         $('.rankResultModal-gameIcon').attr('data-id', id)
    //     },
    //     error: function (req) {

    //     }
    // });
}
$(document).on('click', '.rankResultModal-gameIcon', function () {
    var $this = $(this)
    let id = $this.attr('data-id')
    $('#rankResultModal').modal('hide')
    showSettlement(id, false, 4)
})
// 匹配计时
function rankTime(type) {
    if (type == 'clear') {
        clearInterval(rankMessage.inTimeFun)
        rankMessage.inTime = 0
    } else {
        clearInterval(rankMessage.inTimeFun)
        $('.Match-start-btn-in-time').text(formatMatchTime(rankMessage.inTime))
        rankMessage.inTimeFun = setInterval(function () {
            rankMessage.inTime = rankMessage.inTime + 1
            $('.Match-start-btn-in-time').text(formatMatchTime(rankMessage.inTime))
            if (rankMessage.inTime > 600) {
                // 10分钟补偿
                $('#J_rank_close').click()
            }
        }, 1000)
    }
}
function downrankImg() {
    if ($('.rank-map-list .Match-map-item-inload').length > 0) {
        $('#J_rank_start').addClass('disabled')
        $('#J_rank_start').text(translatesrting('地图下载中'))
        $('.Match-map-item-inload').each(function () {
            var $this = $(this)
            var getnum = parseInt($this.attr('data-load'))
            var shownum = parseInt($this.find('.gx-map-mask-circle').attr('data-br'))
            if (getnum != shownum) {
                $this.find('.gx-map-mask-progress-br').css('width', getnum + '%')
                $this.find('.gx-map-mask-progress-br').attr('data-br', getnum)
                if (getnum >= 100) {
                    // setTimeout(function () {
                    $this.removeClass('Match-map-item-inload')
                    // }, 300)
                }
            }
        })
        if ($('.rank-map-list .Match-map-item-inload').length > 0) {
            $('#J_rank_start').addClass('disabled')
            $('#J_rank_start').text(translatesrting('地图下载中'))
        } else {
            if ($('#J_rank_start').attr('data-isOpen') == 'true') {
                $('#J_rank_start').removeClass('disabled')
                $('#J_rank_start').text(translatesrting('寻找比赛'))
            } else {
                $('#J_rank_start').text(translatesrting('天梯暂未开放'))
            }
        }
    } else {
        if ($('#J_rank_start').attr('data-isOpen') == 'true') {
            $('#J_rank_start').removeClass('disabled')
            $('#J_rank_start').text(translatesrting('寻找比赛'))
        } else {
            $('#J_rank_start').text(translatesrting('天梯暂未开放'))
        }
    }
}
$(document).on('click', '.J_resetRank', function () {
    // if (!rankMessage.MyLevel.rating && rankMessage.MyLevel.nowQualifyingMatchNum == 0) {
    //     gxmodal({
    //         title: '提示',
    //         id: 'resetRankModal',
    //         centent: '<div class="mt15 pb30" style="padding: 10px 22px">' + translatesrting('参与一场排位赛后才可重置比赛数据') + '<div>',
    //         buttons: [
    //             {
    //                 text: translatesrting('确定'),
    //                 class: 'btn-middle btn-middle-blue',
    //                 callback: function (e) {

    //                 }
    //             },
    //         ]
    //     })
    //     return false
    // }
    $.star({
        type: 'GET',
        url: '/battlecenter/platform/config/gold/v1/list',
        showerror: false,
        data: {
        },
        success: function (res) {
            gxmodal({
                title: translatesrting('确认重置段位'),
                id: 'resetRankModal',
                centent: '<div class="mt15 pb30" style="padding: 0px 22px;padding-bottom: 30px;">' + translatesrting('花费') + ' ' + res.data.userResetQualifyingGold + ' ' + translatesrting('金币，天梯段位和MMR将重置为根据上赛季结算分转化为的初始分，当前赛季统计数据将清空，并重新开始') + ' ' + '10' + ' ' + translatesrting('场定级赛！') + '<div>',
                buttons: [
                    {
                        text: translatesrting('确认重置'),
                        class: 'btn-middle btn-middle-blue J_resetAlertRank',
                        preventDefault: true,
                        callback: function (e) {
                            $('#resetRankModal').modal('hide');
                            $.star({
                                type: 'GET',
                                url: '/battlecenter/qualifying/v1/reset',
                                showerror: true,
                                data: {
                                },
                                success: function (res) {
                                    rankMessage.getMyLevel()
                                    showtoast({
                                        message: translatesrting('赛季重置成功')
                                    })
                                    getUserGold()
                                },
                                error: function (req) {
                                }
                            });
                        }
                    },
                    {
                        text: translatesrting('我再想想'),
                        class: 'btn-middle btn-style-ash',
                        callback: function () {
                            // $('.resetRankModal').modal('hide');
                        }
                    }
                ]
            })
        },
        error: function (req) { }
    });
})