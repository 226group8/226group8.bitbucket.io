var mapAdminMesage = {
    type: 1,
    page: 1,
    isPrivate: 2,// 是否私有
    limit: 8,
    localList: [],
    chooseIndex: 0,
    chooseItem: null,
    isLoading: false,
    apply: null,
    updata: {
        url: null,
        name: null,
        desc: null,
        version: null,
        authorName: null
    },
    updataAdd: [],
    descCheck: true,
    descCheckLoading: false,
    namecCheck: true,
    nameCheckLoading: false,
    authorcCheck: true,
    authorCheckLoading: false,
    CheckEnd: false
}
$(document).on('hidden.bs.modal', '#MapAuthorModal', function () {
    mapAdminMesage = {
        type: 1,
        page: 1,
        isPrivate: 2,// 是否私有
        limit: 8,
        localList: [],
        chooseIndex: 0,
        chooseItem: null,
        isLoading: false,
        apply: null,
        updata: {
            url: null,
            name: null,
            desc: null,
            version: null
        },
        updataAdd: [],
        descCheck: true,
        descCheckLoading: false,
        namecCheck: true,
        nameCheckLoading: false,
        authorcCheck: true,
        authorCheckLoading: false,
        CheckEnd: false,
        qq: null
    }
    $('#MapAuthorModal .modal-dialog').removeClass('MapAuthorModalUpdateTop')
})
// 点击进入作者中心
$(document).on('click', '.MapAuthorModal_show', function () {
    $.controlAjax({
        type: "get",
        url: '/api/lobby/author/maps/',
        data: {
            page: mapAdminMesage.page,
            limit: mapAdminMesage.limit,
            status: mapAdminMesage.type,
            isPrivate: mapAdminMesage.isPrivate
        },
        success: function (res) {
            //  $('#MapAuthorModal .modal-dialog').html(template('MapAuthorModal-admin-script'))
            //  mapAdminMesage.type = 1//初始化下
            //  mapAdminInit(res)
            //  $('#MapAuthorModal').modal('show')
            $.controlAjax({
                type: "get",
                url: '/api/lobby/start/mapcenter/',
                data: {},
                success: function (res) { },
                error: function (req) {
                    spop({
                        template: req.errorMsg,
                        autoclose: 3000,
                        style: 'error',
                        group: 'mapAdmin',
                    });
                }
            })
        },
        error: function (req) {
            if (req.errorCode == 1031) {
                // 没通过
                $('.normalModal-content').html(template('MapAuthorModal-alert-script', { type: 'wait', message: translatesrting('很抱歉，您的作者申请未通过'), btnName: translatesrting('重新申请') }))
                $('#NormalModal').modal('show')
            } else if (req.errorCode == 1030) {
                // 申请中
                $('.normalModal-content').html(template('MapAuthorModal-alert-script', { type: 'error', message: translatesrting('您的申请正在审核中') }))
                $('#NormalModal').modal('show')
            } else if (req.errorCode == 1032) {
                // 没申请
                $('.normalModal-content').html(template('MapAuthorModal-alert-script', { type: 'wait', message: translatesrting('很抱歉，您暂时没有地图作者权限') }))
                $('#NormalModal').modal('show')
            } else if (req.errorCode == 1034) {
                // 禁用
                $('.normalModal-content').html(template('MapAuthorModal-alert-script', { type: 'error', message: translatesrting('您已被禁用作者功能') }))
                $('#NormalModal').modal('show')
            }
        }
    })
})
// $('.MapAuthorModal_show').click()
// 切换类型
$(document).on('click', '.MapAuthorModal-modal-nav', function () {
    var $this = $(this)
    if ($this.hasClass('active')) {
        return false
    }
    $this.siblings().removeClass('active')
    $this.addClass('active')
    mapAdminMesage.type = $this.attr('data-type')
    mapAdminMesage.chooseIndex = 0
    mapAdminMesage.page = 1
    mapAdminInit()
})
$(document).on('click', '.MapAuthorModal-modal-navdiva a', function () {
    var $this = $(this)
    if ($this.hasClass('active')) {
        return false
    }
    $this.siblings().removeClass('active')
    $this.addClass('active')
    mapAdminMesage.isPrivate = $this.attr('data-type')
    mapAdminMesage.chooseIndex = 0
    mapAdminMesage.page = 1
    mapAdminInit()
})

// 初始化渲染
function mapAdminInit(getmessage) {
    if (getmessage) {
        $('.MapAuthorModal-modal-scroll').off('scroll')
        $('.MapAuthorModal-modal-scroll').scrollTop(0)
        var getlist = getmessage.data.list
        setTimeout(() => {
            if (getlist.length < mapAdminMesage.limit) {
                $('.MapAuthorModal-modal-scroll-end').addClass('hide')
                $('.MapAuthorModal-modal-scroll').off('scroll')
            } else {
                $('.MapAuthorModal-modal-scroll-end').removeClass('hide')
                mapAdminfunpage()
            }
        }, 300)
        $('.MapAuthorModal-modal-thead-tr').each(function () {
            if ($(this).attr('data-type') == mapAdminMesage.type) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
        mapAdminMesage.localList = getlist
        mapAdminMesage.chooseIndex = 0
        $('.MapAuthorModal-modal-tbody').html(template('MapAuthorModal-modal-tbody-script', { list: getlist, type: mapAdminMesage.type }))
        if (getlist.length > 0) {
            mapAdminMesage.chooseItem = mapAdminMesage.localList[mapAdminMesage.chooseIndex]
            $('.MapAuthorModal-modal-tbody tr').eq(mapAdminMesage.chooseIndex).addClass('active')
            mapAdminMesage.chooseItem.author = userInfo.username
            $('.MapAuthorModal-details').html(template('MapAuthorModal-details-script', { item: mapAdminMesage.chooseItem, type: mapAdminMesage.type, author: userInfo.username }))
        } else {
            $('.MapAuthorModal-details').html("")
        }
        return false
    }
    $.controlAjax({
        type: "get",
        url: '/api/lobby/author/maps/',
        data: {
            page: mapAdminMesage.page,
            limit: mapAdminMesage.limit,
            status: mapAdminMesage.type,
            isPrivate: mapAdminMesage.isPrivate
        },
        success: function (res) {
            $('.MapAuthorModal-modal-scroll').off('scroll')
            $('.MapAuthorModal-modal-scroll').scrollTop(0)
            var getlist = res.data.list
            setTimeout(() => {
                if (getlist.length < mapAdminMesage.limit) {
                    $('.MapAuthorModal-modal-scroll-end').addClass('hide')
                    $('.MapAuthorModal-modal-scroll').off('scroll')
                } else {
                    $('.MapAuthorModal-modal-scroll-end').removeClass('hide')
                    mapAdminfunpage()
                }
            }, 300)
            $('.MapAuthorModal-modal-thead-tr').each(function () {
                if ($(this).attr('data-type') == mapAdminMesage.type) {
                    $(this).removeClass('hide')
                } else {
                    $(this).addClass('hide')
                }
            })
            mapAdminMesage.localList = getlist
            mapAdminMesage.chooseIndex = 0
            $('.MapAuthorModal-modal-tbody').html(template('MapAuthorModal-modal-tbody-script', { list: getlist, type: mapAdminMesage.type }))
            if (getlist.length > 0) {
                mapAdminMesage.chooseItem = mapAdminMesage.localList[mapAdminMesage.chooseIndex]
                $('.MapAuthorModal-modal-tbody tr').eq(mapAdminMesage.chooseIndex).addClass('active')
                $('.MapAuthorModal-details').html(template('MapAuthorModal-details-script', { item: mapAdminMesage.chooseItem, type: mapAdminMesage.type, author: userInfo.username }))
            } else {
                $('.MapAuthorModal-details').html("")
            }
        },
        error: function (req) {
            spop({
                template: req.errorMsg,
                autoclose: 3000,
                style: 'error',
                group: 'mapAdmin',
            });
        }
    })
}
//分页事件
function mapAdminfunpage(page) {
    $('.MapAuthorModal-modal-scroll').on('scroll', function () {
        var $this = $(this)
        var nScrollHight = $this[0].scrollHeight,
            nScrollTop = $this[0].scrollTop,
            nDivHight = $this.height();
        if (nScrollTop + nDivHight + 180 >= nScrollHight && !mapAdminMesage.isLoading) {
            mapAdminMesage.page = mapAdminMesage.page + 1
            mapAdminPage()
        }
    })
}
//分页渲染
function mapAdminPage() {
    if (mapAdminMesage.isLoading) {
        return false;
    }
    mapAdminMesage.isLoading = true
    $.controlAjax({
        type: "get",
        url: '/api/lobby/author/maps/',
        data: {
            page: mapAdminMesage.page,
            limit: mapAdminMesage.limit,
            status: mapAdminMesage.type,
            isPrivate: mapAdminMesage.isPrivate
        },
        success: function (res) {
            mapAdminMesage.isLoading = false
            var getlist = res.data.list
            mapAdminMesage.localList = mapAdminMesage.localList.concat(getlist)
            $('.MapAuthorModal-modal-tbody').append(template('MapAuthorModal-modal-tbody-script', { list: getlist, type: mapAdminMesage.type }))
            if (getlist.length < mapAdminMesage.limit) {
                $('.MapAuthorModal-modal-scroll-end').addClass('hide')
                $('.MapAuthorModal-modal-scroll').off('scroll')
            } else {
                $('.MapAuthorModal-modal-scroll-end').removeClass('hide')
            }
        },
        error: function (req) {
            spop({
                template: req.errorMsg,
                autoclose: 3000,
                style: 'error',
                group: 'mapAdmin',
            });
        }
    })
}
// 申请弹窗
$(document).on('click', '#MapAuthorModal-apply-show', function (e) {
    $('#NormalModal').modal('hide')
    setTimeout(() => {
        $('.normalModal-content').html(template('MapAuthorModal-apply-script'))
        $('#NormalHeightModal').modal('show')
    }, 300)
    mapAdminMesage.apply = null
    mapAdminMesage.qq = null
})
// 申请弹窗
$(document).on('click', '#MapAuthorModal-apply-btn', function (e) {
    if (!mapAdminMesage.apply) {
        spop({
            template: '请填写申请理由',
            autoclose: 3000,
            style: 'error',
            group: 'mapAdmin',
        });
        return false
    }
    if (!mapAdminMesage.qq) {
        spop({
            template: '请填写QQ号',
            autoclose: 3000,
            style: 'error',
            group: 'mapAdmin',
        });
        return false
    }
    $.controlAjax({
        type: "get",
        url: '/api/lobby/author/apply/',
        data: {
            reason: mapAdminMesage.apply,
            qq: mapAdminMesage.qq
        },
        success: function (res) {
            $('#NormalHeightModal').modal('hide')
            spop({
                template: '申请成功,等待审核',
                autoclose: 3000,
                style: 'success',
                group: 'mapAdmin',
            });
        },
        error: function (req) {
            spop({
                template: req.errorMsg,
                autoclose: 3000,
                style: 'error',
                group: 'mapAdmin',
            });
        }
    })
})
// 监听申请理由
$(document).on('input', '#MapAuthorModal-apply-text', function (e) {
    mapAdminMesage.apply = $(this).val()
})
// 监听申请理由
$(document).on('input', '#MapAuthorModal-apply-qq', function (e) {
    mapAdminMesage.qq = $(this).val()
})
// 监听上传新地图
$(document).on('click', '.J_showMapAuthorModal_updata', function (e) {
    $('.MapAuthorModal-admin-content').addClass('hide')
    let message = { JModal: comOption.optionsMode, JSet: comOption.settingArray, gameVersion: 2 }
    message.JModal.map((item) => {
        item.value = false
    })
    message.JSet.map((item) => {
        item.value = 0
    })
    $('.MapAuthorModal-update-content').html(template('MapAuthorModal-update-script', message))
    $('#MapAuthorModal .modal-dialog').addClass('MapAuthorModalUpdateTop')
    $('.MapAuthorModal-update-content').removeClass('hide')
    $('#MapAuthorModal .modal-dialog').css('width', '480px')
    mapAdminMesage.updata = {
        name: null,
        url: [],
        desc: null,
        version: null,
        mapId: null,
        authorName: null
    }
    mapAdminMesage.updata.JModal = comOption.optionsMode
    $('.J_MapAuthorModal_up_sure').text(translatesrting('上传'))
})
// 监听上传返回
$(document).on('click', '.J_showMapAuthorModal_updata_return', function (e) {
    mapAdminMesage.updataAdd = []
    $('.MapAuthorModal-admin-content').removeClass('hide')
    $('.MapAuthorModal-update-content').addClass('hide')
    $('.MapAuthorModal-update-content').html('')
    $('#MapAuthorModal .modal-dialog').removeClass('MapAuthorModalUpdateTop')
    $('#MapAuthorModal .modal-dialog').css('width', '700px')
})
// 监听上传地址
$(document).on('change', '#J_MapAuthorModal_up', function (e) {
    var reg = /(.*)\\/;
    var gameUrl = []
    // e.currentTarget.files.each(function () {
    //     gameUrl.push($(this).path)
    // })
    for (var i = 0; i < e.currentTarget.files.length; i++) {
        gameUrl.push(e.currentTarget.files[i].path)
    }
    $('.J_MapAuthorModal_up_show').val(gameUrl[0] + '\n')
    mapAdminMesage.updata.url = gameUrl
})
// 监听上传图片地址
$(document).on('change', '#J_MapAuthorModal_up_img', function (e) {
    var reg = /(.*)\\/;
    var gameUrl = []
    // e.currentTarget.files.each(function () {
    //     gameUrl.push($(this).path)
    // })
    for (var i = 0; i < e.currentTarget.files.length; i++) {
        gameUrl.push(e.currentTarget.files[i].path)
    }
    $('.J_MapAuthorModal_up_img_show').val(gameUrl[0] + '\n')
    mapAdminMesage.updata.imageFilePath = gameUrl[0]
})
function mapAdminEndBtn() {
    if (mapAdminMesage.CheckEnd) {
        $('.J_MapAuthorModal_up_sure').click()
        mapAdminMesage.CheckEnd = false
    }
}
// 监听上传名称
$(document).on('input', '.MapAuthorModal-updata-content .J_MapAuthorModal_up_name', function (e) {
    mapAdminMesage.updata.name = $(this).val()
    mapAdminMesage.nameCheck = false
})
// 监听上传名称
$(document).on('blur', '.MapAuthorModal-updata-content .J_MapAuthorModal_up_name', function (e) {
    if (mapAdminMesage.updata.name == "" || mapAdminMesage.updata.name == null) {
        $('.J_MapAuthorModal_up_name-error').addClass('hide')
        mapAdminMesage.nameCheck = true
        return false
    }
    mapAdminMesage.nameCheckLoading = true
    $.star({
        type: 'POST',
        url: '/community-security/sensitive/v1/check',
        showerror: true,
        data: {
            word: mapAdminMesage.updata.name,
        },
        success: function (res) {
            mapAdminMesage.nameCheckLoading = false
            $('.J_MapAuthorModal_up_name-error').addClass('hide')
            mapAdminMesage.nameCheck = true
            mapAdminEndBtn()
        },
        error: function (req) {
            mapAdminMesage.nameCheckLoading = false
            if (req.errorCode == 220001) {
                mapAdminMesage.nameCheck = false
                $('.J_MapAuthorModal_up_name-error').text(req.errorMsg)
                $('.J_MapAuthorModal_up_name-error').removeClass('hide')
            }
            mapAdminEndBtn()
        }
    });
})
// 监听上传作者
$(document).on('input', '.MapAuthorModal-updata-content .J_MapAuthorModal_up_author', function (e) {
    mapAdminMesage.updata.authorName = $(this).val()
    mapAdminMesage.authorcCheck = false
})
// 监听上传作者
$(document).on('blur', '.MapAuthorModal-updata-content .J_MapAuthorModal_up_author', function (e) {
    if (mapAdminMesage.updata.authorName == "" || mapAdminMesage.updata.authorName == null) {
        $('.J_MapAuthorModal_up_author-error').addClass('hide')
        mapAdminMesage.authorcCheck = true
        return false
    }
    mapAdminMesage.authorCheckLoading = true
    $.star({
        type: 'POST',
        url: '/community-security/sensitive/v1/check',
        showerror: true,
        data: {
            word: mapAdminMesage.updata.authorName,
        },
        success: function (res) {
            mapAdminMesage.authorCheckLoading = false
            $('.J_MapAuthorModal_up_author-error').addClass('hide')
            mapAdminMesage.authorcCheck = true
            mapAdminEndBtn()
        },
        error: function (req) {
            mapAdminMesage.authorCheckLoading = false
            if (req.errorCode == 220001) {
                mapAdminMesage.authorcCheck = false
                $('.J_MapAuthorModal_up_author-error').text(req.errorMsg)
                $('.J_MapAuthorModal_up_author-error').removeClass('hide')
            }
            mapAdminEndBtn()
        }
    });
})
// 监听上传描述
$(document).on('input', '.MapAuthorModal-updata-content .J_MapAuthorModal_up_desc', function (e) {
    mapAdminMesage.updata.desc = $(this).val()
    mapAdminMesage.descCheck = false
})
// 监听上传描述
$(document).on('blur', '.MapAuthorModal-updata-content .J_MapAuthorModal_up_desc', function (e) {
    if (mapAdminMesage.updata.desc == "" || mapAdminMesage.updata.desc == null) {
        $('.J_MapAuthorModal_up_desc-error').addClass('hide')
        mapAdminMesage.descCheck = true
        return false
    }
    mapAdminMesage.descCheckLoading = true
    $.star({
        type: 'POST',
        url: '/community-security/sensitive/v1/check',
        showerror: true,
        data: {
            word: mapAdminMesage.updata.desc,
        },
        success: function (res) {
            mapAdminMesage.descCheckLoading = false
            $('.J_MapAuthorModal_up_desc-error').addClass('hide')
            mapAdminMesage.descCheck = true
            mapAdminEndBtn()
        },
        error: function (req) {
            mapAdminMesage.descCheckLoading = false
            if (req.errorCode == 220001) {
                mapAdminMesage.descCheck = false
                $('.J_MapAuthorModal_up_desc-error').text(req.errorMsg)
                $('.J_MapAuthorModal_up_desc-error').removeClass('hide')
            }
            mapAdminEndBtn()
        }
    });
})
// 监听版本号
$(document).on('input', '.MapAuthorModal-updata-content .J_MapAuthorModal_up_version', function (e) {
    var $this = $(this)
    this.value = this.value.replace(/[\u4e00-\u9fa5]/ig, '')
    this.value = this.value.replace(/[a-z]/ig, '')
    this.value = this.value.replace(/[`~!@#$%^&*()_\-+=<>?:"{}|,\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘'，。、]/im, '')
    mapAdminMesage.updata.version = $this.val()
})

// 更新地图
$(document).on('click', '.J_MapAuthorModal_up_old', function (e) {
    $('.MapAuthorModal-admin-content').addClass('hide')
    $('.MapAuthorModal-update-content').removeClass('hide')
    $('#MapAuthorModal .modal-dialog').css('width', '480px')
    mapAdminMesage.updata = {
        name: mapAdminMesage.chooseItem.mapName ? mapAdminMesage.chooseItem.mapName : mapAdminMesage.chooseItem.name,
        url: [],
        desc: mapAdminMesage.chooseItem.description,
        version: mapAdminMesage.chooseItem.mapVersion,
        mapId: mapAdminMesage.chooseItem.mapId,
        authorName: mapAdminMesage.chooseItem.author,
        privateStatus: mapAdminMesage.chooseItem.privateStatus,
        preset: mapAdminMesage.chooseItem.preset ? JSON.parse(mapAdminMesage.chooseItem.preset) : '',
        gameVersion: mapAdminMesage.chooseItem.gameVersion
    }
    mapAdminMesage.updata.JModal = comOption.optionsMode
    mapAdminMesage.updata.JSet = comOption.settingArray
    mapAdminMesage.updata.JModal.map((item) => {
        for (let i = 0; i < mapAdminMesage.chooseItem.models.length; i++) {
            if (item.code == mapAdminMesage.chooseItem.models[i]) {
                item.value = true
            } else {
            }
        }
    })
    mapAdminMesage.updata.JSet.map((item) => {
        item.value = 0
    })
    if (mapAdminMesage.updata.preset) {
        mapAdminMesage.updata.JSet.map((item) => {
            for (let i = 0; i < mapAdminMesage.updata.preset.length; i++) {
                if (item.code == mapAdminMesage.updata.preset[i].key) {
                    if (mapAdminMesage.updata.preset[i].value) {
                        item.value = 1
                    } else {
                        item.value = 2
                    }
                }
            }
        })
    }
    $('.MapAuthorModal-update-content').html(template('MapAuthorModal-update-script', mapAdminMesage.updata))
    $('#MapAuthorModal .modal-dialog').addClass('MapAuthorModalUpdateTop')
    $('.J_MapAuthorModal_up_sure').text(translatesrting('更新'))
    mapAdminMesage.descCheck = true
    mapAdminMesage.nameCheck = true
    mapAdminMesage.authorcCheck = true
    $('#MapAuthorModal .MapAuthorModal-left-bot').addClass('hide')
})
// 确定上传
$(document).on('click', '.J_MapAuthorModal_up_sure', function (e) {
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    if (mapAdminMesage.descCheckLoading) {
        spop({
            template: '地图描述检测中',
            autoclose: 3000,
            style: 'error',
            group: 'mapAdmin',
        });
        mapAdminMesage.CheckEnd = true
        return false
    }
    if (mapAdminMesage.nameCheckLoading) {
        spop({
            template: '地图名检测中',
            autoclose: 3000,
            style: 'error',
            group: 'mapAdmin',
        });
        mapAdminMesage.CheckEnd = true
        return false
    }
    if (mapAdminMesage.authorCheckLoading) {
        spop({
            template: '作者名检测中',
            autoclose: 3000,
            style: 'error',
            group: 'mapAdmin',
        });
        mapAdminMesage.CheckEnd = true
        return false
    }
    if (!mapAdminMesage.descCheck) {
        spop({
            template: '地图描述检测未通过',
            autoclose: 3000,
            style: 'error',
            group: 'mapAdmin',
        });
        return false
    }

    if (!mapAdminMesage.nameCheck) {
        spop({
            template: '地图名检测未通过',
            autoclose: 3000,
            style: 'error',
            group: 'mapAdmin',
        });
        return false
    }
    if (!mapAdminMesage.authorcCheck) {
        spop({
            template: '作者名检测未通过',
            autoclose: 3000,
            style: 'error',
            group: 'mapAdmin',
        });
        return false
    }
    if (mapAdminMesage.updata.name == "" || mapAdminMesage.updata.name == null) {
        spop({
            template: '地图名不得为空',
            autoclose: 3000,
            style: 'error',
            group: 'mapAdmin',
        });
        return false
    }
    if (mapAdminMesage.updata.imageFilePath == "" || mapAdminMesage.updata.imageFilePath == null) {
        if ($('.J_MapAuthorModal_up_sure').text() != translatesrting('更新')) {
            spop({
                template: '缩略图不得为空',
                autoclose: 3000,
                style: 'error',
                group: 'mapAdmin',
            });
            return false
        }
    }
    if (mapAdminMesage.updata.version == "" || mapAdminMesage.updata.version == null) {
        spop({
            template: '版本号不得为空',
            autoclose: 3000,
            style: 'error',
            group: 'mapAdmin',
        });
        return false
    }
    if (mapAdminMesage.updata.authorName == "" || mapAdminMesage.updata.authorName == null) {
        spop({
            template: '作者名不得为空',
            autoclose: 3000,
            style: 'error',
            group: 'mapAdmin',
        });
        return false
    } else {
        let pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——=|{}【】‘；：”“'。，、？%]");
        let result = mapAdminMesage.updata.authorName.match(pattern);
        if (result) {
            spop({
                template: '作者名不允许输入‘-’、‘_’以外的特殊字符',
                autoclose: 3000,
                style: 'error',
                group: 'mapAdmin',
            });
            return false
        }
    }
    if (mapAdminMesage.updata.desc == "" || mapAdminMesage.updata.desc == null) {
        spop({
            template: '地图描述不得为空',
            autoclose: 3000,
            style: 'error',
            group: 'mapAdmin',
        });
        return false
    }
    let upmode = []
    $('.MapAuthorModal-mode-item').each(function () {
        if ($(this).find('input:checkbox').is(':checked')) {
            upmode.push($(this).find('input:checkbox').attr('data-code'))
        }
    })
    if ($('.J_MapAuthorModal_up_sure').text() == translatesrting('更新')) {
        let JModalarray = []
        mapAdminMesage.updata.JModal.map((item) => {
            if (item.value == true) {
                JModalarray.push(item.code)
            }
        })
        let diffarray = []
        if (JModalarray.length > upmode.length) {
            for (key in JModalarray) {
                var stra = JModalarray[key];
                var count = 0;
                for (var j = 0; j < upmode.length; j++) {
                    var strb = upmode[j];
                    if (stra == strb) {
                        count++;
                    }
                }
                if (count === 0) { //表示数组1的这个值没有重复的，放到arr3列表中  
                    diffarray.push(stra);
                }
            }
        } else {
            for (key in upmode) {
                var stra = upmode[key];
                var count = 0;
                for (var j = 0; j < JModalarray.length; j++) {
                    var strb = JModalarray[j];
                    if (stra == strb) {
                        count++;
                    }
                }
                if (count === 0) { //表示数组1的这个值没有重复的，放到arr3列表中  
                    diffarray.push(stra);
                }
            }
        }
        if (diffarray.length == 0 && (mapAdminMesage.updata.url == "" || mapAdminMesage.updata.url == null)) {
            upmode = []
        }
    }
    let isares = false
    if ($('#MapAuthorModal-ares').is(':checked')) {
        isares = true
    }
    let addfile = []
    $('.MapAuthorModal-add-div-item').each(function () {
        addfile.push($(this).attr('data-code'))
    })
    if (addfile.length > 0 && (mapAdminMesage.updata.url == "" || mapAdminMesage.updata.url == null)) {
        spop({
            template: '上传附件时需上传地图文件',
            autoclose: 3000,
            style: 'error',
            group: 'mapAdmin',
        });
        return false
    }
    if (addfile.length > 0) {
        let isinname = false
        let pattern = /.*\.(?:(?!(exe|dll|bat|edt|edb|fnt|xdp)).)+/;
        addfile.map((item) => {
            let result = pattern.test(item)
            if (!result) {
                isinname = true
            }
        })
        if (isinname) {
            spop({
                template: 'exe、dll、bat、edt、edb、fnt、xdp扩展名文件禁止上传',
                autoclose: 3000,
                style: 'error',
                group: 'mapAdmin',
            });
            return false;
        }
    }
    let isPrivate = false
    if ($('#MapAuthorModalgameOptionPrivate').is(':checked')) {
        isPrivate = true
    }
    // 游戏设置
    let preset = []
    $('.J_gameSetting').each(function () {
        var $set = $(this)
        if ($set.prop("checked") && $set.val() != 0) {
            let itemarray = {
                cnValue: $set.attr('cnValue'),
                key: $set.attr('name'),
                value: $set.val() == 2 ? false : true,
                valueType: 0,
                must: true
            }
            preset.push(itemarray)
        }
    })
    let gameVersion = 2
    if ($('#J_gameMapedition1').prop("checked") && !$('#J_gameMapedition2').prop("checked")) {
        gameVersion = 1
    }
    if ($('#J_gameMapedition2').prop("checked") && !$('#J_gameMapedition1').prop("checked")) {
        gameVersion = 0
    }
    let itemarray = {
        cnValue: '游戏版本',
        key: 'GameVersion',
        value: gameVersion,
        valueType: 0,
        must: true
    }
    preset.push(itemarray)
    let getmessage = {
        gameVersion: gameVersion,
        preset: JSON.stringify(preset),
        maps: mapAdminMesage.updata.url,
        name: mapAdminMesage.updata.name,
        version: mapAdminMesage.updata.version,
        desc: mapAdminMesage.updata.desc,
        imageFilePath: mapAdminMesage.updata.imageFilePath,
        authorName: mapAdminMesage.updata.authorName,
        models: upmode,
        isAres: isares,
        mapConfiges: addfile,
        isPrivate: isPrivate
    }
    if (mapAdminMesage.updata.mapId) {
        getmessage.mapId = mapAdminMesage.updata.mapId
    }
    $this.gxbtn('loading')
    $.controlAjax({
        type: "POST",
        url: '/Hall/UploadMap/',
        data: getmessage,
        success: function (res) {
            mapAdminMesage.updata = {
                name: null,
                url: [],
                desc: null,
                version: null,
                mapId: null,
                authorName: null
            }
            let message = { JModal: comOption.optionsMode, JSet: comOption.settingArray, gameVersion: 2 }
            message.JModal.map((item) => {
                item.value = false
            })
            message.JSet.map((item) => {
                item.value = 0
            })
            $('.MapAuthorModal-update-content').html(template('MapAuthorModal-update-script', message))
            $('#MapAuthorModal .modal-dialog').removeClass('MapAuthorModalUpdateTop')
            spop({
                template: '上传成功',
                autoclose: 3000,
                style: 'success',
                group: 'mapAdmin',
            });
            $('.MapAuthorModal-admin-content').removeClass('hide')
            $('.MapAuthorModal-update-content').addClass('hide')
            $('#MapAuthorModal .modal-dialog').css('width', '700px')
            if (mapAdminMesage.updata.mapId) {
                mapAdminMesage.type = 0
                $('.MapAuthorModal-modal-nav').each(function () {
                    $(this).removeClass('active')
                    if ($(this).attr('data-type') == 0) {
                        $(this).addClass('active')
                    }
                })
            }
            mapAdminMesage.page = 1
            mapAdminInit()
        },
        error: function (req) {
            $this.gxbtn('reset')
            spop({
                template: req.errorMsg,
                autoclose: 3000,
                style: 'error',
                group: 'mapAdmin',
            });
        }
    })
})

// 列表选中
$(document).on('click', '.MapAuthorModal-modal-tbody-item', function () {
    var mapId = $(this).attr('data-id')
    mapAdminMesage.localList.map((item, index) => {
        if (item.mapId == mapId) {
            mapAdminMesage.chooseItem = item
            mapAdminMesage.chooseIndex = index
            $('.MapAuthorModal-modal-tbody tr').removeClass('active')
            $('.MapAuthorModal-modal-tbody tr').eq(mapAdminMesage.chooseIndex).addClass('active')
            $('.MapAuthorModal-details').html(template('MapAuthorModal-details-script', { item: mapAdminMesage.chooseItem, type: mapAdminMesage.type, author: userInfo.username }))
        }

    })
})
// 添加附件
$(document).on('change', '#MapAuthorModal-add-input', function (e) {
    for (var i = 0; i < e.currentTarget.files.length; i++) {
        mapAdminMesage.updataAdd.push(e.currentTarget.files[i].path)
    }
    $('.MapAuthorModal-add-div').html(template('MapAuthorModal-add-div-script', { items: mapAdminMesage.updataAdd }))
    $(this).val("")
})
$(document).on('click', '.J_MapAuthorModal-add-del-show', function (e) {
    $(this).closest('div').addClass('hide')
    $('.J_MapAuthorModal-add-del').closest('div').removeClass('hide')
    $('.MapAuthorModal-add-div').addClass('MapAuthorModal-add-div-show')
})
$(document).on('click', '.J_MapAuthorModal-add-del-hide', function (e) {
    $(this).closest('div').addClass('hide')
    $('.J_MapAuthorModal-add-del-show').closest('div').removeClass('hide')
    $('.MapAuthorModal-add-div').removeClass('MapAuthorModal-add-div-show')
})
$(document).on('click', '.J_MapAuthorModal-add-choose', function (e) {
    $('.MapAuthorModal-add-div').find('.MapAuthorModal-add-div-item').each(function () {
        $(this).find('input').prop('checked', "true")
    })
})

// 删除附件
$(document).on('click', '.J_MapAuthorModal-add-del', function (e) {
    $('.MapAuthorModal-add-div').find('.MapAuthorModal-add-div-item').each(function () {
        if ($(this).find('input:checkbox').is(':checked')) {
            for (let i = 0; i < mapAdminMesage.updataAdd.length; i++) {
                if ($(this).attr('data-code') == mapAdminMesage.updataAdd[i]) {
                    mapAdminMesage.updataAdd.splice(i, 1)
                }
            }
            $(this).remove()
        }
    })
})
// // 删除全部附件
// $(document).on('click', '.J_MapAuthorModal-add-del-all', function (e) {
//     mapAdminMesage.updataAdd = []
//     $('.MapAuthorModal-add-div').find('.MapAuthorModal-add-div-item').each(function () {
//         $(this).remove()
//     })
// })
// 删除
$(document).on('click', '.J_MapAuthorModal-del', function (e) {
    var $this = $(this)
    var mapid = $this.closest('tr').attr('data-id')
    gxmodal({
        title: translatesrting('提示'),
        centent: '<div class="mt25">' + translatesrting('是否删除此地图?') + '</div>',
        buttons: [
            {
                text: translatesrting('确定'),
                class: 'btn-middle btn-middle-blue',
                callback: function () {
                    $.star({
                        type: 'POST',
                        url: '/battlecenter/redwar/map/v1/delete',
                        showerror: true,
                        data: {
                            mapId: mapid,
                        },
                        success: function (res) {
                            spop({
                                template: translatesrting('删除成功'),
                                autoclose: 2000,
                                style: 'success',
                                group: 'mapAdmin',
                            });
                            $this.closest('tr').remove()
                        },
                        error: function (req) {

                        }

                    });
                }
            },
            {
                text: translatesrting('取消'),
                class: 'btn-middle btn-style-ash',
            }
        ]
    })
})
// 下一步
$(document).on('click', '.MapAuthorModal-content-page-one-btn', function () {
    $('.MapAuthorModal-content-page-one').addClass('hide')
    $('.MapAuthorModal-content-page-two').removeClass('hide')
    $('.MapAuthorModal-update-content .gx-modal-header .ft-indigo-light').each(function () {
        var $this = $(this)
        var index = $this.attr('data-index')
        $this.addClass('hide')
        if (index == 2) {
            $this.removeClass('hide')
        }
    })
})
$(document).on('click', '.MapAuthorModal-content-page-two-btn', function () {
    $('.MapAuthorModal-content-page-two').addClass('hide')
    $('.MapAuthorModal-content-page-three').removeClass('hide')
    $('.MapAuthorModal-update-content .gx-modal-header .ft-indigo-light').each(function () {
        var $this = $(this)
        var index = $this.attr('data-index')
        $this.addClass('hide')
        if (index == 3) {
            $this.removeClass('hide')
        }
    })
})
$(document).on('click', '.MapAuthorModal-content-page-threebefor-btn', function () {
    $('.MapAuthorModal-content-page-three').addClass('hide')
    $('.MapAuthorModal-content-page-two').removeClass('hide')
    $('.MapAuthorModal-update-content .gx-modal-header .ft-indigo-light').each(function () {
        var $this = $(this)
        var index = $this.attr('data-index')
        $this.addClass('hide')
        if (index == 2) {
            $this.removeClass('hide')
        }
    })
})
$(document).on('click', '.MapAuthorModal-content-page-twobefor-btn', function () {
    $('.MapAuthorModal-content-page-two').addClass('hide')
    $('.MapAuthorModal-content-page-one').removeClass('hide')
    $('.MapAuthorModal-update-content .gx-modal-header .ft-indigo-light').each(function () {
        var $this = $(this)
        var index = $this.attr('data-index')
        $this.addClass('hide')
        if (index == 1) {
            $this.removeClass('hide')
        }
    })
})
