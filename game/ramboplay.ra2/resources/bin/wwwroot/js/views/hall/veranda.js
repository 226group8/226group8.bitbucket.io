var verandaMessage = {
    type: 1,//现在展示内容 1列表模式 2地图模式 3地图房间列表
    init: null,
    roomscreen: {
        'war': true,
        'yuri': true,
        'republic': true,
    },//筛选房间
    hotswiper: null,//推荐swiper
    localRoomlist: [],//本地维护全部数据
    showRoomlist: [],//展示list
    getmapList: null,//地图模式列表
    screenModes: null,//model筛选
    screenLabel: null,//类型筛选
    joinloading: false,//加入房间共享CD
    passJoinId: null,//密码房
    topMapList: [],//头部热门地图
    MapList: [],//地图列表
    fastChoosMap: [],//选择快速加入的房间
    fastLaoding: false,// 正在快速加入
    MapfastLaoding: false,// 列表正在快速加入
    mouseinchat: false,//鼠标是否在喊话中
    fastClose: null,//
    downMapset: null,
    returnTime: 3,
    Jsousuofun: "",//搜索,
    JsousuofunTime: null,//搜索自动,
    maplabel: [
        { name: '经典对战', code: 'Battle', value: true },
        { name: '娱乐对战', code: 'Recreation', value: true },
        { name: '任务', code: 'Task', value: true },
        { name: '防守', code: 'Defense', value: true },
    ],//地图模式筛选label
    mapPageLoading: false,//地图筛选方法计时
    refreshloading: 5,//刷新计时
    refreshloadingfun: null,//刷新计时
    autoReset: null,
    optionsGameVersion: [
        { name: '尤里的复仇', code: '0', en: 'yuri' },
        { name: '红警原版', code: '1', en: 'war' },
        { name: '共和国之辉', code: '3', en: 'republic' }
    ],
    roomPage: 1,
    pagelimit: 30
}
var isfirstverandaMessageinit = true;
// 初始化
verandaMessage.init = function () {
    // if (!isfirstverandaMessageinit) {
    //     return false
    // }
    // isfirstverandaMessageinit = false
    // 保留用户选项
    if (getSaveMessage('roomscreen')) {
        let roomscreen = getSaveMessage('roomscreen').list
        if (roomscreen['war'] == true || roomscreen['war'] == false) {
            verandaMessage.roomscreen.war = roomscreen['war']
        }
        if (roomscreen['yuri'] == true || roomscreen['yuri'] == false) {
            verandaMessage.roomscreen.yuri = roomscreen['yuri']
        }
        if (roomscreen['republic'] == true || roomscreen['republic'] == false) {
            verandaMessage.roomscreen.republic = roomscreen['republic']
        }
    }
    if (getSaveMessage('mapscreen')) {
        let mapscreen = getSaveMessage('mapscreen').list
        mapscreen.map((item) => {
            verandaMessage.maplabel.map((localitem) => {
                if (localitem.code == item.code) {
                    localitem.value = item.value
                }
            })
        })
    }
    if (verandaMessage.type == 1) {
        $('.veranda-screen').html(template("veranda-screen-script", { list: verandaMessage.roomscreen }))
    } else {
        $('.veranda-screen').html(template("veranda-screen-map-script", { maplabel: verandaMessage.maplabel }))
    }
    // 获取热门地图
    // verandaMessage.getHotMap()
    // 获取房间列表
    verandaMessage.autoReset = setInterval(() => {
        verandaMessage.getPageMessage('clear')
    }, 30000)
    verandaMessage.getPageMessage()
    let cModel = ""
    let cLabel = ""
    let optionsMode = comOption.optionsMode
    let labelArray = comOption.labelArray
    optionsMode.map((item, i) => {
        if (item.code == verandaMessage.screenModes) {
            cModel = item.name
        }
    })
    labelArray.map((item, i) => {
        if (item.code == verandaMessage.screenLabel) {
            cLabel = item.name
        }
    })
    $('.veranda-content-bottom').html(template("veranda-bottom-script", {
        type: 'roomlist',
        comOptionMode: optionsMode,
        comOptionLabel: labelArray,
        cModel: cModel,
        cLabel: cLabel,
    }))
    $('[data-toggle="tooltip"]').tooltip()
    $('.veranda-header-gxmenu').gxmenu({
        height: "20px",
        top: "40px",
        clickHide: true//点击内部是否消失
    }, function (e) {

    })
    if (localStorage.getItem('isbusy') && !localStorage.getItem('verandaType')) {
        $('.veranda-content-middle-nav').each(function () {
            if ($(this).attr('data-type') == 2) {
                $(this).click()
            }
        })
    } else if (localStorage.getItem('verandaType')) {
        $('.veranda-content-middle-nav').each(function () {
            if ($(this).attr('data-type') == localStorage.getItem('verandaType')) {
                $(this).click()
            }
        })
    }
}
// 改变筛选版本
$(document).on('click', '.veranda-list-check', function () {
    var $this = $(this)
    let type = $this.attr('data-type')
    if (type == 'room') {
        if (verandaMessage.pageLoading) {
            showtoast({
                message: translatesrting('筛选房间过于频繁')
            })
            return false
        }
        let active = false
        if ($this.hasClass('veranda-list-check-active')) {
            active = false
            $this.removeClass('veranda-list-check-active')
        } else {
            active = true
            $this.addClass('veranda-list-check-active')
        }
        verandaMessage.roomscreen[$this.attr('data-name')] = active
        saveMessage('roomscreen', { "list": verandaMessage.roomscreen })
        verandaMessage.getPageMessage('clear')
    } else if (type == 'map') {
        clearTimeout(verandaMessage.mapPageLoading)
        let code = $this.attr('data-name')
        let active = false
        if ($this.hasClass('veranda-list-check-active')) {
            active = false
            $this.removeClass('veranda-list-check-active')
        } else {
            active = true
            $this.addClass('veranda-list-check-active')
        }
        verandaMessage.maplabel.map((item) => {
            if (item.code == code) {
                item.value = active
            }
        })
        saveMessage('mapscreen', { "list": verandaMessage.maplabel })
        verandaMessage.mapPageLoading = setTimeout(() => {
            verandaMessage.getmapList()
        }, 300)
    }
})
// 改变筛选模式
$(document).on('click', '.J_veranda_s_modal', function () {
    var $this = $(this)
    verandaMessage.screenModes = $this.attr('data-code') || null
    var cModel = ""
    if (verandaMessage.screenModes == null) {
        cModel = translatesrting('模式')
    } else {
        let optionsMode = comOption.optionsMode
        optionsMode.map((item, i) => {
            if (item.code == verandaMessage.screenModes) {
                cModel = item.name
            }
        })
    }
    $this.closest('.gx-menu').find('.veranda-header-gxmenu-text').text(translatesrting(cModel))
    verandaMessage.getPageMessage('clear')
})
// 改变筛选label
$(document).on('click', '.J_veranda_s_label', function () {
    var $this = $(this)
    verandaMessage.screenLabel = $this.attr('data-code') || null
    if ($this.attr('data-code') == "") {
        verandaMessage.screenLabel = null
    }
    var cLabel = ""
    if (verandaMessage.screenLabel == null) {
        cLabel = translatesrting('类型')
    } else {
        let labelArray = comOption.labelArray
        labelArray.map((item, i) => {
            if (item.code == verandaMessage.screenLabel) {
                cLabel = item.name
            }
        })
    }
    $this.closest('.gx-menu').find('.veranda-header-gxmenu-text').text(translatesrting(cLabel))
    verandaMessage.getPageMessage('clear')
})

// 获取热门地图
// verandaMessage.getHotMap = function () {
//     $.star({
//         type: 'GET',
//         url: '/battlecenter/redwar/map/v1/config/popular/map',
//         showerror: true,
//         data: {

//         },
//         success: async function (res) {
//             let testhtml = ''
//             verandaMessage.topMapList = JSON.parse(JSON.stringify(res.data))
//             // --------图片数据处理
//             let topMapList = res.data
//             let imgarray = new Map()
//             for (var i = 0; i < topMapList.length; i++) {
//                 imgarray.set(topMapList[i].imageUrl + '?x-oss-process=image/resize,w_350', '')
//             }
//             let dellimgarray = await readerIndexedDBImgUrl(imgarray)
//             for (list of topMapList) {
//                 let itemimg = list.imageUrl + '?x-oss-process=image/resize,w_350'
//                 if (dellimgarray.has(itemimg)) {
//                     if (dellimgarray.get(itemimg)) {
//                         list.imageUrl = dellimgarray.get(itemimg)
//                     } else {
//                         list.imageUrl = itemimg
//                     }
//                 }
//             }
//             // --------图片数据处理
//             for (var i = 0; i < topMapList.length; i++) {
//                 testhtml += '<div class="col-xs-6">' + template("veranda-map-script", { list: topMapList[i], lazy: 'js' }) + '</div>'
//             }
//             $('.veranda-content-hot-swiper').html(testhtml)
//             $(".veranda-content-hot-swiper img.lazy").lazyload({ placeholder: "/images/imageloading.png", effect: "fadeIn", container: $(".veranda-content-hot") });
//         },
//         error: function (req) {
//         }
//     })
// }
// 接口获取房间列表
verandaMessage.getPageMessage = function (type, page) {
    verandaMessage.roomPage = page || 1
    if (verandaMessage.pageLoading) {
        return false
    }
    verandaMessage.pageLoading = true
    let version = []
    if (verandaMessage.roomscreen['yuri']) {
        version.push(0)
    }
    if (verandaMessage.roomscreen['war']) {
        version.push(1)
    }
    if (verandaMessage.roomscreen['republic']) {
        version.push(3)
    }

    if (type == 'clear') {
        clearInterval(verandaMessage.autoReset)
        verandaMessage.autoReset = setInterval(() => {
            verandaMessage.getPageMessage('clear')
        }, 30000)
        verandaMessage.localRoomlist = []
        verandaMessage.showRoomlist = []
        $('#veranda-room-body-hover').html('')
        $('.veranda-roomlist-body').html("")
        $('.veranda-roomlist-body-loading').removeClass('hide')
        let getversion = version.length > 0 ? version.join(',') : ''
        if (getversion == '') {
            $('.veranda-roomlist-body-loading').addClass('hide')
        }
        verandaMessagegetpage(getversion)
        return false
    }

    let getversion = version.length > 0 ? version.join(',') : ''
    verandaMessagegetpage(getversion)
}
function verandaMessagegetpage(getversion) {
    $('.veranda-roomlist-body-error').addClass('hide')
    $('.veranda-roomlist-body').off('scroll')
    $.controlAjax({
        type: "get",
        url: '/api/lobby/game/rooms/',
        showerror: false,
        data: {
            page: verandaMessage.roomPage,
            limit: verandaMessage.pagelimit,
            version: getversion,
            pas: true,
            mode: verandaMessage.screenModes,
            label: verandaMessage.screenLabel,
            name: verandaMessage.Jsousuofun
        },
        success: function (res) {
            verandaMessage.pageLoading = false
            $('.veranda-roomlist-body-loading').addClass('hide')
            if (res.data) {
                for (var i = 0; i < res.data.list.length; i++) {
                    let listItem = res.data.list[i]
                    verandaMessage.dellRoomList(listItem)
                }
                verandaMessage.dellRoomShowList()
                if (res.data.list.length == verandaMessage.pagelimit) {
                    verandaMessagegetpagescroll()
                } else {
                    $('.veranda-roomlist-body').off('scroll')
                }
            }
        },
        error: function (req) {
            verandaMessage.pageLoading = false
            $('.veranda-roomlist-body').html("")
            $('.veranda-roomlist-body-loading').addClass('hide')
            $('.veranda-roomlist-body-error').removeClass('hide')
        }
    })
}
function verandaMessagegetpagescroll() {
    $('.veranda-roomlist-body').on('scroll', function () {
        var $this = $(this)
        var nScrollHight = $this[0].scrollHeight,
            nScrollTop = $this[0].scrollTop,
            nDivHight = $this.height();
        if (nScrollTop + nDivHight + 10 >= nScrollHight && !verandaMessage.pageLoading) {
            $('.veranda-roomlist-body').off('scroll')
            let pages = verandaMessage.roomPage + 1
            verandaMessage.getPageMessage('next', pages)
        }
    })
}
$(document).on('click', '.veranda-roomlist-body-error', function () {
    verandaMessage.getPageMessage('clear')
    $('.veranda-roomlist-body-error').addClass('hide')
})
// 处理外来数据进入本地数据
verandaMessage.dellRoomList = function (items) {
    var getitem = JSON.parse(JSON.stringify(items))
    if (getitem.roomStatus != 2 && getitem.roomStatus != 1 && getitem.roomStatus != 3) {

        let optionsMode = comOption.optionsMode
        for (var i = 0; i < optionsMode.length; i++) {
            let ModeItem = optionsMode[i]
            if (getitem.mode == ModeItem.code) {
                getitem.Jmode = ModeItem.name
            }
        }
        if (getitem.label && getitem.label != "" && getitem.label != '-') {
            let enlabel = getitem.label
            enlabel = enlabel.replace('Battle', translatesrting('对战'))
            enlabel = enlabel.replace('Recreation', translatesrting('娱乐'))
            enlabel = enlabel.replace('Task', translatesrting('任务'))
            enlabel = enlabel.replace('Defense', translatesrting('防守'))
            getitem.Jlabel = enlabel
        } else {
            getitem.Jlabel = ""
        }
        // getitem.imageUrl = getitem.imageUrl.split('.png')[0] + '_small.png'
        // 排序分
        let peplernum = getitem.maxPlayers - getitem.userNum
        if (getitem.roomType != 1 && getitem.roomType != 0) {
            comOption.roomTypeArray.map((roomTypeitem) => {
                if (roomTypeitem.code == getitem.roomType) {
                    peplernum = roomTypeitem.minMapPlayer - (getitem.userNum - getitem.spectatorNum)
                }
            })
        }
        let JgetNum = 0
        if (peplernum == 1) {
            JgetNum = JgetNum + 30
        } else if (peplernum == 2) {
            JgetNum = JgetNum + 20
        } else if (peplernum >= 3) {
            JgetNum = JgetNum + 10
        } else if (peplernum == 0) {
            JgetNum = 1
        }
        if (getitem.roomStatus == 1) {
            JgetNum = 0
        }
        getitem.JgetNum = JgetNum
        // getitem.JgetNum = getitem.activeTime
        let isinit = false
        for (var i = 0; i < verandaMessage.localRoomlist.length; i++) {
            let localItem = verandaMessage.localRoomlist[i]
            if (localItem.roomNo == getitem.roomNo) {
                isinit = true
                verandaMessage.localRoomlist[i] = getitem
            }
        }
        if (!isinit) {
            verandaMessage.localRoomlist.push(getitem)
        }
    } else if (getitem.roomStatus == 2 || getitem.roomStatus == 1 || getitem.roomStatus == 3) {
        // 在本地数据中清除
        for (var i = 0; i < verandaMessage.localRoomlist.length; i++) {
            let localItem = verandaMessage.localRoomlist[i]
            if (localItem.roomNo == getitem.roomNo) {
                verandaMessage.localRoomlist.splice(i, 1)
            }
        }
    }
}
// 处理本地数据进入展示数据
verandaMessage.dellRoomShowList = function () {
    let localList = JSON.parse(JSON.stringify(verandaMessage.localRoomlist))
    let showList = JSON.parse(JSON.stringify(verandaMessage.showRoomlist))
    let addRoom = []//新加入房间
    let oldRoom = []//已有房间
    let delRoom = []//离开的房间

    for (var i = 0; i < localList.length; i++) {
        let localItem = localList[i]
        let isAdd = true
        for (var z = 0; z < showList.length; z++) {
            let showItem = showList[z]
            if (localItem.roomNo == showItem.roomNo) {
                isAdd = false
                oldRoom.push(localItem)
            }
        }
        if (isAdd) {
            addRoom.push(localItem)
        }
    }
    if (addRoom.length > 0 && $('.veranda-roomlist-body').css('opacity') == 0) {
        $('.veranda-roomlist-body-loading').addClass('hide')
    }
    for (var z = 0; z < showList.length; z++) {
        let showItem = showList[z]
        let isout = true
        for (var i = 0; i < localList.length; i++) {
            let localItem = localList[i]
            if (localItem.roomNo == showItem.roomNo) {
                isout = false
            }
        }
        if (isout) {
            delRoom.push(showItem)
        }
    }
    if (addRoom.length > 0) {
        // 新加房间
        if (verandaMessage.type == 1) {
            let addItem = template("veranda-roomlist-body-script", { list: addRoom, roomTypeArray: comOption.roomTypeVerandaArray })
            $('.veranda-roomlist-body').append(addItem)
        } else if (verandaMessage.type == 2) {

        }
    }
    if (delRoom.length > 0) {
        // 消失房间
        if (verandaMessage.type == 1) {
            delRoom.map((item) => {
                let dellItem = $('.veranda-roomlist-body-item-' + item.roomNo)
                dellItem.remove()
            })
        }
    }
    if (oldRoom.length > 0) {
        // 修改房间
        oldRoom.map((newItem) => {
            showList.map((oldItem) => {
                if (newItem.roomNo == oldItem.roomNo) {
                    let changeItem = $('.veranda-roomlist-body-item-' + newItem.roomNo)
                    if (newItem.mapName != oldItem.mapName) {
                        changeItem.find('.changeMapName').text(newItem.mapName)
                    }
                    if (newItem.Jmode != oldItem.Jmode) {
                        changeItem.find('.changeMapJmode').text(newItem.Jmode)
                    }
                    if (newItem.label != oldItem.label) {
                        changeItem.find('.changeMapLabel').text(newItem.Jlabel)
                    }
                    if (newItem.userNum != oldItem.userNum) {
                        changeItem.find('.changeplayersNum').text(newItem.userNum)
                    }
                    if (newItem.maxPlayers != oldItem.maxPlayers) {
                        changeItem.find('.changeplayersMax').text(newItem.maxPlayers)
                    }
                    if (newItem.gameVersion != oldItem.gameVersion) {
                        changeItem.find('img[alt="version"]').attr('src', '/images/gamelogo/version' + newItem.gameVersion + '-34.png')
                    }
                    changeItem.attr('data-JgetNum', newItem.JgetNum)
                    let showismax = changeItem.find('.veranda-joinbtn').hasClass('hide')
                    if (showismax != (newItem.userNum >= newItem.maxPlayers)) {
                        if (newItem.userNum >= newItem.maxPlayers) {
                            changeItem.find('.ismax').removeClass('hide')
                            changeItem.find('.ismax').text(translatesrting('满员'))
                            changeItem.find('.veranda-joinbtn').addClass('hide')
                        } else if (newItem.roomStatus == 1) {
                            changeItem.find('.ismax').removeClass('hide')
                            changeItem.find('.ismax').text(translatesrting('游戏已锁定'))
                            changeItem.find('.veranda-joinbtn').addClass('hide')
                        } else {
                            changeItem.find('.ismax').addClass('hide')
                            changeItem.find('.veranda-joinbtn').removeClass('hide')
                        }
                    }
                }
            })
        })
    }
    verandaMessage.showRoomlist = localList
    // 筛选
    verandaMessage.hidearray()
    // 排序
    verandaMessage.numsort()
}
//房间排序
verandaMessage.numsort = function () {
    let domList = $('.veranda-roomlist-body').find('.veranda-roomlist-body-item').get()
    domList.sort(function (a, b) {
        let elone = parseInt($(a).attr('data-JgetNum'))
        let eltwo = parseInt($(b).attr('data-JgetNum'))
        if (elone > eltwo) return -1
        if (elone < eltwo) return 1
        return 0
    })
    $('.veranda-roomlist-body').html(domList)
}
// 房间筛选
verandaMessage.hidearray = function () {
    let hidearray = []
    // if (verandaMessage.screenModes) {
    //     let screenModes = verandaMessage.screenModes
    //     for (var i = 0; i < verandaMessage.showRoomlist.length; i++) {
    //         if (verandaMessage.showRoomlist[i].mode != screenModes) {
    //             hidearray.push($('.veranda-roomlist-body-item-' + verandaMessage.showRoomlist[i].roomNo))
    //         }
    //     }
    // }
    // if (verandaMessage.screenLabel) {
    //     let screenLabel = verandaMessage.screenLabel
    //     for (var i = 0; i < verandaMessage.showRoomlist.length; i++) {
    //         if (verandaMessage.showRoomlist[i].label != screenLabel) {
    //             hidearray.push($('.veranda-roomlist-body-item-' + verandaMessage.showRoomlist[i].roomNo))
    //         }
    //     }
    // }
    // if (verandaMessage.Jsousuofun != '') {
    //     var Jsousuofun = verandaMessage.Jsousuofun
    //     for (var i = 0; i < verandaMessage.showRoomlist.length; i++) {
    //         // console.log(verandaMessage.showRoomlist[i].mapName, Jsousuofun)
    //         if (verandaMessage.showRoomlist[i].mapName.indexOf(Jsousuofun) == -1 && verandaMessage.showRoomlist[i].roomName.indexOf(Jsousuofun) == -1) {
    //             hidearray.push($('.veranda-roomlist-body-item-' + verandaMessage.showRoomlist[i].roomNo))
    //         }
    //     }
    // }
    let prohibitroomscreen = []
    for (let p in verandaMessage.roomscreen) {
        if (!verandaMessage.roomscreen[p]) {
            verandaMessage.optionsGameVersion.map((itemv) => {
                if (itemv.en == p) {
                    prohibitroomscreen.push(itemv.code)
                }
            })
        }
    }
    for (var i = 0; i < verandaMessage.showRoomlist.length; i++) {
        if ($.inArray(verandaMessage.showRoomlist[i].gameVersion.toString(), prohibitroomscreen) != -1) {
            hidearray.push($('.veranda-roomlist-body-item-' + verandaMessage.showRoomlist[i].roomNo))
        }
    }
    $('.veranda-roomlist-body-item').removeClass('hide')
    $('.veranda-roomlist-body-item').addClass('veranda-roomlist-body-item-show')
    for (var i = 0; i < hidearray.length; i++) {
        hidearray[i].addClass('hide')
        hidearray[i].removeClass('veranda-roomlist-body-item-show')
    }
}
// 切换模式
$(document).on('click', '.veranda-content-middle-nav', function () {
    var $this = $(this)
    if ($this.hasClass('active')) {
        return false
    }
    verandaMessage.type = $this.attr('data-type')
    localStorage.setItem('verandaType', verandaMessage.type)
    if (verandaMessage.type == 1) {
        $('.veranda-screen').html(template("veranda-screen-script", { list: verandaMessage.roomscreen }))
        if (verandaMessage.refreshloadingfun) {
            $('.hall-room-refresh').addClass('hall-room-refresh-loading')
        }
    } else {
        $('.veranda-screen').html(template("veranda-screen-map-script", { maplabel: verandaMessage.maplabel }))
    }
    $this.siblings().removeClass('active')
    $this.addClass('active')
    if (verandaMessage.type == 1) {
        let optionsMode = comOption.optionsMode
        let labelArray = comOption.labelArray
        let cModel = null
        let cLabel = null
        optionsMode.map((item, i) => {
            if (item.code == verandaMessage.screenModes) {
                cModel = item.name
            }
        })
        labelArray.map((item, i) => {
            if (item.code == verandaMessage.screenLabel) {
                cLabel = item.name
            }
        })
        $('.veranda-content-bottom').html(template("veranda-bottom-script", {
            type: 'roomlist',
            comOptionMode: optionsMode,
            comOptionLabel: labelArray,
            cModel: cModel,
            cLabel: cLabel,
        }))
        $('[data-toggle="tooltip"]').tooltip()
        $('.veranda-header-gxmenu').gxmenu({
            height: "20px",
            top: "40px",
            clickHide: true//点击内部是否消失
        }, function (e) {

        })
        // let showRoomlist = JSON.parse(JSON.stringify(verandaMessage.showRoomlist))
        // let addItem = template("veranda-roomlist-body-script", { list: showRoomlist })
        // if (localStorage.getItem('isdetails')) {
        //     verandaCheckOpenWay('closeOpen')
        // } else {
        //     $('.veranda-roomlist-body').html(addItem)
        // }
        if (localStorage.getItem('isdetails')) {
            verandaCheckOpenWay('closeOpen', 'onlycss')
        } else {
            verandaMessage.getPageMessage('clear')
        }
        // 筛选
        verandaMessage.hidearray()
    } else if (verandaMessage.type == 2) {
        $('.veranda-content-bottom').html(template("veranda-bottom-script", { type: 'maplist' }))
        verandaMessage.getmapList()

    }
})
// 用户切换导航/页面刷新以及60秒自动刷新
// setInterval(() => {
//     if (verandaMessage.type == 2) {
//         verandaMessage.getmapList()
//     }
// }, 60000)
// 获取地图列表
verandaMessage.getmapList = function () {
    let labels = []
    verandaMessage.maplabel.map((item) => {
        if (item.value) {
            labels.push(item.code)
        }
    })
    labels = labels.join(',')
    $.star({
        type: 'GET',
        url: '/battlecenter/redwar/map/v1/popular/map',
        showerror: true,
        data: {
            limit: 60,
            labels: labels
        },
        success: async function (res) {
            verandaMessage.MapList = JSON.parse(JSON.stringify(res.data))
            let maplist = res.data
            let testhtmlz = ''
            maplist.map((items) => {
                items.name = items.name + '&&||' + items.enName
            })
            // --------图片数据处理
            let imgarray = new Map()
            for (var i = 0; i < maplist.length; i++) {
                imgarray.set(maplist[i].imageUrl + '?x-oss-process=image/resize,w_362', '')
            }
            let dellimgarray = await readerIndexedDBImgUrl(imgarray)
            for (list of maplist) {
                let itemimg = list.imageUrl + '?x-oss-process=image/resize,w_362'
                if (dellimgarray.has(itemimg)) {
                    if (dellimgarray.get(itemimg)) {
                        list.imageUrl = dellimgarray.get(itemimg)
                    } else {
                        list.imageUrl = itemimg
                    }
                }
            }
            // --------图片数据处理
            for (var i = 0; i < maplist.length; i++) {
                maplist[i].Jhot = formatMapHot(maplist[i].activityNum)
                testhtmlz += '<div class="col-xs-2">' + template("veranda-map-script", { list: maplist[i], lazy: 'js', showList: true, chooseMap: verandaMessage.fastChoosMap }) + '</div>'
            }
            $('.veranda-maplist .row').html(testhtmlz)
            $(".veranda-maplist .row img.lazy").lazyload({ placeholder: "/images/imageloading.png", effect: "fadeIn", container: $(".veranda-maplist-scroll") });
            $('.veranda-maplist-scroll').css('opacity', 1);
        },
        error: function (req) {
        }
    })
}
// 聊天
verandaMessage.chathallhtml = function (message) {
    if (!$('.veranda-content-chatContent-scroll').hasClass('hide') && !verandaMessage.mouseinchat) {//订阅判断
        let JVersion = ''
        verandaMessage.optionsGameVersion.map((item) => {
            if (item.code == message.version) {
                JVersion = item.name
            }
        })
        $('.veranda-content-chatContent-scroll').append(template('chathall-script', {
            data: message,
            JVersion: JVersion,
            roomTypeVerandaArray: comOption.roomTypeVerandaArray
        }))
        if ($('.veranda-content-chatContent-item').length > 100) {
            $('.veranda-content-chatContent-item')[0].remove()
        }
        if (!verandaMessage.mouseinchat) {
            $('.veranda-content-chatContent-scroll').scrollTop($('.veranda-content-chatContent-scroll').prop('scrollHeight'))
        }
        $('.veranda-content-chatContent-item-animate').removeClass('veranda-content-chatContent-item-animate')
    }
}
$(document).on('mouseenter', '.veranda-content-chatContent-scroll', function () {
    verandaMessage.mouseinchat = true
})
$(document).on('mouseleave', '.veranda-content-chatContent-scroll', function () {
    verandaMessage.mouseinchat = false
    $('.veranda-content-chatContent-scroll').scrollTop($('.veranda-content-chatContent-scroll').prop('scrollHeight'))
})
// 加入房间
$(document).on('click', '.J_veranda_join', function (e) {
    e.stopPropagation();
    var $this = $(this)
    let isneedloading = true
    if ($(this).attr('data-loading') == 'none') {
        isneedloading = false
    }
    if ($this.attr('data-haspaw')) {
        e.stopPropagation();
        verandaMessage.passJoinId = $(this).attr('data-id')
        $('#passwordJoinModal').modal('show');
        $('#passwordJoinModalNum').val("")
        return false
    }
    if (verandaMessage.joinloading) {
        showtoast({
            message: translatesrting('正在加入房间中')
        })
        return false
    }
    if (verandaMessage.fastLaoding) {
        showtoast({
            message: translatesrting('正在加入房间中')
        })
        return false
    }
    if (verandaMessage.MapfastLaoding) {
        showtoast({
            message: translatesrting('正在加入房间中')
        })
        return false
    }
    var url = $(this).attr('data-url') || '/Details/newindex'
    var id = $this.attr('data-id')
    var pwd = $this.attr('data-pwd') || ""
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    if (isneedloading) {
        $this.gxbtn('loading')
    }
    verandaMessage.joinloading = true
    // 双房主预防
    $.controlAjax({
        type: "get",
        url: '/api/lobby/room/join/',
        showerror: true,
        data: {
            window: 'createroom',
            url: url,
            width: 1280,
            height: 720,
            gameRoomNo: id,
            password: pwd
        },
        success: function (res) {
            verandaMessage.joinloading = false
            localStorage.removeItem('msgType10')
            localStorage.removeItem('msgType2')
            $('.veranda-joinbtn').gxbtn('reset', translatesrting('加入'))
        },
        error: function (req) {
            verandaMessage.joinloading = false
            $('.veranda-joinbtn').gxbtn('reset', translatesrting('加入'))
            if (req.JerrorCode && req.JerrorCode == "inroom") {
                if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
                    showhallGameRoom()
                } else {
                    if (!$('.hall-header-returnroom').hasClass('hide')) {
                        $('.hall-header-returnroom').tooltip('show')
                        setTimeout(function () {
                            $('.hall-header-returnroom').tooltip('hide')
                        }, 3000)
                    }
                }
            } else if (req.errorCode == '1101') {
                $('.veranda-roomlist-body-item-' + id).remove()
            }
        }
    })
})
// 点击列表进入房间
$(document).on('click', '.veranda-roomlist-body-item', function (e) {
    var $this = $(this);
    if ($this.find('.veranda-joinbtn').length > 0) {
        $this.find('.veranda-joinbtn').click();
    }
})
// 密码房
$(document).on('keyup', '#passwordJoinModalNum', function (event) {
    if (event.keyCode == 13) {
        $("#passwordJoinModalBtn").click()
    }
})
// 密码进入
$(document).on('click', '#passwordJoinModalBtn', function () {
    if ($('#passwordJoinModalNum').val() == "") {
        return false;
    }
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    $this.gxbtn('loading')
    $.controlAjax({
        type: "get",
        url: '/api/lobby/room/join/',
        showerror: true,
        data: {
            window: 'createroom',
            url: '/Details/newindex',
            width: 1280,
            height: 720,
            gameRoomNo: verandaMessage.passJoinId,
            password: $('#passwordJoinModalNum').val()
        },
        success: function (res) {
            $this.gxbtn('reset')
            $('#passwordJoinModal').modal('hide');
            localStorage.removeItem('msgType10')
            localStorage.removeItem('msgType2')
        },
        error: function (req) {
            $this.gxbtn('reset')
            if (req.JerrorCode && req.JerrorCode == "inroom") {
                if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
                    showhallGameRoom()
                } else {
                    if (!$('.hall-header-returnroom').hasClass('hide')) {
                        $('.hall-header-returnroom').tooltip('show')
                        setTimeout(function () {
                            $('.hall-header-returnroom').tooltip('hide')
                        }, 3000)
                    }
                }
            }
        }
    })
})
$(document).on('click', '.J_passwordJoinModal-input-add', function () {
    var $this = $(this)
    $this.toggleClass('show')
    if ($this.hasClass('show')) {
        $this.closest('.gx-pass-num-glass').find('input').attr('type', 'text')
    } else {
        $this.closest('.gx-pass-num-glass').find('input').attr('type', 'password')
    }
})

// 房间号搜索
function roomsousuo() {
    if (verandaMessage.joinloading) {
        return false
    }
    var loadingbtn = $('.veranda-content .room-screen-password-num .input-group-addon')
    if (loadingbtn.hasClass('gx-disabled')) {
        return false
    }
    loadingbtn.gxbtn('loading')
    trackingFunc('join_find')
    verandaMessage.joinloading = true
    let roomNo = $('.veranda-content .room-screen-password .suo-input .form-control').val()
    $.controlAjax({
        type: "get",
        url: '/api/lobby/game/room/',
        showerror: true,
        data: {
            'roomNo': roomNo
        },
        success: function (res) {
            loadingbtn.gxbtn('reset')
            verandaMessage.joinloading = false
            if (res.data.roomStatus == 1 || res.data.roomStatus == 3) {
                showtoast({
                    message: translatesrting('当前房间已开始游戏')
                })
            } else if (res.data.maxPlayers <= res.data.players.length) {
                showtoast({
                    message: translatesrting('当前房间已满')
                })
            } else {
                $('.veranda-content .room-screen-password .suo-input .form-control').val("")
                $('.veranda-content .room-screen-password .suo-input .form-control').trigger('input');
                if (res.data.isHavePas) {
                    verandaMessage.passJoinId = roomNo
                    $('#passwordJoinModal').modal('show');
                    $('#passwordJoinModalNum').val("")
                    $('#passwordJoinModal').one('shown.bs.modal', function () {
                        $('#passwordJoinModalNum').focus()
                    })
                } else {
                    $.controlAjax({
                        type: "get",
                        url: '/api/lobby/room/join/',
                        showerror: true,
                        data: {
                            window: 'createroom',
                            url: '/Details/newindex',
                            width: 1280,
                            height: 720,
                            gameRoomNo: roomNo,
                            password: ''
                        },
                        success: function (res) {
                            localStorage.removeItem('msgType10')
                            localStorage.removeItem('msgType2')
                        },
                        error: function (req) {
                            if (req.JerrorCode && req.JerrorCode == "inroom") {
                                if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
                                    showhallGameRoom()
                                } else {
                                    if (!$('.hall-header-returnroom').hasClass('hide')) {
                                        $('.hall-header-returnroom').tooltip('show')
                                        setTimeout(function () {
                                            $('.hall-header-returnroom').tooltip('hide')
                                        }, 3000)
                                    }
                                }
                            }
                        }
                    })
                }
            }
        },
        error: function (req) {
            loadingbtn.gxbtn('reset')
            verandaMessage.joinloading = false
        }
    })
    return false
}
$(document).on('input', '.veranda-content .room-screen-password .suo-input .form-control', function () {
    var $this = $(this)
    verandaMessage.Jsousuofun = $this.val().replace(/(^\s*)|(\s*$)/g, "");
    if (parseInt($this.val()) != 'NaN' && parseInt($this.val()) == $this.val() && $this.val().length == 6) {
        $this.closest('.room-screen-password').addClass('room-screen-password-num')
    } else {
        $this.closest('.room-screen-password').removeClass('room-screen-password-num')
    }
    if (verandaMessage.Jsousuofun != "") {
        $('.veranda-content .room-screen-password .input-group-after').css('display', 'inline-block')
    } else {
        $('.veranda-content .room-screen-password .input-group-after').css('display', 'none')
    }
    clearTimeout(verandaMessage.JsousuofunTime)
    verandaMessage.JsousuofunTime = setTimeout(function () {
        verandaMessage.getPageMessage('clear')
    }, 1000)
})
$(document).on('keyup', '.room-screen-password-num .form-control', function (event) {
    if (event.keyCode == 13) {
        roomsousuo()
    }
})
$(document).on('click', '.veranda-content .room-screen-password-num .input-group-addon', function (event) {
    roomsousuo()
})
$(document).on('click', '.veranda-content .room-screen-password .input-group-after', function (event) {
    $('.veranda-content .room-screen-password .suo-input .form-control').val("")
    $('.veranda-content .room-screen-password .suo-input .form-control').trigger('input');
})

// 点击创建游戏
$(document).on('click', '.J_veranda_create_btn', function () {
    player_sound_click()
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    if ($this.hasClass('disabled')) {
        return false
    }
    // if (getSaveMessage('noGameUrl')) {
    //     showtoast({
    //         message: translatesrting('请先设置游戏路径')
    //     })
    //     return false
    // }
    trackingFunc('create_modal')
    $this.gxbtn('loading')
    setTimeout(() => {
        $this.gxbtn('reset')
    }, 500)
    // ipcRenderer.send('Main', {
    //     msgType: "ShowModal",
    //     jsonInfo: {
    //         window: 'createroom',
    //         url: '/Modal/createRoom',
    //         width: 518,
    //         height: 394,
    //     }
    // });
    $('.translate-createRoom').html(template('translate-createRoom-script', {
        roomTypeArray: comOption.roomTypeArray
    }))

    $('.createRoom-gx-menu').gxmenu({
        height: '26px',
        top: "25px",
        clickHide: true
    }, function (e) {

    })
    $('#createRoomModal').modal('show')
    createMessageinit()
    getUserGold()
})
// 点击快速游戏
$(document).on('click', '.J_veranda_fast_btn', function () {
    player_sound_click()
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    if ($this.hasClass('disabled')) {
        return false
    }
    if (verandaMessage.fastLaoding) {
        return false
    }
    if (verandaMessage.MapfastLaoding) {
        return false
    }
    trackingFunc('join_fast')
    verandaMessage.fastLaoding = true// 列表正在快速加入

    $this.gxbtn('loading')
    verandaMessage.fastClose = setTimeout(function () {
        $.controlAjax({
            type: "get",
            url: '/api/lobby/quick/join/cancel/',
            showerror: false,
            success: function (res) {
                $('.J_veranda_fast_btn').gxbtn('reset')
                clearFastJoinLoading()
                showtoast({
                    message: translatesrting('未找到符合的房间,请稍后再试')
                })
            },
            error: function (req) {

            }
        })
    }, 20000)
    let version = []
    if (verandaMessage.roomscreen['yuri']) {
        version.push(0)
    }
    if (verandaMessage.roomscreen['war']) {
        version.push(1)
    }
    if (verandaMessage.roomscreen['republic']) {
        version.push(3)
    }
    $.controlAjax({
        type: "get",
        url: '/api/lobby/quick/join/',
        showerror: true,
        data: {
            window: 'createroom',
            url: '/Details/newindex',
            width: 1280,
            height: 720,
            version: version.length > 0 ? version.join(',') : '',
        },
        success: function (res) {

        },
        error: function (req) {

        }
    })
})
$(document).on('click', '.veranda-map .veranda-map-img', function () {
    var $this = $(this)
    $this.closest('.veranda-map').find('.J_veranda_list_fast_btn').click()
})

// 列表点击快速游戏
$(document).on('click', '.J_veranda_list_fast_btn', function () {
    player_sound_click()
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    if (verandaMessage.fastLaoding) {
        return false
    }
    if (verandaMessage.MapfastLaoding) {
        return false
    }
    verandaMessage.MapfastLaoding = true// 列表正在快速加入
    let mapSha1 = $this.attr('data-map')
    let topMapList = verandaMessage.topMapList
    let MapList = verandaMessage.MapList
    let chooseMap = null
    for (var i = 0; i < topMapList.length; i++) {
        if (topMapList[i].mapSha1 == mapSha1) {
            chooseMap = topMapList[i]
        }
    }
    for (var i = 0; i < MapList.length; i++) {
        if (MapList[i].mapSha1 == mapSha1) {
            chooseMap = MapList[i]
        }
    }
    verandaMessage.fastChoosMap = chooseMap
    $this.gxbtn('loading')
    verandaMessage.fastClose = setTimeout(function () {
        $.controlAjax({
            type: "get",
            url: '/api/lobby/quick/join/cancel/',
            showerror: false,
            success: function (res) {
                $('.J_veranda_fast_btn').gxbtn('reset')
                clearFastJoinLoading()
                showtoast({
                    message: translatesrting('未找到符合的房间,请稍后再试')
                })
            },
            error: function (req) {

            }
        })
    }, 20000)
    $this.closest('.veranda-map').addClass('active')
    $.controlAjax({
        type: "get",
        url: '/api/lobby/quick/join/',
        showerror: true,
        data: {
            window: 'createroom',
            url: '/Details/newindex',
            width: 1280,
            height: 720,
            mapSha1: mapSha1
        },
        success: function (res) {

        },
        error: function (req) {

        }
    })
})





var iscaninRoom = false
// 监听房间列表
ipcRenderer.on('WebIpc', (event, message) => {
    if (message.msgType == 3) {
        // 新的房间数据进入
        // var roomMessage = JSON.parse(message.jsonInfo)
        // verandaMessage.dellRoomList(roomMessage)
        // 房间列表数量渲染
    } else if (message.msgType == 33) {
        let chatMessage = JSON.parse(message.jsonInfo)
        if (!chatMessage.inviteStatus && chatMessage.userId == userInfo.userId) {
            return false
        }
        if (localStorage.getItem('Ra_BlackFriends')) {
            let init = false
            let blackUserList = JSON.parse(localStorage.getItem('Ra_BlackFriends'))
            blackUserList.map((item) => {
                if (item.userId == chatMessage.userId) {
                    init = true
                }
            })
            if (init) {
                return false
            }
        }
        verandaMessage.chathallhtml(chatMessage)

    } else if (message.msgType == 6) {
        if (message.jsonInfo == 1) {
            // verandaMessage.init()
        } else if (message.jsonInfo == 2) {
        } else if (message.jsonInfo == 3) {
            // 匹配信息
            matchGame(1)
        }
    } else if (message.msgType == 34) {
        clearTimeout(verandaMessage.fastClose)
        let fastJoin = JSON.parse(message.jsonInfo)
        localStorage.removeItem('msgType10')
        localStorage.removeItem('msgType2')
        if (fastJoin.status == 1) {
            $('.J_veranda_fast_btn').gxbtn('reset')
            clearFastJoinLoading()
        } else if (fastJoin.status == 2) {
            //快速加入的创建房间
            fastJoinCreateRoom()
        }

    } else if (message.msgType == 10 && iscaninRoom) {
        iscaninRoom = false
        // setTimeout(() => {
        //     ipcRenderer.send('Main', {
        //         msgType: "ShowModal",
        //         jsonInfo: {
        //             window: 'createroom',
        //             url: '/Details/index',
        //             width: 1280,
        //             height: 720,
        //         }
        //     });
        // }, 10)
    } else if (message.msgType == 20 && verandaMessage.fastChoosMap.mapSha1) {
        let fastJoin = JSON.parse(message.jsonInfo)
        let obj = $('.veranda-map .gx-map-mask[data-map=' + fastJoin.sign + ']')
        obj.css('display', "inline-block")
        var getnum = parseInt(fastJoin.nowPercentage)
        var shownum = parseInt(obj.find('.gx-map-mask-circle').attr('data-br'))
        if (getnum != shownum) {
            obj.find('.gx-map-mask-progress-br').css('width', getnum + '%')
            obj.find('.gx-map-mask-progress-br').attr('data-br', getnum)
            if (getnum >= 100) {
                setTimeout(function () {
                    obj.css('display', "none")
                }, 1000)
            }
        }
        if (verandaMessage.fastChoosMap.mapSha1 == fastJoin.sign && fastJoin.nowPercentage == "100.0") {
            clearTimeout(verandaMessage.downMapset)
            $('#fastJoinDownMapModal').modal('hide');
            $('#fastJoinDownMapModal').remove()
            let gameRoomName = userInfo.username + translatesrting('的房间')
            iscaninRoom = true
            let labels = "-"
            if (verandaMessage.fastChoosMap.labels != null) {
                if (verandaMessage.fastChoosMap.labels != "") {
                    labels = verandaMessage.fastChoosMap.labels[0]
                }
            }
            localStorage.removeItem('msgType10')
            localStorage.removeItem('msgType2')
            ipcRenderer.send('Main', {
                msgType: "CreateRoom",
                jsonInfo: {
                    password: "",
                    gameRoomName: gameRoomName,
                    tunnelIndex: 0,
                    mapSha1: verandaMessage.fastChoosMap.mapSha1,
                    mapName: verandaMessage.fastChoosMap.name,
                    maxPlayers: verandaMessage.fastChoosMap.maxPlayers,
                    model: "Battle",
                    imageUrl: verandaMessage.fastChoosMap.imageUrl,
                    gameVersion: verandaMessage.fastChoosMap.gameVersion,
                    label: labels
                }
            });
            clearFastJoinLoading()
        }
    } else if (message.msgType == 7) {
        saveMessage('ClientBGM', 'false')
        let openroomtype = JSON.parse(message.jsonInfo)
        if (openroomtype.name == 'main') {
            if (!localStorage.getItem('isdetails') && hallMessage.indexTo == 0) {
                noMessagefun.isopen = true
                verandaMessage.getPageMessage('clear')
                ipcRenderer.send('Main', {
                    msgType: "ActiveStatus",
                    jsonInfo: {
                        status: true
                    }
                });
                $('.verandaHideMessage').remove()
                $('.veranda-roomlist-body').removeClass('hide')
                $('.veranda-roomlist-body-close').addClass('hide')
            }
        } else {
            if (!openroomtype.custom) {
                showhallGameRoom('openmatch')
            } else {
                showhallGameRoom()
            }
        }
    } else if (message.msgType == 42) {
        let joinDetails = JSON.parse(JSON.parse(message.jsonInfo))
        outinvite(joinDetails)
        trackingFunc('url_open')
    }
});
function outinvite(joinDetails) {
    $.controlAjax({
        type: "get",
        url: '/api/lobby/room/user/status/',
        data: {
        },
        success: function (res) {
            if (res.data.inRoom) {
                $.controlAjax({
                    type: "get",
                    url: '/Details/LeaveGameRoom/',
                    data: {},
                    showerror: true,
                    success: function (res) {
                        hallMessage.closeRoom('room')
                        $.controlAjax({
                            type: "get",
                            url: '/api/lobby/room/join/',
                            showerror: true,
                            data: {
                                window: 'createroom',
                                url: '/Details/newindex',
                                width: 1280,
                                height: 720,
                                gameRoomNo: joinDetails.gameRoomNo,
                                password: joinDetails.password
                            },
                            success: function (res) {
                                localStorage.removeItem('msgType10')
                                localStorage.removeItem('msgType2')
                            },
                            error: function (req) {
                                if (req.JerrorCode && req.JerrorCode == "inroom") {
                                    if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
                                        showhallGameRoom()
                                    } else {
                                        if (!$('.hall-header-returnroom').hasClass('hide')) {
                                            $('.hall-header-returnroom').tooltip('show')
                                            setTimeout(function () {
                                                $('.hall-header-returnroom').tooltip('hide')
                                            }, 3000)
                                        }
                                    }
                                }
                            }
                        })
                    },
                    error: function (req) {

                    }
                })
            } else {
                $.controlAjax({
                    type: "get",
                    url: '/api/lobby/room/join/',
                    showerror: true,
                    data: {
                        window: 'createroom',
                        url: '/Details/newindex',
                        width: 1280,
                        height: 720,
                        gameRoomNo: joinDetails.gameRoomNo,
                        password: joinDetails.password
                    },
                    success: function (res) {
                        localStorage.removeItem('msgType10')
                        localStorage.removeItem('msgType2')
                    },
                    error: function (req) {
                        if (req.JerrorCode && req.JerrorCode == "inroom") {
                            if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
                                showhallGameRoom()
                            } else {
                                if (!$('.hall-header-returnroom').hasClass('hide')) {
                                    $('.hall-header-returnroom').tooltip('show')
                                    setTimeout(function () {
                                        $('.hall-header-returnroom').tooltip('hide')
                                    }, 3000)
                                }
                            }
                        }
                    }
                })
            }
        },
        error: function (req) {

        }
    })
    // $.controlAjax({
    //     type: "get",
    //     url: '/Details/LeaveGameRoom/',
    //     data: {},
    //     showerror: true,
    //     success: function (res) {
    //         hallMessage.closeRoom('room')
    //         $.controlAjax({
    //             type: "get",
    //             url: '/api/lobby/room/join/',
    //             showerror: true,
    //             data: {
    //                 window: 'createroom',
    //                 url: '/Details/newindex',
    //                 width: 1280,
    //                 height: 720,
    //                 gameRoomNo: joinDetails.roomNo,
    //                 password: joinDetails.password
    //             },
    //             success: function (res) {
    //                 localStorage.removeItem('msgType10')
    //                 localStorage.removeItem('msgType2')
    //             },
    //             error: function (req) {
    //                 if (req.errorMsg == "您已加入游戏房间" || req.errorMsg == "您已经创建房间") {
    //                     if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
    //                         showhallGameRoom()
    //                     } else {
    //                         if (!$('.hall-header-returnroom').hasClass('hide')) {
    //                             $('.hall-header-returnroom').tooltip('show')
    //                             setTimeout(function () {
    //                                 $('.hall-header-returnroom').tooltip('hide')
    //                             }, 3000)
    //                         }
    //                     }
    //                 }
    //             }
    //         })
    //     },
    //     error: function (req) {

    //     }
    // })

}
// 一定时间清除数据
setInterval(() => {
    verandaMessage.getPageMessage('clear')
}, 600000)

function fastJoinCreateRoom() {
    var lastgameVersion = ""
    $.controlAjax({
        type: "get",
        url: '/Modal/GetLastUseMap',
        showerror: true,
        success: function (res) {
            lastgameVersion = res.data.gameVersion
            verandaMessage.fastChoosMap.gameVersion = lastgameVersion
            let url = '/api/lobby/game/map/'
            let data = {
                mapSha1: verandaMessage.fastChoosMap.mapSha1,//
                model: "Battle",//
                // gameVersion: lastgameVersion
            }
            $.controlAjax({
                type: "get",
                url: url,
                showerror: true,
                data: data,
                success: function (res) {
                    let istDownload = res.data.isDownload
                    verandaMessage.fastChoosMap.maxPlayers = res.data.maxPlayerNum
                    if (istDownload) {
                        let gameRoomName = userInfo.username + translatesrting('的房间')
                        iscaninRoom = true
                        let labels = "-"
                        if (verandaMessage.fastChoosMap.labels != null) {
                            if (verandaMessage.fastChoosMap.labels != "") {
                                labels = verandaMessage.fastChoosMap.labels[0]
                            }
                        }
                        ipcRenderer.send('Main', {
                            msgType: "CreateRoom",
                            jsonInfo: {
                                password: "",
                                gameRoomName: gameRoomName,
                                tunnelIndex: 0,
                                mapSha1: verandaMessage.fastChoosMap.mapSha1,
                                mapName: verandaMessage.fastChoosMap.name,
                                maxPlayers: res.data.maxPlayerNum,
                                model: "Battle",
                                imageUrl: verandaMessage.fastChoosMap.imageUrl,
                                gameVersion: verandaMessage.fastChoosMap.gameVersion,
                                label: labels
                            }
                        });
                        clearFastJoinLoading()
                    } else {

                        let obj = $('.veranda-map .gx-map-mask[data-map=' + verandaMessage.fastChoosMap.mapSha1 + ']')
                        obj.css('display', "inline-block")
                        var getnum = 0
                        obj.find('.gx-map-mask-progress-br').css('width', getnum + '%')
                        obj.find('.gx-map-mask-progress-br').attr('data-br', getnum)
                        setTimeout(() => {
                            var getnum = 30
                            obj.find('.gx-map-mask-progress-br').css('width', getnum + '%')
                            obj.find('.gx-map-mask-progress-br').attr('data-br', getnum)
                        }, 1)
                        // gxmodal({
                        //     id: 'fastJoinDownMapModal',
                        //     closeCoinHide: true,
                        //     centent: '<div class="mt20 f22">' + translatesrting('地图下载中') + '</div>',
                        //     buttons: [
                        //         {
                        //             text: translatesrting('取消'),
                        //             class: 'btn-middle btn-style-ash',
                        //             callback: function () {
                        //                 clearTimeout(verandaMessage.downMapset)
                        //                 $('#fastJoinDownMapModal').modal('hide');
                        //                 $('#fastJoinDownMapModal').remove()
                        //                 clearFastJoinLoading()
                        //                 showtoast({
                        //                     message: translatesrting('地图下载取消')
                        //                 })
                        //             }
                        //         }
                        //     ]
                        // })
                        // 1分钟容错
                        // verandaMessage.downMapset = setTimeout(() => {
                        //     $('#fastJoinDownMapModal').modal('hide');
                        //     $('#fastJoinDownMapModal').remove()
                        //     clearFastJoinLoading()
                        //     showtoast({
                        //         message: translatesrting('地图下载失败')
                        //     })
                        // }, 10000)

                        return false
                    }
                },
                error: function (req) {

                }
            })

        },
        error: function (req) {

        }
    })
}
function clearFastJoinLoading() {
    verandaMessage.MapfastLaoding = false// 列表正在快速加入
    verandaMessage.fastLaoding = false// 正在快速加入  
    $('.J_veranda_list_fast_btn').gxbtn('reset', translatesrting('快速加入'))
    $('.veranda-map').removeClass('active')
    verandaMessage.fastChoosMap = []
}
// $(document).on('mouseenter', '.veranda-content-bottom .veranda-roomlist-body-item', function () {
//     var $this = $(this);
//     // let cloneHTML = $this.closest('tr').clone().attr('id', $this.closest('tr').clone().attr('id') + "-clone")
//     let cloneHTML = $this.clone()
//     $('#veranda-room-body-hover').html(cloneHTML)
//     let tableheight = $this.closest('.veranda-roomlist-body').offset().top
//     let btnheight = $this.offset().top
//     $('#veranda-room-body-hover').css('top', btnheight - tableheight + 40 + "px")
//     if ((btnheight - tableheight + 40) > 500) {
//         $('#veranda-room-body-hover').html('')
//     }
//     if ((btnheight - tableheight + 40) < 39) {
//         $('#veranda-room-body-hover').html('')
//     }
// })
// $(document).on('mouseleave', '#veranda-room-body-hover', function () {
//     $('#veranda-room-body-hover').html('')
// })
// $(document).on('mousewheel DOMMouseScroll', '#veranda-room-body-hover', function (event) {
//     var delta = event.originalEvent.wheelDelta;
//     // if (delta > 0) {
//     //     //鼠标向上滚，上图标显示，下图标隐藏
//     // } else {
//     //     //鼠标向下滚，下图标显示，上图标隐藏
//     // }
//     $('.veranda-roomlist-body').scrollTop($('.veranda-roomlist-body').prop('scrollTop') - delta)
// })


verandaMessage.init()

$(document).on('click', '.hall-room-refresh', function () {
    var $this = $(this);
    if ($this.hasClass('hall-room-refresh-loading')) {
        return false
    }
    $this.addClass('hall-room-refresh-loading')
    verandaMessage.getPageMessage('clear')
    $('.hall-room-refresh-num').text(verandaMessage.refreshloading + 's')
    verandaMessage.refreshloadingfun = setInterval(() => {
        verandaMessage.refreshloading = verandaMessage.refreshloading - 1
        if (verandaMessage.refreshloading <= 0) {
            clearInterval(verandaMessage.refreshloadingfun)
            verandaMessage.refreshloadingfun = null
            verandaMessage.refreshloading = 5
            $('.hall-room-refresh').removeClass('hall-room-refresh-loading')
            return false
        }
        $('.hall-room-refresh-num').text(verandaMessage.refreshloading + 's')
    }, 1000)
})
