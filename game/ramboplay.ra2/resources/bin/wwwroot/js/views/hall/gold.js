var goldHistory = {
    init: null,//初始化
    pageFun: null,//分页
    pageFunScroll: null,
    getRult: null,
    pageNum: 1,
    pageSize: 15,
    rule: null
}

goldHistory.init = function () {
    $('#UserGoldModal').find('.modal-dialog').html(template('UserGoldModal-script'))
    $('#UserGoldModal .gx-modal-header-nav-item').on('click', function () {
        var $this = $(this)
        $('#UserGoldModal .gx-modal-header-nav-item').removeClass('gx-modal-header-nav-active')
        $this.addClass('gx-modal-header-nav-active')
        if ($this.attr('data-index') == 1) {
            $('#UserGoldModal .UserGoldModal-index-one').removeClass('hide')
            $('#UserGoldModal .UserGoldModal-index-two').addClass('hide')

        } else {
            $('#UserGoldModal .UserGoldModal-index-one').addClass('hide')
            $('#UserGoldModal .UserGoldModal-index-two').removeClass('hide')
            if (!goldHistory.rule) {
                goldHistory.getRult()
            }
        }
    })
    goldHistory.pageNum = 1
    goldHistory.pageFun()
    $('#UserGoldModal').modal('show')
}
goldHistory.pageFun = function () {
    $.star({
        type: 'GET',
        url: '/community-user/account/gold/v1/history',
        showerror: false,
        data: {
            pageNum: goldHistory.pageNum,
            pageSize: goldHistory.pageSize,
        },
        success: function (res) {
            if (goldHistory.pageNum == 1) {
                $('#UserGoldModal').find('.UserGoldModal-div-scroll').html(template('UserGoldModal-history-script', { list: res.data.list }))
            } else {
                $('#UserGoldModal').find('.UserGoldModal-div-scroll').append(template('UserGoldModal-history-script', { list: res.data.list }))
            }
            $('.UserGoldModal-div-scroll').off('scroll')
            if (res.data.list.length >= goldHistory.pageSize) {
                goldHistory.pageFunScroll()
            }
        },
        error: function (req) {
        }
    });
}
goldHistory.pageFunScroll = function () {
    $('.UserGoldModal-div-scroll').on('scroll', function () {
        var $this = $(this)
        var nScrollHight = $this[0].scrollHeight,
            nScrollTop = $this[0].scrollTop,
            nDivHight = $this.height();
        if (nScrollTop + nDivHight + 10 >= nScrollHight && !careerMessage.pageLoading) {
            $('.UserGoldModal-div-scroll').off('scroll')
            goldHistory.pageNum = goldHistory.pageNum + 1
            goldHistory.pageFun()
        }
    })
}
goldHistory.getRult = function () {
    commConfig().then(function (res) {
        if (res.success) {
            goldHistory.rule = res.data.taskRuleDesc
            $('.UserGoldModal-div-rule').html(goldHistory.rule)
            $('#UserGoldModal').on('hidden.bs.modal', function () {
                goldHistory.rule = null
            })
        }
    })
}
$(document).on('click', '.J_hall-gold-showModal', function () {
    goldHistory.init()
    getUserGold()
})