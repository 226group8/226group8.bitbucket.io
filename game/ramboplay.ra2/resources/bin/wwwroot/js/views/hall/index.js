
// 翻译
$('.translate-hallpage-modal').html(template('translate-hallpage-modal-script'))
$('.translate-hallpage-header').html(template('translate-hallpage-header-script'))
let LBWlanguage = localStorage.getItem('LBWlanguage') || 'zh'
if (LBWlanguage == 'en') {
    $('.hall-content').addClass('hall-content-en')
}
var hallMessage = {
    indexTo: '4',
    overTimeDownMap: null,
    closeRoom: null,
    generatefun: null,//赛事用
}

//结束初始动画
function StartWork() {
    const { ipcRenderer } = require("electron");
    ipcRenderer.send('start-work', 'Hello');
}
if (getSaveMessage('Version') != null) {
    let test = ''
    if (getSaveMessage('test')) {
        test = '-' + 'test'
    }
    if (getSaveMessage('pre')) {
        test = '-' + 'pre'
    }
    $('.version-content').text(translatesrting('版本号') + ':' + getSaveMessage('Version') + test)
}
StartWork()
// 菜单栏
// var navSwiper = new Swiper('.hall-header-left-swiper', {
//     slidesPerView: 3,
//     spaceBetween: 2,
//     autoplay: false,
// })
var contentSwiper = new Swiper('.hall-content-swiper', {
    slidesPerView: 1,
    spaceBetween: 0,
    autoplay: false,
    // effect: 'fade',
    allowTouchMove: false,
    on: {
        slideChangeTransitionStart: function () {
            hallContentSwiper(this.activeIndex)
        }
    }
})
$(function () {
    if (localStorage.getItem('isinitfirst')) {

    } else {
        localStorage.setItem('isinitfirst', 1)
        newHelpinit()
        // 空掉 为了预加载
        $.controlAjax({
            type: "get",
            url: '/api/lobby/game/rooms/',
            showerror: false,
            data: {
                page: 1,
                limit: 200,
                version: 0,
                pas: true
            },
            success: function (res) {

            },
            error: function (req) {

            }
        })
    }
    if (getQueryString('innav')) {
        $('.hall-header-left-swiper-item').each(function () {
            var $this = $(this)
            if ($this.attr('data-index') == getQueryString('innav')) {
                if ($this.attr('data-index') == 6) {
                    Refreshintosix()
                }
                $this.click()
                return false
            }
        })
    }
    // getActvieList()
})
// 菜单栏
$(document).on('click', '.hall-header-left-swiper-item', function () {
    $('.inmodal-close').click()
    var $this = $(this)
    if ($this.hasClass('active')) {
        return false
    }
    var indexto = $this.attr('data-index')
    hallContentSwiper(indexto)
})
function hallContentSwiper(indexto) {
    $('#informationtextModal').modal('hide')
    $('#settlement').modal('hide')
    hallMessage.indexTo = indexto
    let swiperindex = indexto
    if (indexto == 4) {
        swiperindex = 0
    } else if (indexto == 0) {
        swiperindex = 1
    } else if (indexto == 1) {
        swiperindex = 2
    } else if (indexto == 2) {
        swiperindex = 2
    } else if (indexto == 3) {
        swiperindex = 3
    } else if (indexto == 5) {
        swiperindex = 4
    }
    $('.hall-content-swiper').animate({ opacity: .6 }, 70, function () {
        contentSwiper.slideTo(swiperindex, 0, false)
        $('.hall-content-swiper').animate({ opacity: 1 }, 100)
    })
    $('.hall-header-left-swiper .hall-header-left-swiper-item').each(function () {
        var $this = $(this)
        let showindex = indexto
        // if (showindex == 1) {
        //     showindex = 2
        // }
        if ($this.attr('data-index') == showindex) {
            $this.addClass('active')
        } else {
            $this.removeClass('active')
        }
    })
    // 个人生涯
    if (indexto == '3') {
        careerMessage.resMessage = null
        getUserCareer('init')
        // getCareerApm()
        getCareerList(1)

        if (getSaveMessage('careerGameType2')) {
            $('.career-content-center div').removeClass('career-content-div-active')
            $('.career-content-center div').each(function () {
                if (getSaveMessage('careerGameType2') == $(this).attr('data-type')) {
                    $(this).addClass('career-content-div-active')
                }
            })
        }
    }
    // 天梯
    if (indexto == '1' || indexto == '2') {
        rankMessage.getTypefun()
        trackingFunc('rank_show')
    }
    // 大厅
    if (indexto == '0') {
        // verandaMessage.init()
        if (!localStorage.getItem('isdetails') && !noMessagefun.isopen) {
            verandaCheckOpenWay('inOpen')
        }
    } else {
        if (noMessagefun.isopen) {
            verandaCheckOpenWay('closeOpen')
        }
    }
    if (indexto != '6') {
        $('#competition>iframe').attr('src', 'about:blank')
    }
    // 首页
    if (indexto == '4') {
        informationMessage.init()
        // verandaMessage.init()
    }
    if ($('#detailsSwiper>iframe').attr('src') != 'about:blank' && indexto != 5) {
        if ($('.hall-header-returnroom').hasClass('hide')) {
            $('.hall-header-returnroom').removeClass('hide')
            $('.veranda-content-right .mt15 .gx-animate-btn').addClass('hide')
            $('.veranda-content-right .mt15 .J_veranda_return_room').removeClass('hide')
            $('.hall-header-returnroom').tooltip('show')
            setTimeout(function () {
                $('.hall-header-returnroom').tooltip('hide')
            }, 3000)
        }
    } else {
        $('.veranda-content-right .mt15 .gx-animate-btn').removeClass('hide')
        $('.veranda-content-right .mt15 .J_veranda_return_room').addClass('hide')
        $('.hall-header-returnroom').addClass('hide')
        $('.hall-header-returnroom').tooltip('hide')
    }
    if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
        $('.hall-header-refresh').removeClass('hide')
    } else {
        $('.hall-header-refresh').addClass('hide')
    }
    if ($('.hall-header-returnroom').hasClass('hide') && $('.hall-header-refresh').hasClass('hide')) {
        $('.hall-header-notice-border').addClass('hide')
    } else {
        $('.hall-header-notice-border').removeClass('hide')
    }
}
$.controlAjax({
    type: "get",
    url: '/api/lobby/room/user/status/',
    data: {
    },
    success: function (res) {
        // firstoutroom = false
        if (res.data.inRoom) {
            localStorage.setItem('isdetails', true);
            if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
                if (res.data.type == 1 || res.data.type == 5) {
                    // 自定义
                    showhallGameRoom()
                } else {
                    // showhallGameRoom('openmatch')
                }
            } else {
                hallContentSwiper(5)
            }
        } else {
            wsUserStatus(1)
            localStorage.removeItem('isdetails')
        }
    },
    error: function (req) {
        // firstoutroom = false
        // hallContentSwiper(0)
        // showhallGameRoom('close')
    }
})
// $('.hall-header-left-swiper .hall-header-left-swiper-item').eq(2).click()
// 用户栏
$('#Hall-user').html(template('Hall-user-script', userInfo))

//初始化大厅
function HeartbeatEnterLobby() {
    setTimeout(() => {
        ipcRenderer.send('Main', {
            msgType: "EnterTheLobby"
        });
    }, 5000)
}

HeartbeatEnterLobby()
// 设置
$('.hall-header-setting-select').gxmenu({
    height: "36px",
    top: "64px",
    maxheight: '254px',
    clickHide: true
}, function () {

})
// 设置按钮
$(document).on('click', '.J_Setting', function () {
    showWindowModal('settingmodal', '/Modal/setting')
})
// 检测更新
if (getSaveMessage('msgType21') && getSaveMessage('msgType21').Status == 1) {
    $('.hall-header-setting-select .red-bot').removeClass('hide')
}
// 唤起检测
// $(document).on('click', '.J_showCheckGameBefor', function () {
//     var gameBefor = getSaveMessage('gameBefor')
//     if (gameBefor) {
//         $('#GamebeforModal .modal-dialog #GamebeforModal-content').html(template('GamebeforModal-script', {
//             type: 'init',
//             Completeness: gameBefor.Completeness,
//             GameTestStatus: gameBefor.GameTestStatus,
//         }))
//     } else {
//         $('#GamebeforModal .modal-dialog #GamebeforModal-content').html(template('GamebeforModal-script', { type: 'init', isfirst: true }))
//     }
//     $('#GamebeforModal').modal('show')
//     trackingFunc('check_modal')
// })
//游戏检测
function gameCheckGame() {
    var waeraw = getSaveMessage('msgType19');
    if (waeraw && waeraw.code == 302) {
        showWindowModal('settingmodal', '/Modal/setting?noclose=true')
        localStorage.removeItem('msgType19')
    } else {
        var joinmessage = getSaveMessage('OutInvite', true);
        setTimeout(() => {
            $.controlAjax({
                type: "get",
                url: '/api/lobby/room/join/',
                showerror: true,
                data: {
                    window: 'createroom',
                    url: '/Details/newindex',
                    width: 1280,
                    height: 720,
                    gameRoomNo: joinmessage.gameRoomNo,
                    password: joinmessage.password
                },
                success: function (res) {
                    localStorage.removeItem('msgType10')
                    localStorage.removeItem('msgType2')
                },
                error: function (req) {
                    if (req.JerrorCode && req.JerrorCode == "inroom") {
                        if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
                            showhallGameRoom()
                        } else {
                            if (!$('.hall-header-returnroom').hasClass('hide')) {
                                $('.hall-header-returnroom').tooltip('show')
                                setTimeout(function () {
                                    $('.hall-header-returnroom').tooltip('hide')
                                }, 3000)
                            }
                        }
                    }
                }
            })
        }, 2000)
    }
    saveMessage('errorCode302', 302)
    //路径检测
    // $.ajax({
    //     type: "get",
    //     url: '/Hall/GetReadyInfo/',
    //     success: function (res) {
    //         if (ture) {
    //             spop({
    //                 template: '未设置游戏路径',
    //                 autoclose: 5000,
    //                 style: 'error'
    //             });
    //             ipcRenderer.send('Main', {
    //                 msgType: "ShowModal",
    //                 jsonInfo: {
    //                     window: 'settingmodal',
    //                     url: '/Modal/setting',
    //                     width: 619,
    //                     height: 413,
    //                 }
    //             });
    //         }
    //     },
    //     error: function (req) {

    //     }
    // })
    // 本电脑第一次游戏测试
    // var gameBefor = getSaveMessage('gameBefor')
    // if (gameBefor && !gameBefor.complete) {
    //     $('#GamebeforModal .modal-dialog').html(template('GamebeforModal-script', {
    //         type: 'init',
    //         isfirst: true,
    //         Completeness: gameBefor.Completeness,
    //         GameTestStatus: gameBefor.GameTestStatus
    //     }))
    //     $('#GamebeforModal').modal('show')
    // } else if (!gameBefor) {
    //     $('#GamebeforModal .modal-dialog').html(template('GamebeforModal-script', { type: 'init', isfirst: true }))
    //     $('#GamebeforModal').modal('show')
    // }
}

// 游戏测试
// $(document).on('click', '.J_gameBefor_check', function () {
//     trackingFunc('check_clickgame');
//     var $this = $(this)
//     if ($this.hasClass('gx-disabled')) {
//         return false
//     }
//     $('.GamebeforModal-title').text(translatesrting('游戏测试！'))
//     $this.gxbtn('loading')
//     $.controlAjax({
//         type: "get",
//         url: '/api/site/game/test/',
//         showerror: true,
//         data: {
//             toMain: true
//         },
//         success: function (res) {
//             // $this.gxbtn('reset')
//         },
//         error: function (req) {

//         }
//     })
// })
// $(document).on('click', '.J_gameBefor_check_ares', function () {
//     trackingFunc('check_clickares');
//     var $this = $(this)
//     if ($this.hasClass('gx-checkloading')) {
//         return false
//     }
//     $this.addClass('.gx-checkloading')
//     $this.gxbtn('loading')
//     $('.GamebeforModal-title').text(translatesrting('Ares检测'))
//     $.controlAjax({
//         type: "get",
//         url: '/api/site/game/test/',
//         showerror: true,
//         data: {
//             testType: 1,
//             toMain: true
//         },
//         success: function (res) {
//             // $this.gxbtn('reset')
//         },
//         error: function (req) {

//         }
//     })
// })

// 关闭确定
$(document).on('click', '.J_sureClose', function () {

    let backstageout = getSaveMessage('backstageout') || 0

    if (backstageout == 0 || backstageout == 1) {
        saveMessage('ClientBGM', 'true')
        ipcRenderer.send('Main', {
            msgType: "WindowSize",
            jsonInfo: {
                window: 'main',
                size: 'hide'
            }
        });
    }
    if (backstageout == 0) {
        let myDate = new Date;
        let myDatedate = myDate.getDate()
        if (getSaveMessage('windowMinNotice') && getSaveMessage('windowMinNotice').day == myDatedate) {
            return false
        }
        $.controlAjax({
            type: "POST",
            url: '/api/lobby/system/notify',
            data: {
                title: '',
                body: translatesrting('已最小化到系统托盘'),
            },
            success: function (res) {
                saveMessage('windowMinNotice', { day: myDatedate })
            },
            error: function (req) {

            }
        })
    }
    if (backstageout == 2) {
        gxmodal({
            title: translatesrting('退出确认'),
            centent: '<div class="mt25">' + translatesrting('您确定要退出兰博玩对战平台吗？') + '</div>',
            id: 'sureclosemodal',
            buttons: [
                {
                    text: translatesrting('确定'),
                    class: 'btn-middle btn-middle-blue',
                    callback: function () {
                        $('.J_main_close').click()
                        setTimeout(() => {
                            window.close()
                        }, 500)
                    }
                },
                {
                    text: translatesrting('取消'),
                    class: 'btn-middle btn-style-ash',
                }
            ]
        })
    }
})
// 公告
var noticeMessage = {
    list: [],
    noticeSwiper: null,
    gettips: null,
    counttime: null,
    warningfun: null
}
getNotice()
function getNotice() {
    $.star({
        type: 'GET',
        url: '/battlecenter/notice/v1/maintain',
        showerror: false,
        data: {},
        success: function (res) {

            noticeMessage.gettips(res)
            // if(res.data.maintainVO.content){
            //     if(!newHelpMessage.commonHelping){
            //         noticeMessage.gettips(res)
            //     }else{
            //         setInterval(()=>{
            //             if(){

            //             }
            //         },1000)
            //     }
            // }
        },
        error: function (req) {

        }
    });
}
noticeMessage.gettips = function (res) {
    clearInterval(noticeMessage.counttime)
    if (res.data.maintainVO.content) {
        let type = ''
        if (res.data.maintainVO.type == 0) {
            type = 'serious'
        } else if (res.data.maintainVO.type == 1) {
            type = 'tips'
        } else {
            type = 'warning'
        }
        $('.hall-header-notice-content-text').html(res.data.maintainVO.content)
        $('.hall-header-notice-content-close').removeClass('hide')
        if (type == 'tips') {
            $('.hall-header-notice').removeClass('warning')
            $('.hall-header-notice>img').addClass('hide')
            $('.hall-header-notice-tips').removeClass('hide')

            let myDate = new Date;
            let myDatedate = myDate.getDate()
            if (newHelpMessage.commonHelping) {
                $('.hall-header-notice-content-close').addClass('hide')
                return false
            }
            if (getSaveMessage('allnotice')
                && getSaveMessage('allnotice').id == res.data.maintainVO.id
                && getSaveMessage('allnotice').day == myDatedate
            ) {
                $('.hall-header-notice-content-close').addClass('hide')
                return false
            } else {
                $('.hall-header-notice-content').removeClass('hide')
                $('.hall-header-notice-content').css({ opacity: 0 })
                $('.hall-header-notice-content').animate({ opacity: 1 }, 100, 'linear', function () {
                    $('.hall-header-notice-content').css({ opacity: 1 })
                })
                let myDate = new Date;
                let myDatedate = myDate.getDate()
                saveMessage('allnotice', { id: res.data.maintainVO.id, day: myDatedate })
            }
        }
        if (type == 'warning') {
            if (newHelpMessage.commonHelping) {
                noticeMessage.counttime = setInterval(function () {
                    if (!newHelpMessage.commonHelping) {
                        noticeMessage.warningfun()
                        clearInterval(noticeMessage.counttime)
                    }
                }, 500)
            } else {
                noticeMessage.warningfun()
            }
        }
        if (type == 'serious') {
            if (newHelpMessage.commonHelping) {
                noticeMessage.counttime = setInterval(function () {
                    if (!newHelpMessage.commonHelping) {
                        if (getSaveMessage('isNoticeWhiteList') == 1) {
                            // 白
                            gxmodal({
                                title: translatesrting('提示'),
                                closeCoinHide: false,
                                id: 'alertNoticeOut',
                                centent: '<div class="mt30" style="padding: 0px 22px;">' + translatesrting('与 RamboPlay 平台断开链接，原因：') + res.data.reason + '<div>',
                                buttons: [
                                    {
                                        text: translatesrting('确定'),
                                        class: 'btn-middle btn-middle-blue',
                                        callback: function () {

                                        }
                                    }
                                ]
                            })
                        } else {
                            gxmodal({
                                title: translatesrting('提示'),
                                closeCoinHide: true,
                                id: 'alertNoticeOut',
                                centent: '<div class="mt30" style="padding: 0px 22px;">' + translatesrting('与 RamboPlay 平台断开链接，原因：') + res.data.reason + '<div>',
                                buttons: [
                                    {
                                        text: translatesrting('确定'),
                                        class: 'btn-middle btn-middle-blue',
                                        callback: function () {
                                            window.location.href = '/Login/index?code=101'
                                        }
                                    }
                                ]
                            })
                        }
                        clearInterval(noticeMessage.counttime)
                    }
                }, 500)
            } else {
                if (getSaveMessage('isNoticeWhiteList') == 1) {
                    // 白
                    gxmodal({
                        title: translatesrting('提示'),
                        closeCoinHide: false,
                        id: 'alertNoticeOut',
                        centent: '<div class="mt30" style="padding: 0px 22px;">' + translatesrting('与 RamboPlay 平台断开链接，原因：') + res.data.reason + '<div>',
                        buttons: [
                            {
                                text: translatesrting('确定'),
                                class: 'btn-middle btn-middle-blue',
                                callback: function () {

                                }
                            }
                        ]
                    })
                } else {
                    gxmodal({
                        title: translatesrting('提示'),
                        closeCoinHide: true,
                        id: 'alertNoticeOut',
                        centent: '<div class="mt30" style="padding: 0px 22px;">' + translatesrting('与 RamboPlay 平台断开链接，原因：') + res.data.reason + '<div>',
                        buttons: [
                            {
                                text: translatesrting('确定'),
                                class: 'btn-middle btn-middle-blue',
                                callback: function () {
                                    window.location.href = '/Login/index?code=101'
                                }
                            }
                        ]
                    })
                }
            }
        } else {
            if ($('#alertNoticeOut').legend > 0) {
                $('#alertNoticeOut').modal('hide')
            }
        }
    } else {
        $('.hall-header-notice>img').addClass('hide')
        $('.hall-header-notice-none').removeClass('hide')
        $('.hall-header-notice-content').addClass('hide')
    }
}
noticeMessage.warningfun = function () {
    $('.hall-header-notice').addClass('warning')
    $('.hall-header-notice').removeClass('tips')
    $('.hall-header-notice>img').addClass('hide')
    $('.hall-header-notice-warning').removeClass('hide')

    $('.hall-header-notice-content').removeClass('hide')
    $('.hall-header-notice-content').css({ opacity: 0 })
    $('.hall-header-notice-content').animate({ opacity: 1 }, 100, 'linear', function () {
        $('.hall-header-notice-content').css({ opacity: 1 })
    })
}
$(document).on('click', '.hall-header-notice-content-close', function () {
    var $this = $(this)
    $this.addClass('hide')
    $this.closest('.hall-header-notice').find('.hall-header-notice-content').addClass('hide')
})
$(document).on('mouseenter', '.hall-header-notice>img', function () {
    var $this = $(this)
    if ($this.closest('.hall-header-notice').find('.hall-header-notice-content-close').hasClass('hide')) {
        $this.closest('.hall-header-notice').find('.hall-header-notice-content').removeClass('hide')
        $this.closest('.hall-header-notice').find('.hall-header-notice-content').css({ opacity: 0 })
        $this.closest('.hall-header-notice').find('.hall-header-notice-content').animate({ opacity: 1 }, 100, 'linear', function () {
            $this.closest('.hall-header-notice').find('.hall-header-notice-content').css({ opacity: 1 })
        })
    }
})
$(document).on('mouseleave', '.hall-header-notice>img', function () {
    var $this = $(this)
    if ($this.closest('.hall-header-notice').find('.hall-header-notice-content-close').hasClass('hide')) {
        $this.closest('.hall-header-notice').find('.hall-header-notice-content').animate({ opacity: 0 }, 100, 'linear', function () {
            $this.closest('.hall-header-notice').find('.hall-header-notice-content').addClass('hide')
        })
    }
})
// var noticeMessage = {
//     list: [],
//     noticeSwiper: null
// }
// // 公告
// getNotice()
// setInterval(() => {
//     getNotice()
// }, 300000)
// function getNotice() {
//     $.star({
//         type: 'GET',
//         url: '/battlecenter/notice/v1/list',
//         showerror: false,
//         data: {
//             size: 5,
//             type: 0
//         },
//         success: function (res) {
//             noticeMessage.list = res.data
//             if (noticeMessage.list.length > 0) {
//                 $('.hall-notice').removeClass('hide')
//                 $('.hall-notice .swiper-wrapper').html(template('notice-item', { list: noticeMessage.list }))
//                 if (noticeMessage.noticeSwiper) {
//                     noticeMessage.noticeSwiper.destroy()
//                 }
//                 if (noticeMessage.list.length > 1) {
//                     if (noticeMessage.noticeSwiper) {
//                         noticeMessage.noticeSwiper.destroy()
//                     }
//                     noticeMessage.noticeSwiper = new Swiper('.hall-notice', {
//                         autoplay: {
//                             delay: 5000,
//                             stopOnLastSlide: false,
//                             disableOnInteraction: true,
//                         },
//                         direction: 'vertical',
//                         allowTouchMove: true,
//                         loop: true,
//                     })
//                 }
//             }
//         },
//         error: function (req) {

//         }
//     });
// }
// $(document).on('click', '.hall-notice-item', function () {
//     var $this = $(this)
//     noticefun($this.attr('data-id'))
//     $('#noticeModal').modal('show')
// })
// 公告切换
$(document).on('click', '.noticeModal-foot-item-prev', function () {
    var $this = $(this)
    noticefun($this.attr('data-id'))
})
$(document).on('click', '.noticeModal-foot-item-next', function () {
    var $this = $(this)
    noticefun($this.attr('data-id'))
})

function noticefun(id) {
    var indexitem = ""
    var nextItem = ""
    var prevItem = ""
    noticeMessage.list.map((item, index) => {
        if (item.id == id) {
            indexitem = item
            if (noticeMessage.list[index - 1]) {
                prevItem = noticeMessage.list[index - 1]
            }
            if (noticeMessage.list[index + 1]) {
                nextItem = noticeMessage.list[index + 1]
            }
        }
    })
    $('#noticeModal .gx-modal-content').html(template('noticeModal-script', { item: indexitem, nextItem: nextItem, prevItem: prevItem }))

    indexitem.context = indexitem.context.replace('a href=', 'a class="notice-a" href=')
    $('#noticeModal .gx-modal-content .noticeModal-content').html(indexitem.context)
}
$(document).on('click', '.notice-a', function (e) {
    e.preventDefault()
    var $this = $(this)
    var url = $this.attr('href')
    OpenWebUrl(url)
})


var hearSetTimeout = null

//心跳
ipcRenderer.on('Heartbeat', (event, message) => {
    ipcRenderer.send('Heartbeat', 1);
    clearTimeout(hearSetTimeout)
    $('#Heartbeat').modal('hide')
    hearSetTimeout = setTimeout(() => {
        gxmodal({
            id: 'Heartbeat',
            title: translatesrting('提示'),
            centent: '<div class="mt30" style="padding: 0px 22px;">' + translatesrting('当前连接已断开，请关闭平台后启动游戏客户端') + '<div>',
            buttons: [
                {
                    text: translatesrting('确定'),
                    class: 'btn-middle btn-middle-blue',
                    callback: function () {
                        window.close()
                    }
                },
                {
                    text: translatesrting('取消'),
                    class: 'btn-middle btn-style-ash',
                }
            ]
        })
    }, 30000)
})

// //活动header icon
function getActvieListBtn() {
    $.star({
        type: 'GET',
        url: '/activity/location/query',
        showerror: false,
        data: {
            location: 1
        },
        success: function (res) {
            if (res.data.length > 0) {
                $('.MapAuthorModal_active_show').removeClass('hide')
                $('.MapAuthorModal_active_show').attr('data-url', res.data[0].url)
            }
        },
        error: function (req) {

        }
    });
}
getActvieListBtn()
// function showActiveModal() {
//     if (activeList.list.length <= 0) {
//         return false
//     }
//     if (getQueryString('innav')) {
//         return false
//     }
//     $('#activeModal .gx-modal').html(template('activeModal-script', { message: activeList.list[0], ischecked: activeList.ischecked }))
//     $('#activeModal').modal('show')
// }
// // 关闭活动窗口
// $(document).on('click', '#activeModal .activeModal-close', function () {
//     if (true) {
//         let getList = getSaveMessage('activeList') ? getSaveMessage('activeList').list.split(',') : [];
//         let userId = getSaveMessage('activeList') ? getSaveMessage('activeList').userId : "";
//         if (userInfo.userId != userId) {
//             getList = []
//         }
//         getList.push(activeList.list[0].id)
//         if (getList.length > 10) {
//             getList.splice(0, 1)
//         }
//         saveMessage('activeList', { userId: userInfo.userId, list: getList.join(',') })
//     }
//     activeList.list.splice(0, 1)
//     $('#activeModal').modal('hide')
// })

// 登录参数
$(document).on('click', '#activeModal .token-a', function () {
    if (true) {
        let getList = getSaveMessage('activeList') ? getSaveMessage('activeList').list.split(',') : [];
        let userId = getSaveMessage('activeList') ? getSaveMessage('activeList').userId : "";
        if (userInfo.userId != userId) {
            getList = []
        }
        getList.push(activeList.list[0].id)
        saveMessage('activeList', { userId: userInfo.userId, list: getList.join(',') })
    }
    activeList.list.splice(0, 1)
    $('#activeModal').modal('hide')
})
$(document).on('hidden.bs.modal', '#activeModal', function () {
    setTimeout(() => {
        showActiveModal()
    }, 300)
})
$(document).on('change', '#activeCook', function () {
    activeList.ischecked = $('#activeCook').is(":checked")
})
$(document).on('click', '.token-a', function () {
    var $this = $(this)
    if ($this.attr('data-url')) {
        $.star({
            type: 'POST',
            url: '/community-user/authorize/generate/code',
            showerror: false,
            success: function (res) {
                let tourl = $this.attr('data-url')
                if (tourl.indexOf('?') != -1) {
                    OpenWebUrl(tourl + '&token=' + res.data.authCode)
                } else {
                    OpenWebUrl(tourl + '?token=' + res.data.authCode)
                }
            },
            error: function (req) {
                spop({
                    template: req.errorMsg,
                    autoclose: 5000,
                    style: 'error'
                });
            }
        });
    }
})

var reloadfun = null
var noMessagefun = {
    fun: null,
    isopen: false,
    loadingswitch: false
}
// 30分钟 平台刷新
$(document).on('mousemove', function () {
    clearTimeout(reloadfun)
    reloadfun = setTimeout(function () {
        if (hallMessage && hallMessage.indexTo) {
            window.location.href = "/Hall/index?innav=" + hallMessage.indexTo
        } else {
            window.location.reload()
        }
        // }, 10000)
    }, 1800000)
})
// 5分钟 大厅断链
$(document).on('mousemove', '.veranda-content,#createRoomModal', function () {
    clearTimeout(noMessagefun.fun)
    noMessagefun.fun = setTimeout(function () {
        clearInterval(verandaMessage.autoReset)
        noMessagefun.isopen = false
        noMessagefun.loadingswitch = true
        ipcRenderer.send('Main', {
            msgType: "ActiveStatus",
            jsonInfo: {
                status: false
            }
        });
    }, 300000)
    if (!noMessagefun.isopen && noMessagefun.loadingswitch) {
        verandaMessage.getPageMessage('clear')
        noMessagefun.isopen = true
        noMessagefun.loadingswitch = false
        ipcRenderer.send('Main', {
            msgType: "ActiveStatus",
            jsonInfo: {
                status: true
            }
        });
    }
})
var closeOpenkey = true
setInterval(function () {
    // 房间判断
    if (localStorage.getItem('isdetails') && closeOpenkey) {
        closeOpenkey = false
        verandaCheckOpenWay('closeOpen')
    }
    if (localStorage.getItem('isdetails')) {

    } else {
        closeOpenkey = true
    }
    // 赛事显示
    // if (new Date().getHours() >= 19 && new Date().getHours() <= 22) {
    //     if ($('.hall-header-match-bot').hasClass('hide')) {
    //         $('.hall-header-match-bot').removeClass('hide')
    //     }
    // } else {
    //     if (!$('.hall-header-match-bot').hasClass('hide')) {
    //         $('.hall-header-match-bot').addClass('hide')
    //     }
    // }
    // 设置跳转
    if (localStorage.getItem('gameSeting')) {
        let localgameSeting = getSaveMessage('gameSeting')
        if (localgameSeting.fileCheck) {
            newHelpStart('fileCheck')
        } else if (localgameSeting.customer) {
            qimoChatClick()
        }
        localStorage.removeItem('gameSeting')
    }
    // 语言设置
    if (localStorage.getItem('SetLBWlanguage')) {
        hallLanguage()
    }
}, 1000)
function hallLanguage() {
    localStorage.removeItem('SetLBWlanguage')
    if (hallMessage && hallMessage.indexTo) {
        window.location.href = "/Hall/index?innav=" + hallMessage.indexTo
    } else {
        window.location.reload()
    }
}
// ipcRenderer.send('Main', {
//     msgType: "ActiveStatus",
//     jsonInfo: {
//         status: true
//     }
// });

// 客服参数
var qimoClientId = { userId: userInfo.userId, nickName: userInfo.username }
$(document).on('click', '.J_showqimoClient', function () {
    qimoChatClick()
})
getUserGold()
// 获取用户金币数
function getUserGold() {
    let Dom = $('.userGoldNum')
    let num = Dom.attr('data-num') || 0
    $.star({
        type: 'GET',
        url: '/community-user/account/v1/query',
        showerror: false,
        data: {},
        success: function (res) {
            if (num == 0 || res.data.gold == num) {
                $('.userGoldNum').text(res.data.gold)
                $('.userGoldNum').attr('data-num', res.data.gold)
            } else {
                $('.userGoldNum').attr('data-num', res.data.gold)
                let oldnum = num * 1
                let newnum = res.data.gold
                new CountUp('userGoldNum', oldnum, newnum, 0, 1, {
                    useEasing: true, //效果                       
                    separator: '' //数字分隔符                
                }).start();
            }
        },
        error: function (req) {
        }
    });

}
$(document).on('click', '.UserGoldModal-btn', function () {
    $('#UserGoldModal .gx-modal-header').find('.header-close').click()
})
// 比赛结束循环获取金币数量
var checkGold = null
setInterval(function () {
    if (localStorage.getItem('changeGold')) {
        localStorage.removeItem('changeGold')
        if (checkGold) {
            getUserGold()
            clearTimeout(checkGold)
        }
        checkGold = setTimeout(function () {
            getUserGold();
            checkGold = null
        }, 60000)
    }
}, 10000)


// 下载地图
function downmatchImg() {
    if ($('.Match-map-list .Match-map-item-inload').length > 0) {
        $('#J_Match_start').addClass('disabled')
        $('#J_Match_start').text(translatesrting('地图下载中'))
        $('.Match-map-item-inload').each(function () {
            var $this = $(this)
            var getnum = parseInt($this.attr('data-load'))
            var shownum = parseInt($this.find('.gx-map-mask-circle').attr('data-br'))
            if (getnum != shownum) {
                $this.find('.gx-map-mask-progress-br').css('width', getnum + '%')
                $this.find('.gx-map-mask-progress-br').attr('data-br', getnum)
                if (getnum >= 100) {
                    setTimeout(function () {
                        $this.removeClass('Match-map-item-inload')
                    }, 300)
                }
            }
        })
        if ($('.Match-map-list .Match-map-item-inload').length > 0) {
            $('#J_Match_start').addClass('disabled')
            $('#J_Match_start').text(translatesrting('地图下载中'))
        } else {
            if ($('#J_Match_start').attr('data-isOpen') == 'true') {
                $('#J_Match_start').removeClass('disabled')
                $('#J_Match_start').text(translatesrting('寻找比赛'))
            } else {
                $('#J_Match_start').text(translatesrting('寻找比赛'))
            }
        }
    } else {
        if ($('#J_Match_start').attr('data-isOpen') == 'true') {
            $('#J_Match_start').removeClass('disabled')
            $('#J_Match_start').text(translatesrting('寻找比赛'))
        } else {
            $('#J_Match_start').text(translatesrting('寻找比赛'))
        }
    }
}
function downmatchImgfun(obj, num) {
    var probabilityChart = echarts.init(obj.find('.gx-map-mask-progress')[0]);
    obj.find('.gx-map-mask-circle').text(num + '%')
    var probabilityEchartsOption = {
        title: {
            show: true,
            text: '',
            x: 'center',
            y: 'center',
            textStyle: {
                fontSize: '15',
                color: 'white',
                fontWeight: 'normal'
            }
        },
        grid: {
            top: '0%',
            bottom: '0%',
        },
        tooltip: {
            show: false
        },
        legend: {
            show: false
        },
        series:
        {
            name: '',
            type: 'pie',
            radius: ['80%', '100%'],
            avoidLabelOverlap: true,
            hoverAnimation: false,
            itemStyle: {
                borderRadius: 10
            },
            label: {
                normal: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    show: false
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            color: [
                new echarts.graphic.LinearGradient(1, 0, 0, 0, [{
                    offset: 0,
                    color: 'rgba(94, 250, 237, 1)'
                }, {
                    offset: 1,
                    color: 'rgba(63, 121, 238, 1)'
                }]),
                'rgba(63, 121, 238, 0)'
            ],
            data: [
                { value: num, name: '' },
                { value: 100 - num, name: '' }
            ]
        }
    }
    probabilityChart.setOption(probabilityEchartsOption)
    if (parseInt(num) >= 100) {
        setTimeout(() => {
            probabilityChart.dispose()
            obj.removeClass('Match-map-item-inload')
            if ($('.Match-map-list .Match-map-item-inload').length > 0) {
                $('#J_Match_start').addClass('disabled')
                $('#J_Match_start').text(translatesrting('地图下载中'))
            } else {
                if ($('#J_Match_start').attr('data-isOpen') == 'true') {
                    $('#J_Match_start').removeClass('disabled')
                    $('#J_Match_start').text(translatesrting('寻找比赛'))
                } else {
                    $('#J_Match_start').text(translatesrting('寻找比赛'))
                }
            }
            if ($('.rank-map-list .Match-map-item-inload').length > 0) {
                $('#J_rank_start').addClass('disabled')
                $('#J_rank_start').text(translatesrting('地图下载中'))
            } else {
                if ($('#J_rank_start').attr('data-isOpen') == 'true') {
                    $('#J_rank_start').removeClass('disabled')
                    $('#J_rank_start').text(translatesrting('寻找比赛'))
                } else {
                    $('#J_rank_start').text(translatesrting('天梯暂未开放'))
                }
            }
        }, 300)
    }
}
function playMusic(obj) {
    var audioEle = obj;
    audioEle.load();
    if (audioEle.paused) { /*如果已经暂停*/
        audioEle.play();   //播放
    } else {
        audioEle.pause();  //暂停
    }
}
// 重新下载地图
$(document).on('click', '.rank-left .Match-map-item-retry', function () {
    rankMessage.getTypefun()

    return false
})
// 重新下载地图
$(document).on('click', '.rank-left .Match-map-item-retry', function () {
    $('.J_Match_chooseGame.active').click()
    return false
})
$(document).on('shown.bs.modal', '#settlement', function () {
    $('.hall-header-left-swiper-item').one('click', function () {
        $('#settlement').modal('hide');
    })
})
$(document).on('shown.bs.modal', '#rankRuleModal', function () {
    $('.hall-header-left-swiper-item').one('click', function () {
        $('#rankRuleModal').modal('hide');
    })
})
$(document).on('shown.bs.modal', '#rankResultModal', function () {
    $('.hall-header-left-swiper-item').one('click', function () {
        $('#rankResultModal').modal('hide');
    })
})

$(document).on('shown.bs.modal', '.inhall', function () {
    var $this = $(this)
    $('.hall-header-return').addClass('active')
    $('.hall-header-return').one('click', function () {
        $this.modal('hide')
    })
})
$(document).on('hidden.bs.modal', '.inhall', function () {
    $('.hall-header-return').removeClass('active')
})



// 进入大厅声音逻辑
volumeFn()

// 循环监听bgm播放
let temp_clientVolume = ''
setInterval(() => {
    if (JSON.stringify(temp_clientVolume) != JSON.stringify(getSaveMessage('ClientVolume'))) {
        volumeFn()
        temp_clientVolume = getSaveMessage('ClientVolume')
    }
    let player_bgm = document.getElementById('Audio_BGM')
    let bgm_pause = getSaveMessage('ClientBGM')
    if (bgm_pause == 'false') {
        player_bgm.play()
    } else if (bgm_pause == 'true') {
        player_bgm.pause()
    }
    // 菜单功能
    if (localStorage.getItem('JmenuOpenIndex') && localStorage.getItem('JmenuOpenIndex') == 1) {
        $('.hall-header-left-swiper-item').each(function () {
            var $this = $(this)
            if ($this.attr('data-index') == 4) {
                $this.click()
            }
        })
        localStorage.removeItem('JmenuOpenIndex')
    }
    // 菜单功能
    if (localStorage.getItem('JmenuChangeUser') && localStorage.getItem('JmenuChangeUser') == 1) {
        localStorage.removeItem('JmenuChangeUser')
        if (localStorage.getItem('isdetails')) {
            showtoast({
                message: translatesrting('房间中无法切换账号')
            })
        } else {
            location.href = '/Login/index?code=change'
        }
    }

    if (localStorage.getItem('JmenuSettingOpen') && localStorage.getItem('JmenuSettingOpen') == 1) {
        localStorage.removeItem('JmenuSettingOpen')
        showWindowModal('settingmodal', '/Modal/setting')
    }
    if (localStorage.getItem('Jmenuclose') && localStorage.getItem('Jmenuclose') == 1) {
        gxmodal({
            title: translatesrting('退出确认'),
            centent: '<div class="mt25">' + translatesrting('您确定要退出兰博玩对战平台吗？') + '</div>',
            id: 'sureclosemodal',
            buttons: [
                {
                    text: translatesrting('确定'),
                    class: 'btn-middle btn-middle-blue',
                    callback: function () {
                        $('.J_main_close').click()
                        setTimeout(() => {
                            window.close()
                        }, 500)
                    }
                },
                {
                    text: translatesrting('取消'),
                    class: 'btn-middle btn-style-ash',
                }
            ]
        })
        localStorage.removeItem('Jmenuclose')
    }
}, 500);

hallMessage.closeRoom = function (type) {
    showhallGameRoom('close')
    if (type == 'match') {
        hallContentSwiper(1)
    } else {
        hallContentSwiper(0)
    }
}
// ----------------------隐私------------------------
// var UserPrivateMessage = {
//     battle_switch_user_level: null,
//     battle_switch_user_authenticate: null,
// }
// $(document).on('click', '.J_UserPrivate', function () {
//     $('.UserPrivateModal-content').html(template('UserPrivateModal-content-script'))
//     $('#UserPrivateModal').modal('show')
//     $.star({
//         type: 'GET',
//         url: '/community-user/user/config/v1/list',
//         showerror: true,
//         data: {
//             source: 1
//         },
//         success: function (res) {
//             res.data.map((item) => {
//                 if (item.name == 'battle_switch_user_level') {
//                     UserPrivateMessage.battle_switch_user_level = item.value
//                     if (item.value == 'Y') {
//                         $('#UserPrivateModallevel').prop('checked', 'checked')
//                     } else {
//                         $('#UserPrivateModallevel').prop('checked', false)
//                     }
//                 }
//                 if (item.name == 'battle_switch_user_authenticate') {
//                     UserPrivateMessage.battle_switch_user_authenticate = item.value
//                     if (item.value == 'Y') {
//                         $('#UserPrivateModallabel').prop('checked', 'checked')
//                     } else {
//                         $('#UserPrivateModallabel').prop('checked', false)
//                     }
//                 }

//             })
//             $('#UserPrivateModal .UserPrivateModal-content').css('opacity', 1)
//         },
//         error: function (req) {
//             $('#UserPrivateModal').modal('hide')
//         }
//     });
// })

// $(document).on('hidden.bs.modal', '#UserPrivateModal', function (e) {
//     let battle_switch_user_level = $(this).find('#UserPrivateModallevel').is(':checked') ? 'Y' : 'N'
//     let battle_switch_user_authenticate = $(this).find('#UserPrivateModallabel').is(':checked') ? 'Y' : 'N'
//     if (UserPrivateMessage.battle_switch_user_level != battle_switch_user_level) {
//         $.star({
//             type: 'POST',
//             url: '/community-user/user/config/v1/set',
//             showerror: true,
//             data: {
//                 name: 'battle_switch_user_level',
//                 value: battle_switch_user_level,
//                 source: 1,
//             },
//             success: function (res) {

//             },
//             error: function (req) {

//             }
//         });
//     }
//     if (UserPrivateMessage.battle_switch_user_authenticate != battle_switch_user_authenticate) {
//         $.star({
//             type: 'POST',
//             url: '/community-user/user/config/v1/set',
//             showerror: true,
//             data: {
//                 name: 'battle_switch_user_authenticate',
//                 value: battle_switch_user_authenticate,
//                 source: 1,
//             },
//             success: function (res) {

//             },
//             error: function (req) {

//             }
//         });
//         let customField = customFieldFun('encryption', {
//             avatarFrame: userInfo.avatarFrame,
//             authenticateType: userInfo.authenticateType,
//             authenticateContent: userInfo.authenticateContent,
//             isAuthenticate: $(this).find('#UserPrivateModallabel').is(':checked')
//         })
//         ipcRenderer.send('Main', {
//             msgType: "ModfiyUserInfo",
//             jsonInfo: {
//                 NickName: userInfo.username,
//                 Avatar: userInfo.avatar,
//                 customField: customField
//             }
//         });
//     }
// })
// ----------------------隐私------------------------
$(document).on('mouseleave', 'iframe', function () {
    $('.hall-header').css('-webkit-app-region', 'no-drag')
    setTimeout(() => {
        $('.hall-header').css('-webkit-app-region', 'drag')
    }, 1000)
})
// 开始游戏前弹窗
var startModalMessage = {
    startModalbar: 0,
    startModalbarfun: null
}
function startModal(type) {
    if (type == 'close') {
        $('#startModal').modal('hide')
        $('#startModal').one('hide.bs.modal', function () {
            $('#startModal').remove()
        })
        return false
    }
    if (startModalMessage.startModalbar != 0) {
        return false
    }
    let timeNum = 4200
    if (type == 'fast') {
        timeNum = 2000
    }
    let getHTML = '<div class="modal" id="startModal" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="reportModal"data-backdrop="static"><div class="modal-dialog gx-modal-smell-dialog" role="document"><div class="startModal-content"><img class="startModal-content-en" src="/images/modal/start/en.png" alt=""><div><img class="startModal-content-logo" src="/images/modal/start/logo.png" alt=""></div><div class="startModal-content-progress"><div class="startModal-content-bar"></div></div><div class="startModal-content-text">' + translatesrting('反作弊启动中…') + '</div></div></div></div>'
    $('body').append(getHTML)
    $('#startModal').modal('show')
    startModalMessage.startModalbarfun = setInterval(() => {
        if (type == 'fast') {
            startModalMessage.startModalbar = startModalMessage.startModalbar + Math.random() * 20
        } else {
            startModalMessage.startModalbar = startModalMessage.startModalbar + Math.random() * 12
        }
        if (startModalMessage.startModalbar >= 100) {
            startModalMessage.startModalbar = 100
            clearInterval(startModalMessage.startModalbarfun)
            setTimeout(function () {
                $('.startModal-content-text').text(translatesrting('反作弊已启动'))
            }, 300)
            setTimeout(function () {
                $('#startModal').modal('hide')
                $('#startModal').one('hidden.bs.modal', function () {
                    $('#startModal').remove()
                })
            }, 800)
        }

        // let givemun = parseInt($('#startModal .startModal-content-bar').css('width')) / parseInt($('#startModal .startModal-content-progress').css('width')) * 100
        // if (startModalbar > givemun) {
        //     $('#startModal .startModal-content-bar').css('width', startModalbar + '%')
        // }
        $('#startModal .startModal-content-bar').css('width', startModalMessage.startModalbar + '%')
    }, 300)
    setTimeout(function () {
        startModalMessage.startModalbar = 100
    }, timeNum)
}
$(document).on('hidden.bs.modal', '#startModal', function () {
    $('#startModal .startModal-content-bar').css('width', 0 + '%')
    startModalMessage.startModalbar = 0
})
// 反作弊
var AntiMessages = {
    pageNum: 1,
    pageSize: 10,
    type: 0,
    AntiModal: null,
    AntiModalPage: null,
    pageFunScroll: null,
    AnticlipboardL: null
}
AntiMessages.AntiModal = function () {
    $('#AntiCheatingModal').modal('show')
    $.star({
        type: 'GET',
        url: '/battlecenter/redwar/match/v1/report/data',
        showerror: false,
        data: {},
        success: function (res) {
            let date = new Date(res.data.systemTime * 1000)
            let datatexty = date.getFullYear()
            let datatextm = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)
            let datatextd = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
            datatexty = datatexty.toString().split('')
            datatextm = datatextm.toString().split('')
            datatextd = datatextd.toString().split('')

            let datatext = {
                y: datatexty,
                m: datatextm,
                d: datatextd,
            }
            $('#AntiCheatingModal .modal-dialog').html(template('AntiCheatingModal-script', { message: res.data, time: datatext }))
            AntiMessages.AntiModalPage(1)
        },
        error: function (req) {

        }
    });
}
AntiMessages.AntiModalPage = function (page) {
    AntiMessages.pageNum = page
    $('#AntiCheatingModal .modal-dialog').off('scroll')
    $.star({
        type: 'GET',
        url: '/battlecenter/redwar/match/v1/report/handle/list',
        showerror: false,
        data: {
            pageNum: AntiMessages.pageNum,
            pageSize: AntiMessages.pageSize,
            type: AntiMessages.type,
        },
        success: function (res) {
            if (page == 1) {
                $('.AntiCheating-list-content').html(template('AntiCheating-list-content-script', { list: res.data.list }))
            } else {
                $('.AntiCheating-list-content').append(template('AntiCheating-list-content-script', { list: res.data.list }))
            }
            if (res.data.list.length >= AntiMessages.pageSize) {
                AntiMessages.pageFunScroll()
            }
        },
        error: function (req) {

        }
    });
}
AntiMessages.pageFunScroll = function () {
    $('#AntiCheatingModal .modal-dialog').on('scroll', function () {
        var $this = $(this)
        var nScrollHight = $this[0].scrollHeight,
            nScrollTop = $this[0].scrollTop,
            nDivHight = $this.height();
        if (nScrollTop + nDivHight + 10 >= nScrollHight && !careerMessage.pageLoading) {
            $('#AntiCheatingModal .modal-dialog').off('scroll')
            AntiMessages.pageNum = AntiMessages.pageNum + 1
            AntiMessages.AntiModalPage(AntiMessages.pageNum)
        }
    })
}
$(document).on('click', '.AntiCheating-bot', function () {
    $('#AntiCheatingminModal .modal-dialog').html(template('AntiCheatingminModal-script'))
    $('#AntiCheatingminModal').modal('show')
})
$(document).on('shown.bs.modal', '#AntiCheatingminModal', function () {
    AntiMessages.Anticlipboard = new Clipboard('#AntiCheatingmin-content-a');
    AntiMessages.Anticlipboard.on('success', function () {
        showtoast({
            message: translatesrting('复制成功')
        })
    })
})
$(document).on('hidden.bs.modal', '#AntiCheatingminModal', function () {
    AntiMessages.Anticlipboard.destroy()
})
$(document).on('click', '.AntiCheating-nav-item', function () {
    var $this = $(this)
    let type = $this.attr('data-type')
    $this.siblings().removeClass('active')
    $this.addClass('active')
    AntiMessages.type = type
    AntiMessages.AntiModalPage(1)
    let heightnum = 0

    if (type == 2) {
        heightnum = $('.AntiCheating-text-one .AntiCheating-text-title').height() + $('.AntiCheating-text-one .AntiCheating-text-text').height() + 23
        $('.AntiCheating-text-two').animate({ height: 0, opacity: 0 })
        $('.AntiCheating-text-one').animate({ height: heightnum, opacity: 1 })
    } else if (type == 1) {
        heightnum = $('.AntiCheating-text-two .AntiCheating-text-title').height() + $('.AntiCheating-text-two .AntiCheating-text-text').height() + 23
        $('.AntiCheating-text-one').animate({ height: 0, opacity: 0 })
        $('.AntiCheating-text-two').animate({ height: heightnum, opacity: 1 })
    } else {
        $('.AntiCheating-text-one').animate({ height: 0, opacity: 0 })
        $('.AntiCheating-text-two').animate({ height: 0, opacity: 0 })
        // $('.AntiCheating-text-one').addClass('hide')
        // $('.AntiCheating-text-two').addClass('hide')
    }
})
$(document).on('hidden.bs.modal', '#AntiCheatingModal', function () {
    $('#AntiCheatingModal .modal-dialog').html('')
    AntiMessages.type = 0
    AntiMessages.pageNum = 1
})
$(document).on('shown.bs.modal', '#AntiCheatingModal', function () {
    $('.hall-header-left-swiper-item').one('click', function () {
        $('#AntiCheatingModal').modal('hide')
    })
    $('.hall-header-left-swiper .hall-header-left-swiper-item').removeClass('active')
})
// 校验开放时间
if (getSaveMessage('GameRulesAlert')) {

} else {
    $.star({
        type: "GET",
        url: '/battlecenter/redwar/matching/v2/game-rule',
        showerror: true,
        data: { 'type': 1, 'ladder': true },
        success: function (res) {
            if (res.data.isOpen) {
                $('.hall-header-left-swiper .hall-header-left-swiper-item').each(function () {
                    var $this = $(this)
                    if ($this.attr('data-index') == 1) {
                        $this.tooltip('show')
                        $('.hall-header').one('click', function () {
                            $this.tooltip('hide')
                        })
                    }
                })
                saveMessage('GameRulesAlert', 1)
                $('.Match-bg-padding').find('.match-rank-nav .match-rank-nav-item:nth-child(2)').addClass('active-border')
                $('.Match-bg-padding').find('.match-rank-nav .match-rank-nav-item:nth-child(2)').one('click', function () {
                    $('.Match-bg-padding').find('.match-rank-nav .match-rank-nav-item:nth-child(2)').removeClass('active-border')
                })
            }
        },
        error: function (req) {

        }
    })
}
// 欢迎弹窗
function welcomeModalfun() {
    let canopen = false
    if (localStorage.getItem('bannerday')) {
        let newday = new Date().getDay()
        if (newday != localStorage.getItem('bannerday')) {
            canopen = true
            localStorage.setItem('bannerday', newday)
        } else {
            canopen = false
        }
    } else {
        let newday = new Date().getDay()
        localStorage.setItem('bannerday', newday)
        canopen = true
    }
    if (localStorage.getItem('bannerdayweek')) {
        let newTime = new Date().getTime()
        if (newTime > (localStorage.getItem('bannerdayweek') * 1)) {
            canopen = true
        } else {
            canopen = false
        }
    }
    canopen = false
    if (!canopen) {
        gameCheckGame()
        return false
    }
    $.star({
        type: 'GET',
        url: '/battlecenter/novice/guide/v1/banner',
        showerror: false,
        data: {},
        success: function (res) {
            if (res.data.url) {
                let registday = res.data.systemTime - res.data.registTime
                registday = parseInt(registday / 60 / 60 / 24)
                if (registday <= 0) {
                    registday = 1
                }
                $('#welcomeModal').find('.modal-dialog').html(template('welcomeModal-script', {
                    username: userInfo.username,
                    day: registday,
                    img: res.data.url,
                    type: res.data.type,
                    coin: res.data.awardGoldNum
                }))
                $('#welcomeModal').modal('show')
                $('#welcomeModal .welcomeModal-bg').on('click', function () {
                    bannerOperation(res.data.type, res.data.extend)
                })
                $('#welcomeModal').on('hidden.bs.modal', function () {
                    gameCheckGame()
                    let endTime = new Date().getTime() + 604800000
                    if ($('#welcomeModalcheckbox').is(':checked')) {
                        localStorage.setItem('bannerdayweek', endTime)
                    } else {
                        localStorage.removeItem('bannerdayweek', endTime)
                    }
                })
            } else {
                gameCheckGame()
            }
        },
        error: function (req) {

        }
    });
}
// welcomeModalfun()
// 各种banner操作
function bannerOperation(type, options) {
    if (type == 1) {
        // 1.外链
        let url = options
        if (url.indexOf('ramboplay.com') != -1) {
            OpenRamUrl(url)
        } else {
            OpenWebUrl(url)
        }
    } else if (type == 2) {
        // 2.资讯详情页
        let itemid = options
        $.star({
            type: 'GET',
            url: '/battlecenter/article/v1/query/one',
            showerror: true,
            data: {
                id: itemid
            },
            success: function (res) {
                let message = res.data
                $('#informationtextModal .modal-dialog .informationtext-left').html(template('informationtextModal-script', { message: message }))
                $('#informationtextModal .informationtext-right').html(template('informationtext-right-script'))
                informationMessage.init('first')
                informationMessage.gethotlivelistfun(informationMessage.livelist)
                informationMessage.getHotList()
                $('.informationtext-text').html(message.articleContent)
                $('#informationtextModal').modal('show')
            },
            error: function (req) {

            }
        })
    } else if (type == 3) {
        // 3.大厅列表模式
        $('.hall-header-left-swiper-item').each(function () {
            var $this = $(this)
            if ($this.attr('data-index') == 0) {
                $this.click()
            }
        })
    } else if (type == 4) {
        // 3.大厅地图模式
        $('.hall-header-left-swiper-item').each(function () {
            var $this = $(this)
            if ($this.attr('data-index') == 0) {
                $this.click()
            }
        })
        $('.veranda-content-middle-nav').each(function () {
            var $this = $(this)
            if ($this.attr('data-type') == 2) {
                $this.click()
            }
        })
    } else if (type == 5) {
        //5.天梯匹配
        $('.hall-header-left-swiper-item').each(function () {
            var $this = $(this)
            if ($this.attr('data-index') == 1) {
                $this.click()
            }
        })
    } else if (type == 6) {
        // 6.普通匹配
        $('.hall-header-left-swiper-item').each(function () {
            var $this = $(this)
            if ($this.attr('data-index') == 2) {
                $this.click()
            }
        })
    } else if (type == 7) {
        // 生涯
        $('.hall-header-left-swiper-item').each(function () {
            var $this = $(this)
            if ($this.attr('data-index') == 3) {
                $this.click()
            }
        })
    } else if (type == 8) {
        // 8.修改用户名弹窗
        $('.J_UserAdmin').click()
    } else if (type == 9) {
        // 9.修改头像框弹窗
        $('.J_UserAdmin').click()
    } else if (type == 10) {
        AntiMessages.AntiModal()
    } else if (type == 11) {
        Refreshintosix()
        $('.hall-header-left-swiper-item').each(function () {
            var $this = $(this)
            if ($this.attr('data-index') == 6) {
                $this.click()
            }
        })
    } else if (type == 12) {
        Refreshintosix('apply')
        $('.hall-header-left-swiper-item[data-index="6"]').click()
    }
}

function Refreshintosix(topage) {
    let topurl = ''
    if (getSaveMessage('test')) {
        topurl = 'https://test.ramboplay.com'
    } else {
        topurl = 'https://ra2.com'
    }
    let inpage = '/match'
    if (topage == 'apply') {
        inpage = '/match/apply'
    }

    $('#competition>iframe').attr('src', topurl + inpage + '?isClient=1&UUtoken=' + getSaveMessage('userInfo').accessToken)
    $('#competition>iframe').css('opacity', 1)
}
// $this.parent().on('mouseenter', function () {
//     $this.popover('show')
// })
// $this.parent().on('mouseleave', function () {
//     $this.popover('hide')
// })

$(document).on('mouseenter', '.hall-header-left-swiper-item-generate', function () {
    clearTimeout(hallMessage.generatefun)
    if (hallMessage.indexTo == 6) {
        return false
    } else {
        hallMessage.generatefun = setTimeout(function () {
            Refreshintosix()
        }, 30)
    }
})
$(document).on('mouseleave', '.hall-header-left-swiper-item-generate', function () {
    clearTimeout(hallMessage.generatefun)
})

function verandaCheckOpenWay(type, addtype) {
    if (type == 'closeOpen') {
        if (addtype != 'onlycss') {
            noMessagefun.isopen = false
            clearInterval(verandaMessage.autoReset)
            ipcRenderer.send('Main', {
                msgType: "ActiveStatus",
                jsonInfo: {
                    status: false
                }
            });
        }
        $('.veranda-content-chatContent-scroll-loading').removeClass('hide')
        $('.veranda-content-chatContent-scroll').addClass('hide')
        $('.veranda-roomlist-body').addClass('hide')
        $('.veranda-roomlist-body-close').removeClass('hide')
    } else if (type == 'inOpen') {
        noMessagefun.isopen = true
        verandaMessage.getPageMessage('clear')
        ipcRenderer.send('Main', {
            msgType: "ActiveStatus",
            jsonInfo: {
                status: true
            }
        });
        $('.veranda-content-chatContent-scroll-loading').addClass('hide')
        $('.veranda-content-chatContent-scroll').removeClass('hide')
        $('.veranda-roomlist-body').removeClass('hide')
        $('.veranda-roomlist-body-close').addClass('hide')
    }
}
$(document).on('click', '.J_UserLogOut', function (e) {
    // 更换账号
    localStorage.setItem('JmenuChangeUser', 1)
})
$(document).on('click', '.J_Out', function (e) {
    // 退出
    localStorage.setItem('Jmenuclose', 1)
})