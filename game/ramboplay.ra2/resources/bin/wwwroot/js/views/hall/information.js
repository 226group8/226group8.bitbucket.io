var informationMessage = {
    init: null,//初始化
    topswiper: null,
    getlist: null,
    getlistpage: 1,
    getbanner: null,
    bannerlist: [],
    getrangklist: null,
    getlivelist: null,
    getHotList: null,
    livelist: [],
    livelistpage: 1,
    hotlivelist: [],
    hotlivelistpage: 1,
    liveloading: false,
    pageLoading: false,
    articleId: null,
    hotlist: [],
    hotlistpage: 1,
}
informationMessage.getlist = function (page, message) {
    informationMessage.getlistpage = page
    if (message) {
        informationMessage.getlistfun(1, message)
    } else {
        informationMessage.pageLoading = true
        $.star({
            type: 'GET',
            url: '/battlecenter/article/v1/list',
            showerror: true,
            data: {
                classifyId: 0,
                pageNum: page,
                pageSize: 5
            },
            success: async function (res) {
                informationMessage.pageLoading = false
                // --------图片数据处理
                let reslist = res.data.list
                let imgarray = new Map()
                for (var i = 0; i < reslist.length; i++) {
                    if (reslist[i].coverMode == 0 || reslist[i].coverMode == 1) {
                        for (item of reslist[i].imges) {
                            if (reslist[i].isIframe) {
                                imgarray.set(item + '?x-oss-process=image/resize,w_272', '')
                            } else {
                                imgarray.set(item + '?x-oss-process=image/resize,w_140', '')
                            }
                        }
                    }
                }
                let dellimgarray = await readerIndexedDBImgUrl(imgarray)
                for (var i = 0; i < reslist.length; i++) {
                    if (reslist[i].coverMode == 0 || reslist[i].coverMode == 1) {
                        for (var z = 0; z < reslist[i].imges.length; z++) {
                            let itemimg = ''
                            if (reslist[i].isIframe) {
                                itemimg = reslist[i].imges[z] + '?x-oss-process=image/resize,w_272'
                            } else {
                                itemimg = reslist[i].imges[z] + '?x-oss-process=image/resize,w_140'
                            }
                            if (dellimgarray.has(itemimg)) {
                                if (dellimgarray.get(itemimg)) {
                                    reslist[i].imges[z] = dellimgarray.get(itemimg)
                                } else {
                                    reslist[i].imges[z] = itemimg
                                }
                            }
                        }
                    }
                }
                // --------图片数据处理
                informationMessage.getlistfun(page, reslist, res.data.limit)
            },
            error: function (req) {
                informationMessage.pageLoading = false
            }

        });
    }
}
informationMessage.getlistfun = function (page, res, limit) {
    let list = res
    list.map((item) => {
        if (formatDateBeforDay(item.createTime, item.nowTime) == translatesrting('今天')) {
            item.Jistoday = true
        }
    })
    let additem = template('information-left-scroll-script', { list: list })
    if (page == 1) {
        $('.information-left-scroll').html(additem)
    } else {
        $('.information-left-scroll').append(additem)
    }
    if (list.length < limit) {
        //最后一页
    } else {
        informationMessage.changegetlist()
    }
}
informationMessage.changegetlist = function () {
    $('.information-left-scroll').on('scroll', function () {
        var $this = $(this)
        var nScrollHight = $this[0].scrollHeight,
            nScrollTop = $this[0].scrollTop,
            nDivHight = $this.height();
        if (nScrollTop + nDivHight + 10 >= nScrollHight && !informationMessage.pageLoading) {
            $('.information-left-scroll').off('scroll')
            informationMessage.getlist(Number(informationMessage.getlistpage) + 1)
        }
    })
}
informationMessage.getbanner = function (message) {
    if (message) {
        informationMessage.getbannerfun(message)
    } else {
        $.star({
            type: 'GET',
            url: '/battlecenter/article/v1/banner',
            showerror: true,
            data: {
            },
            success: async function (res) {
                // --------图片数据处理
                let urllist = res.data
                let imgarray = new Map()
                for (var i = 0; i < urllist.length; i++) {
                    imgarray.set(urllist[i].url, '')
                }
                let dellimgarray = await readerIndexedDBImgUrl(imgarray)
                for (list of urllist) {
                    let itemimg = list.url
                    if (dellimgarray.has(itemimg)) {
                        if (dellimgarray.get(itemimg)) {
                            list.url = dellimgarray.get(itemimg)
                        } else {
                            list.url = itemimg
                        }
                    }
                }
                // --------图片数据处理
                informationMessage.getbannerfun(urllist)
            },
            error: function (req) {

            }
        });
    }
}
informationMessage.getbannerfun = function (res) {
    let list = res
    informationMessage.bannerlist = list
    $('.information-right-top-swiper').html(template('information-right-top-swiper-script', { list: list }))
    $('.information-right-top-swiper-nav').html(template('information-right-top-swiper-nav-script', { list: list }))
    let autowidth = 100 / list.length
    $('.information-right-top-swiper-nav-item').css('width', autowidth + '%')
    informationMessage.topswiper = new Swiper('.information-right-top-swiper', {
        autoplay: {
            delay: 5000,//1秒切换一次
        },
        on: {
            slideChangeTransitionStart: function () {
                let nextindex = this.activeIndex
                $('.information-right-top-swiper-nav-item').each(function () {
                    if ($(this).attr('data-index') == nextindex) {
                        $(this).addClass('active')
                    } else {
                        $(this).removeClass('active')
                    }
                })
            },
        }
    })
}
informationMessage.getrangklist = function (message) {
    if (message) {
        informationMessage.getrangklistfun(message)
    } else {
        $.star({
            type: 'GET',
            url: '/battlecenter/qualifying/v1/information/rank',
            showerror: true,
            data: {
                size: 5
            },
            success: function (res) {
                informationMessage.getrangklistfun(res)
            },
            error: function (req) {

            }
        })
    }
}
informationMessage.getrangklistfun = function (res) {
    let list = res.data.qualifyingRankVO
    $(".information-content .information-right-bottom-right-div-header img[alt='logo']").attr('src', res.data.imageUrl + '?x-oss-process=image/resize,w_80').removeClass('opacity0')
    $(".information-content .information-right-bottom-right-div-header div").attr('data-url', res.data.linkUrl)
    $('.information-right-bottom-left-content').html(template('information-right-bottom-left-content-script', { list: list }))
}
informationMessage.getlivelist = function (message) {
    if (message) {
        informationMessage.getlivelistfun(message)
    } else {
        $.star({
            type: 'GET',
            url: '/battlecenter/live/v1/anchor',
            showerror: true,
            data: {
            },
            success: function (res) {
                informationMessage.getlivelistfun(res)
            },
            error: function (req) {

            }
        })
    }
}

informationMessage.getHotList = function () {
    $.star({
        type: 'GET',
        url: '/battlecenter/article/v1/hot/list',
        showerror: true,
        data: {
            presentArticleId: informationMessage.articleId,
            pageNum: 1,
            pageSize: 20
        },
        success: function (res) {
            informationMessage.getHotListFunc(res)
        },
        error: function (req) {

        }
    })
}

informationMessage.gethotlivelistfun = function (res) {
    let list = res.data
    informationMessage.hotlivelistpage = 1
    informationMessage.hotlivelist = res
    var showlist = []
    if (list.length > 4) {
        $('.information-hot-content .J_information-livechange').removeClass('hide')
        showlist = informationMessage.hotlivelist.data.slice(0, 4)
    } else {
        showlist = informationMessage.hotlivelist.data
    }
    $('.information-hot-content .information-right-bottom-right-content').html(template('information-right-bottom-right-content-script', { list: showlist }))
}

informationMessage.getlivelistfun = function (res) {
    let list = res.data
    informationMessage.livelistpage = 1
    informationMessage.livelist = res
    var showlist = []
    if (list.length > 5) {
        $('.information-main .J_information-livechange').removeClass('hide')
        showlist = informationMessage.livelist.data.slice(0, 5)
    } else {
        showlist = informationMessage.livelist.data
    }
    $('.information-main .information-right-bottom-right-content').html(template('information-right-bottom-right-content-script', { list: showlist }))
}

informationMessage.getHotListFunc = function (res) {
    let list = res.data.list
    informationMessage.hotlistpage = 1
    informationMessage.hotlist = res.data.list
    var showlist = []
    if (list.length > 4) {
        $('.J_information-hotchange').removeClass('hide')
        showlist = informationMessage.hotlist.slice(0, 4)
    } else {
        showlist = informationMessage.hotlist
    }
    $('.information-right-bottom-top-content').html(template('information-right-bottom-top-content-script', { list: showlist }))
}

informationMessage.init = function (type) {
    if (type == 'first' && localStorage.getItem('firstHallMessage')) {
        let firstHallMessage = getSaveMessage('firstHallMessage', true)
        informationMessage.getlist(1, firstHallMessage.getlist)
        informationMessage.getbanner(firstHallMessage.getbanner)
        informationMessage.getrangklist(firstHallMessage.getrangklist)
        informationMessage.getlivelist(firstHallMessage.getlivelist)
    } else {
        // 列表
        informationMessage.getlist(1)
        // banner
        informationMessage.getbanner()
        // 排行榜
        informationMessage.getrangklist()
        //直播
        informationMessage.getlivelist()
    }
}
// 热门换一换
$(document).on('click', '.J_information-hotchange', function () {
    var $this = $(this)
    let page = informationMessage.hotlistpage
    let list = informationMessage.hotlist
    var showlist = []
    if ((list.length - (page * 4)) > 0) {
        let slicenum = Number(page * 4)
        showlist = list.slice(slicenum, slicenum + 4)
        informationMessage.hotlistpage = informationMessage.hotlistpage + 1
    } else {
        showlist = list.slice(0, 4)
        informationMessage.hotlistpage = 1
    }
    $('.information-right-bottom-top-content').html(template('information-right-bottom-top-content-script', { list: showlist }))
    if (!informationMessage.liveloading) {
        informationMessage.liveloading = true
        // $this.find('img').css('animation', '0.3s rotate1 linear infinite')
        // $this.find('img').addClass('aniamter')
        setTimeout(() => {
            // $this.find('img').removeClass('aniamter')
            // $this.find('img').css('animation-iteration-count', 'unset')
            informationMessage.liveloading = false
        }, 1000)
    }
})
// 换一换
$(document).on('click', '.J_information-livechange', function () {
    var $this = $(this)
    let numb = $this.attr('data-numb')
    let page = ''
    let list = ''
    var showlist = []
    switch (numb) {
        case '4':
            page = informationMessage.hotlivelistpage
            list = informationMessage.hotlivelist.data
            if ((list.length - (page * 4)) > 0) {
                let slicenum = Number(page * 4)
                showlist = list.slice(slicenum, slicenum + 4)
                informationMessage.hotlivelistpage = informationMessage.hotlivelistpage + 1
            } else {
                showlist = list.slice(0, 4)
                informationMessage.hotlivelistpage = 1
            }
            $('.information-hot-content .information-right-bottom-right-content').html(template('information-right-bottom-right-content-script', { list: showlist }))
            break;
        case '5':
            page = informationMessage.livelistpage
            list = informationMessage.livelist.data
            if ((list.length - (page * 5)) > 0) {
                let slicenum = Number(page * 5)
                showlist = list.slice(slicenum, slicenum + 5)
                informationMessage.livelistpage = informationMessage.livelistpage + 1
            } else {
                showlist = list.slice(0, 5)
                informationMessage.livelistpage = 1
            }
            $('.information-main .information-right-bottom-right-content').html(template('information-right-bottom-right-content-script', { list: showlist }))
            if (!informationMessage.liveloading) {
                informationMessage.liveloading = true
                // $this.find('img').css('animation', '0.3s rotate1 linear infinite')
                // $this.find('img').addClass('aniamter')
                setTimeout(() => {
                    // $this.find('img').removeClass('aniamter')
                    // $this.find('img').css('animation-iteration-count', 'unset')
                    informationMessage.liveloading = false
                }, 1000)
            }
            break;
    }

})
// 查看更多
$(document).on('click', '.information-content .information-right-bottom-left .information-right-bottom-right-div-header div', function () {
    var $this = $(this)
    let url = $this.attr('data-url')
    if (url.indexOf('ramboplay.com') != -1) {
        OpenRamUrl(url)
    } else {
        OpenWebUrl(url)
    }
})
// 切换
$(document).on('click', '.information-right-top-swiper-nav-item', function () {
    var $this = $(this)
    let indexto = $this.attr('data-index')
    $this.siblings().removeClass('active')
    $this.addClass('active')
    informationMessage.topswiper.slideTo(Number(indexto), 500, false)
})
// 播放video
$(document).on('click', '.J_information-video', function () {
    var $this = $(this)
    let videoId = $this.attr('data-id')
    $.star({
        type: 'GET',
        url: '/battlecenter/article/v1/query/one',
        showerror: true,
        data: {
            id: videoId
        },
        success: function (res) { },
        error: function (req) { }
    })
    let url = $this.attr('data-url')
    $('#informationvideoModal video').attr('src', url)
    $('#informationvideoModal').modal('show')
})
// 播放video
$(document).on('click', '#informationvideoModal .modal-dialog', function () {
    $('#informationvideoModal').modal('hide')
})
// 播放video
$(document).on('hidden.bs.modal', '#informationvideoModal', function () {
    $('#informationvideoModal video').attr('src', '')
})
// 详情
$(document).on('click', '.J_information-text', function () {
    var $this = $(this)
    let id = $this.attr('data-id')
    informationMessage.articleId = id

    $.star({
        type: 'GET',
        url: '/battlecenter/article/v1/query/one',
        showerror: true,
        data: {
            id: id
        },
        success: function (res) {
            let message = res.data
            $('#informationtextModal .modal-dialog .informationtext-left').html(template('informationtextModal-script', { message: message }))
            $('#informationtextModal .informationtext-right').html(template('informationtext-right-script'))

            informationMessage.gethotlivelistfun(informationMessage.livelist)
            informationMessage.getHotList()

            $('.informationtext-text').html(message.articleContent)
            $('#informationtextModal').modal('show')
        },
        error: function (req) {

        }
    })
})
$(document).on('hidden.bs.modal', '#informationtextModal', function () {
    $('#informationtextModal .modal-dialog .informationtext-left').html('')
})
var baidulive = {
    timefun: null,
    time: 0,
    id: null
}
// 点击直播
$(document).on('click', '.J_information-live', function () {
    var $this = $(this)
    let id = $this.attr('data-id')
    let liveMessage = []
    informationMessage.livelist.data.map((item) => {
        if (item.id == id) {
            liveMessage = item
        }
    })
    $('#informationliveModal .modal-dialog').html(template('informationliveModal-script', { message: liveMessage }))
    $('#informationliveModal').modal('show')
    trackingFunc('live', id)
    baidulive.time = 0
    baidulive.id = id
    baidulive.timefun = setInterval(function () {
        baidulive.time = baidulive.time + 1
    }, 1000)
})
// $(document).on('click', '.informationliveModal-mock-btn', function (e) {
//     var $this = $(this)
//     let url = $this.attr('data-url')
//     OpenWebUrl(url)
// })
$(document).on('hidden.bs.modal', '#informationliveModal', function () {
    clearInterval(baidulive.timefun)
    trackingFunc('livetime', baidulive.id, baidulive.time)
    $('#informationliveModal .modal-dialog').html(' ')
})

$(document).on('click', '.J_information-banner', function () {
    var $this = $(this)
    let id = $this.attr('data-id')
    trackingFunc('hall_banner', id)
    let chooseitem = []
    informationMessage.bannerlist.map((item) => {
        if (item.id == id) {
            chooseitem = item
        }
    })

    bannerOperation(chooseitem.type, chooseitem.extend)
})


informationMessage.init('first')

// 直播 视频打开关闭bgm

$(document).on('show.bs.modal', '#informationliveModal', function () {
    globalMute(true)
    volumeFn()
})

$(document).on('hide.bs.modal', '#informationliveModal', function () {
    globalMute(false)
    volumeFn()
})

$(document).on('show.bs.modal', '#informationvideoModal', function () {
    globalMute(true)
    volumeFn()
})

$(document).on('hide.bs.modal', '#informationvideoModal', function () {
    globalMute(false)
    volumeFn()
})