var newHelpMessage = {
    commonHelping: false,
    gameUrl: null,
    oldMessage: null,
    type: 0,
    ipc: null,
    bot: 3,
    agin: 3,
    overfun: null
}

// newHelpChange(1)
// newHelpChange(8)


//1 搜索到路径 2搜索不到到路径 3下载中 4有游戏走检测 5文件异常 6游戏测试 7测试异常 8成功 9自动搜索中 10修复中 11第二步的安装中
function newHelpChange(type, options) {
    $('.newHelpModal-step-type').addClass('hide')
    newHelpMessage.type = type
    if (newHelpMessage.overfun) {
        clearTimeout(newHelpMessage.overfun)
    }
    if (type == 1) {
        $('.newHelpModal-step').attr('class', 'newHelpModal-step newHelpModal-step-changeone')
        $('.newHelpModal-step-one-success').removeClass('hide').animate({ opacity: 1 })
        $('.J_NewHelp_getUrl').val(options.url)
    }
    if (type == 2) {
        $('.newHelpModal-step').attr('class', 'newHelpModal-step newHelpModal-step-changeone')
        $('.newHelpModal-step-one-error').removeClass('hide').animate({ opacity: 1 })
        $('.newHelpModal-text').each(function () {
            if ($(this).attr('data-type') == 2) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
    }
    if (type == 3) {
        $('.newHelpModal-step').attr('class', 'newHelpModal-step newHelpModal-step-changeone')
        $('.newHelpModal-step-one-finding').removeClass('hide').animate({ opacity: 1 })
        $('.newHelpModal-text').each(function () {
            if ($(this).attr('data-type') == 2) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
        $('.newHelpModal-step-one-finding .media-right.media-middle').find('div').each(function () {
            if ($(this).attr('data-type') == 1) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
        $('.newHelpModal-step-one-finding .media-body div:nth-child(2)').text(translatesrting('正在为您安装游戏'))
    }
    if (type == 9) {
        $('.newHelpModal-step').attr('class', 'newHelpModal-step newHelpModal-step-changeone')
        $('.newHelpModal-step-one-finding').removeClass('hide').animate({ opacity: 1 })
        $('.newHelpModal-text').each(function () {
            if ($(this).attr('data-type') == 2) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
        $('.newHelpModal-step-one-finding .media-right.media-middle').find('div').each(function () {
            if ($(this).attr('data-type') == 2) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
        newHelpMessage.overfun = setTimeout(function () {
            $('.newHelpModal-step-one-finding-over').removeClass('hide')
        }, 10000)
        $('.newHelpModal-step-one-finding .media-body div:nth-child(2)').text(translatesrting('正在为您搜索游戏'))

    }
    // 第二步
    if (type == 4) {
        $('.newHelpModal-step').attr('class', 'newHelpModal-step newHelpModal-step-changetwo')
        $('.newHelpModal-step-two-start').removeClass('hide').animate({ opacity: 1 })
        $('.newHelpModal-text').each(function () {
            if ($(this).attr('data-type') == 2) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
        $.controlAjax({
            type: "get",
            url: '/api/lobby/game/checkFiles',
            showerror: false,
            success: function (res) {

            },
            error: function (req) {

            }
        })
        trackingFunc('newhelp_CheckGame')
    }
    if (type == 5) {
        $('.newHelpModal-step').attr('class', 'newHelpModal-step newHelpModal-step-changetwo')
        $('.newHelpModal-step-two-error').removeClass('hide').animate({ opacity: 1 })
        $('.newHelpModal-text').each(function () {
            if ($(this).attr('data-type') == 2) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
        $('.J_NewHelp_repair').removeClass('hide')
        $('.J_NewHelp_newInstallGames').removeClass('hide')
    }
    if (type == 6) {
        $('.newHelpModal-step').attr('class', 'newHelpModal-step newHelpModal-step-changetwo')
        $('.newHelpModal-step-two-test').removeClass('hide').animate({ opacity: 1 })
        $('.newHelpModal-text').each(function () {
            if ($(this).attr('data-type') == 2) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
        $.controlAjax({
            type: "POST",
            url: '/api/site/game/test/',
            showerror: true,
            data: {
                testType: 0,
                toMain: true
            },
            success: function (res) {
                // $this.gxbtn('reset')
            },
            error: function (req) {

            }
        })
        trackingFunc('newhelp_StartTest')
    }
    if (type == 7) {
        $('.newHelpModal-step').attr('class', 'newHelpModal-step newHelpModal-step-changetwo')
        $('.newHelpModal-step-two-testerror').removeClass('hide').animate({ opacity: 1 })
        $('.newHelpModal-text').each(function () {
            if ($(this).attr('data-type') == 2) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
    }
    if (type == 10) {
        $('.newHelpModal-step').attr('class', 'newHelpModal-step newHelpModal-step-changetwo')
        $('.newHelpModal-step-two-repairing').removeClass('hide').animate({ opacity: 1 })
        $('.newHelpModal-text').each(function () {
            if ($(this).attr('data-type') == 2) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
        $('.newHelpModal-step-two-repairing .media-right.media-middle').find('div').each(function () {
            if ($(this).attr('data-type') == 1) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
        $('.newHelpModal-step-two-repairing .media-body div:nth-child(2)').text(translatesrting('正在为您修复游戏'))
    }
    if (type == 11) {
        $('.newHelpModal-step').attr('class', 'newHelpModal-step newHelpModal-step-changetwo')
        $('.newHelpModal-step-two-repairing').removeClass('hide').animate({ opacity: 1 })
        $('.newHelpModal-text').each(function () {
            if ($(this).attr('data-type') == 2) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
        $('.newHelpModal-step-two-repairing .media-right.media-middle').find('div').each(function () {
            if ($(this).attr('data-type') == 2) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
        $('.newHelpModal-step-two-repairing .media-body div:nth-child(2)').text(translatesrting('正在为您安装游戏'))
    }
    // 第三步
    if (type == 8) {
        $('.newHelpModal-step').attr('class', 'newHelpModal-step newHelpModal-step-changethree')
        $('.newHelpModal-step-three-end').removeClass('hide').animate({ opacity: 1 })
        $('.newHelpModal-text').each(function () {
            if ($(this).attr('data-type') == 1) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
        clearInterval(newHelpMessage.ipc)
        trackingFunc('newhelp_Sucend')
    }
}
function newHelpinit() {
    $.controlAjax({
        type: "get",
        url: '/api/login/run/first',
        data: {
        },
        success: function (res) {
            newHelpStart()
        },
        error: function (req) {
            if (req.errorCode == 1) {
                setTimeout(function () {
                    newHelpinit()
                }, 1000)
            } else {
                welcomeModalfun()

            }
        }
    })
}
function newHelpStart(type) {
    trackingFunc('newhelp_init')
    newHelpMessage.commonHelping = true
    $.star({
        type: 'GET',
        url: '/community-user/user/v2/info',
        showerror: true,
        data: {},
        success: function (res) {
            newHelpMessage.agin = 3
            let registday = res.data.systemTime - res.data.registTime
            registday = parseInt(registday / 60 / 60 / 24)
            if (registday <= 0) {
                registday = 1
            }
            $('#newHelpModal').find('.modal-dialog').html(template('newHelpModal-content-script', { day: registday, username: userInfo.username }))
            $('#newHelpModal').modal('show')
            $.ajax({
                type: "get",
                url: '/api/site/settings/info/',
                success: function (res) {
                    newHelpMessage.oldMessage = res.data
                    if (res.data && res.data.gamePath && res.data.gamePath != 'game path') {

                        setTimeout(function () {
                            newHelpChange(1, { url: res.data.gamePath })
                            if (type == 'fileCheck') {
                                newHelpChange(4)
                            }
                        }, 2000)
                    } else {
                        // 没有路径开始搜索
                        newHelpChange(9)
                        ipcRenderer.send('Main', {
                            msgType: "FindGamePath",
                            jsonInfo: {
                                status: true,
                                type: 1,//新手自动保存
                                flag: 0//0大厅 1setting
                            }
                        });
                    }

                },
                error: function (req) {

                }
            })
        },
        error: function (req) {
            if (newHelpMessage.agin > 0) {
                newHelpMessage.agin = newHelpMessage.agin - 1
                setTimeout(function () {
                    newHelpStart()
                }, 2000)
            }

        }
    });
    clearInterval(newHelpMessage.ipc)
    newHelpMessage.ipc = setInterval(function () {
        let getitem = localStorage.getItem('newHelp');
        if (getitem && getitem == 'gamedownstart') {
            localStorage.removeItem('newHelp')
            if (newHelpMessage.type == 2 || newHelpMessage.type == 1) {
                newHelpChange(3)
            }
            if (newHelpMessage.type == 5) {
                newHelpChange(11)
            }
        }
        if (getitem && getitem == 'gamedownerror') {
            localStorage.removeItem('newHelp')
            if (newHelpMessage.type == 3) {
                newHelpStart()
            }
            if (newHelpMessage.type == 11) {
                newHelpChange(5)
            }
            trackingFunc('newhelp_InstallGameerror')
        }
        if (getitem && getitem == 'gamedownend') {
            localStorage.removeItem('newHelp')
            if (newHelpMessage.type == 3) {
                newHelpChange(4)
            }
            if (newHelpMessage.type == 11) {
                newHelpChange(6)
            }
            trackingFunc('newhelp_InstallGamesuc')
        }
        if (getitem && getitem == 'repairend') {
            localStorage.removeItem('newHelp')
            // 修复完
            newHelpChange(6)
            trackingFunc('newhelp_CheckGameRepairsuc')
        }
        if (getitem && getitem == 'repairerror') {
            localStorage.removeItem('newHelp')
            // 修复失败
            newHelpChange(5)
            showtoast({
                message: translatesrting('修复失败')
            })
            trackingFunc('newhelp_CheckGameRepairerror')
        }
        if (newHelpMessage.bot == 3) {
            $('.newHelpModal-animate-bot').text(".")
            newHelpMessage.bot = 1
        } else if (newHelpMessage.bot == 2) {
            $('.newHelpModal-animate-bot').text("...")
            newHelpMessage.bot = 3
        } else if (newHelpMessage.bot == 1) {
            $('.newHelpModal-animate-bot').text("..")
            newHelpMessage.bot = 2
        }
    }, 1000)
}
$(document).on('hidden.bs.modal', '#newHelpModal', function () {
    newHelpMessage.commonHelping = false
})
ipcRenderer.on('WebIpc', (event, message) => {
    let jsonmessage = JSON.parse(message.jsonInfo)
    if (message.msgType == 32) {
        if (jsonmessage && jsonmessage.length > 0) {
            //   搜索成功
            newHelpChange(1, { url: jsonmessage[0] })
            trackingFunc('newhelp_find')
        } else {
            //   搜索失败
            newHelpChange(2)
            trackingFunc('newhelp_finderror')
        }
    } else if (message.msgType == 38) {

        if (jsonmessage.status === true) {
            // 正常走测试
            setTimeout(function () {
                newHelpChange(6)
            }, 1000)
            trackingFunc('newhelp_CheckGamesuc')
        } else if (jsonmessage.status === false) {
            newHelpChange(5)
            $('.J_NewHelp_repair').removeClass('hide')
            if (jsonmessage.lessfiles == null || jsonmessage.lessfiles.length == 0) {
                $('.newHelpModal-step-two-error .media-body div:nth-child(2)').text(translatesrting('游戏文件无法修复,请全新安装'))
                $('.J_NewHelp_repair').addClass('hide')
            } else {
                $('.newHelpModal-step-two-error .media-body div:nth-child(2)').text(translatesrting('游戏文件存在异常，请选择'))
            }
            trackingFunc('newhelp_CheckGameerror')
        } else {
            $('.newHelpModal-step-two-start .media-right span').text(Number((Number(jsonmessage) * 100).toFixed(2)) + '%')
        }
    } else if (message.msgType == 25) {
        if (JSON.parse(message.jsonInfo).Completeness && JSON.parse(message.jsonInfo).GameTestStatus) {
            //   检测成功
            newHelpChange(8)
            trackingFunc('newhelp_StartTestSuc')
        } else {
            // 检测失败
            newHelpChange(7)
            trackingFunc('newhelp_StartTestError')
        }
    }
});


// 重置安装游戏
$(document).on('click', '.J_NewHelp_InstallGame', function () {
    showWindowModal('downloadmodal', '/Modal/download?auto=InstallGame')
    trackingFunc('newhelp_InstallGame')
})
// 确定地址走下一步
$(document).on('click', '.J_NewHelp_onetotwo', function () {
    newHelpChange(4)
})
// 我有游戏文件
$(document).on('click', '.J_NewHelp_mychooseGameUrl', function () {
    var file = $('.J_NewHelp_mychooseGameUrl_input')

    file.after(file.clone().val(""));

    file.remove();
    $('.J_NewHelp_mychooseGameUrl_input').click();
})
// 选完文件走二
$(document).on('change', '.J_NewHelp_mychooseGameUrl_input', function (e) {
    var gameUrl = e.currentTarget.files[0].path
    newHelpMessage.gameUrl = gameUrl.match(/(.*)\\/)[0]
    var file = $(this)

    file.after(file.clone().val(""));

    file.remove();
    // 目录不能存在中文
    if (!DoesExistChinese(newHelpMessage.gameUrl)) {
        newHelpzherror(newHelpMessage.gameUrl)
        $('.newHelpModal-step-one-error .media-body div:nth-child(2)').text(translatesrting('路径不支持中文,请更改目录'))
        return false
    }



    $.controlAjax({
        type: "POST",
        url: '/api/site/game/personalize',
        data: {
            gamePath: newHelpMessage.gameUrl,
            gameScreen: newHelpMessage.oldMessage.gameScreen,
            windowMode: newHelpMessage.oldMessage.windowMode,
            renderMode: newHelpMessage.oldMessage.renderMode,
            singleCpu: newHelpMessage.oldMessage.singleCpu,
            scrollRate: newHelpMessage.oldMessage.scrollRate,
            skipScoreScreen: newHelpMessage.oldMessage.skipScoreScreen,
            tooltips: newHelpMessage.oldMessage.tooltips,
            showHiddenObjects: newHelpMessage.oldMessage.showHiddenObjects,
            targetLines: newHelpMessage.oldMessage.targetLines,
        },
        success: function (res) {
            newHelpChange(4)
        },
        error: function (req) {
            $('.newHelpModal-step-one-error .media-body div:nth-child(2)').text(req.errorMsg)
            trackingFunc('newhelp_appointerror')
        }
    })
    trackingFunc('newhelp_appoint')
})
// 极速安装
$(document).on('click', '.J_NewHelp_fastInstallGames', function () {
    showWindowModal('downloadmodal', '/Modal/download?auto=InstallGame')
    trackingFunc('newhelp_InstallGameFast')
})
// 全新安装
$(document).on('click', '.J_NewHelp_newInstallGames', function () {
    $('.J_NewHelp_repair').addClass('hide')
    showWindowModal('downloadmodal', '/Modal/download?auto=InstallGame')
    trackingFunc('newhelp_CheckGameRepairInstall')
})
// 修复
$(document).on('click', '.J_NewHelp_repair', function () {
    $('.J_NewHelp_newInstallGames').addClass('hide')
    showWindowModal('downloadmodal', '/Modal/download?auto=RepairGame')
    newHelpChange(10)
    trackingFunc('newhelp_CheckGameRepair')
})
// 启动测试再一次
$(document).on('click', '.J_NewHelp_testagain', function () {
    newHelpChange(6)
    trackingFunc('newhelp_StartTestagain')

})
// 寻找帮助
$(document).on('click', '.J_NewHelp_findhelp', function () {
    qimoChatClick()
    trackingFunc('newhelp_findhelp')
})

// 完成步骤
$(document).on('click', '.J_NewHelp_startend', function () {
    $('#newHelpModal').modal('hide')
    $('.hall-header-left-swiper-item').each(function () {
        var $this = $(this)
        if ($this.attr('data-index') == 0) {
            $this.click()
        }
    })
    $('.J_veranda_fast_btn').click()
    trackingFunc('newhelp_succlick')
})
// 跳过搜索
$(document).on('click', '.newHelpModal-step-one-finding-over', function () {
    ipcRenderer.send('Main', {
        msgType: "FindGamePath",
        jsonInfo: {
            status: false,
            type: 1,//新手自动保存
            flag: 0//0大厅 1setting
        }
    });
    newHelpChange(2)
})


function newHelpzherror(url) {
    let geturl = url
    var pattern = /[^\u4E00-\u9FA5]/gi
    getstring = geturl.replace(pattern, '')
    getstring = getstring.split('')
    for (var i = 0; i < getstring.length; i++) {
        geturl = geturl.replaceAll(getstring[i], '<span style="color:#6B9EFF">' + getstring[i] + '</span>')
    }
    $('#zherrorModal .modal-dialog').html(template('zherrorModal-script'))
    $('#zherrorModal .f16').html(geturl)
    $('#zherrorModal').modal('show')
    $('#zherrorModal .J_zherrorModalbtnone').on('click', function () {
        $('.J_NewHelp_mychooseGameUrl_input').click();
    })
    $('#zherrorModal .J_zherrorModalbtntwo').on('click', function () {
        showWindowModal('downloadmodal', '/Modal/download?auto=InstallGame')
        $('#zherrorModal').modal('hide')
    })
}



