var updateMessage = {
    message: getSaveMessage('msgType21')
}
$('.updata-status').html(template('updata-status-script', { message: updateMessage.message, }))
// 点击更新
$(document).on('click', '.J_updataBtn', function () {
    $(this).button('loading')
    ipcRenderer.send('Main', {
        msgType: "CheckUpdate",
        jsonInfo: {
            status: 1
        }
    });
    $('.updata-status').html(template('updata-in-script', { nowPercentage: "1" }))
})

// 更新进度
ipcRenderer.on('WebIpc', (event, message) => {
    var jsonmessage = JSON.parse(message.jsonInfo)
    if (message.msgType == 20 && jsonmessage.sign == 'updateClient') {
        if ($('.gx-progress-bar').length > 0) {
            $('.gx-progress-bar').css('width', jsonmessage.nowPercentage + "%")
        } else {
            $('.updata-status').html(template('updata-in-script', { nowPercentage: jsonmessage.nowPercentage }))
        }
    }
});