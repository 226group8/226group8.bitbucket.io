// 翻译
var createMessage = {
    activeMap: null,
    gameRoomName: userInfo.username + translatesrting('的房间'),
    password: '',
    mapSha1: null,
    maxPlayers: null,
    model: null,
    isDownload: true,
    mapUrl: null,
    gameVersion: "1",
    maxPlayerNum: 7,
    roomType: 0
}
function createMessageinit() {
    let RoomName = userInfo.username + translatesrting('的房间'),
        password = '',
        roomType = 0
    if (getSaveMessage('LastGreanRoom') && getSaveMessage('LastGreanRoom').userId == userInfo.userId) {
        RoomName = getSaveMessage('LastGreanRoom').roomName
        password = getSaveMessage('LastGreanRoom').password
        roomType = getSaveMessage('LastGreanRoom').roomType
        $('.J_roomName').val(RoomName)
    }
    $('.J_roomName').attr('placeholder', createMessage.gameRoomName)
    $('.J_roomPassword').val(password)
    $('.J_createRoomChooseRoomType[data-code=' + roomType + ']').click()
    getOldMap()
    // 双房主预防
    localStorage.removeItem('msgType10')
    localStorage.removeItem('msgType2')
    createMessage = {
        activeMap: null,
        gameRoomName: RoomName,
        password: password,
        mapSha1: null,
        maxPlayers: null,
        model: null,
        isDownload: true,
        mapUrl: null,
        gameVersion: "1",
        maxPlayerNum: 7,
        roomType: roomType
    }
    $('.userNum-swich').each(function () {
        var $this = $(this)
        $this.gxswich({
        }, function (e) {
            createMessage.maxPlayerNum = e
        })
    })
}
// 监听房间名
$(document).on('input', '.J_roomName', function () {
    $this = $(this)
    createMessage.gameRoomName = $this.val()
})
// 监听房间密码
$(document).on('input', '.J_roomPassword', function () {
    $this = $(this)
    createMessage.password = $this.val()
})
// 监听游戏版本
$(document).on('click', '.createRoom-gameVersion-div', function () {
    $this = $(this)
    if ($this.hasClass('disabled-check') || $this.hasClass('no-choose')) {
        return false
    }
    $this.siblings().removeClass('active')
    $this.addClass('active')
    createMessage.gameVersion = $this.attr('data-type')
})

// 获取默认地图
function getOldMap() {
    $.controlAjax({
        type: "get",
        url: '/Modal/GetLastUseMap',
        showerror: true,
        success: function (res) {
            res.data.name = res.data.name + '&&||' + res.data.enName
            res.data.activityNum = formatMapHot(res.data.activityNum)
            res.data.imageUrl = res.data.imageUrl + '?x-oss-process=image/resize,w_180'
            oneImgCache(res.data.imageUrl).then((Cachereturn) => {
                res.data.localimageUrl = Cachereturn
                $('#createRoom-inMap').html(template('createRoom-inMap-script', res.data))
                createMessage.activeMap = res.data.name
                createMessage.maxPlayers = res.data.maxPlayerNum
                createMessage.mapSha1 = res.data.mapSha1 ? res.data.mapSha1 : res.data.shA1
                createMessage.model = res.data.lastModel != "" ? res.data.lastModel : res.data.models[0]
                createMessage.imageUrl = res.data.imageUrl ? res.data.imageUrl : null
                createMessage.gold = res.data.gold
                if (res.data.labels) {
                    createMessage.labels = res.data.labels.join("·")
                } else {
                    createMessage.labels = "-"
                }
                // 版本
                if (res.data.supportedVersion && res.data.supportedVersion != "") {
                    let supportedVersion = res.data.supportedVersion.split(',')
                    for (var i = 0; i < $('.createRoom-gameVersion-div').length; i++) {
                        if (supportedVersion.indexOf($('.createRoom-gameVersion-div').eq(i).attr('data-type')) == -1) {
                            $('.createRoom-gameVersion-div').eq(i).addClass('disabled-check')
                        } else {
                            $('.createRoom-gameVersion-div').eq(i).removeClass('disabled-check')
                        }
                    }
                }
                let gameVersion = res.data.gameVersion
                let supportedVersion = res.data.supportedVersion.split(',')
                let isinit = false
                supportedVersion.map((item) => {
                    if (item == gameVersion) {
                        isinit = true
                    }
                })
                if (isinit) {

                } else {
                    gameVersion = supportedVersion[0]
                }
                createMessage.gameVersion = gameVersion
                for (var i = 0; i < $('.createRoom-gameVersion-div').length; i++) {
                    if ($('.createRoom-gameVersion-div').eq(i).attr('data-type') == gameVersion) {
                        $('.createRoom-gameVersion-div').eq(i).click()
                    } else {
                    }
                }
            })
        },
        error: function (req) {
            $('#createRoom-inMap').html(template('createRoom-inMap-script'))
        }
    })
}
// 创建房间
$(document).on('click', '.createRoom-modal-btn', function () {
    var $this = $(this)
    if (!createMessage.activeMap) {
        showtoast({
            message: translatesrting('请先选择地图')
        })
        return false
    }
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    let nogetminMapPlayer = false
    comOption.roomTypeArray.map((item) => {
        if (item.code == createMessage.roomType) {
            if (item.minMapPlayer > createMessage.maxPlayers) {
                nogetminMapPlayer = true
            }
        }
    })
    if (nogetminMapPlayer) {
        showtoast({
            message: translatesrting('地图人数') + ' ' + createMessage.maxPlayers + ' ' + translatesrting('不支持当前房间模式，请切换房间模式')
        })
        return false
    }
    trackingFunc('create_room')
    $this.gxbtn('loading')
    // 增加硬币相应阻拦
    if (createMessage.gold == 0) {

        createSuccessfun()
    } else {
        $.star({
            type: 'GET',
            url: '/community-user/account/v1/query',
            showerror: false,
            data: {},
            success: function (res) {
                if (Number(res.data.gold) < Number(createMessage.gold)) {
                    $this.gxbtn('reset')
                    createErrorAnimated('您的金币不足，请玩一玩匹配或者加入别人的房间一起玩可获得大量的金币哦')
                    return false;
                } else {
                    createSuccessfun()
                }
            },
            error: function (req) {
                $this.gxbtn('reset')
                createErrorAnimated(req.errorMsg)
            }
        });
    }
})
function createSuccessfun() {
    if (createMessage.gameRoomName == '') {
        createMessage.gameRoomName = userInfo.username + translatesrting('的房间')
    } else {
        $('.createRoom-modal-input-error').addClass('opacity0');
    }

    if (!createMessage.isDownload) {
        ipcRenderer.send('Main', {
            msgType: "DownLoadFile",
            jsonInfo: {
                DownLoadPath: createMessage.mapUrl,
                Sign: createMessage.mapSha1
            }
        });
        $('.createRoom-modal-btn').addClass('hide')
        $('.createRoom-modal-btn-downloading').removeClass('hide')
        return false
    }
    if (createMessage.roomType == 4 && createMessage.password == '') {
        createMessage.maxPlayers = 6
    }
    ipcRenderer.send('Main', {
        msgType: "CreateRoom",
        jsonInfo: {
            password: createMessage.password,
            gameRoomName: createMessage.gameRoomName,
            tunnelIndex: 0,
            mapSha1: createMessage.mapSha1,
            mapName: createMessage.activeMap,
            // maxPlayers: Number(createMessage.maxPlayerNum) + 1,
            maxPlayers: createMessage.maxPlayers,
            model: createMessage.model,
            imageUrl: createMessage.imageUrl,
            gameVersion: createMessage.gameVersion,
            label: createMessage.labels,
            roomType: createMessage.roomType
        }
    });
    saveMessage('LastGreanRoom', {
        roomName: createMessage.gameRoomName,
        password: createMessage.password,
        roomType: createMessage.roomType,
        userId: userInfo.userId
    })
    setTimeout(() => {
        $('.createRoom-modal-btn').gxbtn('reset')
    }, 800)
}
function createSuccessAnimated() {
    $('.pageAnimate-border').addClass('pageAnimate-border-in')
    setTimeout(() => {
        $('.gx-modal-content').css('left', 1000)
    }, 320)
}
function createErrorAnimated(message) {
    $('.createRoom-modal-error').text(message).addClass('animated fadeIn')
    setTimeout(() => {
        $('.createRoom-modal-error').addClass('fadeOut')
        setTimeout(() => {
            $('.createRoom-modal-error').removeClass('fadeOut').removeClass('fadeIn')
        }, 1000)
    }, 5000)
}
$(document).on('hidden.bs.modal', '#createRoomModal', function () {
    $('#createRoomModal .modal-dialog').removeClass('createRoomwidth')
})
// 创建成功监听
ipcRenderer.on('WebIpc', (event, message) => {
    if (message.msgType == 2) {
        // ipcRenderer.send('Main', {
        //     msgType: "ChangeWindowSize",
        //     jsonInfo: {
        //         window: 'createroom',
        //         width: 1280,
        //         height: 720,
        //     }
        // });
        if ($('#detailsSwiper>iframe').attr('src') == 'about:blank') {
            showhallGameRoom()
            $('#createRoomModal').modal('hide')
        }
        // location.href = '/Details/index'
        // $('.gx-modal-content').addClass('hide')
    }
    if (message.msgType == 20) {
        var jsonmessage = JSON.parse(message.jsonInfo)
        var mapSha1 = jsonmessage.sign
        if (mapSha1 == createMessage.mapSha1) {
            if ((jsonmessage.nowPercentage - 0) >= 100) {
                ipcRenderer.send('Main', {
                    msgType: "CreateRoom",
                    jsonInfo: {
                        password: createMessage.password,
                        gameRoomName: createMessage.gameRoomName,
                        tunnelIndex: 0,
                        mapSha1: createMessage.mapSha1,
                        mapName: createMessage.activeMap,
                        // maxPlayers: Number(createMessage.maxPlayerNum) + 1,
                        maxPlayers: createMessage.maxPlayers,
                        model: createMessage.model,
                        imageUrl: createMessage.imageUrl,
                        gameVersion: createMessage.gameVersion,
                        label: createMessage.labels,
                        roomType: createMessage.roomType
                    }
                });
                setTimeout(() => {
                    $('.createRoom-modal-btn').gxbtn('reset')
                    $('.createRoom-modal-btn').removeClass('hide')
                    $('.createRoom-modal-btn-downloading').addClass('hide')
                }, 800)
            } else {
                $('.createRoom-modal-btn').addClass('hide')
                $('.createRoom-modal-btn-downloading').removeClass('hide')
            }
        }
    }
    return false;
});
// 切换选择游戏
$(document).on('click', '.J_showChooseMap', function () {
    $('.createRoomlist-modal').removeClass('hide')
    $('.createRoomfirst-modal').addClass('createRoom-hide')
    chooseMapInit()
    // $('.J_chooseMapNav').eq(0).click()
    $('#createRoomModal .modal-dialog').addClass('createRoomwidth')
    // ipcRenderer.send('Main', {
    //     msgType: "ChangeWindowSize",
    //     jsonInfo: {
    //         window: 'createroom',
    //         width: 700,
    //         height: 432,
    //         isCenter: true
    //     }
    // });
})
$(document).on('click', '.J_return', function () {
    $('.createRoomlist-modal').addClass('hide')
    $('.createRoomfirst-modal').removeClass('createRoom-hide')
    $('#createRoomModal .modal-dialog').removeClass('createRoomwidth')
    // ipcRenderer.send('Main', {
    //     msgType: "ChangeWindowSize",
    //     jsonInfo: {
    //         window: 'createroom',
    //         width: 518,
    //         height: 394,
    //         isCenter: true,
    //     }
    // });
})
// 使用地图
$(document).on('click', '.J_sureChoose', function () {
    mapCreateChoose()
})

// 进入房间声音
volumeFn()

$(document).on('click', '.J_createRoom-input-add', function () {
    var $this = $(this)
    $this.toggleClass('show')
    if ($this.hasClass('show')) {
        $this.closest('.createRoom-input-add').find('.J_roomPassword').attr('type', 'text')
    } else {
        $this.closest('.createRoom-input-add').find('.J_roomPassword').attr('type', 'password')
    }
})
$(document).on('click', '.J_createRoomChooseRoomType', function () {
    var $this = $(this)
    let code = $this.attr('data-code')
    let textchose = $this.attr('data-text')
    createMessage.roomType = code
    $this.closest('.createRoom-gx-menu').find('.gx-menu-in-text').text(textchose)
})
