var setingMessage = {
    page: 'gamesetingMove',
    getMessage: null,//获取信息
    checkloading: false,
    scrollRate: [6, 5, 4, 3, 2, 1, 0],
    testshow: true,
    isingamesetingMoveTest: false,
    resolvingArray: [
        { size: '1024x768', isFull: false },
        { size: '1280x800', isFull: false },
        { size: '1280x720', isFull: false },
        { size: '1366x768', isFull: false },
        { size: '1440x900', isFull: false },
        { size: '1536x864', isFull: false },
        { size: '1600x900', isFull: false },
        { size: '1680x1050', isFull: false },
        { size: '1920x1080', isFull: false },
        { size: '1920x1200', isFull: false },
        { size: '2048x1152', isFull: false },
        { size: '2560x1440', isFull: false },
        { size: '2560x1600', isFull: false },
    ],
    resolvingtypeArray: [
        { name: '未设置', code: 'Default' },
        { name: '独占全屏显示', code: 'FullScreen' },
        { name: '窗口模式', code: 'Window' },
        { name: '无边框窗口模式', code: 'BorderlessWindow' },
        { name: '拉伸全屏模式', code: 'WindowFullScreen' }
    ],
    renderModearray: [
        { name: '超级兼容模式', code: 'auto' },
        { name: 'OpenGL', code: 'opengl' },
        { name: 'GDI', code: 'gdi' }
    ],
    changeSetingfun: {
        gamesetingMove: null
    },//接口倒计时
    alertDomFun: null,
    InterfaceKey: [
        {
            "key": "TypeSelect",
            "value": "84",
            "text": "选择同类型单位",
            "keyCode": ['T']
        }, {
            "key": "CombatantSelect",
            "value": "80",
            "text": "选择战斗单位",
            "keyCode": ['P']
        }, {
            "key": "HealthNav",
            "value": "",
            "text": "按生命值顺序选择",
            "keyCode": []
        }, {
            "key": "VeterancyNav",
            "value": "89",
            "text": "按等级顺序选择",
            "keyCode": ['Y']
        }, {
            "key": "NextObject",
            "value": "78",
            "text": "下一个单位",
            "keyCode": ['N']
        }, {
            "key": "PreviousObject",
            "value": "77",
            "text": "上一个单位",
            "keyCode": ['M']
        }
    ],
    operationKey: [
        {
            "key": "StructureTab",
            "value": "81",
            "text": "建筑页",
            "keyCode": ['Q']
        }, {
            "key": "DefenseTab",
            "value": "87",
            "text": "防御页",
            "keyCode": ['W']
        }, {
            "key": "InfantryTab",
            "value": "69",
            "text": "步兵页",
            "keyCode": ['E']
        }, {
            "key": "UnitTab",
            "value": "82",
            "text": "载具页",
            "keyCode": ['R']
        }, {
            "key": "ToggleRepair",
            "value": "75",
            "text": "维修",
            "keyCode": ['K']
        }, {
            "key": "ToggleSell",
            "value": "76",
            "text": "变卖",
            "keyCode": ['L']
        }, {
            "key": "PlanningMode",
            "value": "90",
            "text": "路径点",
            "keyCode": ['Z']
        },
        // {
        //     "key": "CenterOnRadarEvent",
        //     "value": "32",
        //     "text": "雷达事件",
        //     "keyCode": ['Space']
        // }, 
        {
            "key": "CenterBase",
            "value": "72",
            "text": "居中基地",
            "keyCode": ['H']
        }, {
            "key": "ToggleAlliance",
            "value": "65",
            "text": "结盟",
            "keyCode": ['A']
        }, {
            "key": "DeployObject",
            "value": "68",
            "text": "部署",
            "keyCode": ['D']
        }, {
            "key": "GuardObject",
            "value": "71",
            "text": "区域警戒",
            "keyCode": ['G']
        }, {
            "key": "ScatterObject",
            "value": "88",
            "text": "散开",
            "keyCode": ['X']
        }, {
            "key": "StopObject",
            "value": "83",
            "text": "停止",
            "keyCode": ['S']
        }, {
            "key": "Follow",
            "value": "70",
            "text": "跟随",
            "keyCode": ['F']
        }, {
            "key": "View1",
            "value": "112",
            "text": "居中地图区域1",
            "keyCode": ['F1']
        }, {
            "key": "View2",
            "value": "113",
            "text": "居中地图区域2",
            "keyCode": ['F2']
        }, {
            "key": "View3",
            "value": "114",
            "text": "居中地图区域3",
            "keyCode": ['F3']
        }, {
            "key": "View4",
            "value": "115",
            "text": "居中地图区域4",
            "keyCode": ['F4']
        }, {
            "key": "SetView1",
            "value": "624",
            "text": "设置地图区域1",
            "keyCode": ['Ctrl', 'F1']
        }, {
            "key": "SetView2",
            "value": "625",
            "text": "设置地图区域2",
            "keyCode": ['Ctrl', 'F2']
        }, {
            "key": "SetView3",
            "value": "626",
            "text": "设置地图区域3",
            "keyCode": ['Ctrl', 'F3']
        }, {
            "key": "SetView4",
            "value": "627",
            "text": "设置地图区域4",
            "keyCode": ['Ctrl', 'F4']
        }, {
            "key": "PlaceBeacon",
            "value": "66",
            "text": "设置信标",
            "keyCode": ['B']
        }, {
            "key": "Delete",
            "value": "110",
            "text": "删除信标",
            "keyCode": ['Delete']
        }
    ],
    teamKey: [{
        "key": "TeamSelect_1",
        "value": "49",
        "text": "选中编队1",
        "keyCode": ['1']
    }, {
        "key": "TeamSelect_2",
        "value": "50",
        "text": "选中编队2",
        "keyCode": ['2']
    }, {
        "key": "TeamSelect_3",
        "value": "51",
        "text": "选中编队3",
        "keyCode": ['3']
    }, {
        "key": "TeamSelect_4",
        "value": "52",
        "text": "选中编队4",
        "keyCode": ['4']
    }, {
        "key": "TeamSelect_5",
        "value": "53",
        "text": "选中编队5",
        "keyCode": ['5']
    }, {
        "key": "TeamSelect_6",
        "value": "54",
        "text": "选中编队6",
        "keyCode": ['6']
    }, {
        "key": "TeamCreate_1",
        "value": "561",
        "text": "创建编队1",
        "keyCode": ['Ctrl', '1']
    }, {
        "key": "TeamCreate_2",
        "value": "562",
        "text": "创建编队2",
        "keyCode": ['Ctrl', '2']
    }, {
        "key": "TeamCreate_3",
        "value": "563",
        "text": "创建编队3",
        "keyCode": ['Ctrl', '3']
    }, {
        "key": "TeamCreate_4",
        "value": "564",
        "text": "创建编队4",
        "keyCode": ['Ctrl', '4']
    }, {
        "key": "TeamCreate_5",
        "value": "565",
        "text": "创建编队5",
        "keyCode": ['Ctrl', '5']
    }, {
        "key": "TeamCreate_6",
        "value": "566",
        "text": "创建编队6",
        "keyCode": ['Ctrl', '6']
    }, {
        "key": "TeamAddSelect_1",
        "value": "305",
        "text": "新增选中编队1",
        "keyCode": ['Shift', '1']
    }, {
        "key": "TeamAddSelect_2",
        "value": "306",
        "text": "新增选中编队2",
        "keyCode": ['Shift', '2']
    }, {
        "key": "TeamAddSelect_3",
        "value": "307",
        "text": "新增选中编队3",
        "keyCode": ['Shift', '3']
    }, {
        "key": "TeamAddSelect_4",
        "value": "308",
        "text": "新增选中编队4",
        "keyCode": ['Shift', '4']
    }, {
        "key": "TeamAddSelect_5",
        "value": "309",
        "text": "新增选中编队5",
        "keyCode": ['Shift', '5']
    }, {
        "key": "TeamAddSelect_6",
        "value": "310",
        "text": "新增选中编队6",
        "keyCode": ['Shift', '6']
    }, {
        "key": "TeamCenter_1",
        "value": "1073",
        "text": "居中编队1",
        "keyCode": ['Alt', '1']
    }, {
        "key": "TeamCenter_2",
        "value": "1074",
        "text": "居中编队2",
        "keyCode": ['Alt', '2']
    }, {
        "key": "TeamCenter_3",
        "value": "1075",
        "text": "居中编队3",
        "keyCode": ['Alt', '3']
    }, {
        "key": "TeamCenter_4",
        "value": "1076",
        "text": "居中编队4",
        "keyCode": ['Alt', '4']
    }, {
        "key": "TeamCenter_5",
        "value": "1077",
        "text": "居中编队5",
        "keyCode": ['Alt', '5']
    }, {
        "key": "TeamCenter_6",
        "value": "1078",
        "text": "居中编队6",
        "keyCode": ['Alt', '6']
    }
    ],
    key: [
        // { keycode: 27, text: 'Esc' },
        { keycode: 112, text: 'F1' },
        { keycode: 113, text: 'F2' },
        { keycode: 114, text: 'F3' },
        { keycode: 115, text: 'F4' },
        { keycode: 116, text: 'F5' },
        { keycode: 117, text: 'F6' },
        { keycode: 118, text: 'F7' },
        { keycode: 119, text: 'F8' },
        { keycode: 120, text: 'F9' },
        { keycode: 121, text: 'F10' },
        { keycode: 122, text: 'F11' },
        { keycode: 123, text: 'F12' },
        // { keycode: 145, text: 'Scroll_Lock' },
        { keycode: 192, text: '~' },
        { keycode: 49, text: '1' },
        { keycode: 50, text: '2' },
        { keycode: 51, text: '3' },
        { keycode: 52, text: '4' },
        { keycode: 53, text: '5' },
        { keycode: 54, text: '6' },
        { keycode: 55, text: '7' },
        { keycode: 56, text: '8' },
        { keycode: 57, text: '9' },
        { keycode: 48, text: '0' },
        { keycode: 189, text: '-' },
        { keycode: 187, text: '=' },
        { keycode: 8, text: 'BackSpace' },
        // { keycode: 9, text: 'Tab' },
        { keycode: 81, text: 'Q' },
        { keycode: 87, text: 'W' },
        { keycode: 69, text: 'E' },
        { keycode: 82, text: 'R' },
        { keycode: 84, text: 'T' },
        { keycode: 89, text: 'Y' },
        { keycode: 85, text: 'U' },
        { keycode: 73, text: 'I' },
        { keycode: 79, text: 'O' },
        { keycode: 80, text: 'P' },
        { keycode: 219, text: '[' },
        { keycode: 221, text: ']' },
        { keycode: 13, text: 'Enter' },
        { keycode: 20, text: 'Caps_Lock' },
        { keycode: 65, text: 'A' },
        { keycode: 83, text: 'S' },
        { keycode: 68, text: 'D' },
        { keycode: 70, text: 'F' },
        { keycode: 71, text: 'G' },
        { keycode: 72, text: 'H' },
        { keycode: 74, text: 'J' },
        { keycode: 75, text: 'K' },
        { keycode: 76, text: 'L' },
        { keycode: 186, text: ';' },
        { keycode: 222, text: "'" },
        { keycode: 220, text: '|' },
        { keycode: 16, text: 'Shift' },
        { keycode: 90, text: 'Z' },
        { keycode: 88, text: 'X' },
        { keycode: 67, text: 'C' },
        { keycode: 86, text: 'V' },
        { keycode: 66, text: 'B' },
        { keycode: 78, text: 'N' },
        { keycode: 77, text: 'M' },
        { keycode: 188, text: ',' },
        { keycode: 190, text: '.' },
        { keycode: 191, text: '/' },
        { keycode: 17, text: 'Ctrl' },
        // { keycode: 91, text: 'Win' },
        { keycode: 18, text: 'Alt' },
        // { keycode: 32, text: 'Space' },
        // { keycode: 92, text: 'Win' },
        // { keycode: 93, text: 'Fn' },
        { keycode: 19, text: 'Pause_Break' },
        { keycode: 45, text: 'Insert' },
        { keycode: 36, text: 'Home' },
        { keycode: 33, text: 'Page_Up' },
        // { keycode: 46, text: 'Delete' },
        { keycode: 35, text: 'End' },
        { keycode: 34, text: 'Page_Down' },
        { keycode: 38, text: '上' },
        { keycode: 37, text: '左' },
        { keycode: 40, text: '下' },
        { keycode: 39, text: '右' },
        // { keycode: 144, text: 'Num_Lock' },
        { keycode: 111, text: '/' },
        { keycode: 106, text: '*' },
        { keycode: 109, text: '-' },
        { keycode: 103, text: 'Num7' },
        { keycode: 104, text: 'Num8' },
        { keycode: 105, text: 'Num9' },
        { keycode: 100, text: 'Num4' },
        { keycode: 101, text: 'Num5' },
        { keycode: 102, text: 'Num6' },
        { keycode: 97, text: 'Num1' },
        { keycode: 98, text: 'Num2' },
        { keycode: 99, text: 'Num3' },
        { keycode: 107, text: '+' },
        // { keycode: 110, text: 'Delete' },
        { keycode: 96, text: 'Num0' }
    ],
    luKey: {
        open: false,
        keyArray: [],
        timefun: null,
        type: null,
        code: null
    }
}
$(function () {
    if (getQueryString('page')) {
        setingMessage.page = getQueryString('page')
    }
    // 加载
    $.controlAjax({
        type: "get",
        url: '/api/site/settings/info/',
        showerror: true,
        success: function (res) {
            res.data.scrollRate = setingMessage.scrollRate[res.data.scrollRate]
            setingMessage.getMessage = res.data
            // 屏幕尺寸
            let isInarray = false
            setingMessage.resolvingArray.map((item) => {
                if (item.size == res.data.maxGameScreen) {
                    isInarray = true
                    item.isFull = true
                }
            })

            if (!isInarray) {
                setingMessage.resolvingArray.unshift({ size: res.data.maxGameScreen, isFull: true })
            }
            $('#seting').html(template('translate-seting'))
            pageActive()
        },
        error: function (req) {

        }
    })
})
// 监听
ipcRenderer.on('WebIpc', (event, message) => {
    var jsonmessage = JSON.parse(message.jsonInfo)
    if (message.msgType == 25) { // 扫描游戏完成
        ipcRenderer.send('Main', {
            msgType: "WindowSize",
            jsonInfo: {
                window: 'settingmodal',
                size: 'normal'
            }
        });
        if (setingMessage.isingamesetingMoveTest) {
            $('.seting-gamesetting-after-chack-btn').addClass('hide')
            $('.seting-gamesetting-after-chack-btn').gxbtn('reset')
            setingMessage.isingamesetingMoveTest = false
        } else {
            setingMessage.page = 'gamesetingTest'
            pageActive()
        }
        setingMessage.checkloading = false
    }
})

// 菜单显示
function pageActive() {
    let activepage = setingMessage.page
    if (setingMessage.page == 'hotkey') {
        activepage = 'gamesetingMove'
    }
    $('.seting-list-li').each(function () {
        if ($(this).attr('code') == activepage) {
            $(this).addClass('active')
            let getname = $(this).attr('name')
            $('.seting-list-ul').each(function () {
                if ($(this).attr('name') == getname) {
                    $(this).addClass('active')
                } else {
                    $(this).removeClass('active')
                }
            })
        } else {
            $(this).removeClass('active')
        }
    })
    if (setingMessage.page == 'gamesetingMove') {
        setingMessage.getMessage.dumu = localStorage.getItem('Ra_dumuType') || 1
        // 运行设置
        $('.seting-showDiv').html(template('seting-' + setingMessage.page, {
            message: setingMessage.getMessage,
            resolvingArray: setingMessage.resolvingArray,
            resolvingtypeArray: setingMessage.resolvingtypeArray,
            renderModearray: setingMessage.renderModearray
        }))
        $('.gamesetingMove-scrollRate').GXRangeSlider({
            min: 0, max: 6, step: 1, callback: function (e) {
                $('.gamesetingMove-scrollRate').closest('div').find('.gx-range-text').text(e.value)
            }
        });
    } else if (setingMessage.page == 'gamesetingVoice') {
        // 游戏音效设置
        $('.seting-showDiv').html(template('seting-' + setingMessage.page, { message: setingMessage.getMessage }))
        $('.gamesetingVoice .gx-range').each(function () {
            let $gamesetingVoicerange = $(this)
            $gamesetingVoicerange.GXRangeSlider({
                min: 0, max: 10, step: 1, callback: function (e) {
                    $gamesetingVoicerange.closest('div').find('.gx-range-text').text(e.value)
                    $gamesetingVoicerange.closest('div').find('svg').addClass('hide')
                    if (e.value == 0) {
                        $gamesetingVoicerange.closest('div').find('.volume-none').removeClass('hide')
                    } else if (e.value < 4 && e.value > 0) {
                        $gamesetingVoicerange.closest('div').find('.volume-min').removeClass('hide')
                    } else if (e.value < 8 && e.value >= 4) {
                        $gamesetingVoicerange.closest('div').find('.volume-middle').removeClass('hide')
                    } else {
                        $gamesetingVoicerange.closest('div').find('.volume-large').removeClass('hide')
                    }
                }
            });
        })
    } else if (setingMessage.page == 'platformsetingPrivate') {
        // 隐私设置
        $.star({
            type: 'GET',
            url: '/community-user/user/config/v1/list',
            showerror: true,
            data: {
                source: 1
            },
            success: function (res) {
                let battle_switch_user_level = 'N'
                let battle_switch_user_authenticate = 'N'
                let changewei = 'N'
                res.data.map((item) => {
                    if (item.name == 'battle_switch_user_level') {
                        battle_switch_user_level = item.value
                    }
                    if (item.name == 'battle_switch_user_authenticate') {
                        battle_switch_user_authenticate = item.value
                    }
                    if (item.name == 'battle_switch_user_transposition') {
                        changewei = item.value
                    }
                })
                $('.seting-showDiv').html(template('seting-' + setingMessage.page, {
                    userLevel: battle_switch_user_level,
                    authenticate: battle_switch_user_authenticate,
                    changewei: changewei
                }))
            },
            error: function (req) {
            }
        });
    } else if (setingMessage.page == 'platformsetingVoice') {
        // 平台音效
        let temp_json = getSaveMessage('ClientVolume')
        let backstageout = getSaveMessage('backstageout') || 0
        $('.seting-showDiv').html(template('seting-' + setingMessage.page, { message: temp_json, backstageout: backstageout }))
        setingMessage.getMessage.clientBgm = temp_json.clientBgm.volume
        setingMessage.getMessage.clientSound = temp_json.clientSound.volume
        setingMessage.getMessage.globalVolume = temp_json.globalVolume.volume
        $('.platformsetingVoice .gx-range').each(function () {
            let $gamesetingVoicerange = $(this)
            $gamesetingVoicerange.GXRangeSlider({
                min: 0, max: 10, step: 1, callback: function (e) {
                    $gamesetingVoicerange.closest('div').find('.gx-range-text').text(e.value)
                    $gamesetingVoicerange.closest('div').find('svg').addClass('hide')
                    if (e.value == 0) {
                        $gamesetingVoicerange.closest('div').find('.volume-none').removeClass('hide')
                    } else if (e.value < 4 && e.value > 0) {
                        $gamesetingVoicerange.closest('div').find('.volume-min').removeClass('hide')
                    } else if (e.value < 8 && e.value >= 4) {
                        $gamesetingVoicerange.closest('div').find('.volume-middle').removeClass('hide')
                    } else {
                        $gamesetingVoicerange.closest('div').find('.volume-large').removeClass('hide')
                    }
                }
            });
        })
    } else if (setingMessage.page == 'hotkey') {
        // 快捷键
        $.controlAjax({
            type: "get",
            url: '/api/site/game/hotkeys/',
            showerror: true,
            success: function (res) {
                if (res.data) {
                    setingMessage.getMessage.InterfaceKey = dellUserKey('InterfaceKey', res.data);
                    setingMessage.getMessage.operationKey = dellUserKey('operationKey', res.data);
                    setingMessage.getMessage.teamKey = dellUserKey('teamKey', res.data);
                } else {
                    setingMessage.getMessage.InterfaceKey = JSON.parse(JSON.stringify(setingMessage.InterfaceKey))
                    setingMessage.getMessage.operationKey = JSON.parse(JSON.stringify(setingMessage.operationKey))
                    setingMessage.getMessage.teamKey = JSON.parse(JSON.stringify(setingMessage.teamKey))
                }
                $('.seting-showDiv').html(template('seting-' + setingMessage.page, {
                    message: setingMessage.getMessage,
                    InterfaceKey: setingMessage.getMessage.InterfaceKey,
                    operationKey: setingMessage.getMessage.operationKey,
                    teamKey: setingMessage.getMessage.teamKey
                }))
            },
            error: function (req) {

            }
        })
    } else if (setingMessage.page == 'gamesetingTest') {
        // 兼容性
        $.controlAjax({
            type: "get",
            url: '/api/site/game/testResults',
            showerror: true,
            success: function (res) {
                // -1未检测 0失败 1通过
                $('.seting-showDiv').html(template('seting-' + setingMessage.page, {
                    message: setingMessage.getMessage,
                    fileComplete: res.data.fileComplete,
                    runTest: res.data.runTest,
                    aresRunTest: res.data.aresRunTest,
                    modRunTest: res.data.modRunTest,
                }))
                if (res.data.fileComplete == 0 || res.data.runTest == 0 || res.data.aresRunTest == 0 || res.data.modRunTest == 0) {
                    $('.gamesetingTest .J_gamesetingTest_customer').removeClass('hide')
                    $('.gamesetingTest .seting-showDivspop .gamesetingTest-footborder').removeClass('hide')
                }
                let attention = getQueryString('attention')
                if (attention && setingMessage.testshow) {
                    $('.J_gamesetingTest_runcheck').each(function () {
                        if ($(this).attr('data-code') == attention) {
                            $(this).addClass('gx-btn-Attention')
                        }
                    })
                }
            },
            error: function (req) {

            }
        })
    } else if (setingMessage.page == 'platformsetingLanguage') {
        let languagehtml = '简体中文'
        if (localStorage.getItem('LBWlanguage')) {
            comOption.LBWlanguage.map((item) => {
                if (item.code == localStorage.getItem('LBWlanguage')) {
                    languagehtml = item.name
                }
            })
        }

        let GHharmonious = false
        if (localStorage.getItem('GHharmonious')) {
            GHharmonious = true
        }
        $('.seting-showDiv').html(template('seting-' + setingMessage.page, {
            message: setingMessage.getMessage,
            language: languagehtml,
            GHopen: GHharmonious,
            LBWlanguage: comOption.LBWlanguage
        }))
    } else {
        $('.seting-showDiv').html(template('seting-' + setingMessage.page, { message: setingMessage.getMessage }))
        $('.seting-kt-icon').tooltip()
    }
    $('.seting-showDiv .seting-gx-menu').gxmenu({
        height: "24px",
        top: "24px",
        clickHide: true
    }, function (e) {

    })
}
// 菜单点击
$(document).on('click', '.seting-list-li,.J_showhotkey', function () {
    var $this = $(this)
    let code = $this.attr('code')
    setingMessage.page = code
    pageActive()
})
// 菜单缩放
$(document).on('click', '.seting-list-ul', function () {
    var $this = $(this)
    let codename = $this.attr('name')
    if ($this.hasClass('open')) {
        $this.removeClass('open')
        $('.seting-list-li').each(function () {
            if ($(this).attr('name') == codename) {
                $(this).animate({ height: '0px' }, 200)
                $(this).css({ margin: '0px 0px' })
            }
        })
    } else {
        $this.addClass('open')
        $('.seting-list-li').each(function () {
            if ($(this).attr('name') == codename) {
                $(this).animate({ height: '30px' }, 200)
                $(this).css({ margin: '9px 0px' })
            }
        })
    }
})
// 选择分辨率
$(document).on('click', '.gamesetingMove-GameScreen', function () {
    var $this = $(this)
    let code = $this.attr('data-code')
    setingMessage.getMessage.gameScreen = code
    $this.closest('.seting-gx-menu').find('.gx-menu-in').text(code)
    changeSeting('gamesetingMove')
    $('.seting-gamesetting-after-chack-btn').removeClass('hide')
})

// 选择分辨率
$(document).on('click', '.gamesetingMove-resolvingtype', function () {
    var $this = $(this)
    let code = $this.attr('data-code')
    let name = $this.attr('data-name')
    setingMessage.getMessage.windowMode = code
    $this.closest('.seting-gx-menu').find('.gx-menu-in').text(name)
    changeSeting('gamesetingMove')
    $('.seting-gamesetting-after-chack-btn').removeClass('hide')
})
// 选择兼容模式
$(document).on('click', '.gamesetingMove-renderMode', function () {
    var $this = $(this)
    let code = $this.attr('data-code')
    let name = $this.attr('data-name')
    setingMessage.getMessage.renderMode = code
    $this.closest('.seting-gx-menu').find('.gx-menu-in').text(name)
    changeSeting('gamesetingMove')
    $('.seting-gamesetting-after-chack-btn').removeClass('hide')
})

// 选择CPU
$(document).on('click', '#gamesetingMove-cpu1,#gamesetingMove-cpu2', function () {
    var $this = $(this)
    setingMessage.getMessage.singleCpu = parseBoolean($this.val())
    changeSeting('gamesetingMove')
})
// 选择弹幕模式
$(document).on('click', '#gamesetingMove-dumu1,#gamesetingMove-dumu2,#gamesetingMove-dumu3,#gamesetingMove-dumu4', function () {
    var $this = $(this)
    let sucDom = $('.seting-showDiv').find('.seting-showDivspop-success')
    setingMessage.getMessage.dumu = $this.val()
    localStorage.setItem('Ra_dumuType', $this.val())
    sucDom.animate({ 'opacity': 1 }, 200)
    setingMessage.alertDomFun = setTimeout(function () {
        sucDom.animate({ 'opacity': 0 }, 200)
    }, 3000)
})
// 屏幕滚动
$(document).on('change', '.gamesetingMove-scrollRate', function () {
    var $this = $(this)
    if ($this.val() == setingMessage.getMessage.scrollRate) {
        return false
    }
    setingMessage.getMessage.scrollRate = $this.val()
    changeSeting('gamesetingMove')
})
// 个性化
$(document).on('change', '.gamesetingMove-checkbox', function () {
    var $this = $(this)
    var name = $this.attr('name')
    var code = $this.is(':checked')
    setingMessage.getMessage[name] = code
    changeSeting('gamesetingMove')
})
// 声音设置
$(document).on('change', '.volume-range', function () {
    var $this = $(this)
    var code = $this.attr('data-code')
    var type = $this.attr('data-type')
    let valuecode = Number($this.val())
    if (valuecode / 10 == setingMessage.getMessage[code]) {
        return false
    }
    setingMessage.getMessage[code] = (valuecode / 10).toString()
    changeSeting(type)
})
// 声音设置
$(document).on('click', '.gamesetingVoice .seting-page-list-item svg,.platformsetingVoice .seting-page-list-item svg', function () {
    var $this = $(this)
    let gxrange = $this.closest('.seting-page-list-item').find('.gx-range')
    var code = gxrange.attr('data-code')
    var type = gxrange.attr('data-type')
    let valuecode = Number(0)
    gxrange.val(valuecode)
    gxrange.trigger('input')
    if (valuecode / 10 == setingMessage.getMessage[code]) {
        return false
    }
    setingMessage.getMessage[code] = (valuecode / 10).toString()
    changeSeting(type)
})
// 隐私设置
$(document).on('change', '.platformsetingPrivate-checkbox', function () {
    var $this = $(this)
    var code = $this.attr('data-code')
    let valuecode = $(this).is(':checked') ? 'Y' : 'N'
    if (code == 'battle_switch_user_authenticate') {
        valuecode = $(this).is(':checked') ? 'N' : 'Y'
    }
    $.star({
        type: 'POST',
        url: '/community-user/user/config/v1/set',
        showerror: true,
        data: {
            name: code,
            value: valuecode,
            source: 1,
        },
        success: function (res) {
            if (code == 'battle_switch_user_authenticate') {
                let customField = customFieldFun('encryption', {
                    avatarFrame: userInfo.avatarFrame,
                    authenticateType: userInfo.authenticateType,
                    authenticateContent: userInfo.authenticateContent,
                    isAuthenticate: !$this.is(':checked')
                })
                ipcRenderer.send('Main', {
                    msgType: "ModfiyUserInfo",
                    jsonInfo: {
                        NickName: userInfo.username,
                        Avatar: userInfo.avatar,
                        customField: customField
                    }
                });
            }
            if (code == 'battle_switch_user_transposition') {
                if (valuecode == 'Y') {
                    localStorage.setItem('battle_switch_user_transposition', 1)
                } else if (valuecode == 'N') {
                    localStorage.removeItem('battle_switch_user_transposition');
                }
            }
        },
        error: function (req) {

        }
    });

})
// 游戏工具
$(document).on('change', '.othersetingTool-checkbox', function () {
    var $this = $(this)
    var name = $this.attr('data-name')
    var code = $this.is(':checked')
    if (name == 'blindness') {
        if (code) {
            code = 1
            $('.blindness-radio').eq(0).prop('checked', true)
        } else {
            code = 0
            $('.blindness-radio').prop('checked', false)
        }
    }
    setingMessage.getMessage[name] = code
    changeSeting('othersetingTool')
    if (name.indexOf('doubleT') != -1) {
        if (code) {
            $this.closest('.media').find('.doubleT-close').removeClass('hide')
            $this.closest('.media').find('.doubleT-in').addClass('hide')
        } else {
            $this.closest('.media').find('.doubleT-close').addClass('hide')
            $this.closest('.media').find('.doubleT-in').removeClass('hide')
        }
    }
})
// 色盲
$(document).on('change', '.blindness-radio', function () {
    var $this = $(this)
    var code = $this.val()
    $('.othersetingTool-checkbox').each(function () {
        if ($(this).attr('data-name') == 'blindness') {
            $(this).prop('checked', true)
            $(this).closest('.gx-switch').addClass('open')
        }
    })
    setingMessage.getMessage['blindness'] = code
    changeSeting('othersetingTool')
})
// 连点器
var pointstime = null
$(document).on('keyup', '.othersetingTool-points', function (e) {
    clearTimeout(pointstime)
    var $this = $(this)
    pointstime = setTimeout(function () {
        var $obj = $('.othersetingTool-points')
        let nums = parseInt($obj.val().replace(/[^\d]/g, '')) || 5
        if (nums < 5) {
            nums = 5
        } else if (nums > 99) {
            nums = 99
        }
        $this.val(nums)
        setingMessage.getMessage['clickCounts'] = nums
        changeSeting('othersetingTool')
    }, 500)
})
// 文件校验
$(document).on('click', '.J_gamesetingTest_check', function () {
    if (localStorage.getItem('isdetails')) {
        showtoast({
            message: translatesrting('房间中无法检测文件完整性')
        })
        return false
    }
    saveMessage('gameSeting', { fileCheck: true })
    $('.setingmodalclose').click()
})
// 修复工具
$(document).on('click', '.J_gamesetingTest_tool', function () {
    $.controlAjax({
        type: 'GET',
        url: '/Login/Repair/',
        success: function (res) {
        },
        error: function (req) {
        }
    })
    $('.setingmodalclose').click()
})
// 游戏运行测试
$(document).on('click', '.J_gamesetingTest_runcheck,.seting-gamesetting-after-chack-btn', function () {
    var $this = $(this)
    let code = $this.attr('data-code')
    if ($this.attr('data-type') == 'gamesetingMove') {
        setingMessage.isingamesetingMoveTest = true
    }
    if ($this.hasClass('gx-btn-Attention')) {
        setingMessage.testshow = false
    }
    if (setingMessage.checkloading) {
        showtoast({
            message: translatesrting('检测中')
        })
        return false
    }
    if (code == 'mode') {
        showtoast({
            message: translatesrting('敬请期待')
        })
        return false
    }
    if (!setingMessage.getMessage.gamePath || setingMessage.getMessage.gamePath == '') {
        $.controlAjax({
            type: "get",
            url: '/api/site/settings/info/',
            showerror: true,
            success: function (res) {
                if (!res.data.gamePath || res.data.gamePath == '') {
                    showtoast({
                        message: translatesrting('请先设置游戏执行文件')
                    })
                    $('.seting-list-li').each(function () {
                        if ($(this).attr('code') == 'gamesetingMove') {
                            $(this).click()
                        }
                    })
                } else {
                    $this.gxbtn('loading')
                    runcheck(code)
                }
            },
            error: function (req) {

            }
        })
        return false
    }
    $this.gxbtn('loading')
    runcheck(code)
})
function runcheck(code) {
    let type = 0
    // mode game ares testType = 正常游戏 0，Ares = 1 ,fileChek =2,修复工具 =3
    if (code == 'game') {
        type = 0
    }
    if (code == 'ares') {
        type = 1
    }
    setingMessage.checkloading = true
    $.controlAjax({
        type: "POST",
        url: '/api/site/game/test/',
        showerror: true,
        data: {
            testType: type,
            toMain: false
        },
        success: function (res) {
            ipcRenderer.send('Main', {
                msgType: "WindowSize",
                jsonInfo: {
                    window: 'settingmodal',
                    size: 'min'
                }
            });
        },
        error: function (req) {

        }
    })
}
// 客服
$(document).on('click', '.J_gamesetingTest_customer', function () {
    saveMessage('gameSeting', { customer: true })
    $('.setingmodalclose').click()
})
// 选择语言
$(document).on('click', '.platformsetingLanguage-platformseting', function () {
    var $this = $(this)
    let changelanguage = $this.attr('data-code')
    let name = $this.attr('data-name')
    $this.closest('.seting-gx-menu').find('.gx-menu-in').text(name)
    let locallanguage = localStorage.getItem('LBWlanguage') || 'zh'

    if (locallanguage != changelanguage) {
        localStorage.setItem('LBWlanguage', changelanguage)
        let LBWlanguage = localStorage.getItem('LBWlanguage') || 'zh'
        if (LBWlanguage == 'en') {
            LBWlanguage = 'en_US'
        }
        var devheaders = {
            'Content-type': 'application/json',
            'device_id': $.cookie('deviceId'),
            'device': 5,
            'platform': 3,
            'language': LBWlanguage,
            'access_token': userInfo.accessToken,
            'app_version_code': getSaveMessage('msgType21').version || ""
        }
        let customField = customFieldFun('encryption', {
            avatarFrame: userInfo.avatarFrame,
            authenticateType: userInfo.authenticateType,
            authenticateContent: userInfo.authenticateContent,
            isAuthenticate: userInfo.isAuthenticate
        })
        localStorage.removeItem('translateJsonNeed')
        $.controlAjax({
            type: "POST",
            url: '/api/login/success/',
            showerror: true,
            data: {
                customField: customField,
                isAnchor: userInfo.anchor,
                nickName: userInfo.username,
                avatar: userInfo.avatar,
                userName: userInfo.mobile,
                userId: userInfo.userId,
                token: userInfo.accessToken,
                headers: JSON.stringify(devheaders)
            },
            success: function (res) {
                window.location.href = "/Modal/setting?page=" + 'platformsetingLanguage'
                fetchSeting(1, 'platformsetingLanguage')
                localStorage.setItem('SetLBWlanguage', changelanguage);
            },
            error: function (req) {

            }
        })
    }
})
// GH和谐
$(document).on('change', '.platformsetingLanguage-GH', function () {
    var $this = $(this)
    var code = $this.is(':checked')
    if (code) {
        localStorage.setItem('GHharmonious', true)
    } else {
        localStorage.removeItem('GHharmonious')
    }
    fetchSeting(1, 'platformsetingLanguage')
    window.location.href = "/Modal/setting?page=" + 'platformsetingLanguage'
    localStorage.setItem('SetLBWlanguage', 1);
})



// 上传修改
function changeSeting(type) {
    clearTimeout(setingMessage.changeSetingfun[type])
    if (type == 'gamesetingMove') {
        setingMessage.changeSetingfun[type] = setTimeout(function () {
            let scrollRate = setingMessage.scrollRate[setingMessage.getMessage.scrollRate]
            let sendarray = {
                GamePath: setingMessage.getMessage.GamePath,
                gameScreen: setingMessage.getMessage.gameScreen,
                windowMode: setingMessage.getMessage.windowMode,
                renderMode: setingMessage.getMessage.renderMode,
                singleCpu: setingMessage.getMessage.singleCpu,
                scrollRate: scrollRate,
                skipScoreScreen: setingMessage.getMessage.skipScoreScreen,
                tooltips: setingMessage.getMessage.tooltips,
                showHiddenObjects: setingMessage.getMessage.showHiddenObjects,
                targetLines: setingMessage.getMessage.targetLines,
            }
            fetchSeting(sendarray, type, '/api/site/game/personalize')
        }, 500)
    } else if (type == 'gamesetingVoice') {
        setingMessage.changeSetingfun[type] = setTimeout(function () {
            let sendarray = {
                scoreVolume: setingMessage.getMessage.scoreVolume,
                soundVolume: setingMessage.getMessage.soundVolume,
                voiceVolume: setingMessage.getMessage.voiceVolume,
            }
            fetchSeting(sendarray, type, '/api/site/game/volume/')
        }, 500)
    } else if (type == 'platformsetingVoice') {
        let temp_json = getSaveMessage('ClientVolume')
        temp_json.clientBgm.volume = setingMessage.getMessage.clientBgm
        temp_json.clientSound.volume = setingMessage.getMessage.clientSound
        temp_json.globalVolume.volume = setingMessage.getMessage.globalVolume
        temp_json.clientBgm.volume.mute = false
        temp_json.clientSound.volume.mute = false
        temp_json.globalVolume.volume.mute = false
        saveMessage('ClientVolume', temp_json)
        volumeFn()
        $('#Audio_click')[0].play()
        fetchSeting(1, type)
    } else if (type == 'othersetingTool') {
        setingMessage.changeSetingfun[type] = setTimeout(function () {
            let sendarray = {
                clickCounts: setingMessage.getMessage.clickCounts,
                clicktorOpen: setingMessage.getMessage.clicktorOpen,
                doubleT: setingMessage.getMessage.doubleT,
                switchViewing: setingMessage.getMessage.switchViewing,
                reservedNumber: setingMessage.getMessage.reservedNumber,

                doubleTYR: setingMessage.getMessage.doubleTYR,
                switchViewingYR: setingMessage.getMessage.switchViewingYR,
                reservedNumberYR: setingMessage.getMessage.reservedNumberYR,

                doubleTClassic: setingMessage.getMessage.doubleTClassic,
                switchViewingClassic: setingMessage.getMessage.switchViewingClassic,
                reservedNumberClassic: setingMessage.getMessage.reservedNumberClassic,

                blockWinKey: setingMessage.getMessage.blockWinKey,
                blindness: setingMessage.getMessage.blindness,
                blockT: setingMessage.getMessage.blockT
            }
            fetchSeting(sendarray, type, '/api/site/game/tool')
        }, 500)
    }
}
// 上传修改接口
function fetchSeting(sendarray, type, opsturl) {
    let alertitem = $('.' + type).find('.seting-showDivspop')
    let errorDom = alertitem.find('.seting-showDivspop-error')
    let sucDom = alertitem.find('.seting-showDivspop-success')
    clearTimeout(setingMessage.alertDomFun)
    errorDom.animate({ 'opacity': 0 }, 200)
    sucDom.animate({ 'opacity': 0 }, 200)
    if (type == 'platformsetingVoice') {
        sucDom.animate({ 'opacity': 1 }, 200)
        setingMessage.alertDomFun = setTimeout(function () {
            sucDom.animate({ 'opacity': 0 }, 200)
        }, 3000)
        return false
    }
    if (type == 'platformsetingVoice') {
        sucDom.animate({ 'opacity': 1 }, 200)
        setingMessage.alertDomFun = setTimeout(function () {
            sucDom.animate({ 'opacity': 0 }, 200)
        }, 3000)
        return false
    }
    if (type == 'platformsetingLanguage') {
        sucDom.animate({ 'opacity': 1 }, 200)
        setingMessage.alertDomFun = setTimeout(function () {
            sucDom.animate({ 'opacity': 0 }, 200)
        }, 3000)
        return false
    }
    $.controlAjax({
        type: "POST",
        url: opsturl,
        showerror: false,
        data: sendarray,
        success: function (res) {
            sucDom.animate({ 'opacity': 1 }, 200)
            setingMessage.alertDomFun = setTimeout(function () {
                sucDom.animate({ 'opacity': 0 }, 200)
            }, 3000)
        },
        error: function (req) {
            errorDom.find('.seting-showDivspop-errortext').text(req.errorMsg)
            errorDom.animate({ 'opacity': 1 }, 200)
            setingMessage.alertDomFun = setTimeout(function () {
                errorDom.animate({ 'opacity': 0 }, 200)
            }, 3000)
        }
    })
}


// -----------------------------快捷键--------------------------------
// 加密
function key2num(key) {
    let num1 = 0
    let num2 = 0
    let num3 = 0
    if (key.length == 1) {
        setingMessage.key.map((item) => {
            if (key[0] == item.text) {
                num3 = item.keycode
            }
        })
    }
    if (key.length == 2) {
        setingMessage.key.map((item) => {
            key.map((keyitem, i) => {
                if (keyitem == item.text) {
                    if (i == 0) {
                        num2 = item.keycode
                    } else {
                        num3 = item.keycode
                    }
                }
            })
        })
    }
    if (key.length == 3) {
        setingMessage.key.map((item) => {
            key.map((keyitem, i) => {
                if (keyitem == item.text) {
                    if (i == 0) {
                        num1 = item.keycode
                    } else if (i == 1) {
                        num2 = item.keycode
                    } else {
                        num3 = item.keycode
                    }
                }
            })
        })
    }

    // // 2:ctrl 1:shift 4:alt
    if (num2 == 17) {
        num2 = 2
    }
    if (num2 == 16) {
        num2 = 1
    }
    if (num2 == 18) {
        num2 = 4
    }
    if (num1 == 17) {
        num1 = 2
    }
    if (num1 == 16) {
        num1 = 1
    }
    if (num1 == 18) {
        num1 = 4
    }
    return (((num1 | num2) << 8) + num3);
}
// 解密
function num2key(num) {
    var key1 = 0, key2 = 0;
    var key3 = num & 255
    var key_1and2 = (num & 1792) >> 8
    if (key_1and2 == 6) {
        key1 = 2
        key2 = 4
    }
    if (key_1and2 == 5) {
        key1 = 1
        key2 = 4
    }
    if (key_1and2 == 3) {
        key1 = 1
        key2 = 2
    }
    if (key_1and2 == 4) {
        key1 = 0
        key2 = 4
    }
    if (key_1and2 == 2) {
        key1 = 0
        key2 = 2
    }
    if (key_1and2 == 1) {
        key1 = 0
        key2 = 1
    }
    if (key_1and2 == 0) {
        key1 = 0
        key2 = 0
    }

    if (key2 == 2) {
        key2 = 17
    }
    if (key2 == 1) {
        key2 = 16
    }
    if (key2 == 4) {
        key2 = 18
    }
    if (key1 == 2) {
        key1 = 17
    }
    if (key1 == 1) {
        key1 = 16
    }
    if (key1 == 4) {
        key1 = 18
    }

    if (key1 == 0 && key2 == 0) {
        return key3.toString()
    }
    if (key1 == 0 && key2 != 0) {
        return key2 + '+' + key3
    }

    if (key1 != 0) {
        return key1 + '+' + key2 + '+' + key3
    }
}
$(document).on('click', '.J_hotkeyChange .hotkey-key', function () {
    var $this = $(this).closest('.J_hotkeyChange')
    let type = $this.attr('data-type')
    let code = $this.attr('data-code')
    setingMessage.luKey.type = type
    setingMessage.luKey.code = code
    if (!$this.hasClass('open')) {
        setingMessage.luKey.open = true
        setingMessage.luKey.keyArray = []
        $('.J_hotkeyChange').removeClass('open')
        $this.addClass('open')
    }

    $this.find('.seting-gx-menu').gxmenu({
        height: "24px",
        top: "20px",
        clickHide: true
    }, function (e) {

    })
})
// 关闭修改
$(document).on('click', '.J_hotkeyChange .hotkey-close', function () {
    $('.J_hotkeyChange').removeClass('open')
    setingMessage.luKey.open = false
})
// 清除快捷键
$(document).on('click', '.J_hotkeyChange .J_hotkeyClear', function () {
    var $this = $(this).closest('.J_hotkeyChange')
    $('.J_hotkeyChange').removeClass('open')
    setingMessage.luKey.open = false
    let sendarray = {
        hotkey: [setingMessage.luKey.code + '=' + '']
    }
    $this.find('.hotkey-key').text('')
    fetchSeting(sendarray, 'hotkey', '/api/site/game/hotkey/')
})
// 恢复默认
$(document).on('click', '.J_hotkeyChange .J_hotkeyRecovery', function () {
    var $this = $(this).closest('.J_hotkeyChange')
    $('.J_hotkeyChange').removeClass('open')
    setingMessage.luKey.open = false
    let oldcode = ''
    setingMessage[setingMessage.luKey.type].map((item) => {
        if (item.key == setingMessage.luKey.code) {
            oldcode = item.value
        }
    })
    let sendarray = {
        hotkey: [setingMessage.luKey.code + '=' + oldcode]
    }
    let codearray = num2key(oldcode).split('+')
    let keytext = codeTokey(codearray)
    $this.find('.hotkey-key').text(keytext.join(' + '))
    fetchSeting(sendarray, 'hotkey', '/api/site/game/hotkey/')
})
$(document).on('keyup', function (e) {
    clearTimeout(setingMessage.luKey.timefun)
    if (!setingMessage.luKey.open) {
        return false
    }
    setingMessage.luKey.keyArray.push(e.keyCode)
    if (e.altKey) {
        setingMessage.luKey.keyArray.push(18)
    }
    if (e.ctrlKey) {
        setingMessage.luKey.keyArray.push(17)
    }
    if (e.shiftKey) {
        setingMessage.luKey.keyArray.push(16)
    }
    setingMessage.luKey.timefun = setTimeout(function () {
        inluKey()
    }, 200)
})
function inluKey() {
    setingMessage.luKey.open = false
    $('.J_hotkeyChange').removeClass('open')
    // 数据处理
    // 去重
    let new_arr = [];
    for (var i = 0; i < setingMessage.luKey.keyArray.length; i++) {
        var items = setingMessage.luKey.keyArray[i];
        //判断元素是否存在于new_arr中，如果不存在则插入到new_ar中
        if ($.inArray(items, new_arr) == -1) {
            new_arr.push(items);
        }
    }
    setingMessage.luKey.keyArray = new_arr
    if ($.inArray(32, setingMessage.luKey.keyArray) != -1) {
        return false
    }
    if (setingMessage.luKey.keyArray.length == 1) {
        if (setingMessage.luKey.keyArray[0] == 16 || setingMessage.luKey.keyArray[0] == 17 || setingMessage.luKey.keyArray[0] == 18) {
            return false
        }
    }
    if (setingMessage.luKey.keyArray.length > 3) {
        setingMessage.luKey.keyArray = setingMessage.luKey.keyArray.slice(0, 3)
    }
    if (setingMessage.luKey.keyArray.length == 2) {
        if ($.inArray(16, setingMessage.luKey.keyArray) == -1 && $.inArray(17, setingMessage.luKey.keyArray) == -1 && $.inArray(18, setingMessage.luKey.keyArray) == -1) {
            setingMessage.luKey.keyArray = setingMessage.luKey.keyArray.slice(0, 1)
        }
    }
    if (setingMessage.luKey.keyArray.length == 3) {
        let numtime = 0
        if ($.inArray(16, setingMessage.luKey.keyArray) != -1) {
            numtime = numtime + 1
        }
        if ($.inArray(17, setingMessage.luKey.keyArray) != -1) {
            numtime = numtime + 1
        }
        if ($.inArray(18, setingMessage.luKey.keyArray) != -1) {
            numtime = numtime + 1
        }
        if (numtime < 2) {
            setingMessage.luKey.keyArray = setingMessage.luKey.keyArray.slice(0, 1)
        }
    }

    let keytext = codeTokey(setingMessage.luKey.keyArray)
    if (keytext.length == 0) {
        return false
    }
    $('.J_hotkeyChange').each(function () {
        if ($(this).attr('data-code') == setingMessage.luKey.code) {
            $(this).find('.hotkey-key').text(keytext.join('+'))
        }
    })
    let getkey2num = key2num(keytext)
    setingMessage.getMessage[setingMessage.luKey.type].map((item) => {
        if (item.key == setingMessage.luKey.code) {
            item.keyCode = keytext
            item.value = getkey2num
        }
    })
    let sendarray = {
        hotkey: [setingMessage.luKey.code + '=' + getkey2num]
    }
    // 全局去重
    let twokey = []
    setingMessage.getMessage.InterfaceKey.map((item) => {
        if (item.value == getkey2num && item.key != setingMessage.luKey.code) {
            twokey.push(item.key)
        }
    })
    setingMessage.getMessage.operationKey.map((item) => {
        if (item.value == getkey2num && item.key != setingMessage.luKey.code) {
            twokey.push(item.key)
        }
    })
    setingMessage.getMessage.teamKey.map((item) => {
        if (item.value == getkey2num && item.key != setingMessage.luKey.code) {
            twokey.push(item.key)
        }
    })
    if (twokey.length > 0) {
        twokey.map((item) => {
            sendarray.hotkey.push(item + '=' + '')
            $('.J_hotkeyChange').each(function () {
                if ($(this).attr('data-code') == item) {
                    $(this).find('.hotkey-key').text('')
                }
            })
        })
    }
    fetchSeting(sendarray, 'hotkey', '/api/site/game/hotkey/')
}
function dellUserKey(type, res) {
    let dellarray = JSON.parse(JSON.stringify(setingMessage[type]))
    dellarray.map((item) => {
        let init = false
        res.map((resitem) => {
            if (item.key == resitem.key) {
                init = true
                let codearray = num2key(resitem.value).split('+')
                let keytext = codeTokey(codearray)
                item.value = resitem.value
                item.keyCode = keytext
            }
        })
        if (!init) {
            item.value = ''
            item.keyCode = []
        }
    })
    return dellarray
    // let len = InterfaceKey.length
    // InterfaceKey = $.map(InterfaceKey, function (v, i) {
    //     return InterfaceKey[len - 1 - i];
    // });
    // return InterfaceKey
}
function codeTokey(codearray) {
    let keytext = []
    if (codearray) {
        setingMessage.key.map((key) => {
            codearray.map((ankey) => {
                if (key.keycode == ankey) {
                    keytext.push(key.text)
                }
            })
        })
    }
    keytext.sort(function (a, b) {
        let elone = a.length
        let eltwo = b.length
        if (a == 'Shift') {
            return -1
        }
        if (a == 'Ctrl') {
            return -1
        }
        if (a == 'Alt') {
            return -1
        }
        if (elone > eltwo) return -1
        if (elone < eltwo) return 1
        return 0
    })
    return keytext
}
// 重置所有快捷键
$(document).on('click', '.J_hotkeyReset', function () {
    gxmodal({
        title: translatesrting('提示'),
        centent: '<div class="mt30" style="padding: 0px 22px;">' + translatesrting('确定重置所有快捷键') + '<div>',
        buttons: [
            {
                text: translatesrting('确定'),
                class: 'btn-middle btn-middle-blue',
                callback: function () {
                    $.controlAjax({
                        type: "get",
                        url: '/api/site/game/resetHotkey',
                        showerror: true,
                        success: function (res) {
                            pageActive()
                        },
                        error: function (req) {

                        }
                    })
                }
            },
            {
                text: translatesrting('取消'),
                class: 'btn-middle btn-style-ash',
                callback: function () {

                }
            }
        ]
    })
})
// -----------------------------快捷键--------------------------------


// 关闭
$(document).on('click', '.setingmodalclose', function () {
    window.location.reload()
    ipcRenderer.send('Main', {
        msgType: "Close",
        jsonInfo: {
            window: "settingmodal"
        }
    });
})

$(document).on('click', '.J_showDownload', function () {
    $.controlAjax({
        type: "get",
        url: '/api/lobby/room/user/status/',
        data: {
        },
        success: function (res) {
            if (res.data.inRoom) {
                let alertitem = $('.' + 'gamesetingMove').find('.seting-showDivspop')
                let errorDom = alertitem.find('.seting-showDivspop-error')
                clearTimeout(setingMessage.alertDomFun)
                errorDom.animate({ 'opacity': 0 }, 200)
                errorDom.find('.seting-showDivspop-errortext').text(translatesrting('游戏房间中无法修改游戏路径'))
                errorDom.animate({ 'opacity': 1 }, 200)
                setingMessage.alertDomFun = setTimeout(function () {
                    errorDom.animate({ 'opacity': 0 }, 200)
                }, 3000)
            } else {
                showWindowModal('downloadmodal', '/Modal/download')
                $('.setingmodalclose').click()
            }
        },
        error: function (req) {

        }
    })
})

$(document).on('click', '.backstage-radio', function () {
    var $this = $(this)
    saveMessage('backstageout', $this.val())
    changeSeting('platformsetingVoice')
})
$(document).on('click', '.notice-a', function (e) {
    e.preventDefault()
    var $this = $(this)
    var url = $this.attr('href')
    OpenWebUrl(url)
})