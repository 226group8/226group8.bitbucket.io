
// 翻译
$('.translate-modal-content').html(template('translate-modal-content-script'))
$('#DownGameModal').html(template('DownGameModal-script'))
$('#DownGameConfirmModal').html(template('DownGameModal-confirm-script'))

var updateMessage = {
    autoUrlLoading: false, // 扫描文件是否开启
    downloadType: '', // 1: 下载文件 2: 重新下载
    gameUrl: null,
    resolving: null,
    windowMode: null,
    skipScoreScreen: null,
    maxGameScreen: null,
    singleCpu: true,
    resolvingArray: [
        { size: '1920x1080', isFull: false },
        { size: '1440x900', isFull: false },
        { size: '1024x768', isFull: false },
        { size: '1366x768', isFull: false },
        { size: '1536x864', isFull: false },
        { size: '2560x1440', isFull: false },
        { size: '1600x900', isFull: false },
        { size: '2048x1152', isFull: false },
        { size: '1280x720', isFull: false },
        { size: '1280x800', isFull: false },
        { size: '1680x1050', isFull: false },
    ],
    resolvingtypeArray: [
        { name: '未设置', code: 'Default' },
        { name: '独占全屏显示', code: 'FullScreen' },
        { name: '窗口模式', code: 'Window' },
        { name: '无边框窗口模式', code: 'BorderlessWindow' }
    ],
    minsize: false,
    getsetting: null
}
$(function () {
    if (getQueryVariable('noclose') == 'true') {
        $('.header-window').addClass('hide')
        $('.J_settingClose').addClass('hide')
        settionerror('error', translatesrting('请先定位游戏或下载游戏'))
        setTimeout(function () {
            settionerror('hide')
        }, 3000)
    } else {
        $('.header-window').removeClass('hide')
        $('.J_settingClose').removeClass('hide')
    }
    $.ajax({
        type: "get",
        url: '/api/site/settings/info/',
        data: $.param({ gamePath: updateMessage.gameUrl, gameScreen: updateMessage.resolving }, true),
        success: function (res) {
            if (res.data) {
                updateMessage.gameUrl = res.data.gamePath.replace('\\\\', '\\')
                updateMessage.resolving = res.data.gameScreen
                updateMessage.windowMode = res.data.windowMode
                updateMessage.maxGameScreen = res.data.maxGameScreen
                var isInarray = false
                updateMessage.resolvingArray.map((item) => {
                    if (item.size == res.data.maxGameScreen) {
                        isInarray = true
                        item.isFull = true
                    }
                })
                if (!isInarray) {
                    updateMessage.resolvingArray.unshift({ size: res.data.maxGameScreen, isFull: true })
                }
                updateMessage.skipScoreScreen = res.data.skipScoreScreen
                $('.resolving-select-ul').html(template('resolving-select-ul-script', { options: updateMessage.resolvingArray }))
                if (updateMessage.resolving == updateMessage.maxGameScreen) {
                    $('.resolving-select .gx-menu-in').text(updateMessage.resolving + " (" + translatesrting('全屏适配') + ")")
                } else {
                    $('.resolving-select .gx-menu-in').text(updateMessage.resolving)
                }
                $('.J_fileGameUrl').val(updateMessage.gameUrl + "gamemd.exe")
                if (updateMessage.gameUrl == "" || updateMessage.gameUrl == null) {
                    saveMessage('noGameUrl', true)
                }
                $('.resolving-select').gxmenu({
                    height: "40px",
                    top: "40px",
                    clickHide: true
                }, function (e) {

                })
                $('.language-select').gxmenu({
                    height: "26px",
                    top: "26px",
                    clickHide: true
                }, function (e) {

                })
                let languagehtml = '简体中文'
                if (localStorage.getItem('LBWlanguage') == 'en') {
                    languagehtml = 'English'
                }
                $('.language-select .gx-menu-in').text(languagehtml)
                $('.resolvingType-select-ul').html(template('resolving-select-ul-script', { type: 1, options: updateMessage.resolvingtypeArray }))
                updateMessage.resolvingtypeArray.map((item) => {
                    if (item.code == updateMessage.windowMode) {
                        $('.resolvingType-select .gx-menu-in').text(translatesrting(item.name))
                    }
                })
                $('.resolvingType-select').gxmenu({
                    height: "27px",
                    top: "40px",
                    clickHide: true
                }, function (e) {

                })
                if (res.data.skipScoreScreen) {
                    $('#settingCheckbox').prop('checked', 'checked')
                }
                updateMessage.singleCpu = res.data.singleCpu
                $("input[name=singleCpu]").each(function () {
                    if ($(this).attr('value') == res.data.singleCpu.toString()) {
                        $(this).prop('checked', true)
                    } else {
                        $(this).prop('checked', '')
                    }
                })
                $('.setting-connector-bg').html(template('setting-connector-bg-script', {
                    message: res.data
                }))
                updateMessage.getsetting = res.data
                // if (res.data.clicktorOpen) {
                //     $('#connectorcheck').prop('checked', 'checked')
                //     $('.setting-connector-input').removeClass('hide')
                // } else {
                //     $('#connectorcheck').prop('checked', false)
                // }
                // $('.setting-connector-input input').val(res.data.clickCounts)
                // if (updateMessage.minsize) {
                $('.J_setting-autoUrl').click()
                // }
            }
        },
        error: function (req) {
            settionerror('error', req.errorMsg)
        }
    })
})
// 地址
$(document).on('change', '#J_fileGame', function (e) {
    var gameUrl = e.currentTarget.files[0].path
    $('.J_fileGameUrl').val(gameUrl + '\n')
    // updateMessage.gameUrl = gameUrl
})
// 地址
$(document).on('input', '.J_fileGameUrl', function () {
    updateMessage.gameUrl = $(this).val()
})
// 分辨率
$(document).on('click', '.resolving-a', function (e) {
    var $this = $(this)
    updateMessage.resolving = $this.attr('data-type')
    if (updateMessage.resolving == updateMessage.maxGameScreen) {
        $('.resolving-select .gx-menu-in').text(updateMessage.resolving + " (" + translatesrting('全屏适配') + ")")
    } else {
        $('.resolving-select .gx-menu-in').text(updateMessage.resolving)
    }
})
// 分辨率模式
$(document).on('click', '.resolvingtype-a', function (e) {
    var $this = $(this)
    $('.resolvingType-select .gx-menu-in').text($this.text())
    updateMessage.windowMode = $this.attr('data-type')
})
// 结算
$(document).on('change', '#settingCheckbox', function (e) {
    updateMessage.skipScoreScreen = $(this).prop('checked')
})
$(document).on('change', 'input[name=singleCpu]', function (e) {
    if ($(this).val() == 'false') {
        updateMessage.singleCpu = false
    } else {
        updateMessage.singleCpu = true
    }
})
// 提交
$(document).on('click', '.J_settingbtn', function () {
    var $this = $(this)
    if ($this.hasClass('gx-disabled')) {
        return false
    }
    if (!updateMessage.gameUrl || updateMessage.gameUrl == "") {
        settionerror('error', translatesrting('请先定位游戏或下载游戏'))
        return false
    } else {
        settionerror('hide')
    }
    $this.gxbtn('loading')
    let sendarray = {
        gamePath: updateMessage.gameUrl,
        gameScreen: updateMessage.resolving,
        windowMode: updateMessage.windowMode,
        renderMode: updateMessage.getsetting.renderMode,
        singleCpu: updateMessage.singleCpu,
        scrollRate: updateMessage.getsetting.scrollRate,
        skipScoreScreen: updateMessage.skipScoreScreen,
        tooltips: updateMessage.getsetting.tooltips,
        showHiddenObjects: updateMessage.getsetting.showHiddenObjects,
        targetLines: updateMessage.getsetting.targetLines,
    }
    $.controlAjax({
        type: "POST",
        url: '/api/site/game/personalize',
        showerror: false,
        data: sendarray,
        success: function (res) {
            $this.gxbtn('reset')
            if (res.success) {
                controlWindow('settingmodal', 'close')
                settionerror('hide')
            } else {

            }
        },
        error: function (req) {
            $this.gxbtn('reset')
            settionerror('error', req.errorMsg)
        }
    })
})
function settionerror(type, text) {
    if (type != 'hide') {
        $('.setting-error').text(text)
        $('.setting-error').addClass('showOpacity').addClass('animated fadeIn')
        setTimeout(() => {
            $('.setting-error').removeClass('fadeIn');
        }, 1000)
    } else {
        $('.setting-error').removeClass('showOpacity').removeClass('fadeIn')
    }
    if (type == 'error') {
        $('.setting-error').removeClass('alert-success')
        $('.setting-error').addClass('alert-error')
    } else if (type == 'success') {
        $('.setting-error').addClass('alert-success')
        $('.setting-error').removeClass('alert-error')
    }
}
// 关闭
$(document).on('click', '.J_settingClose', function () {
    ipcRenderer.send('Main', {
        msgType: "FindGamePath",
        jsonInfo: {
            status: false,
            flag: 1//0大厅 1setting
        }
    });
    ipcRenderer.send('Main', {
        msgType: "WindowSize",
        jsonInfo: {
            window: 'downloadmodal',
            size: 'hide'
        }
    });
})
// 开启检测游戏路径弹窗
$(document).on('click', '.J_setting-autoUrl', async function () {
    $.controlAjax({
        type: "get",
        url: '/api/lobby/room/user/status/',
        data: {
        },
        success: function (res) {
            // firstoutroom = false
            if (res.data.inRoom) {
                settionerror('error', translatesrting('游戏房间中无法修改游戏路径'))
            } else {
                ipcRenderer.send('Main', {
                    msgType: "ChangeWindowSize",
                    jsonInfo: {
                        window: 'downloadmodal',
                        width: 500,
                        height: 424,
                        isCenter: true
                    }
                });
                $('.J_game-list').removeClass('hide');
                $('.J_game-setup').addClass('hide')

                GetDefaultGamePath()
            }
        },
        error: function (req) {

        }
    })
})

// 获取默认游戏下载路径
function GetDefaultGamePath() {
    $.controlAjax({
        type: "get",
        url: '/api/lobby/game/defaultPath/',
        data: {},
        showerror: true,
        success: function (res) {
            updateMessage.gameUrl = res.data.path
            if (res.data.type == '0') {
                $('J_game-list .main .tisi').removeClass('hide');
                setupGameList([res.data.path], res.data.type)
                return false;
            }
            setupGameList([res.data.path], res.data.type)
        },
        error: function (req) {

        }
    })
}

// 关闭检测游戏路径弹窗
$(document).on('click', '.J_removeMask,.J_closure', async function () {
    // if ($('body').hasClass('minsize')) {
    if (updateMessage.minsize) {
        ipcRenderer.send('Main', {
            msgType: "ChangeWindowSize",
            jsonInfo: {
                window: 'downloadmodal',
                width: 1,
                height: 1,
                isCenter: true
            }
        });
        // $('body').css('opacity', 0)
        $('.settingmodalclose').click()
        return false;
    } else {
        DownLoadFile('2');
        ipcRenderer.send('Main', {
            msgType: "ChangeWindowSize",
            jsonInfo: {
                window: 'downloadmodal',
                width: 1,
                height: 1,
                isCenter: true
            }
        });
        // $('body').css('opacity', 0)
        $('.settingmodalclose').click()
        return false;
    }
    // if (updateMessage.minsize) {
    //     $('.settingmodalclose').click()
    //     return false;
    // }
    // ipcRenderer.send('Main', {
    //     msgType: "ChangeWindowSize",
    //     jsonInfo: {
    //         window: 'downloadmodal',
    //         width: 370,
    //         height: 395,
    //         isCenter: true
    //     }
    // });

    // $('.J_game-list').addClass('hide');
    // $('.J_game-setup').removeClass('hide')

    // DownLoadFile('2');
})


// 开启确认弹窗
$(document).on('click', '.J_position', async function () {
    $('#DownGameConfirmModal').modal('show');
})

// 关闭确认弹窗
$(document).on('click', '.J_onIcon', function () {
    $('#DownGameConfirmModal').modal('hide');
})

// 开始自动搜索
$(document).on('click', '.J_automaticSearch', function () {
    $('.J_automaticSearch').gxbtn('loading')
    DownLoadFile('2').then(res => {
        scanningGamesFile(true).then(reslut => {
            if (reslut) {
                $('.DownGameModal-search').removeClass('hide');
                $('.DownGameModal-content').addClass('hide');
            }
        }).catch(err => {
            // $('#DownGameConfirmModal').modal('hide');
        })
    })

})

// 结束自动搜索
$(document).on('click', '.J_CancelSearch', function () {
    scanningGamesFile(false).then(reslut => {
        if (reslut) {
            $('.DownGameModal-search').addClass('hide');
            $('.DownGameModal-content').removeClass('hide');
        }
    }).catch(err => {
        // settionerror('error', translatesrting(err))
    })
})

// 手动搜索
$(document).on('click', '.J_ManualSearch', function () {
    DownLoadFile('2').then(res => {
        $('#J_fileGame').click();
    })
})

// 手动搜索完成
$(document).on('change', '#J_fileGame', async function () {
    $('.J_ManualSearch').gxbtn('loading')
    const newGamePath = $(this)[0].files[0].path;
    var file = $(this)

    file.after(file.clone().val(""));

    file.remove();
    // 目录不能存在中文
    if (!DoesExistChinese(newGamePath)) {
        $('#DownGameConfirmModal').modal('hide');
        zherror(newGamePath)

        return settionerror('error', translatesrting('游戏路径存在中文，请移动游戏目录或重新安装'));
    }

    const data = await editGamePath(newGamePath.match(/(.*)\\/)[0], true);
    if (data) {
        controlWindow('settingmodal', 'hide')
        settionerror('hide')
        window.location.reload()
    } else {
        $('#DownGameConfirmModal').modal('hide')
    }
    updateMessage.gameUrl = newGamePath
})
$(document).on('hidden.bs.modal', '#DownGameConfirmModal', function () {
    $('.J_ManualSearch').gxbtn('reset')
    $('.J_automaticSearch').gxbtn('reset')
})
// 开始下载游戏
$(document).on('click', '.J_downGame', function () {
    ipcRenderer.send('Main', {
        msgType: "DownLoadFile",
        jsonInfo: {
            isCancel: true,
            Sign: 'game',
            type: '1'
        }
    });
    setTimeout(() => { startgamedowm() }, 1000)

})
async function startgamedowm() {
    const isSuccess = await DownLoadFile('1', updateMessage.gameUrl);
    if (isSuccess) {
        $('#games_list .strip').css('width', '0%')
        $('#games_list .Proportion').text('0%')
        $('#games_list .mb span').text('0MB')
        updateMessage.downloadType = '1';
        $('#games_list .down').addClass('hide');
        $('#games_list .Install').removeClass('hide');
        if (updateMessage.minsize) {
            $('body').addClass('minsize')
            $('.title-name').text(translatesrting('下载列表'))
            ipcRenderer.send('Main', {
                msgType: "ChangeWindowSize",
                jsonInfo: {
                    window: 'downloadmodal',
                    width: 375,
                    height: 135,
                    bottomRight: true
                }
            });
        }
    }
}
// 重新下载游戏
$(document).on('click', '.J_downloadAgain', async function () {

    // const isSuccess = await DownLoadFile('1', updateMessage.gameUrl);
    // if (isSuccess) {
    //     updateMessage.downloadType = '2';
    //     $('#games_list .down').addClass('hide');
    //     $('#games_list .Install').removeClass('hide');
    // }
    // 清除游戏路径
    $.controlAjax({
        type: "get",
        url: '/api/lobby/game/clearPath/',
        data: {},
        showerror: true,
        success: async function (res) {
            GetDefaultGamePath()
        },
        error: function (req) {

        }
    })

})

// 改变安装目录
$(document).on('click', '.select_file', async function () {
    ipcRenderer.send('Main', {
        msgType: "WindowSize",
        jsonInfo: {
            window: 'downloadmodal',
            size: "noalwaysOnTop"
        }
    });
    $.controlAjax({
        type: "get",
        url: '/api/lobby/game/chooseFile/',
        data: {},
        showerror: true,
        success: function (res) {

        },
        error: function (req) {

        }
    })
})

// 手动选择目录选择完成
// $(document).on('change', '#J_fileGame', async function () {
//     const newGamePath = $(this)[0].files[0].path;
//     // 目录不能存在中文
//     if (!DoesExistChinese(newGamePath)) {
//         $('#DownGameConfirmModal').modal('hide');
//         return settionerror('error', translatesrting('游戏路径存在中文，请移动游戏目录或重新安装'));
//     }

//     editGamePath(newGamePath.match(/(.*)\\/)[0]);
// })

/**
 * 搜索成功完成回调
 * @property { array } gamesPath 游戏路径
 * */
async function completeScan(gamesPath) {
    let newPath = gamesPath.match(/(.*)\\/)[0]
    const data = await editGamePath(newPath, true);
    if (data) {

    }
}

function setupGameList(gameList, type = 1) {
    let str = "";
    if (type == 0) {
        $('J_game-list .main .tisi').removeClass('hide');
        str += `
            <li>
                <div class="left">
                    <img src="/images/gamelogo/version3-80.png" alt="">
                </div>
                <div class="content">
                    <div class="game-name">${translatesrting('红警2基础文件(含原版、尤里、共辉)')}</div>
                    <div class="game-path">${gameList[0]}</div>
                </div>
                <div class="right">
                    <div class="down">
                        <button  class="J_downGame">
                            <svg t="1648110660878" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4445" width="200" height="200"><path d="M910.208 308.736a49.344 49.344 0 0 0-49.792 48.896v440.512H163.52V357.632a49.344 49.344 0 0 0-49.728-48.896 49.344 49.344 0 0 0-49.792 48.896v489.408c0 27.072 22.272 48.96 49.792 48.96h796.416c27.52 0 49.792-21.888 49.792-48.96V357.632a49.408 49.408 0 0 0-49.792-48.896z" p-id="4446"></path><path d="M312.896 700.224h398.208c27.52 0 49.792-21.888 49.792-48.896a49.344 49.344 0 0 0-49.792-48.96H312.896a49.344 49.344 0 0 0-49.792 48.96c0 27.008 22.272 48.896 49.792 48.896z m163.776-209.92c4.48 4.352 9.92 7.808 15.872 10.24 1.984 1.024 4.48 1.92 7.04 1.92a43.136 43.136 0 0 0 24.832 0c2.56 0 4.48-0.896 6.976-1.92a49.152 49.152 0 0 0 15.936-10.24l140.864-138.56a48.768 48.768 0 0 0 0-69.44 50.752 50.752 0 0 0-70.656 0l-55.744 55.296v-224.64a48.384 48.384 0 0 0-14.464-34.752A49.984 49.984 0 0 0 512 64a49.536 49.536 0 0 0-49.792 48.96v224.64l-55.68-55.296a50.816 50.816 0 0 0-70.72 0 48.768 48.768 0 0 0 0 69.44l140.8 138.56z" p-id="4447"></path></svg>

                            <span>${translatesrting('下载游戏')}</span>
                        </button>

                        <a class="select_file" href="javascript:;">${translatesrting('更改安装目录')}</a>
                    </div>

                    <div class="Install hide">
                        <div class="title-tisi"></div>
                        <div class="speed">
                            <span class="mb">
                                <span>0MB</span>
                            </span>
                            <span class="Proportion">0%</span>
                        </div>
                        <div class="schedule">
                            <div class="strip"></div>
                        </div>
                    </div>
                </div>
            </li>
        `
        $('#games_list').html(str)
        return false;
    }

    updateMessage.gameUrl = gameList[0];
    $('J_game-list .main .tisi').addClass('hide');
    $.each(gameList, function (index, item) {
        str += `
        <li>
            <div class="left">
                <img src="/images/gamelogo/version3-80.png" alt="">
            </div>
            <div class="content">                         
                <div class="game-name">${translatesrting('红警2基础文件(含原版、尤里、共辉)')}</div>
                <div class="game-path">${item}</div>
            </div>
            <div class="right">
                <div class="down">
                    <button tab-index="${index}" class="J_downGame hide">
                        <svg t="1648110660878" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4445" width="200" height="200"><path d="M910.208 308.736a49.344 49.344 0 0 0-49.792 48.896v440.512H163.52V357.632a49.344 49.344 0 0 0-49.728-48.896 49.344 49.344 0 0 0-49.792 48.896v489.408c0 27.072 22.272 48.96 49.792 48.96h796.416c27.52 0 49.792-21.888 49.792-48.96V357.632a49.408 49.408 0 0 0-49.792-48.896z" p-id="4446"></path><path d="M312.896 700.224h398.208c27.52 0 49.792-21.888 49.792-48.896a49.344 49.344 0 0 0-49.792-48.96H312.896a49.344 49.344 0 0 0-49.792 48.96c0 27.008 22.272 48.896 49.792 48.896z m163.776-209.92c4.48 4.352 9.92 7.808 15.872 10.24 1.984 1.024 4.48 1.92 7.04 1.92a43.136 43.136 0 0 0 24.832 0c2.56 0 4.48-0.896 6.976-1.92a49.152 49.152 0 0 0 15.936-10.24l140.864-138.56a48.768 48.768 0 0 0 0-69.44 50.752 50.752 0 0 0-70.656 0l-55.744 55.296v-224.64a48.384 48.384 0 0 0-14.464-34.752A49.984 49.984 0 0 0 512 64a49.536 49.536 0 0 0-49.792 48.96v224.64l-55.68-55.296a50.816 50.816 0 0 0-70.72 0 48.768 48.768 0 0 0 0 69.44l140.8 138.56z" p-id="4447"></path></svg>
                        <span>${translatesrting('下载游戏')}</span>
                    </button>

                    <button tab-index="${index}" class="J_downloadAgain ">
                        <svg t="1648110660878" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4445" width="200" height="200"><path d="M910.208 308.736a49.344 49.344 0 0 0-49.792 48.896v440.512H163.52V357.632a49.344 49.344 0 0 0-49.728-48.896 49.344 49.344 0 0 0-49.792 48.896v489.408c0 27.072 22.272 48.96 49.792 48.96h796.416c27.52 0 49.792-21.888 49.792-48.96V357.632a49.408 49.408 0 0 0-49.792-48.896z" p-id="4446"></path><path d="M312.896 700.224h398.208c27.52 0 49.792-21.888 49.792-48.896a49.344 49.344 0 0 0-49.792-48.96H312.896a49.344 49.344 0 0 0-49.792 48.96c0 27.008 22.272 48.896 49.792 48.896z m163.776-209.92c4.48 4.352 9.92 7.808 15.872 10.24 1.984 1.024 4.48 1.92 7.04 1.92a43.136 43.136 0 0 0 24.832 0c2.56 0 4.48-0.896 6.976-1.92a49.152 49.152 0 0 0 15.936-10.24l140.864-138.56a48.768 48.768 0 0 0 0-69.44 50.752 50.752 0 0 0-70.656 0l-55.744 55.296v-224.64a48.384 48.384 0 0 0-14.464-34.752A49.984 49.984 0 0 0 512 64a49.536 49.536 0 0 0-49.792 48.96v224.64l-55.68-55.296a50.816 50.816 0 0 0-70.72 0 48.768 48.768 0 0 0 0 69.44l140.8 138.56z" p-id="4447"></path></svg>
                        <span>${translatesrting('重新下载')}</span>
                    </button>

                    <a class="J_ManualSearch" href="javascript:;">${translatesrting('更改安装目录')}</a>
                </div>

                <div class="Install hide">
                    <div class="title-tisi"></div>
                    <div class="speed">
                        <span class="mb">
                            <span>0MB</span>
                        </span>
                        <span class="Proportion">0%</span>
                    </div>
                    <div class="schedule">
                        <div class="strip"></div>
                    </div>
                </div>
            </div>
        </li>
    `
    })
    $('#games_list').html(str)
}

/**
 * 开始/结束扫描游戏文件
 * @property { boolean } isStart 开始/关闭
 * @return   { boolean } 成功/失败
*/
function scanningGamesFile(isStart) {
    return new Promise((reslut, reject) => {
        if (isStart && !updateMessage.autoUrlLoading) {
            updateMessage.autoUrlLoading = true;
            ipcRenderer.send('Main', {
                msgType: "FindGamePath",
                jsonInfo: {
                    status: true,
                    flag: 1//0大厅 1setting
                }
            });
            reslut(true)
        } else if (!isStart && updateMessage.autoUrlLoading) {
            updateMessage.autoUrlLoading = false;
            ipcRenderer.send('Main', {
                msgType: "FindGamePath",
                jsonInfo: {
                    status: false,
                    flag: 1//0大厅 1setting
                }
            });
            reslut(true)
        } else {
            reject(`当前已经是${isStart ? '开启' : '关闭'}状态`)
        }
    })
}

/**
 * 开始下载/取消下载 游戏文件
 * @property { string }  downLoadType 下载或者取消 1:开始下载  2：取消下载
 * @return   { boolean } 成功/失败
 * */
function DownLoadFile(downLoadType, saveDir = null, Sign = 'game') {
    return new Promise((reslut, reject) => {
        switch (downLoadType) {
            case '1':
                if (DoesExistChinese(updateMessage.gameUrl)) {
                    $('.J_game-list .title-tisi').text(translatesrting('下载中'))
                    $('.footer').addClass('hide')
                    ipcRenderer.send('Main', {
                        msgType: "DownLoadFile",
                        jsonInfo: {
                            isCancel: false,
                            Sign: Sign,
                            saveDir,
                            type: '1',
                        }
                    });
                    reslut(true);
                    settionerror('hide')
                } else {
                    reslut(false)
                    settionerror('error', translatesrting('游戏路径存在中文，请移动游戏目录或重新安装'))
                }

                break;
            case '2':
                if (updateMessage.downloadType != "") {
                    ipcRenderer.send('Main', {
                        msgType: "DownLoadFile",
                        jsonInfo: {
                            isCancel: true,
                            Sign: Sign,
                            saveDir,
                            type: '1'
                        }
                    });
                }
                reslut(true);

            default:
                reject(false)
                break;
        }
    })
}

// 监听
ipcRenderer.on('WebIpc', (event, message) => {
    var jsonmessage = JSON.parse(message.jsonInfo)
    if (message.msgType == 32) { // 扫描游戏完成
        settionerror('success', translatesrting('游戏路径搜索结束，请查看'))
        updateMessage.autoUrlLoading = false;
        $('.DownGameModal-search').addClass('hide');
        $('.DownGameModal-content').removeClass('hide');
        $('#DownGameConfirmModal').modal('hide');

        if (jsonmessage && jsonmessage.length > 0) {
            completeScan(jsonmessage[0])
        } else {
            $('J_game-list .main .tisi').removeClass('hide');
            settionerror('error', translatesrting('未搜索到游戏客户端，请下载游戏'))
        }
        setTimeout(() => {
            settionerror('hide')
        }, 2500);
    } else if (message.msgType == 20) { // 下载完成
        $('#games_list .strip').css('width', jsonmessage.nowPercentage + '%')
        $('#games_list .Proportion').text(jsonmessage.nowPercentage + '%')
        $('#games_list .mb span').text((jsonmessage.nowReceiveNum / 1024 / 1024).toFixed(4) + 'MB')
        if (jsonmessage.nowReceiveNum <= 0) {
            $('#games_list .mb span').addClass('hide')
        }
        if (jsonmessage.type == 1) {
            $('.J_game-list .title-tisi').text(translatesrting('下载中'))
            $('.footer').addClass('hide')
        } else if (jsonmessage.type == 2) {
            $('.J_game-list .title-tisi').text(translatesrting('解压中'))
        }

        if (Number(jsonmessage.nowPercentage) > 100) {
            $('.footer').removeClass('hide')
            let savePath = jsonmessage.savePath.match(/(.*)\\/)[0];
            updateMessage.gameUrl = savePath
            settionerror('success', translatesrting('游戏下载完成'))
            $('#games_list .down').removeClass('hide');
            $('#games_list .Install').addClass('hide');
            $('#games_list .strip').css('width', '0%')
            $('.J_game-list .game-path').text(savePath);
            $('#games_list .Proportion').text('0%')
            $('#games_list .mb span').text('0MB')
            if (updateMessage.downloadType == '1') {
                editGamePath(savePath).then(data => {
                    if (data) {
                        setupGameList([savePath, '1'])
                        if (updateMessage.minsize) {
                            localStorage.setItem('newHelp', 'gamedownend');
                        }
                        $('.settingmodalclose').click()
                    }
                })
            }
        } else if (Number(jsonmessage.nowPercentage) == -1) {
            $('.footer').removeClass('hide')
            $('#games_list .down').removeClass('hide');
            $('#games_list .Install').addClass('hide');
            if (jsonmessage.type != 3) {
                settionerror('error', translatesrting('游戏下载失败'))
            }
            $('#games_list .strip').css('width', '0%')
            $('#games_list .Proportion').text('0%')
            $('#games_list .mb span').text('0MB')
            setTimeout(() => {
                settionerror('hide')
            }, 2500);
        }
        if (updateMessage.minsize && Number(jsonmessage.nowPercentage) == -1) {
            $('body').removeClass('minsize')
            let widthsize = 500
            let heightsize = 424
            ipcRenderer.send('Main', {
                msgType: "ChangeWindowSize",
                jsonInfo: {
                    window: 'downloadmodal',
                    width: widthsize,
                    height: heightsize,
                    isCenter: true
                }
            });
            localStorage.setItem('newHelp', 'gamedownerror');
        }
        if (updateMessage.minsize && jsonmessage.type == 1 && Number(jsonmessage.nowPercentage) < 100 && Number(jsonmessage.nowPercentage) != -1) {
            localStorage.setItem('newHelp', 'gamedownstart');
        }
    } else if (message.msgType == 37) { // 文件夹选择完成回调
        if (jsonmessage != "") {
            if (DoesExistChinese(jsonmessage)) {
                $('.J_game-list .game-path').text(jsonmessage);
                updateMessage.gameUrl = jsonmessage
                settionerror('hide')
            } else {
                zherror(jsonmessage)
                settionerror('error', translatesrting('游戏路径存在中文，请移动游戏目录或重新安装'))
            }
        }



        // 置顶弹窗
        ipcRenderer.send('Main', {
            msgType: "WindowSize",
            jsonInfo: {
                window: 'downloadmodal',
                size: "alwaysOnTop"
            }
        });
        // 获取弹窗焦点
        ipcRenderer.send('Main', {
            msgType: "ShowModal",
            jsonInfo: {
                window: 'downloadmodal',
                url: '/Modal/setting',
                width: 370,
                height: 395,
                skipTaskbar: true
            }
        });
    } else if (message.msgType == 39) {
        if (Number(jsonmessage) == -1) {
            $('.settingmodalclose').click()
            localStorage.setItem('newHelp', 'repairerror');
        } else {
            let prenm = Number(jsonmessage) * 100
            if (prenm == 100) {
                localStorage.setItem('newHelp', 'repairend');
                $('.settingmodalclose').click()
            }
            $('.min-downmodal-bro').css('width', prenm + '%')
            $('.min-downmodal-bronum').text(prenm.toFixed(2).replaceAll('.00', '') + '%')
        }
    }
});


// 修改游戏路径
function editGamePath(gameUrl, isclick) {
    return new Promise((reslut, reject) => {
        let sendarray = {
            gamePath: gameUrl,
            gameScreen: updateMessage.resolving,
            windowMode: updateMessage.windowMode,
            renderMode: updateMessage.getsetting.renderMode,
            singleCpu: updateMessage.singleCpu,
            scrollRate: updateMessage.getsetting.scrollRate,
            skipScoreScreen: updateMessage.skipScoreScreen,
            tooltips: updateMessage.getsetting.tooltips,
            showHiddenObjects: updateMessage.getsetting.showHiddenObjects,
            targetLines: updateMessage.getsetting.targetLines,
            cloneGame: isclick ? true : false
        }
        $.controlAjax({
            type: "POST",
            url: '/api/site/game/personalize',
            data: sendarray,
            success: function (res) {
                updateMessage.gameUrl = gameUrl
                $('.J_game-list .game-path').text(gameUrl);
                GetDefaultGamePath()
                reslut(true)
                $('.settingmodalclose').click()
            },
            error: function (req) {
                reslut(false)
                settionerror('error', req.errorMsg)
                $('body').removeClass('minsize')
                let widthsize = 500
                let heightsize = 424
                ipcRenderer.send('Main', {
                    msgType: "ChangeWindowSize",
                    jsonInfo: {
                        window: 'downloadmodal',
                        width: widthsize,
                        height: heightsize,
                        isCenter: true
                    }
                });
            }
        })
    })
}

/**
 * 字符串是否存在中文
 * @property { string } string 路径
 * @return { boolean }
*/

function downGame(type, num, ZipFlag) {
    if (type == 'pre') {
        if (num <= 100) {
            if (ZipFlag == 'game') {
                $('#DownGameModal .DownGameModal-pre .DownGameModal-bor').css('width', num + '%')
                $('#DownGameModal .DownGameModal-num').text(num + '%')
                if (num == 100) {
                    $("#DownGameModal .DownGameText").css('display', 'none')
                    $("#DownGameModal .DownGameZip").css('display', 'block')
                    $("#DownGameModal .DownGameModal-pre").css('display', 'none')
                    $("#DownGameModal .DownGameModal-zip").css('display', 'inline-block')
                    $("#DownGameModal .J_DownGameModal-close").css({ pointerEvents: 'none', background: '#A8A8A8' })
                }
            } else {
                $('#DownGameModal .DownGameModal-zip .DownGameModal-bor').css('width', num + '%')
                $('#DownGameModal .DownGameModal-num').text(num + '%')
            }
        }
    } else if (type == 'over') {
        $("#DownGameModal .J_DownGameModal-close").css({ pointerEvents: 'auto', background: 'rgba(35, 133, 240, 1)' })
        ResetDownload()
        $('#DownGameModal').modal('hide')
        settionerror('success', translatesrting('游戏下载完成'))
        updateMessage.gameUrl = num
        $('.J_fileGameUrl').val(num)
    } else if (type == 'error') {
        $("#DownGameModal .J_DownGameModal-close").css({ pointerEvents: 'auto', background: 'rgba(35, 133, 240, 1)' })
        $('#DownGameModal').modal('hide')
        settionerror('error', translatesrting('游戏下载失败'))
        ResetDownload()
    }
}

function ResetDownload() {
    $("#DownGameModal .DownGameText").css('display', 'block')
    $("#DownGameModal .DownGameZip").css('display', 'none')
    $("#DownGameModal .DownGameModal-pre").css('display', 'inline-block')
    $("#DownGameModal .DownGameModal-zip").css('display', 'none')
    $('#DownGameModal .DownGameModal-pre .DownGameModal-bor').css('width', '0%')
    $('#DownGameModal .DownGameModal-zip .DownGameModal-bor').css('width', '0%')
}

$(document).on('click', '.setting-modal-header-nav-item', function () {
    var $this = $(this);
    var indexs = $this.attr('data-index')
    $('.setting-modal-header-nav-item').removeClass('setting-modal-header-nav-item-active')
    $this.addClass('setting-modal-header-nav-item-active')
    if (indexs == 1) {
        $('.setting-connector').addClass('hide')
        $('.setting-index').removeClass('hide')
        $('.setting-volume').addClass('hide')
    } else if (indexs == 2) {
        $('.setting-connector').removeClass('hide')
        $('.setting-index').addClass('hide')
        $('.setting-volume').addClass('hide')
    } else if (indexs == 3) {
        $('.setting-connector').addClass('hide')
        $('.setting-index').addClass('hide')
        $('.setting-volume').removeClass('hide')
        getVolume()
    }
})

// 开启连点器
$(document).on('change', '#connectorcheck', function (e) {
    var $this = $(this)
    if ($this.prop('checked')) {
        $('.setting-connector-input').removeClass('hide')
    } else {
        $('.setting-connector-input').addClass('hide')
    }
})
$(document).on('keyup', '.setting-points-input', function (e) {
    var $this = $(this)
    let nums = 2
    if ($this.val() == "") {
        nums = ""
    } else {
        nums = parseInt($this.val().replace(/[^\d]/g, ''))
        if (nums > 99) {
            nums = 99
        }
    }
    $this.val(nums)
    updateMessage.getsetting.clickCounts = nums
})
$(document).on('blur', '.setting-connector-input input', function (e) {
    var $this = $(this)
    let nums = 2
    if ($this.val() == "") {
        nums = 2
    } else {
        nums = parseInt($this.val().replace(/[^\d]/g, ''))
        if (nums < 2) {
            nums = 2
        } else if (nums > 99) {
            nums = 99
        }
    }
    $this.val(nums)
})
$(document).on('click', '.language-select a', function () {
    var languagehtml = $(this).text()
    var languagetype = $(this).attr('data-type')
    $('.language-select .gx-menu-in').text(languagehtml)
    localStorage.setItem('SetLBWlanguage', languagetype);
})

function getVolume() {
    let temp_volume_json = [
        {
            type: 'text',
            text: '平台音量设置'
        },
        {
            type: 'rangeslider',
            text: '平台总音量',
            key: 'globalVolume',
            mute: false
        },
        {
            type: 'rangeslider',
            text: '平台背景音乐',
            key: 'clientBgm',
            mute: false
        },
        {
            type: 'rangeslider',
            text: '平台音效',
            key: 'clientSound',
            mute: false
        },
        {
            type: 'text',
            text: '游戏音量设置'
        },
        {
            type: 'rangeslider',
            text: '游戏内音乐',
            key: 'scoreVolume',
            mute: false
        },
        {
            type: 'rangeslider',
            text: '游戏内音效',
            key: 'soundVolume',
            mute: false
        },
        {
            type: 'rangeslider',
            text: '游戏内语音',
            key: 'soiceVolume',
            mute: false
        }]

    let temp_json = getSaveMessage('ClientVolume')

    for (let i = 0; i < temp_volume_json.length; i++) {
        if (temp_volume_json[i].type == 'rangeslider') {
            temp_volume_json[i].value = Number(temp_json[temp_volume_json[i].key].volume * 100)
            temp_volume_json[i].mute = temp_json[temp_volume_json[i].key].mute
        }
        if (temp_volume_json[i].key == 'scoreVolume') {
            temp_volume_json[i].value = Number(updateMessage.getsetting.scoreVolume) * 100
        }
        if (temp_volume_json[i].key == 'soundVolume') {
            temp_volume_json[i].value = Number(updateMessage.getsetting.soundVolume * 100)
        }
        if (temp_volume_json[i].key == 'soiceVolume') {
            temp_volume_json[i].value = Number(updateMessage.getsetting.voiceVolume * 100)
        }
    }

    $('.setting-volume .volume-content').html(template('volumeModal-script', {
        options: temp_volume_json,
    }))

    $('.volume-swich').each(function () {
        var $this = $(this)
        $this.gxswich({}, function (e) {
            if ($this.find('.volume-mute-div').hasClass('volume-mute') == true) {
                $this.find('.volume-mute-div').click()
            }

            let temp_volume = Number(e / 100)
            let temp_json = getSaveMessage('ClientVolume')
            temp_json[$this.attr('data-key')].volume = temp_volume
            let send_json = [
                temp_json['scoreVolume'].volume,
                temp_json['soundVolume'].volume,
                temp_json['soiceVolume'].volume,
            ]
            volumeControl($this.attr('data-key'), temp_volume)
            switch ($this.attr('data-key')) {
                case 'scoreVolume':
                    changeGameSound(send_json)
                    break;
                case 'soundVolume':
                    changeGameSound(send_json)
                    break;
                case 'soiceVolume':
                    changeGameSound(send_json)
                    break;
                case 'globalVolume':
                    volumeFn()
                    break;
                case 'clientBgm':
                    volumeFn()
                    break;
                case 'clientSound':
                    volumeFn()
                    $('#Audio_click')[0].play()
                    break;
            }
        })
    })
}


$(document).on('click', '.volume-mute-div', function () {
    let temp_json = getSaveMessage('ClientVolume')
    if ($(this).hasClass('volume-mute')) {
        $(this).removeClass('volume-mute')
        $(this).prev().removeClass('volume-mute-option')
        temp_json[$(this).attr('data-key')].mute = false
    } else {
        $(this).addClass('volume-mute')
        $(this).prev().addClass('volume-mute-option')
        temp_json[$(this).attr('data-key')].mute = true
    }
    saveMessage('ClientVolume', temp_json)
    volumeFn()
})

function volumeControl(audioName, volume) {
    let temp_json = getSaveMessage('ClientVolume')
    temp_json[audioName].volume = volume
    saveMessage('ClientVolume', temp_json)
}

let gameSoundArray = [
    getSaveMessage('ClientVolume')['scoreVolume'].volume,
    getSaveMessage('ClientVolume')['soundVolume'].volume,
    getSaveMessage('ClientVolume')['soiceVolume'].volume
]
// changeGameSound(gameSoundArray)

// 更改游戏内音效
function changeGameSound(array) {
    $.ajax({
        type: "get",
        url: '/api/site/game/volume/',
        data: $.param({ scoreVolume: array[0], soundVolume: array[1], voiceVolume: array[2] }, true),
        success: function (res) {
            updateMessage.getsetting.scoreVolume = array[0].toString
            updateMessage.getsetting.soundVolume = array[1].toString
            updateMessage.getsetting.voiceVolume = array[2].toString
        },
        error: function (req) { }
    })
}
// 关闭
$(document).on('click', '.settingmodalclose', function () {
    window.location.reload()
    ipcRenderer.send('Main', {
        msgType: "Close",
        jsonInfo: {
            window: "downloadmodal"
        }
    });
})
// 自动化
NewHelpAutoClick()
function NewHelpAutoClick() {
    if (getQueryVariable('auto') == 'InstallGame') {
        updateMessage.minsize = true
        let widthsize = 500
        let heightsize = 424
        ipcRenderer.send('Main', {
            msgType: "ChangeWindowSize",
            jsonInfo: {
                window: 'downloadmodal',
                width: widthsize,
                height: heightsize,
                isCenter: true
            }
        });
        $('.J_game-list').removeClass('hide');
        $('.J_game-setup').addClass('hide')
        $('.footer').addClass('hide')
    }
    if (getQueryVariable('auto') == 'RepairGame') {
        $('.translate-modal-content').addClass('hide')
        setTimeout(() => {
            ipcRenderer.send('Main', {
                msgType: "ChangeWindowSize",
                jsonInfo: {
                    window: 'downloadmodal',
                    width: 375,
                    height: 135,
                    bottomRight: true
                }
            });
            updateMessage.minsize = true
            $('.min-downmodal').removeClass('hide').html(template('min-downmodal-script'))
        }, 500)


        $.controlAjax({
            type: "get",
            url: '/Modal/RepairGameFiles',
            showerror: false,
            success: function (res) {

            },
            error: function (req) {

            }
        })
    }
}
$(document).on('click', '.min-downmodal-close', function () {
    ipcRenderer.send('Main', {
        msgType: "ChangeWindowSize",
        jsonInfo: {
            window: 'downloadmodal',
            width: 1,
            height: 1,
            isCenter: true
        }
    });
    // $('body').css('opacity', 0)
})
$(document).on('change', '.setting-connector-p .gx-switch input[type="checkbox"]', function () {
    var $this = $(this)
    let type = $this.closest('.gx-switch').attr('data-type')
    let isopen = $this.prop('checked')
    updateMessage.getsetting[type] = isopen
})
function zherror(url) {
    let geturl = url
    var pattern = /[^\u4E00-\u9FA5]/gi
    getstring = geturl.replace(pattern, '')
    getstring = getstring.split('')
    for (var i = 0; i < getstring.length; i++) {
        geturl = geturl.replaceAll(getstring[i], '<span style="color:#6B9EFF">' + getstring[i] + '</span>')
    }
    $('#zherrorModal .modal-dialog').html(template('zherrorModal-script'))
    $('#zherrorModal .f16').html(geturl)
    $('#zherrorModal').modal('show')
}
$(document).on('click', '.J_zherrorModalbtn', function () {
    $('#zherrorModal').modal('hide')
    $('.J_downloadAgain').click()
    setTimeout(() => {
        $('.J_downGame').click()
    }, 500)
})