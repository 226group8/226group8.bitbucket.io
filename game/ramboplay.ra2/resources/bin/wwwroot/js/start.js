const { ipcRenderer } = require("electron");

var startMessage = {
    aniamtefun: null,
    progressnum: 0
}
ipcRenderer.send('start-work', 'Hello');
ipcRenderer.on('WebIpc', (event, message) => {
    let jsonmessage = JSON.parse(message.jsonInfo)
    if (message.msgType == 21) {
        localStorage.setItem('Version', JSON.stringify(jsonmessage.version));
        //版本信息
        if (jsonmessage.status == 1) {
            //更新
            $('.upload').addClass('hide')
            $('.updown').removeClass('hide')
            ipcRenderer.send('Main', {
                msgType: "CheckUpdate",
                jsonInfo: {
                    status: 1
                }
            });
        } else {
            //正常
            $('body').animate({ opacity: 0 }, 300, 'linear', function () {
                ipcRenderer.send('Main', {
                    msgType: "ChangeWindowSize",
                    jsonInfo: {
                        window: 'main',
                        width: 1280,
                        height: 720,
                        miniWidth: 1280,
                        miniHeight: 720,
                        isCenter: true
                    }
                });
                location.href = '/Login/index'
            })
        }
    } else if (message.msgType == 20 && jsonmessage.sign == 'updateClient' && Number(startMessage.progressnum) < Number(jsonmessage.nowPercentage)) {
        startMessage.progressnum = Number(jsonmessage.nowPercentage)
        $('.pre-num').text(jsonmessage.nowPercentage + '%')
        $('.pre-div').css('width', jsonmessage.nowPercentage + "%")
    }
});