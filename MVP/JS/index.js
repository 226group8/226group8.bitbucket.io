
// menu
var lis = document.querySelectorAll('.left_menu li');
for (var i = 0; i < lis.length; i++) {

	lis[i].index = i;

	lis[i].addEventListener('mouseenter', function () {
		lis[this.index].children[1].style.display = "block";
	});
	lis[i].addEventListener('mouseleave', function () {
		lis[this.index].children[1].style.display = "none";
	});

}



// add to the cart
var add_to_carts = document.querySelectorAll('.add_to_cart');
for (var i = 0; i < add_to_carts.length; i++) {
	add_to_carts[i].addEventListener('click', function () {
		// alert("Add to cart")

		// delete the prompt
		document.querySelector('.cart_content .empty').style.display = "none";

		var image = this.parentNode.children[0].children[0].children[0].src;
		var title = this.parentNode.children[0].children[1].innerHTML;
		var price = this.parentNode.children[0].children[3].innerHTML;

		var section1 = document.createElement('section');
		section1.className = "cart_content_item";

		var section2 = document.createElement('section');
		section2.className = "image";
		var img = document.createElement('img');
		img.src = image;
		section2.appendChild(img);
		section1.appendChild(section2);

		var section3 = document.createElement('section');
		section3.className = "text";
		section3.innerHTML = title;
		section1.appendChild(section3);

		var section4 = document.createElement('section');
		section4.className = "price";
		section4.innerHTML = price;
		section1.appendChild(section4);

		var a = document.createElement('a');
		a.className = "delete_btn";
		a.href = "javascript:;";
		a.innerHTML = "Delete";
		a.onclick = function () {
			this.parentNode.remove();
			if (document.querySelectorAll('.cart_content .cart_content_item').length == 0) {
				document.querySelector('.cart_content .empty').style.display = "block";
				document.querySelector('.cart_content .gotopay').style.display = "none";
			}
		};
		section1.appendChild(a);
		
		document.querySelector('.cart_content .gotopay').style.display = "block";


		// Add to cart
		var cart_content = document.querySelector('.cart_content');
		cart_content.appendChild(section1);
		//		cart_content.innerHTML = cart_content.innerHTML + template;


	});
}













// The current playback index of the carousel
var slide_current = 0;

// toggle time in milliseconds
var slide_change_time = 3000;

// Carousel item collection
var slide_divs = document.querySelectorAll(".slide-item");

// Add the small icon of the carousel index
for (var i = 0; i < slide_divs.length; i++) {
	var index_span = document.createElement("span");//create a label
	if (i == 0) {
		index_span.classList.add("active");
	}
	index_span.setAttribute("data-index", i);// add custom properties

	// Add click event to small icon
	index_span.addEventListener('click', function () {
		slide_current = this.getAttribute("data-index");
		slide_change(slide_current);
	});
	document.getElementById("pagination").append(index_span);
}

// Index the collection of small icons
var pagination_spans = document.querySelectorAll("#pagination span");

/* Timer, the third parameter is the parameter of the first function */
var interval = setInterval(slide_control, slide_change_time, "next");

/**
* Carousel control
 * @param {Object} operation 
 */
function slide_control(operation) {
	if (operation == "prev") {

		if (slide_current == 0) {
			// already the first page
			slide_current = slide_divs.length - 1;
		} else {
			slide_current--;
		}
	} else if (operation == "next") {

		if (slide_current == slide_divs.length - 1) {
			// already the last page
			slide_current = 0;
		} else {
			slide_current++;
		}

	}
	slide_change(slide_current, operation);
}

















/**
* Switch to the specified index index
 * @param {Object} index
 */
function slide_change(index, operation) {

	//	slide_divs[index].style.left = slide_divs[index].offsetWidth;
	//	animate(slide_divs[index], 0, function() {
	//		
	//	});

	for (var i = 0; i < slide_divs.length; i++) {
		if (slide_current == i) {

			// show the current index
			pagination_spans[i].classList.add("active");
			slide_divs[i].style.zIndex = 100;
			if (operation == 'prev') {
				// move from left to right
				slide_divs[i].style.left = -slide_divs[i].offsetWidth + 'px';
			} else if (operation == 'next') {
				// move from right to left
				slide_divs[i].style.left = slide_divs[i].offsetWidth + 'px';
			}

			var obj = slide_divs[i];

			// use the timer as a property of the obj object
			// Clear the previous timer to ensure that there can only be one timer at the same time
			clearInterval(obj.timer1);
			obj.timer1 = setInterval(function (e) {
				// step = (target value - current position) / speed (the larger the value, the slower the movement speed, the smaller the value, the faster it is)
				var step = (0 - obj.offsetLeft) / 10;
				step = step > 0 ? Math.ceil(step) : Math.floor(step);
				obj.style.left = obj.offsetLeft + step + 'px';

				if (obj.offsetLeft == 0) {
					clearInterval(obj.timer1);
				}

			}, 5);




		} else {
			// hide other
			slide_divs[i].style.zIndex = -100;
			pagination_spans[i].classList.remove("active");
		}
	}
	// Turn off and then turn on first, to avoid the timer switching immediately when the switch is clicked
	clearInterval(interval);
	interval = setInterval(slide_control, slide_change_time, "next");
}

// previous page of carousel
document.querySelector("#slide-page-prev").addEventListener('click', function () {
	slide_control("prev");
});
// Carousel next page
document.querySelector("#slide-page-next").addEventListener('click', function () {
	slide_control("next");
});