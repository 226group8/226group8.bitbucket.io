// get all DOM elements that we need
const form = document.getElementById('form')
const username = document.getElementById('username')
const email = document.getElementById('email')
const password = document.getElementById('password')
const password2 = document.getElementById('password2')
const Address = document.getElementById('Address')
const tel = document.getElementById('tel')
const Birthdate = document.getElementById('Birthdate')


$('#tel').intlTelInput()

var oInput = document.querySelectorAll('input')
oInput.forEach((item) => {
  item.onblur = function () {
    checkEmail(email)
  }
})


// Event listeners
form.addEventListener('submit', function (e) {
  e.preventDefault()
  // e.stopPropagation()
  // use check required function to check array of fields
  checkTel(tel)
  checkRequired([username, email, password, password2, Address, Birthdate])
  checkEmail(email)
  checkLength(username, 3, 15)
  checkLength(password, 6, 25)
  checkPasswordsMath(password, password2)
  // checkTel(tel)
  checkBirthdate(Birthdate)
  let error = document.querySelectorAll('.error').length
  if (!error) {
    alert('Submitted form successfully!')
    let age = GetAge(Birthdate.value.trim())
    if (age > 50) {
      var msg = 'Would you like to see help information?\n\nIf you need help, please click "OK"!'
      if (confirm(msg) == true) {
        return (window.location.href = './help.html')
      } else {
        return false
      }
    }
  }
})


//Show input error message
function showError(input, message) {
  const formControl = input.parentElement
  // console.log(input.parent());
  console.log($('#tel').parent().parent());
  if (message.indexOf('Tel') > -1) {
    // $('#tel').parent().parent().addClass('error')
    // document.getElementsByClassName('.form-control').addClass('error')
    // formControl.parentElement.addClass('error')
    formControl.parentElement.className = 'form-control error'
  } else {
    // formControl.addClass('error')
    // $('#tel').parent().parent().addClass('error')
    formControl.className = 'form-control error'
  }
  const small =
    formControl.querySelector('small') ||
    formControl.parentElement.querySelector('small')
  small.innerText = message
}




//Show success outline
function showSuccess(input) {
  const formControl = input.parentElement
  formControl.className = 'form-control success'
}

// Check email is valid
function checkEmail(input) {
  const re =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if (input.value.trim() === '') return
  if (re.test(input.value.trim())) {
    showSuccess(input)
  } else {
    showError(input, 'Format : Eg. user@gmail.com.')
  }
}

//tel
function checkTel(input) {
  const re = /^\(0([0-9]{3})\)+[0-9]{6,11}$/;
  if (input.value.trim() === '') {
    $('#telCheck').css('display','block')
    $('#telCheck').text( `${getFieldName(input)} is required`)
    return
  }
  if (re.test(input.value.trim())) {
    showSuccess(input)
  } else {
    $('#telCheck').css('display','block')
    $('#telCheck').text('Format :(Conrtry Number) Number Eg.(0061)012345678.')
    // showError(input, 'Format :(Conrtry Number) Number Eg.(0061)012345678.')
  }
}

//Birthdate
function checkBirthdate(input) {
  if (input.value.trim() === '') return

  let age = GetAge(input.value.trim())

  if (age < 13) {
    alert('You must be under 13 years old to register for this website! Thank you for your visit')
    showError(input, 'You cannot register for this site if you are younger than 13')
  }
}

// check required fields
function checkRequired(inputArr) {
  // use high order array method to loop through array
  let isError = true
  inputArr.forEach(function (input) {
    if (input.value.trim() === '') {
      isError = false
      showError(input, `${getFieldName(input)} is required`)
    } else {
      showSuccess(input)
    }
  })

  return isError
}

//Check input length w/ mix max values
function checkLength(input, min, max) {
  if (input.value.trim() === '') return

  if (input.value.length < min || input.value.lenght > max) {
    showError(
      input,
      `${getFieldName(input)} must have ${min} to ${max} characters.`
    )
  } else if (max == 25) {
    if (!/^[A-Z][A-z0-9]*$/.test(input.value.trim())) {
      showError(input, `${getFieldName(input)} The first letter must be capitalized Eg.Yuanbozhang1007 `)
    }
  } else {
    showSuccess(input)
  }
}

// Password validator
function checkPasswordsMath(input1, input2) {
  if (input2.value.trim() === '') return
  if (input1.value !== input2.value) {
    showError(input2, 'Passwords do not match.')
  } else if (input2.value === '') {
    showError(input2, 'Please confirm your password.')
  } else {
    showSuccess(input2)
  }
}

// Get fieldname
function getFieldName(input) {
  return input.id.charAt(0).toUpperCase() + input.id.slice(1)
}

/**
 * @param strBirthday:Refers to the date of birth in the format "1990-01-01"
 */
function GetAge(strBirthday) {
  var returnAge,
    strBirthdayArr = strBirthday.split('-'),
    birthYear = strBirthdayArr[0],
    birthMonth = strBirthdayArr[1],
    birthDay = strBirthdayArr[2],
    d = new Date(),
    nowYear = d.getFullYear(),
    nowMonth = d.getMonth() + 1,
    nowDay = d.getDate()
  if (nowYear == birthYear) {
    returnAge = 0 //The same year is 0 years old
  } else {
    var ageDiff = nowYear - birthYear //The difference between the years
    if (ageDiff > 0) {
      if (nowMonth == birthMonth) {
        var dayDiff = nowDay - birthDay //The difference between the day
        if (dayDiff < 0) {
          returnAge = ageDiff - 1
        } else {
          returnAge = ageDiff
        }
      } else {
        var monthDiff = nowMonth - birthMonth //The difference of the month
        if (monthDiff < 0) {
          returnAge = ageDiff - 1
        } else {
          returnAge = ageDiff
        }
      }
    } else {
      returnAge = -1 //Returning -1 indicates that the date of birth was entered incorrectly later than today
    }
  }
  return returnAge //Return one year of age
}


